#include "Atom.hpp"
#include <fstream>
#include <stdexcept>
#include "Periodic_table.hpp"
#include "../Utility/String_Util.hpp"
#include "../Utility/Verbose.hpp"

using std::endl;
using std::ostream;
using std::string;

Atom::Atom()
{
  //charge = 0;
}
Atom::~Atom(){
}
Atom::Atom(int atomic_number, std::array<double,3> position, double charge)
{
  this->charge = charge;
  this->valence_charge = -1;
  //this->position = position;
  //this->position = new double[3];
  for(int i = 0; i < 3; ++i){
      this -> position[i] = position[i];
  }
  this->atomic_number = atomic_number;
  if(atomic_number > Periodic_atom::periodic_max){
      Verbose::all() << "Atom number " <<atomic_number <<" is invalid!" << std::endl;
      throw std::invalid_argument("Atom number " +String_Util::to_string(atomic_number)+" is invalid!");
  }
  this->symbol.assign(Periodic_atom::periodic_table[atomic_number-1]);
}

Atom::Atom(string sym, std::array<double,3> position, double charge)
{
  this->charge = charge;
  this->valence_charge = -1;
  //this->position = position;
  //this->position = new double[3];
  for(int i = 0; i < 3; ++i){
      this -> position[i] = position[i];
  }
  int i=0;
  while(Periodic_atom::periodic_table[i]!=sym){
    ++i;
    if(i >= Periodic_atom::periodic_max){
        Verbose::all() << "Atom " << sym <<" is invalid!" << std::endl;
        throw std::invalid_argument("Atom " + sym + " is invalid!");
    }
  }
  this->atomic_number=i+1;
  this->symbol.assign(sym);
}
Atom::Atom(Atom& atom){
    //this->position = new double[3];
    this->charge=atom.get_charge();
    this->valence_charge = atom.get_valence_charge();
    this->position[0]=atom.get_position()[0];
    this->position[1]=atom.get_position()[1];
    this->position[2]=atom.get_position()[2];
    this->atomic_number = atom.get_atomic_number();
    this->symbol.assign(Periodic_atom::periodic_table[atomic_number-1]);
}

Atom::Atom(const Atom& atom){
    //this->position = new double[3];
    this->charge=atom.get_charge();
    this->valence_charge=atom.get_valence_charge();
    this->position[0]=atom.get_position()[0];
    this->position[1]=atom.get_position()[1];
    this->position[2]=atom.get_position()[2];
    this->atomic_number = atom.get_atomic_number();
    this->symbol.assign(Periodic_atom::periodic_table[this->atomic_number-1]);
}

int Atom::get_atomic_number()
{
  return this->atomic_number;
}

int Atom::get_atomic_number() const
{
  return this->atomic_number;
}

double Atom::get_valence_charge() const
{
  return this->valence_charge;
}

std::array<double,3> Atom::get_position() const
{
  return this->position;
}

//double * Atom::get_position() const
//{
//  return this->position;
//}

double Atom::get_charge()
{
  return this->charge;
}

double Atom::get_charge() const
{
  return this->charge;
}

double Atom::get_radius()
{
  return Periodic_atom::covalent_radii[atomic_number-1];
}
string Atom::get_symbol()
{
  return this->symbol;
}

bool Atom::operator==(Atom new_atom){
    if (atomic_number != new_atom.get_atomic_number()){
        return false;
    }
    if (position[0] != new_atom.get_position()[0]){
        return false;
    }
    if (position[1] != new_atom.get_position()[1]){
        return false;
    }
    if (position[2] != new_atom.get_position()[2]){
        return false;
    }
    return true;
}

Atom& Atom::operator=(const Atom& original_atom) 
{ 
    atomic_number = original_atom.get_atomic_number();
//    delete[] position;
//    position = new double[3];
    position[0] = original_atom.get_position()[0];
    position[1] = original_atom.get_position()[1];
    position[2] = original_atom.get_position()[2];
    charge = original_atom.get_charge();
    valence_charge = original_atom.get_valence_charge();
    return *this;
}

Atom& Atom::operator=( Atom& original_atom){
    atomic_number = original_atom.get_atomic_number();
//    delete[] position;
//    position = new double[3];
    position[0] = original_atom.get_position()[0];
    position[1] = original_atom.get_position()[1];
    position[2] = original_atom.get_position()[2];
    charge = original_atom.get_charge();
    valence_charge = original_atom.get_valence_charge();
    return *this;
}
void Atom::set_valence_charge(double valence_charge){
    this->valence_charge= valence_charge;
    return;
}
void Atom::set_position(std::array<double,3> position){
    this->position[0]=position[0];
    this->position[1]=position[1];
    this->position[2]=position[2];
    return ;
}
bool Atom::operator!=(Atom new_atom){
    return !(this->operator==(new_atom) );
}
ostream &operator <<(ostream &c, Atom &atom){
    Verbose::all() << "Atom::\t" << "atomic number: \t"<< atom.atomic_number << "\t position: " << atom.position[0] << "," << atom.position[1] << ","<< atom.position[2] <<endl;
    return c;
}
ostream &operator <<(ostream &c, Atom* patom){
    Verbose::all() << *patom ;
    return c;
}

