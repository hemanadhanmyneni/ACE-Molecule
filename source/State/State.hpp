#pragma once
#include <string>
#include <vector>
#include "Teuchos_RCP.hpp"
#include "Teuchos_Array.hpp"
#include "Teuchos_ParameterList.hpp"
//#include "AnasaziTypes.hpp"
#include "Epetra_MultiVector.h"
#include "Epetra_Vector.h"
//#include "Epetra_Map.h"

#include "../Io/Atoms.hpp"
#include "../Basis/Basis.hpp"
#include "../Core/Occupation/Occupation.hpp"
#include "../Utility/Time_Measure.hpp"

/**
    @brief State class contains information for atoms, orbitals, and density
    @author Sunghwan Choi
*/
class State{
    friend class Guess;
    public:
        State();
        //State(RCP<Epetra_Map> map , int polarize);
        //State(RCP<const Epetra_Map> map , int polarize);

        Teuchos::RCP<Time_Measure> timer;
        //State(Array<RCP<Epetra_MultiVector> > orbitals,RCP<Epetra_MultiVector> density,Array<RCP<Occupation> > occupations, Array< std::vector<double > > orbital_energies,RCP<Atoms> atoms,RCP<Epetra_Vector> hartree_potential);
        /**
         * @brief Copy constructor. Technically same with operator=.
         **/
        State(const State& state);
        /**
         * @brief Assign operator.
         * @note Orbital may be shallow copied. See do_shallow_copy_orbitals. Other quantities are deep copied.
         **/
        State& operator=(const State& state);

        /**
         * @brief Get atoms object.
         **/
        Teuchos::RCP<const Atoms> get_atoms() const;
        /**
         * @breif Set atoms object.
         **/
        int set_atoms(Teuchos::RCP<Atoms> atoms);

        Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > get_orbitals() const;
        Teuchos::Array< Teuchos::RCP<const Epetra_Vector> > get_density() const;
        Teuchos::Array< Teuchos::RCP<const Occupation> > get_occupations() const;
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > get_local_potential() const;
        Teuchos::RCP<Epetra_Vector> get_hartree_potential() const;
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > get_core_density() const;
        Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > get_core_density_grad() const;
        Teuchos::Array< std::vector<double > > get_orbital_energies() const ;
        double get_nuclear_nuclear_repulsion() const;
        double get_total_energy() const;

        /**
         * @brief Write orbital to file.
         * @param name Output filename.
         * @param basis Calculation basis/basis.
         * @param type Output type. Choose among cube, or writing axis (x, y, z).
         * @param start Lowest orbital index to write (0 for lowest orbital).
         * @param end Highest orbital index to write (-1 for highest orbital).
         * @return Always zero.
         **/
        int write_orbital(std::string name, Teuchos::RCP<const Basis> basis, std::string type, int start = 0, int end = -1);
        /**
         * @brief Write density to file.
         * @param name Output filename.
         * @param basis Calculation basis/basis.
         * @param type Output type. Choose among cube, or writing axis (x, y, z).
         * @param is_ae If true, write all-electron density if possible. Otherwise, write pseudo electron density.
         * @return Always zero.
         **/
        int write_density(std::string name, Teuchos::RCP<const Basis> basis, std::string type, bool is_ae);
        /**
         * @brief Write local potential to file.
         * @param name Output filename.
         * @param basis Calculation basis/basis.
         * @param type Output type. Choose among cube, or writing axis (x, y, z).
         * @return Always zero.
         **/
        int write_local_potential(std::string name, Teuchos::RCP<const Basis> basis, std::string type);
        /**
         * @brief Write hartree potential to file.
         * @param name Output filename.
         * @param basis Calculation basis/basis.
         * @param type Output type. Choose among cube, or writing axis (x, y, z).
         * @return Always zero.
         **/
        int write_hartree_potential(std::string name, Teuchos::RCP<const Basis> basis, std::string type);
        /**
         * @brief Write exchange-correlation potential to file.
         * @param name Output filename.
         * @param basis Calculation basis/basis.
         * @param type Output type. Choose among cube, or writing axis (x, y, z).
         * @return Always zero.
         **/
        int write_total_xc_potential(std::string name, Teuchos::RCP<const Basis> basis, std::string type);
        /**
         * @brief Write potential due to field.
         * @param name Output filename.
         * @param basis Calculation basis/basis.
         * @param type Output type. Choose among cube, or writing axis (x, y, z).
         * @return Always zero.
         **/
        int write_potential_from_field(std::string name, Teuchos::RCP<const Basis> basis, std::string type);
        
        /**
         * @brief Read parameters->Output and write corresponding entities to file.
         * @details Can write density, local potential, orbital, and geometry.
         * @param basis Calculation basis/basis.
         * @param parameters Parameters for output control.
         * @return Always zero.
         * @note See also write_orbital, write_density, write_local_potential, and Io_Atoms::write_atoms.
         **/
        int write(Teuchos::RCP<const Basis> basis, Teuchos::RCP<Teuchos::ParameterList> parameters);

        Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > orbitals;
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > density;
        Teuchos::Array< Teuchos::RCP<Occupation> > occupations;
        Teuchos::Array< std::vector<double> > orbital_energies;
        Teuchos::RCP<Epetra_Vector> hartree_potential;
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > potential_from_field;
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > local_potential;

        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > core_density;
        Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > core_density_grad;

        std::vector<std::vector< std::array< std::complex<float> ,9> > > polarizability;
        double total_energy = 0.0;
        double total_free_energy = 0.0;
        double nuclear_nuclear_repulsion;
        Teuchos::RCP<Atoms> atoms;

        /**
         * @brief If set to true, copy constructor and assign operator performs shallow copy for orbitals.
         **/
        static bool do_shallow_copy_orbitals;
    protected:
        void copy_orbitals(const Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > orbitals);
        void copy_orbitals(Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbitals);

};
