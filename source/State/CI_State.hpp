#pragma once
#include "State.hpp"

class CI_State: public State{
    public:
        int max_excitation;
        int size_ci_matrix;

        vector< vector<int> > hole_indices;
        vector< vector<int> > particle_indices;

        vector<double> ci_coefficients;
        vector<double> ci_corrections;

    protected:

        int initialize_excitation(double min_energies, double max_energies);
        int initialize_excitation(int num_occ, double num_vir);
};
