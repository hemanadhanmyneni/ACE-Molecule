#include <string>

#include "Teuchos_RCP.hpp"
#include "Teuchos_Array.hpp"
#include "State.hpp"

namespace Dynamic_Accessor{
    int get(Teuchos::RCP<State> state, std::string s, double& output);
    int get(Teuchos::RCP<State> state, std::string s, int i_spin, Teuchos::RCP<Epetra_MultiVector>& output);
    int get(Teuchos::RCP<State> state, std::string s, int i_spin, Teuchos::RCP<Epetra_Vector>& output);
    int set(Teuchos::RCP<State> state, std::string s, double output);
    int set(Teuchos::RCP<State> state, std::string s, int i_spin, Teuchos::RCP<Epetra_MultiVector> output);
    int set(Teuchos::RCP<State> state, std::string s, int i_spin, Teuchos::RCP<Epetra_Vector> output);

};
