#pragma once
#include <string>
#include <map>

#include "Teuchos_RCP.hpp"
#include "Teuchos_Array.hpp"

#include "Epetra_Vector.h"
#include "Epetra_MultiVector.h"

//#include "../Compute/Compute.hpp"
#include "../Basis/Basis.hpp"
#include "State.hpp"

/**
 * @brief States that contains things related with exchange correlation calculations.
 * @note All classes calculating XC should be friend of this!
 * @author Sungwoo Kang
 * @date 18. 9. 18. (Updated)
 **/
class XC_State: public State{
    friend class Exchange_Correlation;
    friend class Paw_XC2;
    friend class Libxc; friend class KLI;
    friend class XC_Custom;
    friend class XC_LC_wPBE_NGau; friend class XC_Orb_Hybrid;
    public:
        XC_State& operator=(const XC_State& state);
        /**
         * @brief Copy constructor. Technically same with operator=.
         **/
        XC_State(const XC_State& state);
        XC_State(int xc_id);

        double get_energy();

        Teuchos::Array< Teuchos::RCP<const Epetra_Vector> > get_val_density() const;
        Teuchos::Array< Teuchos::RCP<const Epetra_Vector> > get_potential() const;
        Teuchos::RCP<const Epetra_Vector> get_energy_density() const;
        Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > get_density_gradient() const;
        void set_energy(double energy);
        void set_potential(Teuchos::Array< Teuchos::RCP<Epetra_Vector> > vxc);
        void set_energy_density(Teuchos::RCP<Epetra_Vector> exc);

        void set_lda_kernel(Teuchos::Array< Teuchos::RCP<Epetra_Vector> > fxc);
        void set_gga_kernel(
                Teuchos::Array< Teuchos::RCP<Epetra_Vector> > vsigma,
                Teuchos::Array< Teuchos::RCP<Epetra_Vector> > v2rho2,
                Teuchos::Array< Teuchos::RCP<Epetra_Vector> > v2rhosigma,
                Teuchos::Array< Teuchos::RCP<Epetra_Vector> > v2sigma2
                );

        void get_lda_kernel(Teuchos::Array< Teuchos::RCP<Epetra_Vector> > &fxc);
        void get_gga_kernel(
                Teuchos::Array< Teuchos::RCP<Epetra_Vector> > &vsigma,
                Teuchos::Array< Teuchos::RCP<Epetra_Vector> > &v2rho2,
                Teuchos::Array< Teuchos::RCP<Epetra_Vector> > &v2rhosigma,
                Teuchos::Array< Teuchos::RCP<Epetra_Vector> > &v2sigma2
                );

        std::map<std::string, bool> get_necessary_fields() const;
        std::string get_functional_type() const;
        double get_EXX_portion() const;
        void get_CAM_portion(double &omega, double &beta, double &alpha) const;
        void set_lrc_info(double exx_portion, double omega);
        int get_xc_id() const;
        double LRC_alpha = 0.0;
        double LRC_omega = 0.0;
        int rel_flag = 0;
        int spin_size = 1;
    protected:
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > all_density;
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > val_density;
        Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > density_grad;
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > density_laplacian;
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > kinetic_energy_density;
        //Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > orbitals; // Defined in State
        //Teuchos::Array< Teuchos::RCP<Occupation> > occupations; // Defined in State
        /**
         * @brief Functional ID of Libxc.
         * @details First entity of vector is XCFunctional ID or XFunctional ID.
         * Second entity of vector is CFunctional ID.
         */
        int xc_id;
        std::map<std::string, bool> is_necessary;

        double energy = 0.0;
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > potential;
        Teuchos::RCP<Epetra_Vector> energy_density;

        // LDA kernel variables
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > fxc;

        // GGA kernel variables
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > vsigma;
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > v2rho2;
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > v2rhosigma;
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > v2sigma2;

        double EXX_portion = 0.0;//< Exact exchange portion defined in functional definition.
        double CAM_omega = 0.0;//< CAM-type omega coefficient defined in functional definition.
        double CAM_alpha = 0.0;//< CAM-type global HF coefficient
        double CAM_beta = 0.0;//< CAM-type short range HF coefficient. Negative if long-ranged.
        double nlc_b = 0.0;//< VV10-type NL correlation. Parameter b in 10.1063/1.3521275.
        double nlc_c = 0.0;//< VV10-type NL correlation  Parameter C in 10.1063/1.3521275.
        std::string type;
    private:
        void initialize_Libxc(int function_id);
        void initialize_KLI(int function_id);
};
