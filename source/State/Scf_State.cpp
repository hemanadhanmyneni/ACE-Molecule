#include "Scf_State.hpp"

using std::vector;
using Teuchos::rcp;
using Teuchos::Array;
using Teuchos::RCP;
using Teuchos::null;

Scf_State::Scf_State(): State(){
}

Scf_State::Scf_State(const Scf_State& state){
    this->operator=(state);
}
Scf_State::Scf_State(const State& state){
    this->operator=(state);
}
Scf_State& Scf_State::operator=(const Scf_State& state){
/*
    this->orbitals=Array< RCP<Epetra_MultiVector> >( state.orbitals);
    this->density =Array<RCP<Epetra_Vector> > ( state.density );
    this->occupations = Array<RCP<Occupation> >( state.occupations );

    this->orbital_energies=Array< vector<double > > ();
    for (int i=0; i<state.orbital_energies.size();i++){
        vector<double > destVector;
        vector<double > sourceVector=state.orbital_energies.at(i);
        destVector.assign(sourceVector.begin(),sourceVector.end());
        this->orbital_energies.append( destVector );
    }
    if (state.atoms!=null){
        this->atoms = rcp(new Atoms(*state.atoms) );
    }
    else{
        this->atoms==null;
    }
    this->nuclear_nuclear_repulsion = state.nuclear_nuclear_repulsion;
    this->total_energy = state.total_energy;
*/

//    this->orbitals=Array< RCP<Epetra_MultiVector> >(state.orbitals);
    this->orbitals=Array< RCP<Epetra_MultiVector> >();
    copy_orbitals(state.orbitals);
//    this->density =Array<RCP<Epetra_Vector> > (state.density);
    this->density =Array<RCP<Epetra_Vector> > ();
    for (int i =0; i < state.orbitals.size(); i++){
        this->density.append( rcp(new Epetra_Vector(*state.density.at(i) )) );
    }
//    this->occupations = Array<RCP<Occupation> >(state.occupations);
    this->occupations = Array<RCP<Occupation> >();
    for(int i =0; i <state.occupations.size();i++){
        this->occupations.append( rcp( state.occupations[i]->clone()  ) );
    }
//    this->orbital_energies=Array< vector<double > > (state.orbital_energies);
    this->orbital_energies=Array< vector<double > > ();
    for (int i=0; i<state.orbital_energies.size();i++){
        vector<double > destVector;
        vector<double > sourceVector=state.orbital_energies.at(i);
        destVector.assign(sourceVector.begin(),sourceVector.end());
        this->orbital_energies.append( destVector );
    }
//    this->local_potential = Array<RCP<Epetra_Vector> > (state.local_potential);
    this->local_potential = Array<RCP<Epetra_Vector> > ();
    if(state.local_potential.size()>0){
        for (int i =0; i < state.local_potential.size(); i++){
            this->local_potential.append( rcp(new Epetra_Vector(*state.local_potential.at(i) )) );
        }
    }
    this->hartree_potential = rcp(new Epetra_Vector(*state.hartree_potential ));

    if (state.core_density.size() > 0){
    //if (state.core_density!=null){
        for(int i_spin=0; i_spin<occupations.size(); i_spin++){
            core_density.push_back(rcp(new Epetra_Vector(*state.core_density[i_spin])));
        }
    }
    if(state.core_density_grad.size() > 0){
    //if(state.core_density_grad!=null){
        for(int i_spin=0; i_spin<occupations.size(); i_spin++){
            core_density_grad.push_back(rcp(new Epetra_MultiVector(*state.core_density_grad[i_spin])));
        }
    }
    //this->core_density = rcp ( new Epetra_Vector(*state.core_density));
    //this->core_density_grad = rcp( new Epetra_Vector(*state.core_density_grad));
    if(state.atoms==null){
        this->atoms = null;
    }
    else{
        this->atoms = rcp(new Atoms(*state.atoms) );
    }
    this->nuclear_nuclear_repulsion = state.nuclear_nuclear_repulsion;
    this->total_energy = state.total_energy;
//    this->hartree_potential = state.hartree_potential;
    this->hartree_potential = rcp(new Epetra_Vector(*state.hartree_potential));
//    this->x_potential = state.x_potential;
//    this->c_potential = state.c_potential;
    this->x_potential = Array<RCP<Epetra_Vector> > ();
    this->c_potential = Array<RCP<Epetra_Vector> > ();
    for (int i =0; i < state.x_potential.size(); i++){
        this->x_potential.append( rcp(new Epetra_Vector(*state.x_potential.at(i) )) );
    }
    for (int i =0; i < state.c_potential.size(); i++){
        this->c_potential.append( rcp(new Epetra_Vector(*state.c_potential.at(i) )) );
    }

    this->hartree_energy = state.hartree_energy;
    this->kinetic_energy = state.kinetic_energy;
    this->external_energy = state.external_energy;
    this->x_energy = state.x_energy;
    this->c_energy = state.c_energy;
    
    return *this;
}

Scf_State& Scf_State::operator=(const State& state){
    this->orbitals=Array< RCP<Epetra_MultiVector> >();
    copy_orbitals(state.get_orbitals());
    this->density =Array<RCP<Epetra_Vector> > ();

    for (int i =0; i < state.get_density().size(); i++){
        this->density.append( rcp(new Epetra_Vector(*state.get_density().at(i) )) );
    }

    this->occupations = Array<RCP<Occupation> >();
    for(int i =0; i <state.get_occupations().size();i++){
        this->occupations.append( rcp(state.get_occupations()[i]->clone() ) );
    }
    this->orbital_energies=Array< vector<double > > ();
    for (int i=0; i<state.get_orbital_energies().size(); i++){
        vector<double> destVector;
        vector<double> sourceVector=state.get_orbital_energies().at(i);
        destVector.assign(sourceVector.begin(),sourceVector.end());
        this->orbital_energies.append( destVector );
    }

    this->local_potential = Array<RCP<Epetra_Vector> > ();
    if(state.get_local_potential().size()>0){
        for (int i =0; i < state.get_local_potential().size(); i++){
            this->local_potential.append( rcp(new Epetra_Vector(*state.get_local_potential().at(i) )) );
        }
    }
    if (state.get_core_density().size() > 0){
    //if (state.core_density!=null){
        for(int i_spin=0; i_spin<state.get_core_density().size(); i_spin++){
            core_density.push_back(rcp(new Epetra_Vector(*state.get_core_density()[i_spin])));
        }
    }
    if(state.get_core_density_grad().size() > 0){
    //if(state.core_density_grad!=null){
        for(int i_spin=0; i_spin<state.get_core_density_grad().size(); i_spin++){
            core_density_grad.push_back(rcp(new Epetra_MultiVector(*state.get_core_density_grad()[i_spin])));
        }
    }

    this->atoms = rcp(new Atoms(*(state.get_atoms()) ) );
    this->nuclear_nuclear_repulsion = state.get_nuclear_nuclear_repulsion();
    this->total_energy = state.get_total_energy();
    return *this;
}
