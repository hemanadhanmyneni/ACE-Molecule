#pragma once
#include <vector>
#include <string>
#include <array>

#include "Compute.hpp"
#include "../Basis/Basis.hpp"
#include "../State/State.hpp"

#include "../Compute/Compute_XC.hpp"
#include "Libxc.hpp"
#include "KLI.hpp"
#include "Poisson_Solver.hpp"

/**
 * @brief Class for calculate Hybrid functional.
 * @details Hybrid class contains several different Exchange_Correlation type object to calculate Hybrid functionals.
 * Basically, each method just combine results from different types of exchange-correlation functionals.
 * @callergraph
 * @callgraph
 **/
class Exchange_Correlation: public Compute{
    public:
        /**
         * @brief New Constructor of Exchange_Correlation class.
         * @param basis Basis data
         * @param xc_classes XC functional computation class
         * @param xc_portions XC functional portions of each functional
         * @param is_grad_from_orbital Decides orbital or density will be used for density gradient calculations.
         * @details
         * x_ids_and_portions or c_ids_and_portions looks like
         * '101 0.3'
         * which means PBE exchange functional with 0.3 portion.
         * All initializations are done in initialization method.
         * @callergraph
         * @callgraph
         */
         Exchange_Correlation(
             Teuchos::RCP<const Basis> basis,
             Teuchos::Array< Teuchos::RCP<Compute_XC> > xc_classes,
             std::vector<double> xc_scale,
             bool is_grad_from_orbital/* = true*/
         );

        ~Exchange_Correlation();
        /**
         * @brief Calculate exchange-correlation energy and potential of given state.
         * @details compute function for the object of State class.
         * @return 0 (normal termination) The computed exchange-correlation energy is added to total energy, and the potential is added to state->local_potential[i_spin]
         * @callergraph
         * @callgraph
         */
        int compute(
            Teuchos::RCP<const Basis> basis,
            Teuchos::Array<Teuchos::RCP<State> >& states
        );
        /**
         * @brief Calculate exchange-correlation energy and potential of given Scf_state.
         * @details compute function for the object of Scf_State class.
         * @return 0 (normal termination) The computed exchange-correlation energy is replaced to exchange_energy and correlation_energy of last Scf_State, and the exchange_potential and correlation_potential of last Scf_State is replaced with calcaulated one.
         * @callergraph
         * @callgraph
         */
        int compute(
            Teuchos::RCP<const Basis> basis,
            Teuchos::Array<Teuchos::RCP<Scf_State> >& states
        );


        /**
         * @callergraph
         * @callgraph
         **/
        void compute_Exc(
            Teuchos::RCP<State> & state,
            double &x_energy,
            double &c_energy
        );

        /**
         * @callergraph
         * @callgraph
         **/
        void integrate_rho_vxc(
            Teuchos::RCP<State> & state,
            double& int_n_vxc
        );

        /**
         * @callergraph
         * @callgraph
         **/
        void compute_vxc(
                Teuchos::RCP<State> & state,
                Teuchos::Array< Teuchos::RCP<Epetra_Vector> >& x_potential,
                Teuchos::Array< Teuchos::RCP<Epetra_Vector> >& c_potential
        );

        /**
         * @callergraph
         * @callgraph
         **/
        void compute_fxc(
            Teuchos::RCP<State> state
        );
        
        /**
         * @brief Calculate TDDFT kernel for spin-restricted systems
         * @details Compute the following term \f[
         *              \int \phi_i^* ({\bf r}_1) \phi_a({\bf r}_1) f_{xc}({\bf r}_1,{\bf r}_2) \phi_b^*({\bf r}_2) \phi_j({\bf r}_2) d\tau_1 d\tau_2
         *          \f]
         * @return A matrix element of the Casida equation (or Tamm-Dancoff approx. equation)
         * @param orbital_coeff orbital coeffecients of the given system
         * @param orbitals orbital values of the given system
         * @param ia_coeff coefficients of \f[
         *                                    \phi_i^*({\bf r}) \phi_a({\bf r})
         *                                 \f]
         * @param jb_coeff coefficients of \f[
         *                                    \phi_b^*({\bf r}) \phi_j({\bf r})
         *                                 \f]
         * @param grad_ia values of \f[
         *                             \nabla [ \phi_i^* ({\bf r}) \phi_a({\bf r}) ]
         *                          \f]
         * @param grad_jb values of \f[
         *                             \nabla [ \phi_j^* ({\bf r}) \phi_b({\bf r}) ]
         *                          \f]
         * @param grad_rho_dot_grad_ia values of \f[
         *                                          \nabla \rho({\bf r}) \cdot \nabla [ \phi_i({\bf r}) \phi_a({\bf r}) ]
         *                                       \f]
         * @param grad_rho_dot_grad_jb values of \f[
         *                                          \nabla \rho({\bf r}) \cdot \nabla [ \phi_j({\bf r}) \phi_b({\bf r}) ]
         *                                       \f]
         * @param Kij values of \f[
         *                         \int \frac{\phi_i({\bf r}_1) \phi_j({\bf r}_1)}{|{\bf r}_1 - {\bf r}_2|} d\tau_1
         *                      \f]
         * @param i index of an occupied orbital
         * @param a index of an unoccupied orbital
         * @param j index of an occupied orbital
         * @param b index of an unoccupied orbital
         * @param spin_index spin index
         * @param total_PGG values from the PGG kernel \f[
         * \int \phi_i^* ({\bf r}_1) \phi_a({\bf r}_1) f_{xc}({\bf r}_1,{\bf r}_2) \phi_b^*({\bf r}_2) \phi_j({\bf r}_2) d\tau_1 d\tau_2
         *                                             \f]
         *                  where the PGG kernel is defined as \f[
         * f_{xc}({\bf r}_1,{\bf r}_2) = -\frac{2|\sum_k n_k \phi_k({\bf r}_1) \phi_k^*({\bf r}_2)|^2}{|{\bf r}_1 - {\bf r}_2| \rho({\bf r}_1) \rho({\bf r}_2)}
         * \f]
         * where n_k is the occupation number.
         * @note See DOI: 10.1016/S0009-2614(99)00137-2 written by Hirata for GGA.
         * @todo Check this function is used.
         * @callergraph
         * @callgraph
         **/
        std::vector<double> ia_fxc_jb_decoupled(
                Teuchos::RCP<Epetra_MultiVector> orbitals,
                Teuchos::RCP<Epetra_Vector> ia_coeff,
                Teuchos::RCP<Epetra_Vector> jb_coeff,
                Teuchos::RCP<Epetra_MultiVector> grad_ia,
                Teuchos::RCP<Epetra_MultiVector> grad_jb,
                Teuchos::RCP<Epetra_MultiVector> vsigma_grad_ia,
                Teuchos::RCP<Epetra_Vector> v2rho2_ia_coeff,
                Teuchos::RCP<Epetra_Vector> grad_rho_dot_grad_ia,
                Teuchos::RCP<Epetra_Vector> grad_rho_dot_grad_jb,
                int i, int a, int j, int b, double total_PGG,
                bool force_polarized = false
                );
        
        /**
         * @brief Calculate TDDFT kernel
         * @details Compute the following term \f[
         *              \int \phi_i^* ({\bf r}_1) \phi_a({\bf r}_1) f_{xc}({\bf r}_1,{\bf r}_2) \phi_b^*({\bf r}_2) \phi_j({\bf r}_2) d\tau_1 d\tau_2
         *          \f]
         * @return A matrix element of the Casida equation (or Tamm-Dancoff approx. equation)
         * @param orbital_coeff orbital coeffecients of the given system
         * @param orbitals orbital values of the given system
         * @param ia_coeff coefficients of \f[
         *                                    \phi_i^*({\bf r}) \phi_a({\bf r})
         *                                 \f]
         * @param jb_coeff coefficients of \f[
         *                                    \phi_b^*({\bf r}) \phi_j({\bf r})
         *                                 \f]
         * @param grad_ia values of \f[
         *                             \nabla [ \phi_i^* ({\bf r}) \phi_a({\bf r}) ]
         *                          \f]
         * @param grad_jb values of \f[
         *                             \nabla [ \phi_j^* ({\bf r}) \phi_b({\bf r}) ]
         *                          \f]
         * @param grad_rho_dot_grad_ia values of \f[
         *                                          \nabla \rho({\bf r}) \cdot \nabla [ \phi_i({\bf r}) \phi_a({\bf r}) ]
         *                                       \f]
         * @param grad_rho_dot_grad_jb values of \f[
         *                                          \nabla \rho({\bf r}) \cdot \nabla [ \phi_j({\bf r}) \phi_b({\bf r}) ]
         *                                       \f]
         * @param Kij values of \f[
         *                         \int \frac{\phi_i({\bf r}_1) \phi_j({\bf r}_1)}{|{\bf r}_1 - {\bf r}_2|} d\tau_1
         *                      \f]
         * @param i index of an occupied orbital
         * @param a index of an unoccupied orbital
         * @param j index of an occupied orbital
         * @param b index of an unoccupied orbital
         * @param spin_index spin index
         * @param total_PGG values from the PGG kernel \f[
         * \int \phi_i^* ({\bf r}_1) \phi_a({\bf r}_1) f_{xc}({\bf r}_1,{\bf r}_2) \phi_b^*({\bf r}_2) \phi_j({\bf r}_2) d\tau_1 d\tau_2
         *                                             \f]
         *                  where the PGG kernel is defined as \f[
         * f_{xc}({\bf r}_1,{\bf r}_2) = -\frac{2|\sum_k n_k \phi_k({\bf r}_1) \phi_k^*({\bf r}_2)|^2}{|{\bf r}_1 - {\bf r}_2| \rho({\bf r}_1) \rho({\bf r}_2)}
         * \f]
         * where n_k is the occupation number.
         * @note See DOI: 10.1016/S0009-2614(99)00137-2 written by Hirata for GGA.
         * @todo Check this function is used.
         * @callergraph
         * @callgraph
         **/
        double ia_fxc_jb(
                Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > orbitals,
                Teuchos::RCP<Epetra_Vector> ia_coeff,
                Teuchos::RCP<Epetra_Vector> jb_coeff,
                Teuchos::RCP<Epetra_MultiVector> grad_ia,
                Teuchos::RCP<Epetra_MultiVector> grad_jb,
                Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > vsigma_grad_ia,
                Teuchos::Array< Teuchos::RCP<Epetra_Vector> > v2rho2_ia_coeff,
                Teuchos::Array< Teuchos::RCP<Epetra_Vector> > grad_rho_dot_grad_ia,
                Teuchos::Array< Teuchos::RCP<Epetra_Vector> > grad_rho_dot_grad_jb,
                int s1, int i, int a, int s2, int j, int b, double total_PGG
        );

        /**
         * @callergraph
         * @callgraph
         **/
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > get_vsigma();
        /**
         * @callergraph
         * @callgraph
         **/
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > get_v2rho2();

        void print_xc_info();
        /**
         * @callergraph
         * @callgraph
         **/
        void update_xc_info(Teuchos::RCP<State> state, bool force_polarized = false);
        void get_saved_grad_density(Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > &saved_grad_density) const;
        std::string get_functional_type() const;
        double get_EXX_portion() const;
        Teuchos::RCP<const Poisson_Solver> get_poisson_solver() const;
    protected:
        Teuchos::Array< Teuchos::RCP<Compute_XC> > xc_list; ///<Exchange_Corrlation object array which contain each component of functionals.
        Teuchos::Array< Teuchos::RCP<XC_State> > xc_data;
        std::vector<std::string> xc_name;
        std::vector<int> xc_kind;

        std::vector<int> xc_function_id; ///< XC functional ID list for functionals.
        std::vector<double> xc_portion;  ///< XC functional scaling factor list for functionals.
        bool EXX_functional_exist; ///< variable that indicate whether KLI type functional is exist

        Teuchos::RCP<Poisson_Solver> poisson_solver;
        Teuchos::RCP<const Basis> basis;
        bool is_grad_from_orbital;

        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > fxc;
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > vsigma;
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > v2rho2;
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > v2rhosigma;
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > v2sigma2;
        int NGPU;
};
