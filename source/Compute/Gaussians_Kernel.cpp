#include "Gaussians_Kernel.hpp"

//#include "Teuchos_TimeMonitor.hpp"
#include "../Utility/Time_Measure.hpp"
#include "../Utility/Verbose.hpp"
#include "../Utility/String_Util.hpp"

using std::abs;
using std::string;
using Teuchos::RCP;
using Teuchos::rcp;
using Teuchos::Array;
using Teuchos::SerialDenseMatrix;

Gaussians_Kernel::Gaussians_Kernel(
        RCP<const Basis> basis,
        std::vector<double> exponent, std::vector<double> coeff, int ngpus)
: Poisson_Solver::Poisson_Solver(basis, 0.0, 0.0, 0.0, 0, 0, false, ngpus){
//: Kernel_Integral::Kernel_Integral(basis, 0.0, 0.0, 0.0, 0, 0, false, ngpus){
    // t_f = sqrt(M_PI) to remove Sundholm asymptotic correction.
    if(exponent.size() != coeff.size()){
        Verbose::all() << "N-Gaussian kernel:: exponent and coefficient have different size of "
                       << exponent.size() << " and " << coeff.size() << std::endl;
        exit(EXIT_FAILURE);
    }
    this->t_total.resize(exponent.size());
    this->w_total = coeff;
    // Exploit Poisson solver that uses 1/r = 2/\sqrt(\pi) \int_0^\infty e^{-t^2r^2} dt
    for(int i = 0; i < this -> w_total.size(); ++i){
        this -> w_total[i] *= sqrt(M_PI)/2;
        this -> t_total[i] = sqrt(exponent[i]);
    }

    this -> type = "NGAU";
    internal_timer = rcp(new Time_Measure() );
    this -> t_sampling();// This hangs if called later.
}

int Gaussians_Kernel::compute(RCP<const Basis> basis, Array<RCP<State> >& states){
    RCP<Epetra_Vector> hartree_potential  = rcp(new Epetra_Vector(*basis->get_map() ) );
    if (*basis!=(*this->basis)){
        Verbose::all() << " basis in Gaussians_Kernel is different to given basis" << std::endl
                       << this->basis.get() << std::endl
                       << basis.get() << std::endl;
        exit(-1);
    }
    const int spin_size = states[states.size()-1]->density.size();
    double hartree_energy;

    //Verbose::single()<< "~~~~~~~~" <<std::endl;
    if (spin_size==0){
        Verbose::all() << "Gaussians_Kernel:: No density!!" << std::endl;
        exit(-1);
    }
    this -> compute(states[states.size()-1]->density, hartree_potential,hartree_energy);

    RCP<State> new_state = rcp(new State(*states[states.size()-1]) );
    new_state->local_potential.clear();
    new_state->local_potential.append(hartree_potential );

    new_state->total_energy = hartree_energy;

    states.append(new_state);
    return 0;
}


void Gaussians_Kernel::t_sampling(){
    Verbose::single(Verbose::Detail) << "NGAU VERSION OF t-sampling is called!!" << std::endl;
    int NumProc = Parallel_Manager::info().get_mpi_size();
    is_t_sampled = true;

    t_proc.clear(); i_proc.clear(); w_proc.clear();
    for(int alpha=0; alpha<t_total.size(); alpha++){
        int iproc = alpha % NumProc;
        if(Parallel_Manager::info().get_mpi_rank() == iproc){
            t_proc.push_back(t_total[alpha]);
            i_proc.push_back(alpha);
            w_proc.push_back(w_total[alpha]);
        }
    }

    this -> which_t_sampling_used = "NGAU";
    return;
}

std::string Gaussians_Kernel::get_info_string() const{
    std::string info = "Hartree potential computer (Gaussians_Kernel) info:\n";
    info += String_Util::to_string((long long)w_total.size()) + " points quadrature\n";
    info += "Number of GPUs: " + String_Util::to_string(ngpus) + "\n";
    for(int i = 0; i < t_total.size(); ++i){
        info += "Gaussian coefficients, exponent pair " + String_Util::to_string(i+1) + ": [" + String_Util::to_string(this -> w_total[i]*2/std::sqrt(M_PI)) + ", " + String_Util::to_string(t_total[i]*t_total[i]) + "]\n";
    }

    info += string("Asymtotic correction: ") + (asymtotic_correction? "on": "off") + "\n";
    return info;
}
