#include "CISD.hpp"
#include <string>
#include <cmath>

#include "Teuchos_SerialDenseMatrix.hpp"
#include "Teuchos_LAPACK.hpp"

#include "Full_matrix_CISD.hpp"
#include "../Utility/String_Util.hpp"
#include "Create_Compute.hpp"

using std::abs;
using std::string;
using std::endl;
using std::vector;
//using std::scientific;
using Teuchos::Array;
using Teuchos::rcp;
using Teuchos::RCP;
using Teuchos::ParameterList;

CISD::CISD(
        RCP<const Basis> basis,
        RCP<ParameterList> parameters,
        Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > eigenvector,
        Array< vector<double > > orbital_energies,
        RCP<Poisson_Solver> poisson_solver,
        RCP<Exchange_Correlation> exchange_correlation)
{
    this->basis = basis;
    this -> num_electrons = static_cast<int>(parameters->sublist("BasicInformation").get<double>("NumElectrons")/2);
    this->parameters = parameters;
    this->core_hamiltonian = core_hamiltonian;
    this->poisson_solver = poisson_solver;
    this->exchange_correlation = exchange_correlation;
    //    cal_two_centered_integ(eigenvector);
    this -> timer = rcp(new Time_Measure());
}

void CISD::initializer(){
    this->cisd_occupation = rcp(new CISD_Occupation(parameters, this -> orbital[0] -> NumVectors(), this -> num_electrons, this -> num_electrons));
    num_eigenvalues = parameters->sublist("CISD").get<int>("NumberOfEigenvalues");
    this -> num_blocks = parameters->sublist("CISD").get<int>("NumBlocks", num_eigenvalues);

    Verbose::set_numformat(Verbose::Occupation);
    Verbose::single(Verbose::Simple) << "================================================" <<endl;
    Verbose::single(Verbose::Simple) << "CISD_Occupation summary" << endl;
    Verbose::single(Verbose::Simple) << "Number of orbitals : " << cisd_occupation->get_num_orbitals() << endl;
    Verbose::single(Verbose::Simple) << "Number of occupied orbitals : " << cisd_occupation->get_num_occupied_orbitals() << endl;
    Verbose::single(Verbose::Simple) << "Number of single exciation : " << cisd_occupation->get_string_number(1) << endl;
    Verbose::single(Verbose::Simple) << "Number of double exciation : " << cisd_occupation->get_string_number(2) << endl;
    Verbose::single(Verbose::Simple) << "================================================" <<endl;
    total_num_string = cisd_occupation->get_string_number(0)*cisd_occupation->get_string_number(0)
        + cisd_occupation->get_string_number(1)*cisd_occupation->get_string_number(0)
        + cisd_occupation->get_string_number(0)*cisd_occupation->get_string_number(1)
        + cisd_occupation->get_string_number(1)*cisd_occupation->get_string_number(1)
        + cisd_occupation->get_string_number(2)*cisd_occupation->get_string_number(0)
        + cisd_occupation->get_string_number(0)*cisd_occupation->get_string_number(2);

    max_iteration = total_num_string/2;
    Epetra_Map CISD_map (total_num_string, 0, basis->get_map()->Comm());

    H0 = rcp(new Epetra_Vector(CISD_map,true));
    return;
}

int CISD::compute(RCP<const Basis> basis,Array<RCP<State> >& states){
    Verbose::single(Verbose::Detail) << "Start CISD!!!!" << endl;

    RCP<State> initial_state = states[states.size()-1];
    this->orbital = states[states.size()-1]->get_orbitals();
    initializer();
    RCP<State> final_state;

    if (core_hamiltonian == Teuchos::null){
        this->core_hamiltonian = Create_Compute::Create_Core_Hamiltonian(basis, states[states.size()-1]->get_atoms(), parameters);
    }
    auto CISD_param =  Teuchos::sublist(parameters,"CISD");
    if(poisson_solver == Teuchos::null){
        this->poisson_solver = Create_Compute::Create_Poisson(basis, CISD_param);
    }
//    if(exchange_correlation==null and parameters->sublist("CISD").isParameter("OrbitalsFrom") and parameters->sublist("CISD").isParameter("UseInitialGuess"))
    if(exchange_correlation == Teuchos::null and parameters->sublist("CISD").isParameter("HcoreByEigenvalues")){
        this->exchange_correlation = Create_Compute::Create_Exchange_Correlation(basis, CISD_param, poisson_solver);
    }
    if(parameters->sublist("CISD").isParameter("UseInitialGuess")){
        Array<string> string_orbital_energies;
        Array< vector<double > > orbital_energies;
        Verbose::single(Verbose::Simple) << "Reading orbital information from cube file." << endl;
        //Cube* cube = new Cube(basis,parameters, states[states.size()-1]->get_atoms());

        if(parameters->sublist("CISD").get<int>("NormCheck", 0) == 1){
            Teuchos::Array< Teuchos::RCP< Epetra_MultiVector> > tmp_orbital;
            for(int i=0; i<orbital.size(); i++){
                tmp_orbital.append(Teuchos::rcp(new Epetra_MultiVector(*orbital[i])));
            }
            Verbose::single(Verbose::Simple) << "Orbital Norm" << endl;
            for(int i=0; i<orbital.size(); i++) {
                for(int j=0; j<tmp_orbital[i]->NumVectors()-1; j++){
                    for(int k=0; k<tmp_orbital[i]->NumVectors()-1; k++){
                        double overlap = 0;
                        tmp_orbital[i]->operator()(j)->Dot(*tmp_orbital[i]->operator()(k),&overlap);
                        Verbose::single(Verbose::Simple) << overlap << " ";
                    }
                    Verbose::single(Verbose::Simple) << endl;
                }
            }
            for(int i=0; i<orbital.size(); i++) {
                tmp_orbital[i] = Teuchos::null;
            }
        }
        if(parameters->sublist("CISD").isParameter("InputEigenvalues")){
            string_orbital_energies = parameters->sublist("CISD").get<Array <string> >("InputEigenvalues");
            for(int i=0; i<string_orbital_energies.size(); i++){
                orbital_energies.push_back(vector<double > ());
            }
            for(int i=0; i<string_orbital_energies.size(); i++){
                double tmp;
                tmp = String_Util::stod(string_orbital_energies[i]);
                orbital_energies[0].push_back(tmp);
            }
        }
        cisd_occupation->cal_two_centered_integ(orbital, poisson_solver,basis);
        cisd_occupation->cal_CI_Hcore(orbital, orbital_energies,basis, parameters->sublist("CISD").get<int>("OrbitalsFrom", 1),states ,exchange_correlation, poisson_solver,core_hamiltonian );
        cisd_occupation->cal_CI_modified_Hcore(); //added 2019/01/07 by jaechang
    } else{
        this->orbital = initial_state->get_orbitals();
        cisd_occupation->cal_two_centered_integ(orbital, poisson_solver,basis);
        cisd_occupation->cal_CI_Hcore(orbital, initial_state->get_orbital_energies(),basis, parameters->sublist("CISD").get<int>("OrbitalsFrom", 1),states ,exchange_correlation, poisson_solver, core_hamiltonian);
        cisd_occupation->cal_CI_modified_Hcore(); //added 2019/01/07 by jaechang
    }

    poisson_solver = Teuchos::null;
    core_hamiltonian = Teuchos::null;
    Verbose::single(Verbose::Detail)<< "cal_H0" <<endl;
    cisd_occupation->cal_H0(H0);
    Verbose::single(Verbose::Detail) << "start cal_total_H" << endl;

    this -> timer -> start("CISD::diagonalize");
    vector<double> CI_energy;
    vector<double> Davidson_corrected_CI_energy;
    RCP<Full_matrix_CISD> full_matrix_cisd;
    parameters->sublist("CISD").get<int>("Method", 1);
    if(parameters->sublist("CISD").get<int>("Method")==0){
        Verbose::single(Verbose::Detail) << "start davidson" << endl;
        CI_energy = Davidson();
    } else if(parameters->sublist("CISD").get<int>("Method")==1){
        full_matrix_cisd = rcp(new Full_matrix_CISD(basis, parameters, cisd_occupation, num_eigenvalues));
        CI_energy = full_matrix_cisd->compute(H0);
    }

    Verbose::set_numformat(Verbose::Energy);
    Verbose::single(Verbose::Simple) << "============================" << endl;
    if(parameters->sublist("CISD").isParameter("UseInitialGuess")){
        for(int i=0; i<num_eigenvalues;i++){
            Verbose::single(Verbose::Simple) << "CI_energy of " << i << " =                "  << CI_energy[i] << endl;
        }
    } else{
        for(int i=0; i<num_eigenvalues;i++){
            Verbose::single(Verbose::Simple) << "CI_energy of " << i << " = "  << CI_energy[i] + initial_state->nuclear_nuclear_repulsion << endl;
        }
    }

    Verbose::single(Verbose::Simple) << "Corrected energy of ground state  = "  << CI_energy[0] - H0->operator[](0) << endl;

    if(parameters->sublist("CISD").isParameter("DavidsonCorrection")){
        Davidson_corrected_CI_energy = Davidson_correction(eigenvectors, CI_energy);
        if(parameters->sublist("CISD").isParameter("UseInitialGuess")){
            for(int i=0; i<num_eigenvalues;i++){
                Verbose::single(Verbose::Simple) << "Davidson_corrected_CI_energy of " << i << " =                "  <<Davidson_corrected_CI_energy[i] << endl;
            }
        }else{
            for(int i=0; i<num_eigenvalues;i++){
                Verbose::single(Verbose::Simple) << "Davidson_corrected_CI_energy of " << i << " = "  << Davidson_corrected_CI_energy[i] + initial_state->nuclear_nuclear_repulsion << endl;
            }
        }
    }

    this -> timer -> end("CISD::diagonalize");
    Verbose::set_numformat(Verbose::Time);
    Verbose::single(Verbose::Simple) << "Diagonalize time: " << this -> timer -> get_elapsed_time("CISD::diagonalize", -1) << " s" << endl;
    Verbose::single(Verbose::Simple) << "============================" << endl;
    Epetra_Map CISD_map (total_num_string, 0, basis->get_map()->Comm());

    return 0;
}

std::vector<double> CISD::Davidson(){
    int iteration = 0;
    CI_energy = 0.0;
    double norm = 0;
    Verbose::single(Verbose::Detail) << "Start Davidson1" << endl;

    Verbose::single(Verbose::Detail) << num_blocks << endl;
    Verbose::single(Verbose::Detail) << iteration << endl;
    Verbose::single(Verbose::Detail) << max_iteration << endl;
    int iteration_time = 0;
    vector<double> eigenvalues(num_eigenvalues);
    max_iteration = 10000;

    for(int iteration = num_blocks; iteration < max_iteration; iteration = iteration + num_blocks) {
        if(iteration==num_blocks){
            Epetra_Map CISD_map (total_num_string, 0, basis->get_map()->Comm());
            for(int i=0; i<num_blocks; i++) {
                CI_coefficient.append(rcp(new Epetra_Vector(CISD_map,true)));
            }
            Teuchos::RCP<Epetra_Vector> temp10 = Teuchos::rcp(new Epetra_Vector(CI_coefficient[0]->Map()));
            if(CI_coefficient[0]->Map().LID(num_blocks)>=0)
                temp10->operator[](CI_coefficient[0]->Map().LID(num_blocks)) = 1.0;
            Gram_Schmidt(temp10);
            temp10 = Teuchos::null;
            for(int i = 0; i<num_blocks; i++){
                if(CISD_map.LID(i)>=0){
                    CI_coefficient[i]->operator[](CISD_map.LID(i)) = 1.0;
                }
            }
        }

        if(iteration_time>20 and norm>0.01){
            num_blocks +=num_eigenvalues;
            iteration = num_blocks;
            CI_coefficient.clear();
            Epetra_Map CISD_map (total_num_string, 0, basis->get_map()->Comm());
            for(int i=0; i<num_blocks; i++){
                CI_coefficient.append(rcp(new Epetra_Vector(CISD_map,true)));
            }
            Teuchos::RCP<Epetra_Vector> temp10 = Teuchos::rcp(new Epetra_Vector(CI_coefficient[0]->Map()));
            if(CI_coefficient[0]->Map().LID(num_blocks)>=0){
                temp10->operator[](CI_coefficient[0]->Map().LID(num_blocks)) = 1.0;
            }
            Gram_Schmidt(temp10);
            temp10 = Teuchos::null;
            for(int i = 0; i<num_blocks; i++){
                if(CISD_map.LID(i)>=0)
                    CI_coefficient[i]->operator[](CISD_map.LID(i)) = 1.0;
            }
            sigma.clear();
            iteration_time = 0;
        }

        int* MyGlobalElements = CI_coefficient[0]->Map().MyGlobalElements();

        Teuchos::RCP<Epetra_Vector> temp1 = Teuchos::rcp(new Epetra_Vector(CI_coefficient[0]->Map()));
        Teuchos::RCP<Epetra_Vector> temp2 = Teuchos::rcp(new Epetra_Vector(CI_coefficient[0]->Map()));
        Teuchos::RCP<Epetra_Vector> temp3 = Teuchos::rcp(new Epetra_Vector(CI_coefficient[0]->Map()));
        Teuchos::RCP<Epetra_Vector> temp4 = Teuchos::rcp(new Epetra_Vector(CI_coefficient[0]->Map()));
        Teuchos::RCP<Epetra_Vector> temp5 = Teuchos::rcp(new Epetra_Vector(CI_coefficient[0]->Map()));
        temp3->PutScalar(1.0);

        Verbose::single(Verbose::Simple) << "===============================" << endl;
        Verbose::single(Verbose::Simple) << " Number of Iteration  " << iteration_time << endl;
        Verbose::single(Verbose::Simple) << " Sigma size   " << sigma.size() << endl;
        Verbose::single(Verbose::Simple) << " CI_coefficient size   " << CI_coefficient.size() << endl;
        Verbose::single(Verbose::Simple) << "===============================" << endl;

        ++iteration_time;
        Verbose::single(Verbose::Detail) << "start cal sigma" << endl;
        if(iteration == num_blocks){
            Teuchos::Array< RCP< Epetra_Vector> > temp_CI_coefficient;
            for(int i=0; i<num_blocks+1; i++)
                temp_CI_coefficient.append(rcp(new Epetra_Vector(*CI_coefficient[i])));
            Teuchos::Array< RCP< Epetra_Vector> > temp_sigma = cisd_occupation->cal_sigma(temp_CI_coefficient);
            for(int i=0; i<num_blocks+1; i++)
                sigma.append(rcp(new Epetra_Vector(*temp_sigma[i])));
            temp_CI_coefficient.clear();
            temp_sigma.clear();
        } else {
            Teuchos::Array< RCP< Epetra_Vector> > temp_CI_coefficient;
            for(int i=iteration-num_blocks+1; i<iteration+1; i++){
                temp_CI_coefficient.append(rcp(new Epetra_Vector(*CI_coefficient[i])));
            }
            Teuchos::Array< RCP< Epetra_Vector> > temp_sigma = cisd_occupation->cal_sigma(temp_CI_coefficient);
            for(int i=0; i<num_blocks; i++){
                sigma.append(rcp(new Epetra_Vector(*temp_sigma[i])));
            }
            temp_CI_coefficient.clear();
            temp_sigma.clear();
        }

        int T_matrix_dim = CI_coefficient.size();
//        Verbose::single()<< "Start Davidson4" << endl;
        Verbose::single(Verbose::Detail) << "start make T matrix" << endl;
        Teuchos::RCP< Teuchos::SerialDenseMatrix<int,double> > Tmatrix = Teuchos::rcp(new Teuchos::SerialDenseMatrix<int, double> (T_matrix_dim, T_matrix_dim));
        double temp = 0;
        double ttemp = 0;
//        Verbose::single() << "Start make T matrix" << endl;
        for(int i = 0; i<T_matrix_dim; i++){
            for(int j = 0; j<T_matrix_dim; j++){
                double temp = 0.0;
                CI_coefficient[i]->Dot(*sigma[j], &temp);
                Tmatrix->operator()(i,j) = temp;
            }
        }

        double** evec = new double* [T_matrix_dim];
        double* eval = new double[T_matrix_dim];

        for(int i=0; i<T_matrix_dim; i++){
            evec[i] = new double[T_matrix_dim];
        }

        Verbose::single(Verbose::Detail) << "start T matrix diagonalize" << endl;
        diagonalize(Tmatrix, eval, evec, T_matrix_dim);
        Tmatrix = Teuchos::null;

        Verbose::single(Verbose::Detail) << "start davidson procedure" << endl;
        Verbose::set_numformat(Verbose::Energy);
        for(int i=0; i<num_blocks; i++){
            Verbose::single(Verbose::Detail) << eval[i] << "   ";
        }
        Verbose::single(Verbose::Detail) << endl;

        int NumMyElements = CI_coefficient[0]->Map().NumMyElements();
        for(int i = 0; i<num_blocks; i++){
            for(int j = 0; j< NumMyElements; j++){
                temp = 0;
                ttemp = 0;

                for(int k = 0; k< CI_coefficient.size()-i; k++){
                    temp += sigma[k]->operator[](j)*evec[k][i];
                    ttemp += CI_coefficient[k]->operator[](j)*evec[k][i];
                }

                temp1->ReplaceMyValue(j,0,temp);
                temp2->ReplaceMyValue(j,0,ttemp);
            }
            temp3->PutScalar(1.0);
            eigenvectors.append(Teuchos::rcp(new Epetra_Vector(*temp2)));
            temp1->Update(-eval[i], *temp2, 1.0);
            temp3->Update(-1.0, *H0, eval[i]);
            double temp_norm = 0;
            temp3->Norm2(&temp_norm);
            //Verbose::single() << "Norm of temp3 : " << temp_norm << endl;

            temp4->Reciprocal(*temp3);
            //Verbose::single() << *temp4 << endl;
            temp_norm = 0;
            temp4->Norm2(&temp_norm);
            //Verbose::single() < "Norm of temp4 : " << temp_norm << endl;
            temp5->Multiply(1.0,*temp4, *temp1, 0.0 );
            temp_norm = 0;
            temp5->Norm2(&temp_norm);
            //Verbose::single() << "Norm of temp5 : " << temp_norm << endl;
            Gram_Schmidt(temp5);
            temp_norm = 0;
            temp5->Norm2(&temp_norm);
            //Verbose::single() << "Norm of new vector : " << temp_norm << endl;
        }

        norm = 0;
        for(int i=0; i<num_eigenvalues; i++){
            norm += sqrt((eval[i]-eigenvalues[i])*(eval[i]-eigenvalues[i]));
        }

        if(norm<parameters->sublist("CISD").get<double>("ConvergenceTolerance", 1e-6)){
            Verbose::single(Verbose::Simple) << "Converged" << endl;
            Verbose::single(Verbose::Simple) << "norm     " << norm << endl;
            Verbose::single(Verbose::Simple) << "eigenvalue   ";
            Verbose::set_numformat(Verbose::Energy);
            for(int i=0; i<3; i++){
                Verbose::single(Verbose::Simple) << eval[i] << "   ";
            }
            Verbose::single(Verbose::Simple)<< endl;
            print_eval(eigenvectors);

            break;
        } else{
            Verbose::single(Verbose::Simple) << "Unconverged" << endl;
            Verbose::single(Verbose::Simple) << "norm     " << norm << endl;
            for(int i=0; i<num_eigenvalues; i++){
                eigenvalues[i] = eval[i];
            }
            delete [] eval;
            for(int i=0; i<T_matrix_dim; i++){
                delete [] evec[i];
            }
            delete [] evec;
            temp1 = Teuchos::null;
            temp2 = Teuchos::null;
            temp3 = Teuchos::null;
            temp4 = Teuchos::null;
            temp5 = Teuchos::null;

            eigenvectors.clear();
        }
    }
    return eigenvalues;
}

void CISD::diagonalize(Teuchos::RCP<Teuchos::SerialDenseMatrix<int, double> > Dmatrix, double* eval, double** evec, int matrix_size){
    double* WORK = new double[5*matrix_size];
    int info;
    Teuchos::LAPACK<int, double> lapack;
    lapack.SYEV('V', 'U', matrix_size, Dmatrix->values(), Dmatrix->stride(), eval, WORK, 5*matrix_size, &info);
    if(info!=0){
        Verbose::all() << " Diagonalize error" << info <<endl;
        exit(EXIT_FAILURE);
    }

    for(int i=0; i<matrix_size; i++){
        for(int j=0; j<matrix_size; j++){
            evec[j][i] = Dmatrix->operator()(j,i);
        }
    }
    delete WORK;
    return;
}

void CISD::Gram_Schmidt(Teuchos::RCP<Epetra_Vector> new_vector){
    double norm = 0;
    new_vector->Norm2(&norm);
    if(abs(norm) < 1.0E-21){
        Verbose::single(Verbose::Simple) << "Reinitializing Gram_Schmidt vector to one vector." << endl;
        new_vector->PutScalar(1.0);
        new_vector->Norm2(&norm);
    }
    new_vector->Scale(1/norm);
    for(int i=0; i<CI_coefficient.size(); i++){
        double overlap = 0;
        new_vector->Dot(*CI_coefficient[i],&overlap);
        new_vector->Update(-overlap, *CI_coefficient[i],1.0) ;

    }
    new_vector->Norm2(&norm);
    new_vector->Scale(1/norm);
    if(norm < 1.0E-19){
        Verbose::single() << "strange2" << endl;
    }
    CI_coefficient.append(rcp(new Epetra_Vector(*new_vector)));
    return;
}

void CISD::print_eval(Teuchos::Array< Teuchos::RCP<Epetra_Vector > > eigenvectors){
    int* MyGlobalElements = CI_coefficient[0]->Map().MyGlobalElements();
    int NumMyElements = CI_coefficient[0]->Map().NumMyElements();

    for(int j=0; j<eigenvectors.size(); j++){
        double* tmp_combine_eigenvectors = new double[total_num_string];
        double* combine_eigenvectors = new double[total_num_string];
        memset(combine_eigenvectors, 0.0, sizeof(double)*total_num_string);
        memset(tmp_combine_eigenvectors, 0.0, sizeof(double)*total_num_string);
        double norm = 0;

        eigenvectors[j]->Norm2(&norm);
        eigenvectors[j]->Scale(1/norm);
        for(int i=0; i<NumMyElements; i++){
            tmp_combine_eigenvectors[MyGlobalElements[i]] = eigenvectors[j]->operator[](i);
        }
        CI_coefficient[j]->Map().Comm().SumAll(tmp_combine_eigenvectors, combine_eigenvectors, total_num_string);
        Verbose::single(Verbose::Simple) << "eigenvector of " << j << endl;
        double coeff_sum = 0.0;
        for(int i=0; i< total_num_string; i++){
            coeff_sum +=abs(combine_eigenvectors[i])*abs(combine_eigenvectors[i]);
            if(abs(combine_eigenvectors[i]) > 0.00001){
                Verbose::single(Verbose::Simple) << i << "   " << combine_eigenvectors[i] <<" " ;
                cisd_occupation->print_configuration(i);
            }
            Verbose::single(Verbose::Simple) << "coefficient sum : " << coeff_sum << endl;
        }
        Verbose::single(Verbose::Simple) << endl;
        delete [] combine_eigenvectors;
        delete [] tmp_combine_eigenvectors;
    }
}

std::vector<double> CISD::Davidson_correction(Teuchos::Array< Teuchos::RCP<Epetra_Vector > > eigenvectors, std::vector<double> CI_energy){
    vector<double> davidson_correction_energy(eigenvectors.size());
    int* MyGlobalElements = eigenvectors[0]->Map().MyGlobalElements();
    int NumMyElements = eigenvectors[0]->Map().NumMyElements();

    double* combine_H0 = new double[total_num_string]();
    double* temp = new double[total_num_string]();
    for(int j=0; j<NumMyElements; j++){
        temp[MyGlobalElements[j]] = H0->operator[](j);
    }
    H0->Map().Comm().SumAll(temp, combine_H0, total_num_string);

    for(int i=0; i<eigenvectors.size(); i++){
        //find the largest coefficient
        int largest_coefficient = 0;
        double* tmp_combine_eigenvectors = new double[total_num_string]();
        double* combine_eigenvectors = new double[total_num_string]();

        double norm = 0;
        eigenvectors[i]->Norm2(&norm);
        eigenvectors[i]->Scale(1/norm);
        for(int k=0; k<NumMyElements; k++){
            tmp_combine_eigenvectors[MyGlobalElements[k]] = eigenvectors[i]->operator[](k);
        }
        eigenvectors[i]->Map().Comm().SumAll(tmp_combine_eigenvectors, combine_eigenvectors, total_num_string);
        for(int j=0; j<total_num_string; j++){
            if(abs(combine_eigenvectors[largest_coefficient]) < abs(combine_eigenvectors[j])){
                largest_coefficient = j;
            }
        }
        int pair_coefficient = cisd_occupation->find_pair_spin(largest_coefficient);
        double energy1 = 0.0;
        if(largest_coefficient==pair_coefficient){
            energy1 += combine_H0[largest_coefficient];
        }else{
            energy1 += 2*cisd_occupation->cal_sorted_CI_element(largest_coefficient, pair_coefficient );
            energy1 += combine_H0[largest_coefficient];
            energy1 = energy1/sqrt(2.0);
        }

        double new_corr_energy = (energy1-CI_energy[i])*(1-combine_eigenvectors[largest_coefficient]*combine_eigenvectors[largest_coefficient])/combine_eigenvectors[largest_coefficient]/combine_eigenvectors[largest_coefficient];
        davidson_correction_energy[i] = energy1 - new_corr_energy;
        Verbose::single(Verbose::Detail) << "largest coeff : " << largest_coefficient << " " << combine_eigenvectors[largest_coefficient] << endl;
        Verbose::single(Verbose::Detail) << energy1 << endl;
        Verbose::single(Verbose::Detail) << new_corr_energy << endl;

        delete[] tmp_combine_eigenvectors;
        delete[] combine_eigenvectors;
    }
    delete[] combine_H0;
    delete[] temp;
    return davidson_correction_energy;
}

CISD::~CISD(){
    Verbose::set_numformat(Verbose::Time);
    this -> timer -> print(Verbose::single(Verbose::Simple));
}
