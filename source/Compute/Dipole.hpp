#pragma once
#include "Scf.hpp"
#include <string>
#include <stdexcept>
#include <EpetraExt_MatrixMatrix.h>

#include "../Utility/Parallel_Util.hpp"
#include "../State/State.hpp"
#include "../State/Scf_State.hpp"
#include "../Basis/Basis.hpp"
#include "../Utility/Time_Measure.hpp"

#define _ENERGY_WIDTH_ (10)

class Dipole{
    public:
    bool Compute_dipole(
        Teuchos::RCP<State> &state,
        Teuchos::RCP<const Basis> mesh
        );
};
