#pragma once
#include <string>
#include <map>

#include "Teuchos_RCP.hpp"
#include "Teuchos_Array.hpp"

#include "Epetra_Vector.h"
#include "Epetra_MultiVector.h"

#include "Compute.hpp"
#include "../Basis/Basis.hpp"
#include "../State/State.hpp"
#include "Poisson_Solver.hpp"
#include "../State/XC_State.hpp"

/**
 * @brief Abstract class for all exchange-correlation functionals.
 */
class Compute_XC: public Compute{
    public:
        /**
         * @brief Null constructor of Exchange_Correlation class
         */
        //Compute_XC();
        /**
         * @brief Constructor of Exchange_Correlation class
         */
        Compute_XC(
            Teuchos::RCP<const Basis> basis, ///< Basis data
            int xc_id ///< XC functional id
        );
        /**
         * @brief Calculate exchange-correlation energy and potential of given state.
         * @details Inherited from Compute class.
         * @return -10
         */
        virtual int compute(
            Teuchos::RCP<const Basis> basis, ///< Basis data
            Teuchos::Array<Teuchos::RCP<State> >& states///< Teuchos::Array of State object. Last entry is read and updated.
        );
        /**
         * @brief Calculate exchange-correlation energy and potential of given Scf_state.
         * @details compute function for the object of Scf_State class.
         * @return 0 (normal termination) The computed exchange-correlation energy is replaced to exchange_energy and correlation_energy of last Scf_State, and the exchange_potential and correlation_potential of last Scf_State is replaced with calcaulated one.
         */
        virtual int compute(
            Teuchos::RCP<XC_State>& xc_info///< Teuchos::Array of Scf_State object. Last entry is read and updated.
        );

        /**
         * @brief Calculate exchange-correlation energy
         * @details Note that this routine CAN compute KLI and HF exchange potential, since it have orbital informations in argument.
         * @return parameter exchange_energy, and correlation_energy will be changed to corresponding values.
         * @param density electron (spin) density
         * @param density_grad gradient of density electron (spin) density
         * @param occupations orbital occupation numbers of the given system
         * @param exchange_energy output, exchange energy of given density
         * @param correlation_energy output, correlation energy of given density
         * @param core_density core electron (spin) density, default: null Epetra_Vector
         * @param core_density_grad core electron (spin) density gradient components, default: null Epetra_MultiVector
         */
        virtual void compute_Exc(Teuchos::RCP<XC_State> &xc_info)=0;

        /**
         * @brief Calculate exchange-correlation potential
           @details Note that this routine CAN compute KLI and HF exchange potential, since it has orbital informations.
         * @return parameter exchange_potential, and correlation_potential will be changed to corresponding values.
         * @param density electron (spin) density
         * @param density_grad gradient of density electron (spin) density
         * @param occupations orbital occupation numbers of the given system
         * @param core_density core electron (spin) density
         * @param core_density_grad core electron (spin) density gradient components
         * @param exchange_potential exchange potential of given system
         * @param correlation_potential correlation potential of given system
         */
        virtual void compute_vxc(
            Teuchos::RCP<XC_State> &xc_info
        ) = 0;

        /**
         * @brief Get TDDFT kernel, fx and fc.
         * @details Get TDDFT kernel, fx and fc.
         * @param fx_input exchange kernel
         * @param fc_input correlation kernel
         **/
        virtual void compute_kernel(Teuchos::RCP<XC_State> &xc_info)=0;

        int get_functional_id();
        virtual int get_functional_kind()=0;
        virtual std::string get_info_string(bool verbose = true)=0;
        virtual Teuchos::RCP<Poisson_Solver> get_rsh_poisson_solver();
    protected:
        Teuchos::RCP<const Basis> basis; ///< Basis data
        /**
         * @brief Functional ID of Libxc + our custom ID.
         * @details
         * - -10 : no exchange potential(only energy)
         * - -11 : Slater potential
         * - -12 : OEP-EXX potential with KLI approximation
         * - -13 : OEP-EXX potential(not implemented)
         */
        int function_id;///< XC functional ID

        // LDA kernel
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > fxc; ///< LDA kernel, see <a href="http://www.tddft.org/programs/octopus/wiki/index.php/Libxc:manual">Here</a>.
        // end

        // GGA kernel
        Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > vsigma; ///<GGA kernel, see <a href="http://www.tddft.org/programs/octopus/wiki/index.php/Libxc:manual">Here</a>.
        Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > v2rho2;///<GGA kernel, see <a href="http://www.tddft.org/programs/octopus/wiki/index.php/Libxc:manual">Here</a>.
        Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > v2rhosigma;///<GGA kernel, see <a href="http://www.tddft.org/programs/octopus/wiki/index.php/Libxc:manual">Here</a>.
        Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > v2sigma2;///<GGA kernel, see <a href="http://www.tddft.org/programs/octopus/wiki/index.php/Libxc:manual">Here</a>.
        // end

        Teuchos::RCP<Poisson_Solver> poisson_solver = Teuchos::null; ///< Poisson_Solver for calculating two-centered integral.
};
