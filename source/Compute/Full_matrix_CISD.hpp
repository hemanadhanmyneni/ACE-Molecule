#pragma once
#include "../Utility/Value_Coef.hpp"
#include "CISD_Occupation.hpp"
#include "Epetra_MultiVector.h"
#include "Teuchos_ParameterList.hpp"
#include "Teuchos_Array.hpp"
#include "Teuchos_RCP.hpp"
#include "Poisson_Solver.hpp"
#include "Exchange_Correlation.hpp"
#include "Teuchos_LAPACK.hpp"
#include "Teuchos_SerialDenseMatrix.hpp"
#include "xc.h"
#include "../Core/Diagonalize/Pure_Diagonalize.hpp"
#include "../Core/Diagonalize/Create_Diagonalize.hpp"
#ifdef HAVE_MPI
#include "Epetra_MpiComm.h"
#include "mpi.h"
#else
#include "Epetra_SerialComm.h"
#endif

class Full_matrix_CISD {
    public:
        Full_matrix_CISD(
                Teuchos::RCP<const Basis> basis,
                Teuchos::RCP<Teuchos::ParameterList> parameters,
                Teuchos::RCP<CI_Occupation> cisd_occupation,
                int num_eigenvalues);
        std::vector<double> compute(Teuchos::RCP<Epetra_Vector> H0);
        Teuchos::RCP<Epetra_MultiVector> get_eigenvectors();

    protected:
        int total_num_string;
        Teuchos::RCP<Epetra_MultiVector> eigenvectors;
        Teuchos::RCP<Epetra_CrsMatrix> make_CI_matrix(Teuchos::RCP<Epetra_Vector> H0);
        Teuchos::RCP<CI_Occupation> cisd_occupation;
        Teuchos::RCP<const Basis> basis;
        Teuchos::RCP<Teuchos::ParameterList> parameters;
        int num_eigenvalues;
        Teuchos::RCP<Diagonalize> diagonalize;
        double get_eval_diff(int occupation);
        std::vector<double> orbital_energies;
        int type_distinguish(int occupation);
};
