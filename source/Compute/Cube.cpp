#include "Cube.hpp"
#include "../Utility/Parallel_Manager.hpp"
#include "../Utility/Parallel_Util.hpp"
#include "../Utility/Density/Density_From_Orbitals.hpp"
#include "../Utility/Read/Read_Output.hpp"
#include "../Utility/Value_Coef.hpp"

using Teuchos::Array;
using Teuchos::ParameterList;
using Teuchos::rcp;
using Teuchos::RCP;

Cube::Cube(Array< RCP<Occupation> > occupations, RCP<const Atoms> atoms, Array<std::string> cube_filenames, bool is_bohr){
    this->occupations = occupations;
    this->atoms = atoms;
    this->cube_filenames = cube_filenames;
    this->is_info = false;
    this -> is_bohr = is_bohr;
}

Cube::Cube(Array< RCP<Occupation> > occupations, RCP<const Atoms> atoms, Array<std::string> cube_filenames, std::string info_filename, std::string info_filetype, bool is_bohr){
    this->occupations = occupations;
    this->atoms = atoms;
    this->cube_filenames = cube_filenames;
    this->is_info = true;
    this->info_filename = info_filename;
    this->info_filetype = info_filetype;
    this -> is_bohr = is_bohr;
}

int Cube::compute(RCP<const Basis> basis,Array< RCP<State> >& states){
    // Read orbitals
    initialize_states(basis,states);

    auto state = states[states.size()-1];

    RCP<const Epetra_Map> map= basis->get_map();
    Array< RCP<Epetra_MultiVector> > orbitals;

    Verbose::single(Verbose::Normal) << "\n#------------------------------------------------------- Cube::compute"<<std::endl;
    for(int i_spin=0; i_spin<occupations.size(); i_spin++){
        state->density.append(rcp(new Epetra_Vector(*basis->get_map())));
        orbitals.append(rcp(new Epetra_MultiVector(*basis->get_map(), occupations[i_spin]->get_size())));
        int root = (i_spin > 0 and cube_filenames.size() > i_spin*occupations[i_spin]-> get_size())? i_spin*occupations[i_spin]->get_size(): 0;
        std::vector<std::string> files_to_read(cube_filenames.begin()+root, cube_filenames.begin()+root+occupations[i_spin]->get_size());
        basis -> parallel_read_cube(files_to_read, orbitals[i_spin], is_bohr);
        Value_Coef::Value_Coef(basis, orbitals[i_spin], true, false, orbitals[i_spin]);
    }

    Verbose::single(Verbose::Normal) << "=============== Norm check =====================" <<std::endl;
    for(int i_spin=0; i_spin<occupations.size(); i_spin++ ){
        for(int j =0; j< orbitals[i_spin]->NumVectors(); j++){
            double norm = 0.0;
            orbitals[i_spin]->operator()(j)->Norm2(&norm);
            Verbose::single(Verbose::Normal) << j+1 << "\t " << std::scientific << norm <<std::endl;
        }
    }
    Verbose::single(Verbose::Normal) << "================================================" <<std::endl;
    state->orbitals = orbitals;

    Density_From_Orbitals::compute_total_density(basis, occupations, state->orbitals, state->density);
    // end

    // Read eigenvalues
    if(is_info){
//        double octopus_external_energy = 0.0;
        if(info_filetype == "Octopus"){
            //Read::Octopus::read_octopus(info_filename, state->orbital_energies, octopus_external_energy);
            Read::Octopus::read_octopus(info_filename, state->orbital_energies);
            state -> total_energy = 0.0;
        }
        else if (info_filetype == "ACE"){
            //Read::ACE::read_ACE(info_filename, state->orbital_energies, octopus_external_energy);
            Read::ACE::read_ACE(info_filename, state->orbital_energies, state -> total_energy);
        }

        Verbose::single(Verbose::Normal) << "\n#------------------------------------------------------- Cube::compute"<<std::endl;
        Verbose::single(Verbose::Normal) << " Read orbital energies from \"info\"."<<std::endl;
        for(int i_spin=0; i_spin<occupations.size(); i_spin++){
            for(int i=0; i<occupations[i_spin]->get_size(); i++){
                Verbose::single(Verbose::Normal) << i+1 << ":\t" << std::real(state->orbital_energies[i_spin][i]) << "\n";
            }
        }
        Verbose::single(Verbose::Normal) << "#---------------------------------------------------------------------"<<std::endl;
    }
    // end
    return 0;
}
