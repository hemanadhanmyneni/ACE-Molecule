#include "Create_Compute_Interface.hpp"
#include <vector>
#include <string>
#include <stdexcept>

#include "Atomic_Density.hpp"
#include "Huckel.hpp"
#include "Grid_Cutting.hpp"
#include "Random.hpp"
#include "Cube.hpp"
#include "Cube_Density.hpp"
#include "Cube_Density_from_Orbitals.hpp"
#include "Initialize_Guess.hpp"

#include "Scf.hpp"
#include "Scf2.hpp"
#include "TDDFT.hpp"
#include "TDDFT_C.hpp"
#include "TDDFT_ABBA.hpp"
#include "TDDFT_TDA.hpp"

#ifdef ACE_MKL
#include "DDA.hpp"
#endif

#include "Force.hpp"
#include "Force_KB.hpp"

#ifdef USE_CI
#include "CIS.hpp"
#include "CISD.hpp"
#endif

#ifdef USE_PAW
#include "Paw_Guess.hpp"
#include "Force_Paw.hpp"
#endif

#include "../Io/Periodic_table.hpp"
#include "../Core/Occupation/Create_Occupation.hpp"
#include "../Utility/String_Util.hpp"
#include <dirent.h>

using std::string;
using std::vector;
using Teuchos::rcp;
using Teuchos::RCP;
using Teuchos::Array;
using Teuchos::ParameterList;
using Teuchos::sublist;

RCP<Guess> Create_Compute_Interface::Create_Guess(RCP<const Atoms> atoms, RCP<ParameterList> parameters, RCP<const Basis> basis/* == Teuchos::null*/){
    int guess_type = parameters -> sublist("Guess").get<int>("InitialGuess", 6);

    //exit(-1);
    Array<string> initial_filenames;
    if( guess_type == 1 or guess_type == 3 or guess_type == 4 or guess_type==7 or guess_type == 8){
        if(!parameters->sublist("Guess").isParameter("InitialFilenames")){
            if( (guess_type == 1 or guess_type == 4) ){
                if(parameters -> sublist("Guess").isParameter("InitialFilePath")
                        and parameters->sublist("Guess").isParameter("InitialFileSuffix")){
                    string path = parameters -> sublist("Guess").get<string>("InitialFilePath");
                    if(path.size() > 0){
                        path += "/";
                    }
                    string suffix = parameters->sublist("Guess").get<string>("InitialFileSuffix");
                    vector<int> atomic_numbers = atoms -> get_atom_types();
                    vector<string> atomic_symbols;
                    for(int ia = 0; ia < atomic_numbers.size(); ++ia){
                        string atom_symbol = Periodic_atom::periodic_table[atomic_numbers[ia]-1];
                        transform(atom_symbol.begin()+1, atom_symbol.end(), atom_symbol.begin()+1, ::tolower);
                        initial_filenames.push_back( path + atom_symbol + suffix );
                    }
                    parameters -> sublist("Guess").set("InitialFilenames", initial_filenames);
                    Verbose::single(Verbose::Normal) << "InitialFilenames inferred are:" << std::endl;
                    Verbose::single(Verbose::Normal) << parameters -> sublist("Guess").get< Array<string> >("InitialFilenames") << std::endl;
                } else if(parameters -> sublist("BasicInformation").sublist("Pseudopotential").isParameter("PSFilenames")){
                    initial_filenames = parameters -> sublist("Guess").get< Array<string> >("InitialFilenames",
                        parameters -> sublist("BasicInformation").sublist("Pseudopotential").get< Array<string> >("PSFilenames"));
                } else {
                    Verbose::single() << *parameters << std::endl;
                    Verbose::all()<< "Create_Guess::initialize_parameters - CANNOT find InitialFilenames." <<"\n" ;
                    exit(EXIT_FAILURE);
                }
            }
            else if( (guess_type == 3 or guess_type == 7 or guess_type == 8) and
                       parameters -> sublist("Guess").isParameter("InitialFilePath") ){
                DIR *dpdf;
                struct dirent *epdf;

                dpdf = opendir(parameters -> sublist("Guess").get<string>("InitialFilePath").c_str());
                if (dpdf != NULL){
                    while (epdf = readdir(dpdf)){
                        if( string(epdf->d_name) == "." or string(epdf->d_name) == ".." ) continue;
                        initial_filenames.push_back( parameters -> sublist("Guess").get<string>("InitialFilePath")+"/"+epdf->d_name );
                    }
                } else {
                    Verbose::all() << "Failed to open InitialFilePath " << parameters -> sublist("Guess").get<string>("InitialFilePath") << std::endl;
                    exit(EXIT_FAILURE);
                }
                closedir(dpdf);
                std::sort(initial_filenames.begin(), initial_filenames.end());
                if(parameters -> sublist("Guess").isParameter("NumberOfEigenvalues")){
                    int num_eigval = parameters -> sublist("Guess").get<int>("NumberOfEigenvalues");//////////////////////////////////////////////////jaewook
                    int polarize = 1;
                    if(parameters->sublist("BasicInformation").isParameter("Polarize")){
                        polarize += parameters->sublist("BasicInformation").get<int>("Polarize");
                    }
                    if(num_eigval > 0){
                        initial_filenames.resize(num_eigval*polarize);
                    }
                }
                parameters -> sublist("Guess").set("InitialFilenames", initial_filenames);
                Verbose::single(Verbose::Normal) << "InitialFilenames inferred are:" << std::endl;
                Verbose::single(Verbose::Normal) << parameters -> sublist("Guess").get< Array<string> >("InitialFilenames") << std::endl;
            }
            else {
                Verbose::all()<< "Create_Guess::initialize_parameters - CANNOT find InitialFilenames." <<"\n";
                exit(EXIT_FAILURE);
            }
        }
        else{
            initial_filenames = parameters->sublist("Guess").get<Array<string> >("InitialFilenames");
        }
    }

/////////////////////////////////////////////////////////////////////////////////////// initialize  occ_param
    Teuchos::RCP<Teuchos::ParameterList> occ_param;
//    if(parameters->isSublist("Scf")){
//        if(parameters->sublist("Scf").isSublist("Occupation")){
//            occ_param = sublist(sublist(parameters,"Scf"),"Occupation");
//        }
//    }
    // read parameter from input
    if(parameters->isSublist("Guess")){
        if(parameters->sublist("Guess").isSublist("Occupation")){
            occ_param = sublist(sublist(parameters,"Guess"),"Occupation");
        }
    }
    else if (parameters->isSublist("SCF")){
        if (parameters->sublist("SCF").isSublist("Occupation")){
            occ_param = sublist(sublist(parameters,"SCF"),"Occupation");
        }
    }

    //
    if(occ_param == Teuchos::null){
        occ_param = rcp( new ParameterList () );
        occ_param->set("OccupationMethod","ZeroTemp");
    }

    occ_param->set("NumElectrons",parameters->sublist("BasicInformation").get<double>("NumElectrons") );
    occ_param->set("SpinMultiplicity",parameters->sublist("BasicInformation").get<double>("SpinMultiplicity") );
    if(parameters -> sublist("BasicInformation").isParameter("Polarize")){
        occ_param->set("Polarize",parameters->sublist("BasicInformation").get<int>("Polarize") );
    }

    // set OccupationSize
    if(parameters -> sublist("Guess").isParameter("NumberOfEigenvalues")){
        occ_param -> set("OccupationSize", parameters->sublist("Guess").get<int>("NumberOfEigenvalues"));
    }
    if(guess_type == 3){
        int polarize = 1;
        if(parameters->sublist("BasicInformation").isParameter("Polarize")){
            polarize += parameters->sublist("BasicInformation").get<int>("Polarize");
        }
        occ_param -> set("OccupationSize", static_cast<int>(initial_filenames.size()/polarize));
    }

    //exit(-1);
    auto occupations = Create_Occupation::Create_Occupation(occ_param );
    if( guess_type == 0 ){
        Verbose::single(Verbose::Normal) << "Core Hamiltonian Guess was used." << std::endl;
        Verbose::single(Verbose::Detail) << parameters->sublist("BasicInformation").get<int> ("StoreCoreHamiltonian", 0)<< std::endl ;
        Verbose::single(Verbose::Detail) << *parameters <<std::endl;
        return rcp( new Core_Hamiltonian(basis, occupations, atoms,
                        parameters->sublist("BasicInformation").get<int> ("StoreCoreHamiltonian", 0)==0? false:true,
                        parameters) );
    }
    else if( guess_type == 1 ){
        Verbose::single(Verbose::Normal) << "Atomic Density Guess is used.\n" ;
        return rcp( new Atomic_Density( occupations, atoms, initial_filenames) );
    }
    else if( guess_type == 2 ){
        Verbose::single(Verbose::Normal) << "Huckel Guess is used.\n" ;
        string eht_filename = parameters->sublist("Guess").get<string>("EHTFilename");
        return rcp( new Huckel( occupations, atoms, eht_filename) );
    }
    else if( guess_type == 3 ){
        Verbose::single(Verbose::Normal) << "Cube Guess is used.\n" ;
        bool is_bohr = true;
        if( parameters -> sublist("Guess").get<string>("CubeUnit", "Bohr") == "Angstrom" ){
            is_bohr = false;
        }
        if(parameters->sublist("Guess").isParameter("Info")){
            string info_filename = parameters->sublist("Guess").get<string>("Info");
            string info_filetype = parameters->sublist("Guess").get<string>("InfoType","ACE");
            return rcp(new Cube(occupations, atoms, initial_filenames, info_filename, info_filetype, is_bohr));
        }
        else{
            if(parameters->sublist("BasicInformation").get<string>("Mode") == "TDDFT"){
                Verbose::all() << "Cube::initialize_parameters - For direct TDDFT calculation, \"Guess::Info\" file must exist.\n";
                exit(EXIT_FAILURE);
            }
            return rcp(new Cube(occupations, atoms, initial_filenames, is_bohr) );
        }
    }
#ifdef USE_PAW
    else if( guess_type == 4 ){
        return rcp( new Paw_Guess( occupations, atoms, initial_filenames) );
    //} else if( guess_type == 5 ){
        //return rcp( new Restart() );
    }
#endif
    else if( guess_type == 6 ){
        Verbose::single(Verbose::Normal) << "Grid Cutting Guess is used.\n" ;
        return rcp( new Grid_Cutting( occupations, parameters, atoms) );
    }
    else if(guess_type == 7 ){
        bool is_bohr = true;
        if(parameters -> sublist("Guess").isParameter("CubeUnit")){
            if( parameters -> sublist("Guess").get<string>("CubeUnit") == "angstrom" ){
                is_bohr = false;
            }
        }
        Verbose::single(Verbose::Normal) << "Cube_Density is used. " << initial_filenames.size() << std::endl;
        return rcp( new Cube_Density(occupations, atoms, initial_filenames, is_bohr));
    }
    else if(guess_type == 8){
        RCP<ParameterList> occ_param2 = sublist(sublist(parameters, "Guess"), "InputOccupation");
        auto input_occupations = Create_Occupation::Create_Occupation(occ_param2);

        bool is_bohr = true;
        if(parameters -> sublist("Guess").isParameter("CubeUnit")){
            if( parameters -> sublist("Guess").get<string>("CubeUnit") == "angstrom" ){
                is_bohr = false;
            }
        }
        Verbose::single(Verbose::Normal) << "Cube_Density_from_Orbitals is used. " << initial_filenames.size() <<std::endl;
        return rcp( new Cube_Density_from_Orbitals(occupations, atoms, input_occupations, initial_filenames, is_bohr));
    }
    else if( guess_type == -1){
        Verbose::single(Verbose::Normal) << "Random guess is used.\n" ;
        return rcp(new Random(occupations, atoms));
    }
    else if( guess_type == -99){
        Verbose::single(Verbose::Normal) << "Initializing guess. " << std::endl;
        return rcp(new Initialize_Guess(occupations, atoms));
    }
    Verbose::all() << "Non-supported InitialGuess type " << guess_type << std::endl;
    throw std::invalid_argument("Non-supported InitialGuess type " + String_Util::to_string(guess_type));
    return Teuchos::null;
}

/*
RCP<Scf> Create_Compute_Interface::Create_Scf( RCP<const Basis> basis, RCP<ParameterList> parameters, RCP<Core_Hamiltonian> core_hamiltonian* = Teuchos::null* ){
    return rcp( new Scf(basis, parameters, core_hamiltonian) );
}
*/

// RCP<TDDFT>? RCP<Scf>? RCP<DFT>?
// Probably RCP<DFT> would be good.
// Need work, more.
RCP<Compute> Create_Compute_Interface::Create_DFT( RCP<const Basis> basis, RCP<ParameterList> parameters, RCP<Core_Hamiltonian> core_hamiltonian/* = Teuchos::null*/){
    if(parameters -> name() == "Scf"){
        // This set parameter default value. Displayed as default when printed.
        parameters -> sublist("Scf").get<int>("IterateMaxCycle", 128);
        parameters -> sublist("Scf").get<int>("StepSizeForReduceDiagonalizeTol", 5);
        parameters -> sublist("Scf").get<int>("ComputeInitialEnergy", 0);
        //parameters -> sublist("Scf").get<int>("Making_Hamiltonian_Matrix", 1); //shchoi
        parameters -> sublist("Scf").get<int>("DiagonalizeShouldNotBeConverged", 1);
        if(parameters -> sublist("Scf").get<string>("ConvergenceType", "Density") == "Energy"){// This initialization is overrides Create_Convergence.
            parameters -> sublist("Scf").get<int>("EnergyDecomposition", 1);
        } else {
            parameters -> sublist("Scf").get<int>("EnergyDecomposition", 0);
        }
        parameters -> sublist("Scf").get<int>("MakingHamiltonianMatrix", 1);
        if(parameters -> sublist("Scf").get<int>("MicroIter", 0) != 0){
            return rcp(new Scf2(basis, parameters, core_hamiltonian) );
        }
        else{
            return rcp( new Scf(basis, parameters, core_hamiltonian) );
        }
    }
    else if(parameters -> name() == "TDDFT"){
        // Initialization
        parameters->sublist("TDDFT").get<string>("TheoryLevel", "Casida");
        parameters->sublist("TDDFT").get<string>("SortOrbital", "Order");
        parameters->sublist("TDDFT").sublist("Diagonalize").get<string>("Solver","Direct");
        if(parameters -> sublist("TDDFT").get<int>("RestrictedTriplet", 0) != 0){
            Verbose::all() << "RestrictedTriplet is not supported!" << std::endl;
            throw std::invalid_argument("RestrictedTriplet option is not supported");
        }
        if(!parameters -> sublist("TDDFT").isSublist("ExchangeCorrelation")){
            parameters -> sublist("TDDFT").sublist("ExchangeCorrelation") = parameters -> sublist("Scf").sublist("ExchangeCorrelation");
        }

        RCP<Poisson_Solver> poisson_solver = Create_Compute::Create_Poisson(basis, Teuchos::sublist(parameters, "TDDFT"));
        Verbose::single(Verbose::Normal) << "TDDFT exchange correlation:" << std::endl;
        RCP<Exchange_Correlation> exchange_correlation = Create_Compute::Create_Exchange_Correlation(basis, Teuchos::sublist(parameters, "TDDFT"), poisson_solver);

        if(exchange_correlation -> get_EXX_portion() > 0.0){
            parameters -> sublist("TDDFT").get<string>("ExchangeKernel", "HF-EXX");
        } else {
            parameters -> sublist("TDDFT").get<string>("ExchangeKernel", "");
        }

        // Backward compatability
        if(parameters -> sublist("TDDFT").get<string>("ExchangeKernel") == "KS-CI"){
            parameters -> sublist("TDDFT").get<string>("ExchangeKernel", "HF-EXX");
        }
        // Backward compatability end
        if(parameters -> sublist("TDDFT").get<string>("ExchangeKernel") == "HF-EXX"){
            // This is 1 for backward compatability. Will be changed to 2.
            int delta_option = parameters -> sublist("TDDFT").get<int>("DeltaCorrection", 1);
            if(delta_option == 3){
                Verbose::all() << "DeltaCorrection == 3 is not implemented!" << std::endl;
                throw std::invalid_argument("DeltaCorrection == 3 is not implemented!");
            }
        }
        // Initialization end
        string _class;
        if(parameters -> sublist("TDDFT").isParameter("_TDClass")){
            _class = parameters -> sublist("TDDFT").get<string>("_TDClass");
        }
        else {
            if(parameters -> sublist("TDDFT").get<string>("TheoryLevel") == "TammDancoff"){
                _class = "TDA";
            }
            else if(parameters -> sublist("TDDFT").get<string>("TheoryLevel") == "Casida"){
                string xc = parameters -> sublist("TDDFT").get<string>("ExchangeKernel");
                if(xc == "HF" or xc == "HF-EXX"){
                    _class = "ABBA";
                }
                else {
                    _class = "C";
                }
            }
            else {
                throw std::invalid_argument("Wrong TDDFT TheoryLevel!");
            }
        }
        if(_class == "TDA"){
            return rcp(new TDDFT_TDA(basis, exchange_correlation, poisson_solver, parameters));
        }
        else if(_class == "ABBA"){
            return rcp(new TDDFT_ABBA(basis, exchange_correlation, poisson_solver, parameters));
        }
        else if(_class == "C"){
            return rcp(new TDDFT_C(basis, exchange_correlation, poisson_solver, parameters));
        }
        else {
            throw std::invalid_argument("Cannot interprete TDDFT TheoryLevel!");
        }
        return rcp( new TDDFT_ABBA(basis, exchange_correlation, poisson_solver, parameters) );
    }
    Verbose::all() << "Non-supported DFT type: should be selected among Scf, and TDDFT" << std::endl;
    throw std::invalid_argument("Non-supported DFT type.");
    return Teuchos::null;
}

RCP<Compute> Create_Compute_Interface::Create_Force( RCP<const Basis> basis, RCP<const Atoms> atoms, RCP<ParameterList> parameters){
    RCP<Core_Hamiltonian> core_hamiltonian = Create_Compute::Create_Core_Hamiltonian(basis, atoms, parameters);
    RCP<Nuclear_Potential> np = core_hamiltonian->get_core_hamiltonian_matrix()->nuclear_potential;

    // What should be parameters' name?
    //if(parameters -> name() == "BasicInformation"){ Does not matter since BasicInformation sublist is always included for ANY parameters segments.
    auto external_field = Create_External_Field::Create_External_Field(basis, Teuchos::sublist(parameters,"BasicInformation") );
    if (parameters -> sublist("BasicInformation").sublist("Pseudopotential").get<int>("Pseudopotential") == 1){
        return rcp( new Force_KB(basis, atoms, np,  external_field, parameters) );
    }
#ifdef USE_PAW
    else if (parameters -> sublist("BasicInformation").sublist("Pseudopotential").get<int>("Pseudopotential") == 3){
        return rcp( new Force_Paw(basis, atoms, np, external_field, parameters) );
    }
#endif

    Verbose::all() << "Force calculation is only supported for Pseudopotential and PAW methods!" << std::endl;
    throw std::invalid_argument("Force calculation is only supported for Pseudopotential and PAW methods!");
    return Teuchos::null;
}

// RCP<CI> good?
// Need work, more.
//RCP<CI> Create_Compute_Interface::Create_CI( RCP<const Basis> basis, RCP<ParameterList> parameters ){
RCP<Compute> Create_Compute_Interface::Create_CI( RCP<const Basis> basis, RCP<ParameterList> parameters){
    Teuchos::Array< std::vector<double > > orbital_energies;
    Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > eigenvector;

#ifdef USE_CI
    if(parameters -> name() == "CIS"){
        return rcp( new CIS(basis, parameters, eigenvector, orbital_energies) );
    } else if( parameters -> name() == "CISD"){
        return rcp( new CISD(basis, parameters, eigenvector, orbital_energies) );
    } else if( parameters -> name() == "GDST"){
        std::cout <<"These routines are under test. You can not use it now" << std::endl;
        exit(-1);
        //return rcp( new GDST(basis, parameters, eigenvector, orbital_energies) );
    }
#endif

    Verbose::all() << "Non-supported CI type: should be selected among CIS, CISD, and GDST" << std::endl;
    throw std::invalid_argument("Non-supported CI type: should be selected among CIS, CISD, and GDST");
    return Teuchos::null;
}

RCP<Compute> Create_Compute_Interface::Create_Utility( RCP<const Basis> basis, RCP<ParameterList> parameters){
#ifdef ACE_MKL
    if(parameters -> name() == "DDA"){
        return rcp( new DDA(basis, parameters) );
    }
#endif
    Verbose::all() << "Non-supported utility type: should be selected among DDA" << std::endl;
    throw std::invalid_argument("Non-supported utility type: should be selected among DDA");
    return Teuchos::null;
}
