#include "Update_Position.hpp"
#include <string>

#include "../Utility/Verbose.hpp"
#include "../Utility/String_Util.hpp"
#include "../Utility/Matrix_Multiplication.hpp"
#include "Hessian.hpp"

using std::string;
using std::vector;
using std::setw;

using Teuchos::RCP;

Update_Position::Update_Position(RCP<const Atoms> atoms, RCP<Teuchos::ParameterList> parameters){
    this->atoms = atoms;
    this->parameters = parameters;
}

vector<double> Update_Position::compose(vector<double *> positions){

    auto center_of_mass = atoms->get_center_of_mass();
    vector<double> position;

    for(int i=0; i<positions.size(); i++){
        position.push_back(positions.at(i)[0]-center_of_mass[0]);
        position.push_back(positions.at(i)[1]-center_of_mass[1]);
        position.push_back(positions.at(i)[2]-center_of_mass[2]);
    }
    return position;
}

vector<double> Update_Position::conjugate_gradient(vector<double> old_force, vector<double> current_force, vector<double> old_position, vector<double> current_position){
    vector<double> new_direction;

    int dim = old_position.size();
    if(dim == 0){
        for(int i=0; i<current_force.size(); i++){
            double tmp = current_force[i];
            //double tmp = -current_force[i];

            new_direction.push_back(tmp);
        }
    }
    else{
        for(int i=0; i<dim; i++){
            double old_direction_tmp = current_position[i] - old_position[i];
            double tmp = 0.0;
            if(std::abs(old_force[i]) >= parameters->sublist("BasicInformation").sublist("Opt").get<double>("ForceTolerance")){
                //tmp = -current_force[i] + old_direction_tmp * (current_force[i]*current_force[i])/(old_force[i]*old_force[i]);
                tmp = current_force[i] + old_direction_tmp * (current_force[i]*current_force[i])/(old_force[i]*old_force[i]);
            }
            new_direction.push_back(tmp);
        }
    }
    return new_direction;
}

double Update_Position::line_search(vector<double> gradient, vector<double> direction, vector<vector<double> > hessian){

    double numerator = Matrix_Multiplication::dot(gradient, direction);
    double denomiator = Matrix_Multiplication::dot(direction, Matrix_Multiplication::multiply(hessian, direction));
    double alpha = 0.0;
    if(std::abs(denomiator) > 1.0E-5){
        alpha = numerator/denomiator;
    }
    else{
        alpha = 0.0;
    }

    return alpha;
}

void Update_Position::update(vector< vector<double> > old_Hessian, vector<double> old_position, vector<double> old_force, vector<double> current_force){
    vector<double *> positions = atoms->get_positions();
    vector<double> current_position = this->compose(positions);
    vector<double *> new_positions;
    double tolerance = parameters->sublist("BasicInformation").sublist("Opt").get<double>("ForceTolerance");
    //double* tmp_position = new double[3];

    if(parameters->sublist("BasicInformation").sublist("Opt").get<string>("OptimizationMethod") == "QN"){
        // Linear mixing
        new_hessian = Hessian::update_hessian(old_Hessian, old_force, current_force, old_position, current_position, tolerance);
        vector<double> direction = Matrix_Multiplication::multiply(new_hessian, current_force);

        int a = direction.size()/3;
        new_position.clear();
        if(old_position.size()==0){
            for(int i=0; i<a; i++){
                double* tmp_position = new double[3];
                tmp_position[0] = current_position.at(3*i) + direction.at(3*i);
                tmp_position[1] = current_position.at(3*i+1) + direction.at(3*i+1);
                tmp_position[2] = current_position.at(3*i+2) + direction.at(3*i+2);
                new_positions.push_back(tmp_position);
                Verbose::single(Verbose::Normal) << "direction : " << std::endl;
                Verbose::single(Verbose::Normal) << "0 : " << direction[0] << "\t1 : " << direction[1] << "\t2 : " << direction[2] << std::endl;
                Verbose::single(Verbose::Normal) << "position : " << std::endl;
                Verbose::single(Verbose::Normal) << "0 : " << tmp_position[0] << "\t1 : " << tmp_position[1] << "\t2 : " << tmp_position[2] << std::endl;
                //delete[] tmp_position;
            }
        }

        else{
            double alpha = this->line_search(current_force, direction, new_hessian);
            for(int i=0; i<a; i++){
                double* tmp_position = new double[3];
                tmp_position[0] = current_position.at(3*i) + alpha*direction.at(3*i);
                tmp_position[1] = current_position.at(3*i+1) + alpha*direction.at(3*i+1);
                tmp_position[2] = current_position.at(3*i+2) + alpha*direction.at(3*i+2);
                new_positions.push_back(tmp_position);
                //delete[] tmp_position;
                Verbose::single(Verbose::Detail) << "dir1 : " << direction[3*i] <<  "dir2 : " << direction[3*i+1] <<  "dir3 : " << direction[3*i+2] << std::endl;
                Verbose::single(Verbose::Detail) << "alpha : " << alpha << std::endl;
            }
        }
        new_position = this->compose(new_positions);
    }

    else if(parameters->sublist("BasicInformation").sublist("Opt").get<string>("OptimizationMethod") == "GD"){
        // GEDIIS
        Verbose::all() << "GEDIIS not implemented!" << std::endl;
        exit(-1);
    }

    else if(parameters->sublist("BasicInformation").sublist("Opt").get<string>("OptimizationMethod") == "CG"){
        vector<double> direction = this->conjugate_gradient(old_force, current_force, old_position, current_position);

        int a = direction.size()/3;
        for(int i=0; i<a; i++){
            double* tmp_position = new double[3];
            tmp_position[0] = current_position.at(3*i) + direction.at(3*i);
            tmp_position[1] = current_position.at(3*i+1) + direction.at(3*i+1);
            tmp_position[2] = current_position.at(3*i+2) + direction.at(3*i+2);
            new_positions.push_back(tmp_position);
            //delete[] tmp_position;
        }
        new_position = this->compose(new_positions);
    }

    else{
        Verbose::all() << "Wrong optimization method: Choose 'CG' or 'QN' or 'GD' " << std::endl;
        exit(-1);
    }

    return;
}

vector<double> Update_Position::get_positions(){
    return new_position;
}

vector< vector<double> > Update_Position::get_hessian(){
    return new_hessian;
}
