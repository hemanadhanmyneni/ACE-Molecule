#pragma once
#include "Teuchos_RCP.hpp"
#include "Teuchos_ParameterList.hpp"

#include "Guess.hpp"
#include "Core_Hamiltonian.hpp"

#include "../Basis/Basis.hpp"
#include "../Io/Atoms.hpp"

/**
 * @brief Factory pattern for Compute_Interfaces.
 * @author Sungwoo Kang
 * @date 2016.2
 **/
namespace Create_Compute_Interface{
    /**
     * @brief Factory pattern for Guess.
     * @details Currently supported Guess types are: Core Hamiltonian, Atomic Density, Cube, PAW guess, Grid Cutting, and Random.
     * @param atoms Atoms information.
     * @param parameters Input parameters.
     * @param basis Exists for Core_Hamiltonian guess.
     * @note This contains initialization of Occupations, too. KSW does not think it is right.
     **/
    Teuchos::RCP<Guess> Create_Guess(
        Teuchos::RCP<const Atoms> atoms, 
        Teuchos::RCP<Teuchos::ParameterList> parameters, 
        Teuchos::RCP<const Basis> basis = Teuchos::null
    );
    
    /*
    Teuchos::RCP<Scf> Create_Scf(
        Teuchos::RCP<const Basis> basis,
        Teuchos::RCP<Teuchos::ParameterList> parameters, 
        Teuchos::RCP<Core_Hamiltonian> core_hamiltonian = Teuchos::null
    );
    */

    /**
     * @brief Factory pattern for Scf, TDDFT.
     * @param basis Basis for system.
     * @param parameters Input parameters.
     * @param core_hamiltonian Exists to remove duplicate core hamiltonian construction, if core hamiltonian guess is used.
     * @note This is incomplete and very basic, since Scf and TDDFT are not a good design and actually does both parameter interpretation and calculation.
     * @note Should this include Opt?
     **/
    Teuchos::RCP<Compute> Create_DFT(
        Teuchos::RCP<const Basis> basis,
        Teuchos::RCP<Teuchos::ParameterList> parameters, 
        Teuchos::RCP<Core_Hamiltonian> core_hamiltonian = Teuchos::null
    );

    Teuchos::RCP<Compute> Create_Force(
        Teuchos::RCP<const Basis> basis,
        Teuchos::RCP<const Atoms> atoms, 
        Teuchos::RCP<Teuchos::ParameterList> parameters
    );

    /**
     * @brief Factory pattern for CI;.
     * @details Currently supported CI methods are: CIS, CISD, GDST, and all of them are incompelte and experimental.
     * @param basis Basis for system.
     * @param parameters Input parameters.
     * @param num_orbitals Number of orbitals to compute.
     * @note This is incomplete and very basic, since all CI classes are not a good design and actually does both parameter interpretation and calculation, and incomplete.
     **/
    Teuchos::RCP<Compute> Create_CI(
        Teuchos::RCP<const Basis> basis, 
        Teuchos::RCP<Teuchos::ParameterList> parameters
    );
    
    Teuchos::RCP<Compute> Create_Utility(
        Teuchos::RCP<const Basis> basis,
        Teuchos::RCP<Teuchos::ParameterList> parameters
    );
};

