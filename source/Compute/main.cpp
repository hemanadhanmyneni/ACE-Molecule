#include <iostream>
#include <string>
#include <stdexcept>
#include <cmath>
#include <cctype>

#include "../Utility/ACE_Config.hpp"

#include "Teuchos_RCP.hpp"
#include "Teuchos_ParameterList.hpp"
#include "Teuchos_TimeMonitor.hpp"

#include "Epetra_Vector.h"
#include "Epetra_Map.h"

#include "Epetra_BLAS.h"

#ifdef ACE_HAVE_MPI
    #include "mpi.h"
    #include "Epetra_MpiComm.h"
#endif
#include "Epetra_Comm.h"
#include "Epetra_SerialComm.h"

#include "Epetra_CrsMatrix.h"
#include "EpetraExt_MatrixMatrix.h"
#include "Epetra_Version.h"

//#include <time.h>

#include "../Basis/Basis.hpp"
#include "../Basis/Create_Basis.hpp"

#include "../Utility/Parallel_Manager.hpp"
#include "../Utility/Verbose.hpp"
#include "../Utility/Time_Measure.hpp"

#include "../Io/Io.hpp"
#include "../Io/Atoms.hpp"
#include "../Utility/Default_Values/Set_Default_Values.hpp"
#include "../Utility/Default_Values/Set_Basic_Information.hpp"
//#include "../Core/Occupation/Occupation_Zero_Temp.hpp"
//#include "../Core/Occupation/Occupation.hpp"
#include "Create_Compute.hpp"
#include "Create_Compute_Interface.hpp"
#include "../Utility/String_Util.hpp"
#include "Scf.hpp"
#include "DDA.hpp"
#ifdef ACE_HAVE_OPT
    #include "Opt.hpp"
#endif
#include "../State/State.hpp"
#include "Dipole.hpp"
#include "../Utility/ParamList_Util.hpp"

/********** EDISON **********/
#ifdef EDISON
#include <sys/stat.h>// For linux mkdir
#include "../Utility/Default_Values/Set_EDISON.hpp"
#endif
/********** END **********/

using std::string;
using std::fixed;
using std::scientific;
using std::cout;

using Teuchos::RCP;
using Teuchos::rcp;
using Teuchos::Array;
using Teuchos::ParameterList;
using Teuchos::rcp;

#include <ctime>
void pause(int dur)
{
    int temp = time(NULL) + dur;
    while(temp > time(NULL));
}

/**
@mainpage
This program is a real-space density functional theory (DFT) package.
This doxygen page contains source code documentation.

You may refer program page (https://gitlab.com/aceteam.kaist/ACE-Molecule/wikis/home) for more detail information.

*/


/** page InstallPage Install
ACE-Molecule currently contains 4 mandatory dependencies. First of all, Trilinos packages, which is developed by Sandia National Lab. takes charge of linear algebraic operations in ACE-Molecule. The most of performance-sensitive part is done by Trilinos package. Thus, please take care of installing Trilinos package to obtain high performance. Also, default configurations used for ACE-Molecule is determined as the same to the configuration that are used for Trilinos.
Libxc is exchange-correlation libraries. Except a few functionals, most of functionals are rely on that library. GSL library takes charge of the multi-dimensional conjugate gradient routine for geometry optimization. CMake(minimum version 2.8) is required to install ACE-Molecule.

For optional libraries, please refer the below list
CUDA : If you want to enable GPU, CUDA library should be given
MAGMA : For GPU calculations, this is optional dependency.

*/

int main (int argc, char* argv[]){
#ifdef EDISON
    argc = 3;
    argv[1] = argv[2];
    argv[2] = argv[4];
#endif
    if(argc < 2){
        string errmsg = "Wrong input argument. You have to provide inputfile.\n";
        errmsg += "ex) " + string(argv[0]) + " input.txt\n";
        std::cout << errmsg;
        throw std::runtime_error(errmsg);
    }

    Parallel_Manager::construct( argc, argv );
    Teuchos::RCP<Time_Measure> timer = Teuchos::rcp(new Time_Measure() );
    timer->start("Total Time");
    //Parallel_Manager::construct(argc, argv);
    string input = argv[1];
    Io* io = new Io();
    Teuchos::Array< Teuchos::RCP<Teuchos::ParameterList> > parameters;
    io->read_input(input,parameters);
    RCP<Teuchos::ParameterList> parameters_basic = ParamList_Util::get_subparameter(parameters, "BasicInformation");
    int verboselevel = parameters_basic -> get<int>("VerboseLevel",0);
#ifdef EDISON
    std::ofstream fstream;
    if( Parallel_Manager::info().get_total_mpi_rank() == 0 ){
        mkdir("result", S_IRWXU | S_IRWXG | S_IRWXO);
        fstream.open("result/output.log", std::ofstream::out);
        std::cout.flush();
        fstream.flush();
    }
    TeeStream teestream(fstream, std::cout);
    if( Parallel_Manager::info().get_total_mpi_rank() == 0 ){
        Verbose::construct(&teestream, verboselevel);
    } else {
        Verbose::construct(NULL, verboselevel);
    }
#else
    Verbose::construct(NULL, verboselevel);
#endif
    Teuchos::RCP<Epetra_Comm> comm = Parallel_Manager::info().get_all_comm();
    Verbose::single(Verbose::Simple)<<"\n";
    Verbose::single(Verbose::Simple)<<"                             ..:::::::::::::::.... \n" ;
    Verbose::single(Verbose::Simple)<<"                   .::== .#%*  +++++++++++++++++++: =##= .:.. \n";
    Verbose::single(Verbose::Simple)<<"            .::=+++++++: =@@@  +++++++++++++++++++. #@@# .+++++==:. \n" ;
    Verbose::single(Verbose::Simple)<<"          .:=+++++++++++:    .=++++++++++++++++++++.    .+++++++++=:            \n" ;
    Verbose::single(Verbose::Simple)<<"                ..:==++++++++++++++++++++++++++++++++++++++=::.\n" ;
    Verbose::single(Verbose::Simple)<<"                        ..:::====++++++++++++++===:::.. \n" ;
    Verbose::single(Verbose::Simple)<<"                              .:+**************\n";
    Verbose::single(Verbose::Simple)<<"                          .:++++=. +++++++++=+++: \n";
    Verbose::single(Verbose::Simple)<<"                   .+++=++++=.     +++++++++= :+++. \n";
    Verbose::single(Verbose::Simple)<<"                   +++++:.         +++++++++=   =+++:=: \n";
    Verbose::single(Verbose::Simple)<<"                    .::.           +++++++++=     :+++++ \n";
    Verbose::single(Verbose::Simple)<<"                                   ++=::::++=      :+++: \n";
    Verbose::single(Verbose::Simple)<<"                                   ++:    ++= \n";
    Verbose::single(Verbose::Simple)<<"                                   ++:    ++= \n" ;
    Verbose::single(Verbose::Simple)<<"                              .::::++:    ++=.::: \n";
    Verbose::single(Verbose::Simple)<<"                             .*++++++:    +++++++* \n" ;
    Verbose::single(Verbose::Simple)<<"\n";
    Verbose::single(Verbose::Simple)<<"           .,,**////////((((((((((((((((((((######################(/*..           \n";
    Verbose::single(Verbose::Simple)<<".,,******///////////////((((((((((((((((((((#####################%%%%%%%%%%%%#(/,.\n";
    Verbose::single(Verbose::Simple)<<"                   ..,**////((((((((((((((((##########((((/*,..                   \n";
    Verbose::single(Verbose::Simple)<<"\n";
    Verbose::single(Verbose::Simple)<<"\n";
    Verbose::single(Verbose::Simple)<<"==================================================================================\n";
    Verbose::single(Verbose::Simple)<<" If you use our program, please cite this work : \n";
    Verbose::single(Verbose::Simple)<<" Sunghwan Choi, Kwangwoo Hong, Jaewook Kim, and Woo Youn Kim,\n";
    Verbose::single(Verbose::Simple)<<"   \"Accuracy of Lagrange-sinc functions as a basis set for electronic structure \n";
    Verbose::single(Verbose::Simple)<<"    calculations of atoms and molecules\"\n";
    Verbose::single(Verbose::Simple)<<"                               The Journal of Chemical Physics, 142, 094116 (2015)\n";
    Verbose::single(Verbose::Simple)<<"==================================================================================\n";
    Verbose::single(Verbose::Simple)<<"\n";

    Verbose::single(Verbose::Normal) << "\nDATE COMPILED  : " <<__DATE__<< ", "<< __TIME__<<"\n";
#ifdef __GIT_SHA__
    Verbose::single(Verbose::Normal) << "Git commit hash: " << __GIT_SHA__ << std::endl;
#endif
#ifdef __ACE_COMPILE_FLAGS__
    Verbose::single(Verbose::Normal) << "Compiler flags : " << __ACE_COMPILE_FLAGS__ << std::endl;
#endif
#ifdef __ACE_LINKER_FLAGS__
    Verbose::single(Verbose::Normal) << "Linker flags   : " << __ACE_LINKER_FLAGS__ << std::endl;
#endif
    Verbose::single(Verbose::Simple)<<"\n" << Epetra_Version() <<"\n \n" ;
    Verbose::all(Verbose::Simple)<< Parallel_Manager::info();
    Verbose::single(Verbose::Simple) << std::endl;

    Array<RCP<Default_Values::Set_Default_Values> > default_params;
    default_params.append(Teuchos::rcp( new Default_Values::Set_Basic_Information() ));
    /********** EDISON **********/
    #ifdef EDISON
    if( !parameters_basic -> isParameter("GeometryFilename") and argc > 2){
        parameters_basic -> set("GeometryFilename", argv[2]);
    }
    default_params.insert(default_params.begin(), Teuchos::rcp( new Default_Values::Set_EDISON() ));
    #endif
    /********** END **********/
    for(int i = 0; i < default_params.size(); ++i){
        default_params[i]->set_parameters(parameters);
    }
    RCP<Atoms> atoms = Teuchos::rcp(new Atoms() );

    parameters_basic = ParamList_Util::get_subparameter(parameters, "BasicInformation");
    if(!parameters_basic -> isParameter("GeometryFilename")){
        throw std::invalid_argument("GeometryFilename is not given!");
    }
    atoms->operator=(io->read_atoms(parameters_basic -> get<string>("GeometryFilename"), parameters_basic -> get<string>("GeometryFormat")));

    ParamList_Util::initialize_electron_parameters(Teuchos::rcp_const_cast<const Atoms>(atoms), parameters);

    //io->test_input(parameters,*atoms);
    Verbose::set_numformat(Verbose::Pos);
    if(parameters_basic -> get<int>("Centered", 1) != 0){
        Verbose::single(Verbose::Simple) << "Center of mass is moved to 0,0,0 \n" ;
        atoms->move_center_of_mass(0.0,0.0,0.0);
    } else {
        Verbose::single(Verbose::Simple) << "Center of mass= "  ;
        Verbose::single(Verbose::Simple) << atoms->get_center_of_mass()[0] << ",";
        Verbose::single(Verbose::Simple) << atoms->get_center_of_mass()[1] << ",";
        Verbose::single(Verbose::Simple) << atoms->get_center_of_mass()[2] << std::endl;
    }
    Verbose::single(Verbose::Simple) << *atoms <<std::endl;
    Verbose::set_numformat();

    State::do_shallow_copy_orbitals=false;
    if(parameters_basic -> get<int>("ShallowCopyOrbitals", 0) != 0){
        State::do_shallow_copy_orbitals=true;
        Verbose::single(Verbose::Simple) << "(ShallowCopyOrbitals) memory save mode is turned on" <<std::endl;
    }

    Verbose::set_numformat(Verbose::Pos);
    RCP<Basis> basis = Create_Basis::Create_Basis(*comm, parameters_basic, atoms);
    Verbose::set_numformat(Verbose::Pos);
    Verbose::single(Verbose::Simple)<< *basis <<std::endl;

    Array< RCP<State> > states;

    //Verbose::single(Verbose::Simple) << "====== Input parameters ======\n";
    //Verbose::single(Verbose::Simple)<< *parameters << std::endl;

    Array< RCP<ParameterList> > array_parameters = io->split(parameters);
    Verbose::single(Verbose::Detail) << "= There are initial " << array_parameters.size() << " calculation step : \n";
    for (int i = 0; i<array_parameters.size(); i++){
        Verbose::single(Verbose::Detail)<< "======Initial Input parameters for step #" << i+1 << " ======" <<std::endl;
        Verbose::single(Verbose::Detail)<< array_parameters[i] -> name() <<std::endl;
        Verbose::single(Verbose::Detail)<< *array_parameters[i] <<std::endl;
        Verbose::single(Verbose::Detail)<< std::endl <<std::endl;
    }

    // Create_Compute routine
    Array< RCP<Compute> > computes;
    if(parameters_basic -> get<string>("Mode") == "Auto"){
        for(int i = 0; i < array_parameters.size(); ++i){
            string name = array_parameters[i] -> name();
            if(name == "Guess"){
                computes.append( Create_Compute_Interface::Create_Guess(atoms,array_parameters[i], basis) ) ;
            } else if (name == "Scf" or name == "TDDFT" or name == "Opt"){
                computes.append( Create_Compute_Interface::Create_DFT(basis,array_parameters[i] ) );
            } else if(name == "DDA"){
                computes.append( Create_Compute_Interface::Create_Utility(basis,array_parameters[i] ) );
            } else if(name == "Force"){
                computes.append( Create_Compute_Interface::Create_Force(basis, atoms, array_parameters[i] ) );
            } else {
                computes.append( Create_Compute_Interface::Create_CI(basis, array_parameters[i]) );
            }
        }
    }
#ifdef ACE_HAVE_OPT
    else if(parameters_basic -> get<string>("Mode") == "Opt"){
        computes.append( Teuchos::rcp_implicit_cast<Compute> ( rcp( new Opt( basis, atoms, states, array_parameters) ) ) );
    }
#endif
    else{
        //1. Guess always exists array_parameter[0] 2. Scf doesn't change. 3. other calculation routines is always placed on end of array_parameters.
        for(int i = 0; i < array_parameters.size(); ++i){
            if(array_parameters[i] -> name() == "Guess"){
                RCP<ParameterList> paramlist = array_parameters[i];
                array_parameters.remove(i);
                array_parameters.insert(array_parameters.begin(),paramlist);
                break;
            }
        }
        if( parameters_basic -> get<string>("Mode") == "Sp" ){
            for(int i = 0; i < array_parameters.size(); ++i){
                if(array_parameters[i] -> name() == "Scf"){
                    RCP<ParameterList> paramlist = array_parameters[i];
                    array_parameters.remove(i);
                    array_parameters.insert(array_parameters.begin()+1,paramlist);
                }
            }
        }
        else if( parameters_basic -> get<string>("Mode") == "SpTDDFT" ){
            for(int i = 0; i < array_parameters.size(); ++i){
                if(array_parameters[i] -> name() == "TDDFT"){
                    RCP<ParameterList> paramlist = array_parameters[i];
                    array_parameters.remove(i);
                    array_parameters.insert(array_parameters.begin()+1,paramlist);
                }
            }
            for(int i = 0; i < array_parameters.size(); ++i){
                if(array_parameters[i] -> name() == "Scf"){
                    RCP<ParameterList> paramlist = array_parameters[i];
                    array_parameters.remove(i);
                    array_parameters.insert(array_parameters.begin()+1,paramlist);
                }
            }
        }
        else if( parameters_basic -> get<string>("Mode") == "TDDFT" ){
            for(int i = 0; i < array_parameters.size(); ++i){
                if(array_parameters[i] -> name() == "TDDFT"){
                    RCP<ParameterList> paramlist = array_parameters[i];
                    array_parameters.remove(i);
                    array_parameters.insert(array_parameters.begin()+1,paramlist);
                }
            }
        }
        int cal_num = 0; // It is used to avoid repetition because element of array_pameters is added end of array
        for(int i = 0; i <array_parameters.size() - cal_num ; ++i){
            bool compute_routine = false;
            if(array_parameters[i] -> name() == "Force"){
                compute_routine = true;
            }
            if(array_parameters[i] -> name() == "CIS" or array_parameters[i] -> name() == "CISD"){
                compute_routine = true;
            }

            if(array_parameters[i] -> name() == "DDA"){
                compute_routine = true;
            }
            if(compute_routine == true){
                cal_num++;
                RCP<ParameterList> paramlist = array_parameters[i];
                array_parameters.remove(i);
                array_parameters.push_back(paramlist);
            }
        }
        for(int i = 0; i < array_parameters.size(); ++i){
            string name = array_parameters[i] -> name();
            if(name == "Guess"){
                computes.append( Create_Compute_Interface::Create_Guess(atoms,array_parameters[i], basis) ) ;
            } else if (name == "Scf" or name == "TDDFT" or name == "Opt"){
                computes.append( Create_Compute_Interface::Create_DFT(basis,array_parameters[i] ) );
            } else if(name == "DDA"){
                computes.append( Create_Compute_Interface::Create_Utility(basis,array_parameters[i] ) );
            } else if(name == "Force"){
                computes.append( Create_Compute_Interface::Create_Force(basis, atoms, array_parameters[i] ) );
            } else {
                computes.append( Create_Compute_Interface::Create_CI(basis, array_parameters[i]) );
            }
        }
    }

    // This part is printed after grid cutting information because of Guess part.
    Verbose::single(Verbose::Simple) << std::endl << "= There are " << array_parameters.size() << " calculation steps";
    for (int i = 0; i<array_parameters.size(); i++){
        Verbose::single(Verbose::Normal)<< "====== Input parameters for step #" << i+1 << " ======" <<std::endl;
        Verbose::single(Verbose::Normal)<< array_parameters[i] -> name() <<std::endl;
        Verbose::single(Verbose::Normal)<< *array_parameters[i] <<std::endl;
        Verbose::single(Verbose::Normal)<< std::endl <<std::endl;
    }

    // Actual Computation routine
    for (int i =0; i<computes.size();i++){
        Verbose::single(Verbose::Simple)<< "\n================= Calculation Step #" << i+1 << "/" << computes.size() << " (" << array_parameters[i] -> name() << ") =====================" <<std::endl;

        int ierr = computes[i]->compute(basis,states);
        int terminate = array_parameters[i] -> sublist(array_parameters[i] -> name()).get<int>("ErrorTermination", 0);

        if( ierr < 0 and terminate > -1){
            Verbose::single(Verbose::Simple) << std::endl;
            Verbose::single(Verbose::Simple)
                              << "WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING" << std::endl
                              << "WARNING" <<std::endl
                              << "WARNING  Calculation Step #" << i+1 << " (" << array_parameters[i] -> name() << ") terminated errorously" << std::endl
                              << "WARNING  Error code: " << ierr << std::endl
                              << "WARNING  DO NOT TRUST FURTHER CALCULATIONS!!" <<std::endl
                              << "WARNING" <<std::endl
                              << "WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING" << std::endl;
            Verbose::single(Verbose::Simple) << std::endl;
        }
        if( ierr < 0 and terminate > 0){
            throw ("Calculation Step #" + String_Util::to_string(i+1) + " (" + array_parameters[i] -> name() + ") terminated errorously");
        }

        states.at(states.size()-1) -> write( basis, array_parameters[i] );
        //states[states.size()-1]->timer = rcp(new Time_Measure() );
        if( states.at(states.size()-1) -> timer != Teuchos::null ){
            if( states.at(states.size()-1) -> timer -> get_tags().size() > 0 ){
                Verbose::single(Verbose::Simple) << "Computation routine " << states.size() << " time profile:" << std::endl;
                Verbose::set_numformat(Verbose::Time);
                states.at(states.size()-1)->timer->print(Verbose::single(Verbose::Simple));
                Verbose::set_numformat(Verbose::Default);
            }
        }
    }

    Verbose::set_numformat(Verbose::Energy);
    if(std::abs(states.at(states.size()-1) -> total_free_energy) > 1.0E-15 and std::abs(states.at(states.size()-1) -> total_free_energy - states.at(states.size()-1) -> total_energy) > 1.0E-15){
        Verbose::single() << "\n! total free energy  " << std::setprecision(6) << states.at(states.size()-1)->total_free_energy <<std::endl;
    }
    Verbose::single() << "\n! total energy  " << std::setprecision(6) << states.at(states.size()-1)->total_energy <<std::endl;
    
    Verbose::single()<< "orbital occupations / energies" <<std::endl;
    for(int s = 0; s < states.at(states.length()-1)->orbital_energies.size(); ++s){
        Verbose::single() << "SPIN " << s << std::endl;
        for (int i=0; i <states.at(states.length()-1)->orbital_energies[0].size() ;i++){
            Verbose::set_numformat(Verbose::Occupation);
            Verbose::single() << states.at(states.length()-1)->occupations[s]->operator[](i) << '\t';
            Verbose::set_numformat(Verbose::Energy);
            Verbose::single() << std::real(states.at(states.length()-1)->orbital_energies[s][i]) << std::endl;
        }
    }

    Dipole a;
    a.Compute_dipole(states.at(states.size()-1),basis);
/*/     Verbose dipole 
    Verbose::single(Verbose::Simple) << "===Dipole moment=============================================" << std::endl;
    Verbose::single(Verbose::Simple) << "Dipole moment       = " << setw(_ENERGY_WIDTH_)<<states.at(states.length()-1)->get_dipole_moment << " a.u." << std::endl;
    Verbose::single(Verbose::Simple) << "     X component    = " << setw(_ENERGY_WIDTH_)<<states.at(states.length()-1)->get_dipole_moments[0]<< " a.u." <<  std::endl;
    Verbose::single(Verbose::Simple) << "     Y component    = " << setw(_ENERGY_WIDTH_)<<states.at(states.length()-1)->get_dipole_moments[1]<< " a.u." <<  std::endl;
    Verbose::single(Verbose::Simple) << "     Z component    = " << setw(_ENERGY_WIDTH_)<<states.at(states.length()-1)->get_dipole_moments[2]<< " a.u." <<  std::endl;
    Verbose::single(Verbose::Simple) << "=============================================================" << std::endl<< std::endl;
*/
    Verbose::set_numformat(Verbose::Default);

    computes.clear();

    Verbose::single(Verbose::Simple) << "====== Final parameters ======\n";
    Verbose::single(Verbose::Simple) << "= There are " << array_parameters.size() << " calculation step : \n";
    for (int i = 0; i<array_parameters.size() ; i++){
        Verbose::single(Verbose::Simple)<< "====== Final parameters for step #" << i << " ======" <<std::endl;
        Verbose::single(Verbose::Simple)<< *array_parameters[i] <<std::endl << std::endl<<std::endl;
    }
    //timer -> stop();
    //Teuchos::TimeMonitor::report(cout);

    delete io;

    Core_Hamiltonian::free_matrix();
    timer->end("Total Time");
    Verbose::set_numformat(Verbose::Time);
    timer->print(Verbose::single(Verbose::Simple));
    Verbose::free();
    Parallel_Manager::finalize();
    return 0;
}
