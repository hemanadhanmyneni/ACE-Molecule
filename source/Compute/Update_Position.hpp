#pragma once
#include "Teuchos_ParameterList.hpp"
#include "../Io/Atoms.hpp"

class Update_Position{
    public:
        Update_Position(
            Teuchos::RCP<const Atoms> atoms,
            Teuchos::RCP<Teuchos::ParameterList> parameters
        );

        double line_search(std::vector<double> gradient, std::vector<double> direction, std::vector<std::vector<double> > hessian);
        std::vector<double> compose(std::vector<double *> positions);
        std::vector<double> conjugate_gradient(std::vector<double> old_force, std::vector<double> current_force, std::vector<double> old_position, std::vector<double> current_position);
        void update(std::vector< std::vector<double> > old_Hessian, std::vector<double> old_position, std::vector<double> old_force, std::vector<double> current_force);
        std::vector<double> get_positions();
        std::vector< std::vector<double> > get_hessian();

    protected:
        Teuchos::RCP<const Atoms> atoms;
        Teuchos::RCP<Teuchos::ParameterList> parameters;

        std::vector< std::vector<double> > new_hessian;
        std::vector<double> new_position;
};
