#include "Scf2.hpp"
#include <string>
#include <stdexcept>
#include <EpetraExt_MatrixMatrix.h>

#include "../Core/Convergence/Create_Convergence.hpp"
#include "../Core/Diagonalize/Create_Diagonalize.hpp"
#include "../Core/Mixing/Create_Mixing.hpp"

#include "../Core/Occupation/Create_Occupation.hpp"
#include "../Core/Diagonalize/Pure_Diagonalize.hpp"
#include "../Core/Diagonalize/Direct_Diagonalize.hpp"

#include "../Io/Atoms.hpp"
#include "../Utility/Density/Density_From_Orbitals.hpp"
#include "../Utility/String_Util.hpp"
#include "../Utility/Value_Coef.hpp"
#include "../Utility/Two_e_integral.hpp"
#include "../Utility/SymMMMul.hpp"
#include "../Utility/Parallel_Manager.hpp"
#include "Epetra_LocalMap.h"
#define VERBOSE_DEBUG (-1)

using std::string;
using std::vector;
using std::setw;

using Teuchos::Array;
using Teuchos::RCP;
using Teuchos::rcp;
//using Teuchos::ParameterList;

Scf2::Scf2(RCP<const Basis> basis,RCP<Teuchos::ParameterList> parameters,RCP<Core_Hamiltonian> core_hamiltonian): Scf(basis, parameters, core_hamiltonian){
//    Scf::Scf(basis, parameters, core_hamiltonian);
    max_iter_micro_scf = parameters -> sublist("Scf").get<int>("MaxMicroIter", 10);
}

int Scf2::iterate(RCP<State> initial_state, RCP<State>& final_state){
    // This does not support 
    if (core_hamiltonian->get_core_hamiltonian_matrix()->nuclear_potential->get_type().compare("PAW") ==0){
        Verbose::single() <<  "micro SCF does not support PAW calculations" << std::endl;
        exit(-1);
    }
    if(false==make_hamiltonian_matrix){
        Verbose::single() << "micro SCF does not support implicit matirx " <<std::endl;
        exit(-1);
    }


    Verbose::single()<< std::endl;
    Verbose::single()<< "#-------------------------------------------------------------- Scf2::iterate" << std::endl;
    auto timer = Teuchos::rcp(new Time_Measure() );
    timer->start("Scf2::iterate");
    this -> internal_timer -> start("Scf2::iterate::initialization");
    //const int spin_size = initial_state->occupations.size();
    const int num_eigval = parameters->sublist("Scf").get<int>("NumberOfEigenvalues");
    const int max_cycle = parameters->sublist("Scf").get<int>("IterateMaxCycle");
    int return_val=-1;

    convergence = Create_Convergence::Create_Convergence(Teuchos::sublist(parameters,"Scf") );
    mixing = Create_Mixing::Create_Mixing(Teuchos::sublist(parameters,"Scf"));

    Array<RCP<Epetra_Vector> > external_potential;
    for(int i_spin=0;i_spin<initial_state->occupations.size();i_spin++){
        external_potential.append(rcp( new Epetra_Vector(*basis->get_map())) );
    }
    if(external_field != Teuchos::null ){
        if(external_field->is_field_applied()){
            external_field->get_potential(external_potential);
        }    
    }

    /////////////////////////////End/////////////////////////////////////////
    /**
    //Scf2 iteration, start!
    //in_state 
    //3rd page
    //for linear mixing, we mix "in_state" and "out_state"
    //for pulay mixing, we mix "in_state", which are stored in states
    //  (be aware that pulay mixing class stores "residue" = "out_state" - "in_state"
    //in scf_states, ONLY the "in_state" should be stored (to restart the calculation),
    // except for the last state(last state is "out_state", which is converged state)
    //
    // nth scf_states component contain follwing information:
    //    n-1th out_state orbitals & orbital energies
    //    n-1th mixed_state(or out_state) density = nth in_state density
    //    n-1th mixed_state(or out_state) local_potential = nth in_state local_potential
    //    occupations (occupation information would not be changed)
    //    core_density & core_density_grad (which come from psudopotential)
    //    n-1th mixed_state(or out_state) hartree, x, c_potential,
    //                           which are components of the local_potential.
    */
    //initialization!
    scf_states.clear();

    //in_state should contain rho_0^in, psi_0^in, V_loc,0^in.
    //use prho_0^in and psi_0^in from initial_state.
    //calculate V_loc,0^in
    /* generate new occupations and put it in in_state */
    Teuchos::RCP<Teuchos::ParameterList> occ_param;
    if(parameters->isSublist("Scf")){
        if(parameters->sublist("Scf").isSublist("Occupation")){
            occ_param = sublist(sublist(parameters,"Scf"),"Occupation");
        }
        else{
            occ_param = Teuchos::rcp(new Teuchos::ParameterList() );
        }
    
        occ_param->set("NumElectrons",parameters->sublist("BasicInformation").get<double>("NumElectrons") );
        occ_param->set("SpinMultiplicity",parameters->sublist("BasicInformation").get<double>("SpinMultiplicity") );
        occ_param->set("Polarize", parameters -> sublist("BasicInformation").get<int>("Polarize",0));
        if(parameters -> sublist("Scf").isParameter("NumberOfEigenvalues")){
            occ_param -> set("OccupationSize", parameters->sublist("Scf").get<int>("NumberOfEigenvalues"));
        }

        auto occupations = Create_Occupation::Create_Occupation(occ_param );
        initial_state->occupations = occupations;
    }
    


    Verbose::single(Verbose::Normal)<<  "#-------------------------------------------------------------- Scf2::iterate" << std::endl;
    Verbose::single(Verbose::Normal) << "initialization," <<std::endl;
    Verbose::single(Verbose::Normal) << "    calculate xc and hartree potential of initial_state " <<std::endl;
    Verbose::single(Verbose::Normal)<<  "#---------------------------------------------------------------------------" << std::endl;

    scf_states.clear();
    scf_states.append( rcp(new Scf_State(*initial_state)) );

    if(parameters->sublist("Scf").get<int>("ComputeInitialEnergy",0)!=0){
        calculate_energy( scf_states[0] );
    }
    generate_local_potential(scf_states[scf_states.size()-1]);

//    duplicate? 
//    bool make_hamiltonian_matrix = true;
//    if(parameters->sublist("Scf").get<int>("Making_Hamiltonian_Matrix")==0){
//        Verbose::single() << "Diagonalization will be done without making hamiltonian matrix" << std::endl;
//        make_hamiltonian_matrix = false;
//    }
    this -> internal_timer -> end("Scf2::iterate::initialization");
    Verbose::single(Verbose::Simple) << "Scf2::iterate::initialization time: " << this -> internal_timer -> get_elapsed_time("Scf2::iterate::initialization", -1) << "s" << std::endl;

    RCP<Epetra_Vector> diagonal_of_hamiltonian = rcp(new Epetra_Vector(*basis -> get_map()));

    RCP<Direct_Diagonalize> direct_diagonalizer = rcp(new Direct_Diagonalize() );
    int dimension_of_reduced_space;
    if(parameters->sublist("Scf").isParameter("SizeOfReducedOrbitalSpace")){
        dimension_of_reduced_space= parameters->sublist("Scf").get<int>("SizeOfReducedOrbitalSpace");
    }
    else{
        if(num_eigval>15){
            dimension_of_reduced_space = num_eigval*2;
        }
        else{
            dimension_of_reduced_space = num_eigval+15;
        }
    }

    assert(num_eigval< dimension_of_reduced_space);

    const int occupation_size = initial_state->occupations.size();

    Teuchos::LAPACK<int,double> lapack;
    Teuchos::BLAS<int,double> blas;

    // generate orbital space and randomly initialize
    for (int i=0; i< occupation_size ; i++){
        orbital_space.append(rcp(new Epetra_MultiVector(*basis->get_map(), dimension_of_reduced_space) ) );
        orbital_space[orbital_space.size()-1]->Random();
        orbital_space[orbital_space.size()-1]->Scale(sqrt(3.0/orbital_space[orbital_space.size()-1]->Map().NumGlobalElements() ));

        double* norm = new double[dimension_of_reduced_space];
        orbital_space[orbital_space.size()-1]->Norm2(norm);
        for (int j=0; j<dimension_of_reduced_space; j++){
            orbital_space[orbital_space.size()-1]->operator()(j)->Scale(1.0/norm[j]);
        }
        delete[] norm ;
    }
    // start macro SCF cycle 

    // nth scf_states component contain follwing information:
    //    n-1th out_state orbitals & orbital energies
    //    n-1th mixed_state(or out_state) density = nth in_state density
    //    n-1th mixed_state(or out_state) local_potential = nth in_state local_potential
    //    occupations (occupation information would not be changed)
    //    core_density & core_density_grad (which come from psudopotential)
    //    n-1th mixed_state(or out_state) hartree, x, c_potential,
    //                           which are components of the local_potential.


    // -------------------------        generate_in_state(generate_local_potential)        ------------------<------
    // |                                                                                                            |
    // |                                                            eigenvalue&eigenvector                          |
    // -> in_state ------------------------> (update H matrix & Diag) ------------------> out_state-->append_state ->
    //
    auto in_state = Teuchos::rcp(new Scf_State(*scf_states[scf_states.size()-1]) );
    for (int j_scf=0; j_scf< max_cycle; ++j_scf){
        Array<RCP<Epetra_MultiVector> >  new_orbital_space;

        // update_orbital_space contains update_H_mailtrix, Diagonalize, append_state, and generate_in_state 
        // after those operation, it returns new in_state
        in_state = update_orbital_space( in_state, external_potential, orbital_space, new_orbital_space );
        
        const int occupation_size = new_orbital_space.size();
        double orbital_space_difference = 0.0;
        int    count_orbital_space = 0;

        // append states and check convergence of orbital space 
        for (int i_spin=0; i_spin< occupation_size; i_spin++){
            double* norm = new double[new_orbital_space[i_spin]->NumVectors()];
            Epetra_MultiVector tmp_vector(Epetra_LocalMap(new_orbital_space[i_spin]->NumVectors(),0, new_orbital_space[i_spin]->Comm() ), new_orbital_space[i_spin]->NumVectors());
            tmp_vector.Multiply('T','N',1.0,*orbital_space[i_spin], *new_orbital_space[i_spin],0.0);
            tmp_vector.Norm2(norm);
            
            for(int j = 0; j< num_eigval; j++){
                //Verbose::single()<< "dot["<<j <<"]: "<< norm [j] <<std::endl;
                orbital_space_difference +=norm[j];
            }
            delete [] norm;
            count_orbital_space += num_eigval;
        }
        
        // update orbital space 
        for (int i=0; i< occupation_size; i++){
            orbital_space[i]->operator=(*new_orbital_space[i]);
        }

        Verbose::single() << "Scf2::Convergence of orbital space: "<< orbital_space_difference <<std::endl;
        // if orbital space converge enough, let's break the for loop
        if ( fabs(orbital_space_difference-count_orbital_space) < parameters->sublist("Scf").get<double>("ConvergenceCriteraForOrbitalSpace",0.0001)*count_orbital_space ) {
            Verbose::single() << "Scf2::convergence of macro SCF is now achieved " <<std::endl;
            bool is_compute_energy_this_step = print_energy(j_scf,in_state);
            return_val=0;
            break;
        }
        else{
            Verbose::single() << "Scf2::convergence of macro SCF is not achieved yet" <<std::endl;
        }
        new_orbital_space.clear();
        Verbose::single()<< "micro-scf is now turned off temperally" <<std::endl;

        // project core hamiltonian matrix + external_potential on orbital space 
        project_core_hamiltonians(dimension_of_reduced_space, external_potential);
//        std::string filename = "before";
//        scf_states[scf_states.size()-1]->write_orbital(filename+String_Util::to_string(j_scf) , basis, "cube" );
        // micro SCF
        for (int i_scf=0; i_scf< max_iter_micro_scf; ++i_scf){

            internal_timer->start("Scf2 micro iteration cycle");
            // init in_state and out_state
//rcp(new Scf_State(*scf_states[scf_states.size()-1]));

            // scf_states[stats.size()-1] is idendical to in_state and out_state
            RCP<Scf_State> out_state = rcp(new Scf_State(*in_state));
            // remove orbitals and orbital energies of out_state
            out_state->orbitals.clear();
            out_state->orbital_energies.clear();
            
            for(int i_spin=0; i_spin < in_state->occupations.size();++i_spin){
                RCP<Epetra_Vector> local_potential = rcp(new Epetra_Vector( *(in_state->local_potential[i_spin])));
    
                this -> internal_timer -> start("Scf2::iterate::Hamiltonian projection");
                RCP<Epetra_MultiVector> VU = rcp( new Epetra_MultiVector(*basis->get_map(),orbital_space[i_spin]->NumVectors()) );
                for (int i =0; i< orbital_space[i_spin]->NumVectors(); i++ ){
                    // element-wise multiplication
                    VU->operator()(i)->Multiply(1.0, *local_potential, *orbital_space[i_spin]->operator()(i),0.0);
                }

                auto projected_local_potential = project(i_spin, VU ); 
                auto projected_hamiltonian_matrix = rcp ( new Epetra_MultiVector( *projected_core_hamiltonians[i_spin] ));
                // add core hamiltonian 
                blas.AXPY(dimension_of_reduced_space*dimension_of_reduced_space, 1.0, projected_local_potential->Values(), 1,  projected_hamiltonian_matrix->Values(),1);

                this -> internal_timer -> end("Scf2::iterate::Hamiltonian projection");
    
                // diagonalize
                internal_timer->start("Diagonalize");
                bool is_diag_success = direct_diagonalizer->diagonalize(dimension_of_reduced_space, dimension_of_reduced_space, projected_hamiltonian_matrix->Values() );
                internal_timer->end("Diagonalize");
                assert(is_diag_success);
                // back up orbitals 
                auto projected_eigenvectors = direct_diagonalizer->get_eigenvectors();
                auto projected_eigenvalues = direct_diagonalizer->get_eigenvalues();

                for (int q=0; q< projected_eigenvalues.size(); q++){
                    Verbose::single(Verbose::Detail) << "projected_eigenvalues["<< q<< "]: "<< projected_eigenvalues[q] << std::endl;
                }
                //exit(-1);
//                is_diag_success = direct_diagonalizer->diagonalize(dimension_of_reduced_space, dimension_of_reduced_space, projected_core_hamiltonians[0]->Values() );
//
//                projected_eigenvectors = direct_diagonalizer->get_eigenvectors();
//                projected_eigenvalues = direct_diagonalizer->get_eigenvalues();
//
//
//                for (int q=0; q< projected_eigenvalues.size(); q++){
//                    Verbose::single() << "projected_eigenvalues of core_hamiltonian[q]" <<  q << " "<< projected_eigenvalues[q] << std::endl;
//                }

                RCP<Epetra_MultiVector> recovered_eigenvectors = rcp(new Epetra_MultiVector(*basis->get_map() , projected_eigenvectors->NumVectors() ) );
                // (U V')  //V' =(projected eigenvectors)
                int info = recovered_eigenvectors->Multiply('N', 'N', 1.0, *orbital_space[i_spin], *projected_eigenvectors, 0.0);
                assert(info==0);
                
                // set eigenpairs on out_state
                out_state->orbitals.append(recovered_eigenvectors );
                out_state->orbital_energies.append( projected_eigenvalues );
                // check norm 
                double* norm = new double[ out_state->orbital_energies[out_state->orbital_energies.size()-1].size()];
                out_state->orbitals[out_state->orbital_energies.size()-1]->Norm2(norm);
                for(int i =0; i< out_state->orbital_energies[out_state->orbital_energies.size()-1].size(); i++){
                    assert(std::fabs(norm [i] - 1)<0.00001);
                }
                delete[] norm;
            }

            Density_From_Orbitals::compute_total_density(basis,out_state->occupations,out_state->orbitals,out_state->density);
            generate_local_potential(out_state);
            append_states(out_state);

            bool is_compute_energy_this_step = print_energy(j_scf+i_scf,out_state);

            in_state = generate_in_state();
            internal_timer->end("Scf2 micro iteration cycle");
            if(convergence_check(i_scf, is_compute_energy_this_step) ) {
                break;
            }
        }
//        filename = "after";
//        scf_states[scf_states.size()-1]->write_orbital(filename+String_Util::to_string(j_scf) , basis, "cube" );
//        filename = "micro_density";
//        filename = "density";
//        scf_states[scf_states.size()-1]->write_density(filename+String_Util::to_string(j_scf) , basis, "cube" , false);
        // generate initial eigenvectors for new orbital space 
        internal_timer->start("Scf::construct initial eigenvectors");
        
        for(int i_spin = 0; i_spin< occupation_size; i_spin++){
            orbital_space[i_spin]->operator=(*scf_states[scf_states.size()-1]->orbitals[i_spin] );
        }
        //exit(-1);
        internal_timer->end("Scf::construct initial eigenvectors");
//        bool is_compute_energy_this_step = print_energy(j_scf,out_state);
        bool is_compute_energy_this_step = print_energy(j_scf,in_state);
    }
    if(this -> is_final_diag){
        final_diagonalize(external_potential, Teuchos::null);
    }

    timer->end("Scf2::iterate");
    final_state = scf_states[scf_states.size()-1];
//    Verbose::single(Verbose::Simple) << *(final_state->get_occupations()[0]) <<std::endl;
    final_state->timer = timer;

    return return_val;
}


RCP<Epetra_MultiVector > Scf2::project(const int i_spin, RCP<Epetra_MultiVector> multivector){ 
    const int dimension_of_reduced_space = orbital_space[i_spin]->NumVectors();
    assert(dimension_of_reduced_space !=0);
    Epetra_LocalMap local_map ( dimension_of_reduced_space ,0, basis->get_map()->Comm() );
    RCP<Epetra_MultiVector > return_val = rcp(new Epetra_MultiVector(local_map,multivector->NumVectors()));
//    Verbose::single() << *orbital_space[i_spin] <<std::endl;
    int info  = return_val->Multiply('T', 'N', 1.0, *orbital_space[i_spin], *multivector, 0.0);
    assert(0==info);
//    Verbose::single() << "(" <<info << ") return_val " << *return_val <<std::endl;
    return return_val;
}

RCP<Scf_State> Scf2::update_orbital_space(RCP<Scf_State> in_state, Array<RCP<Epetra_Vector> > external_potential, Array<RCP<Epetra_MultiVector> > initial_vectors, Array<RCP<Epetra_MultiVector> >& new_orbital_space ){
    double norm;
    in_state->density[0]->Norm1(&norm);
    
    RCP<Scf_State> out_state = rcp( new Scf_State(*in_state));
    out_state->orbitals.clear();
    out_state->orbital_energies.clear();

    RCP<Epetra_CrsMatrix> hamiltonian_matrix;// = rcp(new Epetra_CrsMatrix(Copy, *basis -> get_map(), 0) );
    RCP<Epetra_Vector> diagonal_of_hamiltonian = rcp(new Epetra_Vector(*basis -> get_map()));
    for (int i_spin =0; i_spin< initial_vectors.size(); i_spin++){

        ////////////////  extract diagonal element of core hamiltonian ///////////
        core_hamiltonian->get_core_hamiltonian(i_spin,hamiltonian_matrix);
        hamiltonian_matrix -> ExtractDiagonalCopy( *diagonal_of_hamiltonian );
        diagonal_of_hamiltonian->Update(1.0,*in_state->local_potential[i_spin],1.0);
        diagonal_of_hamiltonian->Update(1.0,*external_potential[i_spin],1.0);
        hamiltonian_matrix->ReplaceDiagonalValues(*diagonal_of_hamiltonian);
        //std::cout << *hamiltonian_matrix <<std::endl;
        internal_timer->start("Diagonalize");
        // diagonalize
        bool is_diag_success = diagonalize->diagonalize(hamiltonian_matrix, initial_vectors[i_spin]->NumVectors(), initial_vectors[i_spin], Teuchos::null);
        internal_timer->end("Diagonalize");
        // set new orbital space
        new_orbital_space.append( diagonalize->get_eigenvectors() ) ;

        // set scf_state
        const int num_eigval = parameters->sublist("Scf").get<int>("NumberOfEigenvalues");
        std::vector<double> orbital_energy(num_eigval);
        RCP<Epetra_MultiVector> orbital = rcp(new Epetra_MultiVector(diagonalize->get_eigenvectors()->Map(), num_eigval));
        for(int i=0; i< num_eigval; i++){
            orbital_energy[i] = diagonalize->get_eigenvalues()[i];
            orbital->operator()(i)->operator=( *diagonalize->get_eigenvectors()->operator()(i) );
        }
        out_state->orbital_energies.append(orbital_energy);
        out_state->orbitals.append(orbital);
    }
    Density_From_Orbitals::compute_total_density(basis,out_state->occupations,out_state->orbitals,out_state->density);
    generate_local_potential(out_state);
    append_states(out_state);


    std::string filename = "after";
    scf_states[scf_states.size()-1]->write_orbital(filename , basis, "cube" );
    return generate_in_state();


//    // for debug
//    const int dimension_of_reduced_space = orbital_space[0]->NumVectors();
//    Epetra_LocalMap local_map ( dimension_of_reduced_space ,0, basis->get_map()->Comm() );
//
//    RCP<Direct_Diagonalize> direct_diagonalizer = rcp(new Direct_Diagonalize() );
//    for (int i_spin =0; i_spin< orbital_space.size(); i_spin++){
//        RCP<Epetra_MultiVector> tmp_multivector = rcp(new Epetra_MultiVector(*new_orbital_space[i_spin]));
//        core_hamiltonian->get_core_hamiltonian_matrix()->multiply(i_spin, new_orbital_space[i_spin], tmp_multivector );
//        auto matrix = rcp(new Epetra_MultiVector(local_map,tmp_multivector->NumVectors()));
//        matrix->Multiply('T', 'N', 1.0,*new_orbital_space[i_spin], *tmp_multivector, 0.0 );
//
//        // project local potential 
//        RCP<Epetra_MultiVector> VU = rcp( new Epetra_MultiVector(*basis->get_map(),tmp_multivector->NumVectors()) );
//        for (int i =0; i< orbital_space[i_spin]->NumVectors(); i++ ){
//            VU->operator()(i)->Multiply(1.0, *local_potential[i_spin], *new_orbital_space[i_spin]->operator()(i),0.0);
//        }
//        auto projected_local_potential = rcp(new Epetra_MultiVector(local_map,tmp_multivector->NumVectors()));
//        projected_local_potential->Multiply('T', 'N', 1.0, *new_orbital_space[i_spin], *VU ,0.0);
//
//        Teuchos::BLAS<int,double> blas;
//        blas.AXPY(dimension_of_reduced_space*dimension_of_reduced_space, 
//                    1.0, projected_local_potential->Values(), 1,
//                    matrix->Values(),1);
//
//        direct_diagonalizer->diagonalize(dimension_of_reduced_space, dimension_of_reduced_space, matrix->Values() );
//        
//        auto projected_eigenvectors = direct_diagonalizer->get_eigenvectors();
//        auto projected_eigenvalues = direct_diagonalizer->get_eigenvalues();
//
//        for (int q=0; q< projected_eigenvalues.size(); q++){
//            Verbose::single() << "tmp_projected_eigenvalues["<< q<< "]: "<< projected_eigenvalues[q] << std::endl;
//        }
//    }
//    // end 


}

//
int Scf2::project_core_hamiltonians(const int dimension_of_reduced_space, Array<RCP<Epetra_Vector> > external_potential){
    Epetra_LocalMap local_map ( dimension_of_reduced_space ,0, basis->get_map()->Comm() );
    projected_core_hamiltonians.clear();

    for (int i_spin =0; i_spin< orbital_space.size(); i_spin++){
        RCP<Epetra_MultiVector> tmp_multivector = rcp(new Epetra_MultiVector(*orbital_space[i_spin]));
        core_hamiltonian->get_core_hamiltonian_matrix()->multiply(i_spin, orbital_space[i_spin], tmp_multivector );
        for (int i =0; i<dimension_of_reduced_space; i++){
            tmp_multivector->operator()(i)->Multiply(1.0,*external_potential[i_spin],*orbital_space[i_spin]->operator()(i),1.0);
        }
        auto projected_matrix = project(i_spin, tmp_multivector);

        projected_core_hamiltonians.append(projected_matrix);
    }
    return 0 ;
}





//    //From now on, scf_states[scf_states.size()-1] should be modified
//    //mixing start!
//    this -> internal_timer -> start("Scf2::iterate::Mixing");
//    if(mixing->get_mixing_type() == "Potential"){
//        Verbose::single(Verbose::Normal) << std::endl << "#-------------------------------------------------------------- Scf2::iterate" << std::endl;
//        Verbose::single(Verbose::Normal) << "Mixing type is \"" <<  mixing->get_mixing_type() << "\"\n";
//        Verbose::single(Verbose::Normal) << "calculate xc and hartree potential of out_state " <<std::endl;
//        Verbose::single(Verbose::Normal) <<  "#---------------------------------------------------------------------------" << std::endl;
//    
//        this -> internal_timer -> start("Scf2::iterate::Mixing (potential update)");
//        generate_local_potential(scf_states[scf_states.size()-1]);
//        this -> internal_timer -> end("Scf2::iterate::Mixing (potential update)");
//    }
//    
//    for(int i_spin=0; i_spin<in_state->occupations.size(); i_spin++){
//        mixing->update(basis, scf_states, i_spin, core_hamiltonian -> get_core_hamiltonian_matrix() -> nuclear_potential);
//    }
//    // now, scf_states[stats.size()-1] becomes new in_state
//    in_state = scf_states[scf_states.size()-1];
//    Verbose::single(Verbose::Normal) << "occupation of input state" <<std::endl;
//    Verbose::single(Verbose::Normal) << * in_state->occupations[0] <<std::endl;
//    
//    if(mixing->get_mixing_type() == "Density" or mixing -> get_mixing_type() == "PAW_Density" or mixing->get_mixing_type() == "Orbital"){
//        Verbose::single()<<  std::endl << "#-------------------------------------------------------------- Scf2::iterate" << std::endl;
//        Verbose::single() << "Mixing type is \"" <<  mixing->get_mixing_type() << "\"\n";
//        Verbose::single() << "calculate xc and hartree potential of new in_state " <<std::endl;
//        Verbose::single() <<  "#---------------------------------------------------------------------------" << std::endl;
//    
//        this -> internal_timer -> start("Scf2::iterate::Mixing (potential update)");
//        generate_local_potential(in_state);
//        this -> internal_timer -> end("Scf2::iterate::Mixing (potential update)");
//        Verbose::single()<<  "out, generate_local_potential" << std::endl;
//    }
//    //mixing end!
//    //new in_state was made
//    this -> internal_timer -> end("Scf2::iterate::Mixing");
//    Verbose::single() << "Scf2::iterate::Mixing time: " << this -> internal_timer -> get_elapsed_time("Scf2::iterate::Mixing", -1) << "s" << std::endl;
//    return 0;
//}
