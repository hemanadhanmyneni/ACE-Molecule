#pragma once
#include <string>
#include <vector>

#include "Guess.hpp"
#include "../Basis/Basis.hpp"
#include "../Utility/EHT_parameter.hpp"

#include "Epetra_MultiVector.h"
#include "Teuchos_Array.hpp"
#include "Teuchos_RCP.hpp"

class Huckel: public Guess{
    public:
        Huckel(Teuchos::Array< Teuchos::RCP<Occupation> > occupations, Teuchos::RCP<const Atoms> atoms, std::string ehtpar_filename );
        int compute(Teuchos::RCP<const Basis> basis, Teuchos::Array< Teuchos::RCP<State> >& states);
        
    private:
        void extended_huckel();
        double get_overlap_integral(int i, int p, int j, int q);
        double local_integration_convergence_test(double local_val, double* x, int i, int p, int j, int q, double scaling_factor, double interval, int iter_num);
        double atomic_orbital(int i, int p, double* x_old); //good
        double get_hamiltonian(int i, int p, int n, int j, int q, int m); //good
        bool is_orbital_index_ended(int i, int p); //good

        int basis_size;
        std::vector<int> number_of_basis_function;
        std::vector< std::vector<double> > overlap_matrix;
        std::vector< std::vector<double> > hamiltonian;
        std::vector<double> energy; // E_i, smaller i -> lower energy
        std::vector< std::vector<double> > wavefunction; // wavefunction[i][j] : psi_i = sum( phi_j, j=1..mesh_size)
        Teuchos::RCP<EHT_parameter> param;
};
