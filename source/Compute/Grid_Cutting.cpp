#include "Grid_Cutting.hpp"
#include <string>
#include <sstream>
#include <vector>
#include <cmath>
#include <ctime>

#include "Scf.hpp"
#include "Create_Compute_Interface.hpp"
#include "../Basis/Create_Basis.hpp"
#include "Create_Compute.hpp"
#include "../Utility/Density/Density_Orbital.hpp"
#include "../Utility/Change_Indice.hpp"
#include "../Utility/String_Util.hpp"

using std::abs;
using std::endl;
using std::string;
using std::vector;
using Teuchos::Array;
using Teuchos::RCP;
using Teuchos::rcp;
using Teuchos::ParameterList;

Grid_Cutting::Grid_Cutting(Array<RCP<Occupation> > occupations, RCP<Teuchos::ParameterList> parameters, RCP<const Atoms> atoms){
    this->parameters = parameters;
    this->atoms = atoms;
    this->occupations = occupations;
}

int Grid_Cutting::compute(RCP<const Basis> basis, Array<RCP<State> >& states ){
    this -> internal_timer -> start("Grid_Cutting");

    if(this -> parameters -> sublist("Guess").isParameter("Radius")){
        this -> parameters -> sublist("Guess").get<string>("Grid", "Atoms");
    } else {
        this -> parameters -> sublist("Guess").get<double>("Ratio", 0.75);
    }

    RCP<Atoms> tmp_atoms = rcp(new Atoms());
    for(int k = 0; k < atoms -> get_size(); ++k){
        Atom tmp_atom =  Atom(atoms->operator[](k));
        tmp_atoms->push(tmp_atom);
    }

    RCP<ParameterList> GC_param = rcp(new ParameterList());
    if(parameters -> sublist("Guess").isParameter("Ratio")){
        GC_param -> sublist("BasicInformation") = ParameterList(this -> parameters -> sublist("BasicInformation"));
        double ratio = this -> parameters -> sublist("Guess").get<double>("Ratio");
        if(this -> parameters -> sublist("BasicInformation").get<string>("Grid") == "Atoms"){
            Array<string> radius = this -> parameters -> sublist("BasicInformation").get< Array<string> >("Radius");
            for(int i = 0; i < radius.size(); ++i){
                double new_radius = atof(radius[i].c_str())*ratio;
                radius[i] = String_Util::to_string(new_radius);
            }
            GC_param -> sublist("BasicInformation").set("Radius", radius);
        } else {
            GC_param -> sublist("BasicInformation").set("PointX",
                    static_cast<int>(this -> parameters -> sublist("BasicInformation").get<int>("PointX") * ratio));
            GC_param -> sublist("BasicInformation").set("PointY",
                    static_cast<int>(this -> parameters -> sublist("BasicInformation").get<int>("PointY") * ratio));
            GC_param -> sublist("BasicInformation").set("PointZ",
                    static_cast<int>(this -> parameters -> sublist("BasicInformation").get<int>("PointZ") * ratio));
        }
    } else {
        GC_param -> sublist("BasicInformation") = ParameterList(this -> parameters -> sublist("Guess"));
        GC_param -> sublist("BasicInformation").setParametersNotAlreadySet(this -> parameters -> sublist("BasicInformation"));
        //Verbose::all() << GC_param -> sublist("BasicInformation") << std::endl;
    }

    if(GC_param -> sublist("BasicInformation").sublist("Pseudopotential").get<int>("UsingDoubleGrid", 0) == 1){
        Verbose::single(Verbose::Simple) << "No doublegrid is used for Grid_Cutting step!" << std::endl;
        GC_param -> sublist("BasicInformation").sublist("Pseudopotential").set<int>("UsingDoubleGrid", 0);
    }
    if(GC_param -> sublist("BasicInformation").sublist("Pseudopotential").get<int>("UsingFiltering", 0) == 1){
        Verbose::single(Verbose::Simple) << "No filtering is used for Grid_Cutting step!" << std::endl;
        GC_param -> sublist("BasicInformation").sublist("Pseudopotential").set<int>("UsingFiltering", 0);
    }

    GC_param -> sublist("BasicInformation").set<int>("AllowOddPoints", 1);
    RCP<Basis> GC_basis = Create_Basis::Create_Basis(basis->get_map()->Comm(), Teuchos::sublist(GC_param, "BasicInformation"), tmp_atoms);
    RCP<ParameterList> GC_param_Guess = rcp(new ParameterList(*GC_param));
    if(this -> parameters -> sublist("Guess").isSublist("Guess")){
        GC_param_Guess -> sublist("Guess") = ParameterList(this -> parameters -> sublist("Guess").sublist("Guess"));
    } else {
        GC_param_Guess -> sublist("Guess").get<int>("InitialGuess", 0);
    }
    RCP<ParameterList> GC_param_Scf = rcp(new ParameterList(*GC_param));
    if(this -> parameters -> sublist("Guess").isSublist("Scf")){
        GC_param_Scf -> sublist("Scf") = ParameterList(this -> parameters -> sublist("Guess").sublist("Scf"));
    } else {
        GC_param_Scf -> sublist("Scf").get<double>("ConvergenceTolerance", 0.1);
        GC_param_Scf -> sublist("Scf").sublist("ExchangeCorrelation").get< Array<string> >("XFunctional", Array<string>(1, "1"));
    }

    GC_param_Guess -> setName("Guess");
    GC_param_Scf -> setName("Scf");

    Array<RCP<Compute> > computes;
    computes.append(Create_Compute_Interface::Create_Guess(atoms, GC_param_Guess, GC_basis));
    computes.append(Create_Compute_Interface::Create_DFT(GC_basis, GC_param_Scf));

    Array< RCP<State> > gc_states;
    if(states.size() > 0){
        gc_states.append(states.at(states.size()-1));
    }
    computes[0]->compute(GC_basis, gc_states);
    computes[1]->compute(GC_basis, gc_states);
    //RCP<State> new_state = states[states.size()-1];
    Size_Up(GC_basis, basis, gc_states);
    states.append(gc_states.at(gc_states.size()-1));

    // Hack to remove CH.
    Core_Hamiltonian::free_matrix();

    this -> internal_timer -> end("Grid_Cutting");
    return 0 ;
}

void Grid_Cutting::Size_Up( RCP<const Basis> basis1, RCP<const Basis> basis2,Array<RCP<State> >& states ){
    clock_t start_extrapolation_time, end_extrapolation_time;
    start_extrapolation_time = clock();
    //int size1 = basis1->get_original_size();
    int size2 = basis2->get_original_size();

    int NumMyElements1 = basis1->get_map()->NumMyElements();
    int* MyGlobalElements1 = basis1->get_map()->MyGlobalElements();
    int NumMyElements2 = basis2->get_map()->NumMyElements();
    int* MyGlobalElements2 = basis2->get_map()->MyGlobalElements();
    RCP<Change_Indice> change_indice = rcp(new Change_Indice(basis1, basis2));
    //RCP<State> new_state = rcp(new State(*states[states.size()-1]));
    RCP<State> new_state = rcp(new State());
    //new_state->density.clear();
    //new_state->orbitals.clear();
    for(int i =0;i <states[states.size()-1]->density.size(); i++){
        new_state->density.append(rcp(new Epetra_Vector(*basis2->get_map() ) ) );
    }
    for(int i =0;i <states[states.size()-1]->orbitals.size(); i++){
        new_state->orbitals.append(rcp(new Epetra_MultiVector(*basis2->get_map(), states[states.size()-1]->orbitals[i]->NumVectors() ) ) );
    }
    Verbose::single(Verbose::Detail) <<"density size up start" << endl;
    for(int spin = 0; spin<new_state->density.size(); spin++){
        double * combine_density = new double[size2];
        double * tmp_density = new double[size2];
        memset(tmp_density,0.0, sizeof(double)*size2);
        memset(combine_density,0.0, sizeof(double)*size2);
        for(int i=0; i<NumMyElements1; i++){
            int index = change_indice->change_indice(MyGlobalElements1[i]);
            if(index>=0)
                tmp_density[index] = states[states.size()-1]->density[spin]->operator[](i);
        }
        basis1->get_map()->Comm().SumAll(tmp_density, combine_density, size2);
        for(int i=0; i<NumMyElements2; i++){
            new_state->density[spin]->ReplaceGlobalValue(MyGlobalElements2[i], spin, combine_density[MyGlobalElements2[i]]);
        }
        delete [] tmp_density;
        delete [] combine_density;
    }
    Verbose::single(Verbose::Detail) << "orbital size up start " << endl;
    for(int spin = 0; spin<new_state->orbitals.size(); spin++){
        for(int j = 0; j<new_state->orbitals[spin]->NumVectors(); j++){
            double *  combine_orbital = new double[size2];
            double *  tmp_orbital = new double[size2];
            memset(tmp_orbital,0.0, sizeof(double)*size2);
            memset(combine_orbital,0.0, sizeof(double)*size2);
            for(int i=0; i<NumMyElements1; i++){
                int index = change_indice->change_indice(MyGlobalElements1[i]);
                if(index>=0){
                    tmp_orbital[index] = states[states.size()-1]->orbitals[spin]->operator[](j)[i];
                }
            }
            basis1->get_map()->Comm().SumAll(tmp_orbital, combine_orbital, size2);
            for(int i=0; i<NumMyElements2; i++){
                new_state->orbitals[spin]->operator[](j)[i] = combine_orbital[MyGlobalElements2[i]];
            }
            delete [] combine_orbital;
            delete [] tmp_orbital;
        }
    }
    new_state->occupations.clear();
    new_state->core_density.clear();
    new_state->core_density_grad.clear();
    new_state->orbital_energies.clear();
    for (int i_spin=0; i_spin<new_state->density.size(); i_spin++){
        new_state->occupations.append(occupations[i_spin]);
        new_state->core_density.push_back(rcp(new Epetra_Vector(*basis2->get_map())));
        new_state->core_density_grad.push_back(rcp(new Epetra_MultiVector(*basis2->get_map(), 3)));
        //new_state->orbital_energies.push_back(vector<std::complex<double> >());
        new_state->orbital_energies.push_back(vector<double >());
        for(int i=0; i<occupations[i_spin]->get_size(); i++){
            //std::complex<double> tmp(0.0,0.0);
            //new_state->orbital_energies[i_spin].push_back(tmp);
            double tmp = 0.0;
            new_state->orbital_energies[i_spin].push_back(tmp);
        }
    }
    new_state->set_atoms(states[states.size()-1]->atoms);
    for(int i=0; i<new_state->local_potential.size(); i++){
        new_state->local_potential[i] = rcp(new Epetra_Vector( *basis2->get_map()  ) );
    }
    states.append(new_state);
    end_extrapolation_time = clock();
    Verbose::single(Verbose::Detail) << "=============================" << endl;
    Verbose::single(Verbose::Detail) << "Grid_Cutting extrapolation end" << endl ;
    Verbose::single(Verbose::Normal) << "Grid_Cutting extrapolation time = " << double((end_extrapolation_time-start_extrapolation_time))/CLOCKS_PER_SEC << " s" << endl;
    Verbose::single(Verbose::Detail) << "=============================" << endl;
}
