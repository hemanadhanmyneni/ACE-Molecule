#include "Atomic_Density.hpp"
#include <cmath>

#include "../Utility/ACE_Config.hpp"
#include "../Utility/Read/Read_Upf.hpp"
#include "../Utility/Density/Density_Orbital.hpp"
#include "../Utility/Interpolation/Spline_Interpolation.hpp"
#include "../Utility/Interpolation/Linear_Interpolation.hpp"

#ifdef ACE_HAVE_OMP
#include "omp.h"
#endif

using std::abs;
using std::vector;
using std::string;
using Teuchos::rcp;
using Teuchos::Array;
using Teuchos::RCP;
using Teuchos::rcp;

Atomic_Density::Atomic_Density(Array< RCP<Occupation> > occupations, RCP<const Atoms> atoms, Array<string> upf_filenames ){
    this->occupations = occupations;
    this->atoms = atoms;
    this->upf_filenames = upf_filenames;
}

//Teuchos::Array<Teuchos::RCP<Epetra_MultiVector> > Atomic_Density::compute(Teuchos::Array<Occupation* > occupations, Teuchos::RCP<Epetra_MultiVector> atomic_density){
int Atomic_Density::compute(RCP<const Basis> basis,Array< RCP<State> >& states){

    Verbose::single(Verbose::Normal)<< "#-------------------------------------------------------- Atomic_Density::compute" << std::endl;
    Verbose::single(Verbose::Normal)<< "Linear interpolation will be used " << std::endl;
    initialize_states(basis,states);
    RCP<State> state = states[states.size()-1];
    vector<std::array<double,3> > position=atoms->get_positions();

    //Array< RCP<Epetra_MultiVector> > orbitals;
    RCP<const Epetra_Map> map= basis->get_map();
    int NumMyElements = map->NumMyElements();
    int* MyGlobalElements= map->MyGlobalElements();
    double* density=new double [NumMyElements]();
/*
    for(int i=0;i<NumMyElements;i++){
        density[i]=0.0;
    }
*/
    double total_Z =0.0;
    double xi,yi,zi,x,y,z,rr;
    std::string upf_filename;
    std::vector<double> radial_mesh;
    std::vector<double> rho;

    std::vector<int> elements;
    for(int iatom=0;iatom<atoms->get_size();iatom++){
        double Zval_tmp = 0.0;
        int number_vps = 0, number_pao = 0, mesh_size_tmp = 0;  // dummy value
        bool core_correction = false;                           // dummy value

        // Find corresponding pseudopotential file
        for(int i=0;i<atoms->get_num_types();i++){
            if(atoms->get_atomic_numbers()[iatom] == atoms->get_atom_types()[i]){
                upf_filename=upf_filenames[i];
                break;
            }
        }
        // end

        // Get number of valence electron
        Read::Upf::read_header(upf_filename, &Zval_tmp, &number_vps, &number_pao, &mesh_size_tmp, &core_correction);
        total_Z+=Zval_tmp;
        //end


        // Read density
        rho.clear();
        rho = Read::Upf::read_upf(upf_filename, "PP_RHOATOM", false);

        while(abs(rho[rho.size()-1]) < 1.0E-10){
            rho.pop_back();
        }
        
        // Read radial_mesh points
        radial_mesh.clear();

        radial_mesh = Read::Upf::read_upf(upf_filename, "PP_R", false);

        while(radial_mesh[0] < 1.0E-30){
            radial_mesh.erase(radial_mesh.begin());
            rho.erase(rho.begin());
        }

        if(rho.size() > radial_mesh.size()){
            Verbose::all() << "Atomic_Density rho.size() > radial_mesh.size()" << rho.size() << " vs. " << radial_mesh.size() << std::endl;
            exit(EXIT_FAILURE);
        }
        while (rho.size() < radial_mesh.size()){
            radial_mesh.pop_back();
        }
        Verbose::single(Verbose::Detail)<<"RHOATOM cutoff= " << radial_mesh[rho.size()-1]<<"\n";
        // end

        // From the manual of Quantum Espresso, PP_RHOATOM in *.upf is
        // radial atomic (pseudo-)charge. This is 4*pi*r^2 times the true charge.
        // Therefore, this code saves the value divided by 4*pi*r^2.
        #ifdef ACE_HAVE_OMP
        #pragma omp parallel for
        #endif
        for(int i=0;i<rho.size();i++){
            rho[i] /= (4*M_PI*radial_mesh[i]*radial_mesh[i]);
        }
        /*
        // insert value when r=0
        radial_mesh.insert(radial_mesh.begin(),0.0);

        // linear extrapolation at r=0
        double tmp;
        tmp=(rho[1]-rho[0])/(radial_mesh[2]-radial_mesh[1])*(radial_mesh[0]-radial_mesh[1])+rho[0];
        rho.insert(rho.begin(),tmp);
        */
        /*
           if(MyPID == 0 and iatom == 1){
           cout << "Atomic_Density from *.upf file" << endl;
           for(int k=0;k<rho.size();k++){
           cout << radial_mesh[k] << "\t" << rho[k] << endl;
           }
           exit(EXIT_FAILURE);
           }
           */
        // end

        // Spline interpolation
        int rho_size = rho.size();
        double yp1 = (rho[1]-rho[0])/(radial_mesh[1]-radial_mesh[0]);
        double ypn = (rho[rho_size-1]-rho[rho_size-2])/(radial_mesh[rho_size-1]-radial_mesh[rho_size-2]);
        vector<double> y2 = Interpolation::Spline::spline(radial_mesh, rho, rho_size, yp1, ypn);
#ifdef ACE_HAVE_OMP
#pragma omp parallel for private(xi,yi,zi,x,y,z,rr)
#endif
        for(int i=0;i<NumMyElements;i++){
            // (PBC)
            basis -> find_nearest_displacement(MyGlobalElements[i], position[iatom][0], position[iatom][1], position[iatom][2], x, y, z, rr);

            /*
            // Distance between the grid point & atom
            x=xi-position[iatom][0];
            y=yi-position[iatom][1];
            z=zi-position[iatom][2];
            rr=sqrt(x*x+y*y+z*z);
            */
            if(rr<=radial_mesh[radial_mesh.size()-1]){
                density[i] = Interpolation::Spline::splint(radial_mesh, rho, y2, rho_size, rr);
                //density[i] = Interpolation::Linear::linear_interpolate(rr, radial_mesh, rho);
            }
            else{
                density[i] = 0;
            }
        }

    }

    for(int i =0;i <occupations.size() ; i++){
        state->density.append(rcp(new Epetra_Vector(*basis->get_map() ) ) );
    }
    double num_electron = 0.0;   //total number of electron in this system
    if(occupations.size()==1){
        num_electron = occupations[0]->get_total_occupation();
        #ifdef ACE_HAVE_OMP
        #pragma omp parallel for
        #endif
        for(int i=0;i<NumMyElements;i++){
            state->density[0]->ReplaceGlobalValue(MyGlobalElements[i], 0, density[i]);
        }
    }
    else if(occupations.size()==2){
        double spin_up = occupations[0]->get_total_occupation();
        double spin_down = occupations[1]->get_total_occupation() ;
        num_electron = spin_up+spin_down;
        #ifdef ACE_HAVE_OMP
        #pragma omp parallel for
        #endif
        for(int i=0;i<NumMyElements;i++){
            state->density[0]->ReplaceGlobalValue(MyGlobalElements[i], 0, density[i] * spin_up / num_electron);
            state->density[1]->ReplaceGlobalValue(MyGlobalElements[i], 0, density[i] * spin_down / num_electron);
        }
    }

    // correct non-neutral system
    for (int i_spin = 0; i_spin < occupations.size() ; i_spin++){
        state->density[i_spin]->Scale(num_electron/total_Z);
    }
    // end
    state->orbitals = Density_Orbital::compute_density_orbitals(basis,state->density,occupations);
    delete[] density;
    Verbose::single(Verbose::Normal)<< "#--------------------------------------------------------------------------------" << std::endl;


    return 0;
}
