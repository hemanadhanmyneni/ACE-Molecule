#include "Guess.hpp"

using std::vector;
using Teuchos::rcp;
using Teuchos::Array;
using Teuchos::RCP;


/*
int Guess::set_occupations(Array<RCP<Occupation> > occupations){
    this->occupations=occupations;
    return 0;
}
int Guess::set_atoms(RCP<Atoms> atoms){
    this->atoms = atoms;
    return 0;
}
*/

int Guess::initialize_states(RCP<const Basis> basis, Array<RCP<State> >& states){
    internal_timer->start("Guess::initialize_states");
    RCP<const Epetra_Map> map = basis->get_map();
    if (states.size()==0){
        states.append(rcp(new State()));
        for (int i_spin=0; i_spin<occupations.size(); i_spin++){
            states[0]->occupations.append(occupations[i_spin]);
            states[0]->core_density.push_back(rcp(new Epetra_Vector(*map)));
            states[0]->core_density_grad.push_back(rcp(new Epetra_MultiVector(*map, 3)));
            states[0]->orbital_energies.push_back(vector<double >());
            for(int i=0; i<occupations[i_spin]->get_size(); i++){
                double tmp = 0.0;
                states[0]->orbital_energies[i_spin].push_back(tmp);
            }
        }
        states[0]->set_atoms(rcp(new Atoms(*atoms)));
    }
    else{
        states.append(rcp(new State(*states[states.size()-1] ) ));
        if(states[states.size()-1]->atoms!=atoms){
            Verbose::single(Verbose::Simple) << "Scf::Warning! A set of atoms of states is overwritten. Please check molecular geometry of state or Scf" <<std::endl;
            states[states.size()-1]->set_atoms(rcp(new Atoms(*atoms)));
        }
        if(states[states.size()-1]->occupations.size()==0){
            for (int i=0;i<occupations.size();i++){
                states[states.size()-1]->occupations.append(occupations[i]);
            }
        }
    }
    internal_timer->end("Guess::initialize_states");
    return 0;
}
