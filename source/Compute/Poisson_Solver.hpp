#pragma once


#include "Teuchos_RCP.hpp"
#include "Teuchos_Array.hpp"

#include "../Basis/Basis.hpp"
#include "../State/State.hpp"
#include "Compute.hpp"

#include "../Utility/Kernel_Integral.hpp"

//#include "../../libcerf-1.4/include/cerf.h"

/**
This class solves Electronic Poisson Equation. ISF method is used.
*/

class Poisson_Solver: public Compute, public Kernel_Integral{
    public:
        /// This function compute linear equation. The first argument is density vector and the second potential vector. This function use gaussian background charge.
        virtual int compute(Teuchos::RCP<const Basis> basis, Teuchos::Array< Teuchos::RCP<State> >& states);
        using Kernel_Integral::compute;

//        /**
//        @brief Constructor
//        @author Kwangwoo Hong, Sunghwan Choi
//        @date 2013-12-24
//        @param basis basis information
//        @param t_i - (In) The starting point for t sampling range
//        @param t_l - (In)  The point that seperate the first and second range on t space
//        @param t_f - (In)  The endpoint for t sampling range
//        @param num_points1 -(In) The number of sampling points on the first range [t_i, t_l]
//        @param num_points2 -(In) The number of sampling points on the second range [t_l, t_f]
//        @param asymtotic_correction - (In) Use asymtotic correction term by Sundholm (JCP 132, 024102, 2010)
//        */
        Poisson_Solver(Teuchos::RCP<const Basis> basis,double t_i, double t_l, double t_f, int num_points1, int num_points2, bool asymtotic_correction = true, int ngpus = 0 );
        virtual std::string get_info_string() const;
    protected:
        virtual void t_sampling();
};
