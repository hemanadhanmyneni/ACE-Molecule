#pragma once
//#include <time.h>
#include "Epetra_MultiVector.h"
#include "Teuchos_Array.hpp"
#include "Teuchos_RCP.hpp"

#include "Guess.hpp"
#include "../Basis/Basis.hpp"
#include "../Io/Atoms.hpp"
#include "../Core/Occupation/Occupation.hpp"
#include "../State/State.hpp"

//class State;

class Cube: public Guess{
  public:
    Cube(
        Teuchos::Array< Teuchos::RCP<Occupation> > occupations, 
        Teuchos::RCP<const Atoms> atoms, 
        Teuchos::Array<std::string> cube_filenames,
        bool is_bohr = true
    );
    Cube(
        Teuchos::Array< Teuchos::RCP<Occupation> > occupations, 
        Teuchos::RCP<const Atoms> atoms, 
        Teuchos::Array<std::string> cube_filenames, 
        std::string info_filename,
        std::string info_filetype, 
        bool is_bohr = true
    );
    int compute(
        Teuchos::RCP<const Basis> basis, 
        Teuchos::Array< Teuchos::RCP<State> >& states
    );

  protected:
    //void initialize();
    Cube(){};
    Teuchos::Array<std::string> cube_filenames;

    bool is_info;
    bool is_bohr;
    std::string info_filename;
    std::string info_filetype;

    //string cube_filename;
    //vector<double> radial_mesh;
    //vector<double> rho;
    //double Zval;
    //bool type_new;

    //RCP<Epetra_Vector> atomic_hartree;
};

