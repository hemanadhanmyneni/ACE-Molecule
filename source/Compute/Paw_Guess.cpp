#include "Paw_Guess.hpp"
#include <vector>
#include <cmath>
//#include <algorithm>
//#include <utility>

#include "../Utility/Read/Read_Paw.hpp"
#include "../Utility/Density/Density_Orbital.hpp"
#include "../Utility/Math/Spherical_Harmonics.hpp"
#include "../Utility/Interpolation/Spline_Interpolation.hpp"
#include "../Utility/Interpolation/Radial_Grid_Paw.hpp"
//#include "../Utility/Verbose.hpp"

using Teuchos::Array;
using Teuchos::RCP;
using Teuchos::rcp;
using std::string;
using std::vector;
using Read::Read_Paw;

Paw_Guess::Paw_Guess( Array< RCP<Occupation> > occupations, RCP<const Atoms> atoms, Array<string> paw_filenames){
    this -> occupations = occupations;
    this -> atoms = atoms;
    this -> paw_filenames = paw_filenames;
}

int Paw_Guess::compute( RCP<const Basis> basis, Array< RCP< State> > &states){
    initialize_states( basis, states );
    RCP<State> state = states[states.size()-1];
    state -> timer -> start("PAW Guess");

    int retval;
    if( this -> atoms -> get_size() == 1 ){
        retval = this -> one_atom_initialize( basis, state );
    } else {
        retval = this -> many_atom_initialize( basis, state );
    }

    /*
    double ps_charge = 0.0;
    double scaling = basis -> get_scaling()[0] * basis -> get_scaling()[1] * basis -> get_scaling()[2];
    RCP<Epetra_Vector> unitvect = rcp( new Epetra_Vector( *basis -> get_map(), false ) );
    unitvect -> PutScalar(1.0);
    for(int s = 0; s < state -> density.size(); ++s){
        double tmp;
        state -> density[s] -> Dot( *unitvect, &tmp );
        ps_charge += tmp;
    }
    Verbose::single(Verbose::Detail) << "PAW Guess charge = " << ps_charge*scaling << std::endl;
    */
    state -> timer -> end("PAW Guess");
    return retval;
}

int Paw_Guess::one_atom_initialize( RCP<const Basis> basis, RCP<State> &state ){
    vector<std::array<double,3> > position=atoms->get_positions();
    int spin_size = occupations.size();
    int total_occupation = 0;
    RCP<const Epetra_Map> map = basis -> get_map();

    vector<double> spin_num(spin_size);
    for(int s = 0; s < spin_size; ++s){
        spin_num[s] = occupations[s] -> get_total_occupation();
        total_occupation += spin_num[s];
    }

    Array< RCP<Epetra_MultiVector> > wavefunctions;
    Array< RCP<Epetra_Vector> > atomic_density;
    for(int alpha=0; alpha<spin_size ;alpha++){
        wavefunctions.append( rcp(new Epetra_MultiVector( *map,occupations[alpha]->get_size()) )  );
        atomic_density.append( rcp(new Epetra_Vector( *map ) )  );
    }
    int orb_index = 0;

    string paw_filename = paw_filenames[0];

    // Read PAW setup file
    RCP<Read_Paw> read_paw = rcp( new Read_Paw(paw_filename, true) );
    vector<string> pw_types = read_paw -> get_partial_wave_types();
    // end

    // Sort PAW partial wave states
    vector<int> ipw_list;
    for(int ipw = 0; ipw < pw_types.size(); ++ipw ){
        ipw_list.push_back(ipw);
    }
    std::sort( ipw_list.begin(), ipw_list.end(), [&](int i, int j)->bool{return read_paw->get_partial_wave_state(i).energy<read_paw->get_partial_wave_state(j).energy;} );

    int file_occupation = 0;
    vector<int> occupation_list;
    for(int ipw = 0; ipw < pw_types.size(); ++ipw ){
        occupation_list.push_back( read_paw -> get_partial_wave_state(ipw).occupation_number );
        file_occupation += occupation_list[ipw];
    }

    // Consider charge
    if( file_occupation > total_occupation ){
        for(int i1 = occupation_list.size()-1; i1 >= 0; --i1){
            int i = ipw_list[i1];
            while( occupation_list[i] > 0 and file_occupation > total_occupation ){
                --file_occupation;
                --occupation_list[i];
            }
        }
    } else if( file_occupation < total_occupation ){
        for(int i1 = 0; i1 < occupation_list.size(); ++i1){
            int i = ipw_list[i1];
            int l = read_paw -> get_partial_wave_state(i).l;
            while( occupation_list[i] < 4*l+2 and file_occupation < total_occupation ){
                ++file_occupation;
                ++occupation_list[i];
            }
        }
    }

    // Read PAW PS partial waves
    //for(int ipw = 0; ipw < pw_types.size(); ++ipw ){
    for(int ipw2 = 0; ipw2 < pw_types.size(); ++ipw2 ){
        int ipw = ipw_list[ipw2];
        Read::PAW_STATE pw_info = read_paw -> get_partial_wave_state(ipw);
        vector<double> pw = read_paw -> get_smooth_partial_wave_r(ipw).second;
        vector<double> grid = read_paw -> get_grid( read_paw -> get_smooth_partial_wave_r(ipw).first );

        //if( pw_info.occupation_number < 1 ) continue;
        if( occupation_list[ipw] < 1 ) continue;
        int l = pw_info.l;
        for( int m = -l; m <= l; ++m){
        //if( occupation_list[ipw] < 1 and std::max(occupations[0] -> get_size(), occupations[spin_size-1] -> get_size()) < orb_index) continue;
            RCP<Epetra_Vector> pw_val = rcp( new Epetra_Vector( *map ) );
            Radial_Grid::Paw::Interpolate_from_radial_grid( l, m, pw, grid, position[0], basis, pw_val);

            RCP<Epetra_Vector> pwpw = rcp( new Epetra_Vector( *map, false ) );
            pwpw -> Multiply(1.0, *pw_val, *pw_val, 0.0);

            RCP<Epetra_Vector> pw_coeff = rcp( new Epetra_Vector( *map ) );
            Radial_Grid::Paw::get_basis_coeff_from_radial_grid( l, m, pw, grid, position[0], basis, pw_coeff);

            if( spin_size == 1 ){
                if( orb_index >= occupations[0] -> get_size() ){
                    break;
                }
                if( occupation_list[ipw] < 1 ){
                    // Unfilled. Just orbitals.
                    wavefunctions[0] -> operator()(orb_index) -> Update( 1.0, *pw_coeff, 0.0 );
                    ++orb_index;
                } else if( occupation_list[ipw] >= 4*l+2 ){
                    // Fully filled
                    atomic_density[0] -> Update( 2.0, *pwpw, 1.0 );
                    wavefunctions[0] -> operator()(orb_index) -> Update( 1.0, *pw_coeff, 0.0 );
                    ++orb_index;
                } else if (occupation_list[ipw] < 4*l+2 ){
                    // Partially filled
                    if( l+m <= occupation_list[ipw]/2 ){
                        atomic_density[0] -> Update( 1.0, *pwpw, 1.0 );
                        wavefunctions[0] -> operator()(orb_index) -> Update( 1.0, *pw_coeff, 0.0 );
                        ++orb_index;
                    }
                }
            } else if( spin_size == 2 ){
                if( orb_index >= std::min(occupations[0] -> get_size(), occupations[1] -> get_size()) ){
                    break;
                }
                if( occupation_list[ipw] < 1 ){
                    // Unfilled. Just orbitals.
                    wavefunctions[0] -> operator()(orb_index) -> Update( 1.0, *pw_coeff, 0.0 );
                    wavefunctions[1] -> operator()(orb_index) -> Update( 1.0, *pw_coeff, 0.0 );
                    ++orb_index;
                } else if( occupation_list[ipw] >= 4*l+2 ){
                    // Fully filled
                    atomic_density[0] -> Update( 1.0, *pwpw, 1.0 );
                    atomic_density[1] -> Update( 1.0, *pwpw, 1.0 );
                    wavefunctions[0] -> operator()(orb_index) -> Update( 1.0, *pw_coeff, 0.0 );
                    wavefunctions[1] -> operator()(orb_index) -> Update( 1.0, *pw_coeff, 0.0 );
                    ++orb_index;
                } else if (occupation_list[ipw] < 2*l+1 ){
                    // Partially filled. Only alpha spins filled.
                    if( l+m < occupation_list[ipw] ){
                        atomic_density[0] -> Update( 1.0, *pwpw, 1.0 );
                        wavefunctions[0] -> operator()(orb_index) -> Update( 1.0, *pw_coeff, 0.0 );
                        wavefunctions[1] -> operator()(orb_index) -> Update( 1.0, *pw_coeff, 0.0 );
                        ++orb_index;
                    }
                } else if( occupation_list[ipw] >= 2*l+1 && occupation_list[ipw] < 4*l+2 ){
                    // Partially filled. Alpha spins are fully filled and beta spins are partially filled.
                    atomic_density[0] -> Update( 1.0, *pwpw, 1.0 );
                    wavefunctions[0] -> operator()(orb_index) -> Update( 1.0, *pw_coeff, 0.0 );
                    if( l+m < occupation_list[ipw] - (2*l+1) ){
                        atomic_density[1] -> Update( 1.0, *pwpw, 1.0 );
                    }
                    wavefunctions[1] -> operator()(orb_index) -> Update( 1.0, *pw_coeff, 0.0 );
                    ++orb_index;
                }
            }
        }
    }

    /*
    // Use smooth valence density for initial density.
    vector<double> ps_density = read_paw -> get_smooth_valence_density_r().second;
    vector<double> grid = read_paw -> get_grid( read_paw -> get_smooth_valence_density_r().first );
    RCP<Epetra_Vector> density_val = rcp( new Epetra_Vector( *map ) );
    Radial_Grid::Interpolate_from_radial_grid( 0, 0, ps_density, grid, position[0], basis, density_val);
    if( spin_size == 1 ){
        atomic_density[0] -> Update( 1.0, *density_val, 0.0 );
    } else if( spin_size == 2 ){
        double spin_up = occupations[0] -> get_total_occupation();
        double spin_down = occupations[1] -> get_total_occupation();
        atomic_density[0] -> Update( spin_up/(spin_up+spin_down), *density_val, 0.0 );
        atomic_density[1] -> Update( spin_up/(spin_up+spin_down), *density_val, 0.0 );
    }
    */

    state -> density = atomic_density;
    state -> orbitals = wavefunctions;

    return 0;
}

int Paw_Guess::many_atom_initialize( RCP<const Basis> basis, RCP<State> &state ){
    int number_atom=atoms->get_size();
    vector<std::array<double,3> > position=atoms->get_positions();
    int spin_size = occupations.size();
    RCP<const Epetra_Map> map = basis -> get_map();

    vector<double> spin_num(spin_size);
    for(int s = 0; s < spin_size; ++s){
        spin_num[s] = occupations[s] -> get_total_occupation();
    }

    Array< RCP<Epetra_MultiVector> > wavefunctions;
    Array< RCP<Epetra_Vector> > atomic_density;
    for(int alpha=0; alpha<occupations.size() ;alpha++){
        wavefunctions.append( rcp(new Epetra_MultiVector( *map, occupations[alpha]->get_size()) )  );
        atomic_density.append( rcp(new Epetra_Vector( *map ) )  );
    }

    int file_occupation = 0;

    Array< RCP<Read_Paw> > read_paw_list;
    for(int i = 0; i < atoms -> get_num_types(); ++i){
        read_paw_list.append( rcp( new Read_Paw(paw_filenames[i], true) ) );
    }

    vector<int> tmp_occupation(spin_size);

    for(int iatom=0;iatom<number_atom;iatom++){
        // Find corresponding pseudopotential file
        RCP<Read_Paw> read_paw;
        for(int i=0;i<atoms->get_num_types();i++){
            if(atoms->get_atomic_numbers()[iatom] == atoms->get_atom_types()[i]){
                //read_paw = rcp( new Read_Paw(paw_filenames[i], true) );
                read_paw = read_paw_list[i];
                break;
            }
        }
        // end

        vector<string> pw_types = read_paw -> get_partial_wave_types();
        vector<int> occupation_list;
        for(int ipw = 0; ipw < pw_types.size(); ++ipw ){
            occupation_list.push_back( read_paw -> get_partial_wave_state(ipw).occupation_number );
            file_occupation += occupation_list[ipw];
        }

        // Sort PAW partial wave states
        vector<int> ipw_list;
        for(int ipw = 0; ipw < pw_types.size(); ++ipw ){
            ipw_list.push_back(ipw);
        }
        // XXX is for necessary here?
        for(int ipw = 0; ipw < pw_types.size(); ++ipw ){
            std::sort( ipw_list.begin(), ipw_list.end(), [&](int i, int j)->bool{return read_paw->get_partial_wave_state(i).energy<read_paw->get_partial_wave_state(j).energy;} );
        }
        for(int ipw2 = 0; ipw2 < pw_types.size(); ++ipw2 ){
            int ipw = ipw_list[ipw2];
            Read::PAW_STATE pw_info = read_paw -> get_partial_wave_state(ipw);
            vector<double> pw = read_paw -> get_smooth_partial_wave_r(ipw).second;
            vector<double> grid = read_paw -> get_grid( read_paw -> get_smooth_partial_wave_r(ipw).first );

            //if( pw_info.occupation_number < 1 ) continue;
            if( occupation_list[ipw] < 1 ) continue;
            int l = pw_info.l;
            for( int m = -l; m <= l; ++m){
                RCP<Epetra_Vector> pw_val = rcp( new Epetra_Vector( *map ) );
                Radial_Grid::Paw::Interpolate_from_radial_grid( l, m, pw, grid, position[iatom], basis, pw_val);

                RCP<Epetra_Vector> pwpw = rcp( new Epetra_Vector( *map, false ) );
                pwpw -> Multiply(1.0, *pw_val, *pw_val, 0.0);

                if( spin_size == 1 ){
                    if( occupation_list[ipw] >= 4*l+2 ){
                        // Fully filled
                        atomic_density[0] -> Update( 2.0, *pwpw, 1.0 );
                        tmp_occupation[0] += 2;
                    } else if (occupation_list[ipw] < 4*l+2 ){
                        // Partially filled
                        if( l+m <= occupation_list[ipw]/2 ){
                            atomic_density[0] -> Update( 2.0, *pwpw, 1.0 );
                            tmp_occupation[0] += 2;
                        }
                    }
                } else if( spin_size == 2 ){
                    if( occupation_list[ipw] >= 4*l+2 ){
                        // Fully filled
                        atomic_density[0] -> Update( 1.0, *pwpw, 1.0 );
                        atomic_density[1] -> Update( 1.0, *pwpw, 1.0 );
                        tmp_occupation[0] += 1;
                        tmp_occupation[1] += 1;
                    } else if (occupation_list[ipw] < 2*l+1 ){
                        // Partially filled. Only alpha spins filled.
                        if( l+m < occupation_list[ipw] ){
                            atomic_density[0] -> Update( 1.0, *pwpw, 1.0 );
                            tmp_occupation[0] += 1;
                        }
                    } else if( occupation_list[ipw] >= 2*l+1 && occupation_list[ipw] < 4*l+2 ){
                        // Partially filled. Alpha spins are fully filled and beta spins are partially filled.
                        atomic_density[0] -> Update( 1.0, *pwpw, 1.0 );
                        tmp_occupation[0] += 1;
                        if( l+m < occupation_list[ipw] - (2*l+1) ){
                            atomic_density[1] -> Update( 1.0, *pwpw, 1.0 );
                            tmp_occupation[1] += 1;
                        }
                    }
                }
            }
        }
        /*
        // Read PAW Valence density
        vector<double> ps_density = read_paw -> get_smooth_valence_density_r().second;
        vector<double> grid = read_paw -> get_grid( read_paw -> get_smooth_valence_density_r().first );
        RCP<Epetra_Vector> density_val = rcp( new Epetra_Vector( *map ) );
        Radial_Grid::Interpolate_from_radial_grid( 0, 0, ps_density, grid, position[iatom], basis, density_val);

        if( spin_size == 1 ){
            atomic_density[0] -> Update( 1.0, *density_val, 0.0 );
        } else if( spin_size == 2 ){
            double spin_up = occupations[0] -> get_total_occupation();
            double spin_down = occupations[1] -> get_total_occupation();
            atomic_density[0] -> Update( spin_up/(spin_up+spin_down), *density_val, 0.0 );
            atomic_density[1] -> Update( spin_up/(spin_up+spin_down), *density_val, 0.0 );
        }
        */
    }

    //double spacing = basis -> get_scaling()[0] * basis -> get_scaling()[1] * basis -> get_scaling()[2];
    // I don't know why this correctly works. -- KSW
    if(occupations.size() == 1 and occupations[0] -> get_total_occupation() > 0.0){
        //double norm;
        //atomic_density[0] -> Norm2(&norm);
        atomic_density[0] -> Scale(occupations[0] -> get_total_occupation() / tmp_occupation[0]);
        Verbose::single(Verbose::Normal) << "PAW Guess scale factor = " << occupations[0] -> get_total_occupation() / tmp_occupation[0] << std::endl;
    }
    /*
    // This routine is seriously broken for polarized array of 1-e system (i.e. Li89)
    for(int s = 0; s < spin_size; ++s){
        double spin_curr = occupations[s] -> get_total_occupation();
        if( spin_curr > 0.0 ){
            atomic_density[s] -> Scale( spin_curr / tmp_occupation[s] );
        }
        Verbose::single() << "PAW GUESS spin_curr = " << spin_curr << ", " << tmp_occupation[s] << std::endl;
    }
    */
    // I don't know why this correctly works. --KSW
    if(occupations.size() == 2){
        RCP<Epetra_Vector> tot_atomic_density = rcp(new Epetra_Vector(*map, false));
        tot_atomic_density -> Update(1.0, *atomic_density[0], 0.0);
        tot_atomic_density -> Update(1.0, *atomic_density[1], 1.0);
        for(int s = 0; s < spin_size; ++s){
            atomic_density[s] -> Update(occupations[s] -> get_total_occupation() / (tmp_occupation[0]+tmp_occupation[1]), *tot_atomic_density, 0.0);
            Verbose::single(Verbose::Normal) << "PAW Guess scale factor = " << occupations[s] -> get_total_occupation() / (tmp_occupation[0]+tmp_occupation[1]) << std::endl;
        }
    }

    state -> density = atomic_density;
    state -> orbitals = Density_Orbital::compute_density_orbitals(basis,atomic_density,occupations);

    return 0;
}
