#include "Huckel.hpp"

#include "Epetra_LAPACK.h"
#include "Epetra_MultiVector.h"

#include "../Utility/Value_Coef.hpp"
#include "../Utility/Verbose.hpp"

using std::vector;
using Teuchos::rcp;

Huckel::Huckel(Teuchos::Array< Teuchos::RCP<Occupation> > occupations, Teuchos::RCP<const Atoms> atoms, std::string ehtpar_filename ){
    this->atoms = atoms;
    this -> occupations = occupations;
    //this->param = new EHT_parameter("source/Data/eht_parms.dat");
    this->param = rcp( new EHT_parameter(ehtpar_filename) );
    this->number_of_basis_function.resize(atoms->get_size());
    this -> basis_size=0;

    vector<std::array<double,3> > position = atoms->get_positions();
    for(int i=0;i<atoms->get_size();i++){
        number_of_basis_function[i] = param->get_number_of_basis_function(atoms->get_atomic_numbers()[i]);
        basis_size += number_of_basis_function[i];
    }
}

int Huckel::compute(Teuchos::RCP<const Basis> basis, Teuchos::Array< Teuchos::RCP<State> >& states){
    Verbose::single(Verbose::Simple) << "Huckel guess will be used" << std::endl;
    Verbose::single(Verbose::Normal) << "Huckel:: compute" << std::endl;
    initialize_states(basis,states);
    extended_huckel();
    Verbose::single(Verbose::Detail) << "Diagonalization finished" << std::endl;

    int size = basis->get_original_size();
    //int number_atom = this -> atoms->get_size();

    Teuchos::Array< Teuchos::RCP<Epetra_Vector> > huckel_density;
    Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > huckel_wavefunctions;
    int spin_size = this -> occupations.size();

    //density and orbital initialization
    vector< vector<double> > density(spin_size);
    for(int i=0;i<spin_size;i++){
        density[i].resize(size, 0.0);
        huckel_density.append( Teuchos::rcp(new Epetra_Vector(*basis -> get_map())) );
        huckel_wavefunctions.append( Teuchos::rcp(new Epetra_MultiVector(*basis -> get_map(), this -> occupations[i]->get_size())) );
    }
    //density and orbital value insertion
    double x[3];
    int l_x=0, l_y=0, l_z=0;
    for(int l=0;l<size;l++){
        basis->get_position(l,x[0],x[1],x[2]);
        for(int orbital_num = 0; orbital_num < occupations[0]->get_size(); orbital_num++){
            double orbital_local_value = 0.0;
            for(int i=0, p=0, n=0; n<basis_size;n++){
                orbital_local_value += wavefunction.at(n).at(orbital_num)*atomic_orbital(i,p,x);
                if(is_orbital_index_ended(i,p)){p=0; i++;}
                else{p++;}
            }
            for(int i=0;i<spin_size;i++){
                //huckel_wavefunctions[i]->operator()(orbital_num)->operator[](l) = orbital_local_value;
                huckel_wavefunctions[i]->operator()(orbital_num)->ReplaceGlobalValue(l, 0, orbital_local_value);
                density.at(i).at(l) += occupations[i]->operator[](orbital_num) *orbital_local_value * orbital_local_value;
            }
        }

    }
    Verbose::single(Verbose::Detail) << "Append end" << std::endl;

    //orbitral value to coeffcient
    //density norm calculation
    double density_norm[2] = {0.0, 0.0};
    for(int i=0;i<spin_size;i++){
        Value_Coef::Value_Coef(basis,huckel_wavefunctions[i],true,false, huckel_wavefunctions[i]);// KSW maybe this cause error.
        for(int l=0;l<size;l++){
            density_norm[i] += density[i][l];
        }
    }
    Verbose::single(Verbose::Detail) << "Value_Coef end" << std::endl;

    density_norm[0] *= basis->get_scaling()[0] * basis->get_scaling()[1] * basis->get_scaling()[2];
    density_norm[1] *= basis->get_scaling()[0] * basis->get_scaling()[1] * basis->get_scaling()[2];
    //density norm calculation done

    Verbose::single(Verbose::Detail) << "density_norm : " << density_norm[0] << ' ' << density_norm[1] << std::endl;
    double density_normalization_factor[2] = {1.0, 1.0};
    for(int s = 0; s < spin_size; ++s){
        if( this -> occupations[s] -> get_total_occupation() > 1.0E-30 ){
            density_normalization_factor[s] = this -> occupations[s] -> get_total_occupation()/density_norm[0];
        }
    }
    //Density normalization
    density_norm[0] = density_norm[1] = 0.0;
    for(int i=0;i<spin_size;i++){
        for(int l=0;l<size;l++){
            huckel_density[i] -> ReplaceGlobalValue(l,i,density[i][l]*density_normalization_factor[i]);
            density_norm[i] += density[i][l]*density_normalization_factor[i];
        }
    }
    density_norm[0] *= basis ->get_scaling()[0] * basis ->get_scaling()[1] * basis ->get_scaling()[2];
    density_norm[1] *= basis ->get_scaling()[0] * basis ->get_scaling()[1] * basis ->get_scaling()[2];
    Verbose::single(Verbose::Detail) << "density_norm (after correction) : " << density_norm[0] << ' ' << density_norm[1] << std::endl;

    //return wavefunction and density
    states[states.size()-1] -> orbitals = huckel_wavefunctions;
    states[states.size()-1] -> density = huckel_density;
    //return huckel_wavefunctions;
    return 0;
}

void Huckel::extended_huckel(){
    Verbose::single(Verbose::Normal) << "Memory estimate for extended Huckel: " << (basis_size*(basis_size*2+3))*sizeof(double)/1024.0/1024 << " MB" << std::endl;
    this -> overlap_matrix.resize(basis_size);
    this -> hamiltonian.resize(basis_size);
    for(int i=0;i<basis_size;i++){
        overlap_matrix[i].resize(basis_size);
        hamiltonian[i].resize(basis_size);
    }
    double * S = new double[basis_size*(basis_size+1)/2];
    double * H = new double[basis_size*(basis_size+1)/2];

    //clock_t start_time, end_time;
    //start_time = clock();

    Verbose::single(Verbose::Normal) << "calculating S and H!" << std::endl;
    Verbose::single(Verbose::Normal) << "Number of overlap integrals need to calcualte: " << (basis_size * (basis_size+1))/2 << std::endl;
    int number_of_overlap_integral_to_be_calculated = 0;
    for(int i=0, p=0, n=0;n<basis_size;n++){
        for(int j=i, q=p, m=n;m<basis_size;m++){

            overlap_matrix.at(n).at(m) = get_overlap_integral(i,p,j,q);
            overlap_matrix.at(m).at(n) = overlap_matrix.at(n).at(m);
            S[n+m*(m+1)/2] = overlap_matrix.at(n).at(m);

            hamiltonian.at(n).at(m) = get_hamiltonian(i,p,n,j,q,m);
            hamiltonian.at(m).at(n) = hamiltonian.at(n).at(m);
            H[n+m*(m+1)/2] = hamiltonian.at(n).at(m);
            if(is_orbital_index_ended(j,q)){q=0; j++;}
            else{q++;}
            number_of_overlap_integral_to_be_calculated++;
            Verbose::single(Verbose::Normal) <<  number_of_overlap_integral_to_be_calculated << "/" << (basis_size*(basis_size+1))/2 << std::endl;
        }
        if(is_orbital_index_ended(i,p)){p=0; i++;}
        else{p++;}
    }
    //end_time = clock();
    //cout << "Calculation time : " << ((double)(end_time-start_time))/CLOCKS_PER_SEC << "sec" << endl;
    energy.resize(basis_size);
    double* MO = new double[basis_size*basis_size];
    double* work = new double[3*basis_size];
    int info = 0;
    Epetra_LAPACK lapack;
    lapack.SPGV(1, 'V', 'U', basis_size, H, S, energy.data(), MO, basis_size, work, &info);
    wavefunction.resize(basis_size);
    for(int i=0;i<basis_size;i++){
        wavefunction.at(i).resize(basis_size);
        for(int j=0;j<basis_size;j++){
            wavefunction.at(i).at(j) = MO[j*basis_size+i];
        }
    }
    delete[] S;
    delete[] H;
    delete [] MO;
    delete [] work;

}

double Huckel::get_overlap_integral(int i, int p, int j, int q){
    Verbose::single(Verbose::Detail) << "overlap_integral" << i << ' ' << p << ' ' << j << ' ' << q << std::endl;
    double return_value = 0.0;
    //integrating ith atom's pth orbital with jth atom's qth orbital
    double r[2][3];
    int num_1 = atoms->get_atomic_numbers()[i];
    int num_2 = atoms->get_atomic_numbers()[j];
    if(i==j){
        if(p==q) return_value = 1.0;
        //else return_value = 0.0;
    }
    else if(p==0&&q==0&&num_1==num_2&&num_1>=3&&num_1<=10){
        double zeta = param->get_zeta1(num_1,0);
        double zetaR = zeta*atoms->distance(i,j);
        return_value = exp(-zetaR)*(zetaR*(zetaR*(zetaR*(zetaR+5.0)+20.0)+45.0)+45.0)/(45.0);
    }
    else{
        for(int n=0;n<3;n++){
            r[0][n] = atoms->get_positions()[i][n];
            r[1][n] = atoms->get_positions()[j][n];
        }

        if(atoms->distance(i,j)>(param->get_cutoff(num_1)+param->get_cutoff(num_2))) return_value = 0.0;//neglegible overlap
        else{
            /*
             * Why this exists here?
            for(int n=0;n<3;n++){
                r[0][n] = r[0][n];
                r[1][n] = r[1][n];
            }
            */

            double max[3];
            double min[3];
            double mid[3];

            double interval = 1;

            for(int n=0;n<3;n++){
                max[n] = r[0][n]+param->get_cutoff(num_1);
                min[n] = r[0][n]-param->get_cutoff(num_1);
                if(max[n] < r[1][n]+param->get_cutoff(num_2)){
                    max[n] = r[1][n]+param->get_cutoff(num_2);
                }
                if(min[n] > r[1][n]-param->get_cutoff(num_2)){
                    min[n] = r[1][n]-param->get_cutoff(num_2);
                }
                mid[n] = (max[n]+min[n])/2.0;
                if(interval<max[n]-min[n]) interval = max[n]-min[n];
            }

            double x[3];
            for(int n=0;n<3;n++){
                min[n] = mid[n] - (0.5*interval);
                max[n] = mid[n] + (0.5*interval);
                x[n] = mid[n];
            }
            double scaling_factor = pow(interval,3);

            double local_val = atomic_orbital(i,p,x)*atomic_orbital(j,q,x)*scaling_factor;
            local_val = local_integration_convergence_test(local_val, x, i, p, j, q, scaling_factor, interval, 0);
            return_value = local_val;
        }
    }
    return return_value;
}

double Huckel::local_integration_convergence_test(double local_val, double* x, int i, int p, int j, int q, double scaling_factor, double interval, int iter_num){
    iter_num++;
    double xnew[3];
    double new_val = 0.0;

    double sublattice[3][3][3];
    for(int ll=-1;ll<=1;ll++){
        for(int mm=-1;mm<=1;mm++){
            for(int nn=-1;nn<=1;nn++){
                if(ll==0&&mm==0&&nn==0){
                    sublattice[1][1][1] = local_val/27.0;
                }
                else{
                    xnew[0] = x[0]+(double)ll*interval/3.0;
                    xnew[1] = x[1]+(double)mm*interval/3.0;
                    xnew[2] = x[2]+(double)nn*interval/3.0;
                    sublattice[ll+1][mm+1][nn+1] = atomic_orbital(i,p,xnew)*atomic_orbital(j,q,xnew)*scaling_factor/27.0;
                }
                new_val += sublattice[ll+1][mm+1][nn+1];
            }
        }
    }
    if(abs(local_val-new_val)<1E-13*pow(27.0,iter_num)){
        return new_val;
    }
    else{
        local_val = new_val;
        new_val=0.0;
        for(int ll=-1;ll<=1;ll++){
            for(int mm=-1;mm<=1;mm++){
                for(int nn=-1;nn<=1;nn++){
                    xnew[0] = x[0]+(double)ll*interval/3.0;
                    xnew[1] = x[1]+(double)mm*interval/3.0;
                    xnew[2] = x[2]+(double)nn*interval/3.0;
                    new_val += local_integration_convergence_test(sublattice[ll+1][mm+1][nn+1],xnew,i,p,j,q,scaling_factor/27.0,interval/3.0,iter_num);
                }
            }
        }
    }
    return new_val;
}



double Huckel::atomic_orbital(int i, int p, double* x_old){
    //this routine uses EHT_parameter->atomic_orbital(atomic_nubmer, orbital_nubmer, x)
    double* x = new double[3];
    for(int a=0;a<3;a++){
        x[a] = x_old[a]-atoms->get_positions()[i][a];
    }
    double return_value = param->atomic_orbital(atoms->get_atomic_numbers()[i],p,x);
    delete [] x;
    return return_value;
}


double Huckel::get_hamiltonian(int i, int p, int n,  int j, int q, int m){
    //K=1.75, H_ij = 0.5*K*S_ij*(H_ii + H_jj)
    double K2=0.875;
    int orbital_number1, orbital_number2;
    switch(p){
        case 0:
            orbital_number1 = 0; // s orbital
            break;
        case 1:        case 2:        case 3:
            orbital_number1 = 1; // p orbital
            break;
        case 4: case 5: case 6: case 7: case 8:
            orbital_number1 = 2; // d orbital
            break;
        case 9: case 10: case 11: case 12: case 13: case 14: case 15:
            orbital_number1 = 3; // f orbital
            break;
        default:
            Verbose::all() << "ERROR IN GET_HAMILTONIAN" << std::endl;
            exit(1);
    }
    switch(q){
        case 0:
            orbital_number2 = 0; // s orbital
            break;
        case 1:        case 2:        case 3:
            orbital_number2 = 1; // p orbital
            break;
        case 4: case 5: case 6: case 7: case 8:
            orbital_number2 = 2; // d orbital
            break;
        case 9: case 10: case 11: case 12: case 13: case 14: case 15:
            orbital_number2 = 3; // f orbital
            break;
        default:
            Verbose::all() << "ERROR IN GET_HAMILTONIAN" << std::endl;
            exit(1);
    }
    if(n==m)
        return param->get_orbital_energy(atoms->get_atomic_numbers()[i],orbital_number1);
    else
        return K2*overlap_matrix[n][m]*(param->get_orbital_energy(atoms->get_atomic_numbers()[i],orbital_number1) + param->get_orbital_energy(atoms->get_atomic_numbers()[j],orbital_number2));
}

bool Huckel::is_orbital_index_ended(int i, int p){
    return param->is_orbital_index_ended(atoms->get_atomic_numbers()[i],p);
}
