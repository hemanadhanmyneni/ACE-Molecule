#include "Cube_Density_from_Orbitals.hpp"
#include "../Utility/Density/Density_Orbital.hpp"

using Teuchos::Array;
using Teuchos::ParameterList;
using Teuchos::rcp;
using Teuchos::RCP;

Cube_Density_from_Orbitals::Cube_Density_from_Orbitals(Array< RCP<Occupation> > occupations, RCP<const Atoms> atoms, Array< RCP<Occupation> > input_occupations, Array<std::string> cube_filenames, bool is_bohr)
: Cube::Cube(input_occupations, atoms, cube_filenames, is_bohr){
    this -> out_occupations = occupations;
}

int Cube_Density_from_Orbitals::compute(RCP<const Basis> basis,Array< RCP<State> >& states){
    Verbose::single(Verbose::Detail) << * this -> occupations[0] << std::endl;
    Verbose::single(Verbose::Detail) << * this -> occupations[1] << std::endl;
    Verbose::single(Verbose::Detail) << this -> cube_filenames << std::endl;
    Cube::compute(basis, states);
    RCP<State> state = states[states.size()-1];
    states[states.size()-1] -> orbitals = Density_Orbital::compute_density_orbitals(basis, state -> density, this -> out_occupations);
    return 0;
}
