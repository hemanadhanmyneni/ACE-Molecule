#pragma once
#include <vector>
#include "Teuchos_RCP.hpp"
#include "Teuchos_ParameterList.hpp"
#include "Teuchos_Array.hpp"
#include "EpetraExt_MatrixMatrix.h"
#include "Epetra_Map.h"

#include "Core_Hamiltonian_Matrix.hpp"
#include "Guess.hpp"
#include "../Basis/Basis.hpp"
#include "../Core/Diagonalize/Diagonalize.hpp"
#include "../Core/Pseudo-potential/Nuclear_Potential.hpp"
#include "../Io/Atoms.hpp"


/**
@author Sunghwan Choi
@brief Core_Hamiltonian class compute nuclear attraction and electron kinetic terms.
@details This class is a unavoidable part for all DFT calculations.
*/
class Core_Hamiltonian: public Guess{
    public:
        Core_Hamiltonian(Teuchos::RCP<const Basis> basis, Teuchos::Array<Teuchos::RCP<Occupation> > occupations,Teuchos::RCP<const Atoms> atoms,bool store_crs_matrix, Teuchos::RCP<Teuchos::ParameterList> &parameters);

        static void free_matrix();
        int compute(Teuchos::RCP<const Basis> basis,Teuchos::Array<Teuchos::RCP<State> >& states);
        int get_core_electron_info(Teuchos::Array< Teuchos::RCP<Epetra_Vector> >& density, Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> >& density_grad);

        int get_kinetic_energy(Teuchos::RCP<State> state, std::vector<double>& kinetic_energies);
        int calculate_ion_ion(double& nuclear_nuclear_repulsion );
        int calculate_nuclear_density_interaction(Teuchos::RCP<State> state,double& external_energy);

        int get_core_hamiltonian(int i_spin, Teuchos::RCP<Epetra_CrsMatrix>& return_val);

        void update_hamiltonian(
            Teuchos::Array< Teuchos::RCP<const Occupation> > occupations,
            Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbitals
        );

        void update_orbital_info(
            Teuchos::Array< Teuchos::RCP<const Occupation> > occupations,
            Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbitals
        );
        Teuchos::RCP<Core_Hamiltonian_Matrix> get_core_hamiltonian_matrix();
        Teuchos::RCP<Nuclear_Potential> get_nuclear_potential();
        //int get_pp_matrix(int i_spin, Teuchos::RCP<Epetra_CrsMatrix>& return_val);
        Teuchos::RCP<Diagonalize> diagonalize;

    protected:
        Teuchos::RCP< const Basis > basis;
        //Teuchos::Array<Teuchos::RCP<Epetra_CrsMatrix> > core_hamiltonians;
        /// This funciton fills kinetic values on core_hamiltonian which takes core_hamiltonian as an argument
//        void kinetic(Teuchos::RCP<Epetra_CrsMatrix> core_hamiltonian);
        int kinetic(Teuchos::RCP<const Basis> basis,bool overwrite=false);
        /// This function constructs core_hamiltonian matrix
        void construct_matrix(Teuchos::RCP<const Basis> basis);

        //Teuchos::RCP<Epetra_Vector> external_potential_vector;
        static Teuchos::RCP<Core_Hamiltonian_Matrix> core_H_matrix;
};
