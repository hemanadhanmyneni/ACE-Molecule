#include "TDDFT.hpp"
#include <complex>
#include <stdexcept>
#include <cmath>
#include "Epetra_CrsMatrix.h"
#include "Teuchos_Time.hpp"
#include "Teuchos_TimeMonitor.hpp"

#include "../Basis/Basis_Function/Finite_Difference.hpp"
#include "../Basis/Basis_Function/Sinc.hpp"
#include "../Core/Diagonalize/Create_Diagonalize.hpp"
#include "../Core/Occupation/Occupation_From_Input.hpp"
#include "../Core/Solvation/Create_Solvation.hpp"

#include "../Utility/Two_e_integral.hpp"
#include "../Utility/Value_Coef.hpp"
#include "../Utility/Calculus/Lagrange_Derivatives.hpp"
#include "../Utility/Calculus/Integration.hpp"
#include "../Utility/Parallel_Manager.hpp"
#include "../Utility/Parallel_Util.hpp"
#include "../Utility/String_Util.hpp"
#include "../Utility/Density/Density_From_Orbitals.hpp"

#define DEBUG (1)// 0 turns off fill-time output.
#define GGA_OUT (1)// 0 turns off GGA_fill_output.
#define _ACE_NEW_CONTRIB_ (1)
#define _HA_TO_EV_ (27.211396132)

using std::abs;
using std::vector;
using std::endl;
using std::string;
using std::setw;
using std::setiosflags;
using std::ios;
using Teuchos::RCP;
using Teuchos::rcp;
using Teuchos::Array;
using Teuchos::Time;
using Teuchos::TimeMonitor;

TDDFT::TDDFT(RCP<const Basis> basis, RCP<Exchange_Correlation> exchange_correlation, RCP<Poisson_Solver> poisson_solver, RCP<Teuchos::ParameterList> parameters){
    this -> cutoff = parameters -> sublist("TDDFT").get<double>("TruncationCriteria", 1E-15);
    if(parameters -> sublist("TDDFT").get<int>("Root", 1) != 1){
        Verbose::all() << "Root option is not supported!" << std::endl;
        throw std::invalid_argument("Root option is not supported");
    }
    this -> new_orbital_grad = parameters->sublist("TDDFT").get<int>("Gradient",1)==1;

    string sort_orbital = parameters -> sublist("TDDFT").get<string>("SortOrbital");
    if(sort_orbital != "Order" or sort_orbital != "Tolerance"){
        //Verbose::single() << "TDDFT::compute - The option \"SortOrbital\" is wrong.\n";
        //Verbose::single() << "TDDFT::compute - Set \"SortOrbital\" as \"Order\"\n";
        parameters->sublist("TDDFT").set<string>("SortOrbital", "Order");
    }
    if(sort_orbital == "Tolerance"){
        parameters->sublist("TDDFT").get<double>("OrbitalTolerance", 0.1);
    }

    this->basis = basis;
    auto tddft_param = Teuchos::sublist(parameters, "TDDFT");
    this->poisson_solver = poisson_solver;
    this->exchange_correlation = exchange_correlation;
    this -> exchange_poisson_solver = Teuchos::rcp_const_cast<Poisson_Solver>(this -> exchange_correlation -> get_poisson_solver());
    this->parameters = parameters;
    this->internal_timer = Teuchos::rcp(new Time_Measure() );

    this -> _exchange_kernel = parameters -> sublist("TDDFT").get<string>("ExchangeKernel");
    if(this -> _exchange_kernel == "HF-EXX"){
       this -> is_tdhf_kli = (parameters -> sublist("TDDFT").get<int>("DeltaCorrection") == 2);
    }

    //Not working
    this -> is_rtriplet = parameters -> sublist("TDDFT").get<int>("RestrictedTriplet", 0) == 1;

    //open-shell TDDFT
    this -> is_open_shell = parameters -> sublist("BasicInformation").get<int>("Polarize", 0) == 1;

    this -> theorylevel = parameters -> sublist("TDDFT").get<string>("TheoryLevel");
    Verbose::single(Verbose::Normal) << "TDDFT: poisson solver is:" << std::endl << this -> poisson_solver -> get_info_string() << std::endl;
    if(this -> exchange_poisson_solver != Teuchos::null){
        Verbose::single(Verbose::Simple) << "TDDFT: exchange_poisson solver is:" << std::endl << this -> exchange_poisson_solver -> get_info_string() << std::endl;
    }
    this -> orbital_pair_cutoff = parameters -> sublist("TDDFT").get("OrbitalPairCutoff", -1.0);

#ifdef ACE_PCMSOLVER
    this -> solvation = Create_Solvation::Create_Solvation(basis, Teuchos::sublist(tddft_param, "SolvationModel"));
#endif
}

TDDFT::~TDDFT(){
    Verbose::single(Verbose::Simple) << "TDDFT Time Profile" << std::endl;
    Verbose::set_numformat(Verbose::Time);
    this -> internal_timer -> print(Verbose::single(Verbose::Simple));
}

int TDDFT::compute(RCP<const Basis> basis, Array< RCP<State> >& states){
    this -> state = rcp(new State( *states[states.size()-1]));
    state->timer->start("TDDFT::compute");

    // Orbital number initialization
    initialize_orbital_numbers(state);

    this -> matrix_dimension = 0;    
    for(int i_spin=0; i_spin< (is_open_shell? 2:1) ; i_spin++){
        int num_hole_states = this -> number_of_occupied_orbitals[i_spin] - this -> iroot;
        int num_electron_states = this -> number_of_orbitals[i_spin] - this -> number_of_occupied_orbitals[i_spin];
        this -> matrix_dimension += num_hole_states * num_electron_states;
    }
    this -> matrix_dimension *= this -> mat_dim_scale;
    this -> num_excitation = parameters->sublist("TDDFT").get<int>("NumberOfStates", matrix_dimension);

    if(this -> type == "ABBA"){
        if(num_excitation <= matrix_dimension/2){
            num_excitation += matrix_dimension/2;
            parameters->sublist("TDDFT").set<int>("NumberOfStates", num_excitation);
        }
    }
    // Orbital number initialization end

    // Map and overlap matrix initialization
    Epetra_Map config_map(matrix_dimension,0,this -> basis -> get_map()->Comm());
    Teuchos::RCP<Epetra_CrsMatrix> overlap_matrix = Teuchos::null;
    if(this -> type == "ABBA"){
        overlap_matrix = rcp(new Epetra_CrsMatrix(Copy, config_map, 0));
        for(int i = 0; i < matrix_dimension; ++i){
            double val = (i < matrix_dimension/2)? 1.: -1.;
            if(config_map.MyGID(i)){
                overlap_matrix -> InsertGlobalValues(i, 1, &val, &i);
            }
        }
        overlap_matrix -> FillComplete();
    }
    // Orbital number and overlap matrix iniitalization

    // Sanity check
    if(*basis != (*this->basis)){
        Verbose::all() << "TDDFT::compute - \"basis\" in TDDFT is different to given basis\n";
        Verbose::all() << this->basis.get() << std::endl << basis.get() << std::endl;
        throw std::logic_error("TDDFT::compute - \"basis\" in TDDFT is different to given basis\n");
    }
    if(state->orbitals.size() == 0){
        Verbose::all() << "TDDFT::compute - Orbitals must exist for TDDFT calculation.\n";
        throw std::runtime_error("No orbitals for TDDFT calculations");
    }

    Verbose::set_numformat(Verbose::Energy);
    Verbose::single(Verbose::Normal) << "Orbital check" << std::endl;
    for(int s = 0; s < state -> get_occupations().size(); ++s){
        double * norm2 = new double[state -> get_occupations()[s]->get_size()];
        state -> get_orbitals()[s] -> Norm2(norm2);
        Verbose::single(Verbose::Normal) << "Spin " << s << std::endl;
        for(int i = 0; i < state -> get_occupations()[s]->get_size(); ++i){
            if(norm2[i] < 0.9 or norm2[i] > 1.1){
                Verbose::set_numformat(Verbose::Occupation);
                Verbose::all() << "2-norm of spin " << s << " orbital #" << i+1 << " is " << norm2[i] << "!" << std::endl;
                throw std::runtime_error("2-norm of spin " + String_Util::to_string(s) + " orbital #" + String_Util::to_string(i+1) + " is " + String_Util::to_string(norm2[i]) + "!");
            }
            Verbose::single(Verbose::Normal) << state -> get_orbital_energies()[s][i] << "\t";
        }
        Verbose::single(Verbose::Normal) << std::endl;
        delete[] norm2;
    }
    // Sanity check end
    
    // Correction term initialization
    this -> nuclear_potential = Create_Compute::Create_Core_Hamiltonian(basis, state -> get_atoms(), parameters) -> get_core_hamiltonian_matrix()->nuclear_potential;
#ifdef ACE_PCMSOLVER
    if(this -> solvation != Teuchos::null){
        this -> solvation -> initialize(this->state -> get_atoms(), nuclear_potential->get_Zeffs());
    }
#endif
    // Correction term initialization end

    // DFT kernel computation
    exchange_correlation->compute_fxc(state);

    // TD hybrid portion calculation and output
    string functional_type = exchange_correlation->get_functional_type();
    functional_type = this -> change_exx_kernel(functional_type, this -> _exchange_kernel);
    this -> print_tddft_theory_info(functional_type);

    vector<string> functionals = String_Util::strSplit(functional_type, "_");
    vector<double> portions(functionals.size(), 1.0);
    for(vector<string>::iterator it = functionals.begin(); it != functionals.end(); ++it){
        if(*it == "HF" or *it == "HF-EXX"){
            portions[it-functionals.begin()] = this -> exchange_correlation -> get_EXX_portion();
        }
    }
    // TD hybrid portion calculation and output end

    // TD matrix construction and diagonalization
    Array< RCP<Diagonalize> > diagonalizes;
    if(!is_open_shell){
        internal_timer->start("TDDFT::construct_matrix");
        Array< RCP<Epetra_CrsMatrix> > sparse_matrices;
        sparse_matrices.append( rcp(new Epetra_CrsMatrix(Copy,config_map,0)) );
        diagonalizes.append(Create_Diagonalize::Create_Diagonalize(Teuchos::sublist(Teuchos::sublist(parameters,"TDDFT"), "Diagonalize")));
        this -> get_full_TD_matrix(functionals, portions, iroot, state, sparse_matrices[0]);

        sparse_matrices[0]->FillComplete(); //shchoi sparse
        internal_timer -> end("TDDFT::construct_matrix");

        Verbose::single(Verbose::Simple) << "\n#------------------------------------------------------ TDDFT::compute\n";
        internal_timer->print(Verbose::single(Verbose::Simple), "TDDFT::construct_matrix");
        Verbose::single(Verbose::Simple) << "#---------------------------------------------------------------------\n";

        internal_timer -> start("TDDFT::compute diagonalize");
        auto initial_eigenvectors = rcp(new Epetra_MultiVector( config_map, num_excitation ) );
        initial_eigenvectors->Random();
        diagonalizes[0]->diagonalize(sparse_matrices[0], num_excitation, initial_eigenvectors, overlap_matrix);
        internal_timer -> end("TDDFT::compute diagonalize");
    } else {
        internal_timer -> start("TDDFT::construct_matrix");
        RCP<Epetra_CrsMatrix> sparse_matrix = rcp(new Epetra_CrsMatrix(Copy, config_map, 0));
        diagonalizes.append(Create_Diagonalize::Create_Diagonalize(Teuchos::sublist(Teuchos::sublist(parameters,"TDDFT"), "Diagonalize")));

        this -> get_full_TD_matrix(functionals, portions, iroot, state, sparse_matrix);

        sparse_matrix -> FillComplete();
        internal_timer -> end("TDDFT::construct_matrix");

        Verbose::single(Verbose::Simple) << "\n#------------------------------------------------------ TDDFT::compute\n";
        internal_timer->print(Verbose::single(Verbose::Simple), "TDDFT::construct_matrix");
        Verbose::single(Verbose::Simple) << "#---------------------------------------------------------------------\n";

        internal_timer -> start("TDDFT::compute diagonalize");
        auto initial_eigenvectors = rcp(new Epetra_MultiVector( config_map, num_excitation ) );
        initial_eigenvectors->Random();
        diagonalizes[0]->diagonalize(sparse_matrix, num_excitation, initial_eigenvectors, overlap_matrix);
        internal_timer -> end("TDDFT::compute diagonalize");
    }
    // TD matrix construction and diagonalization end

    // Postprocessing
    Verbose::single(Verbose::Simple) << "\n#------------------------------------------------------ TDDFT::compute\n";
    if(!is_open_shell){
        for(int s = 0; s < diagonalizes.size(); ++s){
            if(s == 0){
                Verbose::single(Verbose::Simple) << " Singlet excitation information:" << std::endl;
            } else {
                Verbose::single(Verbose::Simple) << " Triplet excitation information:" << std::endl;
            }
            RCP<Diagonalize> diagonalize = diagonalizes[s];
            std::vector<double> energies;
            Teuchos::RCP<Epetra_MultiVector> eigenvectors;
            this -> get_excitations(diagonalize, energies, eigenvectors);

            this -> postprocessing(energies, eigenvectors, state, energies.size());
        }
    } else {
        RCP<Diagonalize> diagonalize = diagonalizes[0];
        std::vector<double> energies;
        Teuchos::RCP<Epetra_MultiVector> eigenvectors;
        this -> get_excitations(diagonalize, energies, eigenvectors);

        this -> postprocessing(energies, eigenvectors, state, energies.size());
    }
    Verbose::single(Verbose::Simple) <<   "#---------------------------------------------------------------------\n";
    // Postprocessing end

    state->timer->end("TDDFT::compute");
    states.push_back(state);

    return 0;
}

void TDDFT::postprocessing(
        vector<double> excitation_energies,
        RCP<Epetra_MultiVector> eigenvectors,
        RCP<State> state,
        int num_excitation
){
    bool is_abba = (this -> type == "ABBA")? true: false;
    internal_timer->start("TDDFT postprocessing"); //postprocessing
    // Compute oscillator strength
    vector<double> oscillator_strength(num_excitation);
    vector< vector<double> > state_overlap(3);
    Verbose::single(Verbose::Simple) << "TDDFT::compute - Compute oscillator strength\n";
    internal_timer->start("TDDFT::compute oscillator strength");
    this -> calculate_oscillator_strength(excitation_energies,
            eigenvectors, state, num_excitation,
            state_overlap, oscillator_strength, is_abba);
    internal_timer->end("TDDFT::compute oscillator strength");
    internal_timer-> print(Verbose::single(Verbose::Simple), "TDDFT::compute oscillator strength");
    vector< vector<double> > ct_character;

    this -> CT_analysis(excitation_energies,
            eigenvectors, state, num_excitation,
            ct_character, this -> spatial_overlap, is_abba);
    // end

    double tolerance = 0.0;
    int default_max_order = (is_abba)? eigenvectors -> GlobalLength()/2: eigenvectors -> GlobalLength();
    default_max_order = std::min(10, default_max_order);
    int order = default_max_order;
    internal_timer -> start("TDDFT_print");
    if(parameters->sublist("TDDFT").get<string>("SortOrbital") == "Tolerance"){
        tolerance = parameters->sublist("TDDFT").get<double>("OrbitalTolerance");
    }
    else if(parameters->sublist("TDDFT").get<string>("SortOrbital") == "Order"){
        order = parameters->sublist("TDDFT").get<int>("MaximumOrder", default_max_order);
    }
    internal_timer -> start("print_excitation");
    this -> print_excitation_info(tolerance, order,
            eigenvectors,
            this -> iroot, number_of_occupied_orbitals[0], number_of_orbitals[0],
            excitation_energies, ct_character,
            state_overlap, oscillator_strength, is_abba);

    Verbose::set_numformat(Verbose::Time);
    internal_timer -> end("print_excitation");
    internal_timer -> print(Verbose::single(Verbose::Simple), "print_excitation");
    internal_timer -> end("TDDFT_print");
    internal_timer-> print(Verbose::single(Verbose::Simple), "TDDFT_print");
    internal_timer-> end("TDDFT postprocessing");
    internal_timer-> print(Verbose::single(Verbose::Simple), "TDDFT postprocessing");
}

void TDDFT::print_excitation_info(
    double tolerance, int order,
    RCP<Epetra_MultiVector> eigenvectors,
    int iroot, int num_occ_orbitals, int num_orbitals,
    std::vector<double> excitation_energies,
    std::vector< std::vector<double> > ct_character,
    std::vector< std::vector<double> > state_overlap,
    std::vector<double> oscillator_strength,
    bool upper_half/* = false*/
){
    int num_excitation = excitation_energies.size();
    vector< vector<int> > determinant_index;
    for(int i=iroot; i<num_occ_orbitals; ++i){
        for(int a=num_occ_orbitals; a<num_orbitals; ++a){
            vector<int> tmp(2);
            tmp[0] = i+1; tmp[1] = a+1;
            determinant_index.push_back(tmp);
        }
    }
    for(int i = 0; i < num_excitation; ++i){
        Verbose::set_numformat(Verbose::Energy);
        Verbose::single(Verbose::Simple) << setw(0) << " #" << setw(0) << i+1 << ": Excitation energy = " << excitation_energies[i] << " Hartree = " << excitation_energies[i] * 27.211396132 << " eV\n";
        Verbose::set_numformat(Verbose::Scientific);
        Verbose::single(Verbose::Simple) << "     <x> = " << std::scientific << state_overlap[0][i] << "\n";
        Verbose::single(Verbose::Simple) << "     <y> = " << std::scientific << state_overlap[1][i] << "\n";
        Verbose::single(Verbose::Simple) << "     <z> = " << std::scientific << state_overlap[2][i] << "\n";
        Verbose::single(Verbose::Simple) << "     Oscillator strength = " << std::scientific << oscillator_strength[i] << "\n";
        Verbose::single(Verbose::Simple) << "     Orbital overlap = " << std::scientific << ct_character.at(0)[i] << "\n";
        Verbose::single(Verbose::Simple) << "     r-index = " << std::scientific << ct_character.at(1)[i] << "\n";
        Verbose::single(Verbose::Simple) << "     (Gamma-r)-index = " << std::scientific << ct_character[2][i]-ct_character[1][i] << "\n";
        Verbose::single(Verbose::Simple) << "     Gamma-index = " << std::scientific << ct_character.at(2)[i] << "\n";
        print_contribution(eigenvectors, i, determinant_index, order, tolerance, upper_half);
    }
}

string TDDFT::change_exx_kernel(string functional_type, string exchange_kernel, string exx_default/* = "PGG"*/){
    if(exchange_kernel.size() == 0){
        return functional_type;
    }
    std::size_t found = functional_type.find("EXX");
    if(found != string::npos){
        if(exchange_kernel == "HF"){
            functional_type.replace(found, 3, "HF");
        } else if(exchange_kernel == "KLI-PGG" or exchange_kernel == "PGG"){
            functional_type.replace(found, 3, "PGG");
        } else if(exchange_kernel == "HF-EXX"){
            functional_type.replace(found, 3, "HF-EXX");
        } else {
            Verbose::single() << "EXX kernel set to default value "+exx_default+"." << std::endl;
            functional_type.replace(found, 3, exx_default);
        }
    }
    found = functional_type.find("KLI-PGG");
    if(found != string::npos){
        if(exchange_kernel == "HF"){
            functional_type.replace(found, 7, "HF");
        } else if(exchange_kernel == "KLI-PGG" or exchange_kernel == "PGG"){
            functional_type.replace(found, 7, "PGG");
        } else if(exchange_kernel == "HF-EXX"){
            functional_type.replace(found, 7, "HF-EXX");
        } else {
            Verbose::single(Verbose::Normal) << "EXX kernel set to default value PGG." << std::endl;
            functional_type.replace(found, 7, "PGG");
        }
    }
    found = functional_type.find("KLI");
    if(found != string::npos){
        if(exchange_kernel == "HF"){
            functional_type.replace(found, 3, "HF");
        } else if(exchange_kernel == "KLI-PGG" or exchange_kernel == "PGG"){
            functional_type.replace(found, 3, "PGG");
        } else if(exchange_kernel == "HF-EXX"){
            functional_type.replace(found, 3, "HF-EXX");
        } else {
            Verbose::single(Verbose::Normal) << "EXX kernel set to default value PGG." << std::endl;
            functional_type.replace(found, 3, "PGG");
        }
    }
    return functional_type;
}

void TDDFT::calculate_oscillator_strength(
        vector<double> excitation_energies,
        RCP<Epetra_MultiVector> TD_excitation,
        RCP<State> state,
        int num_excitation,
        vector< vector<double> > &state_overlap,
        vector<double> &oscillator_strength,
        bool is_deexcit/* = false*/
){
    auto map = basis->get_map();
    auto config_map = TD_excitation -> Map();
    int matrix_dimension = config_map.NumGlobalElements();
    state_overlap.resize(3);
    oscillator_strength = vector<double>(num_excitation);
    for(int d = 0; d < 3; ++d){
        state_overlap[d] = vector<double>(num_excitation);
    }
    // R F_I = Omega_I^2 F_I
    auto F_I = TD_excitation; // RCP<MV>
    int NumMyElements = map->NumMyElements();
    int* MyGlobalElements = map->MyGlobalElements();

    int config_NumMyElements = config_map.NumMyElements();
    int* config_MyGlobalElements = config_map.MyGlobalElements();

    const int mat_size = is_deexcit? matrix_dimension/2: matrix_dimension;

    double* orbital_overlap_x = new double [mat_size]();
    double* orbital_overlap_y = new double [mat_size]();
    double* orbital_overlap_z = new double [mat_size]();
    double* overlap_x = new double [mat_size]();
    double* overlap_y = new double [mat_size]();
    double* overlap_z = new double [mat_size]();

    int matrix_index_r = 0;
    this -> eps_diff = vector<double>(mat_size);

    for(int matrix_index_r = 0; matrix_index_r < mat_size; ++matrix_index_r){
        int ispin, i, a;
        this -> unpack_index(matrix_index_r, ispin, i, a);
        for(int k=0; k<NumMyElements; ++k){
            int k_x=0, k_y=0, k_z=0;
            double x, y, z;
            basis->get_position(MyGlobalElements[k], x, y, z);

            overlap_x[matrix_index_r] += state->get_orbitals()[ispin]->operator[](i)[k] * state->get_orbitals()[ispin]->operator[](a)[k] * x;
            overlap_y[matrix_index_r] += state->get_orbitals()[ispin]->operator[](i)[k] * state->get_orbitals()[ispin]->operator[](a)[k] * y;
            overlap_z[matrix_index_r] += state->get_orbitals()[ispin]->operator[](i)[k] * state->get_orbitals()[ispin]->operator[](a)[k] * z;
        }
        this -> eps_diff[matrix_index_r] = sqrt(state -> orbital_energies[ispin][a] - state -> orbital_energies[ispin][i]);
    }

    Parallel_Util::group_sum(overlap_x, orbital_overlap_x, mat_size);
    Parallel_Util::group_sum(overlap_y, orbital_overlap_y, mat_size);
    Parallel_Util::group_sum(overlap_z, orbital_overlap_z, mat_size);

    delete[] overlap_x;
    delete[] overlap_y;
    delete[] overlap_z;

    vector< vector<double> > z_vec = this -> gather_z_vector(F_I, state -> orbital_energies);
    for(int i=0; i<num_excitation; ++i){
        double r_portion[3] = {0.0, 0.0, 0.0};
        for(int j = 0; j < mat_size; ++j){
            r_portion[0] += z_vec[i][j] * orbital_overlap_x[j];
            r_portion[1] += z_vec[i][j] * orbital_overlap_y[j];
            r_portion[2] += z_vec[i][j] * orbital_overlap_z[j];
        }

        for(int d = 0; d < 3; ++d){
            r_portion[d] *= eps_diff[i];
            state_overlap[d][i] = sqrt(2.0) * r_portion[d];
        }

        oscillator_strength[i] = 2.0 / 3.0 * (r_portion[0]*r_portion[0] + r_portion[1]*r_portion[1] + r_portion[2]*r_portion[2]);
        if(state -> get_orbitals().size() == 1){
            oscillator_strength[i] *= 2;
        }
    }

    delete[] orbital_overlap_x;
    delete[] orbital_overlap_y;
    delete[] orbital_overlap_z;
}

void TDDFT::CT_analysis(
        vector<double> excitation_energies,
        RCP<Epetra_MultiVector> TD_excitation,
        RCP<State> state,
        int num_excitation,
        vector< vector<double> > &ct_character,
        vector<double> &spatial_overlap,
        bool is_deexcit/* = false*/
){
    internal_timer->start("TDDFT CTcal");
    int NumMyElements = this -> basis -> get_map()->NumMyElements();
    int* MyGlobalElements = this -> basis -> get_map()->MyGlobalElements();
    const int matrix_dimension = TD_excitation -> Map().NumGlobalElements();

    const int mat_size = is_deexcit? matrix_dimension/2: matrix_dimension;
    const int spin_size = is_open_shell? 2: 1;
    //const int num_virt_orb = this -> number_of_orbitals[ispin] - this -> number_of_occupied_orbitals[ispin];
    //const int num_occ_orb = this -> number_of_occupied_orbitals[ispin] - start;
    vector< vector<double> > z_vec = this -> gather_z_vector(TD_excitation, state -> orbital_energies);

    double* orbital_overlap = new double [mat_size]();
    double* overlap = new double [mat_size]();

    if(this -> eps_diff.size() != mat_size){
        this -> eps_diff = vector<double>(mat_size);
        for(int matrix_index_r = 0; matrix_index_r < mat_size; ++matrix_index_r){
            int ispin, i, a;
            this -> unpack_index(matrix_index_r, ispin, i, a);
            this -> eps_diff[matrix_index_r] = sqrt(state -> orbital_energies[ispin][a] - state -> orbital_energies[ispin][i]);
        }
    }
    for(int matrix_index_r = 0; matrix_index_r < mat_size; ++matrix_index_r){
        int ispin, i, a;
        this -> unpack_index(matrix_index_r, ispin, i, a);
        for(int k=0; k<NumMyElements; ++k){
            overlap[matrix_index_r] += std::abs(state->get_orbitals()[ispin]->operator[](i)[k]) * std::abs(state->get_orbitals()[ispin]->operator[](a)[k]);
        }
    }

    Parallel_Util::group_sum(overlap, orbital_overlap, mat_size);
    delete[] overlap;

    double** orbital_centoid_x = new double* [spin_size];
    double** orbital_centoid_y = new double* [spin_size];
    double** orbital_centoid_z = new double* [spin_size];

    double** orbital_centoid_x2 = new double* [spin_size];
    double** orbital_centoid_y2 = new double* [spin_size];
    double** orbital_centoid_z2 = new double* [spin_size];
    for(int ispin = 0; ispin < spin_size; ++ispin){
        orbital_centoid_x[ispin] = new double [number_of_orbitals[ispin]]();
        orbital_centoid_y[ispin] = new double [number_of_orbitals[ispin]]();
        orbital_centoid_z[ispin] = new double [number_of_orbitals[ispin]]();
        double * centoid_x = new double [number_of_orbitals[ispin]]();
        double * centoid_y = new double [number_of_orbitals[ispin]]();
        double * centoid_z = new double [number_of_orbitals[ispin]]();

        orbital_centoid_x2[ispin] = new double [number_of_orbitals[ispin]]();
        orbital_centoid_y2[ispin] = new double [number_of_orbitals[ispin]]();
        orbital_centoid_z2[ispin] = new double [number_of_orbitals[ispin]]();
        double * centoid_x2 = new double [number_of_orbitals[ispin]]();
        double * centoid_y2 = new double [number_of_orbitals[ispin]]();
        double * centoid_z2 = new double [number_of_orbitals[ispin]]();
        for(int k=0; k<NumMyElements; ++k){
            double x, y, z;
            basis->get_position(MyGlobalElements[k], x, y, z);
            for(int i = this -> iroot; i < number_of_orbitals[ispin]; ++i){
                centoid_x[i] += pow(state->get_orbitals()[ispin]->operator[](i)[k],2) * x;
                centoid_y[i] += pow(state->get_orbitals()[ispin]->operator[](i)[k],2) * y;
                centoid_z[i] += pow(state->get_orbitals()[ispin]->operator[](i)[k],2) * z;

                centoid_x2[i] += pow(state->get_orbitals()[ispin]->operator[](i)[k],2) * x*x;
                centoid_y2[i] += pow(state->get_orbitals()[ispin]->operator[](i)[k],2) * y*y;
                centoid_z2[i] += pow(state->get_orbitals()[ispin]->operator[](i)[k],2) * z*z;
            }
        }

        Parallel_Util::group_sum(centoid_x, orbital_centoid_x[ispin], number_of_orbitals[ispin] - this->iroot);
        Parallel_Util::group_sum(centoid_y, orbital_centoid_y[ispin], number_of_orbitals[ispin] - this->iroot);
        Parallel_Util::group_sum(centoid_z, orbital_centoid_z[ispin], number_of_orbitals[ispin] - this->iroot);
        delete[] centoid_x; delete[] centoid_y; delete[] centoid_z;

        Parallel_Util::group_sum(centoid_x2, orbital_centoid_x2[ispin], number_of_orbitals[ispin] - this->iroot);
        Parallel_Util::group_sum(centoid_y2, orbital_centoid_y2[ispin], number_of_orbitals[ispin] - this->iroot);
        Parallel_Util::group_sum(centoid_z2, orbital_centoid_z2[ispin], number_of_orbitals[ispin] - this->iroot);
        delete[] centoid_x2; delete[] centoid_y2; delete[] centoid_z2;
    }


    // Orbital spatial overlap (lambda) calculation from DOI: 10.1063/1.2831900
    vector< vector<double> > kappa(num_excitation);
    vector<double> kappa2(num_excitation);
    for(int i = 0; i < num_excitation; ++i){
        kappa[i] = vector<double>(z_vec[i].begin(), z_vec[i].end());
        for(int j = 0; j < mat_size; ++j){
            kappa[i][j] *= eps_diff[j];
            kappa2[i] += kappa[i][j]*kappa[i][j];
        }
    }

    vector<double> lambda(num_excitation);
    // CT excitation distance (Delta-r index) calculation from DOI: 10.1021/ct400337e
    vector<double> r_ind(num_excitation);

    for(int i = 0; i < num_excitation; ++i){
        for(int j = 0; j < mat_size; ++j){
            int ispin, ji, ja;
            this -> unpack_index(j, ispin, ji, ja);
            double dx2 = pow(orbital_centoid_x[ispin][ji]-orbital_centoid_x[ispin][ja],2);
            double dy2 = pow(orbital_centoid_y[ispin][ji]-orbital_centoid_y[ispin][ja],2);
            double dz2 = pow(orbital_centoid_z[ispin][ji]-orbital_centoid_z[ispin][ja],2);
            r_ind[i] += kappa[i][j]*kappa[i][j]*std::sqrt(dx2+dy2+dz2);
            lambda[i] += kappa[i][j]*kappa[i][j]*orbital_overlap[j];
        }
    }

    for(int i = 0; i < num_excitation; ++i){
        r_ind[i] /= kappa2[i];
        lambda[i] /= kappa2[i];
    }

    vector< vector<double> > var_x(spin_size), var_y(spin_size), var_z(spin_size);
    for(int ispin = 0; ispin < spin_size; ++ispin){
        var_x[ispin].resize(number_of_orbitals[ispin]), var_y[ispin].resize(number_of_orbitals[ispin]), var_z[ispin].resize(number_of_orbitals[ispin]);
        for(int i = 0; i < number_of_orbitals[ispin]; ++i){
            var_x[ispin][i] = orbital_centoid_x2[ispin][i]-pow(orbital_centoid_x[ispin][i],2);
            var_y[ispin][i] = orbital_centoid_y2[ispin][i]-pow(orbital_centoid_y[ispin][i],2);
            var_z[ispin][i] = orbital_centoid_z2[ispin][i]-pow(orbital_centoid_z[ispin][i],2);
        }
    }

    spatial_overlap = vector<double>(orbital_overlap, orbital_overlap+mat_size);
    ct_character.push_back(lambda);
    delete[] orbital_overlap;
    // delete[] orbital_centoid_r at next section.
    ct_character.push_back(r_ind);
    // END CT excitation distance (Delta-r index) calculation from DOI: 10.1021/ct400337e
    delete[] orbital_centoid_x; delete[] orbital_centoid_y; delete[] orbital_centoid_z;
    delete[] orbital_centoid_x2; delete[] orbital_centoid_y2; delete[] orbital_centoid_z2;
    // END Orbital spatial overlap calculation from DOI: 10.1063/1.2831900

    vector<double> sigma_ind(num_excitation);
    vector<double> lambda_ind(r_ind.begin(), r_ind.end());

    for(int i = 0; i < num_excitation; ++i){
        for(int j = 0; j < mat_size; ++j){
            int s, ji, ja;
            this -> unpack_index(j, s, ji, ja);
            sigma_ind[i] += kappa[i][j]*kappa[i][j]*std::abs(std::sqrt(var_x[s][ja]+var_y[s][ja]+var_z[s][ja])-std::sqrt(var_x[s][ji]+var_y[s][ji]+var_z[s][ji]));
        }
        sigma_ind[i] /= kappa2[i];
        lambda_ind[i] += sigma_ind[i];
    }
    ct_character.push_back(lambda_ind);
    internal_timer->end("TDDFT CTcal");
    Verbose::set_numformat(Verbose::Time);
    internal_timer->print(Verbose::single(Verbose::Simple), "TDDFT CTcal");
    Verbose::single(Verbose::Normal) << "Gamma index calculation end" << std::endl;
    // END CT excitation distance (Gamma index) calculation from DOI: 10.1063/1.4867007
}

void TDDFT::print_contribution(RCP< Epetra_MultiVector > eigen_vectors, int index, vector< vector<int> > determinant_index, int num_request, double tolerance, bool upper_half/* = false*/){
    double* evec = eigen_vectors->operator[](index);
    ///////////////// Gather values on root CPU /////////////
    int total_size = eigen_vectors->GlobalLength();
    double* total_evec = new double[total_size];

    if(upper_half){
        total_size /= 2;
    }
    int* total_index = new int[total_size];
    for(int i =0; i<total_size;++i){
        total_index[i]=i;
    }
    vector< vector<double> > z_vec = this -> gather_z_vector(eigen_vectors, this -> state -> orbital_energies);
    memcpy(total_evec, z_vec[index].data(), total_size*sizeof(double));
    ///////////////////////////// Sort & Print /////////////////////////////////
    Verbose::set_numformat(Verbose::Scientific);
    if(Parallel_Manager::info().get_mpi_rank()==0){
        std::sort(total_index, total_index+total_size, [&total_evec](int i1, int i2) {return total_evec[i1]*total_evec[i1] > total_evec[i2]*total_evec[i2];});

#ifdef _ACE_NEW_CONTRIB_
        double total_eps = 0.0;
        for(int i = 0; i < total_size; ++i){
            total_eps += pow(total_evec[i]*this->eps_diff[i],2);
        }
        Verbose::single(Verbose::Simple) << "     Eigenvalue contribution = " << std::scientific << total_eps << "\n";
#endif
        for(int i=0; i<num_request; ++i){
            if(fabs(total_evec[total_index[i]])<tolerance) break;
            int i2 = total_index[i];
            int s_, i_, a_;
            this -> unpack_index(i2, s_, i_, a_);
            string spin_char = (!is_open_shell)? string(""): (s_ == 0)? "A": "B";
#ifdef _ACE_NEW_CONTRIB_
            Verbose::single(Verbose::Simple) << "      " << setw(4) << setiosflags(ios::right) << i2 << ": " << setw(15) << setiosflags(ios::right) << total_evec[i2]
                << " (" << setw(4) << setiosflags(ios::right) << i_+1 << spin_char << " to " << setw(4) << setiosflags(ios::right) << a_+1 << spin_char << ")"
                << ", (Eigval = " << this -> eps_diff[i2]*this -> eps_diff[i2] * _HA_TO_EV_ << " eV, overlap = " << this -> spatial_overlap[i2] << ")"
                << "\n";
#else
            Verbose::single(Verbose::Simple) << "      " << setw(4) << setiosflags(ios::right) << i2 << ": " << setw(15) << setiosflags(ios::right) << total_evec[i2]
                << " (" << setw(4) << setiosflags(ios::right) << i_+1 << spin_char << " to " << setw(4) << setiosflags(ios::right) << a_+1 << spin_char << ")"
                <<"\n";
#endif
        }
    }
    Verbose::set_numformat();

    delete[] total_evec;
    delete[] total_index;

    return;
}

void TDDFT::initialize_orbital_numbers(RCP<State> &state){
    auto occupations = state -> get_occupations();
    number_of_orbitals = vector<int>(occupations.size());
    number_of_occupied_orbitals = vector<int>(occupations.size());

    std::vector<int> number_of_electrons = vector<int>(occupations.size());

    //this -> iroot = this -> parameters -> sublist("TDDFT").get<int>("Root", 1)-1;
    this -> iroot = 0;
    if(this -> iroot < 0) this -> iroot = 0;

    for(int i_spin=0; i_spin<occupations.size(); ++i_spin){
        number_of_orbitals[i_spin] = occupations[i_spin]->get_size();
        number_of_electrons[i_spin] = ceil(occupations[i_spin]->get_total_occupation());
        number_of_occupied_orbitals[i_spin] = number_of_orbitals[i_spin];

        if(occupations.size() == 1){
            number_of_electrons[0] = ceil(0.5*(occupations[0]->get_total_occupation()));
        }

        if(number_of_electrons[i_spin] < number_of_occupied_orbitals[i_spin]){
            for(int i=number_of_orbitals[i_spin]-1; i>=0; --i){
                if(occupations[i_spin]->operator[](i) < 1.0E-30){
                    --number_of_occupied_orbitals[i_spin];
                }
                else{
                    break;
                }
            }
        }
        
        Verbose::single(Verbose::Simple) << "\n#----------------------------------------- TDDFT::initialize_orbital_numbers" << endl;
        Verbose::single(Verbose::Simple) << " starting orbital: " << iroot << "\tnumber_of_orbitals[" << i_spin << "] : " << number_of_orbitals[i_spin] << "\tnumber_of_occupied_orbitals: " << number_of_occupied_orbitals[i_spin] << "\tnumber_of_electrons: " << number_of_electrons[i_spin] << endl;
        Verbose::single(Verbose::Simple) << "#---------------------------------------------------------------------------" << endl;

        if(this -> iroot > number_of_occupied_orbitals[i_spin]){
            Verbose::all() << "No occupied orbitals included for calculations!" << std::endl;
            throw std::invalid_argument("No occupied orbitals included for calculations!");
        }

        if(number_of_orbitals[i_spin] <= number_of_occupied_orbitals[i_spin]){
            Verbose::all() << "No virtual orbitals included for calculations!" << std::endl;
            throw std::invalid_argument("No virtual orbitals included for calculations!");
        }
    }
    return;
}

void TDDFT::get_full_TD_matrix(
        vector<string> functionals,
        vector<double> portions,
        int iroot,
        RCP<State> state,
        RCP<Epetra_CrsMatrix> &sparse_matrix
){
    bool multiply_orbitals = false; bool calculate_grad = false;
    bool calculate_pgg = false;
    double hf_portion = 0.0; double ks_ci_portion = 0.0;

    // Freezing core orbital is not implemented!
    assert(iroot == 0);
    for(vector<string>::iterator it = functionals.begin(); it != functionals.end(); ++it){
        if(*it == "LDA"){
            multiply_orbitals = true;
        } else if(*it == "GGA"){
            multiply_orbitals = true;
            calculate_grad = true;
        } else if(*it == "PGG"){
            calculate_pgg = true;
        } else if(*it == "HF"){
            hf_portion = portions.at(it-functionals.begin());
        } else if(*it == "HF-EXX"){
            ks_ci_portion = portions.at(it-functionals.begin());
        } else {
            throw std::invalid_argument("Unsupported functional (" + *it + ") found.");
        }
    }
    bool is_pgg_upper_only = (this -> type == "TDA")? true: false;

    auto map = basis->get_map();
    int spin_size = (is_open_shell)? 2: 1;

    int matrix_index_c = 0;
    int matrix_index_r = 0;

    Array< RCP<Epetra_MultiVector> > orbitals;
    Array< RCP<Epetra_MultiVector> > density_gradient;
    for(int i_spin=0; i_spin < spin_size; ++i_spin){
        orbitals.push_back(rcp(new Epetra_MultiVector(*map, state->get_orbitals()[i_spin]->NumVectors())));
        orbitals[i_spin]->Update(1.0, *state->get_orbitals()[i_spin], 0.0);
        Value_Coef::Value_Coef(basis, orbitals[i_spin], false, true, orbitals[i_spin]);
        if(calculate_grad){
            density_gradient.push_back(rcp(new Epetra_MultiVector(*map, 3)));
        }
    }

    std::vector<double> iajb = get_iajb(orbitals, false);
    int index_iajb = 0;
    RCP<Epetra_Vector> Kia = rcp(new Epetra_Vector(*map));

    // For LDA & GGA
    RCP<Epetra_Vector> ia_coeff, jb_coeff;
    if(multiply_orbitals){
        ia_coeff = rcp(new Epetra_Vector(*map));
        jb_coeff = rcp(new Epetra_Vector(*map));
    }
    // end

    // For GGA
    // grad_ia, grad_jb: ia -> s1, jb -> s2.
    RCP<Epetra_MultiVector> grad_ia, grad_jb;
    // vsigma_grad_ia: (uu)(u), (du)(u) or (dd)(d), (ud)(d).
    Array< RCP<Epetra_MultiVector> > vsigma_grad_ia;
    // v2rho2_ia_coeff: (uu)(u), (du)(u) or (dd)(d), (ud)(d).
    Array< RCP<Epetra_Vector> > v2rho2_ia_coeff;
    // grad_rho_dot_grad_ia, grad_rho_dot_grad_jb: u(u), d(u) or d(d), u(d)
    Array< RCP<Epetra_Vector> > grad_rho_dot_grad_ia, grad_rho_dot_grad_jb;
    RCP<Epetra_MultiVector> tmp_mv;
    Array< Teuchos::RCP<Epetra_Vector> > vsigma;// uu, ud, dd
    Array< Teuchos::RCP<Epetra_Vector> > v2rho2;// uu, ud, dd
    Array< RCP<Epetra_Vector> > vsigma_coeff(is_open_shell? 3: 1);
    if(calculate_grad){
        this -> exchange_correlation -> get_saved_grad_density(density_gradient);
        grad_ia = rcp(new Epetra_MultiVector(*map, 3));
        grad_jb = rcp(new Epetra_MultiVector(*map, 3));
        for(int s = 0; s < spin_size; ++s){
            vsigma_grad_ia.append(rcp(new Epetra_MultiVector(*map, 3)));
            grad_rho_dot_grad_ia.append(rcp(new Epetra_Vector(*map)));
            grad_rho_dot_grad_jb.append(rcp(new Epetra_Vector(*map)));
            v2rho2_ia_coeff.append(rcp(new Epetra_Vector(*map)));
        }
        tmp_mv = rcp(new Epetra_MultiVector(*map, 3));

        this -> construct_gradient(orbitals, this -> new_orbital_grad);
        vsigma = exchange_correlation->get_vsigma();
        v2rho2 = exchange_correlation->get_v2rho2();
        for(int s = 0; s < vsigma.size(); ++s){
            Value_Coef::Value_Coef(basis, vsigma[s], true, false, vsigma_coeff[s]);
        }
    }
    // end
    // fxc^PGG contribution
    RCP<Epetra_Vector> i_over_density, j_over_density;
    RCP<Epetra_Vector> ia_over_density, jb_over_density;
    RCP<Epetra_Vector> kernel_ia;
    if(calculate_pgg){
        i_over_density = rcp(new Epetra_Vector(*map));
        ia_over_density = rcp(new Epetra_Vector(*map));
        kernel_ia = rcp(new Epetra_Vector(*map));
        j_over_density = rcp(new Epetra_Vector(*map));
        jb_over_density = rcp(new Epetra_Vector(*map));
    }
    // end

    const int index_max = this -> matrix_dimension / this -> mat_dim_scale;

    int mat_i_prev = -1;
    for(int mat_i = 0; mat_i < index_max; ++mat_i){
        int s1, i, a;
        this -> unpack_index(mat_i, s1, i, a);
        // fxc^PGG contribution
        if(mat_i_prev != i){
#if DEBUG == 1
            internal_timer -> start("i-loop");
#endif
            if(calculate_pgg){
                i_over_density->ReciprocalMultiply(1.0, *state->get_density()[s1], *orbitals[s1]->operator()(i), 0.0);
            }
            mat_i_prev = i;
        }
        // end
        if(multiply_orbitals){
            ia_coeff->Multiply(1.0, *state->get_orbitals()[s1]->operator()(i), *orbitals()[s1]->operator()(a), 0.0);
        }

        if(calculate_grad){
            get_gradient_ia_RI_using_matrix(state->get_orbitals(), s1, i, a, grad_ia);
            for(int tmp_s = 0; tmp_s < spin_size; ++tmp_s){
                int s_ = (tmp_s == 0)? s1: 1-s1;
                // s1 == 0: uu, du / s1 == 1: dd, ud
                tmp_mv->Multiply(1.0, *density_gradient[s_], *grad_ia, 0.0);
                grad_rho_dot_grad_ia[tmp_s]->Update(1.0, *tmp_mv->operator()(0), 0.0);
                grad_rho_dot_grad_ia[tmp_s]->Update(1.0, *tmp_mv->operator()(1), 1.0);
                grad_rho_dot_grad_ia[tmp_s]->Update(1.0, *tmp_mv->operator()(2), 1.0);

                // s1 == 0: u0, u1 / s1 == 1: d2, d1
                int s_vsigma = (tmp_s == 0)? 2*s1: 1;
                vsigma_grad_ia[tmp_s]->operator()(0)->Multiply(1.0, *grad_ia->operator()(0), *vsigma_coeff[s_vsigma], 0.0);
                vsigma_grad_ia[tmp_s]->operator()(1)->Multiply(1.0, *grad_ia->operator()(1), *vsigma_coeff[s_vsigma], 0.0);
                vsigma_grad_ia[tmp_s]->operator()(2)->Multiply(1.0, *grad_ia->operator()(2), *vsigma_coeff[s_vsigma], 0.0);
                // s1 == 0: u0, u1 / s1 == 1: d2, d1
                v2rho2_ia_coeff[tmp_s]->Multiply(1.0, *ia_coeff, *v2rho2[tmp_s], 0.0);
            }
        }

        if(calculate_pgg){
            RCP<Time> time = TimeMonitor::getNewCounter("TDDFT::Kernel_PGG");
            time->start();
            ia_over_density->Multiply(1.0, *i_over_density, *orbitals[s1]->operator()(a), 0.0);
            kernel_ia = this -> calculate_PGG_kernel_ia(ia_over_density, orbitals[s1], number_of_occupied_orbitals[s1], is_pgg_upper_only);
            Value_Coef::Value_Coef(basis, kernel_ia, true, false, kernel_ia); // becomes coeff
            Verbose::single(Verbose::Simple) << " TDDFT::Kernel_PGG - Time to construct KLI-PGG kernel for (s, i,a) = (" << s1 << ", " <<  i+1 << "," << a+1 << ") : " << time->stop() << " s\n";
        }

        int mat_j_prev = -1;
        for(int mat_j = mat_i; mat_j < index_max; ++mat_j){
            int s2, j, b;
            this -> unpack_index(mat_j, s2, j, b);
            // fxc^PGG contribution
            if(mat_j_prev != j and calculate_pgg){
                j_over_density->ReciprocalMultiply(1.0, *state->get_density()[s2], *orbitals[s2]->operator()(j), 0.0);
                mat_j_prev = j;
            }
            // end

            if(multiply_orbitals){
                jb_coeff->Multiply(1.0, *state->get_orbitals()[s2]->operator()(j), *orbitals()[s2]->operator()(b), 0.0);
            }

            if(calculate_grad){
                get_gradient_ia_RI_using_matrix(state->get_orbitals(), s2, j, b, grad_jb);
                for(int tmp_s = 0; tmp_s < spin_size; ++tmp_s){
                    int s_ = (tmp_s == 0)? s2: 1-s2;
                    tmp_mv->Multiply(1.0, *density_gradient[s_], *grad_jb, 0.0);
                    grad_rho_dot_grad_jb[tmp_s]->Update(1.0, *tmp_mv->operator()(0), 0.0);
                    grad_rho_dot_grad_jb[tmp_s]->Update(1.0, *tmp_mv->operator()(1), 1.0);
                    grad_rho_dot_grad_jb[tmp_s]->Update(1.0, *tmp_mv->operator()(2), 1.0);
                }
            }

            double total_PGG = 0.0;
            // fxc^PGG contribution
            if(calculate_pgg and s1 == s2){
                jb_over_density->Multiply(1.0, *j_over_density, *state->get_orbitals()[s2]->operator()(b), 0.0);
                kernel_ia -> Dot(*jb_over_density, &total_PGG);
                if(is_open_shell){
                    total_PGG = -total_PGG;
                } else {
                    // -4 from restricted (from using total density instead of spin density) and 1/2 from restricted singlet decoupled matrix formalism.
                    total_PGG *= -2.0;
                }
            }
            // end

            // Hartree_contrib is (ia|jb)
            double Hartree_contrib = iajb.at(index_iajb);
            internal_timer -> start("kernel");
            
            double xc_contrib = exchange_correlation->ia_fxc_jb(
                orbitals, ia_coeff, jb_coeff, grad_ia, grad_jb, vsigma_grad_ia, v2rho2_ia_coeff, 
                grad_rho_dot_grad_ia, grad_rho_dot_grad_jb, s1, i, a, s2, j, b, total_PGG
            );

            internal_timer -> end("kernel");
#if DEBUG == 1
            internal_timer -> start("fill time");
#endif
            this -> fill_TD_matrix(state -> orbital_energies, Hartree_contrib, xc_contrib, mat_i, mat_j, sparse_matrix);
#if DEBUG == 1
            internal_timer -> end("fill time");

#endif
            ++index_iajb;
        }
#if DEBUG == 1
        if(a+1 == number_of_orbitals[s1]){
            internal_timer -> end("i-loop");
            Verbose::single(Verbose::Simple) << "TDDFT::get_TD_matrix index for i = " << i << " time = " << internal_timer -> get_elapsed_time("i-loop",-1) << std::endl;
            //internal_timer -> print(Verbose::single());
        }
#endif
    }
    if(hf_portion > this -> cutoff){
        Verbose::set_numformat(Verbose::Normal);
        Verbose::single(Verbose::Detail) << "HF portion: " << hf_portion << std::endl;
        internal_timer -> start("TDDFT::HF exchange");

        this -> Kernel_TDHF_X(state, sparse_matrix, true, false, hf_portion); // kernel is not hybrid, and do NOT add delta term
        internal_timer -> end("TDDFT::HF exchange");

        Verbose::set_numformat(Verbose::Time);
        Verbose::single(Verbose::Simple) << "\n#------------------------------------------------------ TDDFT::compute\n";
        internal_timer->print(Verbose::single(Verbose::Simple), "TDDFT::HF exchange");
        Verbose::single(Verbose::Simple) << "#---------------------------------------------------------------------\n";
    }
    if(ks_ci_portion > this -> cutoff){
        bool kernel_is_not_hybrid = false;
        if(this -> exchange_correlation -> get_functional_type() == "KLI-PGG"){
            kernel_is_not_hybrid = true;
        }
        Verbose::set_numformat(Verbose::Normal);
        Verbose::single(Verbose::Detail) << "HF-EXX portion: " << ks_ci_portion << std::endl;
        internal_timer -> start("TDDFT::HF-EXX exchange");
        
        this -> Kernel_TDHF_X(state, sparse_matrix, kernel_is_not_hybrid, true, ks_ci_portion); // ADD delta term
        internal_timer -> end("TDDFT::HF-EXX exchange");

        Verbose::set_numformat(Verbose::Time);
        Verbose::single(Verbose::Simple) << "\n#------------------------------------------------------ TDDFT::compute\n";
        internal_timer->print(Verbose::single(Verbose::Simple), "TDDFT::HF-EXX exchange");
        Verbose::single(Verbose::Simple) << "#---------------------------------------------------------------------\n";
    }
    return;
}

RCP<Epetra_Vector> TDDFT::calculate_PGG_kernel_ia(
        RCP<Epetra_Vector> ia_over_density,
        RCP<Epetra_MultiVector> orbital,
        int end, bool is_upper_only
){
    auto map = basis->get_map();
    RCP<Epetra_Vector> kernel_ia = rcp(new Epetra_Vector(*map));
    RCP<Epetra_Vector> kernel_ia_tmp = rcp(new Epetra_Vector(*map));
    RCP<Epetra_Vector> iak_over_density = rcp(new Epetra_Vector(*map));
    Array< RCP<Epetra_Vector> > iakl_over_density;
    iakl_over_density.push_back(rcp(new Epetra_Vector(*map)));

    for(int k=0; k<end; ++k){
        iak_over_density->Multiply(1.0, *ia_over_density, *orbital->operator()(k), 0.0);
        for(int l=k; l<end; ++l){
            iakl_over_density[0]->Multiply(1.0, *iak_over_density, *orbital->operator()(l), 0.0);

            double tmp_val = 0.0;
            kernel_ia_tmp->PutScalar(0.0);
            poisson_solver->compute(iakl_over_density, kernel_ia_tmp, tmp_val);

            kernel_ia_tmp->Multiply(1.0, *orbital->operator()(k), *kernel_ia_tmp, 0.0);
            kernel_ia_tmp->Multiply(1.0, *orbital->operator()(l), *kernel_ia_tmp, 0.0);

            double coeff = 1.0;
            if(l != k and !is_upper_only){
                coeff = 2.0;
            }
            kernel_ia->Update(coeff, *kernel_ia_tmp, 1.0);
        }
    }
    return kernel_ia;
}

void TDDFT::get_gradient_ia_RI_using_matrix(
        Teuchos::Array< RCP<const Epetra_MultiVector> > orbital_coeff, 
        int s, int i, int a, 
        RCP<Epetra_MultiVector>& grad_ia
){
    auto map = basis->get_map();
    //RCP<Epetra_MultiVector> tmp = rcp(new Epetra_MultiVector(*grad_ia));
    if(this -> new_orbital_grad){
        assert(gradient.size() > 0);
        grad_ia->operator()(0)->Multiply(1.0, *gradient[s][0]->operator()(i), *orbital_coeff[s]->operator()(a), 0.0);
        grad_ia->operator()(0)->Multiply(1.0, *gradient[s][0]->operator()(a), *orbital_coeff[s]->operator()(i), 1.0);
                                                                                               
        grad_ia->operator()(1)->Multiply(1.0, *gradient[s][1]->operator()(i), *orbital_coeff[s]->operator()(a), 0.0);
        grad_ia->operator()(1)->Multiply(1.0, *gradient[s][1]->operator()(a), *orbital_coeff[s]->operator()(i), 1.0);
                                                                                               
        grad_ia->operator()(2)->Multiply(1.0, *gradient[s][2]->operator()(i), *orbital_coeff[s]->operator()(a), 0.0);
        grad_ia->operator()(2)->Multiply(1.0, *gradient[s][2]->operator()(a), *orbital_coeff[s]->operator()(i), 1.0);
    } else {
        assert(gradient_matrix.size() == 3);
        RCP<Epetra_Vector> ia = rcp(new Epetra_Vector(*map, false));
        ia->Multiply(1.0, *orbital_coeff[s]->operator()(i), *orbital_coeff[s]->operator()(a), 0.0);
        Value_Coef::Value_Coef(basis, ia, false, true, ia);
        for(int axis=0; axis<3; axis++){
            gradient_matrix[axis]->Multiply(false, *ia, *grad_ia->operator()(axis));
        }
    }
    return;
}

void TDDFT::construct_gradient(
        Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > orbitals, 
        bool initialize_gradient
){
    RCP<Time> time = TimeMonitor::getNewCounter("TDDFT::construct_gradient_matrix");
    time->start();

    RCP<Basis_Function> basis_function = Teuchos::null;

    Verbose::single(Verbose::Normal) << "\n#------------------------------------ TDDFT::construct_gradient_matrix\n";

    if(parameters->sublist("TDDFT").isParameter("GradientMatrix")){
        if(parameters->sublist("TDDFT").get<string>("GradientMatrix") == "Finite_Difference"){
            int deriv_order = parameters->sublist("TDDFT").get<int>("DerivativesOrder", 9);
            std::array<int,3> points;
            std::array<double,3>  scaling;

            for(int d = 0; d < 3; ++d){
                points[d] = basis_function -> get_points()[d];
                scaling[d] = basis_function -> get_scaling()[d];
            }
            Verbose::single(Verbose::Normal) << " Gradient from finite difference: " << deriv_order << "-point stencil.\n";
            basis_function = rcp(new Finite_Difference(points, scaling, deriv_order));

        }
    }

    if(basis_function == Teuchos::null){
        this -> gradient_matrix = Lagrange_Derivatives::get_gradient_matrix(basis);
    } else {
        RCP<Basis> grad_basis = rcp(new Basis( basis_function, basis_function, basis -> get_grid_setting(), Teuchos::rcp_const_cast<Epetra_Map>(basis -> get_map())));
        this -> gradient_matrix = Lagrange_Derivatives::get_gradient_matrix(grad_basis);
    }

    Verbose::set_numformat(Verbose::Time);
    Verbose::single(Verbose::Normal) << "Total time for construct_gradient_matrix = " << time->stop() << " s\n";
    Verbose::single(Verbose::Normal) << "#---------------------------------------------------------------------\n";

    if(initialize_gradient){
        Verbose::single(Verbose::Detail) << "Pre-calculate density gradient." << std::endl;
        this -> gradient.resize(is_open_shell? 2: 1);
        for(int s = 0; s < this -> gradient.size(); ++s){
            for(int axis = 0; axis < 3; ++axis){
                this -> gradient[s].push_back(rcp(new Epetra_MultiVector(*basis -> get_map(), number_of_orbitals[s])));
            }
            RCP<Epetra_MultiVector> tmp = rcp(new Epetra_MultiVector(*orbitals[s]));

            for(int axis = 0; axis < 3; ++axis){
                gradient_matrix[axis] -> Multiply(false, *tmp, *this -> gradient[s][axis]);
            }
        }
    }
    return;
}

Array< RCP<Epetra_Vector> > TDDFT::get_dft_potential(RCP<State> state){
    auto map = basis->get_map();
    Array< RCP<Epetra_Vector> > xc_potential;
//    RCP<Epetra_Vector> local_potential = rcp(new Epetra_Vector(*map));
    if(state->local_potential.size()!=0){
        for(int i_spin=0;i_spin<state->local_potential.size();++i_spin){
            xc_potential.push_back(state->get_local_potential()[i_spin]);
        }

        if (state->density.size()==0){
            if (state->orbitals.size()==0){
                Verbose::all() << "TDDFT::get_dft_potential - Something wrong, no guess orbitals.\n";
                exit(EXIT_FAILURE);
            }
            else{
                Density_From_Orbitals::compute_total_density(basis,state->occupations,state->orbitals,state->density);
            }
        }

        RCP<Epetra_Vector> hartree_potential;
        double hartree_energy;
        poisson_solver->compute(state->density, hartree_potential, hartree_energy);
        Verbose::single(Verbose::Detail)<< "TDDFT::get_dft_potential Hartree potential calculation is done" <<std::endl;

        for(int i_spin=0;i_spin<state->local_potential.size();++i_spin){
            xc_potential[i_spin]->Update(-1.0,*hartree_potential, 1.0);
        }
    }
    else{
        Array< RCP<Epetra_Vector> > x_potential;
        Array< RCP<Epetra_Vector> > c_potential;
        for(int i_spin=0; i_spin<state->occupations.size(); ++i_spin){
            xc_potential.push_back(rcp(new Epetra_Vector(*map)));
            x_potential.push_back(rcp(new Epetra_Vector(*map)));
            c_potential.push_back(rcp(new Epetra_Vector(*map)));
        }
        auto tddft_xc_param = Teuchos::sublist(Teuchos::sublist(this -> parameters, "TDDFT", true), "OrbitalInfo", true);

        RCP<Exchange_Correlation> xc_class_for_orbitals = Create_Compute::Create_Exchange_Correlation(basis, tddft_xc_param, poisson_solver);

        xc_class_for_orbitals->compute_vxc(state, x_potential, c_potential);

        for(int i_spin=0; i_spin<state->occupations.size(); ++i_spin){
            xc_potential[i_spin]->Update(1.0, *x_potential[i_spin], 1.0, *c_potential[i_spin], 0.0);
        }
    }

    return xc_potential;
}

//v_sigma(r) = get_dft_potential(xc_wo_EXX) - get_dft_potential(from gs, state or "OrbitalInfo")

Teuchos::Array< Teuchos::RCP<Epetra_Vector> > TDDFT::calculate_v_diff(Teuchos::RCP<State> state, bool without_EXX, bool kernel_is_not_hybrid){
    if(kernel_is_not_hybrid){
        // TDHF(EXX)-like pure kernel!
        // v_sigma(r) = -get_dft_potential(from gs calc, state or "OrbitalInfo")  (EXX part is in Kernel_X)
        Array< RCP<Epetra_Vector> > xc_potential_KS = get_dft_potential(state);
        for(int i_spin=0; i_spin<state->occupations.size(); ++i_spin){
            xc_potential_KS[i_spin]->Scale(-1.0);
        }
        return xc_potential_KS;
    }
    else{
        //return value = get_dft_potential(xc_wo_EXX) - get_dft_potential(from gs, state or "OrbitalInfo")
        Teuchos::RCP<Exchange_Correlation> xc_GKS;
        bool no_functional_defined_except_EXX = true;
        if(without_EXX){
            // without_EXX == true
            // read XC-Functional list and remove the EXX functional terms, i.e. KLI (-12) and Slater (-11)
            auto xcwoEXX_param = Teuchos::rcp(new Teuchos::ParameterList(this -> parameters -> sublist("TDDFT").sublist("OrbitalInfo")));
            if( xcwoEXX_param -> sublist("ExchangeCorrelation").isParameter("XCFunctional") ){
                xcwoEXX_param -> sublist("ExchangeCorrelation").set<int>("AutoEXXPortion", 0);
                no_functional_defined_except_EXX = false;
                //AutoEXXPortion == 0: XC Functional without EXX potential part
            } else if(xcwoEXX_param -> sublist("ExchangeCorrelation").isParameter("XFunctional")){
                Array<string> x_functional = xcwoEXX_param -> sublist("ExchangeCorrelation").get< Array<string> >("XFunctional");
                Array<string> x_functional_new;
                for(int i = 0; i < x_functional.size(); ++i){
                    vector<string> tok = String_Util::Split_ws(x_functional[i]);
                    if(tok[0][0] != '-'){ // if the functional is not -10, -11, or -12
                        x_functional_new.push_back(x_functional[i]);
                        no_functional_defined_except_EXX = false;
                    }
                }
                xcwoEXX_param -> sublist("ExchangeCorrelation").set("XFunctional", x_functional_new);
            }
            if(!no_functional_defined_except_EXX){
                xc_GKS = Create_Compute::Create_Exchange_Correlation(basis, xcwoEXX_param, poisson_solver);
            }
        }
        else{
            no_functional_defined_except_EXX = false;
            xc_GKS = exchange_correlation;
        }
        //xc_potential_KS = get_dft_potential : orbital potential!
        Array< RCP<Epetra_Vector> > xc_potential_KS = get_dft_potential(state);
        //kernel potentials
        Array< RCP<Epetra_Vector> > x_potential_GKS;
        Array< RCP<Epetra_Vector> > c_potential_GKS;
        if(!no_functional_defined_except_EXX){
            xc_GKS->compute_vxc(state, x_potential_GKS, c_potential_GKS);
            for(int i_spin=0; i_spin<state->occupations.size(); ++i_spin){
                x_potential_GKS[i_spin]->Update(1.0, *c_potential_GKS[i_spin], 1.0);
                xc_potential_KS[i_spin]->Update(1.0, *x_potential_GKS[i_spin], -1.0);
            }
        }
        return xc_potential_KS;
    }
}

double TDDFT::potential_difference_norm(Teuchos::RCP<const Epetra_MultiVector> orbital_coeff, int i, int j, RCP<Epetra_Vector> potential_diff){
    RCP<Epetra_Vector> tmp_vec = Teuchos::rcp(new Epetra_Vector(potential_diff->Map()));
    tmp_vec->Multiply(1.0, *(orbital_coeff->operator()(i)), *potential_diff, 0.0);
    double retval = 0.0;
    tmp_vec->Dot(*(orbital_coeff->operator()(j)), &retval);
    return retval;
}

std::vector<double> TDDFT::get_iajb(
    Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > orbital_value_, 
    bool exchange
){
    Verbose::single(Verbose::Detail) << "TDDFT::get_iajb" << std::endl;
#if DEBUG == 1
    internal_timer -> start("get_iajb");
#endif
    Array< RCP<const Epetra_MultiVector> > orbital_value;
    for(int s = 0; s < orbital_value_.size(); ++s){
        orbital_value.append(Teuchos::rcp_const_cast<const Epetra_MultiVector>(orbital_value_[s]));
    }
    // Solvation correction should be only to Hartree term when solvation is on.
    bool use_solvation = this -> solvation != Teuchos::null and not exchange;
    vector<double> iajb;
    vector< vector<double> > mep_list;
    if(parameters -> sublist("TDDFT").get("MemorySaveMode", 0) == 0){
        iajb = this -> calculate_iajb_highmem(orbital_value, exchange, use_solvation, mep_list);
    } else {
        iajb = this -> calculate_iajb_lowmem(orbital_value, exchange, use_solvation, mep_list);
    }
#if DEBUG == 1
    internal_timer -> end("get_iajb");
    Verbose::set_numformat(Verbose::Time);
    Verbose::single(Verbose::Simple) << "TDDFT::get_iajb, time = " << internal_timer -> get_elapsed_time("get_iajb",-1) << std::endl;
#endif
#ifdef ACE_PCMSOLVER
    if(use_solvation){
        vector<double> iajb_solv(iajb.size());
        internal_timer -> start("get_iajb solv");
        int index = 0;

        int index_max = this -> matrix_dimension / this -> mat_dim_scale;
        int i_prev = -1;
        for(int index1 = 0; index1 < index_max; ++index1){
            int s1, i, a;
            this -> unpack_index(index1, s1, i, a);
            if(i_prev != i){
                Verbose::single(Verbose::Normal) << "TDDFT::get_iajb, solvation, i = " << i << std::endl;
                i_prev = i;
            }
            for(int index2 = index1; index2 < index_max; ++index2){
                if(index%Parallel_Manager::info().get_mpi_size()==Parallel_Manager::info().get_mpi_rank()){
                    iajb_solv.at(index) = this -> solvation -> compute_polarization_from_mep_allproc(mep_list.at(index1), mep_list.at(index2));
                }
                ++index;
            }
        }
        vector<double> iajb_solv_all(iajb.size());
        Parallel_Manager::info().group_allreduce(iajb_solv.data(), iajb_solv_all.data(), iajb.size(), Parallel_Manager::Sum);
        for(int i = 0; i < iajb.size(); ++i){
            iajb[i] += iajb_solv_all[i];
        }
        internal_timer -> end("get_iajb solv");
        Verbose::single(Verbose::Normal) << "TDDFT::get_iajb solv, time = " << internal_timer -> get_elapsed_time("get_iajb solv",-1) << std::endl;
    }
#endif
    return iajb;
}

std::vector<double> TDDFT::calculate_iajb_highmem(
    Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbital_value, 
    bool use_exchange_poisson, bool use_solvation, std::vector< std::vector<double> > &mep_list
){
    std::vector<double> tmp_iajb;
    //int size = orbital_value->operator()(0)->GlobalLength();
    const int size = basis -> get_original_size();
    const int spin_size = (is_open_shell)? 2: 1;
    double*** combine_orbital = new double**[spin_size];
    for(int s = 0; s < spin_size; ++s){
        combine_orbital[s] = new double*[number_of_orbitals[s]];
        for(int i=0; i < number_of_orbitals[s]; i++){
            combine_orbital[s][i] = new double[size]();
        }
        Parallel_Util::group_allgather_multivector(orbital_value[s], combine_orbital[s]);
    }
    double* Kia = new double[size];

    int index_max = this -> matrix_dimension / this -> mat_dim_scale;
    int i_prev = -1;
    for(int mat_i = 0; mat_i < index_max; ++mat_i){
        int s1, i, a;
        this -> unpack_index(mat_i, s1, i, a);
        if(i_prev != i){
            Verbose::single(Verbose::Detail) << "TDDFT::get_iajb, i = " << i << std::endl;
            i_prev = i;
        }
        bool check = false;
        if(mat_i%Parallel_Manager::info().get_mpi_size()==Parallel_Manager::info().get_mpi_rank()){
            memset(Kia, 0, size*sizeof(double));
            if(use_exchange_poisson){
                Two_e_integral::compute_Kji(combine_orbital[s1][i], combine_orbital[s1][a], exchange_poisson_solver, Kia, size);
            }
            else{
                Two_e_integral::compute_Kji(combine_orbital[s1][i], combine_orbital[s1][a], poisson_solver, Kia, size);
            }
            check = true;
#ifdef ACE_PCMSOLVER
            if(use_solvation){
                mep_list.push_back(this -> solvation -> interpolate_mep(Kia));
            }
        } else if(use_solvation){
            memset(Kia, 0, size*sizeof(double));
            mep_list.push_back(this -> solvation -> interpolate_mep(Kia));
#endif
        }
        for(int mat_j = mat_i; mat_j < index_max; ++mat_j){
            int s2, j, b;
            this -> unpack_index(mat_j, s2, j, b);
            if(check){
                double Hartree_contrib = Two_e_integral::compute_two_center_integral(basis, Kia, combine_orbital[s2][b], combine_orbital[s2][j], size);
                tmp_iajb.push_back(Hartree_contrib);
            }
            else{
                tmp_iajb.push_back(0.0);
            }
        }
    }
    for(int s = 0; s < spin_size; ++s){
        for(int i=0; i < number_of_orbitals[s]; ++i){
            delete [] combine_orbital[s][i];
        }
        delete[] combine_orbital[s];
    }
    delete [] combine_orbital;
    delete [] Kia;
    std::vector<double> iajb(tmp_iajb.size());
    Parallel_Util::group_sum(&tmp_iajb[0], &iajb[0], tmp_iajb.size());

    for(int i = 0; i < mep_list.size(); ++i){
        Parallel_Manager::info().group_bcast(mep_list.at(i).data(), mep_list.at(i).size(), i%Parallel_Manager::info().get_mpi_size());
    }
    return iajb;
}

std::vector<double> TDDFT::calculate_iajb_lowmem(
    Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbital_value, 
    bool use_exchange_poisson, bool use_solvation, std::vector< std::vector<double> > &mep_list
){
    std::vector<double> iajb;
    const int size = basis -> get_original_size();
    const int spin_size = (is_open_shell)? 2: 1;
    Array< RCP<Epetra_MultiVector> > orbital_value2;
    for(int s = 0; s < spin_size; ++s){
        orbital_value2.append(Teuchos::rcp_const_cast<Epetra_MultiVector>(orbital_value[s]));
    }
    RCP<Epetra_Vector> Kia = rcp(new Epetra_Vector(*basis->get_map(), false));

    int index_max = this -> matrix_dimension / this -> mat_dim_scale;
    int mat_i_prev = -1;
    for(int mat_i = 0; mat_i < index_max; ++mat_i){
        int s1, i, a;
        this -> unpack_index(mat_i, s1, i, a);
        if(mat_i_prev != mat_i){
            Verbose::single(Verbose::Normal) << "TDDFT::get_iajb, i = " << i << std::endl;
            mat_i_prev = mat_i;
        }
        Kia -> PutScalar(0.0);
        if(use_exchange_poisson){
            Two_e_integral::compute_Kji(basis, orbital_value2[s1] -> operator()(i), orbital_value2[s1] -> operator()(a), exchange_poisson_solver, Kia);
        }
        else{
            Two_e_integral::compute_Kji(basis, orbital_value2[s1] -> operator()(i), orbital_value2[s1] -> operator()(a), poisson_solver, Kia);
        }
#ifdef ACE_PCMSOLVER
        if(use_solvation){
            mep_list.push_back(this -> solvation -> interpolate_mep(Kia));
        }
#endif
        for(int mat_j = mat_i; mat_j < index_max; ++mat_j){
            int s2, j, b;
            this -> unpack_index(mat_j, s2, j, b);
            double Hartree_contrib = Two_e_integral::compute_two_center_integral(basis, Kia, orbital_value2[s2]->operator()(b), orbital_value2[s2]->operator()(j));
            iajb.push_back(Hartree_contrib);
        }
    }
    return iajb;
}

void TDDFT::print_tddft_theory_info(std::string functional_type){
    Verbose::single(Verbose::Simple) << "\n#------------------------------------------------------ TDDFT::compute\n";
    Verbose::single(Verbose::Simple) << " " + functional_type + "-type kernel is used.\n";
    Verbose::single(Verbose::Simple) << " Theory level     : " << theorylevel << "\n";
    Verbose::single(Verbose::Simple) << " Matrix dimension : " << matrix_dimension << "\n";
    std::size_t found = functional_type.find("HF-EXX");
    if(found != string::npos){
        Verbose::single(Verbose::Simple) << " Exchange kernel : HF-EXX\n";
        Verbose::single(Verbose::Simple) << "   [1] CIS : J. Kim, K. Hong, S. Choi, S.-Y. Hwang, and W.Y. Kim, Phys. Chem. Chem. Phys. 17, 31434 (2015).\n";
        Verbose::single(Verbose::Simple) << "   [2] TDDFT : unpublished.\n";
        if(!parameters -> sublist("TDDFT").sublist("OrbitalInfo").isSublist("ExchangeCorrelation")){
            throw std::invalid_argument("HF-EXX kernel requires TDDFT.OrbitalInfo.Exchange_Correlation variables!");
        }
    }
    Verbose::single(Verbose::Simple) << "#---------------------------------------------------------------------\n";
}

int TDDFT::pack_index(int s, int i, int a){
    assert(s == 0 or s == 1);
    assert(i >= iroot); assert(i < number_of_orbitals[s]); assert(a >= number_of_orbitals[s]);
    int rv = (i - iroot) * (number_of_orbitals[s] - number_of_occupied_orbitals[s]) + (a - number_of_occupied_orbitals[s]);
    if(s == 1){
        rv += (number_of_orbitals[0] - number_of_occupied_orbitals[0]) * (number_of_occupied_orbitals[0] - iroot);
    }
    return rv;
}

void TDDFT::unpack_index(int mat_ind, int &s, int &i, int &a){
    s = mat_ind < ((number_of_orbitals[0] - number_of_occupied_orbitals[0]) * (number_of_occupied_orbitals[0] - iroot))? 0: 1;
    int ia = mat_ind % ((number_of_orbitals[0] - number_of_occupied_orbitals[0]) * (number_of_occupied_orbitals[0] - iroot));
    i = ia / (number_of_orbitals[s] - number_of_occupied_orbitals[s]);
    a = ia % (number_of_orbitals[s] - number_of_occupied_orbitals[s]) + number_of_occupied_orbitals[s];
}
