#pragma once
#include "Teuchos_Array.hpp"
#include "Teuchos_RCP.hpp"

#include "Guess.hpp"
#include "../Basis/Basis.hpp"
#include "../Io/Atoms.hpp"
#include "../Core/Occupation/Occupation.hpp"
#include "../State/State.hpp"

//class State;

/**
 * @brief Random Guess for SCF cycle 
 * Random class generates random numbers for set of orbitals. 
 * Density is also obtained from randomly set of randomly generated orbital values. 
 *
 */
class Random: public Guess{
    public:
        /**
         * @brief Constructor for Random class 
         * @param [in]  occupations array of occupations 
         * @param [in]  atoms rcp pointer to refer Atoms object 
         */
        Random(
            Teuchos::Array< Teuchos::RCP<Occupation> > occupations, 
            Teuchos::RCP<const Atoms> atoms
        );

        /**
         * @brief compute function 
         * @param [in] basis    basis information 
         * @param [out] states compute function append new state whose orbitals are randomly generated 
        */
        int compute(
            Teuchos::RCP<const Basis> basis, 
            Teuchos::Array< Teuchos::RCP<State> >& states
        );

    protected:
};

