#pragma once
#include <vector>
#include "Epetra_Vector.h"
#include "Epetra_Map.h"

#include "Teuchos_RCP.hpp"

#include "../Basis/Basis.hpp"
#include "Poisson_Solver.hpp"

/**
 * @brief Calculates the Gaussian-approximated Coulomb potential.
 * @details \f[ \frac{1}{r_{12}} = \sum_i \beta_i e^{-\alpha_i r_{12}^2} \f].
 *          See J. Chem. Phys. 143, 024102 (2015).
 * @author Sungwoo Kang
 * @date 2016-10-24
**/

//class Poisson_NGau_Solver: public Compute, public Kernel_Integral{
class Gaussians_Kernel: public Poisson_Solver{
    public:
        /**
         * @brief Constructor
         * @param basis basis information
         * @param exponent List of Gaussian exponent \f[\alpha_i\f].
         * @param coeff List of Gaussian coefficient \f[\beta_i\f].
         **/
        Gaussians_Kernel(Teuchos::RCP<const Basis> basis, std::vector<double> exponent, std::vector<double> coeff, int ngpus);

        /*
        int compute(Teuchos::RCP<const Basis> basis, Teuchos::Array< Teuchos::RCP<State> >& states);
        int compute(Teuchos::Array< Teuchos::RCP<Epetra_Vector> > density, Teuchos::RCP<Epetra_Vector>& hartree_potential,double& hartree_energy);
        double get_energy(Teuchos::Array< Teuchos::RCP<Epetra_Vector> > density);
        */
        /**
         * @brief Copyed from Poisson_Solver, duplicated codes.
         **/
        virtual int compute(Teuchos::RCP<const Basis> basis, Teuchos::Array< Teuchos::RCP<State> >& states);
        using Kernel_Integral::compute;
        virtual std::string get_info_string() const;
    protected:
        /**
         * @brief
         * @author Sungwoo Kang
         **/
        virtual void t_sampling();

        /*
        double compute_F(double t, int i, int j, int axis);
        void __construct_matrix(const Epetra_BlockMap map);
        void __check_input();
        std::string output;
        double t_i;
        double t_l;
        double t_f;
        int num_points1;
        int num_points2;

        std::vector<double> t_total;
        std::vector<double> w_t;
        Teuchos::RCP<const Basis> basis ;
        Teuchos::Array< Teuchos::SerialDenseMatrix<int, double> > F_xs;
        Teuchos::Array< Teuchos::SerialDenseMatrix<int, double> > F_ys;
        Teuchos::Array< Teuchos::SerialDenseMatrix<int, double> > F_zs;

        std::vector<double> t_proc;
        std::vector<int> i_proc;

        bool fill_complete;
        */
};
