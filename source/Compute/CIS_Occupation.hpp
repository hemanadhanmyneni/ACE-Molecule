#pragma once
#include <vector>
#include "Poisson_Solver.hpp"
#include "Core_Hamiltonian.hpp"
#include "CI_Occupation.hpp"

/**
 * @brief CIS_Occupation class. But it performs calculations.
 * @author Jaechang Lim. Modification and documantation by Sungwoo Kang.
 * @note See also CIS_Occupation. Many calculations are done there.
 **/
class CIS_Occupation: public CI_Occupation{
    public:
        /**
         * @brief Constructor. Note that initializer() is called here.
         * @param parameters ParameterList with CIS sublist.
         * @param num_orbitals It seems that it is the number of occupied orbitals.
         * @param num_alpha_electrons Number of up spin electrons. num_orbitals <= num_alpha_electrons.
         * @param num_beta_electrons Number of down spin electrons. num_orbitals <= num_beta_electrons.
         * @todo Variable parameters are only used in CI_Occupation::cal_CI_Hcore(). Only HcoreByEigenvalues are used.
         **/
        CIS_Occupation(Teuchos::RCP<Teuchos::ParameterList> parameters,  int num_orbitals, int num_alpha_electrons, int num_beta_electrons);

        //these routines are used for calculation of sigma
        int cal_E2( std::vector<int> occupation1, std::vector<int> occupation2, int p, int q, int r, int s, int* index1);
        int cal_E1( std::vector<int> occupation1, std::vector<int> occupation2, int p, int q);

        int cal_E2( std::vector<int> occupation1, int p, int q, int r, int s, int* index1);
        int cal_E1( std::vector<int> occupation1, int p, int q);
        double cal_sorted_CI_element(int index1, int index2);

        int cal_D(int excitation1, int excitation2, int address1, int p, int q, int address2);
        double cal_G(int excitation1, int excitation2, int address1, int p, int q, int r, int s, int address2, int excitation3, int address3);
        double cal_sigma2_same_spin( int excitation1, int excitation2, int address1, int p, int q, int r, int s, int address2);
        double cal_sigma2_different_spin( int excitation1, int excitation2, int address1, int p, int q, int r, int s, int address2);
        double cal_sigma1( int excitation1, int excitation2, int address1, int p, int q, int address2);
        int cal_D(int excitation1, int address1, int p, int q);
        double cal_G(int excitation1, int address1, int p, int q, int r, int s, int excitation3, int address3);
        double cal_sigma2_same_spin( int excitation1, int address1, int p, int q, int r, int s);
        double cal_sigma2_different_spin( int excitation1, int excitation2, int address1, int p, int q, int r, int s);
//        double cal_sigma1( int excitation1, int address1, int p, int q);
//void cal_CI_Hcore(Teuchos::Array< Teuchos::RCP< const Epetra_MultiVector> > eigenstd::vectors, RCP<Core_Hamiltonian> core_hamiltonian );
//        double cal_sigma2_beta_beta( int excitation1, int excitation2, int address1, int p, int q, int r, int s, int address2);
//        double cal_sigma2_alpha_beta( int excitation1, int excitation2, int address1, int p, int q, int r, int s, int address2);
//        double cal_sigma1_alpha_alpha( int excitation1, int excitation2, int address1, int p, int q, int address2);

        std::vector<double> cal_Hcore_energy(Teuchos::RCP< Epetra_MultiVector > CI_coefficient, int num_eigenvalues);
        Teuchos::RCP<Epetra_Vector> cal_CI_density(
                Teuchos::RCP<const Basis> basis,
                Teuchos::Array< Teuchos::RCP<Epetra_Vector> > density_of_orbital,
                Teuchos::RCP<Epetra_Vector> CI_coefficient,
                Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbital) ;
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > cal_sigma(Teuchos::Array< Teuchos::RCP< Epetra_Vector > > CI_coefficient) ;
        void make_ST_list(std::vector< std::vector< std::vector<int> > > & ST_list) ;
        std::vector< std::vector<int> > make_GD_list() ;
        int get_ST_size() ;
        int get_GD_size();
        void check_category(int coordinate, int* alpha, int* beta, int* alpha_address, int* beta_address); //input : coordinate in total_H, output : cartegory in which alpha string and beta string

        void address_to_excitation(int* excitation, int* ret_address, int address);
    protected:

        //calculate sign of given excitation
        int cal_sign(int excitation, int address);

        //these routines are used for calculate sigma
        std::vector<double> cal_sigma1_alpha_alpha(int excitation1, int address1, int excitation2, int address2, int num_blocks);
        std::vector<double> cal_sigma1_beta_beta(int excitation1, int address1, int excitation2, int address2, int num_blocks);
        std::vector<double> cal_sigma2_alpha_alpha(int excitation1, int address1, int excitation2, int address2, int num_blocks);
        std::vector<double> cal_sigma2_beta_beta(int excitation1, int address1, int excitation2, int address2, int num_blocks);
        std::vector<double> cal_sigma2_alpha_beta(int excitation1, int address1, int excitation2, int address2, int num_blocks);

        //collected coefficient
        double** combine_coefficient;

        /**
         * @brief Initializer.
         * @details Does following things.
         * 1. Calculates SY matrix.
         **/
        void initializer();

   //     double cal_sigma1(int excitation1, int address1, int excitation2, int address2);
        //int Eij(std::vector<int> occupation, int p, int q);

        //make list of signs of configurations
        void make_sign_list();
};
