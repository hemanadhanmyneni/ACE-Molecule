#include "CIS.hpp"
#include <string>
#include "Create_Compute.hpp"
#include "../Core/Diagonalize/Create_Diagonalize.hpp"

using std::endl;
using Teuchos::RCP;
using Teuchos::rcp;
using Teuchos::Array;
using std::vector;
using std::string;
using Teuchos::ParameterList;

CIS::CIS(RCP<const Basis> basis,
        RCP<ParameterList> parameters,
        Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > eigenvector,
        Array< vector<double > > orbital_energies,
        RCP<Poisson_Solver> poisson_solver,
        RCP<Exchange_Correlation> exchange_correlation)
{
    this->basis = basis;
    this -> num_electrons = static_cast<int>(parameters->sublist("BasicInformation").get<double>("NumElectrons")/2);
    this->parameters = parameters;
    this->core_hamiltonian = core_hamiltonian;
    this->poisson_solver = poisson_solver;
    this->exchange_correlation = exchange_correlation;
    auto CIS_param =  Teuchos::sublist(parameters,"CIS");
    this->diagonalize = Create_Diagonalize::Create_Diagonalize(CIS_param);
    //    cal_two_centered_integ(eigenvector);
    this -> timer = rcp(new Time_Measure());
}

void CIS::initializer(){
    this -> cis_occupation = rcp(new CIS_Occupation(parameters, this -> orbital[0] -> NumVectors(), num_electrons, num_electrons));
    num_eigenvalues = parameters->sublist("CIS").get<int>("NumberOfEigenvalues");
    num_blocks = num_eigenvalues;
    total_num_string = cis_occupation->get_string_number(0)*cis_occupation->get_string_number(0)
        + cis_occupation->get_string_number(1)*cis_occupation->get_string_number(0)
        + cis_occupation->get_string_number(0)*cis_occupation->get_string_number(1);

    Verbose::set_numformat(Verbose::Occupation);
    Verbose::single(Verbose::Simple) << "================================================" <<endl;
    Verbose::single(Verbose::Simple) << "CIS_Occupation summary" << endl;
    Verbose::single(Verbose::Simple) << "Number of orbitals : " << cis_occupation->get_num_orbitals() << endl;
    Verbose::single(Verbose::Simple) << "Number of occupied orbitals : " << cis_occupation->get_num_occupied_orbitals() << endl;
    Verbose::single(Verbose::Simple) << "Number of single exciation : " << cis_occupation->get_string_number(1) << endl;
    Verbose::single(Verbose::Simple) << "Number of total string : " << total_num_string << endl;
    Verbose::single(Verbose::Simple) << "================================================" <<endl;

    max_iteration = total_num_string/2;
    Epetra_Map CIS_map (total_num_string, 0, basis->get_map()->Comm());
    for(int i=0; i<num_blocks; i++){
        CI_coefficient.append(rcp(new Epetra_Vector(CIS_map,true)));
    }

    for(int i = 0; i<num_blocks; i++){
        if(CIS_map.LID(i)>=0)
            CI_coefficient[i]->operator[](CIS_map.LID(i)) = 1.0;
    }
    H0 = rcp(new Epetra_Vector(CIS_map,true));
    return;
}

int CIS::compute(RCP<const Basis> basis,Array<RCP<State> >& states){
    RCP<State> initial_state = states[states.size()-1];
    this->orbital = states[states.size()-1]->get_orbitals();
    initializer();
    RCP<State> final_state;

    if (core_hamiltonian==Teuchos::null)
        this->core_hamiltonian = Create_Compute::Create_Core_Hamiltonian(basis, states[states.size()-1]->get_atoms(), parameters);
    auto CIS_param =  Teuchos::sublist(parameters,"CIS");
    if(poisson_solver==Teuchos::null){
        this->poisson_solver = Create_Compute::Create_Poisson(basis, CIS_param);
    }
    this->exchange_correlation = Teuchos::null;

    Array<string> string_orbital_energies;
    Array< vector<double> > orbital_energies;
    if(parameters->sublist("CIS").isParameter("UseInitialGuess")){
        Verbose::single(Verbose::Simple) << "Reading orbital information from cube file." << endl;
    } else {
        this->orbital = initial_state->get_orbitals();
        orbital_energies = initial_state -> get_orbital_energies();
    }
    cis_occupation->cal_CI_Hcore(orbital, orbital_energies, basis, parameters->sublist("CIS").get<int>("OrbitalsFrom", 1),states ,exchange_correlation, poisson_solver, core_hamiltonian);
    cis_occupation->cal_two_centered_integ(orbital, poisson_solver,basis);
    cis_occupation->cal_CI_modified_Hcore();

    Verbose::single(Verbose::Detail) << "cal_H0" <<endl;
    cis_occupation->cal_H0(H0);
    Verbose::single(Verbose::Detail) << "start cal_total_H" << endl;
    Verbose::single(Verbose::Detail) << "start davidson" << endl;

    this -> timer -> start("CIS::diagonalize");
    vector<double> CI_energy;
//    CI_energy = Davidson();
//    CI_energy = Full_matrix_diagonalize();
    CI_energy = direct_matrix_diagonalize();
    this -> timer -> end("CIS::diagonalize");

    Verbose::set_numformat(Verbose::Energy);
    Verbose::single(Verbose::Simple) << "============================" << endl;
    if(parameters->sublist("CIS").isParameter("UseInitialGuess")){
        for(int i=0; i<num_eigenvalues;i++){
            Verbose::single(Verbose::Simple) << "CI_energy of " << i << " =                "  << CI_energy[i] << endl;
        }
    }
    else{
        for(int i=0; i<num_eigenvalues;i++){
            Verbose::single(Verbose::Simple) << "CI_energy of " << i << " = "  << CI_energy[i] + initial_state->nuclear_nuclear_repulsion << endl;
        }
    }
    Verbose::single(Verbose::Simple) << "Corrected energy of ground state  = "  << CI_energy[0] - H0->operator[](0) << endl;

    Verbose::set_numformat(Verbose::Time);
    Verbose::single(Verbose::Simple) << "diagonalize time : " << this -> timer -> get_elapsed_time("CIS::diagonalize", -1) << " s" << endl;
    Verbose::single(Verbose::Simple) << "============================" << endl;
    return 0;
}

std::vector<double> CIS::Davidson(){
    int iteration = 0;
    CI_energy = 0.0;

    Verbose::single(Verbose::Detail) << "Start Davidson1" << endl;
    int* MyGlobalElements = CI_coefficient[0]->Map().MyGlobalElements();

    Verbose::single(Verbose::Detail) << num_blocks << endl;
    Verbose::single(Verbose::Detail) << iteration << endl;
    Verbose::single(Verbose::Detail) << max_iteration << endl;
    int iteration_time = 0;
    vector<double> eigenvalues(num_eigenvalues);
    max_iteration = 100000;

    Teuchos::RCP<Epetra_Vector> temp10 = Teuchos::rcp(new Epetra_Vector(CI_coefficient[0]->Map()));

    if(CI_coefficient[0]->Map().LID(num_blocks)>=0){
        temp10->operator[](CI_coefficient[0]->Map().LID(num_blocks)) = 1.0;
    }

    Gram_Schmidt(temp10);
    if(CI_coefficient[0]->Map().LID(iteration)>=0){
        temp10->operator[](CI_coefficient[0]->Map().LID(iteration)) = 0.0;
    }
    temp10 = Teuchos::null;
    for( int iteration = num_blocks; iteration < max_iteration; iteration = iteration + num_blocks) {
        Teuchos::RCP<Epetra_Vector> temp1 = Teuchos::rcp(new Epetra_Vector(CI_coefficient[0]->Map()));
        Teuchos::RCP<Epetra_Vector> temp2 = Teuchos::rcp(new Epetra_Vector(CI_coefficient[0]->Map()));
        Teuchos::RCP<Epetra_Vector> temp3 = Teuchos::rcp(new Epetra_Vector(CI_coefficient[0]->Map()));
        Teuchos::RCP<Epetra_Vector> temp4 = Teuchos::rcp(new Epetra_Vector(CI_coefficient[0]->Map()));
        Teuchos::RCP<Epetra_Vector> temp5 = Teuchos::rcp(new Epetra_Vector(CI_coefficient[0]->Map()));
        temp3->PutScalar(1.0);

        Verbose::single(Verbose::Simple) << "===============================" << endl;
        Verbose::single(Verbose::Simple) << " Number of Iteration  " << iteration_time << endl;
        Verbose::single(Verbose::Simple) << " Sigma size   " << sigma.size() << endl;
        Verbose::single(Verbose::Simple) << " CI_coefficient size   " << CI_coefficient.size() << endl;
        Verbose::single(Verbose::Simple) << "===============================" << endl;

        ++iteration_time;
        if(iteration == num_blocks){
            Teuchos::Array< RCP< Epetra_Vector> > temp_CI_coefficient;
            for(int i=0; i<num_blocks+1; i++){
                temp_CI_coefficient.append(rcp(new Epetra_Vector(*CI_coefficient[i])));
            }
            Teuchos::Array< RCP< Epetra_Vector> > temp_sigma = cis_occupation->cal_sigma(temp_CI_coefficient);
            for(int i=0; i<num_blocks+1; i++){
                sigma.append(rcp(new Epetra_Vector(*temp_sigma[i])));
            }
        } else {
            Teuchos::Array< RCP< Epetra_Vector> > temp_CI_coefficient;
            for(int i=iteration-num_blocks+1; i<iteration+1; i++){
                temp_CI_coefficient.append(rcp(new Epetra_Vector(*CI_coefficient[i])));
            }
            Teuchos::Array< RCP< Epetra_Vector> > temp_sigma = cis_occupation->cal_sigma(temp_CI_coefficient);
            for(int i=0; i<num_blocks; i++){
                sigma.append(rcp(new Epetra_Vector(*temp_sigma[i])));
            }
        }

        int T_matrix_dim = CI_coefficient.size();
        Teuchos::RCP< Teuchos::SerialDenseMatrix<int,double> > Tmatrix = Teuchos::rcp(new Teuchos::SerialDenseMatrix<int, double> (T_matrix_dim, T_matrix_dim));
        double temp = 0;
        double ttemp = 0;
        for(int i = 0; i<T_matrix_dim; i++){
            for(int j = 0; j<T_matrix_dim; j++){
                double temp = 0.0;
                CI_coefficient[i]->Dot(*sigma[j], &temp);
                Tmatrix->operator()(i,j) = temp;
            }
        }
        double** evec = new double* [T_matrix_dim];
        double* eval = new double[T_matrix_dim];

        for(int i=0; i<T_matrix_dim; i++){
            evec[i] = new double[T_matrix_dim];
        }

        Tmatrix = Teuchos::null;
        for(int i=0; i<num_blocks; i++){
            Verbose::single() << eval[i] << "   " ;
        }

        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > eigenvectors;
        int NumMyElements = CI_coefficient[0]->Map().NumMyElements();
        for(int i = 0; i<num_blocks; i++){
            for(int j = 0; j< NumMyElements; j++){
                temp = 0;
                ttemp = 0;

                for(int k = 0; k< CI_coefficient.size()-i; k++){
                    temp += sigma[k]->operator[](j)*evec[k][i];
                    ttemp += CI_coefficient[k]->operator[](j)*evec[k][i];
                }
                temp1->ReplaceMyValue(j,0,temp);
                temp2->ReplaceMyValue(j,0,ttemp);
            }

            eigenvectors.append(Teuchos::rcp(new Epetra_Vector(*temp2)));
            temp1->Update(-eval[i], *temp2, 1.0);

            temp3->Update(-1.0, *H0, eval[i]);
            temp4->Reciprocal(*temp3);
            temp5->Multiply(1.0,*temp4, *temp1, 0.0 );
            Gram_Schmidt(temp5);
        }

        double norm = 0;
        for(int i=0; i<num_eigenvalues; i++){
            norm += sqrt((eval[i]-eigenvalues[i])*(eval[i]-eigenvalues[i]));
        }

        if(norm < parameters->sublist("CIS").get<double>("ConvergenceTolerance", 1.0E-6)){
            Verbose::single(Verbose::Simple) << "Converged" << endl;
            Verbose::single(Verbose::Simple) << "norm     " << norm << endl;
            Verbose::single(Verbose::Simple) << "eigenvalue   ";
            Verbose::set_numformat(Verbose::Energy);
            for(int i=0; i<3; i++){
                Verbose::single(Verbose::Simple) << eval[i] << "   " ;
            }
            Verbose::single(Verbose::Simple) << endl;

            print_eval(eigenvectors);
            break;
        } else{
            Verbose::single(Verbose::Simple) << "Unconverged" << endl;
            Verbose::single(Verbose::Simple) << "norm     " << norm << endl;
            for(int i=0; i<num_eigenvalues; i++){
                eigenvalues[i] = eval[i];
            }
            delete [] eval;
            for(int i=0; i<T_matrix_dim; i++)
                delete [] evec[i];
            delete [] evec;
            temp1 = Teuchos::null;
            temp2 = Teuchos::null;
            temp3 = Teuchos::null;
            temp4 = Teuchos::null;
            temp5 = Teuchos::null;
        }
    }
    return eigenvalues;
}

void CIS::Gram_Schmidt(Teuchos::RCP<Epetra_Vector> new_vector){
    double norm = 0;
    new_vector->Norm2(&norm);
    if(abs(norm) < 1.0E-21){
        Verbose::single(Verbose::Simple) << "Reinitializing Gram_Schmidt vector to one vector." << endl;
        new_vector->PutScalar(1.0);
        new_vector->Norm2(&norm);
    }
    new_vector->Scale(1/norm);
    for(int i=0; i<CI_coefficient.size(); i++){
        double overlap = 0;
        new_vector->Dot(*CI_coefficient[i],&overlap);
        new_vector->Update(-overlap, *CI_coefficient[i],1.0) ;
    }

    norm = 0.0;
    new_vector->Norm2(&norm);
    new_vector->Scale(1/norm);
    CI_coefficient.append(rcp(new Epetra_Vector(*new_vector)));
    return;
}

void CIS::print_eval(Teuchos::Array< Teuchos::RCP<Epetra_Vector > > eigenvectors){
    int* MyGlobalElements = CI_coefficient[0]->Map().MyGlobalElements();
    int NumMyElements = CI_coefficient[0]->Map().NumMyElements();

    for(int j=0; j<eigenvectors.size(); j++){
        double* tmp_combine_eigenvectors = new double[total_num_string]();
        double* combine_eigenvectors = new double[total_num_string]();
        double norm = 0;

        eigenvectors[j]->Norm2(&norm);
        eigenvectors[j]->Scale(1/norm);
        for(int i=0; i<NumMyElements; i++){
            tmp_combine_eigenvectors[MyGlobalElements[i]] = eigenvectors[j]->operator[](i);
        }
        CI_coefficient[j]->Map().Comm().SumAll(tmp_combine_eigenvectors, combine_eigenvectors, total_num_string);
        Verbose::single(Verbose::Simple) << "eigenvector of " << j << endl;
        for(int i=0; i< total_num_string; i++){
            if(abs(combine_eigenvectors[i]) > -0.01){
                Verbose::single(Verbose::Simple) << i << "   " << combine_eigenvectors[i] <<" " ;
                cis_occupation->print_configuration(i);
                Verbose::single(Verbose::Simple) << endl;
            }
        }
        Verbose::single(Verbose::Simple) << endl;
        delete [] combine_eigenvectors;
        delete [] tmp_combine_eigenvectors;
    }
}
RCP<Epetra_CrsMatrix> CIS::make_CI_matrix_crs(Teuchos::RCP<Epetra_Vector> H0){
    this -> timer -> start("CIS::make_CI_matrix");
    Epetra_Map Full_matrix_CISD_map (total_num_string, 0, basis->get_map()->Comm());
    RCP<Epetra_CrsMatrix> CI_matrix = rcp(new Epetra_FECrsMatrix (Copy,Full_matrix_CISD_map,0 ));
    Verbose::single(Verbose::Detail) << "start cal make CI matrix" << endl;
    double CI_criteria = -1.0E-10;
    for(int index1=0; index1<total_num_string; index1++){
        Verbose::single(Verbose::Detail) << index1 << " ";
        if(Full_matrix_CISD_map.LID(index1)>=0){
            vector<double> value;
            vector<int> index;
            for(int index2=0; index2<total_num_string; index2++){
                double temp = cis_occupation->cal_sorted_CI_element(index1,index2);
                if(abs(temp) > CI_criteria){
                    value.push_back(temp);
                    index.push_back(index2);
                }
                if(index1 == index2){
                    value.push_back(1.0);
                    index.push_back(index2);
                }
                int index_size = index.size();
                CI_matrix->InsertGlobalValues(index1, index_size, &value[0], &index[0]);
                value.clear();
                index.clear();
            }
        }
    }
    Verbose::single(Verbose::Detail) << endl;
    CI_matrix->FillComplete();
    CI_matrix->ReplaceDiagonalValues(*H0);
    Verbose::single(Verbose::Detail) << "make Full matrix end" << endl;
    this -> timer -> end("CIS::make_CI_matrix");
    Verbose::set_numformat(Verbose::Default);
    Verbose::single(Verbose::Simple) << "make full matrix time : " << this -> timer -> get_elapsed_time("CIS::make_CI_matrix") << " s" << endl;
    return CI_matrix;
}

std::vector<double> CIS::Full_matrix_diagonalize(){
    Epetra_Map Full_matrix_CISD_map (total_num_string, 0, basis->get_map()->Comm());
    cis_occupation->cal_pure_Hcore();
    Verbose::single(Verbose::Simple) << "Dimension of CI matrix is " << total_num_string << endl;
    auto CI_matrix = make_CI_matrix_crs(H0);
    Verbose::single(Verbose::Normal) << "density of CI matrix is " << static_cast<double>(CI_matrix->NumGlobalNonzeros())/total_num_string/total_num_string << endl;
    RCP<Epetra_MultiVector> initial_eigenvectors = rcp(new Epetra_MultiVector(Full_matrix_CISD_map, num_eigenvalues));
    RCP<Epetra_MultiVector> eigenvectors;
    for(int i=0; i<num_eigenvalues; i++){
        if(Full_matrix_CISD_map.LID(i)>=0){
            initial_eigenvectors->operator[](i)[i] = 1.0;
        }
    }

    int iteration = 0;
    int block_size = parameters->sublist("CIS").sublist("Diagonalize").get<int>("BlockSize", parameters->sublist("CIS").get<int>("NumberOfEigenvalues"));
    while(true){
        iteration++;
        diagonalize->diagonalize(CI_matrix, num_eigenvalues, initial_eigenvectors);
        Verbose::single(Verbose::Detail) << "eigenvalues check" << endl;
        vector<double > eigenvalues = diagonalize->get_eigenvalues();
        Verbose::single(Verbose::Normal) << "eigenvalues by full matrix diagonalize" << endl;
        for(int i=0; i<num_eigenvalues; i++){
            Verbose::single(Verbose::Normal) << eigenvalues[i] << endl;
        }
        if (diagonalize->returnCode == Anasazi::Converged){
            break;
        }
        else{
            parameters->sublist("CIS").sublist("Diagonalize").set<int>("BlockSize", block_size*(iteration+1));
            auto CISD_param =  Teuchos::sublist(parameters,"CIS");
            initial_eigenvectors = diagonalize->get_eigenvectors();

            diagonalize = Create_Diagonalize::Create_Diagonalize(CISD_param);
        }
    }
    eigenvectors = diagonalize->get_eigenvectors();
    //for(int i=0; i<CI_coefficient.size(); i++)
    //     CI_coefficient[i] = Teuchos::null;
    //CI_coefficient.~Array();
    CI_coefficient.clear();
    for(int i=0; i< eigenvectors->NumVectors(); i++){
        CI_coefficient.append(rcp(new Epetra_Vector(*eigenvectors->operator()(i))));
    }
    int* MyGlobalElements = Full_matrix_CISD_map.MyGlobalElements();
    int NumMyElements = Full_matrix_CISD_map.NumMyElements();

    Verbose::set_numformat(Verbose::Energy);
    for(int j=0; j<num_eigenvalues; j++){
        double* tmp_combine_eigenvectors = new double[total_num_string]();
        double* combine_eigenvectors = new double[total_num_string]();
        //memset(combine_eigenvectors, 0.0, sizeof(double)*total_num_string);
        //memset(tmp_combine_eigenvectors, 0.0, sizeof(double)*total_num_string);
        for(int i=0; i<NumMyElements; i++){
            tmp_combine_eigenvectors[MyGlobalElements[i]] = eigenvectors->operator[](j)[i];
        }
        Full_matrix_CISD_map.Comm().SumAll(tmp_combine_eigenvectors, combine_eigenvectors, total_num_string);

        Verbose::single(Verbose::Simple) << "eigenvector of " << j << endl;
        for(int i=0; i< total_num_string; i++){
            if(abs(combine_eigenvectors[i]) > -0.1){
                Verbose::single(Verbose::Simple) << i << "   " << combine_eigenvectors[i] <<" " ;
                cis_occupation->print_configuration(i);
                Verbose::single(Verbose::Simple) << endl;
            }
        }
        Verbose::single(Verbose::Simple) << endl;
        delete [] combine_eigenvectors;
        delete [] tmp_combine_eigenvectors;
    }
    vector<double > eigenvalues = diagonalize->get_eigenvalues();
    Verbose::single(Verbose::Normal) << "eigenvalues by full matrix diagonalize" << endl;
    for(int i=0; i<num_eigenvalues; i++){
        Verbose::single(Verbose::Normal) << eigenvalues[i] << endl;
    }
    vector<double> ret_eigenvalues(num_eigenvalues);
    for(int i=0; i<num_eigenvalues; i++){
        ret_eigenvalues[i] = eigenvalues[i];
    }
    Verbose::set_numformat(Verbose::Default);
    Verbose::single(Verbose::Detail)<<  "diagonalize done" << endl;
    return ret_eigenvalues;
}

std::vector<double> CIS::direct_matrix_diagonalize(){
    Epetra_Map Full_matrix_CISD_map (total_num_string, 0, basis->get_map()->Comm());
    cis_occupation->cal_pure_Hcore();
    Verbose::single(Verbose::Simple) << "Dimension of CI matrix is " << total_num_string << endl;
    auto CI_matrix = make_CI_matrix_dense(H0);
    double** evec = new double*[total_num_string];
    for(int i = 0; i<total_num_string; i++){
        evec[i] = new double[total_num_string];
    }
    double* eval = new double[total_num_string];
    Verbose::single(Verbose::Detail) << "symmetry check" << endl;
    for(int i=0; i<total_num_string;i++){
        for(int j=0; j<total_num_string;j++){
            if(abs(CI_matrix->operator()(i,j)-CI_matrix->operator()(j,i)) > 1.0E-7){
                Verbose::set_numformat(Verbose::Scientific);
                Verbose::single(Verbose::Detail) <<"CI matrix not symmetric for index " << i << ", " << j << endl;
                Verbose::single(Verbose::Detail) << CI_matrix->operator()(i,j) << "  " << CI_matrix->operator()(j,i)<< endl;
            }
        }
    }

    direct_diagonalize(CI_matrix, eval, evec, total_num_string);
    Verbose::set_numformat(Verbose::Energy);
    for(int j=0; j<num_eigenvalues; j++){
        Verbose::single(Verbose::Simple) << "eigenvector of " << j << "   " << eval[j] << endl;
        for(int i=0; i< total_num_string; i++){
            if (i<1){
                if(abs(evec[j][i])>0.1){
                    Verbose::single(Verbose::Simple) << i << "   " << evec[j][i] <<" " ;
                    cis_occupation->print_configuration(i);
                    Verbose::single(Verbose::Simple) << endl;
                }
            } else if (i<1+ 2*cis_occupation->get_string_number(1)){
                if(abs(evec[j][i])>0.01){
                    Verbose::single(Verbose::Simple) << i << "   " << evec[j][i] <<" " ;
                    cis_occupation->print_configuration(i);
                    Verbose::single(Verbose::Simple) << endl;
                }
            }
        }
        Verbose::single(Verbose::Simple) << endl << endl;
    }
    Verbose::single(Verbose::Normal) << "eigenvalues by direct matrix diagonalize" << endl;
    for(int i=0; i<num_eigenvalues; i++){
        Verbose::single(Verbose::Normal) << eval[i] << endl;
    }
    vector<double> ret_eigenvalues(num_eigenvalues);
    for(int i=0; i<num_eigenvalues; i++){
        ret_eigenvalues[i] = eval[i];
    }
    Verbose::single(Verbose::Detail)<<  "diagonalize done" << endl;
    Verbose::set_numformat(Verbose::Default);

    delete[] eval;
    for(int i = 0; i < total_num_string; ++i){
        delete[] evec[i];
    }
    delete[] evec;

    return ret_eigenvalues;
}

Teuchos::RCP< Teuchos::SerialDenseMatrix<int,double> > CIS::make_CI_matrix_dense(Teuchos::RCP<Epetra_Vector> H0){
    this -> timer -> start("CIS::make_CI_matrix");
    Epetra_Map Full_matrix_CISD_map (total_num_string, 0, basis->get_map()->Comm());
    Verbose::single(Verbose::Detail) << "start cal make CI matrix" << endl;
    double CI_criteria = -1.0E-10;
    Teuchos::RCP< Teuchos::SerialDenseMatrix<int,double> > CI_matrix = Teuchos::rcp(new Teuchos::SerialDenseMatrix<int, double> (total_num_string, total_num_string));
    for(int index1=0; index1<total_num_string; index1++){
        //Verbose::single() << index1 << " ";
        vector<double> value;
        vector<int> index;
        for(int index2=0; index2<total_num_string; index2++){
            double temp = cis_occupation->cal_sorted_CI_element(index1,index2);
            if(abs(temp)>CI_criteria){
                CI_matrix->operator()(index1, index2) = temp;
            }
            if(index1==index2){
                CI_matrix->operator()(index1, index2) = 1;
            }
        }
    }
    int NumMyElements = H0->Map().NumMyElements();
    int* MyGlobalElements = H0->Map().MyGlobalElements();
    double* temp = new double[total_num_string]();
    double* combine = new double[total_num_string]();
    //memset(temp, 0.0, sizeof(double)*total_num_string);
    //memset(combine, 0.0, sizeof(double)*total_num_string);
    for(int j=0; j<NumMyElements; j++){
        temp[MyGlobalElements[j]] = H0->operator[](j);
    }
    H0->Map().Comm().SumAll(temp, combine, total_num_string);
    for(int j=0; j<total_num_string; j++){
        CI_matrix->operator()(j,j) = combine[j];
    }
    delete[] temp;
    delete[] combine;
    Verbose::single(Verbose::Detail) << endl;
    Verbose::single(Verbose::Detail) << "make direct matrix end" << endl;
    this -> timer -> end("CIS::make_CI_matrix");
    Verbose::single(Verbose::Simple) << "make direct matrix time : " << this -> timer -> get_elapsed_time("CIS::make_CI_matrix", -1) << " s" << endl;
    return CI_matrix;
}

void CIS::direct_diagonalize(Teuchos::RCP<Teuchos::SerialDenseMatrix<int, double> > Dmatrix, double* eval, double** evec, int matrix_size){
    double* WORK = new double[5*matrix_size];
    int info;
    Teuchos::LAPACK<int, double> lapack;
    lapack.SYEV('V', 'U', matrix_size, Dmatrix->values(), Dmatrix->stride(), eval, WORK, 5*matrix_size, &info);
    if(info!=0){
        Verbose::all() << " Diagonalize error" << info <<endl;
        exit(EXIT_FAILURE);
    }

    for(int i=0; i<matrix_size; i++) {
        for(int j=0; j<matrix_size; j++) {
            evec[i][j] = Dmatrix->operator()(j,i);
        }
    }
    delete[] WORK;
    return;
}

CIS::~CIS(){
    Verbose::set_numformat(Verbose::Time);
    this -> timer -> print(Verbose::single(Verbose::Simple));
}
