#pragma once
#include <vector>
//#include <algorithm>
#include "Teuchos_ParameterList.hpp"
#include "CI_Occupation.hpp"

class CISD_Occupation: public CI_Occupation{
    public:
        void make_ST_list(std::vector< std::vector< std::vector<int> > > & ST_list);
        int get_ST_size();
        int get_GD_size();
        std::vector< std::vector<int> > make_GD_list();
        CISD_Occupation(Teuchos::RCP<Teuchos::ParameterList> parameters, int num_orbitals, int num_alpha_electrons, int num_beta_electrons);

        //calculate sigma
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > cal_sigma(Teuchos::Array< Teuchos::RCP< Epetra_Vector > > CI_coefficient);
        std::vector<double> cal_Hcore_energy(Teuchos::RCP< Epetra_MultiVector >  CI_coefficient, int num_eigenvalues);
//        double cal_sigma1( int excitation1, int address1, int p, int q);

        //calculate density of CI eigenvectors
        Teuchos::RCP<Epetra_Vector> cal_CI_density(
                Teuchos::RCP<const Basis> basis,
                Teuchos::Array< Teuchos::RCP<Epetra_Vector> > density_of_orbital,
                Teuchos::RCP<Epetra_Vector> CI_coefficient,
                Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbital);
        //write CI density
        void write_CI_density(
                Teuchos::RCP<const Basis> basis,
                Teuchos::Array<Teuchos::RCP<State> >& states,
                Teuchos::RCP<Epetra_MultiVector> CI_coefficient,
                Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbital);
        //calculate comvine H0
        void cal_combine_H0(Teuchos::RCP<Epetra_Vector> H0);

        //find index of opposite spin
        int find_pair_spin(int index);
//void cal_CI_Hcore(Teuchos::Array< Teuchos::RCP< const Epetra_MultiVector> > eigenstd::vectors, RCP<Core_Hamiltonian> core_hamiltonian );
//        double cal_sigma2_beta_beta( int excitation1, int excitation2, int address1, int p, int q, int r, int s, int address2);
//        double cal_sigma2_alpha_beta( int excitation1, int excitation2, int address1, int p, int q, int r, int s, int address2);
//        double cal_sigma1_alpha_alpha( int excitation1, int excitation2, int address1, int p, int q, int address2);
//
//

        //make row of CI_matrix
        void make_CI_matrix_row(int occupation, std::vector<double>& value, std::vector<int>& index, Epetra_Map* CISD_map );

        //get sign of given excitation
        int get_sign(int excitation, int address);

        //obtain cartegory of given configuration
        void check_category(int coordinate, int* alpha, int* beta, int* alpha_address, int* beta_address); //input : coordinate in total_H, output : cartegory in which alpha string and beta string
    protected:

        //combine H0
        double* combine_H0;

        //calculate sign of given excitation
        int cal_sign(int excitation, int address);

        //these routines are used for calculate sigma
        void cal_sigma_alpha_alpha(Teuchos::Array< Teuchos::RCP< Epetra_Vector > > sigma, int num_blocks);
        void cal_sigma_beta_beta(Teuchos::Array< Teuchos::RCP< Epetra_Vector > > sigma, int num_blocks);
        std::vector<double> cal_sigma2_alpha_beta(int excitation1, int address1, int excitation2, int address2, int num_blocks);

        //convert address to excitation
        void address_to_excitation(int* excitation, int* ret_address, int address);

        //combine coefficient of CI eigenvector
        double** combine_coefficient;

        //initializer
        void initializer();

        //cal sign of double excitation
        int cal_double_sign(int address);
   //     double cal_sigma1(int excitation1, int address1, int excitation2, int address2);
        //int Eij(std::vector<int> occupation, int p, int q);

        //make sign list of configurations
        void make_sign_list();
        void same_spin_interaction(std::vector<int> occupation1, std::vector<int> occupation2, std::vector<double>& value, std::vector<int>& index, Epetra_Map* CISD_map);
};
