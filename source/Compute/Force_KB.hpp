#pragma once
#include <vector>
#include <string>

#include "Teuchos_ParameterList.hpp"
#include "Teuchos_RCP.hpp"
#include "Teuchos_Array.hpp"

//#include "Compute_Interface.hpp"
#include "Force.hpp"

class Force_KB: public Force {
    public:
        Force_KB(
            Teuchos::RCP<const Basis> basis,
            Teuchos::RCP<const Atoms> atoms,
            Teuchos::RCP<Nuclear_Potential> nuclear_potential,
            Teuchos::RCP<External_Field> external_field,
            Teuchos::RCP<Teuchos::ParameterList> parameters
        );

        std::vector<double> compute_NN();
        std::vector<double> compute_local(Teuchos::Array< Teuchos::RCP<const Epetra_Vector> > density, Teuchos::Array< Teuchos::RCP<const Occupation> > occupation, Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbitals, Teuchos::Array< Teuchos::Array< Teuchos::RCP< Epetra_MultiVector> > > grad_orbital);
        std::vector<double> compute_nonlocal(Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbitals, Teuchos::Array< Teuchos::RCP<const Occupation> > occupation, Teuchos::Array< Teuchos::Array< Teuchos::RCP< Epetra_MultiVector> > > grad_orbital);
        int compute(
            Teuchos::RCP<const Basis> basis,
            Teuchos::Array< Teuchos::RCP<State> >& states
        );
        //std::vector<double> get_force();


    protected:
        //Teuchos::RCP<const Basis> basis;
        //Teuchos::RCP<const Atoms> atoms;
        //Teuchos::RCP<Core_Hamiltonian> core_hamiltonian;
        //Teuchos::RCP<Teuchos::ParameterList> parameters;
        std::string force_derivative_method;

        //std::vector<double> F_tot;
};
