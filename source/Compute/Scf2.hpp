#pragma once
#include <vector>

#include "Scf.hpp"


/**
@brief Self Consistant Field calculation 
@author Kwangwoo Hong, and Sunghwan Choi, (modified by Jaewook Kim, Sungwoo Kang, and Jaechang Lim)
*/
class Scf2: public Scf{
    public:
        //vector<double> iterate(double ion_ion);
        //vector<double> nonSCF(double ion_ion);
        //Scf2(Teuchos::RCP<Core_Hamiltonian> core_hamiltonian,Teuchos::RCP<Poisson_Solver> poisson_solver,Teuchos::RCP<Exchange_Correlation> exchange_correlation, Teuchos::RCP<ParameterList> parameters);
        Scf2(
            Teuchos::RCP<const Basis> basis,
            Teuchos::RCP<Teuchos::ParameterList> parameters,
            Teuchos::RCP<Core_Hamiltonian> core_hamiltonian = Teuchos::null
        );

    protected:

/**
 * @brief Actual Itration is done in here.
 * @todo The part before the main for loop should be changed to Create_Compute_Interface
 * @todo StepSizeForReduceDiagonalizeTol modify
 * @detail Definitions of the "in_state" and "out_state" are written in
    http://ftp.abinit.org/ws07/Liege_Torrent2_SC_Mixing.pdf
    3rd page
    
    for linear mixing, we mix "in_state" and "out_state"
    for pulay mixing, we mix "in_state", which are stored in states
    (be aware that pulay mixing class stores "residue" = "out_state" - "in_state"
    in scf_states, ONLY the "in_state" should be stored (to restart the calculation),
    except for the last state(last state is "out_state", which is converged state)
   
    nth scf_states component contain follwing information:
      -  n-1th out_state orbitals & orbital energies
      -  n-1th mixed_state(or out_state) density = nth in_state density
      -  n-1th mixed_state(or out_state) local_potential = nth in_state local_potential
      -  occupations (occupation information would not be changed)
      -  core_density & core_density_grad (which come from psudopotential)
      -  n-1th mixed_state(or out_state) hartree, x, c_potential, which are components of the local_potential.
 */
        int iterate(Teuchos::RCP<State> initial_state, Teuchos::RCP<State>& final_state);
        Teuchos::Array<Teuchos::RCP<Epetra_MultiVector> > orbital_space;
        int max_iter_micro_scf;
        Teuchos::RCP<Epetra_MultiVector > project(const int i_spin, Teuchos::RCP<Epetra_MultiVector> multivector); 
        int project_core_hamiltonians(const int dimension_of_reduced_space,
                        Teuchos::Array<Teuchos::RCP<Epetra_Vector> > external_potential);
        Teuchos::Array<Teuchos::RCP<Epetra_MultiVector > > projected_core_hamiltonians;
        Teuchos::RCP<Scf_State> update_orbital_space(
                        Teuchos::RCP<Scf_State> in_state,
                        Teuchos::Array<Teuchos::RCP<Epetra_Vector> > external_potential,
                        Teuchos::Array<Teuchos::RCP<Epetra_MultiVector> > initial_vectors, 
                        Teuchos::Array<Teuchos::RCP<Epetra_MultiVector> >& new_orbital_space );

/*
        struct Convergence_Checking_Parameters{
            double energy;
            Teuchos::Array< vector<double > > eigenvalues;
            Teuchos::RCP<Epetra_MultiVector> density;
            Teuchos::RCP<Epetra_MultiVector> potential;
            Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > orbitals;
        };

        bool check_convergence(Convergence_Checking_Parameters* current,Convergence_Checking_Parameters* before);
*/
        //vector<double> calculate_energy(double ion_ion, Convergence_Checking_Parameters* convergence_current);
        //vector<double> calculate_energy_1(double ion_ion, Convergence_Checking_Parameters* convergence);
};
  

