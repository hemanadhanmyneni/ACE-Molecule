#include "Random.hpp"
#include "Epetra_MultiVector.h"
#include "../Utility/String_Util.hpp"

using Teuchos::Array;
using Teuchos::rcp;
using Teuchos::RCP;

Random::Random(Array< RCP<Occupation> > occupations, RCP<const Atoms> atoms){
    this->occupations = occupations;
    this->atoms = atoms;
}

int Random::compute(RCP<const Basis> basis, Array< RCP<State> >& states){
    initialize_states(basis,states);

    auto state = states[states.size()-1];

    Array< RCP<Epetra_MultiVector> > orbitals;
    RCP<const Epetra_Map> map = basis->get_map();
    int NumMyElements = map->NumMyElements();
    double* density = new double [NumMyElements];

    for(int i_spin=0; i_spin<occupations.size(); ++i_spin){
        const int entry_per_line = 5;
        const int lines = ceil(static_cast<float>(occupations[i_spin] -> get_size())/entry_per_line);
        const int width = String_Util::to_string(occupations[i_spin] -> get_size()).size();

        state->orbitals.append(rcp(new Epetra_MultiVector(*map, occupations[i_spin]->get_size())));
        state->orbitals[i_spin]->Random();
        state->orbitals[i_spin]->Scale(sqrt(3.0/map->NumGlobalElements()));
        double* norm2 = new double [occupations[i_spin]->get_size()];
        state->orbitals[i_spin]->Norm2(norm2);
        for(int i=0; i<occupations[i_spin]->get_size(); ++i){
            state->orbitals[i_spin]->operator()(i)->Scale(1.0/norm2[i]);
        }
        Verbose::single(Verbose::Detail) << "Random guess:: 2nd norm of initial orbitals before normalization (spin " << i_spin << "):" << std::endl;
        for(int i = 0; i < lines; ++i){
            const int num_entry = std::min(entry_per_line, occupations[i_spin] -> get_size()-entry_per_line*i);
            Verbose::single(Verbose::Detail) << std::setw(width) << entry_per_line*i+1 << " - " << std::setw(width) << entry_per_line*i+num_entry << ": ";
            for(int j = 0; j < num_entry; ++j){
                Verbose::single(Verbose::Detail) << std::setw(7) << std::setprecision(4) << norm2[entry_per_line*i+j] << "  ";
            }
            Verbose::single(Verbose::Detail) << std::endl;
        }
        state->orbitals[i_spin]->Norm2(norm2);
        Verbose::single(Verbose::Detail) << "Random guess:: 2nd norm of initial orbitals after normalization (spin " << i_spin << "):" << std::endl;
        for(int i = 0; i < lines; ++i){
            const int num_entry = std::min(entry_per_line, occupations[i_spin] -> get_size()-entry_per_line*i);
            Verbose::single(Verbose::Detail) << std::setw(width) << entry_per_line*i+1 << " - " << std::setw(width) << entry_per_line*i+num_entry << ": ";
            for(int j = 0; j < num_entry; ++j){
                Verbose::single(Verbose::Detail) << std::setw(7) << std::setprecision(4) << norm2[entry_per_line*i+j] << "  ";
            }
            Verbose::single(Verbose::Detail) << std::endl;
        }
        delete[] norm2;

        state->density.append(rcp(new Epetra_Vector(*map)));
        for(int i=0; i<occupations[i_spin]->get_size(); i++){
            state->density[i_spin]->Multiply(occupations[i_spin]->operator[](i), *state->orbitals[i_spin]->operator()(i), *state->orbitals[i_spin]->operator()(i), 1.0);
        }
        state->density[i_spin]->Scale(1.0/basis->get_scaling()[0]/basis->get_scaling()[1]/basis->get_scaling()[2]);

        double norm1 = 0.0;
        state->density[i_spin]->Norm1(&norm1);
        Verbose::single(Verbose::Detail) << "Random guess: Norm of initial density = " << norm1*basis->get_scaling()[0]*basis->get_scaling()[1]*basis->get_scaling()[2] << std::endl;

    }
    delete[] density;

    return 0;
}
