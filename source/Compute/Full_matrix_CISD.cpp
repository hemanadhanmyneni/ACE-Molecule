#include "Full_matrix_CISD.hpp"
#include <iostream>
#include "../Utility/Time_Measure.hpp"

using std::vector;
using std::endl;
using std::string;
using Teuchos::RCP;
using Teuchos::rcp;
using Teuchos::Array;
using Teuchos::ParameterList;

Full_matrix_CISD::Full_matrix_CISD(RCP<const Basis> basis, RCP<ParameterList> parameters, RCP<CI_Occupation> cisd_occupation, int num_eigenvalues){
    this->basis = basis;
    auto CISD_param =  Teuchos::sublist(parameters,"CISD");
    this->parameters = parameters;
    this->cisd_occupation = cisd_occupation;
    this-> num_eigenvalues = num_eigenvalues;
    this->diagonalize = Create_Diagonalize::Create_Diagonalize(CISD_param);

    total_num_string = cisd_occupation->get_string_number(0)*cisd_occupation->get_string_number(0)
        + cisd_occupation->get_string_number(1)*cisd_occupation->get_string_number(0)
        + cisd_occupation->get_string_number(0)*cisd_occupation->get_string_number(1)
        + cisd_occupation->get_string_number(1)*cisd_occupation->get_string_number(1)
        + cisd_occupation->get_string_number(2)*cisd_occupation->get_string_number(0)
        + cisd_occupation->get_string_number(0)*cisd_occupation->get_string_number(2);
}

std::vector<double> Full_matrix_CISD::compute(Teuchos::RCP<Epetra_Vector> H0){
    Epetra_Map Full_matrix_CISD_map (total_num_string, 0, basis->get_map()->Comm());
    cisd_occupation->cal_pure_Hcore();
    Verbose::single(Verbose::Simple) << "Dimension of CI matrix is " << total_num_string << endl;
    auto CI_matrix = make_CI_matrix(H0);
    Verbose::single(Verbose::Normal) << "Density of CI matrix is " << static_cast<double>(CI_matrix->NumGlobalNonzeros())/total_num_string/total_num_string << endl;
    RCP<Epetra_MultiVector> initial_eigenvectors = rcp(new Epetra_MultiVector(Full_matrix_CISD_map, num_eigenvalues));

    for(int i=0; i<num_eigenvalues; i++){
        if(Full_matrix_CISD_map.LID(i)>=0){
            initial_eigenvectors->operator[](i)[i] = 1.0;
        }
    }
    cisd_occupation->reduce_memory();
    /*
    double* norm2 = new double [initial_eigenvectors->NumVectors()];
    initial_eigenvectors->Norm2(norm2);
    for(int i=0; i<initial_eigenvectors->NumVectors(); i++){
        Verbose::single() <<  norm2[i] <<"\t";
        initial_eigenvectors->operator()(i)->Scale(1/norm2[i]);
    }
    */
    orbital_energies;
    if(parameters->sublist("CISD").isParameter("EigenvaluesCheck")){
        Array<string> string_orbital_energies = parameters->sublist("CISD").get<Array <string> >("InputEigenvalues");

        int numberofstates = parameters->sublist("CISD").get<int>("NumberOfStates");
        if(string_orbital_energies.size()<numberofstates){
            Verbose::all() <<"number of eigenvalues mismatch" << endl;
            exit(-1);
        }
    }
    int iteration = 0;
    int block_size = parameters->sublist("CISD").sublist("Diagonalize").get<int>("BlockSize", parameters->sublist("CISD").get<int>("NumberOfEigenvalues"));
    while(true){
        iteration++;
        diagonalize->diagonalize(CI_matrix, num_eigenvalues, initial_eigenvectors);
        Verbose::single(Verbose::Detail) << "eigenvalues check" << endl;
        vector<double > eigenvalues = diagonalize->get_eigenvalues();
        Verbose::single(Verbose::Detail) << "eigenvalues by full matrix diagonalize" << endl;
        for(int i=0; i<num_eigenvalues; i++){
            Verbose::single(Verbose::Detail) << eigenvalues[i] << endl;
        }
        if (diagonalize->returnCode == Anasazi::Converged){
            break;
        }else{
//            if(parameters->sublist("CISD").sublist("Diagonalize").isParameter("BlockSizeIncrease"))
            parameters->sublist("CISD").sublist("Diagonalize").set<int>("BlockSize", block_size*(iteration+1));
            auto CISD_param =  Teuchos::sublist(parameters,"CISD");
            initial_eigenvectors = Teuchos::null;
            initial_eigenvectors = diagonalize->get_eigenvectors();
            diagonalize = Create_Diagonalize::Create_Diagonalize(CISD_param);
            //Verbose::single() << initial_eigenvectors->operator[](0)[0] << endl;
            //Verbose::single() << initial_eigenvectors->operator[](1)[1] << endl;
            //Verbose::single() << initial_eigenvectors->operator[](2)[2] << endl;
            //Verbose::single() << initial_eigenvectors->operator[](3)[3] << endl;
            //Verbose::single() << initial_eigenvectors->operator[](4)[4] << endl;

        }
    }
    eigenvectors = diagonalize->get_eigenvectors();
    vector<double > eigenvalues = diagonalize->get_eigenvalues();
    int* MyGlobalElements = Full_matrix_CISD_map.MyGlobalElements();
    int NumMyElements = Full_matrix_CISD_map.NumMyElements();
    for(int j=0; j<num_eigenvalues; j++){
        double * type_coefficient = new double[6];
        for(int i=0; i<6; i++)
            type_coefficient[i] =0.0;
        double* tmp_combine_eigenvectors = new double[total_num_string]();
        double* combine_eigenvectors = new double[total_num_string]();
        //memset(combine_eigenvectors, 0.0, sizeof(double)*total_num_string);
        //memset(tmp_combine_eigenvectors, 0.0, sizeof(double)*total_num_string);
        for(int i=0; i<NumMyElements; i++){
            tmp_combine_eigenvectors[MyGlobalElements[i]] = eigenvectors->operator[](j)[i];
        }
        Full_matrix_CISD_map.Comm().SumAll(tmp_combine_eigenvectors, combine_eigenvectors, total_num_string);

        Verbose::set_numformat(Verbose::Energy);
        Verbose::single(Verbose::Simple) << "eigenvector of " << j << "   " << eigenvalues[j] << endl;
        double coeff_sum = 0;
        double coeff_sum1 = 0;
        double coeff_sum2 = 0;
        for(int i=0; i< total_num_string; i++){
            if (i<1){
                coeff_sum +=abs(combine_eigenvectors[i])*abs(combine_eigenvectors[i]);
                if(abs(combine_eigenvectors[i])>0.1){
                    Verbose::single(Verbose::Simple) << i << "   " << combine_eigenvectors[i] <<" " ;
                    cisd_occupation->print_configuration(i);
                    Verbose::single(Verbose::Simple) << endl;
                }
            }
            else if (i<1+ 2*cisd_occupation->get_string_number(1)){
                if(abs(combine_eigenvectors[i])>0.01){
                    Verbose::single(Verbose::Simple) << i << "   " << combine_eigenvectors[i] <<" " ;
                    cisd_occupation->print_configuration(i);
                    Verbose::single(Verbose::Simple) << endl;
                }
                coeff_sum1 +=abs(combine_eigenvectors[i])*abs(combine_eigenvectors[i]);
            }
            else{
                if(abs(combine_eigenvectors[i])>0.03){
                    Verbose::single(Verbose::Simple) << i << "   " << combine_eigenvectors[i] <<" " ;
                    cisd_occupation->print_configuration(i);
                    if(parameters->sublist("CISD").isParameter("EigenvaluesCheck")){
                        Verbose::single(Verbose::Simple) << " eigen value diff : " << get_eval_diff(i) << endl;
                    }
                    Verbose::single(Verbose::Simple) << endl;
                }
                coeff_sum2 +=abs(combine_eigenvectors[i])*abs(combine_eigenvectors[i]);
                type_coefficient[type_distinguish(i)] += abs(combine_eigenvectors[i])*abs(combine_eigenvectors[i]);
            }

        }
        Verbose::single(Verbose::Simple) << "Single excitations contribution: " << coeff_sum1 << endl;
        Verbose::single(Verbose::Simple) << "Double excitations contribution: " << coeff_sum2 << endl;
        Verbose::single(Verbose::Detail) << "Total contribution: " << coeff_sum2 + coeff_sum1 +coeff_sum  << endl << endl;
        Verbose::single(Verbose::Detail) << "Excitation of two alpha spins contribution: " << type_coefficient[0] << endl;
        Verbose::single(Verbose::Detail) << "Excitation of two beta spins contribution: " << type_coefficient[1] << endl;
        Verbose::single(Verbose::Detail) << "Excitation from 1u1d to 2u2d contribution: " << type_coefficient[2] << endl;
        Verbose::single(Verbose::Detail) << "Excitation from 1u1d to 2u3d contribution: " << type_coefficient[3] << endl;
        Verbose::single(Verbose::Detail) << "Excitation from 1u2d to 3u3d contribution: " << type_coefficient[4] << endl;
        Verbose::single(Verbose::Detail) << "Excitation from 1u2d to 3u4d contribution: " << type_coefficient[5] << endl;

        Verbose::single(Verbose::Simple) << endl;
        delete [] combine_eigenvectors;
        delete [] tmp_combine_eigenvectors;
    }

    Verbose::single(Verbose::Detail) << "eigenvalues by full matrix diagonalize" << endl;
    for(int i=0; i<num_eigenvalues; i++){
        Verbose::single(Verbose::Detail) << eigenvalues[i] << endl;
    }
    /*
    Verbose::single() << "eigenvalues imiginary part check" << endl;
    for(int i=0; i<num_eigenvalues; i++){
        Verbose::single() << eigenvalues[i] << endl;
    }
    */
    vector<double> ret_eigenvalues(num_eigenvalues);
    for(int i=0; i<num_eigenvalues; i++){
        ret_eigenvalues[i] = eigenvalues[i];
    }
    Verbose::single(Verbose::Detail)<<  "diagonalize done" << endl;
    return ret_eigenvalues;
}

RCP<Epetra_CrsMatrix> Full_matrix_CISD::make_CI_matrix(Teuchos::RCP<Epetra_Vector> H0){
    RCP<Time_Measure> timer = rcp(new Time_Measure());
    timer -> start("time");
    Epetra_Map Full_matrix_CISD_map (total_num_string, 0, basis->get_map()->Comm());
    RCP<Epetra_CrsMatrix> CI_matrix = rcp(new Epetra_FECrsMatrix (Copy,Full_matrix_CISD_map,0 ));
    Verbose::single(Verbose::Detail) << "start cal make CI matrix" << endl;
    double CI_criteria = 1.0E-10;
    if(parameters->sublist("CISD").isParameter("CIcriteria")){
        CI_criteria = parameters->sublist("CISD").get<double>("CIcriteria");
    }

    for(int index1=0; index1<total_num_string; index1++){
        //Verbose::single() << index1 << " ";
        if(Full_matrix_CISD_map.LID(index1)>=0){
            vector<double> value;
            vector<int> index;
            for(int index2=0; index2<total_num_string; index2++){
                double temp = cisd_occupation->cal_sorted_CI_element(index1,index2);
                if(abs(temp)>CI_criteria){
                  //  value.push_back(index1+index2);
                    value.push_back(temp);
                    index.push_back(index2);
                }
                if(index1==index2){
                    value.push_back(1.0);
                    index.push_back(index2);
                }

            }
            int index_size = index.size();
            CI_matrix->InsertGlobalValues(index1, index_size, &value[0], &index[0]);
            value.clear();
            index.clear();
        }
    }
    Verbose::single() << endl;
    CI_matrix->FillComplete();
    CI_matrix->ReplaceDiagonalValues(*H0);
    Verbose::single(Verbose::Detail) << "make Full matrix end" << endl;
    timer -> end("time");
    Verbose::set_numformat(Verbose::Time);
    Verbose::single(Verbose::Simple) << "Full CISD matrix construction time: " << timer -> get_elapsed_time("time", -1) << " s" << endl;
    return CI_matrix;
}
double Full_matrix_CISD::get_eval_diff(int occupation){
        vector<int> alpha_occ1;
        vector<int> alpha_occ2;
        vector<int> beta_occ1;
        vector<int> beta_occ2;
        vector<int> same_alpha_occ;
        vector<int> same_beta_occ;
        vector<int> diff_alpha_occ;
        vector<int> diff_beta_occ;
        vector<int> diff_beta_index;
        vector<int> diff_alpha_index;
        vector<int> same_beta_index;
        vector<int> same_alpha_index;
        cisd_occupation->check_same_diff_occupation(0, occupation , alpha_occ1, beta_occ1, alpha_occ2, beta_occ2, same_alpha_occ, diff_alpha_occ, same_beta_occ, diff_beta_occ, diff_alpha_index, diff_beta_index, same_alpha_index, same_beta_index);
        if(diff_alpha_occ.size()==4){
            return abs((orbital_energies[diff_alpha_occ[0]]+orbital_energies[diff_alpha_occ[1]])-(orbital_energies[diff_alpha_occ[2]]+orbital_energies[diff_alpha_occ[3]]));
        }
        else if(diff_beta_occ.size()==4){
            return abs((orbital_energies[diff_beta_occ[0]]+orbital_energies[diff_beta_occ[1]])-(orbital_energies[diff_beta_occ[2]]+orbital_energies[diff_beta_occ[3]]));
        }
        else{
            return abs((orbital_energies[diff_beta_occ[0]]+orbital_energies[diff_alpha_occ[0]])-(orbital_energies[diff_alpha_occ[1]]+orbital_energies[diff_beta_occ[1]]));
        }
}
int Full_matrix_CISD::type_distinguish(int occupation){
    vector<int> alpha_occ1;
    vector<int> alpha_occ2;
    vector<int> beta_occ1;
    vector<int> beta_occ2;
    vector<int> same_alpha_occ;
    vector<int> same_beta_occ;
    vector<int> diff_alpha_occ;
    vector<int> diff_beta_occ;
    vector<int> diff_beta_index;
    vector<int> diff_alpha_index;
    vector<int> same_beta_index;
    vector<int> same_alpha_index;
    cisd_occupation->check_same_diff_occupation(0, occupation , alpha_occ1, beta_occ1, alpha_occ2, beta_occ2, same_alpha_occ, diff_alpha_occ, same_beta_occ, diff_beta_occ, diff_alpha_index, diff_beta_index, same_alpha_index, same_beta_index);
    if(diff_alpha_occ.size() == 4)
        return 0;
    else if(diff_beta_occ.size() == 4)
        return 1;
    else if(diff_alpha_occ.size()==2 and diff_beta_occ.size()==2){
        if(diff_alpha_occ[0]==diff_beta_occ[0] and diff_alpha_occ[1]==diff_beta_occ[1])
            return 2;
        else if(diff_alpha_occ[0]==diff_beta_occ[0] and diff_alpha_occ[1]!=diff_beta_occ[1])
            return 3;
        else if(diff_alpha_occ[0]!=diff_beta_occ[0] and diff_alpha_occ[1]==diff_beta_occ[1])
            return 4;
        else if(diff_alpha_occ[0]!=diff_beta_occ[0] and diff_alpha_occ[1]!=diff_beta_occ[1])
            return 5;
    }
    return 0;
}
Teuchos::RCP<Epetra_MultiVector> Full_matrix_CISD::get_eigenvectors(){
//    RCP<Epetra_MultiVector> retval = rcp(new Epetra_Multivector(*eigenvectors))
    return eigenvectors;
}
