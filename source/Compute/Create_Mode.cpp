#include "Create_Mode.hpp"

#include <string>
#include <vector>
//#include <exception>

#include "Scf.hpp"
#include "../Utility/Verbose.hpp"
#include "../Utility/String_Util.hpp"

using std::vector;
using std::string;
using Teuchos::RCP;
using Teuchos::rcp;
using Teuchos::Array;

Array< RCP<Compute> > Create_Mode::get_array(RCP<const Basis> basis, RCP<const Atoms> atoms, Array< RCP<Teuchos::ParameterList> > array_parameters){
    string mode;
    if( array_parameters[0] -> sublist("BasicInformation").isParameter("Mode") ){
        mode = array_parameters[0] -> sublist("BasicInformation").get<string>("Mode");
    } else {
        mode = array_parameters[0] -> name();
        for(int i = 1; i < array_parameters.size(); ++i){
            mode += " ";
            mode += array_parameters[1] -> name();
        }
    }

    std::unordered_map<string, int> array_name_map = Create_Mode::get_parameter_names(array_parameters);
    vector<string> modes;
    if( mode == "Sp" ){
        modes.push_back("Guess");
        modes.push_back("Scf");
    } else if( mode == "Opt" ){
        modes.push_back("Guess");
        modes.push_back("Opt");
    } else if( mode == "SpTDDFT" ){
        modes.push_back("Guess");
        modes.push_back("Scf");
        modes.push_back("TDDFT");
    } else if( mode == "TDDFT" ){
        modes.push_back("Guess");
        modes.push_back("TDDFT");
    } else {
        modes = String_Util::strSplit( mode, " " );
    }

    if( modes.size() < array_parameters.size() ){
        for(std::unordered_map<string,int>::iterator it = array_name_map.begin(); it != array_name_map.end(); ++it){
            if(std::find(modes.begin(), modes.end(), it->first) == modes.end()){
                modes.push_back(it->first);
            }
        }
    }

    if(array_parameters[0] -> sublist("BasicInformation").isParameter("ForceCalculation")){
        Verbose::single(Verbose::Simple) << "ForceCalculation Option is depreciated. Use %%Force block instead." << std::endl;
        array_parameters.append(rcp(new Teuchos::ParameterList("Force")));
        array_parameters.back() -> sublist("BasicInformation") = Teuchos::ParameterList(array_parameters[0]->sublist("BasicInformation"));
        array_parameters.back() -> sublist("Force").set("ForceDerivative", array_parameters[0]->sublist("BasicInformation").get<string>("ForceDerivative"));
    }

    if( modes.size() != array_parameters.size() ){
        Verbose::all() << "BasicInformation::Mode is incompatible with your input!" << std::endl;
        std::cerr << "BasicInformation::Mode is incompatible with your input!" << std::endl;
        exit(EXIT_FAILURE);
    }

    RCP<Core_Hamiltonian> core_hamiltonian = Teuchos::null;
    Array< RCP<Compute> > compute_array;
    for(int i = 0; i < modes.size(); ++i){
        if( modes[i] == "Guess" ){
            compute_array.append( Create_Compute_Interface::Create_Guess(atoms, array_parameters[array_name_map["Guess"]], basis) );
            if( array_parameters[array_name_map["Guess"]] -> sublist("Guess").get<int>("InitialGuess") == 0 ){
                core_hamiltonian = Teuchos::rcp_dynamic_cast<Core_Hamiltonian>( compute_array[i] );
            }
        } else if( modes[i] == "Scf" or modes[i] == "Opt" or modes[i] == "TDDFT" ){
            compute_array.append( Create_Compute_Interface::Create_DFT(basis, array_parameters[array_name_map[modes[i]]], core_hamiltonian ) );
            if( modes[i] == "Scf"){
                core_hamiltonian = Teuchos::rcp_dynamic_cast<Scf>(compute_array.back()) -> get_core_hamiltonian();
            }
        } else if( modes[i] == "Force" ){
            compute_array.append( Create_Compute_Interface::Create_Force(basis, atoms, array_parameters[array_name_map[modes[i]]] ) );
        } else if( modes[i] == "CI" or modes[i] == "CIS" or modes[i] == "CISD" ){
            compute_array.append( Create_Compute_Interface::Create_CI(basis, array_parameters[array_name_map[modes[i]]]) );
        } else {
            Verbose::all() << "Uninterpretable mode name " << modes[i] << std::endl;
            exit(EXIT_FAILURE);
        }
    }
    return compute_array;
}

std::unordered_map<string, int> Create_Mode::get_parameter_names( Array< RCP<Teuchos::ParameterList> > array_parameters){
    std::unordered_map<string, int> array_name_map;
    for(int i = 0; i < array_parameters.size(); ++i){
        array_name_map[array_parameters[i] -> name()] = i;
    }
    return array_name_map;
}
