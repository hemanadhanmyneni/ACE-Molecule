#include "Force.hpp"

#include <string>
#include "../Utility/Verbose.hpp"
#include "../Utility/Calculus/Lagrange_Derivatives.hpp"

using Teuchos::Array;
using Teuchos::RCP;
using std::setw;
using std::setprecision;

Force::Force(
        RCP<const Basis> basis, RCP<const Atoms> atoms,
        RCP<Nuclear_Potential> nuclear_potential,
        RCP<External_Field> external_field,
        RCP<Teuchos::ParameterList> parameters
){
    this->basis = basis;
    this->atoms = atoms;
    //this->states = states;
    this->nuclear_potential = nuclear_potential;
    this->parameters = parameters;
    this->external_field = external_field;
}

std::vector<double> Force::get_minus_force(){
    return minus_F_tot;
}

/*
std::vector<double> Force::get_force(){
    std::vector<double> F_tot;
    for(int i = 0; i < this -> minus_F_tot.size(); ++i){
        F_tot.push_back(-this->minus_F_tot[i]);
    }
    return minus_F_tot;
}
*/

void Force::print_force(){
    Verbose::single(Verbose::Simple) << std::endl;
    Verbose::set_numformat(Verbose::Energy);
    Verbose::single(Verbose::Simple) << "!================================================" << std::endl;
    Verbose::single(Verbose::Simple) << "! Force:: List of total force in atomic unit." << std::endl;
    Verbose::single(Verbose::Simple) << "! Atom           x         y         z" <<  std::endl;
    for(int ia = 0; ia < this -> atoms -> get_size(); ++ia){
        Verbose::single(Verbose::Simple) << "! Atom " << setw(3) << ia << "  "
                          << setprecision(6) << setw(9) << -this -> minus_F_tot[ia*3+0] << " "
                          << setprecision(6) << setw(9) << -this -> minus_F_tot[ia*3+1] << " "
                          << setprecision(6) << setw(9) << -this -> minus_F_tot[ia*3+2] << std::endl;
    }
    Verbose::single(Verbose::Simple) << "!================================================" << std::endl;
    Verbose::single(Verbose::Simple) << std::endl;
}
