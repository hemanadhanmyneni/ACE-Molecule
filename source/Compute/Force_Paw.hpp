#pragma once
#include "Teuchos_Array.hpp"
#include "Teuchos_RCP.hpp"
#include "Teuchos_ParameterList.hpp"

//#include "Compute.hpp"// Should be changed to Force interface.
#include "Force.hpp"

/**
 * @brief Force calculation for PAW.
 * @author Sungwoo Kang
 * @date 2016.4.12
 **/
class Force_Paw: public Force{
    public:
        /**
          * @brief Constructor
          * @param basis Basis.
          * @param atoms Atoms.
          * @param nuclear_potential Nuclear Potential class..
          * @param parameters ParameterList.
          **/
        Force_Paw(
                Teuchos::RCP<const Basis> basis,
                Teuchos::RCP<const Atoms> atoms,
                Teuchos::RCP<Nuclear_Potential> nuclear_potential,
                Teuchos::RCP<External_Field> external_field,
                Teuchos::RCP<Teuchos::ParameterList> parameters
        );
        /**
         * @brief Compute method that is accesable to others.
         * @details It is composed of three parts.
         *             1. Core density contribution.<br>
         *                \f$ -\int d \vec{r}^\prime \tilde{v}_\textrm{eff}(\vec{r}^\prime) \frac{\partial \tilde{n}(\vec{r}^\prime)}{\partial \vec{R}^a} \f$.<br>
         *             2. Compensation charge contribution.<br>
         *                \f$ -\int d \vec{r}^\prime u_H(\vec{r}^\prime) \sum_L Q_L \frac{\partial \tilde{g}^a_L(\vec{r}^\prime)}{\partial \vec{R}^a} \f$.<br>
         *            3. Valence density contribution.<br>
         *                \f$ -\sum_n f_n \sum_{ij} \left[ \Delta H^a_{ij} - \epsilon_n \Delta S^a_{ij} \right]
         *                     \left( P_{ni}^{a*} \langle \frac{d \tilde{p}_j^a}{d \vec{R}^a} | \tilde{\phi}_n \rangle + 
         *                            \langle \tilde{\phi}_n | \frac{d \tilde{p}_i^a}{d \vec{R}^a} \rangle P_{nj}^{a*} \right) \f$.
         * @param basis Basis.
         * @param states States. Input and output. Output states will be appended.
         **/
        int compute(
            Teuchos::RCP<const Basis> basis,
            Teuchos::Array< Teuchos::RCP<State> > &states
        );
};
