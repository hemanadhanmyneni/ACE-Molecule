#include "Opt.hpp"
#include <string>

#include <iomanip>
#include <fstream>
#include <gsl/gsl_multimin.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_errno.h>

#include "Create_Compute.hpp"
#include "Scf.hpp"
#include "Force_KB.hpp"
#include "../Basis/Create_Basis.hpp"
#include "../Io/Atoms.hpp"
#include "../Utility/String_Util.hpp"
#include "../Utility/Parallel_Manager.hpp"
#include "../Utility/Verbose.hpp"

using std::string;
using std::vector;
using std::setw;
using std::fixed;
using std::scientific;
using std::cout;

using Teuchos::Array;
using Teuchos::RCP;
using Teuchos::rcp;

Opt::Opt(RCP<const Basis> basis, RCP<Atoms> atoms, Array< RCP<State> > states, Array<RCP<Teuchos::ParameterList> > array_parameters){
    this-> basis = basis;
    this-> atoms = atoms;
    this-> states = states;
    this-> array_parameters = array_parameters;
    if (!array_parameters[0]->sublist("BasicInformation").sublist("Opt").isParameter("IterateMaxCycle")){
        array_parameters[0]->sublist("BasicInformation").sublist("Opt").set("IterateMaxCycle",256);
    }
    if (!array_parameters[0]->sublist("BasicInformation").sublist("Opt").isParameter("ForceTolerance")){
        array_parameters[0]->sublist("BasicInformation").sublist("Opt").set("ForceTolerance",1.0E-3);
    }
    if (!array_parameters[0]->sublist("BasicInformation").sublist("Opt").isParameter("PositionTolerance")){
        array_parameters[0]->sublist("BasicInformation").sublist("Opt").set("PositionTolerance",1.0E-3);
    }
    if (!array_parameters[0]->sublist("BasicInformation").sublist("Opt").isParameter("OptimizationMethod")){
        array_parameters[0]->sublist("BasicInformation").sublist("Opt").set("OptimizationMethod",5);
    }
    if (!array_parameters[0]->sublist("BasicInformation").sublist("Opt").isParameter("UsingOptGuess")){
        array_parameters[0]->sublist("BasicInformation").sublist("Opt").set("UsingOptGuess","Yes");
    }

    this->before_states = before_states;
    this->opt_computes = opt_computes;

    this->num_sp_cal = num_sp_cal;
    this->num_grad_cal = num_grad_cal;
    this->tot_energy = tot_energy;
    this->before_geometry = before_geometry;
    this->current_geometry = current_geometry;
    this->current_gradient = current_gradient;
    this->maxdr = maxdr;
    this->maxgrad = maxgrad;

    this -> opt_method = array_parameters[0]->sublist("BasicInformation").sublist("Opt").get<int>("OptimizationMethod");
    this -> max_cycle = array_parameters[0]->sublist("BasicInformation").sublist("Opt").get<int>("IterateMaxCycle");
    this -> force_tolerance = array_parameters[0]->sublist("BasicInformation").sublist("Opt").get<double>("ForceTolerance");
    this -> dPosition_tolerance = array_parameters[0]->sublist("BasicInformation").sublist("Opt").get<double>("PositionTolerance");

    this -> max_cycle = array_parameters[0] -> sublist("BasicInformation").sublist("Opt").get<int>("IterateMaxCycle");
}

int Opt::compute(RCP<const Basis> basis, Array< RCP<State> >& states){

    // Initialize geometry optimization loop 
    Verbose::single() << "===== Opt::START GEOMETRY OPTIMIZATION =====" << std::endl;
    int status = 0;

    const gsl_multimin_fdfminimizer_type *T;
    gsl_multimin_fdfminimizer *s;
    int dim = 3*atoms->get_size();

    gsl_vector *geometry;
    this->before_geometry.resize(dim);
    this->current_geometry.resize(dim);
    this->current_gradient.resize(dim);
    void *p=this;

    gsl_multimin_function_fdf my_func;
    my_func.n = dim;
    my_func.f = &single_point_calculation;
    my_func.df = &gradient_calculation;
    my_func.fdf = &energy_and_gradient;
    my_func.params = p;

    if(array_parameters[0]->sublist("BasicInformation").isParameter("Centered")){
        if(array_parameters[0]->sublist("BasicInformation").get<int>("Centered")==1){
            Verbose::single()<< "center of mass is moved to 0,0,0 \n" ;
            atoms->move_center_of_mass(0.0,0.0,0.0);
            Verbose::single()<< *atoms <<std::endl;
        }
    }

    vector<std::array<double,3> > positions = atoms->get_positions();
    geometry = gsl_vector_alloc(dim);
    for(int i=0; i<positions.size(); i++){
        gsl_vector_set (geometry, 3*i, positions[i][0]);   
        gsl_vector_set (geometry, 3*i+1, positions[i][1]);   
        gsl_vector_set (geometry, 3*i+2, positions[i][2]);   
        current_geometry[3*i] = positions[i][0];
        current_geometry[3*i+1] = positions[i][1];
        current_geometry[3*i+2] = positions[i][2];
    }
    
    //gsl_vector *absgrad, *absdr;
    //absgrad = gsl_vector_alloc(dim);
    //absdr = gsl_vector_alloc(dim);

    //double tol = array_parameters[0]->sublist("BasicInformation")/sublist("Opt").get<double>("LineSearchTol");
    double tol = 1.0;

    switch(this -> opt_method){
        case 1:
            T = gsl_multimin_fdfminimizer_conjugate_fr;
            break;
        case 2:
            T = gsl_multimin_fdfminimizer_conjugate_pr;
            break;
        case 3:
            T = gsl_multimin_fdfminimizer_steepest_descent;
            break;
        case 4:
            T = gsl_multimin_fdfminimizer_vector_bfgs;
            break;
        case 5:
            T = gsl_multimin_fdfminimizer_vector_bfgs2;
            break;
    }

    s = gsl_multimin_fdfminimizer_alloc(T,dim);
    gsl_multimin_fdfminimizer_set(s, &my_func, geometry, 0.01, tol);

    for(int i=0; i<max_cycle; i++){
        status = gsl_multimin_fdfminimizer_iterate(s);

        for(int j=0; j<dim; j++){
            current_geometry[j] = gsl_vector_get(gsl_multimin_fdfminimizer_x(s), j);
        }
        for(int j=0; j<dim; j++){
            current_gradient[j] = gsl_vector_get(gsl_multimin_fdfminimizer_gradient(s), j);
        }
      
        if (status==GSL_FAILURE){
          break;
        }
      
        // Check convergence criteria
        Verbose::single() << std::endl << std::endl;
        Verbose::single() << "----- Opt::Check convergence criteria ----- " <<  std::endl;
        Verbose::single() << "----- TotEnergy = " << this->tot_energy << std::endl;
        Verbose::single() << "----- Maxgrad   = " << this->maxgrad << "\t Maxdr = " << this->maxdr << std::endl;
        Verbose::single() << std::endl << std::endl;

        if ( (this -> force_tolerance > 0 and this->maxgrad <= this->force_tolerance) or (this -> dPosition_tolerance > 0 and this->maxdr <= dPosition_tolerance) ){
            status = GSL_SUCCESS;
        }
        else{
            status = GSL_CONTINUE;
        }

        this-> update_geometry(current_geometry);

        if(status == GSL_SUCCESS){
            string method_print;
            switch(this -> opt_method){
                case 1:
                    method_print = "Fletcher-Reeves Conjugate Gradient";
                    break;
                case 2:
                    method_print = "Polak-Ribiere Conjugate Gradient";
                    break;
                case 3:
                    method_print = "Steepest-Descent";
                    break;
                case 4:
                    method_print = "Quasi-Newton BFGS";
                    break;
                case 5:
                    method_print = "Quasi-Newton BFGS2";
                    break;
            }

            Verbose::single() << std::endl << std::endl;
            Verbose::single() << "===== Opt::Geometry Optimization converged at " << (i+1) << "th step" << std::endl;
            Verbose::single() << "===== Opt:: Using " << method_print<< " method to update geometry " << std::endl;
            Verbose::single() << "===== Opt::Number of single point energy calculation   = " << this->num_sp_cal << std::endl;
            Verbose::single() << "===== Opt::Number of single point gradient calculation = " << this->num_grad_cal << std::endl;
            Verbose::single() << "===== Opt::Final total energy                          = " << std::setprecision(8) << s->f << std::endl;
            //Verbose::single() << "===== Opt::Final total energy         = " << std::setprecision(8) << this->states[states.size()-1]->total_energy << std::endl;
            Verbose::single() << "===== Opt::Maximum absolute gradient                   = " << this->maxgrad << std::endl; 
            Verbose::single() << "===== Opt::Maximum absolute dPosition                  = " << this->maxdr <<  std::endl;
            Verbose::single() << "===== Opt::Final geometry of given system is " << std::endl;
            Verbose::single() << *atoms << std::endl;
            Verbose::single() << "===== Opt::Terminate Geometry Optimization Procedure =====" << std::endl;
            Verbose::single() << std::endl << std::endl;
            break;
        }
    }
    gsl_multimin_fdfminimizer_free(s);
    gsl_vector_free(geometry);
    states.append( rcp( new State(*this->states.back()) ) );
    return status;
}

void Opt::update_geometry(vector<double> updated_geometry){
    //double* new_positions = new double[3];
    std::array<double,3> new_positions;
    for (int i=0; i<atoms->get_size(); i++){
        new_positions[0] = updated_geometry[3*i];
        new_positions[1] = updated_geometry[3*i+1];
        new_positions[2] = updated_geometry[3*i+2];
        atoms->operator[](i).set_position(new_positions);
    }
    //delete[] new_positions;
}

double Opt::single_point_calculation(const gsl_vector *x, void *params){

    // Initialize single point energy calculation in optimization loop.
    Opt* opt_inside = (Opt *) params;
    
    // Update geometry before cacluating singple point energy
    for(int i=0; i<x->size; i++){
        opt_inside->current_geometry[i] = gsl_vector_get(x, i);
    }
    opt_inside->update_geometry(opt_inside->current_geometry);

    if(opt_inside->num_sp_cal > 0){
        // Get maxdr & maxgrad
        vector<double> dGeometry;
        dGeometry.resize(opt_inside->current_geometry.size());
        for(int i=0; i<dGeometry.size(); i++){
            dGeometry[i] = (opt_inside->current_geometry[i]-opt_inside->before_geometry[i]);
        }
        opt_inside->maxdr = opt_inside->get_maxval(dGeometry);
        opt_inside->maxgrad = opt_inside->get_maxval(opt_inside->current_gradient);

        if(opt_inside->maxgrad < 1E-3 || opt_inside->maxdr < 1E-3){
            return opt_inside->tot_energy;
        }

        Verbose::single() << "Check before step informations " << std::endl;
        Verbose::single() << "Before Total energy                      : " << opt_inside->tot_energy << std::endl;
        Verbose::single() << "Before maximum abolute gradient element  : " << opt_inside->maxgrad << std::endl;
        Verbose::single() << "Before maximum abolute dPosition element : " << opt_inside->maxdr << std::endl;
    }


    opt_inside->num_sp_cal++;
    if(opt_inside->num_sp_cal > 1 & opt_inside->array_parameters[0]->sublist("BasicInformation").sublist("Opt").get<string>("UsingOptGuess") == "Yes"){
        opt_inside->array_parameters[0] -> sublist("Guess").get<int>("InitialGuess") = -99 ;
    }

    // Caculating single point energy
    Verbose::single() << std::endl<< std::endl;
    Verbose::single() << "----- Opt::Start Single Point Energy Calculation : " << opt_inside->num_sp_cal << "th trial" << std::endl;
    Verbose::single() << "----- Opt::Current geometry ----- " << std::endl;
    Verbose::single() << *opt_inside->atoms << std::endl;
    Verbose::single() << std::endl << std::endl;
    double val = 0.0;

    opt_inside->opt_computes.clear();
    opt_inside->states.clear();
    //opt_inside->states = opt_inside->before_states;

    RCP<Time_Measure> timer = rcp(new Time_Measure() );
    timer->start("Opt-Sp");
    opt_inside->spcal(&val);
    timer->end("Opt-Sp");
    // Printing calculation results | Total energy
    Verbose::single() << "----- Opt::Finalize " << opt_inside->num_sp_cal << "th Single Point Energy Calculation ----- "<< std::endl;
    Verbose::single() << "----- Opt::Final Total Energy = " << std::setprecision(8) << val << std::endl;
    timer->print(Verbose::single());

    // Save geometry 
    opt_inside->before_geometry = opt_inside->current_geometry;
    opt_inside->before_states = opt_inside->states;
    opt_inside->tot_energy = val;

    return val;
}

void Opt::gradient_calculation(const gsl_vector *x, void *params, gsl_vector *gsl_gradient){

    // Initialize single point gradient calculation in optimization loop.
    Opt* opt_inside = (Opt *) params;
    opt_inside->num_grad_cal++;

    Verbose::single() << std::endl << std::endl;
    Verbose::single() << "----- Opt::Start Single Point Gradient Calculation : " << opt_inside->num_grad_cal << "th trial" << std::endl;
    Verbose::single() << std::endl << std::endl;

    opt_inside->gradcal(gsl_gradient);

    //Saving gradient
    for(int i=0; i<gsl_gradient->size; i++){
        opt_inside->current_gradient[i] = gsl_vector_get(gsl_gradient,i);
    }

    Verbose::single() << std::endl << std::endl;
    Verbose::single() << "----- Opt::Finalize " << opt_inside->num_grad_cal << "th Single Point Gradient Calculation ----- "<< std::endl;
    Verbose::single() << std::endl << std::endl;
}

void Opt::energy_and_gradient(const gsl_vector *x, void *params, double *total_energy, gsl_vector *gsl_gradient){
    *total_energy = single_point_calculation(x, params);
    gradient_calculation(x, params, gsl_gradient);

}

void Opt::spcal(double *energy){
    opt_computes.append( Create_Compute_Interface::Create_Guess(atoms,array_parameters[0], basis) ) ;
    if(this->num_sp_cal > 1 & this->array_parameters[0]->sublist("BasicInformation").sublist("Opt").get<string>("UsingOptGuess") == "Yes"){
        int ierr = opt_computes[0]->compute(basis, states);
        states[states.size()-1] -> occupations = before_states[before_states.size()-1]->occupations;
        states[states.size()-1] -> density = before_states[before_states.size()-1]->density;
        states[states.size()-1] -> orbitals = before_states[before_states.size()-1]->orbitals;
        opt_computes.clear();
    }

    opt_computes.append( Create_Compute_Interface::Create_DFT(basis,array_parameters[1] ) );
    for(int istep=0; istep<opt_computes.size(); istep++){
        int ierr = opt_computes[istep]->compute(basis,states);
        states[states.size()-1] -> write( basis, array_parameters[istep] );
        if( ierr < 0 ){
            Verbose::single() << std::endl;
            Verbose::single() << "WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING" << std::endl
                              << "WARNING" <<std::endl
                              << "WARNING  Calculation Step #" << istep << " (" << array_parameters[istep] -> name() << ") terminated errorously" << std::endl
                              << "WARNING  Error code: " << ierr << std::endl
                              << "WARNING  DO NOT TRUST FURTHER CALCULATIONS!!" <<std::endl
                              << "WARNING" <<std::endl
                              << "WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING" << std::endl;
            Verbose::single() << std::endl;
        }
    }
    *energy = states[states.size()-1]->total_energy;
}

void Opt::gradcal(gsl_vector *gsl_gradient){
   //opt_computes.append( Teuchos::rcp_implicit_cast<Compute> ( rcp( new Force_KB( basis, atoms, Teuchos::rcp_dynamic_cast<Scf>(opt_computes[opt_computes.size()-1])->get_core_hamiltonian(), array_parameters[0] ) ) ) );
   opt_computes.append( Create_Compute_Interface::Create_Force( basis, atoms, Teuchos::rcp_dynamic_cast<Scf>(opt_computes[opt_computes.size()-1])->get_core_hamiltonian(), array_parameters[0] ) );
   int ierr = opt_computes[opt_computes.size()-1]->compute(basis,states);
   vector <double> minus_atomic_force;
   minus_atomic_force = Teuchos::rcp_dynamic_cast<Force>(opt_computes[opt_computes.size()-1])->get_minus_force();

   for(int i=0; i<minus_atomic_force.size(); i++){
        gsl_vector_set(gsl_gradient, i, minus_atomic_force[i]);
   }
}

double Opt::get_maxval(vector<double> target){
    double max_val = abs(*max_element(target.begin(), target.end()));
    double min_val = abs(*min_element(target.begin(), target.end()));

    return (max_val>min_val)? max_val: min_val;
}
