#pragma once
#include <vector>

#include "Teuchos_ParameterList.hpp"
#include "Teuchos_RCP.hpp"
#include "Teuchos_Array.hpp"

#include "Compute_Interface.hpp"
#include "../Core/Pseudo-potential/Nuclear_Potential.hpp"
#include "../Core/ExternalField/External_Field.hpp"
#include "../Io/Atoms.hpp"

class Force: public Compute_Interface {
    public:
        Force(
            Teuchos::RCP<const Basis> basis,
            Teuchos::RCP<const Atoms> atoms,
            Teuchos::RCP<Nuclear_Potential> nuclear_potential,
            Teuchos::RCP<External_Field> external_field,
            Teuchos::RCP<Teuchos::ParameterList> parameters
        );

        virtual int compute(
            Teuchos::RCP<const Basis> basis,
            Teuchos::Array< Teuchos::RCP<State> >& states
        ) = 0;

        virtual ~Force(){};
        std::vector<double> get_force();
        std::vector<double> get_minus_force();
        void print_force();

    protected:
        Teuchos::RCP<const Basis> basis;
        Teuchos::RCP<const Atoms> atoms;
        Teuchos::RCP<Nuclear_Potential> nuclear_potential;
        Teuchos::RCP<External_Field> external_field;
        Teuchos::RCP<Teuchos::ParameterList> parameters;

        std::vector<double> minus_F_tot;

};
