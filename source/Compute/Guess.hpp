#pragma once
#include "Epetra_CrsMatrix.h"
#include "Teuchos_RCP.hpp"
#include "Teuchos_ParameterList.hpp"
#include "Teuchos_Array.hpp"
#include "Epetra_MultiVector.h"
//#include "Teuchos_ArrayRCP.hpp"

#include "../Core/Occupation/Occupation.hpp"
#include "Compute_Interface.hpp"
#include "../State/State.hpp"
#include "../Io/Atoms.hpp"
#include "../Utility/Time_Measure.hpp"

//class Atoms;
/**
  @brief Guess class create an initial state of system. Density (and orbitals in some cases) is generated.
*/
class Guess: public Compute_Interface{
    public:
        /**
         * @brief Virtual destructor for abstract class.
         **/
        virtual ~Guess(){};
    protected:
        int initialize_states(
            Teuchos::RCP<const Basis> basis, 
            Teuchos::Array< Teuchos::RCP<State> >& states
        );

        /// This variable should be set in constructor of all child of Guess class
        Teuchos::Array< Teuchos::RCP<Occupation> > occupations;
        /// This variable should be set in constructor of all child of Guess class
        Teuchos::RCP<const Atoms> atoms;
        //double num_electron;
        //double spin_multiplicity;
};
