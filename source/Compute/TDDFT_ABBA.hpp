#pragma once
#include <vector>

#include "Teuchos_ParameterList.hpp"
#include "Teuchos_RCP.hpp"
#include "Teuchos_Array.hpp"
#include "Epetra_CrsMatrix.h"

#include "Compute_Interface.hpp"
#include "Create_Compute.hpp"
#include "Poisson_Solver.hpp"
#include "../State/State.hpp"
#include "TDDFT.hpp"

class TDDFT_ABBA: public TDDFT{
    public:
        TDDFT_ABBA(Teuchos::RCP<const Basis> basis, Teuchos::RCP<Exchange_Correlation> exchange_correlation, Teuchos::RCP<Poisson_Solver> poisson_solver, Teuchos::RCP<Teuchos::ParameterList> parameters);
        //virtual int compute(Teuchos::RCP<const Basis> basis, Teuchos::Array< Teuchos::RCP<State> >& states);
        virtual ~TDDFT_ABBA(){};

    protected:
        virtual void get_excitations(Teuchos::RCP<Diagonalize> diagonalize, std::vector<double> &energies, Teuchos::RCP<Epetra_MultiVector> &eigenvectors);
        /**
         * @brief Fill restricted TDDFT matrix. Governs matrix index.
         * @callergraph
         * @callgraph
         **/
        virtual void fill_TD_matrix(
                Teuchos::Array< std::vector<double> > orbital_energies,
                double Hartree_contrib, double xc_contrib,
                int matrix_index_c, int matrix_index_r,
                Teuchos::RCP<Epetra_CrsMatrix> &sparse_matrix
        );
        /**
         * @brief Fill restricted TDDFT matrix. Governs matrix index.
         * @callergraph
         * @callgraph
         **/
        virtual void fill_TD_matrix_restricted(
                std::vector<double> orbital_energies,
                double Hartree_contrib, std::vector<double> xc_contrib,
                int matrix_index_c, int matrix_index_r,
                Teuchos::Array< Teuchos::RCP<Epetra_CrsMatrix> >& sparse_matrix
        );
// old comment
        /**
         * @brief Kernel matrix calculation routine for HF.
         * @details Calculate kernel
         * @param state Input state.
         * @param sparse_matrix Output TDDFT kernel matrix.
         * @note This routine cannot handle Tamm-Dancoff Approximation.
         * @callergraph
         * @callgraph
         **/
        virtual void Kernel_TDHF_X(
                Teuchos::RCP<State> state, 
                Teuchos::RCP<Epetra_CrsMatrix>& sparse_matrix, 
                bool kernel_is_not_hybrid, 
                bool add_delta_term, double scale = 1.0
        );

        /**
         * @brief Get Z vector from eigenvectors and spread over processors.
         * @param input Input eigenvector.
         * @param eigvals GS eigenvalues.
         * @return output Z vector.
         * @callergraph
         * @callgraph
         **/
        virtual std::vector< std::vector<double> > gather_z_vector(
            Teuchos::RCP<Epetra_MultiVector> input,
            Teuchos::Array< std::vector<double> > eigvals
        );
};
