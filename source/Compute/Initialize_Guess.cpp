#include "Initialize_Guess.hpp"

using Teuchos::Array;
using Teuchos::ParameterList;
using Teuchos::rcp;
using Teuchos::RCP;

Initialize_Guess::Initialize_Guess(Array< RCP<Occupation> > occupations, RCP<const Atoms> atoms){
    this->occupations = occupations;
    this->atoms = atoms;
}

int Initialize_Guess::compute(RCP<const Basis> basis,Array< RCP<State> >& states){
    initialize_states(basis,states);
    return 0;
}
