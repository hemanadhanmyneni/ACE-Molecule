#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <functional>

#include "Teuchos_Array.hpp"
#include "Teuchos_RCP.hpp"

#include "Epetra_MultiVector.h"
#include "Epetra_Vector.h"
#include "Epetra_Map.h"
#include "Epetra_CrsMatrix.h"
#include "Epetra_Comm.h"// Need to include this.

#include "../Io/Atoms.hpp"
#include "Basis_Function/Basis_Function.hpp"
#include "Kinetic_Matrix.hpp"
#include "Grid_Setting/Grid_Setting.hpp"

class Kinetic_Matrix;

/**
 * @brief Abstract class containing all informations related with simulation box.
 * @author Sunghwan Choi
 * @date 2016.1

*/
class Basis{
    friend std::ostream& operator <<(std::ostream &c, const Basis &pbasis);
    friend std::ostream& operator <<(std::ostream &c, const Basis* pbasis);
    friend bool operator==(const Basis& basis1, const Basis& basis2);
    friend bool operator!=(const Basis& basis1, const Basis& basis2);

    friend class Kinetic_Matrix;

    public:
        /**
        @brief Constructor. Do not call it directly unless you are knowing what you are doing.
               Use Create_Basis if you want to create Basis class.
        @param [in] basis Basis function class.
        @param [in] kinetic_basis Basis function class used for kinetic energy calculation.
        @param [in] grid_setting Simulation box specification class.
        @param [in] map Epetra_Map corresponds to our simulation grid.
        */
        Basis(
            Teuchos::RCP<Basis_Function> basis,
            Teuchos::RCP<Basis_Function> kinetic_basis,
            Teuchos::RCP<Grid_Setting> grid_setting,
            Teuchos::RCP<Epetra_Map> map,
            int n_cutoff=300,
            double kinetic_cutoff = 1E-30
        );
        //~Basis();

        /**
         * @return Number of points in x, y, z-axis in length 3 C-array.
         * @note Do not delete returned pointer.
         * @todo Remove pointer usage.
         **/
        std::array<int,3> get_points() const;
        //const int* get_points() const;
        /**
         * @brief Used for Grid_Atom construction?
         * @param [in] axis index for axis (0,1 or 2)
         * @param [in] axis_points grid index on the given axis
         * @param [out] neighboring_points indices of neighboring points 
         * @note Can this replace get_neighbors in Tricubic_Interpolation?
         **/
        void get_neighboring_points(int axis, int axis_points, std::vector<int>& neighboring_points) const;
        /**
         * @return Scaling factor(grid spacing) along x, y, z-axis in length 3 C-array.
         **/
        std::array<double,3> get_scaling() const;
        /**
         *
         * @return Total number of points in the grid.
         **/
        int get_original_size() const;

        /**
         * @return Length of cell, x, y, z direction. Should be close to 2*Cell.
         **/
        const std::array<double,3> get_grid_length() const;
        /**
         * @return Basis type (Sinc, Laguree, etc.).
         **/
        std::string get_basis_type() const;
        /**
         * @return Grid type (Grid_Basic, Grid_Sphere, Grid_Atom).
         **/
        std::string get_grid_type() const;
        /**
         * @param mic Use minimum image convention. If true, find minimum distance when axis is periodic.
         * @return Distance between input points.
         **/
        double compute_distance(int i_x,int i_y, int i_z, int j_x, int j_y, int j_z, bool mic = false) const;
        double compute_1d_distance(int axis, int i_x, int j_x, bool mic = false ) const;
        double find_closest_point(double x, double y, double z, int i_x, int i_y, int i_z) const;

        /**
         * @brief Returns value of ith basis function at given point.
         * @param[in] i Basis index.
         * @param[in] x x position.
         * @param[in] y y position.
         * @param[in] z z position.
         * @return Value of ith basis function at given point.
         **/
        double compute_basis(int i, double x, double y, double z) const;

// shchoi 
//        /**
//         * @brief this function returns distance between i-th & j-th grid points.
//         * @return Distance between given position and grid points 
//         * @param [in] double_x position on x axis 
//         * @param [in] double_y position on y axis 
//         * @param [in] double_z position on z axis 
//         * @param [in] index on x axis for i-th grid points
//         * @param [in] index on y axis for i-th grid points
//         * @param [in] index on z axis for i-th grid points
//         **/
//        double find_closest_point(double x, double y, double z, int* i_x, int* i_y, int* i_z) const;

//        bool operator== (const Basis basis) const;
//        bool operator!= (const Basis basis) const;
        //bool operator== (const RCP<Basis> basis);
        /**
         * @return Epetra_Map corresponds to this basis.
         **/
        Teuchos::RCP<const Epetra_Map> get_map() const;
        /**
         * @return Kinetic operator matrix of this grid.
         **/
        //Teuchos::RCP<const Epetra_CrsMatrix> get_kinetic_matrix() const;
        //RCP<const Epetra_CrsMatrix> get_kinetic_matrix();
        std::vector< std::vector< double> > make_kinetic_vector();

//        virtual std::vector< int > get_shell_point(Basis_Function* basis, double depth) =0;
//        virtual std::vector< int > get_shell_point(Basis_Function* basis) =0;

        /**
         * @brief Calculate position from 1-D form index.
         * @param[in] index Index
         * @param[out] x X-position.
         * @param[out] y Y-position.
         * @param[out] z Z-position.
         **/
        void get_position(int index, double& x, double& y, double &z) const;
        /**
         * @name Member functions using axiswise index.
         * @{
         **/
        /**
         * @return x-, y-, z- direction grid coordinates.
         * @note Do not delete retuned pointer.
         * @todo Remove pointer usage.
         **/
        const double** get_scaled_grid() const;
        /**
         * @brief Decompose basis index into the axiswise index.
         * @param[in] index basis form index.
         * @param[out] i_x Output. X-axis index.
         * @param[out] i_y Output. Y-axis index.
         * @param[out] i_z Output. Z-axis index.
         **/
        void decompose(int index, int& i_x, int& i_y, int& i_z) const;
        /**
         * @brief Combine x-, y-, z-direction indices into 1-D form index.
         * @param  [in] i_x x-axis index for i-th grid..
         * @param  [in] i_y y-axis index for i-th grid.
         * @param  [in] i_z z-axis index for i-th grid.
         * @return 1-D form index.
         **/
        int combine(int i_x,int i_y,int i_z) const;
        /**
         * @brief Calculate distance of given basis from origin.
         * @param[in] i_x x_axis index.
         * @param[in] i_y y-axis index.
         * @param[in] i_z z-axis index.
         * @return Distance of input point from origin.
         **/
        double compute_distance(int i_x,int i_y, int i_z) const;
        /**
         * @brief Calculate 1-D basis component.
         * @param[in] j Axiswise index.
         * @param[in] point Position along given axis.
         * @param[in] axis 0 for x axis, 1 for y axis, and 2 for z axis.
         **/
        double compute_1d_basis(int j, double point, int axis) const;
        /**
         * @brief Calculate 1-D basis second derivative.
         * @param[in] j Axiswise index.
         * @param[in] point Position along given axis.
         * @param[in] axis 0 for x axis, 1 for y axis, and 2 for z axis.
         **/
        double compute_second_der(int j, double point, int axis) const;
        /**
         * @brief Calculate 1-D basis first derivative.
         * @param[in] j Axiswise index.
         * @param[in] point Position along given axis.
         * @param[in] axis 0 for x axis, 1 for y axis, and 2 for z axis.
         **/
        double compute_first_der(int j, double point, int axis) const;
        /**
         * @}
         */

//        bool operator== (RCP<Basis> mesh);
        Teuchos::RCP<Grid_Setting> get_grid_setting() const;
        Teuchos::RCP<Basis_Function> get_basis_function() const;

//        virtual int from_basic(int basic_grid_index)=0;
//        virtual int to_basic(int current_grid_index)=0;
        void write_cube(
            std::string name,
            Teuchos::RCP<const Atoms> atoms,
            Teuchos::RCP<Epetra_Vector> entity,
            int PID=0
        ) const;
        void write_cube(
            std::string name,
            Teuchos::RCP<const Atoms> atoms,
            Teuchos::RCP<Epetra_MultiVector> entity,
            int index_offset = 0
        ) const;
        void write_cube(
            std::string name,
            Teuchos::RCP<const Atoms> atoms,
            Teuchos::Array< Teuchos::RCP<Epetra_Vector> > entity
        ) const;
        
        void write_cube(
            std::string name,
            Teuchos::RCP<const Atoms> atoms,
            Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > entity,
            int index_offset = 0
        ) const;

        /**
         * @brief Read cube files and generate distributed dataformat (Epetra_Vector)
         * @param [in] names Cube file names 
         * @param [out] Epetra_Vector whose elements are obtained from the given cube file 
         * @param [in] is_bohr optional argument which determines whether cube file is written on Bohr unit or Angstrom 
         */
        void read_cube(
            std::string name,
            Teuchos::RCP<Epetra_Vector>& entity,
            bool is_bohr = true
        ) const;

        /**
         * @brief Read cube files and generate distributed dataformat (Epetra MultiVector)
         * @param [in] names Array of cube file names 
         * @param [out] Epetra_MultiVector whose elements are obtained from cube files 
         * @param [in] is_bohr optional argument which determines whether cube file is written on Bohr unit or Angstrom 
         */
        void read_cube(
            Teuchos::Array<std::string> names,
            Teuchos::RCP<Epetra_MultiVector>& entity,
            bool is_bohr = true
        ) const;

        /**
         * @brief Parallelly read cube files and generate distributed dataformat (Epetra MultiVector)
         * @param [in] Array of cube file names
         * @param [out] Epetra_MultiVector whose elements are obtained from cube files 
         * @param [in] is_bohr optional argument which determines whether cube file is written on Bohr unit or Angstrom 
         * */
        void parallel_read_cube(
            Teuchos::Array<std::string> names,
            Teuchos::RCP<Epetra_MultiVector>& entity,
            bool is_bohr = true
        ) const;


        /**
         * @brief This function read cube file and generate double array 
         * @param [in] name name of cube file 
         * @param [out] retval return value 
         * @param [in] is_bohr optional argument which determines whether cube file is written on Bohr unit or Angstrom 
         */
        void read_cube(std::string name, double* retval, bool is_bohr = true) const;

        void write_along_axis(
            std::string name, int axis,
            Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > entity,
            std::vector<double> offset = std::vector<double>(),
            int index_offset = 0
        ) const;    
        void write_along_axis(
            std::string name, int axis,
            Teuchos::RCP<Epetra_MultiVector> entity,
            std::vector<double> offset = std::vector<double>(),
            int index_offset = 0
        ) const;
        void write_along_axis(
            std::string name, int axis,
            Teuchos::Array< Teuchos::RCP<Epetra_Vector> > entity,
            std::vector<double> offset = std::vector<double>()
        ) const;
        void write_along_axis(
            std::string name, int axis,
            Teuchos::RCP<Epetra_Vector> entity,
            std::vector<double> offset = std::vector<double>()
        ) const;

        void write_along_axis(
            std::string name,
            std::vector< std::array<double,3> > positions,
            Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > entity,
            int index_offset = 0
        ) const;
        void write_along_axis(
            std::string name,
            std::vector< std::array<double,3> > positions,
            Teuchos::RCP<Epetra_MultiVector> entity,
            int index_offset = 0
        ) const;
        void write_along_axis(
            std::string name,
            std::vector< std::array<double,3> > positions,
            Teuchos::Array< Teuchos::RCP<Epetra_Vector> > entity
        ) const;
        void write_along_axis(
            std::string name,
            std::vector< std::array<double,3> > positions,
            Teuchos::RCP<Epetra_Vector> entity
        ) const;
        /*void density_along_axis(
            std::vector< std::array<double,3> > positions,
            Teuchos::RCP<Epetra_MultiVector>  entity,
            std::vector<std::array<double,4> > return_vector
        ) const;
*/
        void parallel_write_cube(
                   std::string name,
                Teuchos::RCP<const Atoms> atoms,
                   Teuchos::RCP<Epetra_MultiVector> entity,
                int index_offset=0
        ) const;

        void write_polarizability(
            std::string name,
            Teuchos::RCP<const Atoms> atoms,
            std::vector<double> wavelength,
            std::vector<std::complex<float> > polarizability
        ) const;

        void write_dipole(
            std::string name,
            Teuchos::RCP<const Atoms> atoms,
            std::vector<double> wavelength,
            std::vector<std::vector< std::vector<std::complex<float> > > > dipole
        ) const;

        void get_stencil(int* max_stencil_x,
                int* max_stencil_y,
                int* max_stencil_z) const ;
        /**
         * @brief Return number of periodic axis.
         * @return Number of periodic axis, 0-3.
         **/
        int get_periodicity() const;
        //(PBC) n_cutoff test
        int get_Hartree_cutoff() const;
        double get_kinetic_cutoff() const;

        /**
         * @brief Iterate given function for given basis indicies.
         * @details Usage example:
         * 
         * @code{.cpp}
         * int iatom = 0;
         * std::function<double(double, double, double)> dist_function = [atoms, iatom](double x, double y, double z) -> double{
         *      std::array<double,3> atom_pos = atoms -> get_positions()[iatom];
         *      double r = std::sqrt(pow(x-atom_pos[0], 2) + pow(y-atom_pos[1], 2) + pow(z-atom_pos[2], 2));
         *      return r;
         * };
         * std::vector<int> indicies;
         * indicies.push_back(0); indicies.push_back(mesh -> get_original_size()-1);
         * std::vector<double> rv = mesh -> iterate<double>(dist_function, indicies);
         * std::cout << "Atom " << iatom << " distance from first and last mesh points : " << rv[0] << ", " << rv[1] << std::endl;
         * @endcode
         * 
         * $ Atom 0 distance from first and last mesh points : 12.0, 12.0
         * @param[in] func Function of type T, arguments are x, y, z position.
         * @param[in] indicies List of indicies to iterate.
         * @return Vector of retval of func on indicies.
         **/
        template<typename T>
        std::vector<T> iterate(std::function<T(double, double, double)> func, std::vector<int> indicies);

        void find_nearest_displacement(
            int index, double x_src, double y_src, double z_src,
            double &xi, double &yi, double &zi, double &r
        )const ;
    protected:
        double compute_kinetic(int i, int j, int axis) const;
        //int compute_kinetic_matrix();
        Teuchos::RCP<Grid_Setting> grid_setting;
        Teuchos::RCP<Basis_Function> basis;
        Teuchos::RCP<Basis_Function> kinetic_basis;
        Teuchos::RCP<Epetra_Map> map;

        // (PBC) n_cutoff test
        int n_cutoff;
        // kinetic cutoff for checking kinetic matrix sparsity
        double kinetic_cutoff;
};

// Should be defined here to avoid undefined reference error.
template<typename T>
std::vector<T> Basis::iterate(std::function<T(double, double, double)> func, std::vector<int> indicies){
    std::vector<T> retval;
    for(int i2 = 0; i2 < indicies.size(); ++i2){
        int i = indicies[i2];
        double x, y, z;
        this -> get_position(i, x, y, z);
        retval.push_back(func(x, y, z));
    }
    return retval;
}
