#include "Grid_Basic.hpp"

/*
class Grid_Basic: public Grid_Setting{
    public:
        void decompose(int index, int* points, int* i_x, int* i_y, int* i_z);
        int  combine(int i_x,int i_y,int i_z,int* points);
        Grid_Basic(int* points);
    protected:
        int* points;
//                double* get_all_points(double* scaling,double** grid);
};
*/

Grid_Basic::Grid_Basic(std::array<int,3> points){
    this->grid_type="Basic" ;

    this->points[0] = points[0];
    this->points[1] = points[1];
    this->points[2] = points[2];
    
    size=points[0]*points[1]*points[2];
}

Grid_Basic::~Grid_Basic(){
}

//int Grid_Basic::decompose(int index, int* points){
void Grid_Basic::decompose(int index, std::array<int,3> points, int& i_x, int& i_y, int& i_z){
    /*
    int decomposed_values[3];
    decomposed_values[0]=(index%points[0]);
    decomposed_values[1]=((index/points[0])%points[1]);
    decomposed_values[2]=((index/points[0])/points[1]);
    return decomposed_values;
    */
    i_x = index%points[0];
    i_y = (index/points[0])%points[1];
    i_z = (index/points[0])/points[1];
    return;
}

int Grid_Basic::combine(int i_x,int i_y, int i_z, std::array<int,3> points){
    int return_val =i_x+points[0]*(i_y+points[1]*i_z);
    if (return_val>size){
        return -return_val;
    }
    //std::cout << i_x << " " << points[0] << " " << i_y << " " << points[1] << " " << i_z << " " << points[2] << std::endl;
    if( i_x < 0 or i_x >= points[0] ) return -2;
    if( i_y < 0 or i_y >= points[1] ) return -2;
    if( i_z < 0 or i_z >= points[2] ) return -2;
    return return_val;
}

int Grid_Basic::from_basic(int basic_grid_index){
    return basic_grid_index;
}
int Grid_Basic::to_basic(int current_grid_index){
    return current_grid_index;
}


/*
int main (){
    Grid_Basic* grid = new Grid_Basic();
    int points[3] = {1,2,3};
    int i_x,i_y,i_z;
    grid->decompose(0,points,i_x,i_y,i_z);
    cout << i_x <<endl;

}
*/
