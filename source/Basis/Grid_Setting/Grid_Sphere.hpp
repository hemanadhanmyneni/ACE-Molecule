#pragma once 
#include "Teuchos_RCP.hpp"
#include "Grid_Setting.hpp"
#include "../Basis_Function/Basis_Function.hpp"
#include <vector>

/**
 * @brief This class should be hidden behind Basis class and its method should not be called explicitly.
 **/
class Grid_Sphere: public Grid_Setting{
    public:
        /**
         * @brief Decompose 1-D form index into the indices of x-, y-, z-coordinates.
         * @param [in] index 1-D form index.
         * @param [in] points Number of points of each axis.
         * @param [out] i_x Pointer to x-axis index.
         * @param [out] i_y Pointer to y-axis index.
         * @param [out] i_z Pointer to z-axis index.
         */
        void decompose(int index, std::array<int,3> points, int& i_x, int& i_y, int& i_z);
        /**
         * @brief Combine x-, y-, z-direction indices into 1-D form index.
         * @param [in] i_x x-axis index.
         * @param [in] i_y y-axis index.
         * @param [in] i_z z-axis index.
         * @param [out] points 1-D form index.
         */
        int  combine(int i_x,int i_y,int i_z, std::array<int,3> points);
        /**
         * @brief Convert basic grid index to sphere grid index.
         * @param [in] basic_grid_index Basic grid index.
         */
        int from_basic(int basic_grid_index);
        /**
         * @brief Convert sphere grid index to basic grid index.
         * @param [in] current_grid_index Sphere grid index.
         */
        int to_basic(int current_grid_index);

        ///This is constructor that specifies radius of total grid.
        //Grid_Sphere(int* points, Teuchos::RCP<Basis_Function> basis, double radius);  //shchoi 17.03.17
        //Grid_Sphere(const int* points, Teuchos::RCP<Basis_Function> basis, double radius);//shchoi 17.03.17
        ///This is constructor. The radius is set as maximum of given values.
        //Grid_Sphere(int* points, Teuchos::RCP<Basis_Function> basis);//shchoi 17.03.17
        Grid_Sphere(std::array<int,3> points, Teuchos::RCP<Basis_Function> basis);
        ~Grid_Sphere();


    protected:
        void scan_boundary(Teuchos::RCP<Basis_Function> basis,int v_x,int v_y, int v_z);
        Teuchos::RCP<Grid_Setting> grid_basic;
        std::vector<int> match_sphere_to_basic;
        std::vector<int> match_basic_to_sphere;
        //double* get_all_points(double* scaling,double** grid);
};

