#include "Grid_Sphere.hpp"
#include <cmath>
#include "Grid_Basic.hpp"

using std::max;
using std::abs;
using Teuchos::RCP;

// no more used; 17.03.17 shchoi
/*
Grid_Sphere::Grid_Sphere(int* points, RCP<Basis_Function> basis,double radius){
    grid_type="Sphere";
    grid_basic = new Grid_Basic(points);
    
    int i_x=0,i_y=0,i_z=0;
    double distance=0;

    size = points[0]*points[1]*points[2];
    this->points = new int[3];
    this->points[0] = points[0];
    this->points[1] = points[1];
    this->points[2] = points[2];

    for (int i=0; i< size;i++ ){
        grid_basic->decompose(i,points,i_x,i_y,i_z);
        distance=basis->compute_distance(i_x,i_y,i_z);
        if (distance-radius<1e-6  ){
            match_sphere_to_basic.push_back(i);
            match_basic_to_sphere.push_back(match_sphere_to_basic.size()-1);
        }
        else{
            // If point is deleted, the value in vector is -1
            match_basic_to_sphere.push_back(-1);
        }
    }

    size=match_sphere_to_basic.size();
}
Grid_Sphere::Grid_Sphere(const int* points, RCP<Basis_Function> basis,double radius){
    grid_type="Sphere";
    grid_basic = new Grid_Basic(points);

    int i_x=0,i_y=0,i_z=0;
    double distance=0;

    size = points[0]*points[1]*points[2];
    this->points = new int[3];
    this->points[0] = points[0];
    this->points[1] = points[1];
    this->points[2] = points[2];

    for (int i=0; i< size;i++ ){
        grid_basic->decompose(i,points,i_x,i_y,i_z);
        distance=basis->compute_distance(i_x,i_y,i_z);
        if (distance-radius<1e-6 ){
            match_sphere_to_basic.push_back(i);
            match_basic_to_sphere.push_back(match_sphere_to_basic.size()-1);
        }
        else{
            match_basic_to_sphere.push_back(-1);
        }
    }

    size=match_sphere_to_basic.size();
}
Grid_Sphere::Grid_Sphere(int* points, RCP<Basis_Function> basis){
    grid_type="Sphere";
    grid_basic = new Grid_Basic(points);

    int i_x=0,i_y=0,i_z=0;
    double distance=0;
    const double ** scaled_grid = basis->get_scaled_grid();
    double radius = max(max(abs(scaled_grid[0][0]),abs(scaled_grid[1][0])),abs(scaled_grid[2][0]));
    //cout << "Grid Sphere set radius as " << radius <<endl;
    size = points[0]*points[1]*points[2];

    this->points = new int [3];
    this->points[0]  = points[0];
    this->points[1]  = points[1];
    this->points[2]  = points[2];

    for (int i=0; i< size;i++ ){
        grid_basic->decompose(i,points,i_x,i_y,i_z);
        distance=basis->compute_distance(i_x,i_y,i_z);
        //cout << "distance "<<distance <<endl;

        if (distance-radius<1e-6  ){
            match_sphere_to_basic.push_back(i);
            match_basic_to_sphere.push_back(match_sphere_to_basic.size()-1);
        }
        else{
            //cout << "else" <<endl;
            match_basic_to_sphere.push_back(-1);
        }
    }
    size=match_sphere_to_basic.size();
}
*/
Grid_Sphere::Grid_Sphere(std::array<int,3> points, RCP<Basis_Function> basis){
    grid_type="Sphere";
    grid_basic = Teuchos::rcp(new Grid_Basic(points));

    int i_x=0,i_y=0,i_z=0;
    double distance=0;
    const double ** scaled_grid = basis->get_scaled_grid();
    double radius = max(max(abs(scaled_grid[0][0]),abs(scaled_grid[1][0])),abs(scaled_grid[2][0]));
    size = points[0]*points[1]*points[2];
    this->points[0]  = points[0];
    this->points[1]  = points[1];
    this->points[2]  = points[2];

    for (int i=0; i< size;i++ ){
        grid_basic->decompose(i,points,i_x,i_y,i_z);
        distance=basis->compute_distance(i_x,i_y,i_z);
        if (distance-radius<1e-6 ){
            match_sphere_to_basic.push_back(i);
            match_basic_to_sphere.push_back(match_sphere_to_basic.size()-1);
        }
        else{
            match_basic_to_sphere.push_back(-1);
        }
    }
    size=match_sphere_to_basic.size();
}

Grid_Sphere::~Grid_Sphere(){
}

void Grid_Sphere::decompose(int index, std::array<int,3> points, int& i_x, int& i_y, int& i_z){
    int index_in_basic_grid_setting = match_sphere_to_basic[index];
    grid_basic->decompose(index_in_basic_grid_setting,points,i_x,i_y,i_z);
    return;
}

int Grid_Sphere::combine(int i_x,int i_y, int i_z, std::array<int,3> points){
    int index_in_basic_grid_setting =  grid_basic->combine(i_x,i_y,i_z, points);
    if(index_in_basic_grid_setting<0){
        return index_in_basic_grid_setting;  // return error number!!
    }
    return match_basic_to_sphere[index_in_basic_grid_setting];
}

int Grid_Sphere::from_basic(int basic_grid_index){
    return match_basic_to_sphere[basic_grid_index];
}
int Grid_Sphere::to_basic(int current_grid_index){
    return match_sphere_to_basic[current_grid_index];
}
/*
int main (){
    int points[3]={13,13,13};
    double scaling[3]={0.1,0.1,0.1};
    Basis_Function* basis = new Sinc (points,scaling);
    Grid_Setting* grid_setting = new Grid_Sphere(points,basis,0.6);
    int i_x=0,i_y=0,i_z=0;
    grid_setting->decompose(0,points,i_x,i_y,i_z);
    return 0;
}
*/
