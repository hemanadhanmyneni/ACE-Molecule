#include "Grid_Setting.hpp"
#include "../../Utility/Verbose.hpp"

using std::vector;
using std::ostream;

int Grid_Setting::get_size() const {
    return size;
}
std::string Grid_Setting::get_type()const{
    return grid_type;
}
std::array<int,3> Grid_Setting::get_points() const{
    return points;
}
vector< int* > Grid_Setting::get_boundary(){
    return boundary;
}
bool Grid_Setting::operator== (Grid_Setting* grid_setting) const{

    if ( (this->get_type()).compare(grid_setting->get_type() ) !=0){
        return false;
    }
    if( this->get_size()!= grid_setting->get_size() ){
        return false;
    }


    if( this->get_points()[0] != grid_setting->get_points()[0]){
        return false;
    }
    if( this->get_points()[1] != grid_setting->get_points()[1]){
        return false;
    }
    if( this->get_points()[2] != grid_setting->get_points()[2]){
        return false;
    }


    return true;
}


ostream &operator <<(ostream &c, const Grid_Setting &grid_setting){
    Verbose::single() <<"====== Grid_Setting ========" <<std::endl;
    //cout << grid_setting.grid_type <<endl;
    Verbose::single() << "This grid_setting is " << grid_setting.grid_type << " type" << std::endl;
    Verbose::single() << "# of grid points on each axis are " << grid_setting.points[0]<<","<< grid_setting.points[1]<<","<< grid_setting.points[2]<< std::endl;
    Verbose::single() << "total # of grid points is " << grid_setting.size <<std::endl;
    Verbose::single() << "============================" <<std::endl;
    return c;
}
ostream &operator <<(ostream &c, const Grid_Setting* pgrid_setting){
    Verbose::all() << *pgrid_setting ;
    return c;
}
