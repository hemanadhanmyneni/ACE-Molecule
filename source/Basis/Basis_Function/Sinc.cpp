#include "../Basis_Function/Sinc.hpp"
#include <array>
#include "../../Utility/Verbose.hpp"
#include <cmath>

using std::abs;
/*
class Sinc: public Basis {
    private:
        double sinc(double x);
        void compute_grid();
    public:
        Sinc (int * _point, double * _scaling);

        double compute_kinetic(int i, int j, int axis);
        double compute_1d_basis(int j, double point, int axis);
        double compute_second_der(int j, double point, int axis);
};
*/

Sinc::Sinc (std::array<int,3> _points, std::array<double,3> _scaling, int periodicity){
    this->basis_type = "Sinc" ;
    this->points[0] = _points[0];
    this->points[1] = _points[1];
    this->points[2] = _points[2];
    // (PBC)
    this->periodicity = periodicity;
    // If "Periodicity" is 3, x,y,z-axis are periodic.
    // If "Periodicity" is 2, x,y-axis are periodic.
    // If "Periodicity" is 1, x-axis is periodic.
    if (periodicity == 3){
        //this->periodicity_array = {1,1,1};
        this->periodicity_array[0] = 1;
        this->periodicity_array[1] = 1;
        this->periodicity_array[2] = 1;
    }
    else if (periodicity == 2){
        //this->periodicity_array = {1,1,0};
        this->periodicity_array[0] = 1;
        this->periodicity_array[1] = 1;
        this->periodicity_array[2] = 0;
    }
    else if (periodicity == 1){
        //this->periodicity_array = {1,0,0};
        this->periodicity_array[0] = 1;
        this->periodicity_array[1] = 0;
        this->periodicity_array[2] = 0;
    }

    size=points[0]*points[1]*points[2];

    compute_grid();

    this->scaling[0] = _scaling[0];
    this->scaling[1] = _scaling[1];
    this->scaling[2] = _scaling[2];

    compute_scaled_grid();

}

Sinc::~Sinc(){
    for(int i = 0; i < 3; ++i){
        delete[] this -> grid[i];
        delete[] this -> scaled_grid[i];
    }
    delete[] this -> grid;
    delete[] this -> scaled_grid;
}

double Sinc::sinc(double x){
    if(x==0){
        return double (1.0);
    }
    
    return sin(M_PI*x)/(M_PI*x);
}
// (PBC) 주석 compute_grid() 수정. 한 쪽 끝 점을 포함하지 않는 grid생성하도록
// periodicity가 없으면 원래 grid를 쓰고, periodicity가 있을 때는 else밑의 grid를 쓰도록.
void Sinc::compute_grid(){
    double ** _grid = new double* [3];
    if ( periodicity==0 ){
        for (int i=0;i<3;i++){
            _grid[i] = new double[points[i]];
        }
    
        for (int i =0;i<3;i++){
            for (int j =0;j<points[i];j++){
                _grid[i][j]=-0.5*(double((points[i]-1)))+j;
            }
        }
    }
    else{
        for (int i=0;i<3;i++){
            _grid[i] = new double[points[i]];
        }
    
        for (int i =0;i<3;i++){
            for (int j =0;j<points[i];j++){
                _grid[i][j]=-0.5*(double((points[i])))+j;
            }
        }
    }


    this->grid=_grid;

    return;
}

/*
double Sinc::compute_kinetic(int i,int j, int axis){
    if (grid[axis][j]==grid[axis][i]){
    
        return 1.6449340668482264*pow(scaling[axis],-2);
    }
    else{
        return pow(double(-1),double (i-j))/((scaling[axis]*(grid[axis][i]-grid[axis][j]))*(scaling[axis]*(grid[axis][i]-grid[axis][j])));    
    }
}
*/
// (PBC)
double Sinc::compute_kinetic(int i, int j, int axis){
    
    if (periodicity_array[axis] == 1){
        if (grid[axis][j] == grid[axis][i]){
            return 1.6449340668482264*pow(scaling[axis],-2) + pow(scaling[axis]*points[axis],-2) * pow(M_PI, 2) / 3; 
            // points[axis] = number of grid points in each axis
        }
        else{
            return pow(double(-1),double (i-j))*pow(scaling[axis], -2)*pow(M_PI, 2)*pow(sin(M_PI*(grid[axis][i] - grid[axis][j]) / points[axis] ), -2) * pow(points[axis] , -2);
        }
    }
    else{
        if (grid[axis][j]==grid[axis][i]){
            return 1.6449340668482264*pow(scaling[axis],-2);
        }
        else{
            return pow(double(-1),double (i-j))/((scaling[axis]*(grid[axis][i]-grid[axis][j]))*(scaling[axis]*(grid[axis][i]-grid[axis][j])));    
        }
    }
}

double Sinc::compute_1d_basis(int j, double point,int axis){
    double x=(point/scaling[axis])-grid[axis][j];
    return pow(scaling[axis],-0.5)*sinc(x);

}
/*
double Sinc::compute_second_der(int j, double point, int axis){

    if (abs(point-scaled_grid[axis][j])<1E-13){
        return -3.289868133696453*pow(scaling[axis],-2.5);
    }
    
    double dif = M_PI*(point/scaling[axis]-grid[axis][j]);
    double inverse_dif=1.0/dif;

    double return_val=M_PI*M_PI;
    return_val*=(2.0-dif*dif)*sin(dif)-2.0*dif*cos(dif);
    return_val*=pow(scaling[axis],-2.5)*(inverse_dif*inverse_dif*inverse_dif);

    return return_val;
}
*/
//(PBC)
double Sinc::compute_second_der(int j, double point, int axis){

    double dif = M_PI*(point/scaling[axis] - grid[axis][j]);

    /*
    if (periodicity_array[axis] == 1){
        int n_cutoff = 1000;

        double return_val = 0;

        for (int n = -n_cutoff; n <= n_cutoff; n++){
            double point_pbc = dif + n*points[axis]*M_PI;
            double return_tmp = 0;
            return_tmp += (2 - pow(point_pbc, 2))*sin(point_pbc) - 2*point_pbc*cos(point_pbc);
            return_tmp /= pow(point_pbc,3);
            return_val += 2*return_tmp;

            // convergence test
            if ( n == n_cutoff && abs(return_tmp) > 1E-4 ){
                Verbose::single(Verbose::Normal) << "compute_second_der does not be converged!!" << std::endl;
                Verbose::single(Verbose::Normal) << "return_tmp = " << return_tmp << std::endl;
            }
        }
        return_val *= M_PI*M_PI;
        return_val *= pow(scaling[axis], -2.5);
        return return_val;
    }
    */

    if (abs(point-scaled_grid[axis][j])<1E-13){
        return -3.289868133696453*pow(scaling[axis],-2.5);
    }

    //double dif = M_PI*(point/scaling[axis]-grid[axis][j]);
    double inverse_dif=1.0/dif;

    double return_val=M_PI*M_PI;
    return_val*=(2.0-dif*dif)*sin(dif)-2.0*dif*cos(dif);
    return_val*=pow(scaling[axis],-2.5)*(inverse_dif*inverse_dif*inverse_dif);

    return return_val;
}
/*
double Sinc::compute_first_der(int j, double point, int axis){
    if ( abs(point -scaled_grid[axis][j])<1E-13){
        return 0.0;
    }
    point = M_PI*(point/scaling[axis]-grid[axis][j]);
    double return_val = point*cos(point)-sin(point);
    return_val/=point*point;
    return_val*=pow(scaling[axis],-1.5);
    return return_val*M_PI;
}
*/
//(PBC)
double Sinc::compute_first_der(int j, double point, int axis){
    if ( abs(point -scaled_grid[axis][j])<1E-13){
        return 0.0;
    }
    //dif = (point/scaling[axis] - grid[axis][j]);
    //dif_pi = M_PI*dif;
    double dif_pi = M_PI*(point/scaling[axis]-grid[axis][j]);

    //(PBC)
    // Analytic solution

    if (periodicity_array[axis] == 1){
        int dif = round(point/scaling[axis] - grid[axis][j]);

        if ( points[axis] % 2 == 0){
            double return_val = (M_PI / points[axis]);
            return_val *= cos(M_PI * dif / points[axis]);
            return_val /= sin(M_PI * dif / points[axis]);
            return_val *= pow(scaling[axis], -1.5);

            if ( dif % 2 == 0 ){
                return return_val;
            }
            else{
                return -return_val;
            }
        }
        else if ( points[axis] % 2 == 1){
            double tmp = M_PI / (2 * points[axis]);
            double return_val = cos(tmp * dif) / sin(tmp * dif);
            return_val -= cos(tmp* (dif - points[axis])) / sin(tmp * (dif - points[axis]));
            return_val *= tmp;
            return_val *= pow(scaling[axis], -1.5);

            if ( dif % 2 == 0 ){
                return return_val;
            }
            else{
                return -return_val;
            }
        }
    }

    double return_val = dif_pi*cos(dif_pi)-sin(dif_pi);
    return_val/=dif_pi*dif_pi;
    return_val*=pow(scaling[axis],-1.5);
    return return_val*M_PI;
}
bool Sinc::is_uniform(){
    return true;
}

double Sinc::get_weight_factor(int j,int axis){
  return 1.0;
}

double Sinc::get_weight_function(double point,int axis){
  return 1.0;
}

int Sinc::set_scaling(std::array<double,3> scaling){
    this->scaling=scaling;
    compute_scaled_grid();
    return 0;
}
void Sinc::get_neighboring_points(int axis, int axis_points, std::vector<int>& neighboring_points){
    neighboring_points.clear();
    for(int i=0; i<points[axis]; i++){
        neighboring_points.push_back(i);
    }
}
/*
int main (){

    int points[3]={13,11,7};
    double scaling[3]={0.5,0.4,0.3};
    double tmp_scaling[3]={1,1,1};
//    Sinc* basis = new Sinc (points,scaling);
    Sinc* basis = new Sinc (points,tmp_scaling);
    basis->set_scaling(scaling);

    cout << "number of points on x-axis \t" <<basis->get_points()[0] <<endl;
    cout << "scaling on x-axis \t"<<basis->get_scaling()[0] <<endl;
    cout << "0th pre-scaled point on x-axis \t"<<(basis->get_grid())[0][0] << endl;
    cout << "0th scaled point on x-axis \t"<<basis->get_scaled_grid()[0][0] <<endl;
    cout << "first derivative \t" << basis->compute_first_der(0,1,0) <<endl;

//    cout << basis->compute_second_der(0,0,2) <<endl; 
//    cout << basis->compute_1d_basis(0,0.0,0) <<endl; 
//    cout << basis->compute_distance(0,0,0) <<endl;
    return 0;
}

*/
