#pragma once
#include <array>
/**
 * @brief This class and its children should be hidden behined Basis class, except for special propose (Kernel_Integral).
 **/
class Legendre{
    public:
        Legendre(std::array<int,3> _point, std::array<double,3> _scaling);
        ~Legendre();

        std::array<double,3> get_scaling();
        double** get_grid();
        std::array<int,3> get_points();
        double get_weight_factor(int j, int axis);
        double get_weight_function(double point, int axis);

        double** get_scaled_grid();
        int set_scaling(std::array<double,3> scaling);

        void gauleg(double x1, double x2, double* x, double* w, int n);
        //void get_neighboring_points(int axis, int axis_points, std::vector<int>& neighboring_points);    
    private:
        std::array<int,3> points;
        std::array<double,3> scaling;
        double** grid;
        double** scaled_grid=NULL;
        int size;

        double** weight_factor;

        void compute_grid_and_weight_factors();
        bool is_computed_weight;
};
