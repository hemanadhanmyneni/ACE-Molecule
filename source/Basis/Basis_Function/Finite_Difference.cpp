#include "../Basis_Function/Finite_Difference.hpp"

#include <cmath>
#include "../../Utility/Verbose.hpp"

#include <cstdlib>

using std::labs;
using std::fabs;
using std::endl;

Finite_Difference::Finite_Difference (std::array<int,3> _points, std::array<double,3> _scaling, int periodicity, int der_order){
    this->basis_type = "Finite_Difference" ;
    this->points[0] = _points[0];
    this->points[1] = _points[1];
    this->points[2] = _points[2];

    size=points[0]*points[1]*points[2];
    compute_grid();

    this->scaling[0] = _scaling[0];
    this->scaling[1] = _scaling[1];
    this->scaling[2] = _scaling[2];

    this->der_order = der_order;
    compute_coef();
    compute_scaled_grid();

    this->periodicity = periodicity;
    if (periodicity == 3){
        //this->periodicity_array = {1,1,1};
        this->periodicity_array[0] = 1;
        this->periodicity_array[1] = 1;
        this->periodicity_array[2] = 1;
    }
    else if (periodicity == 2){
        //this->periodicity_array = {1,1,0};
        this->periodicity_array[0] = 1;
        this->periodicity_array[1] = 1;
        this->periodicity_array[2] = 0;
    }
    else if (periodicity == 1){
        //this->periodicity_array = {1,0,0};
        this->periodicity_array[0] = 1;
        this->periodicity_array[1] = 0;
        this->periodicity_array[2] = 0;
    }
}

Finite_Difference::~Finite_Difference(){
    for(int i = 0; i < 3; ++i){
        delete[] this -> grid[i];
        delete[] this -> scaled_grid[i];
    }
    delete[] this -> grid;
    delete[] this -> scaled_grid;
}

void Finite_Difference::compute_coef(){
    switch (der_order){
        case 3:
            second_center_coef.push_back(1);
            second_center_coef.push_back(-2);
            second_center_coef.push_back(1);

            first_center_coef.push_back(-0.5);
            first_center_coef.push_back(0.0);
            first_center_coef.push_back(-0.5);
            break;

        case 5:
            second_center_coef.push_back(-1.0/12.0);
            second_center_coef.push_back(4.0/3.0);
            second_center_coef.push_back(-5.0/2.0);
            second_center_coef.push_back(4.0/3.0);
            second_center_coef.push_back(-1.0/12.0);
            
            first_center_coef.push_back(1.0/12.0);
            first_center_coef.push_back(-2.0/3.0);
            first_center_coef.push_back(0.0);
            first_center_coef.push_back(2.0/3.0);
            first_center_coef.push_back(-1.0/12.0);
            break;

        case 7:
            second_center_coef.push_back(1.0/90.0);
            second_center_coef.push_back(-3.0/20.0);
            second_center_coef.push_back(3.0/2.0);
            second_center_coef.push_back(-49.0/18.0);
            second_center_coef.push_back(3.0/2.0);
            second_center_coef.push_back(-3.0/20.0);
            second_center_coef.push_back(1.0/90.0);

            first_center_coef.push_back( -1.0/60.0);
            first_center_coef.push_back( 3.0/20.0);
            first_center_coef.push_back( -3.0/4.0);
            first_center_coef.push_back( 0.0);
            first_center_coef.push_back(3.0/4.0);
            first_center_coef.push_back(-3.0/20.0);
            first_center_coef.push_back( 1.0/60.0);
            break;
   
        case 9:
            first_center_coef.push_back(1.0/280.0);
            first_center_coef.push_back(-4.0/105.0);
            first_center_coef.push_back(1.0/5.0);
            first_center_coef.push_back(-4.0/5.0);
            first_center_coef.push_back(0.0);
            first_center_coef.push_back(4.0/5.0);
            first_center_coef.push_back(-1.0/5.0);
            first_center_coef.push_back(4.0/105.0);
            first_center_coef.push_back(-1.0/280.0);
                       
            second_center_coef.push_back(-1.0/560.0);
            second_center_coef.push_back(8.0/315.0);
            second_center_coef.push_back(-1.0/5.0);
            second_center_coef.push_back(8.0/5.0);
            second_center_coef.push_back(-205.0/72.0);
            second_center_coef.push_back(8.0/5.0);
            second_center_coef.push_back(-1.0/5.0);
            second_center_coef.push_back(8.0/315.0);
            second_center_coef.push_back(-1.0/560.0);
            break;

        case 11:
            first_center_coef.push_back(-1.0/1260.0);
            first_center_coef.push_back(5.0/504.0);
            first_center_coef.push_back(-5.0/84.0);
            first_center_coef.push_back(5.0/21.0);
            first_center_coef.push_back(-5.0/6.0);
            first_center_coef.push_back(0.0);
            first_center_coef.push_back(5.0/6.0);
            first_center_coef.push_back(-5.0/21.0);
            first_center_coef.push_back(5.0/84.0);
            first_center_coef.push_back(-5.0/504.0);
            first_center_coef.push_back(1.0/1260.0);

            second_center_coef.push_back(1.0/3150.0);
            second_center_coef.push_back(-5.0/1008.0);
            second_center_coef.push_back(5.0/126.0);
            second_center_coef.push_back(-5.0/21.0);
            second_center_coef.push_back(5.0/3.0);
            second_center_coef.push_back(-5269.0/1800.0);
            second_center_coef.push_back(5.0/3.0);
            second_center_coef.push_back(-5.0/21.0);
            second_center_coef.push_back(5.0/126.0);
            second_center_coef.push_back(-5.0/1008.0);
            second_center_coef.push_back(1.0/3150.0);
            break;

        case 13:
            first_center_coef.push_back(1.0/5544.0);
            first_center_coef.push_back(-1.0/385.0);
            first_center_coef.push_back(1.0/56.0);
            first_center_coef.push_back(-5.0/63.0);
            first_center_coef.push_back(15.0/56.0);
            first_center_coef.push_back(-6.0/7.0);
            first_center_coef.push_back(0.0);
            first_center_coef.push_back(6.0/7.0);
            first_center_coef.push_back(-15.0/56.0);
            first_center_coef.push_back(5.0/63.0);
            first_center_coef.push_back(-1.0/56.0);
            first_center_coef.push_back(1.0/385.0);
            first_center_coef.push_back(-1.0/5544.0);
    
            second_center_coef.push_back(-1.0/16632.0);
            second_center_coef.push_back(2.0/1925.0);
            second_center_coef.push_back(-1.0/112.0);
            second_center_coef.push_back(10.0/189.0);
            second_center_coef.push_back(-15.0/56.0);
            second_center_coef.push_back(12.0/7.0);
            second_center_coef.push_back(-5369.0/1800.0);
            second_center_coef.push_back(12.0/7.0);
            second_center_coef.push_back(-15.0/56.0);
            second_center_coef.push_back(10.0/189.0);
            second_center_coef.push_back(-1.0/112.0);
            second_center_coef.push_back(2.0/1925.0);
            second_center_coef.push_back(-1.0/16632.0);
            break;

        // This is not fully tested.
        case 21:
            first_center_coef.push_back(5.4125441125705218e-07);
            first_center_coef.push_back(-1.2027875805107519e-05);
            first_center_coef.push_back(0.0001285479226620488);
            first_center_coef.push_back(-0.00088147146965767082);
            first_center_coef.push_back(0.0043706293703086058);
            first_center_coef.push_back(-0.016783216781858047);
            first_center_coef.push_back(0.052447552443401518);
            first_center_coef.push_back(-0.13986013985049445);
            first_center_coef.push_back(0.34090909089149779);
            first_center_coef.push_back(-0.90909090906520895);
            first_center_coef.push_back(0.0);
            first_center_coef.push_back(0.90909090906520895);
            first_center_coef.push_back(-0.34090909089149779);
            first_center_coef.push_back(0.13986013985049445);
            first_center_coef.push_back(-0.052447552443401518);
            first_center_coef.push_back(0.016783216781858047);
            first_center_coef.push_back(-0.0043706293703086058);
            first_center_coef.push_back(0.00088147146965767082);
            first_center_coef.push_back(-0.0001285479226620488);
            first_center_coef.push_back(1.2027875805107519e-05);
            first_center_coef.push_back(-5.4125441125705218e-07);

            second_center_coef.push_back(-1.0825088238734188e-07);
            second_center_coef.push_back(2.6728612931475671e-06);
            second_center_coef.push_back(-3.213698069930479e-05);
            second_center_coef.push_back(0.00025184899156026021);
            second_center_coef.push_back(-0.001456876457868163);
            second_center_coef.push_back(0.0067132867166915531);
            second_center_coef.push_back(-0.026223776232812142);
            second_center_coef.push_back(0.09324009325906453);
            second_center_coef.push_back(-0.34090909094111044);
            second_center_coef.push_back(1.8181818182257485);
            second_center_coef.push_back(-3.0995354623824349);
            second_center_coef.push_back(1.8181818182257485);
            second_center_coef.push_back(-0.34090909094111044);
            second_center_coef.push_back(0.09324009325906453);
            second_center_coef.push_back(-0.026223776232812142);
            second_center_coef.push_back(0.0067132867166915531);
            second_center_coef.push_back(-0.001456876457868163);
            second_center_coef.push_back(0.00025184899156026021);
            second_center_coef.push_back(-3.213698069930479e-05);
            second_center_coef.push_back(2.6728612931475671e-06);
            second_center_coef.push_back(-1.0825088238734188e-07);
            break;

        default:
            Verbose::all() << "Unsupported DerivativesOrder." << endl;
            // Find more at: http://math.stackexchange.com/questions/1398250/finite-difference-differentiation-formula
            exit(EXIT_FAILURE);
            break;
        
/*
    // This is not fully tested.
    case 41:
            first_center_coef.push_back(3.6185705122395563e-13);
            first_center_coef.push_back(-1.5238601860733137e-11);
            first_center_coef.push_back(3.1371342314269368e-10);
            first_center_coef.push_back(-4.2081515947918135e-09);
            first_center_coef.push_back(4.1365090098158071e-08);
            first_center_coef.push_back(-3.1773583816204892e-07);
            first_center_coef.push_back(1.9861672752284356e-06);
            first_center_coef.push_back(-1.0390802840770935e-05);
            first_center_coef.push_back(4.6440891610489469e-05);
            first_center_coef.push_back(-0.00018016033996378001);
            first_center_coef.push_back(0.00061443083607015355);
            first_center_coef.push_back(-0.0018621511520068839);
            first_center_coef.push_back(0.0050633290849181837);
            first_center_coef.push_back(-0.012464948777198386);
            first_center_coef.push_back(0.028048925051986428);


-0.058346930463165109
0.11396764294747387
-0.21454136905460147
0.41122629163098218
-0.95235298109302868
-2.3361374424517509e-05
0.95239752777810482
-0.41126488916216863
0.21457173648497499
-0.11398930829801977
0.058360919336922355
-0.028057078390707042
0.012469224381346258
-0.0050653381657157666
0.0018629928543051799
-0.00061474330916343059
0.00018026235794806701
-4.6469910038744664e-05
1.0397909513807188e-05
-1.9876431291862502e-06
3.1799055164177796e-07
-4.140062325459897e-08
4.2119993962886769e-09
-3.1401673562107285e-10
1.5254076636053695e-11
-3.6224037654893426e-13


            second_center_coef.push_back(-1.0825088238734188e-07);
            break;
*/
    }
    return;
}

void Finite_Difference::compute_grid(){
    double ** _grid = new double* [3];
    if ( periodicity==0 ){
        for (int i=0;i<3;i++){
            _grid[i] = new double[points[i]];
        }
    
        for (int i =0;i<3;i++){
            for (int j =0;j<points[i];j++){
                _grid[i][j]=-0.5*(double((points[i]-1)))+j;
            }
        }
    }
    else{
        for (int i=0;i<3;i++){
            _grid[i] = new double[points[i]];
        }
    
        for (int i =0;i<3;i++){
            for (int j =0;j<points[i];j++){
                _grid[i][j]=-0.5*(double((points[i])))+j;
            }
        }
    }


    this->grid=_grid;

    return;
}

double Finite_Difference::compute_kinetic(int i,int j, int axis){

    int order=(der_order-1)/2;
    int dif=j-i;
    //int dif=i-j;

    // (PBC)
    if (periodicity_array[axis] == 1){
        int n[] = {j-i, j-i+points[axis], j-i-points[axis]};
        int min = n[0];
        for (int k = 1; k < 3; k++){
            if (abs(n[k]) < abs(min)){
                min = n[k];
            }
        }
        dif = min;
    }

    if (labs(dif)>order){
        return 0.0;
    }

    return -0.5*second_center_coef[dif+order]/(scaling[axis]*scaling[axis]);
}

double Finite_Difference::compute_1d_basis(int j, double point,int axis){
    if (fabs(point -scaled_grid[axis][j]) <0.5*scaling[axis]){
        return pow(scaling[axis],-0.5);
    }
    else{
        return 0.0;
    }

}
// used to construct Poisson matrix 
double Finite_Difference::compute_second_der(int j, double point, int axis){

    if (point < scaled_grid[axis][0]-(der_order/2.0)*scaling[axis]){
        Verbose::single(Verbose::Normal) << "Finite_Difference::compute_second_der range error!" << std::endl;
        return 0.0;
    }
    else if (point > scaled_grid[axis][points[axis]-1]+(der_order/2.0)*scaling[axis]){
        Verbose::single(Verbose::Normal) << "Finite_Difference::compute_second_der range error!" << std::endl;
        return 0.0;
    }

    int i=floor((point-scaled_grid[axis][0])/scaling[axis]+0.5);

    if (this->periodicity == 1 && i==points[axis]+1){
        i = 0;
    }
    //cout << "the nearest grid index with given point \t" <<     i <<endl;

    int order=(der_order-1)/2;
    int dif=j-i;
    //int dif=i-j;
    
    // (PBC)
    if (periodicity_array[axis] == 1){
        int n[] = {j-i, j-i+points[axis], j-i-points[axis]};
        int min = n[0];
        for (int k = 1; k < 3; k++){
            if (abs(n[k]) < abs(min)){
                min = n[k];
            }
        }
        dif = min;
    }

    if (labs(dif)>order){
        return 0.0;
    }

    return second_center_coef[dif+order]*pow(scaling[axis],-2.5); 
}

double Finite_Difference::compute_first_der(int j, double point, int axis){
    if (point < scaled_grid[axis][0]-(der_order/2.0)*scaling[axis]){
        Verbose::single(Verbose::Normal) << "Finite_Difference::compute_first_der range error!" << std::endl;
        return 0.0;
    }
    else if (point > scaled_grid[axis][points[axis]-1]+(der_order/2.0)*scaling[axis]){
        Verbose::single(Verbose::Normal) << "Finite_Difference::compute_first_der range error!" << std::endl;
        return 0.0;
    }

    int i=floor((point-scaled_grid[axis][0])/scaling[axis]+0.5);

    // If the periodicity is turned off, simulation box starts at (-0.5,-0.5,-0.5) and ends at (L+0.5,L+0.5,L+0.5)
    // However, if the periodicity is turned on, (0,0,0) point in unit cell is the same point with grid point (0,0,0)
    // Therefore, the derivatives at (L+0.5+x,L+0.5+y,L+0.5+z) should be calculated as follows. (x,y,z)<0.5 
    if (this->periodicity == 1 && i==points[axis]+1){
        i = 0;
    }

    int order=(der_order-1)/2;
    //int dif=j-i;
    int dif=i-j;

    if (periodicity_array[axis] == 1){
        int n[] = {i-j, i-j+points[axis], i-j-points[axis]};
        int min = n[0];
        for (int k = 1; k < 3; k++){
            if (abs(n[k]) < abs(min)){
                min = n[k];
            }
        }
        dif = min;
    }

    if (labs(dif)>order){
        return 0.0;
    }
    //Verbose::single(Verbose::Normal) << "compute_first_der" << std::endl;
    //Verbose::single(Verbose::Normal) << " (" << i << ", " << j << ")  = " << first_center_coef[dif+order] << std::endl;

    return first_center_coef[dif+order] / scaling[axis];
}

bool Finite_Difference::is_uniform(){
    return true;
}

double Finite_Difference::get_weight_factor(int j,int axis){
  return 1.0;
}

double Finite_Difference::get_weight_function(double point,int axis){
  return 1.0;
}

int Finite_Difference::set_scaling(std::array<double,3> scaling){
    this->scaling=scaling;
    compute_scaled_grid();
    return 0;
}
void Finite_Difference::get_neighboring_points(int axis, int axis_points, std::vector<int>& neighboring_points){
    neighboring_points.clear();
    if (periodicity_array[axis] == 1){
        for (int i=0; i < points[axis]; i++){
            neighboring_points.push_back(i);
        }
    }
    else{
        int stencil = der_order/2;
        for(int i=axis_points-stencil; i<=axis_points+stencil; i++){
            neighboring_points.push_back(i);
        }
    }

    // Compare Time
    /*
    if (periodicity_array[axis] == 1){
        int stencil = der_order/2;
        for (int i = axis_points - stencil; i <= axis_points + stencil; i++){
            if (i >= 0){
                neighboring_points.push_back(i);
            }
            else{
                neighboring_points.push_back(i+points[axis]);
            }
        }
    }
    else{
        int stencil = der_order/2;
        for (int i = axis_points - stencil; i <= axis_points + stencil; i++){
            neighboring_points.push_back(i);
        }
    }
    */
}

/*
int main (){

    int points[3]={31,31,31};
    double tmp_scaling[3]={0.3,0.3,0.3};
//    Sinc* basis = new Sinc (points,tmp_scaling);

    Finite_Difference* basis = new Finite_Difference (points,tmp_scaling);
    basis->set_scaling(tmp_scaling);
    Grid_Setting* grid_setting = new Grid_Sphere (points,basis);
    int size = grid_setting->get_size();
    Epetra_SerialComm Comm;
    Epetra_Map Map(grid_setting->get_size(),0,Comm);

    Teuchos::RCP<Epetra_CrsMatrix> hamiltonian = Teuchos::rcp(new Epetra_CrsMatrix(Copy,Map,0) );
    double* local_potential = new double[grid_setting->get_size()];
    double** scaled_grid = basis->get_scaled_grid();

    double value;
    int i_x=0,i_y=0,i_z=0;
    int j_x=0,j_y=0,j_z=0;
    for (int i=0;i<size;i++){
        grid_setting->decompose(i,basis->get_points(),i_x,i_y,i_z);
        for (int j=i;j<size;j++){
            grid_setting->decompose(j,basis->get_points(),j_x,j_y,j_z);
            value=0;
            if(i_y==j_y && i_z==j_z){
                value+=basis->compute_kinetic(i_x,j_x,0);
            }
            if(i_x==j_x && i_z==j_z){
                value+=basis->compute_kinetic(i_y,j_y,1);
            }
            if(i_y==j_y && i_x==j_x){
                value+=basis->compute_kinetic(i_z,j_z,2);
            }
            if(value!=0){
                if (i==j){
                    hamiltonian->InsertGlobalValues(i,1,&value,&j);
                    continue;
                }

                hamiltonian->InsertGlobalValues(i,1,&value,&j);
                hamiltonian->InsertGlobalValues(j,1,&value,&i);
            }
        }
    }

//    for (int i =0; i<grid_setting->get_size(); i++){
//        for (int j=i;j<grid_setting->get_size();j++){
//            double value = basis->compute_kinetic(i,j,0);
//            hamiltonian->InsertGlobalValues(i,1,&value,&j);
//            hamiltonian->InsertGlobalValues(j,1,&value,&i);
//        }
//        local_potential[i] = (0.5*scaled_grid[0][i]*scaled_grid[0][i]);
//    }

    for (int i =0; i<grid_setting->get_size(); i++){
        grid_setting->decompose(i,basis->get_points(),i_x,i_y,i_z);
        local_potential[i] = 0.5*(scaled_grid[0][i_x]*scaled_grid[0][i_x]+scaled_grid[1][i_y]*scaled_grid[1][i_y]+scaled_grid[2][i_z]*scaled_grid[2][i_z]);
    }
    hamiltonian->FillComplete();
    Teuchos::RCP<Epetra_Vector> external_potential_vector = Teuchos::rcp( new Epetra_Vector(Copy,hamiltonian->RowMap(),local_potential) );
    Teuchos::RCP<Epetra_Vector> diagonal =Teuchos::rcp(new Epetra_Vector(hamiltonian->RowMap(),false));
    hamiltonian->ExtractDiagonalCopy(*diagonal);
    diagonal->Update(1.0,*external_potential_vector,1.0);
    hamiltonian->ReplaceDiagonalValues(*diagonal);

    int numev=3;
    cout <<"start diagonalize" <<endl;
    Teuchos::RCP < Anasazi::BasicEigenproblem<double,MV,OP> > MyProblem =Teuchos::rcp( new Anasazi::BasicEigenproblem<double,MV,OP>() );
    int verb = Anasazi::Warnings + Anasazi::Errors+ Anasazi::FinalSummary + Anasazi::TimingDetails;
    MyProblem->setNEV( numev );
    MyProblem->setHermitian(true);
    MyProblem->setOperator(hamiltonian);
    Teuchos::RCP<Epetra_MultiVector> initial_eigenvector = Teuchos::rcp( new Epetra_MultiVector (Map,numev)  );
    initial_eigenvector->Random();
    MyProblem->setInitVec(initial_eigenvector);
    bool boolret=MyProblem->setProblem();
    Teuchos::ParameterList MyPL;
    MyPL.set( "Verbosity", verb );
    MyPL.set( "Which", "SR" );
    //MyPL.set( "Use Locking", true );
    MyPL.set( "Convergence Tolerance", 0.00000001 );
    MyPL.set( "Full Ortho", true );
    Anasazi::LOBPCGSolMgr<double,MV,OP> MySolverMan(MyProblem, MyPL );
    Anasazi::ReturnType solverreturn = MySolverMan.solve();

    Anasazi::Eigensolution<double, Epetra_MultiVector> solution;
    solution = MyProblem->getSolution();
    for (int i=0;i<numev;i++){
         cout << "eigenvalue   "<< i << "\t "<< solution.Evals[i].real <<endl;
         
    }


//    cout << "number of points on x-axis \t" <<basis->get_points()[0] <<endl;
//    cout << "scaling on x-axis \t"<<basis->get_scaling()[0] <<endl;
//    cout << "0th pre-scaled point on x-axis \t"<<(basis->get_grid())[0][0] << endl;
//    cout << "0th scaled point on x-axis \t"<<basis->get_scaled_grid()[0][0] <<endl;
//    cout << "first derivative \t" << basis->compute_first_der(0,1,0) <<endl;
//   cout << "compute_second_der \t" << basis->compute_second_der(6,0.0,0) <<endl;
    
//    cout << basis->compute_second_der(0,0,2) <<endl; 
//    cout << basis->compute_1d_basis(0,0.0,0) <<endl; 
//    cout << basis->compute_distance(0,0,0) <<endl;
    return 0;
}
*/
