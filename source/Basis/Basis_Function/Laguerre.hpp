#pragma once
#include <array>

/**
 * @brief This class and its children should be hidden behined Basis class, except for special propose.
 **/
class Laguerre{
    public:
        Laguerre(std::array<int,3>_point, std::array<double,3> _scaling);
        ~Laguerre();

        std::array<double,3> get_scaling();
        double** get_grid();
        std::array<int,3> get_points();
        double get_weight_factor(int j, int axis);
        double get_weight_function(double point, int axis);

        double** get_scaled_grid();
        int set_scaling(std::array<double,3> scaling);
        int* get_neighboring_points(int axis, int axis_points, int* neighboring_points);    

    private:
        std::array<int,3> points;
        std::array<double,3> scaling;
        double** grid;
        double** scaled_grid=NULL;
        int size;
        double **weight_factor;

        void compute_grid_and_weight_factors();
        void compute_scaled_grid();

        void gaulag(double *x, double *w, int n);
        double gammln(double xx);

};

