#pragma once 
#include <iostream>
#include <vector>
#include <string>
#include <array>
/**
 * @brief This class and its children should be hidden behined Basis class, except for special propose.
 **/
class Basis_Function{
    friend std::ostream &operator <<(std::ostream &c, const Basis_Function &basis);
    friend std::ostream &operator <<(std::ostream &c, const Basis_Function* pbasis);

    public:
        /**
         * @brief Virtual destructor for abstract class.
         **/
        virtual ~Basis_Function(){};
        /// get points 
        std::array<int,3> get_points();
        /// Don't use it. Instead, you can use the same function in Grid_Setting class
        int get_size();
        /// This gives scaling factor. The length of returned array is 3
        std::array<double,3> get_scaling();
        /// This gives . This is not real grid points. You can refer get_scaled_grid()
        double** get_grid();
        /// This returns scaled grid points. get_grid does not return real grid value. 
        const double** get_scaled_grid();
        /// This returns the type of basis function.
        
        // (PBC)
        int get_periodicity() const;
        std::string get_type();

        /// This function set scaling factor. This function call compute_scaled_grid. Thus you don't need to call explicity.
        virtual int set_scaling(std::array<double,3> scaling)=0;

        /// This function compute distance from origin to given grid point.
        double compute_distance(int i_x,int i_y, int i_z);
        double compute_distance(int i_x,int i_y, int i_z, int j_x, int j_y, int j_z);

        bool operator== (Basis_Function* basis);

        virtual bool is_uniform()=0;

        virtual double compute_kinetic(int i, int j, int axis)=0;
        virtual double compute_1d_basis(int j, double point, int axis)=0;
        virtual double compute_second_der(int j, double point, int axis)=0;
        virtual double compute_first_der(int j, double point, int axis)=0;
        virtual double get_weight_factor(int j,int axis)=0;    
        virtual double get_weight_function(double point,int axis)=0;    
        virtual void get_neighboring_points(int axis, int axis_points, std::vector<int>& neighboring_points)=0;    
        virtual int get_order(){return -1;};

    protected:
        /**
         * @brief be maked by compute_grid function.
         * @details If Periodicity = 0, there are grid points in both side in cell.
         * @details If Periodicity != 0, there is only grid points in left side in cell.
         * @details Example) When Points = 3,
         * @details if Periodicity = 0, grid = [-1, 0, 1]
         * @details if Periodicity != 0, grid = [-1.5, -0.5, 0.5] 
         */
        std::array<int,3> points;
        std::array<double,3> scaling;
        double** grid;
        double** scaled_grid=NULL;
        int size;
        // (PBC)
        int periodicity=0;
        int periodicity_array[3] = {0,0,0};
        //std::array<int,3> periodicity_array = {0,0,0};
        virtual void compute_grid()=0;
        void compute_scaled_grid();
        std::string basis_type;
};
