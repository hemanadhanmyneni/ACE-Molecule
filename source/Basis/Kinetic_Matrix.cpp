#include "Kinetic_Matrix.hpp"
#include <iostream>
#include <cmath>
#include <cstdlib>
#include "../Utility/Verbose.hpp"
#include "../Utility/Parallel_Manager.hpp"
#include "../Utility/Time_Measure.hpp"

#ifdef ACE_HAVE_MPI
    #include "Epetra_MpiComm.h"
#endif
#ifdef ACE_HAVE_OMP
    #include "omp.h"
#endif

using std::vector;
using Teuchos::rcp;
using Teuchos::RCP;
//static int static_value = 0;

Kinetic_Matrix::Kinetic_Matrix( RCP< const Basis > basis){
    this->basis = basis;
    make_kinetic_vector();
    this->kinetic_cutoff = basis->get_kinetic_cutoff();
}

int Kinetic_Matrix::multiply(RCP<Epetra_MultiVector> input, RCP<Epetra_MultiVector> &output) const{
//    Verbose::single() <<"kinetic multiply will be done by not making kinetic matrix" << std::endl;
    const int size = basis->get_original_size();
    const int NumMyElements = basis->get_map()->NumMyElements();
    int* MyGlobalElements = basis->get_map()->MyGlobalElements();
    //const int numVec = input->NumVectors();

    //RCP<Teuchos::Time> commtime = Teuchos::TimeMonitor::getNewCounter("comm");
    //commtime->start();

#ifdef ACE_HAVE_MPI
    int* sizeList = new int[basis->get_map()->Comm().NumProc()];
    Parallel_Manager::info().group_allgather(const_cast<int*>(&NumMyElements), 1, sizeList, 1 );
    int* displs = new int[basis->get_map()->Comm().NumProc()]();

    for(int i = 0; i < basis->get_map()->Comm().NumProc(); i++){
        if (i!=0) {displs[i] = displs[i-1]+sizeList[i-1];}
        else{ displs[i]=0; }
    }
    for(int j=0; j< input->NumVectors(); j++){
        double* pValue = new double[NumMyElements];
        if (input->operator()(j)->ExtractCopy(pValue)!=0){
            Verbose::all() << "Failed to Extract Kinetic Matrix multiplication vector!" << std::endl;
            exit(-1);
        }
        double* combine_input = new double[size];
//        memset(combine_input,0.0, size);
        Parallel_Manager::info().group_allgatherv(pValue, NumMyElements, combine_input, sizeList, displs);
        for(int i=0; i<NumMyElements; i++){
            vector<int> index;
            vector<double> value;
            make_row(MyGlobalElements[i], value, index);

            double dot = 0;
            for(int k=0; k<index.size(); k++){
                if(std::abs(value[k]) > 1.0E-30){
                    dot += value[k]*combine_input[index[k]];
                }
            }
            output->operator[](j)[i] = dot;
        }
        delete combine_input;
        delete pValue;
    }
    return 0;
#else
    Verbose::all() << "This serial version of calculating kinetic version is not stable" << endl;
    exit(-1);
#endif
}
int Kinetic_Matrix::multiply2(RCP<Epetra_MultiVector> input, RCP<Epetra_MultiVector> &output){
    RCP<Epetra_CrsMatrix> kinetic_matrix = rcp(new Epetra_CrsMatrix(Copy, *basis -> get_map(), 0) );
    add(kinetic_matrix, 1.0);
    kinetic_matrix->FillComplete();
    kinetic_matrix->Multiply(false, *input, *output);
    return 0;
}
int Kinetic_Matrix::multiply3(RCP<Epetra_MultiVector> input, RCP<Epetra_MultiVector> &output){
    RCP<Time_Measure> timer  = rcp(new Time_Measure() );
    if(!hold_kinetic_crsmatrix){
        kinetic_crsmatrix = rcp(new Epetra_CrsMatrix(Copy, *basis -> get_map(), 0) );
        add(kinetic_crsmatrix, 1.0);
        kinetic_crsmatrix->FillComplete();
        hold_kinetic_crsmatrix=true;
    }
    kinetic_crsmatrix->Multiply(false, *input, *output);
    return 0;
}
int Kinetic_Matrix::multiply(const Anasazi::MultiVec<double>& X,
        Anasazi::MultiVec<double>& Y) const{
    return 0;
}

int Kinetic_Matrix::add(RCP<Epetra_CrsMatrix> matrix, RCP<Epetra_CrsMatrix> &output, double scalar){
    output = Teuchos::null;
    output = rcp(new Epetra_CrsMatrix(*matrix) );
    return this -> add(output, scalar);
}

int Kinetic_Matrix::add(RCP<Epetra_CrsMatrix> &matrix, double scalar){
    int* MyGlobalElements = matrix->Map().MyGlobalElements();
    int NumMyElements = matrix->Map().NumMyElements();
#ifdef ACE_HAVE_OMP
    Verbose::single(Verbose::Detail)<< "Kinetic_Matrix::add omp_num_threads" << omp_get_num_threads() <<std::endl;
#endif
    if(matrix->Filled()){
        return this -> add_filled(matrix, scalar);
    }

    int ierr;

/////shchoi
    int sum1=0; int sum2=0;
    //int new_sum1=0; int new_sum2=0;
/////
    #ifdef ACE_HAVE_OMP
    #pragma omp parallel for reduction(+:sum1)
    #endif
    for(int i=0; i<NumMyElements; i++){
        vector<int> index;
        vector<double> value;
        int index_size;

        make_row(MyGlobalElements[i], value, index);
        index_size = index.size();
        sum1+=index.size();
        for(int k=0; k<value.size(); k++){
            value[k] = value[k]*scalar;
        }
        ierr = matrix->InsertGlobalValues(MyGlobalElements[i], index_size, value.data(), index.data());
        if(ierr != 0){
            Verbose::all() << "Kinetic_Matrix::add InsertGlobalValues error!: " << ierr << std::endl;
            Verbose::all() << *matrix << std::endl;
            exit(-1);
        }
    }
    //matrix->Comm().SumAll(&sum1,&new_sum1,1);
    matrix->FillComplete();
    //sum2 = matrix->NumMyNonzeros();
    //matrix->Comm().SumAll(&sum2,&new_sum2,1);

    //Verbose::single()<< "Kinetic_Matrix::add:   "<< new_sum1<<"\t" <<new_sum2 <<std::endl;
    //exit(-1);
    return ierr;
}


int Kinetic_Matrix::add_filled(RCP<Epetra_CrsMatrix> &matrix, double scalar){
    int* MyGlobalElements = matrix->Map().MyGlobalElements();
    int NumMyElements = matrix->Map().NumMyElements();

    int ierr;
    #ifdef ACE_HAVE_OMP
    #pragma omp parallel for
    #endif
    for(int i=0; i<NumMyElements; i++){
        vector<int> index;
        vector<double> value;
        int index_size;

        make_row(MyGlobalElements[i], value, index);
        index_size = index.size();
        for(int k=0; k<value.size(); k++){
            value[k] = value[k]*scalar;
        }

        //for(int k=0; k<index_size; k++){
        //    index[k] = matrix->ColMap().LID(index[k]);
        //}
        //ierr = matrix->SumIntoMyValues(i, index_size, &value[0], &index[0]);

        ierr = matrix->SumIntoGlobalValues(MyGlobalElements[i], index_size, &value[0], &index[0]);
        //Parallel_Manager::info().all_barrier();
        //std::cout<< *matrix<<std::endl;
        //Parallel_Manager::info().all_barrier();
        if(ierr != 0){
            Verbose::all() << "Kinetic_Matrix::add_filled SumIntoGlobalValues error!: " << ierr << std::endl;
            Verbose::all() << "TRY Kinetic_Matrix::add with not Filled matrix" << std::endl;
            throw -1;
            exit(-1);
        }
    }

    //if (static_value ==2) exit(-1);
    //static_value++;
    return ierr;
}

void Kinetic_Matrix::make_kinetic_vector(){
    vector< double > tmp;
    for(int x=0; x< basis->get_points()[0]; x++){
        tmp.push_back(basis->compute_kinetic(0,x,0));
    }
    kinetic_vector.push_back(tmp);
    tmp.clear();
    for(int y=0; y< basis->get_points()[1]; y++){
        tmp.push_back(basis->compute_kinetic(0,y,1));
    }
    kinetic_vector.push_back(tmp);
    tmp.clear();
    for(int z=0; z< basis->get_points()[2]; z++){
        tmp.push_back(basis->compute_kinetic(0,z,2));
    }
    kinetic_vector.push_back(tmp);

    return ;
}

void Kinetic_Matrix::make_row(int row_index, vector<double>& value, vector<int>& index) const{
    int i_x=0,i_y=0,i_z=0;
    basis->decompose(row_index, i_x, i_y, i_z);  // by lkh
    value.clear();
    index.clear();
    vector<int> neighboring_points;

    basis->get_neighboring_points(0, i_x,neighboring_points);
    for(int a=0;a<neighboring_points.size();a++){
        if(neighboring_points[a]>=0){
            int ind = basis->combine(neighboring_points[a],i_y,i_z);
            if(ind >= 0){
                double tmp = kinetic_vector[0][abs(i_x-neighboring_points[a])];
                // kinetic cutoff test for compare sparsity
                //if(std::abs(tmp) > 1.0E-30){
                if(std::abs(tmp) > this->kinetic_cutoff){
                    //Verbose::single(Verbose::Detail) << "outside kinetic cutoff range!" << std::endl << 
                    //"a = " << a << std::endl << "kinetic value = " << tmp << std::endl;
                    index.push_back(ind);
                    value.push_back(tmp);
                }
                else{
                    //Verbose::single(Verbose::Detail) << "inside kinetic cutoff range!"  << std::endl << "kinetic value = " << tmp << std::endl;
                }
            }
        }
    }
    basis->get_neighboring_points(1, i_y,neighboring_points);
    for(int b=0;b<neighboring_points.size();b++){
        if(neighboring_points[b]>=0){
            int ind = basis->combine(i_x,neighboring_points[b],i_z);
            if(ind >= 0){
                double tmp = kinetic_vector[1][abs(i_y-neighboring_points[b])];
                // kinetic cutoff test 
                //if(std::abs(tmp) > 1.0E-30){
                if(std::abs(tmp) > this->kinetic_cutoff){
                    index.push_back(ind);
                    value.push_back(tmp);
                }
            }
        }
    }
    basis->get_neighboring_points(2, i_z,neighboring_points);

    for(int c=0;c<neighboring_points.size();c++){
        if(neighboring_points[c]>=0){
        int ind = basis->combine(i_x,i_y,neighboring_points[c]);
            if(ind >= 0){
                double tmp = kinetic_vector[2][abs(i_z-neighboring_points[c])];
                // kinetic cutoff test
                //if(std::abs(tmp) > 1.0E-30){
                if(std::abs(tmp) > this->kinetic_cutoff){
                    index.push_back(ind);
                    value.push_back(tmp);
                }
            }
        }
    }
    return ;
}

Kinetic_Matrix::~Kinetic_Matrix(){
    kinetic_crsmatrix = Teuchos::null;
}
