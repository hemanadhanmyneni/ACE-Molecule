#pragma once

#include "Teuchos_ParameterList.hpp"

#include "Basis.hpp"
#include "Basis_Function/Basis_Function.hpp"
#include "Basis_Function/Finite_Difference.hpp"
#include "Basis_Function/Sinc.hpp"
#include "Grid_Setting/Grid_Setting.hpp"
#include "Grid_Setting/Grid_Basic.hpp"
#include "Grid_Setting/Grid_Sphere.hpp"
#include "Grid_Setting/Grid_Atoms.hpp"



namespace Create_Basis{
    /**
     * @brief Create Basis class. See input manual for details.
     * @param[in] Comm Epetra communicator.
     * @param[in] parameters Parameter, should be subsection BasicInformation.
     * @param[in] atoms Atoms object required for Grid_Atoms.
     **/
    Teuchos::RCP<Basis> Create_Basis(const Epetra_Comm& Comm, Teuchos::RCP<Teuchos::ParameterList> parameters, Teuchos::RCP<Atoms> atoms = Teuchos::null);

    /**
     * @brief Create Basis centered at given atom with given absolute radius.
     * @details This class is useful for interpolating atom-centered quantities (pseudopotential, etc.) to simulation grid.
     * @param[in] points Number of points along each axis.
     * @param[in] scaling Scaling factor along each axis.
     * @param[in] comm Epetra cmmunicator.
     * @param[in] basis_type Basis type. Either Sinc or Finite_Difference.
     * @param[in] atom Atom object. Resulting mesh is always spherical centered on given atom.
     * @param[in] radius Absolute radius. Resulting mesh is spherical with radius specified with this variable.
     * @param[in] periodicity Number of periodic axes.
     * @return Basis class, atom-centered.
     **/
    Teuchos::RCP<Basis> Create_Auxiliary_Basis(std::array<int,3> points, std::array<double,3> scaling, const Epetra_Comm& comm, 
            std::string basis_type, Atom atom, double radius, int periodicity = 0
    );
}
