#include "Basis.hpp"

#include <sstream>
#include <fstream>
#include <ios>
#include <cmath>
#include <string>
#include <tuple>
#include <stdexcept>
#include <limits>
#include "../Utility/Parallel_Manager.hpp"
#include "../Utility/Parallel_Util.hpp"
//#include "../Utility/Interpolation/Tricubic_Interpolation.hpp"
#include "../Utility/Interpolation/Trilinear_Interpolation.hpp"
#include "../Utility/String_Util.hpp"
#include "../Utility/Verbose.hpp"

using std::ofstream;
using std::ostringstream;
using std::ostream;
using std::setw;
using std::endl;
using std::scientific;
using std::ifstream;
using std::istringstream;
using std::string;
using std::vector;
using Teuchos::rcp;
using Teuchos::Array;
using Teuchos::RCP;
using std::abs;

Basis::Basis(RCP<Basis_Function> basis, RCP<Basis_Function> kinetic_basis, RCP<Grid_Setting> grid_setting, RCP<Epetra_Map> map, int n_cutoff, double kinetic_cutoff){
    this -> basis= basis;
    this -> kinetic_basis = kinetic_basis;
    this -> grid_setting  = grid_setting;
    this -> map = map;
    // (PBC) n_cutoff test
    this -> n_cutoff = n_cutoff;
    this -> kinetic_cutoff = kinetic_cutoff;
}

/*
   Basis::~Basis(){
   delete basis;
   delete kinetic_basis;
   delete grid_setting;
   }
 */
RCP<const Epetra_Map> Basis::get_map()const {
    return this->map;
}
/*
   RCP<const Epetra_CrsMatrix> Basis::get_kinetic_matrix() const{
   return this->kinetic_matrix.getConst();
   }
 */
std::array<int,3> Basis::get_points() const{
    return basis->get_points();
}
void Basis::get_neighboring_points(int axis, int axis_points, vector<int>& neighboring_points) const{
    kinetic_basis->get_neighboring_points(axis, axis_points, neighboring_points);
}
std::array<double,3> Basis::get_scaling() const{
    return basis->get_scaling();
}
int Basis::get_original_size() const{
    return grid_setting->get_size();
}
string Basis::get_basis_type() const {
    return basis->get_type();
}
string Basis::get_grid_type() const {
    return grid_setting->get_type();
}
const double** Basis::get_scaled_grid() const{
    return basis->get_scaled_grid();
}

double Basis::compute_distance(int i_x,int i_y, int i_z) const{
    return basis->compute_distance(i_x, i_y, i_z);
}
double Basis::compute_distance(int i_x,int i_y, int i_z, int j_x, int j_y, int j_z, bool mic/* = false */) const{
    if( i_x < 0 or i_x >= get_points()[0] ) return std::numeric_limits<double>::signaling_NaN();
    if( i_y < 0 or i_y >= get_points()[1] ) return std::numeric_limits<double>::signaling_NaN();
    if( i_z < 0 or i_z >= get_points()[2] ) return std::numeric_limits<double>::signaling_NaN();
    if( j_x < 0 or j_x >= get_points()[0] ) return std::numeric_limits<double>::signaling_NaN();
    if( j_y < 0 or j_y >= get_points()[1] ) return std::numeric_limits<double>::signaling_NaN();
    if( j_z < 0 or j_z >= get_points()[2] ) return std::numeric_limits<double>::signaling_NaN();
    const double ** scaled_grid = this -> get_scaled_grid();
    double dr[3];
    dr[0] = scaled_grid[0][i_x]-scaled_grid[0][j_x]; dr[1] = scaled_grid[1][i_y]-scaled_grid[1][j_y]; dr[2] = scaled_grid[2][i_z]-scaled_grid[2][j_z];
    double dr_orig[3];
    for(int d = 0; d < 3; ++d){ dr_orig[d] = dr[d]; }
    std::array<double,3> cell_size = this -> get_grid_length();
    
    for(int d = 0; d < 3; ++d){
        if(d < this -> get_periodicity() and mic){
            // Axis d is periodic!
            for(int i = -1; i <= 1; ++i){
                if(std::abs(dr[d]) > std::abs(dr_orig[d] + i*cell_size[d])){
                    dr[d] = dr_orig[d] + i*cell_size[d];
                }
            }
        }
    }
    return sqrt(dr[0]*dr[0]+dr[1]*dr[1]+dr[2]*dr[2]);
    //return basis->compute_distance(i_x, i_y, i_z, j_x, j_y, j_z);
}

double Basis::compute_1d_distance(int axis, int i_x, int j_x, bool mic/* = false */) const{
    assert(0 <= axis and axis < 3);
    if( i_x < 0 or i_x >= get_points()[axis] ) return std::numeric_limits<double>::signaling_NaN();
    if( j_x < 0 or j_x >= get_points()[axis] ) return std::numeric_limits<double>::signaling_NaN();
    const double ** scaled_grid = this -> get_scaled_grid();
    double dr = scaled_grid[axis][i_x]-scaled_grid[axis][j_x];
    std::array<double,3> cell_size = this -> get_grid_length();

    if(axis < this -> get_periodicity() and mic){
        // Axis d is periodic!
        for(int i = -1; i <= 1; ++i){
            if(std::abs(dr) > std::abs(dr + i*cell_size[axis])){
                dr += i*cell_size[axis];
            }
        }
    }
    return std::abs(dr);
}

double Basis::find_closest_point(double x, double y, double z, int i_x, int i_y, int i_z) const{
    assert(this -> get_periodicity() == 0);// Not implemented
    auto get_distance = [](double x1, double y1, double z1, double x2, double y2, double z2)
                        {return std::sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)+(z1-z2)*(z1-z2));};

    int j,j_x,j_y,j_z;
    auto scaled_grid = get_scaled_grid();

    //////////////////////////////////////////////////////// find minimum using omp
    int* min_j = new int[Parallel_Manager::info().get_openmp_num_threads()];
    double* min_val = new double[Parallel_Manager::info().get_openmp_num_threads()];
    double distance;

    for(int i =0; i< Parallel_Manager::info().get_openmp_num_threads(); i++){
        min_val[i]=std::numeric_limits<double>::max();
    }

    #ifdef ACE_HAVE_OMP
    #pragma omp parallel for private(j,j_x,j_y,j_z,distance)
    #endif
    for(int i =0; i< map->NumMyElements(); i++){
        j = map->GID(i);
        decompose(j, j_x, j_y, j_z);
        distance = get_distance(x,y,z,scaled_grid[0][j_x],scaled_grid[1][j_y],scaled_grid[2][j_z]);
        if(distance<min_val[Parallel_Manager::info().get_openmp_thread_num()]){
            min_val[Parallel_Manager::info().get_openmp_thread_num()] = distance;
            min_j[Parallel_Manager::info().get_openmp_thread_num()] = j;
        }
    }
    int minimum_index =min_j[0];
    double minimum_value = min_val[0];
    for(int i=1; i<Parallel_Manager::info().get_openmp_num_threads(); i++){
        if(minimum_value>min_val[i]){
            minimum_value=min_val[i];
            minimum_index=min_j[i];
        }
    }
    delete [] min_j;
    delete [] min_val;
    //////////////////////////////////////////////////////////////////////////////
    int* minimum_indices = new int[Parallel_Manager::info().get_mpi_size()];
    double* minimum_values = new double [Parallel_Manager::info().get_mpi_size()];
    Parallel_Manager::info().group_allgather(&minimum_index, 1, minimum_indices, 1);
    Parallel_Manager::info().group_allgather(&minimum_value, 1, minimum_values, 1);

    minimum_index = minimum_indices[0];
    minimum_value = minimum_indices[0];
    for(int i=1; i<Parallel_Manager::info().get_mpi_size(); i++){
        if(minimum_value>minimum_values[i]){
            minimum_value = minimum_values[i];
            minimum_index = minimum_indices[i];
        }
    }
    decompose(minimum_index, i_x,i_y,i_z);

    delete [] minimum_indices;
    delete [] minimum_values;

    return minimum_value;
}
bool operator!=(const Basis& mesh1, const Basis& mesh2) {
    if (mesh1==(mesh2)){
        return false;
    }
    return true;
}
bool operator==(const Basis& basis1, const Basis& basis2){
    bool condition =true;
    if (basis2.get_points()[0]!=basis1.get_points()[0] or
            basis2.get_points()[1]!=basis1.get_points()[1] or
            basis2.get_points()[2]!=basis1.get_points()[2] )
    {
        condition = false;
    }
    if(basis2.get_scaling()[0]!=basis1.get_scaling()[0] or
            basis2.get_scaling()[1]!=basis1.get_scaling()[1] or
            basis2.get_scaling()[2]!=basis1.get_scaling()[2] )
    {
        condition = false;
    }
    if(basis2.get_basis_type() != basis1.get_basis_type()){
        condition = false;
    }
    if(basis2.get_grid_type()!=basis1.get_grid_type() ){
        condition = false;
    }
    if(basis2.get_original_size()!=basis1.get_original_size()){
        condition=false;
    }
    return condition;
}
double Basis::compute_kinetic(int i, int j, int axis) const{
    return kinetic_basis->compute_kinetic( i, j, axis);
}
double Basis::compute_1d_basis(int j, double point, int axis) const{
    return basis->compute_1d_basis(j,point,axis);
}
double Basis::compute_basis(int i, double x, double y, double z) const{
    int i_x, i_y, i_z;
    this -> decompose(i, i_x, i_y, i_z);
    return basis -> compute_1d_basis(i_x, x, 0) * basis -> compute_1d_basis(i_y, y, 1) * basis -> compute_1d_basis(i_z, z, 2);
}

double Basis::compute_second_der(int j, double point, int axis) const{
    return basis->compute_second_der(j, point, axis);
    //return kinetic_basis->compute_second_der(j, point, axis);
}
double Basis::compute_first_der(int j, double point, int axis) const{
    return basis->compute_first_der(j, point, axis);
    //return kinetic_basis->compute_first_der(j, point, axis);
}

void Basis::decompose(int index, int& i_x, int& i_y, int& i_z) const{
    return grid_setting->decompose(index,basis->get_points(),i_x,i_y,i_z);
}
int Basis::combine(int i_x,int i_y,int i_z) const{
    return grid_setting->combine(i_x,i_y,i_z,basis->get_points());
}

void Basis::get_position(int index, double& x, double& y, double& z) const{
    int i_x, i_y, i_z;
    this -> decompose(index, i_x, i_y, i_z);
    const double** scaled_grid = this -> get_scaled_grid();
    x = scaled_grid[0][i_x]; y = scaled_grid[1][i_y]; z = scaled_grid[2][i_z];
}

ostream &operator <<(ostream &c, const Basis &basis){
    Verbose::single() << *basis.basis <<std::endl;
    Verbose::single() << *basis.grid_setting <<std::endl;
    return c;
}
ostream &operator <<(ostream &c, const Basis* pbasis){
    Verbose::single() << *pbasis ;
    return c;
}

void Basis::write_cube(string name, RCP<const Atoms> atoms, RCP<Epetra_Vector> entity, int PID) const{
    int NumMyElements = map->NumMyElements();
    //int* MyGlobalElements= map->MyGlobalElements();
    //int MyPID = map->Comm().MyPID();
    int MyPID = Parallel_Manager::info().get_total_mpi_rank();
    const double** scaled_grid=get_scaled_grid();
    vector<std::array<double,3> > positions = atoms->get_positions();
    int nx = get_points()[0];
    int ny = get_points()[1];
    int nz = get_points()[2];
    double dx = get_scaling()[0];
    double dy = get_scaling()[1];
    double dz = get_scaling()[2];
    ofstream output;

    if(MyPID == PID){
        string filename = name + ".cube";
        output.open(filename.c_str());
        output << " <HEADER> ACE-Molecule CUBE FILE" << endl;
        output << " <HEADER> " << name << ".cube" << endl;

        output << setw(5) << atoms->get_size();
        output << std::fixed;
        output << std::setprecision(6) << setw(12) << scaled_grid[0][0] << setw(12) << scaled_grid[1][0] << setw(12) << scaled_grid[2][0] << "    1" << endl;
        output.unsetf(std::ios::floatfield);

        output << setw(5) << nx;
        output << std::fixed;
        output << std::setprecision(6) << setw(12) << dx << "    0.000000    0.000000" << endl;
        output.unsetf(std::ios::floatfield);

        output << setw(5) << ny;
        output << std::fixed;
        output << std::setprecision(6) << "    0.000000" << setw(12) << dy << "    0.000000" << endl;
        output.unsetf(std::ios::floatfield);

        output << setw(5) << nz;
        output << std::fixed;
        output << std::setprecision(6) << setw(12) << "    0.000000    0.000000" << setw(12) << dz << endl;
        output.unsetf(std::ios::floatfield);
        for(int i=0;i<atoms->get_size();i++){
            output << setw(5) << atoms->get_atomic_numbers()[i];
            output << std::fixed;
            output << std::setprecision(6) << setw(12) << (float)(atoms->get_atomic_numbers()[i]);
            output << std::setprecision(6) << setw(12) << positions[i][0];
            output << std::setprecision(6) << setw(12) << positions[i][1];
            output << std::setprecision(6) << setw(12) << positions[i][2];
            output << endl;
            output.unsetf(std::ios::floatfield);
        }
        output << scientific;
        output.precision(5);
    }

    // KSW REMOVE SUMALL 160107
    int dim = this -> get_original_size();
    double *val;
    if(MyPID==PID){
        val = new double[dim];
    }
    Parallel_Util::group_gather_vector(entity, val, PID);
    if(MyPID==PID){
        for(int ix=0;ix<nx;ix++){
            for(int iy=0;iy<ny;iy++){
                for(int iz=0;iz<nz;iz++){
                    int i = combine( ix, iy, iz );
                    if(i < 0){
                        output << setw(13) << 0.0;
                    }
                    else{
                        output << setw(13) << val[i];
                    }
                    if(iz%6 == 5 ) output << endl;
                }
                if((nz%6)!=0) output << endl;
            }
        }
        output.close();
        delete[] val;
    }

    return;
}

void Basis::write_cube(string name, RCP<const Atoms> atoms, RCP<Epetra_MultiVector> entity, int index_offset/* = 0*/) const{
    //for(int i=start_index;i<end_index;i++){
    int num_vec = entity->NumVectors();
    for(int i=0;i<num_vec;i++){
        //ostringstream os;
        //os << i+1+index_offset;
        //string multivector_index (os.str());
        string new_name = name + ".ind" + String_Util::fill0(i+1+index_offset,num_vec+index_offset);
        write_cube( new_name, atoms,rcp( new Epetra_Vector(*entity->operator()(i)) ));
    }
    return;
}

void Basis::write_cube(string name, RCP<const Atoms> atoms, Array<RCP<Epetra_Vector> > entity) const{
    int num_vec = entity.size();
    for(int i_spin=0;i_spin<num_vec;i_spin++){
        ostringstream os;
        os << i_spin;
        string spin_index (os.str());
        string new_name = name + ".s" + spin_index ;

        write_cube(new_name, atoms,entity[i_spin]);
    }
    return;
}

void Basis::write_cube(string name, RCP<const Atoms> atoms, Array<RCP<Epetra_MultiVector> > entity, int index_offset/* = 1*/) const{
    int num_vec = entity.size();
    for(int i_spin=0;i_spin<num_vec;i_spin++){
        ostringstream os;
        os << i_spin;
        string spin_index (os.str());
        string new_name = name + ".s" + spin_index ;
        //write_cube(new_name, atoms,entity[i_spin], index_offset);
        parallel_write_cube(new_name, atoms,entity[i_spin]);
    }
    return;
}

void Basis::parallel_read_cube(Array<string> names, RCP<Epetra_MultiVector>& entity, bool is_bohr/* = true*/) const{
    int dim = this -> get_original_size();
    int MyPID = Parallel_Manager::info().get_total_mpi_rank();
    int NumProcs = Parallel_Manager::info().get_total_mpi_size();
    int NumMyElements = map->NumMyElements();
    int* displs = new int[NumProcs];
    int* sizeList = new int[NumProcs];
    Parallel_Manager::info().group_allgather(&NumMyElements, 1, sizeList, 1);
    for(int i=0; i<NumProcs;i++){
        if (i!=0) {displs[i] = displs[i-1]+sizeList[i-1];}
        else{ displs[i]=0; }
    }

    vector<int> list;
    for(int i = 0; i < names.size(); ++i){
        if(i % NumProcs == MyPID){
            list.push_back(i);
        }
    }
    std::vector<std::vector<double> > input(list.size());
    for(int i=0; i<list.size(); ++i){
        std::vector<double> tmp (dim);
        this -> read_cube(names.at(list[i]), tmp.data(), is_bohr);
        input[i] = tmp;
    }
    Parallel_Manager::info().all_barrier();

    for(int j=0; j<NumProcs; ++j){
        int size = (j == MyPID)? input.size(): 0;
        Parallel_Manager::info().group_bcast(&size, 1, j);
        for(int i=0; i<size; ++i){
            std::vector<double> distributed(NumMyElements);
            double *inp2 = (i < input.size())? &input[i][0]: NULL;// This line is important to prevent segfaults if # of cube file > NumProcs!!!
            Parallel_Manager::info().group_scatterv(inp2, sizeList, distributed.data(), NumMyElements, displs, j);
            int id = (j == MyPID)? list[i]: 0;// This line is important to prevent segfaults if # of cube file < NumProcs!!!
            Parallel_Manager::info().group_bcast(&id, 1, j);
            for(int k=0; k<NumMyElements; ++k){
                entity->operator[](id)[k] = distributed[k];
            }
        }
    }

    delete[] displs;
    delete[] sizeList;
}

void Basis::read_cube(Array<string> names, RCP<Epetra_MultiVector>& entity, bool is_bohr/* = true*/) const{
    for(int i = 0; i < names.size(); ++i){
        RCP<Epetra_Vector> tmp = rcp(new Epetra_Vector(entity -> Map()));
        this -> read_cube(names[i], tmp, is_bohr);
        entity -> operator()(i) -> Update(1.0, *tmp, 0.0);
    }
}

void Basis::read_cube(string name, RCP<Epetra_Vector>& entity, bool is_bohr/* = true*/) const{
    vector<double> tmp(this -> get_original_size());
    this -> read_cube(name, tmp.data(), is_bohr);
    vector<int> indices(this -> get_original_size());
    for(int i = 0; i < this -> get_original_size(); ++i){
        indices[i] = i;
    }
    entity -> ReplaceGlobalValues(this -> get_original_size(), tmp.data(), indices.data());
}

void Basis::write_along_axis(string name, int axis, Array< RCP<Epetra_MultiVector> > entity, vector<double> offset/* = vector<double>()*/, int index_offset/* = 0*/) const{
    vector< std::array<double,3> > positions;
    const double ** scaled_grid = this -> get_scaled_grid();
    auto points = this -> get_points();

    if( axis == 0 ){ name += ".x"; }
    else if( axis == 1 ){ name += ".y"; }
    else if( axis == 2 ){ name += ".z"; }
    else {
        Verbose::all()  << "Mesh::write_along_axis - Wrong option. Option should be 0, 1, or 2." << std::endl;
        exit(EXIT_FAILURE);
    }

    for(int i = 0; i < points[axis]; ++i){
        std::array<double,3> pos;
        pos[axis] = scaled_grid[axis][i];
        if(offset.size() == 3){
            for(int j = 0; j < 3; ++j){
                pos[j] += offset[j];
            }
        }
        positions.push_back(pos);
    }

    this -> write_along_axis(name, positions, entity, index_offset);
    return;
}

void Basis::write_along_axis(string name, int axis, RCP<Epetra_MultiVector> entity, vector<double> offset/* = vector<double>()*/, int index_offset/* = 0*/) const{
    vector< std::array<double,3> > positions;
    const double ** scaled_grid = this -> get_scaled_grid();
    auto points = this -> get_points();

    if( axis == 0 ){ name += ".x"; }
    else if( axis == 1 ){ name += ".y"; }
    else if( axis == 2 ){ name += ".z"; }
    else {
        Verbose::all()  << "Mesh::write_along_axis - Wrong option. Option should be 0, 1, or 2." << std::endl;
        exit(EXIT_FAILURE);
    }

    for(int i = 0; i < points[axis]; ++i){
        std::array<double,3> pos;
        pos[axis] = scaled_grid[axis][i];
        positions.push_back(pos);
        if( offset.size() >= 3 ){
            for(int j = 0; j < 3; ++j){
                positions[i][j] += offset[j];
            }
        }
    }

    this -> write_along_axis(name, positions, entity, index_offset);
    return;
}

void Basis::write_along_axis(string name, int axis, Array< RCP<Epetra_Vector> > entity, vector<double> offset/* = vector<double>()*/) const{
    vector< std::array<double,3> > positions;
    const double ** scaled_grid = this -> get_scaled_grid();
    auto points = this -> get_points();

    if( axis == 0 ){ name += ".x"; }
    else if( axis == 1 ){ name += ".y"; }
    else if( axis == 2 ){ name += ".z"; }
    else {
        Verbose::all()  << "Mesh::write_along_axis - Wrong option. Option should be 0, 1, or 2." << std::endl;
        exit(EXIT_FAILURE);
    }

    for(int i = 0; i < points[axis]; ++i){
        std::array<double,3> pos;
        pos[axis] = scaled_grid[axis][i];
        positions.push_back(pos);
        if( offset.size() >= 3 ){
            for(int j = 0; j < 3; ++j){
                positions[i][j] += offset[j];
            }
        }
    }

    this -> write_along_axis(name, positions, entity);
    return;
}

void Basis::write_along_axis(string name, int axis, RCP<Epetra_Vector> entity, vector<double> offset/* = vector<double>()*/) const{
    vector< std::array<double,3> > positions;
    const double ** scaled_grid = this -> get_scaled_grid();
    auto points = this -> get_points();

    if( axis == 0 ){ name += ".x"; }
    else if( axis == 1 ){ name += ".y"; }
    else if( axis == 2 ){ name += ".z"; }
    else {
        Verbose::all()  << "Mesh::write_along_axis - Wrong option. Option should be 0, 1, or 2." << std::endl;
        exit(EXIT_FAILURE);
    }

    for(int i = 0; i < points[axis]; ++i){
        std::array<double,3> pos;
        pos[axis] = scaled_grid[axis][i];
        positions.push_back(pos);
        if( offset.size() >= 3 ){
            for(int j = 0; j < 3; ++j){
                positions[i][j] += offset[j];
            }
        }
    }

    this -> write_along_axis(name, positions, entity);
    return;
}

void Basis::write_along_axis(string name, vector< std::array<double,3> > positions, Array< RCP<Epetra_MultiVector> > entity, int index_offset/* = 0*/) const{
    for(int s = 0; s < entity.size(); ++s){
        this -> write_along_axis(name+".s"+String_Util::to_string((long long)s), positions, entity[s], index_offset);
    }
    return;
}

void Basis::write_along_axis(string name, vector< std::array<double,3> > positions, RCP<Epetra_MultiVector> entity, int index_offset/* = 0*/) const{
    int end_index = entity->NumVectors()+index_offset;
    for(int i = 0; i < entity -> NumVectors(); ++i){
        this -> write_along_axis(name+".ind"+String_Util::fill0(i+index_offset,end_index), positions, rcp( new Epetra_Vector(*entity -> operator()(i)) ));
    }
    return;
}

void Basis::write_along_axis(string name, vector< std::array<double,3> > positions, Array< RCP<Epetra_Vector> > entity) const{
    for(int s = 0; s < entity.size(); ++s){
        this -> write_along_axis(name+".s"+String_Util::to_string((long long)s), positions, entity[s]);
    }
    return;
}

void Basis::write_along_axis(string name, vector< std::array<double,3> > positions, RCP<Epetra_Vector> entity) const{
    int MyPID = Parallel_Manager::info().get_total_mpi_rank();

    vector<double> val;
    /*
       for(int i = 0; i < 3; ++i){
       if( this -> get_points()[i] % 2 != 0){
       Verbose::all() << "The number of grid points should be even number!" << std::endl;
       exit(EXIT_FAILURE);
       }
       }
     */

    //val = Tricubic_Interpolation::interpolate(rcp(new Basis(*this)), entity, positions);
    val = Interpolation::Trilinear::interpolate(rcp(new Basis(*this)), entity, positions);

    ofstream output;
    if(MyPID == 0){
        ostringstream os;
        string filename = name + ".txt";
        output.open(filename.c_str());
        output.precision(10);
    }

    for(int i = 0; i < positions.size(); ++i){
        if(MyPID == 0) output.unsetf( std::ios_base::floatfield );
        if(MyPID == 0) output << std::fixed << std::setprecision(4) << std::setw(9) << positions[i][0] << "  "
            << std::fixed << std::setprecision(4) << std::setw(9) << positions[i][1] << "  "
                << std::fixed << std::setprecision(4) << std::setw(9) << positions[i][2] << "  "
                << "\t" << std::setw(15) << std::scientific << val[i] << std::endl;
    }
    if(MyPID == 0) output.close();

    return;
}

void Basis::parallel_write_cube(string name, RCP<const Atoms> atoms, RCP<Epetra_MultiVector> entity, int index_offset) const{
    int NumMyElements = map->NumMyElements();
    int num_vec = entity->NumVectors();
    //int* MyGlobalElements= map->MyGlobalElements();
    //int MyPID = map->Comm().MyPID();
    int MyPID = Parallel_Manager::info().get_total_mpi_rank();
    const double** scaled_grid=get_scaled_grid();
    vector<std::array<double,3> > positions = atoms->get_positions();
    int nx = get_points()[0];
    int ny = get_points()[1];
    int nz = get_points()[2];
    double dx = get_scaling()[0];
    double dy = get_scaling()[1];
    double dz = get_scaling()[2];
    int NumProcs = map->Comm().NumProc();
    int dim = this -> get_original_size();
    ofstream output;
    int i=0;
    while(1){
        double *val;
        int MyPart = -1;

        for(int j=0; j<NumProcs; j++){
            if(i%NumProcs==MyPID){
                val = new double[dim];
                MyPart = i;
            }
            Parallel_Util::group_gather_vector(rcp( new Epetra_Vector(*entity->operator()(i)) ), val, i%NumProcs);
            i++;
            if(i==num_vec)
                break;

        }
        if(MyPart>=0){
            string new_name = name + ".ind" + String_Util::fill0(MyPart+1+index_offset,num_vec+index_offset);
            string filename = new_name + ".cube";
            output.open(filename.c_str());
            output << "<HEADER> ACE-Molecule CUBE FILE" << endl;
            output << "<HEADER> by kwangwoo, Jaewook @ CQMS" << endl;
            output << atoms->get_size() << "\t" << scaled_grid[0][0] << "\t" << scaled_grid[1][0] << "\t" << scaled_grid[2][0] << endl;
            output << nx << "\t" << dx << "\t 0.000000 \t 0.000000" << endl;
            output << ny << "\t 0.000000 \t " << dy << "\t 0.000000" << endl;
            output << nz << "\t 0.000000 \t 0.000000 \t" << dz << endl;
            for(int i=0;i<atoms->get_size();i++){
                output << atoms->get_atomic_numbers()[i] << "\t 0.000000 \t" << positions[i][0] << "\t" << positions[i][1] << "\t" << positions[i][2] << endl;
            }
            output << scientific;
            output.precision(6);

            // KSW REMOVE SUMALL 160107
            for(int ix=0;ix<nx;ix++){
                for(int iy=0;iy<ny;iy++){
                    for(int iz=0;iz<nz;iz++){
                        int i = combine( ix, iy, iz );
                        if(i < 0){
                            output << "0.000000e+00" << "\t";
                        }
                        else{
                            output << val[i] << "\t";
                        }
                        if(iz%6 == 5 ) output << endl;
                    }
                    if((nz%6)!=0) output << endl;
                }
            }
            output.close();
            delete[] val;
        }
        if(i==num_vec)
            break;
    }
    Parallel_Manager::info().all_barrier();
    return;
}
void Basis::read_cube(string name, double* retval, bool is_bohr/* = true*/) const{
    vector<string> axis_name(3);
    axis_name[0] = 'x'; axis_name[1] = 'y'; axis_name[2] = 'z';
    string line;
    auto points = get_points();
    auto scaling = get_scaling();

    double dx = get_scaling()[0];
    double dy = get_scaling()[1];
    double dz = get_scaling()[2];

    ifstream inputfile (name.c_str());
    if(inputfile.fail()){
        Verbose::all() << "Basis::read_cube - CANNOT find " << name << std::endl;
        exit(EXIT_FAILURE);
    }
    int line_index = 0;
    Verbose::single(Verbose::Detail) << "Cube file info" <<std::endl;
    istringstream iss;
    bool is_multicube = false;
    int atom_length = 0;

    for(int i = 0; i < 3; ++i){
        getline(inputfile, line);
    }
    iss.clear();
    iss.str(line);
    iss >> atom_length;
    Verbose::single(Verbose::Normal) << "Read Cube atom_length = " << atom_length << std::endl;

    for(int i = 3; i < 6; ++i){
        getline(inputfile, line);
        //cout << line_index << endl;
        if(i == 3){
            iss.clear();
            iss.str(line);
            string word;
            iss >> word;
            if(String_Util::stoi(word) < 0){
                is_multicube = true;
                throw std::invalid_argument("Gaussian cube with multiple orbitals is not supported!");
            }
        }
        iss.clear();
        iss.str(line);
        string word;
        iss >> word;
        int point = String_Util::stoi(word);
        if(is_bohr and point < 0){
            Verbose::single() << "Basis::Read_cube Warning! Bohr selected but cube file unit is Angstrom!" << std::endl;
        } else if(!is_bohr and point > 0){
            Verbose::single() << "Basis::Read_cube Warning! Angstrom selected but cube file unit is Bohr!" << std::endl;
        }
        if(point < 0){
            point = -point;
        }
        double spacing=0.0;
        for(int k=0; k<3;k++){
            iss>>word;
            double scaling_axis = String_Util::stod(word);
            spacing+=scaling_axis*scaling_axis;
        }
        spacing = sqrt(spacing);
        if(!is_bohr){
            spacing *= 1.889725989; // Unit conversion from Ang to Bohr
        }
        Verbose::single(Verbose::Detail)<< i-3<< " axis: " << point << "\t" << spacing <<std::endl;
        if(points[i-3]!=point or std::abs(scaling[i-3]-spacing) > 1.0E-10){
            Verbose::all() << "Basis::read_cube Warning! axis " << i-3 << " infomation on cube " << name << " is not matched with Mesh information " <<std::endl;
            Verbose::all() << "Points and scaling of cube: " << point << ", " << spacing << " bohr" << std::endl;
            Verbose::all() << "Points and scaling of mesh: " << points[i-3] << ", " << scaling[i-3] << " bohr" << std::endl;
            Verbose::all() << "Scaling difference: " << spacing - scaling[i-3] << " bohr" << std::endl;
            Verbose::all() << "Cube reading line: " << line << std::endl;
            Verbose::all() << "Our Mesh: " << std::endl;
            Verbose::all() << *this << std::endl;
            throw std::invalid_argument("Basis::read_cube Warning! axis " + axis_name[i-3] + " infomation on cube " + name + " is not matched with Mesh information ");
        }
        line_index++;
    }
    for(int i = 0; i < atom_length; ++i){
        getline(inputfile, line);
        line_index++;
    }
    if(is_multicube){
        getline(inputfile, line);
    }
    //todo : exception handling
    string line1;
    int index=0;
    std::vector<int> index_list;
    std::vector<double> values;
    for(int ix=0; ix<points[0]; ix++){
        for(int iy=0; iy<points[1]; iy++){
            for(int iz=0; iz<points[2]; iz++){
                string sub;
                if(iz%6 == 0){
                    getline(inputfile, line1);
                    iss.clear();
                    iss.str(line1);
                }
                iss >> sub;
                int i = grid_setting->combine(ix, iy, iz, points);
                values.push_back(atof(sub.c_str()));
                index_list.push_back(i);
            }
        }
    }
    for(int i=0;i<values.size(); i++){
        if(index_list[i]>=0){
            retval[index_list[i]] = values[i];
        }
    }
    inputfile.close();
    return;
}

void Basis::get_stencil(int* max_stencil_x,
            int* max_stencil_y,
            int* max_stencil_z) const {

    auto order = basis->get_order();
    if (order<0 ){
        auto points = get_points();
        *max_stencil_x=points[0];
        *max_stencil_y=points[1];
        *max_stencil_z=points[2];
        return;
    }
    *max_stencil_x=order;
    *max_stencil_y=order;
    *max_stencil_z=order;

    return;
}

void Basis::write_polarizability(
        std::string name,
        Teuchos::RCP<const Atoms> atoms,
        std::vector<double> wavelength,
        std::vector<std::complex<float> > polarizability
        ) const {
    if(wavelength.size() != polarizability.size()){
        std::cout << "ERROR::Write Polarizability " << wavelength.size() << " " << polarizability.size() << std::endl;
    }
    ofstream output;

    if(Parallel_Manager::info().get_total_mpi_rank()==0){
        string filename = name + ".pol";
        output.open(filename.c_str());
        output << "wavelength(nm)\treal\timag\tabsorbance" << std::endl;
        for(int w=0; w<wavelength.size(); w++){
            output << wavelength[w]
                << " " << polarizability[w].real()
                << " " << polarizability[w].imag()
                << " " << 1/wavelength[w]*polarizability[w].imag()*10000.0 << std::endl;
        }
        output.close();
    }
    Parallel_Manager::info().all_barrier();

    return;
}

void Basis::write_dipole(
        std::string name,
        Teuchos::RCP<const Atoms> atoms,
        std::vector<double> wavelength,
        std::vector<std::vector< std::vector<std::complex<float> > > > dipole
        ) const {
    if(wavelength.size() != dipole.size()){
        std::cout << "ERROR::Write Polarizability " << wavelength.size() << " " << dipole.size() << std::endl;
        exit(-1);
    }

    auto positions = atoms->get_positions();
    if(Parallel_Manager::info().get_total_mpi_rank()==0){
        for(int w=0; w<wavelength.size(); w++){
            ofstream output;
            string filename = name + String_Util::to_string(wavelength[w])+".dipole";
            output.open(filename.c_str());
            output << "x\ty\tz\treal_x\treal_y\treal_z\timag_x\timag_y\timag_z" << std::endl;
            for(int iatom=0; iatom<dipole[0].size(); iatom++){
            output << positions[iatom][0] *0.529177249
                << " " << positions[iatom][1] *0.529177249
                << " " << positions[iatom][2] *0.529177249
                << " " << dipole[w][iatom][0].real()
                << " " << dipole[w][iatom][1].real()
                << " " << dipole[w][iatom][2].real()
                << " " << dipole[w][iatom][0].imag()
                << " " << dipole[w][iatom][1].imag()
                << " " << dipole[w][iatom][2].imag()
                << std::endl;
            }
            output.close();
        }

    }
    Parallel_Manager::info().all_barrier();

    return;
}

const std::array<double,3> Basis::get_grid_length() const{
    std::array<double,3> retval;
    for(int d = 0; d < 3; ++d){
        retval[d] = (this -> get_points()[d]-1) * this -> get_scaling()[d];

        // periodicity 0: does nothing. 1: +1 to x. 2: +1 to x,y. 3: +1 to all.
        if(d < this -> get_periodicity()){
            retval[d] += this -> get_scaling()[d];
        }
    }
    return retval;
}

// (PBC) 주석: get 함수 추가. 다음과 같이 사용. mesh->get_periodicity()
int Basis::get_periodicity() const{
    return basis->get_periodicity();
    // return kinetic_basis->get_periodicity;
}

// (PBC) n_cutoff test
int Basis::get_Hartree_cutoff() const{
    return n_cutoff;
}

// (PBC) kinetic_cutoff test for sparsity
double Basis::get_kinetic_cutoff() const{
    return kinetic_cutoff;
}

// (PBC)
void Basis::find_nearest_displacement(
    int index, double x_src, double y_src, double z_src,
    double &xi, double &yi, double &zi, double &r
)const {
    double x_p, y_p, z_p;
    this->get_position(index, x_p, y_p, z_p);

    double ri[3];
    ri[0] = x_p - x_src;
    ri[1] = y_p - y_src;
    ri[2] = z_p - z_src;
    double ri_orig[3];
    for(int d = 0; d < 3; ++d){ ri_orig[d] = ri[d]; }

    std::array<double,3> cell_size = this -> get_grid_length();
    
    for(int d = 0; d < 3; ++d){
        if(d < this -> get_periodicity()){
            // Axis d is periodic!
            for(int i = -1; i <= 1; ++i){
                if(std::abs(ri[d]) > std::abs(ri_orig[d] + i*cell_size[d])){
                    ri[d] = ri_orig[d] + i*cell_size[d];
                }
            }
        }
    }
    xi = ri[0]; yi = ri[1]; zi = ri[2];
    r = sqrt(ri[0]*ri[0]+ri[1]*ri[1]+ri[2]*ri[2]);
}

Teuchos::RCP<Grid_Setting> Basis::get_grid_setting() const{
    return this -> grid_setting;
}

Teuchos::RCP<Basis_Function> Basis::get_basis_function() const{
    return this -> basis;
}
