#pragma once
#include <vector>
#include <string>

#include "Teuchos_RCP.hpp"
#include "Epetra_Vector.h"
#include "../Basis/Basis.hpp"

/**
 * @brief Supersampling method implementation.
 * @details See J. Chem. Phys., 2016, 144(9), 094141.
 */
namespace Double_Grid {
    /**
     * @brief Returns supersampling coefficients.
     * @param FilterType Type of supersampling filter. Sinc or Lagrange.
     * @param FineDimension Supersampling rate.
     * @return Supersampling coefficients.
     * @note Lagrange type uses Lagrange function of order 8.
     */
    std::vector<double> get_sampling_coefficients( std::string FilterType, int FineDimension );

    /**
     * @brief Perform supersampling procedure. Note that fine_vals outside of cutoff will be ignored.
     * @param fine_vals Value of target entity on fine grid.
     * @param fine_inds Nonzero indices of target entity on fine grid.
     * @param fine_basis Basis about fine grid.
     * @param out_basis Output basis.
     * @param filter_type Supersampling filter type.
     * @param FineDimension Supersampling rate.
     * @param beta Supersampling cutoff extension.
     * @param center Center of cutoff sphere.
     * @param cutoff Cutoff radius.
     * @return Supersampled entity.
     */
    Teuchos::RCP<Epetra_Vector> sample(
            std::vector<double> fine_vals,
            std::vector<int> fine_inds,
            Teuchos::RCP<const Basis> fine_basis,
            Teuchos::RCP<const Basis> out_basis,
            std::string filter_type,
            int FineDimension, double beta,
            std::vector<double> center, double cutoff
    );

    /**
     * @brief Perform supersampling procedure. Note that fine_vals outside of cutoff will be ignored.
     * @param fine_vevtor Vector of target entity on fine grid.
     * @param fine_basis Basis about fine grid.
     * @param out_basis Output basis.
     * @param filter_type Supersampling filter type.
     * @param FineDimension Supersampling rate.
     * @param beta Supersampling cutoff extension.
     * @param center Center of cutoff sphere.
     * @param cutoff Cutoff radius.
     * @return Supersampled entity.
     */
    Teuchos::RCP<Epetra_MultiVector> sample(
            Teuchos::RCP<Epetra_MultiVector> fine_vector,
            Teuchos::RCP<const Basis> fine_basis,
            Teuchos::RCP<const Basis> out_basis,
            std::string filter_type,
            int FineDimension, double beta,
            std::vector<double> center, double cutoff
    );

    /**
     * @brief Perform supersampling procedure. Note that fine_vals outside of cutoff will be ignored.
     * @param fine_vevtor Vector of target entity on fine grid.
     * @param fine_basis Basis about fine grid.
     * @param out_basis Output basis.
     * @param filter_type Supersampling filter type.
     * @param FineDimension Supersampling rate.
     * @param beta Supersampling cutoff extension.
     * @param center Center of cutoff sphere.
     * @param cutoff Cutoff radius.
     * @return Supersampled entity.
     */
    Teuchos::RCP<Epetra_Vector> sample(
            Teuchos::RCP<Epetra_Vector> fine_vector,
            Teuchos::RCP<const Basis> fine_basis,
            Teuchos::RCP<const Basis> out_basis,
            std::string filter_type,
            int FineDimension, double beta,
            std::vector<double> center, double cutoff
    );
};

