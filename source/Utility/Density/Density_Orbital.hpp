#pragma once 
#include "Teuchos_Array.hpp"
#include "Teuchos_RCP.hpp"
#include "Teuchos_Array.hpp"

#include "Epetra_Vector.h"
#include "Epetra_MultiVector.h"

#include "../../Core/Occupation/Occupation.hpp"
#include "../../Basis/Basis.hpp"

namespace Density_Orbital{
    Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > compute_density_orbitals(
        Teuchos::RCP<const Basis> basis, Teuchos::Array< Teuchos::RCP<Epetra_Vector> > density,
        Teuchos::Array< Teuchos::RCP<Occupation> > occupations
    );
    //static Teuchos::Array<Teuchos::RCP<Epetra_MultiVector> > compute_density_orbitals(Basis* basis,Grid_Setting* grid_setting,RCP<Epetra_MultiVector> density,Array<RCP<Occupation> > occupations);

};
