#pragma once
#include "Teuchos_Array.hpp"
#include "Teuchos_RCP.hpp"

#include "Epetra_MultiVector.h"
#include "Epetra_Vector.h"
#include "../../Basis/Basis.hpp"
#include "../../Core/Occupation/Occupation.hpp"

namespace Density_From_Orbitals{
    //single orbital -> orbital density
    /**
     * @brief Compute density from orbital by multiplying the orbital.
     * @param basis Basis.
     * @param orbital Orbitals.
     * @param is_value False if the orbital is the coefficient of the basis.
     * @param orbital_density Return value.
     **/
    int compute_orbital_density(
        Teuchos::RCP<const Basis> basis, Teuchos::RCP<Epetra_Vector> orbital,
        bool is_value, Teuchos::RCP<Epetra_Vector>& orbital_density
    );
    //orbitals -> overlap density
    /**
     * @brief Compute overlap density from two orbitals by multiplying the orbitals.
     * @param basis Basis.
     * @param orbital1 Orbital 1.
     * @param is_value1 False if the orbital 1 is the coefficient of the basis.
     * @param orbital2 Orbital 2.
     * @param is_value2 False if the orbital 2 is the coefficient of the basis.
     * @param overlap_density Return value.
     **/
    int compute_overlap_density(
        Teuchos::RCP<const Basis> basis, Teuchos::RCP<Epetra_Vector> orbital1, bool is_value1,
        Teuchos::RCP<Epetra_Vector> orbital2, bool is_value2, Teuchos::RCP<Epetra_Vector>& overlap_density
    );
    //orbitals -> total density
    /**
     * @brief Compute total density from the orbitals and the occupations.
     * @param basis Basis.
     * @param orbitals Orbitals. Spin index and then orbital index.
     * @param occupations Occupations.
     * @param density Output density.
     **/
    int compute_total_density(
        Teuchos::RCP<const Basis> basis, Teuchos::Array< Teuchos::RCP<Occupation> > occupations,
        Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > orbitals, Teuchos::Array< Teuchos::RCP<Epetra_Vector> >& density
    );
    /**
     * @brief Compute total density from the orbitals and the occupations.
     * @param basis Basis.
     * @param orbitals Orbitals for one spin.
     * @param occupations Occupations.
     * @param density Output density.
     **/
    int compute_total_density(
        Teuchos::RCP<const Basis> basis, Teuchos::Array< Teuchos::RCP<Occupation> > occupations,
        Teuchos::RCP<Epetra_MultiVector> orbital, Teuchos::Array< Teuchos::RCP<Epetra_Vector> >& density
    );

    /**
     * @brief Compute total density from the orbitals and the occupations for one spin.
     * @param basis Basis.
     * @param orbitals Orbitals for one spin.
     * @param occupations Occupations.
     * @param density Output density.
     **/
    int compute_total_density(
        Teuchos::RCP<const Basis> basis, Teuchos::RCP<Occupation> occupations,
        Teuchos::RCP<Epetra_MultiVector> orbital, Teuchos::RCP<Epetra_Vector>& density
    );
}
