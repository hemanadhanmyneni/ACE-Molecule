#include "Density_Orbital.hpp"
#include "../ACE_Config.hpp"
#include <cmath>

using Teuchos::Array;
using Teuchos::RCP;
using Teuchos::rcp;

Array<RCP<Epetra_MultiVector> > Density_Orbital::compute_density_orbitals(RCP<const Basis> basis, Array<RCP<Epetra_Vector> > density,Array<RCP<Occupation> > occupations){
    RCP<const Epetra_Map> map = basis->get_map();
    const int NumMyElements=map->NumMyElements();
    auto scaling = basis->get_scaling();
    Array<RCP<Epetra_MultiVector> > density_orbitals;


    //////////////////// Find maximum occupation size ////////////////////
    int occupation_size = -1;
    for(int alpha=0; alpha<occupations.size(); alpha++){
        if( occupation_size < occupations[alpha] -> get_size() ){
            occupation_size = occupations[alpha]->get_size();
        }
    }
    //////////////////////////////////////////////////////////////////////
    /////////////////////   generate empty orbital  //////////////////////////////////
    for(int alpha=0; alpha<occupations.size(); alpha++){
        density_orbitals.append( rcp(new Epetra_MultiVector(density[0]->Map(),occupation_size) )  );
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////

    for(int alpha=0; alpha<occupations.size(); alpha++){
        //int occupation_size = occupations[alpha]->get_size();
        double total_occ = occupations[alpha]->get_total_occupation();

        double sum = 0.0;
        if(std::abs(total_occ) > 1.0E-30){
            for(int k=0; k<occupation_size; k++){
                int current_occupation = occupations[alpha] -> operator[](k);
                if( occupations.size() != 1 and occupations[0]->operator[](k)+occupations[1]->operator[](k) > 0 ) current_occupation = 1;

                const double factor = current_occupation*scaling[0]*scaling[1]*scaling[2] *occupations.size()/(2*total_occ);
                //double factor = current_occupation*occupations.size()/(2*total_occ);
                #ifdef ACE_HAVE_OMP
                #pragma omp parallel for reduction(+:sum)
                #endif
                for(int i=0; i<NumMyElements; i++){
                    density_orbitals[alpha]->operator[](k)[i] = sqrt(factor * density[alpha]->operator[](i) );
                    sum+=factor * density[alpha]->operator[](i);
                }
/*
                if(occupations.size() == 1){
                    for(int i=0; i<NumMyElements; i++){
                        //density_orbitals[alpha]->operator[](k)[i] = sqrt(0.5 * density[alpha]->operator[](i) * occupations[alpha]->operator[](k) / total_occ);
                        density_orbitals[alpha]->operator[](k)[i] = sqrt(0.5 * density[alpha]->operator[](i) * current_occupation / total_occ);
                    }
                }
                else{
                    for(int i=0; i<NumMyElements; i++){
                        //density_orbitals[alpha]->operator[](k)[i] = sqrt(density[alpha]->operator[](i) * occupations[alpha]->operator[](k) / total_occ);
                        density_orbitals[alpha]->operator[](k)[i] = sqrt(density[alpha]->operator[](i) * current_occupation / total_occ);
                    }
                }
*/
            }
            //density_orbitals[alpha]->Scale(sqrt(scaling[0]*scaling[1]*scaling[2] ));
        }
        Verbose::single(Verbose::Detail) << "Atomic Density::sum-  " << sum << std::endl;
    }
    return density_orbitals;
}
