#include "Density_Orbital_Derivatives.hpp"
#include <vector>
#include "../Verbose.hpp"
#include "../Calculus/Lagrange_Derivatives.hpp"
#include "../Value_Coef.hpp"

using std::vector;
using Teuchos::rcp;
using Teuchos::RCP;
using Teuchos::Array;
using Teuchos::null;

int Density_Orbital_Derivatives::density_gradient_from_orbital(
    RCP<const Basis> basis,
    RCP<const Occupation> occupation,
    RCP<const Epetra_MultiVector> orbital,
    bool is_value,
    RCP<Epetra_MultiVector>& gradient
){
    if (gradient==null){
        gradient = rcp(new Epetra_MultiVector(*basis->get_map(), 3));
    }

    vector<int> orb_index;
    for(int i = 0; i < occupation -> get_size(); ++i){
        if(occupation -> operator[](i) > 1.0E-15){
            orb_index.push_back(i);
        }
    }
    if(orb_index.size() > 0){
        Array< RCP<Epetra_MultiVector> > orbital_grad;
        RCP<Epetra_MultiVector> orbital_val;
        Value_Coef::Value_Coef(basis, orbital, false, true, orbital_val);

        for(int d = 0; d < 3; ++d){
            orbital_grad.append( rcp(new Epetra_MultiVector(*basis -> get_map(), occupation -> get_size())) );
        }
        Lagrange_Derivatives::gradient(basis, orbital, orbital_grad, is_value);
        for(int i = 0; i < orb_index.size(); ++i){
            int i2 = orb_index[i];
            for(int d = 0; d < 3; ++d){
                gradient -> operator()(d) -> Multiply(occupation -> operator[](i2)*2, *orbital_grad[d] -> operator()(i2), *orbital_val -> operator()(i2), 1.0);
            }
        }
    }
	return 0;
}

int Density_Orbital_Derivatives::kinetic_energy_density(
    RCP<const Basis> basis,
    RCP<const Occupation> occupation,
    RCP<const Epetra_MultiVector> orbital,
    bool is_value,
    RCP<Epetra_Vector>& kinetic_energy_density
){
    if (kinetic_energy_density == null){
        kinetic_energy_density = rcp(new Epetra_Vector(*basis->get_map()));
    }

    Array< RCP<Epetra_MultiVector> > orbital_grad;
    for(int d = 0; d < 3; ++d){
        orbital_grad.append( rcp(new Epetra_MultiVector(*basis -> get_map(), occupation -> get_size())) );
    }
    Lagrange_Derivatives::gradient(basis, orbital, orbital_grad, is_value);
    for(int i = 0; i < occupation -> get_size(); ++i){
        for(int d = 0; d < 3; ++d){
            kinetic_energy_density -> Multiply(occupation -> operator[](i)*0.5, *orbital_grad[d] -> operator()(i), *orbital_grad[d] -> operator()(i), 1.0);
        }
    }
	return 0;
}
