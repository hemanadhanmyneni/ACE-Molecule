#include "Density_From_Orbitals.hpp"
#include "../Verbose.hpp"
#include "../Value_Coef.hpp"

using Teuchos::rcp;
using Teuchos::RCP;
using Teuchos::Array;
using Teuchos::null;

//single orbital -> orbital density
int Density_From_Orbitals::compute_orbital_density(RCP<const Basis> basis, RCP<Epetra_Vector> orbital, bool is_value, RCP<Epetra_Vector>& orbital_density){
    if (orbital_density==null){
        orbital_density = rcp(new Epetra_Vector(*basis->get_map() ));
    }

    orbital_density->PutScalar(0.0);
//    Teuchos::RCP<Epetra_Vector> orbital_density = Teuchos::rcp(new Epetra_Vector(orbital->Map())); 
    if(is_value==true){
        orbital_density->Multiply(1.0, *orbital, *orbital, 0.0);
        return 1;
    }
    RCP<Epetra_Vector> orbital_value; 
    Value_Coef::Value_Coef(basis,orbital,false,true,orbital_value);
    orbital_density->Multiply(1.0,*orbital_value,*orbital_value, 0.0);
    
    return 0;
}

//two orbital -> overlap density
int Density_From_Orbitals::compute_overlap_density(RCP<const Basis> basis, RCP<Epetra_Vector> orbital1, bool is_value1, RCP<Epetra_Vector> orbital2, bool is_value2, RCP<Epetra_Vector>& overlap_density){
//    Teuchos::RCP<Epetra_Vector> overlap_density = Teuchos::rcp(new Epetra_Vector(orbital->Map())); 
    if (overlap_density==null){
        overlap_density = rcp(new Epetra_Vector(*basis->get_map() ));
    }

    overlap_density->PutScalar(0.0);

    if(is_value1 && is_value2){
        overlap_density->Multiply(1.0, *orbital1, *orbital2, 0.0);
        return 1;
    }
    else if(is_value1==true && is_value2==false){        
        RCP<Epetra_Vector> orbital2_value ; 
        Value_Coef::Value_Coef(basis,orbital2,false,true,orbital2_value);
        overlap_density->Multiply(1.0, *orbital1, *orbital2_value, 0.0);
        return 2;
    }
    else if(is_value1==false && is_value2==true){
        RCP<Epetra_Vector> orbital1_value ;
        Value_Coef::Value_Coef(basis,orbital1,false,true,orbital1_value);
        overlap_density->Multiply(1.0, *orbital1_value, *orbital2, 0.0);
        return 3;
    }
    else{

        RCP<Epetra_Vector> orbital1_value; 
        RCP<Epetra_Vector> orbital2_value;
        Value_Coef::Value_Coef(basis,orbital1,false,true,orbital1_value);
        Value_Coef::Value_Coef(basis,orbital2,false,true,orbital2_value);
        overlap_density->Multiply(1.0, *orbital1_value, *orbital2_value, 0.0);
        return 4;
    }
    return 0;
}

//orbitals -> total density
int Density_From_Orbitals::compute_total_density(RCP<const Basis> basis, Array<RCP<Occupation> > occupations, Array<RCP<Epetra_MultiVector> > orbitals, Array<RCP<Epetra_Vector> >& density){
    if (density.size()==0){
        for(int i_spin =0; i_spin < occupations.size(); i_spin++){
            density.append(rcp(new Epetra_Vector(*basis->get_map()  )) );
        }
    }
//    Teuchos::RCP<Epetra_MultiVector> density = Teuchos::rcp(new Epetra_MultiVector(orbital[0]->Map(), occupations.size() )); 
    //const double* scaling = basis->get_scaling();    
    //int NumMyElements = basis->get_map()->NumMyElements();
    for(int i_spin=0; i_spin<occupations.size(); i_spin++){
        density[i_spin]->PutScalar(0.0);
        RCP<Epetra_MultiVector> orbital_value;
        Value_Coef::Value_Coef(basis,orbitals[i_spin],false,true,orbital_value);
        for(int i=0; i<occupations[i_spin]->get_size(); i++){
            if (occupations[i_spin]->operator[](i)<1E-30) continue;
            if( orbital_value -> NumVectors() <= i ){
                Verbose::all() << "Density_From_Orbitals::compute_total_density: Input orbitals do not match with occupations." << std::endl;
            }
            /*
            for(int j=0; j<NumMyElements; j++){
                density[i_spin]->SumIntoMyValue(j, 0, occupations[i_spin]->operator[](i) * orbital_value->operator[](i)[j] * orbital_value->operator[](i)[j]);
            }
            */
            density[i_spin] -> Multiply(occupations[i_spin] -> operator[](i), *orbital_value -> operator()(i), *orbital_value -> operator()(i), 1.0);
        }
    }

    return 0;
}

int Density_From_Orbitals::compute_total_density(RCP<const Basis> basis, RCP<Occupation> occupations, RCP<Epetra_MultiVector> orbital, RCP<Epetra_Vector>& density){
    Array< RCP<Occupation> > array_occupation(1, occupations);
    Array< RCP<Epetra_MultiVector> > array_orbital(1, orbital);
    Array< RCP<Epetra_Vector> > array_rho(1, rcp(new Epetra_Vector(orbital -> Map())));
    Density_From_Orbitals::compute_total_density(basis, array_occupation, array_orbital, array_rho);
    density = array_rho[0];
    return 0;
}
