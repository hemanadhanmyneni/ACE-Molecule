#pragma once
#include "Teuchos_Array.hpp"
#include "Teuchos_RCP.hpp"

#include "Epetra_MultiVector.h"
#include "Epetra_Vector.h"
#include "../../Basis/Basis.hpp"
#include "../../Core/Occupation/Occupation.hpp"

/**
 * @brief Computes density and orbital derivatives useful for XC.
 * @author Sungwoo Kang
 * @date 2018.9.18.
 **/
namespace Density_Orbital_Derivatives{
    /**
     * @brief Compute density gradient from orbitals.
     * @param basis Basis.
     * @param occupations Occupations.
     * @param orbital Orbitals.
     * @param is_value False if the orbital is the coefficient of the basis.
     * @param gradient Return value.
     **/
    int density_gradient_from_orbital(
        Teuchos::RCP<const Basis> basis,
        Teuchos::RCP<const Occupation> occupation,
        Teuchos::RCP<const Epetra_MultiVector> orbital,
        bool is_value, Teuchos::RCP<Epetra_MultiVector>& gradient
    );
    /**
     * @brief Compute kinetic energy density from orbitals.
     * @details \sum_i f_i | \grad \phi_i |^2
     * @param basis Basis.
     * @param occupations Occupations.
     * @param orbital Orbitals.
     * @param is_value False if the orbital is the coefficient of the basis.
     * @param kinetic_energy_density Return value.
     **/
    int kinetic_energy_density(
        Teuchos::RCP<const Basis> basis,
        Teuchos::RCP<const Occupation> occupation,
        Teuchos::RCP<const Epetra_MultiVector> orbital,
        bool is_value, Teuchos::RCP<Epetra_Vector>& kinetic_energy_density
    );
}
