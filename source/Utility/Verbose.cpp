#include "Verbose.hpp"
#include <stdexcept>
#include "NullOStream.hpp"
#include "Parallel_Manager.hpp"

using std::cout;
using std::ostream;
using std::streambuf;

Verbose* Verbose::singleton_instance=NULL;

void Verbose::construct( std::ostream* os , int verbose_level){
    if(singleton_instance==NULL){
        if(os==NULL)
            singleton_instance = new Verbose( &std::cout, verbose_level);
        else
            singleton_instance = new Verbose(os, verbose_level);
    }
    Verbose::set_numformat(Verbose::Default);
    return;
}
void Verbose::free(){
    delete singleton_instance;
    singleton_instance=NULL;
}
/*
Verbose::Verbose(std::ostream* os ): out(os){
    return;
}
*/
std::ostream& Verbose::single(int verbose){
    if(Parallel_Manager::info().get_total_mpi_rank()==0 and verbose<=singleton_instance->verbose_level){
       return *singleton_instance->out;
    }
    return cnul;
}
std::ostream& Verbose::all(int verbose){
    if (verbose<=singleton_instance->verbose_level)    return  *singleton_instance->out;
    return cnul;
}
std::ostream& Verbose::master_group(int verbose){
    if(Parallel_Manager::info().get_group_rank()==0 and verbose<=singleton_instance->verbose_level) return *singleton_instance->out;
    return cnul;
}
std::ostream& Verbose::master_rank(int verbose){
    if(Parallel_Manager::info().get_mpi_rank()==0 and verbose<=singleton_instance->verbose_level) return *singleton_instance->out;
    return cnul;
}

void Verbose::set_numformat(int numformat){
    if(singleton_instance -> out == NULL){
        return;
    }
    singleton_instance -> out -> unsetf(std::ios_base::floatfield);
    *singleton_instance -> out << std::left;
    //*singleton_instance -> out << std::defaultfloat;//std++11
    if(numformat == Verbose::Energy){
        //singleton_instance -> out -> setf(std::ios_base::fixed, std::ios_base::floatfield);
        //singleton_instance -> out -> precision(10);
        //*singleton_instance -> out << std::right;
        *singleton_instance -> out << std::setw(13) << std::fixed << std::setprecision(6) << std::right;
    } else if(numformat == Verbose::Occupation){
        //singleton_instance -> out -> setf(std::ios_base::fixed, std::ios_base::floatfield);
        //singleton_instance -> out -> precision(3);
        *singleton_instance -> out << std::fixed << std::setprecision(4) << std::right;
    } else if(numformat == Verbose::Time){
        //singleton_instance -> out -> setf(std::ios_base::fixed, std::ios_base::floatfield);
        //singleton_instance -> out -> precision(3);
        *singleton_instance -> out << std::fixed << std::setprecision(3) << std::left;
    } else if(numformat == Verbose::Pos){
        //singleton_instance -> out -> setf(std::ios_base::fixed, std::ios_base::floatfield);
        //singleton_instance -> out -> precision(6);
        *singleton_instance -> out << std::fixed << std::setprecision(6) << std::right;
    } else if(numformat == Verbose::Scientific){
        singleton_instance -> out -> setf(std::ios_base::scientific, std::ios_base::floatfield);
        //*singleton_instance -> out << std::scientific;
    }
}

int Verbose::get_verbose_level(){
    return singleton_instance -> verbose_level;
}

int TeeStreamBuf::overflow(int c){
    if(c==EOF){
        return !EOF;
    } else {
        int e1 = sb1 -> sputc(c);
        int e2 = sb2 -> sputc(c);
        return e1 == EOF || e2 == EOF ? EOF : c;
    }
}

int TeeStreamBuf::sync(){
    int e1 = sb1 -> pubsync();
    int e2 = sb2 -> pubsync();
    return e1 == 0 && e2 == 0? 0: -1;
}

TeeStream::TeeStream(ostream &o1, ostream &o2): ostream(&tbuf), tbuf(o1.rdbuf(), o2.rdbuf()){
}
