#include "String_Util.hpp"
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <cstdio>//sprintf
#include <cstdlib>
#include <cstring>
#include "Verbose.hpp"

using std::vector;
using std::string;

//int String_Util::isStringDouble(const char *s){
bool String_Util::isStringDouble(const char *s){
    size_t size = strlen(s);
    if(size == 0) return false;

    for(int i=0; i<(int)size; i++){
        if(s[i] == '.' or s[i] == '-' or s[i] == '+') continue;
        if(s[i] < '0' or s[i] > '9') return false;
    }

    return true;
}

bool String_Util::isStringInteger(const char *s){
    size_t size = strlen(s);
    if(size == 0) return false;

    for(int i=0; i<(int)size; i++){
        if(s[i] < '0' or s[i] > '9') return false;
    }

    return true;
}

// strOrigin을 strTok를 기준으로 잘라서 strResult에 넣어준다.
//string* String_Util::strSplit(string strOrigin,string strTok){
vector<string> String_Util::strSplit(string strOrigin,string strTok, bool preserve_none/* = false*/){
    int cutAt;  // 자르는 위치
    //int index = 0;  // 문자열 인덱스
    //int MAX_TOK=3;
    //string* strResult = new string[MAX_TOK]; // 결과 return 변수
    vector<string> strResult;; // 결과 return 변수

    // strTok을 찾을 때까지 반복
    while( (cutAt = strOrigin.find_first_of(strTok)) != strOrigin.npos){
        if(preserve_none){
            if(cutAt >= 0){  // 자르는 위치가 0보다 크거나 같으면
                // 결과 배열에 추가
                //strResult[index++] = strOrigin.substr(0, cutAt);
                strResult.push_back( strOrigin.substr(0, cutAt) );
            }
        } else {
            if(cutAt > 0){  // 자르는 위치가 0보다 크면
                // 결과 배열에 추가
                //strResult[index++] = strOrigin.substr(0, cutAt);
                strResult.push_back( strOrigin.substr(0, cutAt) );
            }
        }
        // 원본은 자른 부분을 제외한 나머지
        strOrigin = strOrigin.substr(cutAt + 1);
    }
    if (strOrigin.length() > 0){ // 원본이 아직 남았으면
        // 나머지를 결과 배열에 추가
        //strResult[index++] = strOrigin.substr(0, cutAt);
        strResult.push_back( strOrigin.substr(0, cutAt) );
    }
    return strResult;
}

vector<string> String_Util::strSplit(string strOrigin, vector<string> strToks, bool preserve_none/* = false*/){
    vector<string> retval(1, strOrigin);
    for(int i = 0; i < strToks.size(); ++i){
        string strTok = strToks[i];
        vector<string> myresult;
        for(int j = 0; j < retval.size(); ++j){
            vector<string> res2 = String_Util::strSplit(retval[j], strTok, preserve_none);
            myresult.insert(myresult.end(), res2.begin(), res2.end());
        }
        retval = myresult;
    }
    return retval;
}

vector<string> String_Util::Split_ws(string strOrigin, bool preserve_none/* = false*/){
    string whitespaces[6] = {" ", "\f", "\n", "\r", "\t", "\v"};
    return String_Util::strSplit(strOrigin, vector<string>(whitespaces, whitespaces+6), preserve_none);
}

// 주어진 string에 대해 숫자만 뽑아낸다.
int String_Util::string_to_int(string word){
    int value=0;
    string tmp;
    for(int i=0;i<word.size();i++){
        if(word[i]>=48 && word[i]<=57){ // ascii 코드로 48은 0, 57은 9에 해당
            tmp.push_back(word[i]);
        }
    }
    value=atoi(tmp.c_str());
    return value;
}

// Factorial 구하는 코드
int String_Util::factorial(int x, int result){
    if(x < 0){
        throw std::invalid_argument("x should be zero or positive integer!");
    }
    else if(x == 0) return 1;
    else if(x == 1) return result;
    else return factorial(x-1, x*result);
}

long long int String_Util::factorial(long long int x, long long int result){
    if(x < 0){
        throw std::invalid_argument("x should be zero or positive integer!");
    }
    else if(x == 0) return 1;
    else if(x == 1) return result;
    else return factorial(x-1, x*result);
}

int String_Util::double_factorial(int x){
    int rv = 1;
    for(int i = x; i > 0; i-=2){
        rv *= i;
    }
    return rv;
}

long long int String_Util::double_factorial(long long int x){
    long long int rv = 1;
    for(long long int i = x; i > 0; i-=2){
        rv *= i;
    }
    return rv;
}

std::string String_Util::trim(std::string s){
    std::string drop = "\t\n\v\r\f ";
    std::string r=s.erase(s.find_last_not_of(drop)+1);
    return r.erase(0,r.find_first_not_of(drop));
}
std::string String_Util::rtrim(std::string s){
    std::string drop = "\t\n\v\r\f ";
    return s.erase(s.find_last_not_of(drop)+1);
}
std::string String_Util::ltrim(std::string s){
    std::string drop = "\t\n\v\r\f ";
    return s.erase(0,s.find_first_not_of(drop));
}

//N의 자릿수만큼 i 앞에 0을 채워서 string으로 만들어 출력해준다.
std::string String_Util::fill0(int i, int N){
    int output_length = String_Util::to_string((long long)N).length();
    int num_length = String_Util::to_string((long long)i).length();
    std::string retval = "";
    for(int j=0;j<output_length-num_length;j++) retval += "0";
    return retval + String_Util::to_string((long long)i);
}

int String_Util::stoi( std::string s){
    return atoi(s.c_str() );
}
double  String_Util::stod( std::string s){
    return atof(s.c_str() );
}
std::string String_Util::to_string(long long value){
    char str[1024] ;
    sprintf(str,"%lld",value);
    return std::string(str);
}
std::string String_Util::to_string(int value){
    char str[1024] ;
    sprintf(str,"%d",value);
    return std::string(str);
}
std::string String_Util::to_string(double value){
    std::ostringstream sstream;
    sstream << value;
    return sstream.str();
}
