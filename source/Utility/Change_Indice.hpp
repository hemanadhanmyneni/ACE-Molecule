#pragma once
//#include <string>
#include "Teuchos_RCP.hpp"

#include "../Basis/Basis.hpp"

class Change_Indice{
    public:
        Change_Indice(Teuchos::RCP<const Basis> basis1, Teuchos::RCP<const Basis> basis2); // it makes vetor containing index from grid_setting1 to grid_setting2
        int change_indice(int i);
    private:
        std::vector<int> index_list;
};
