#pragma once
#include <vector>

class Matrix_Multiplication{
    public:
        static std::vector< std::vector< double> > multiply(std::vector< std::vector<double> > A, std::vector< std::vector< double> > B);
        static std::vector<double> multiply(std::vector< std::vector<double> > A, std::vector<double> v);
        static std::vector<double> multiply(std::vector<double> v,std::vector< std::vector<double> > A);
        static double dot(std::vector<double> v1, std::vector<double> v2);
        static std::vector< std::vector<double> > outer(std::vector<double> v1, std::vector<double> v2);

    protected:
};

