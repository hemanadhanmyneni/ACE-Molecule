#include "Parallel_Manager.hpp"
#include <unistd.h>
#include <cstring>
#include "String_Util.hpp"
#include "Verbose.hpp"
#ifdef ACE_HAVE_MPI
    #include "Epetra_MpiComm.h"
MPI_Op  mpi_operation[4]={MPI_MAX,MPI_MIN,MPI_SUM,MPI_PROD};
#else
    #include "Epetra_SerialComm.h"
#endif

#ifdef USE_CUDA
#include <cuda.h>
#include <cuda_runtime_api.h>
#endif


Parallel_Manager* Parallel_Manager::instance =NULL;

Parallel_Manager& Parallel_Manager::info(){
    return *instance;
}

void Parallel_Manager::construct(int argc, char* argv[], int group_size){
    if(instance==NULL){
        instance = new Parallel_Manager(argc,argv, group_size);
    }
    else if(group_size!=instance->group_size){
        finalize();
        instance = new Parallel_Manager(argc,argv, group_size);
    }
    return;
}
void Parallel_Manager::change_group_size(int group_size){
#ifdef ACE_HAVE_MPI
    int initialized;

    MPI_Initialized(&initialized);
    if (!initialized){
        std::cout <<"construt function should be called before construct" <<std::endl;
        exit(-1);
    }

    MPI_Comm_rank(MPI_COMM_WORLD, &total_mpi_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &total_mpi_size);
    if(total_mpi_size%group_size!=0){
        if(total_mpi_rank==0) {
            std::cout << "mpi size is not a multiple of group_size" <<std::endl;
        }
        exit(-1);
    }

    this->mpi_size = total_mpi_size / group_size;
    this->mpi_rank = total_mpi_rank % mpi_size;
    this->group_rank = total_mpi_rank / mpi_size;
    this->group_size = group_size;

    if(group_size>1){
        if(group_comm!=MPI_COMM_NULL and  group_comm!=MPI_COMM_WORLD) MPI_Comm_free(&group_comm);
        MPI_Comm_split(MPI_COMM_WORLD, this->group_rank, this->mpi_rank ,&group_comm);
    }
    else{
        group_comm = MPI_COMM_WORLD;
    }
    if(rank_comm!=MPI_COMM_NULL and rank_comm!=MPI_COMM_WORLD) MPI_Comm_free(&rank_comm);
    MPI_Comm_split(MPI_COMM_WORLD, this->mpi_rank, this->group_rank ,&rank_comm);
    //this -> epetra_group_comm = Teuchos::rcp( new Epetra_MpiComm(group_comm) );//KSW
#else
    this->total_mpi_size = 1;
    this->total_mpi_rank = 0;
    this->mpi_size = 1;
    this->mpi_rank = 0;
    this->group_rank = 0;
    this->group_size = 1;
    //this -> epetra_group_comm = rcp( new Epetra_SerialComm() );//KSW
#endif
    return;
}
Parallel_Manager::Parallel_Manager(int argc, char* argv[], int group_size){
#ifdef ACE_HAVE_MPI
    MPI_Init(&argc, &argv);
    change_group_size(group_size);
#endif

#ifdef ACE_HAVE_OMP
    #pragma omp parallel
    {
        #pragma omp master
        {
            openmp_num_threads = omp_get_num_threads();
        }
    }
#else
    openmp_num_threads = 1;
#endif
    return;
}
Parallel_Manager::~Parallel_Manager(){
#ifdef ACE_HAVE_MPI
    MPI_Finalize();
#endif
    instance=NULL;
    return;
}


Teuchos::RCP<Epetra_Comm> Parallel_Manager::get_group_comm(){
    Teuchos::RCP<Epetra_Comm> retval;
#ifdef ACE_HAVE_MPI
    retval = Teuchos::rcp(new Epetra_MpiComm(group_comm) );
    //retval = Teuchos::rcp(this -> epetra_group_comm ->Clone() );
#else
    retval = Teuchos::rcp(new Epetra_SerialComm() );
#endif
    return retval;
}

Teuchos::RCP<Epetra_Comm> Parallel_Manager::get_all_comm(){
    Teuchos::RCP<Epetra_Comm> retval;
#ifdef ACE_HAVE_MPI
    retval = Teuchos::rcp(new Epetra_MpiComm(MPI_COMM_WORLD) );
#else
    retval = Teuchos::rcp(new Epetra_SerialComm() );
#endif
    return retval;
}

Teuchos::RCP<Epetra_Comm> Parallel_Manager::get_rank_comm(){
    Teuchos::RCP<Epetra_Comm> retval;

#ifdef ACE_HAVE_MPI
    retval = Teuchos::rcp(new Epetra_MpiComm(rank_comm) );
#else
    retval = Teuchos::rcp(new Epetra_Serialcomm() );
#endif

    return retval;
}

int Parallel_Manager::group_allgatherv(double* send_values, int send_count, double* recv_buf, const int* recv_counts, const int *displs){
#ifdef ACE_HAVE_MPI
    return MPI_Allgatherv(send_values, send_count, MPI_DOUBLE, recv_buf, const_cast<int*>(recv_counts), const_cast<int*>(displs), MPI_DOUBLE, group_comm);
#else
    if(recv_counts[0]!=send_count){
        exit(-1);
    }
    std::memcpy(recv_buf, send_values, sizeof(double)*send_count);
#endif

    return 0;
}

int Parallel_Manager::group_allgatherv(int* send_values, int send_count, int* recv_buf, const int* recv_counts, const int *displs){
#ifdef ACE_HAVE_MPI
    return MPI_Allgatherv(send_values, send_count, MPI_INT, recv_buf, const_cast<int*>(recv_counts), const_cast<int*>(displs), MPI_INT, group_comm);
#else
    if(recv_counts[0]!=send_count){
        exit(-1);
    }
    std::memcpy(recv_buf, send_values, sizeof(int)*send_count);
#endif

    return 0;
}

int Parallel_Manager::group_allgather(int* send_values, int send_count, int* recv_buf, int recv_count){
#ifdef ACE_HAVE_MPI
    return MPI_Allgather(send_values, send_count, MPI_INT, recv_buf, recv_count, MPI_INT, group_comm);
#else
    if(recv_count!=send_count){
        exit(-1);
    }
    std::memcpy(recv_buf, send_values, sizeof(int)*send_count);
#endif

    return 0;
}


int Parallel_Manager::group_allgather(double* send_values, int send_count, double* recv_buf, int recv_count){
#ifdef ACE_HAVE_MPI
    return MPI_Allgather(send_values, send_count, MPI_DOUBLE, recv_buf, recv_count, MPI_DOUBLE, group_comm);
#else
    if(recv_count!=send_count){
        exit(-1);
    }
    std::memcpy(recv_buf, send_values, sizeof(double)*send_count);
#endif

    return 0;
}

int Parallel_Manager::group_gather(double* send_values, int send_count, double* recv_buf, int recv_counts, int root){
#ifdef ACE_HAVE_MPI
    return MPI_Gather(send_values, send_count, MPI_DOUBLE, recv_buf, recv_counts, MPI_DOUBLE, root, group_comm);
#else
    if(recv_count!=send_count){
        exit(-1);
    }
    std::memcpy(recv_buf, send_values, sizeof(double)*send_count);
#endif
    return 0;
}
int Parallel_Manager::group_gather(int* send_values, int send_count, int* recv_buf, int recv_counts,int root){
#ifdef ACE_HAVE_MPI
    return MPI_Gather(send_values, send_count, MPI_INT, recv_buf, recv_counts, MPI_INT, root, group_comm);
#else
    if(recv_count!=send_count){
        exit(-1);
    }
    std::memcpy(recv_buf, send_values, sizeof(int)*send_count);
#endif
    return 0;
}
int Parallel_Manager::rank_gather(int* send_values, int send_count, int* recv_buf, int recv_counts,int root){
#ifdef ACE_HAVE_MPI
    return MPI_Gather(send_values, send_count, MPI_INT, recv_buf, recv_counts, MPI_INT, root, rank_comm);
#else
    if(recv_count!=send_count){
        exit(-1);
    }
    std::memcpy(recv_buf, send_values, sizeof(int)*send_count);
#endif
    return 0;
}

int Parallel_Manager::rank_gather(double* send_values, int send_count, double* recv_buf, int recv_counts,int root){
#ifdef ACE_HAVE_MPI
    return MPI_Gather(send_values, send_count, MPI_INT, recv_buf, recv_counts, MPI_DOUBLE, root, rank_comm);
#else
    if(recv_count!=send_count){
        exit(-1);
    std::memcpy(recv_buf, send_values, sizeof(int)*send_count);
#endif
    return 0;
}


int Parallel_Manager::rank_gatherv(double* send_values, int send_count, double* recv_data, const int recv_counts[], const int displs[], int root ){
#ifdef ACE_HAVE_MPI
    //MPI_Gatherv(send_values, send_count, MPI_DOUBLE, recv_buf, const_cast<int*>(recv_counts), const_cast<int*>(displs), MPI_DOUBLE, root, rank_comm);
    return MPI_Gatherv(send_values, send_count, MPI_DOUBLE, recv_data, const_cast<int*>(recv_counts), const_cast<int*>(displs), MPI_DOUBLE, root, rank_comm);
#else
    if(recv_counts[0]!=send_count){
        exit(-1);
    }
    std::memcpy(recv_buf, send_values, sizeof(double)*send_count);
#endif

    return 0;
}

int Parallel_Manager::all_bcast(int* buffer, int count, int root){
#ifdef ACE_HAVE_MPI
    return MPI_Bcast(buffer, count, MPI_INT, root, MPI_COMM_WORLD);
#endif
    return 0;
}
int Parallel_Manager::all_bcast(double* buffer, int count, int root){
#ifdef ACE_HAVE_MPI
    return MPI_Bcast(buffer, count, MPI_DOUBLE, root, MPI_COMM_WORLD);
#endif
    return 0;
}
int Parallel_Manager::group_bcast(int* buffer, int count, int root){
#ifdef ACE_HAVE_MPI
    return MPI_Bcast(buffer, count, MPI_INT, root, group_comm);
#endif
    return 0;
}
int Parallel_Manager::group_bcast(double* buffer, int count, int root){
#ifdef ACE_HAVE_MPI
    return MPI_Bcast(buffer, count, MPI_DOUBLE, root, group_comm);
#endif
    return 0;
}

int Parallel_Manager::group_bcast(bool* buffer, int count, int root){
#ifdef ACE_HAVE_MPI
    return MPI_Bcast(buffer, count, MPI_CXX_BOOL, root, group_comm);
#endif
    return 0;
}

int Parallel_Manager::rank_bcast(int* buffer, int count, int root){
#ifdef ACE_HAVE_MPI
    return MPI_Bcast(buffer, count, MPI_INT, root, rank_comm);
#endif
    return 0;
}
int Parallel_Manager::rank_bcast(double* buffer, int count, int root){
#ifdef ACE_HAVE_MPI
    return MPI_Bcast(buffer, count, MPI_DOUBLE, root, rank_comm);
#endif
    return 0;
}
int Parallel_Manager::group_gatherv(int* send_values, int send_count, int* recv_data, const int recv_counts[], const int displs[], int root ){
#ifdef  ACE_HAVE_MPI
    return MPI_Gatherv(send_values, send_count, MPI_INT, recv_data, recv_counts, displs, MPI_INT, root, group_comm);
#endif
    return 0;
}
int Parallel_Manager::group_gatherv(double* send_values, int send_count, double* recv_data, const int recv_counts[], const int displs[], int root ){
#ifdef ACE_HAVE_MPI
    return MPI_Gatherv(send_values, send_count, MPI_DOUBLE, recv_data, recv_counts, displs, MPI_DOUBLE, root, group_comm);
#endif
    return 0;
}

int Parallel_Manager::group_scatter(double* send_data, int send_count, double* recv_data, int recv_count, int root){
#ifdef ACE_HAVE_MPI
    return MPI_Scatter(send_data, send_count, MPI_DOUBLE, recv_data, recv_count, MPI_DOUBLE, root, group_comm);
#endif
    return 0;
}

int Parallel_Manager::group_scatterv(double* send_data, int* send_count, double* recv_data, int recv_count, const int displs[], int root){
#ifdef ACE_HAVE_MPI
    return MPI_Scatterv(send_data, send_count, displs, MPI_DOUBLE, recv_data, recv_count, MPI_DOUBLE, root, group_comm);
#endif
    return 0;
}

int Parallel_Manager::rank_scatter(double* send_data, int send_count, double* recv_data, int recv_count, int root){
#ifdef ACE_HAVE_MPI
    return MPI_Scatter(send_data, send_count, MPI_DOUBLE, recv_data, recv_count, MPI_DOUBLE, root, rank_comm);
#endif
    return 0;
}

void Parallel_Manager::rank_barrier(){
#ifdef ACE_HAVE_MPI
    MPI_Barrier(rank_comm);
#endif
    return;
}

void Parallel_Manager::group_barrier(){
#ifdef ACE_HAVE_MPI
    MPI_Barrier(group_comm);
#endif
    return;
}

void Parallel_Manager::all_barrier(){
#ifdef ACE_HAVE_MPI
    MPI_Barrier(MPI_COMM_WORLD);
#endif
    return;
}

void Parallel_Manager::rank_allreduce(double* send_data, double *recv_data, int count, operation op){
#ifdef ACE_HAVE_MPI
    MPI_Allreduce(send_data, recv_data, count, MPI_DOUBLE, mpi_operation[op], rank_comm);
#endif
    return;
}
void Parallel_Manager::rank_allreduce(int* send_data,int* recv_data, int count, operation op){
#ifdef ACE_HAVE_MPI
    MPI_Allreduce(send_data, recv_data, count, MPI_INT, mpi_operation[op], rank_comm);
#endif
    return;
}

void Parallel_Manager::group_allreduce(double* send_data, double *recv_data, int count, operation op){
#ifdef ACE_HAVE_MPI
    MPI_Allreduce(send_data, recv_data, count, MPI_DOUBLE, mpi_operation[op], group_comm);
#endif
    return;
}
void Parallel_Manager::group_allreduce(int* send_data, int *recv_data, int count, operation op){
#ifdef ACE_HAVE_MPI
    MPI_Allreduce(send_data, recv_data, count, MPI_INT, mpi_operation[op], group_comm);
#endif
    return;
}
void Parallel_Manager::group_allreduce(std::complex<float>* send_data, std::complex<float>* recv_data, int count, operation op){
#ifdef ACE_HAVE_MPI
    MPI_Allreduce(send_data, recv_data, count, MPI_CXX_FLOAT_COMPLEX, mpi_operation[op], group_comm);
#endif
    return;

}

void Parallel_Manager::all_allreduce(double* send_data, double *recv_data, int count, operation op){
#ifdef ACE_HAVE_MPI
    MPI_Allreduce(send_data, recv_data, count, MPI_DOUBLE, mpi_operation[op], MPI_COMM_WORLD);
#endif
    return;
}
void Parallel_Manager::all_allreduce(int* send_data, int* recv_data, int count, operation op){
#ifdef ACE_HAVE_MPI
    MPI_Allreduce(send_data, recv_data, count, MPI_INT, mpi_operation[op], MPI_COMM_WORLD);
#endif
    return;
}

std::string Parallel_Manager::print_error(int err_no){
    if( err_no == MPI_SUCCESS ){
        return "MPI_SUCCESS: no error!";
    } else if( err_no == MPI_ERR_COMM ){
        return "MPI_ERR_COMM: Invalied communicator!";
    } else if( err_no == MPI_ERR_COUNT ){
        return "MPI_ERR_COUNT: Invalid count argument!";
    } else if( err_no == MPI_ERR_TYPE ){
        return "MPI_ERR_TYPE: Invalid datatype argument!";
    } else if( err_no == MPI_ERR_BUFFER ){
        return "MPI_BUFFER: Invalid buffer pointer!";
    } else if( err_no == MPI_ERR_ROOT ){
        return "MPI_ROOT: Invalid root!";
    } else {
        return "error number not implemented!";
    }
}

std::ostream& operator<<(std::ostream& out, const Parallel_Manager &manager){

    char hostname[1024];
    gethostname(hostname, 1024);
    if( manager.total_mpi_rank == 0) out << "====================  Parallel Manager  =========================" <<std::endl;
    if( manager.total_mpi_rank == 0) out << "**MPI INFO" <<std::endl;
    const int size = manager.total_mpi_size;
    /*
    for(int ip = 0; ip < size; ++ip){
        #ifdef ACE_HAVE_MPI
        MPI_Barrier(MPI_COMM_WORLD);
        if( ip == manager.total_mpi_rank ) out << "Proccessor\t" << ip << " responding." << " ( " << ip+1 << "/" << size << " )" << ", Processor ID = " << getpid() << " on host " << hostname;
        MPI_Barrier(MPI_COMM_WORLD);
        #endif
    }
    */
    #ifdef ACE_HAVE_MPI
    std::string sout;
    sout = "Proccessor\t" + String_Util::to_string(manager.total_mpi_rank) + " responding. (" + String_Util::to_string(manager.total_mpi_rank+1) + "/" + String_Util::to_string(size) + "), Processor ID = " + String_Util::to_string(getpid()) + " on host " + hostname;
    String_Util::trim(sout);
    sout += "\n";

    int* sizeList = new int[manager.total_mpi_size];
    int mysize = sout.size();
    MPI_Allgather( &mysize, 1, MPI_INT, sizeList, 1, MPI_INT, MPI_COMM_WORLD);
    int* displs = new int[manager.total_mpi_size]();
    for(int i=0; i<manager.total_mpi_size;++i){
        if (i!=0) {displs[i] = displs[i-1]+sizeList[i-1];}
        else{ displs[i]=0; }
    }
    int total_size = 0;
    for(int i = 0; i < manager.total_mpi_size; ++i){
        total_size += sizeList[i];
    }
    //char* mpioutput = new char[total_size+manager.total_mpi_size];
    char* mpioutput = new char[total_size+1]();
    //mpioutput[total_size]='\0';
    //;MPI_Allgatherv( const_cast<char*>(sout.c_str()), sout.size(), MPI_CHAR, mpioutput, sizeList, displs, MPI_CHAR, MPI_COMM_WORLD );
    MPI_Allgatherv( (sout.c_str()), sout.size(), MPI_CHAR, mpioutput, sizeList, displs, MPI_CHAR, MPI_COMM_WORLD );
    if( manager.total_mpi_rank == 0) out << mpioutput;
    delete[] sizeList;
    delete[] displs;
    delete[] mpioutput;
    #else
    out << "Serially running on "<< hostname<< std::endl;
    #endif
    if( manager.total_mpi_rank == 0) out << std::endl;
    if( manager.total_mpi_rank == 0) out << "**OPENMP INFO" <<std::endl;
    if( manager.total_mpi_rank == 0) out << "  openmp_num_threads: " << manager.openmp_num_threads <<std::endl;
    if( manager.total_mpi_rank == 0) out << "=================================================================" <<std::endl;

    return out;
}
#ifdef USE_CUDA
void Parallel_Manager::assign_gpus(const int ngpu,gputag tag){
    if (ngpu <= 0){
        return;
    }
    // already called
    if (vec_gpu_enables.size()>0){
        if (ngpu!=this->ngpu){
            Verbose::all() << "calling assign_gpus twice but given ngpus are different" <<std::endl;
            exit(-1);
        }
    }
    else{
        this->ngpu = ngpu;
    }

    // add tag
    if (tag==All){
        vec_gpu_enables.clear();
        for (int i=0; TAGEND != (gputag) i ; i++){
            vec_gpu_enables.push_back((gputag) i);
        }
    }
    else{
        if (get_gpu_enable(tag)==false){
            vec_gpu_enables.push_back(tag);
        }
    }

    Verbose::single(Verbose::Simple) << "GPU INFO==========================================" <<std::endl;
    Verbose::single(Verbose::Simple) << "ngpu:   "<<ngpu <<std::endl;
    if(Parallel_Manager::info().get_total_mpi_size()!=ngpu){
        Verbose::all(Verbose::Simple) << "number of processor : " <<Parallel_Manager::info().get_total_mpi_size() << std::endl;
        Verbose::all(Verbose::Simple) << "number of GPU : " <<ngpu << std::endl;
        Verbose::all(Verbose::Simple) << "number of processor and number of GPU must be same" << std::endl;
        exit(-1);
    }
    int nDevices;
    cudaGetDeviceCount(&nDevices);

    cudaError_t check;
    Verbose::single(Verbose::Simple) << "GPU is enabled" << std::endl;
    const int rank = Parallel_Manager::info().get_total_mpi_rank();
    int device=0;
//    shchoi
    check = cudaSetDevice(rank%nDevices);
    if(check!=cudaSuccess){
        std::cout << "cudaSetDevice error" << std::endl;
        std::cout << "rank : " << rank << std::endl;
        std::cout << "nDevices : " << nDevices << std::endl;
        std::cout << check << std::endl;
        exit(-1);
    }
    Verbose::single(Verbose::Detail) << "0??!!"  <<std::endl;
    check = cudaGetDevice(&device);
    Verbose::single(Verbose::Detail) << "1??!!"  <<std::endl;
    if(check!=cudaSuccess){
        std::cout << "cudaGetDevice error" << std::endl;
        std::cout << "rank : " << rank << std::endl;
        std::cout << check << std::endl;
        exit(-1);
    }

    cudaDeviceProp prop;

    check = cudaGetDeviceProperties(&prop, device);

    if(check!=cudaSuccess){
        std::cout << "cudaGetDeviceProperties error" << std::endl;
        std::cout << "rank : " << rank << std::endl;
        exit(-1);
    }
    printf("Device Number: %d\n", device);
    printf("  Device name: %s\n", prop.name);
    printf("  Memory Clock Rate (KHz): %d\n",
            prop.memoryClockRate);
    printf("  Memory Bus Width (bits): %d\n",
            prop.memoryBusWidth);
    printf("  Peak Memory Bandwidth (GB/s): %f\n\n",
            2.0*prop.memoryClockRate*(prop.memoryBusWidth/8)/1.0e6);
    Parallel_Manager::info().all_barrier();

    return;
}
/*
bool Parallel_Manager::get_gpu_assigned(){
    if (vec_gpu_enables.size==0)
        return false;
    else
        return true;
}*/
#endif
bool Parallel_Manager::get_gpu_enable(gputag tag){
    if (tag==All){
        for (int i=0; TAGEND != (gputag) i ; i++){
            if( vec_gpu_enables.end() ==std::find(vec_gpu_enables.begin(),vec_gpu_enables.end(),(gputag) i )){
                return false;
            }
        }
    }
    else{
        if(vec_gpu_enables.end() ==std::find(vec_gpu_enables.begin(),vec_gpu_enables.end(),tag)){
            return false;
        }
    }
    return true;
}
int Parallel_Manager::get_ngpu(){
    return ngpu;
}
