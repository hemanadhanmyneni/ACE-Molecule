#pragma once
#include <string>
#include <array>
#include <fstream>
#include "../../Io/Atoms.hpp"
#include "../../Basis/Basis.hpp"

namespace Read{
namespace Cube {
    /**
     * @brief Read cube file and generate Atoms object from it.
     **/
    Atoms read_cube_atoms(std::string filename);
    /**
     * @brief Read cube file and generate Atoms object from it.
     **/
    Atoms read_cube_atoms(std::ifstream &input);
    /**
     * @brief Read cube file and get the simulation box information.
     **/
    void read_cube_mesh(std::string filename, std::array<int, 3> &points, std::array< std::array<double, 3>, 3> &spacing);
    /**
     * @brief Read cube file and get the simulation box information.
     **/
    void read_cube_mesh(std::ifstream &input, std::array<int, 3> &points, std::array< std::array<double, 3>, 3> &spacing);
};
};
