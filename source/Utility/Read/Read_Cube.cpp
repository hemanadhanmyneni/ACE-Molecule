#include "Read_Cube.hpp"
#include <vector>
#include <fstream>
#include "../String_Util.hpp"
#include "../../Io/Periodic_table.hpp"
#include "../Verbose.hpp"

#define ANG_TO_BOHR (1.889725989)

using std::string;
using std::vector;
using std::array;
using std::ifstream;

Atoms Read::Cube::read_cube_atoms(std::string filename){
    ifstream input(filename);
    Atoms retval = Read::Cube::read_cube_atoms(input);
    input.close();
    return retval;
}
Atoms Read::Cube::read_cube_atoms(std::ifstream &input){
    Atoms return_val;
    string line;
    for(int i = 0; i < 2+1; ++i){
        getline(input, line);
    }
    //getline(input, line);
    vector<string> tokens = String_Util::Split_ws(line);
    int natoms = String_Util::stoi(tokens[0]);
    bool is_gaussian = false;
    for(int j = 1; j < 4; ++j){
        if(std::abs(String_Util::stod(tokens[j])) == 0.0){
            is_gaussian = true;
        }
    }
    if(natoms < 0) natoms = -natoms;

    bool is_bohr = true;
    for(int i = 0; i < 3; ++i){
        getline(input, line);
        if(String_Util::Split_ws(line)[0][0] == '-'){
            is_bohr = false;
        }
    }
    for(int i = 0; i < natoms; ++i){
        getline(input, line);
        vector<string> tokens = String_Util::Split_ws(line);
        string symbol = Periodic_atom::periodic_table[String_Util::stoi(tokens[0])-1];
        std::array<double,3> position;
        position.fill(0.);
        for(int i = 0; i < 3; i++){
            position[i] = String_Util::stod(tokens[i+2]);
            if(!is_bohr){
                position[i] = position[i] * ANG_TO_BOHR; // Unit conversion from Ang to Bohr
            }
        }
        Atom atom(symbol, position, 0);
        return_val.push(atom, false);
    }
    if(is_gaussian){
        array<double, 3> n_center;
        n_center.fill(0.);
        array<int, 3> pts;
        array< array<double, 3>, 3> uvec;
        input.clear();
        input.seekg(0);
        Read::Cube::read_cube_mesh(input, pts, uvec);
        for(int i = 0; i < 3; ++i){
            for(int j = 0; j < 3; ++j){
                n_center[i] -= pts[j]*uvec[j][i]/2.;
            }
        }
        return_val.move_positions(n_center[0], n_center[1], n_center[2]);
    }
    return_val.reset_connection_table();
    return return_val;
}

void Read::Cube::read_cube_mesh(std::string filename, std::array<int, 3> &points, std::array< std::array<double, 3>, 3> &spacing){
    ifstream input(filename);
    if(input.fail()){
        Verbose::all()<< "Read_Cube::read_cube_mesh - CANNOT find file " << filename <<  std::endl;
    }
    Read::Cube::read_cube_mesh(input, points, spacing);
    input.close();
}

void Read::Cube::read_cube_mesh(std::ifstream &input, std::array<int, 3> &points, std::array< std::array<double, 3>, 3> &spacing){
    string line;
    for(int i = 0; i < 2+1; ++i){
        getline(input, line);
    }
    //getline(input, line);
    bool is_bohr = true;
    for(int i = 0; i < 3; ++i){
        getline(input, line);
        vector<string> tokens = String_Util::Split_ws(line);
        if(String_Util::Split_ws(line)[0][0] == '-'){
            is_bohr = false;
        }
        points[i] = String_Util::stoi(tokens[0]);
        for(int j = 0; j < 3; ++j){
            spacing[i][j] = String_Util::stod(tokens[j+1]);
        }
    }
    if(!is_bohr){
        for(int i = 0; i < 3; ++i){
            for(int j = 0; j < 3; ++j){
                spacing[i][j] *= ANG_TO_BOHR;
            }
        }
    }
    return;
}
