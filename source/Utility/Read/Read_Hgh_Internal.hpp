#pragma once
#include <string>
#include <vector>

namespace Read{
/**
 * @brief Returns hard-coded HGH pseudopotential parameters.
 * @note The function names match that of Read::Hgh. See Read_Hgh.?pp
 **/
namespace Hgh_Internal {
    /**
     * @param [in] symbol HGH pseudopotential atom symbol.
     * @param [out] Zval Number of valence electrons.
     * @param [in] xc_type XC used to construct pp. LDA or PBE.
     * @param [out] core_correction If NLCC exists, true.
     **/
    void read_header(std::string symbol, double& Zval, std::string xc_type, bool& core_correction);
    /**
     * @param [in] symbol HGH pseudopotential symbol.
     * @param [in] xc_type XC used to construct pp. LDA or PBE.
     * @param [out] rloc r_loc value of HGH pseudopotential specification. Value is appended.
     * @param [out] nloc Length of c_i. Value is appended.
     * @param [out] c_i ith value corresponds to c_(i+1) value of HGH pseudopotential specification. Value is appended. Zero values are trucated.
     **/
    void read_local(std::string symbol, std::string xc_type, std::vector<double>& rloc, std::vector<int>& nloc, std::vector< std::vector<double> >& c_i);
    /**
     * @param [in] symbol HGH pseudopotential atom symbol.
     * @param [in] xc_type XC used to construct pp. LDA or PBE.
     * @param [out] h_ij_total h_ij value of HGH pseudopotential specification. Value is appended.
     * @param [out] k_ij_total Meant to be k_ij value of HGH pseudopotential specification. This value is untouched in current implementations.
     * @param [out] r_l List of r_l values (r_0, r_1, ...) of HGH pseudopotential specification. (Corresponds to angular momentum part of projector).
     * @param [out] n_l Number of nonzero elements of h_ii. If h_11 only, n_l = 1. If h_ij exists up to h_22, n_l = 2. If h_ij exists up to h_33, n_l = 3.
     * @param [out] number_vps_file Effectively, size of r_l.
     * @param [out] oamom_tmp List of projector's orbital angular momentums. r_0 -> 0, r_1 -> 1, r_2 -> 2, ... Value is appended.
     * @param [out] itype Not used. why?
     * @details Detailed indexing of h_ij_total: h_ij_total[-1] is the value read in this function.
     * Second index corresponds to angular momentum (and r_l).
     * Third index differs by corresponding n_l values.
     * If n_l[i] == 1, h_ij_total[-1][l] has size of 1 and contains h11 only.
     * If n_l[i] == 2, h_ij_total[-1][l] has size of 4 and contains h11 h12 h21 h22.
     * If n_l[i] == 3, h_ij_total[-1][l] has size of 6 and contains h11 h12 h13 h21 h22 h23 h31 h32 h33.
     **/
    void read_nonlocal(std::string symbol, std::string xc_type,
        std::vector< std::vector<std:: vector<double> > >& h_ij_total,
        std::vector< std::vector< std::vector<double> > >& k_ij_total,
        std::vector< std::vector<double> >& r_l, std::vector< std::vector<int> >& n_l,
        std::vector<int>& number_vps_file, std::vector<int>& oamom_tmp, int ixc_type);

    /**
     * @param [in] symbol HGH pseudopotential file name.
     * @param [in] xc_type XC used to construct pp. LDA or PBE.
     * @param [out] rcore r_core of HGH pseudopotential specification.
     * @param [out] ccore c_core of HGH pseudopotential specification.
     **/
    void read_core_correction(std::string symbol, std::string xc_type, double& rcore, double& ccore);
};
};
