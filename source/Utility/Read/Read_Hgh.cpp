#include "Read_Hgh.hpp"
#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>

#include "../Verbose.hpp"
#include "../String_Util.hpp"

using std::string;
using std::vector;
using std::ifstream;
using std::getline;
using std::istringstream;

void Read::Hgh::read_header(string filename, double& Zval, string type, bool& core_correction){
    string word;

    ifstream inputfile(filename.c_str());
    if(inputfile.fail()){
        Verbose::all() << "Read_Hgh::read_header - CANNOT find " << filename << std::endl;
        exit(EXIT_FAILURE);
    }

    core_correction = false;

    if(type == "HGH"){
        getline(inputfile,word);

        istringstream iss (word);
        string sub;
        iss >> sub;
        Zval = atof(sub.c_str());
    }
    else if(type == "Willand"){
        while(inputfile.eof() == false){
            getline(inputfile,word);

            if(word.find("zion",0) != string::npos){
                istringstream iss (word);
                string sub;
                iss >> sub;
                iss >> sub;
                Zval = atof(sub.c_str());
            }

            if(word.find("rcore", 0) != string::npos){
                core_correction = true;
            }
        }
    }
    else{
        Verbose::all() << "Wrong pseudopotential format: Pseudopotential.HghFormat is wrong\n";
        Verbose::all() << type << std::endl;
        exit(EXIT_FAILURE);
    }

    inputfile.close();

    return;
}

void Read::Hgh::read_local(string filename, string type, vector<double>& rloc, vector<int>& nloc, vector< vector<double> >& c_i){
    string word;

    vector<double> c_i_tmp;
    ifstream inputfile(filename.c_str());
    if(inputfile.fail()){
        Verbose::all() << "Read_Hgh::read_local - CANNOT find " << filename << std::endl;
        exit(EXIT_FAILURE);
    }

    if(type == "HGH"){
        getline(inputfile, word);

        istringstream iss (word);
        string sub;
        iss >> sub;
        iss >> sub;
        rloc.push_back(atof(sub.c_str()));

        while(iss >> sub){
            if(String_Util::isStringDouble(sub.c_str())) c_i_tmp.push_back(atof(sub.c_str()));
        }
        nloc.push_back(c_i_tmp.size());
    }
    else if(type == "Willand"){
        while(inputfile.eof() == false){
            getline(inputfile,word);
            if(word.find("rloc",0) != string::npos){
                /*
                jaechang
                istringstream iss (word);
                string sub;
                iss >> sub;
                rloc.push_back(atof(sub.c_str()));
                while(iss >> sub){
                    c_i_tmp.push_back(atof(sub.c_str()));
                }
                nloc.push_back(c_i_tmp.size());
                break;
                */
                istringstream iss (word);
                string sub;
                iss >> sub;
                rloc.push_back(atof(sub.c_str()));
                iss >> sub;
                nloc.push_back(atoi(sub.c_str()));
                for(int i=0;i<nloc[nloc.size()-1];i++){
                    iss >> sub;
                    c_i_tmp.push_back(atof(sub.c_str()));
                }
                break;
            }
        }
    }
    else{
        Verbose::all() << "Wrong pseudopotential format: Pseudopotential.HghFormat is wrong\n";
        exit(EXIT_FAILURE);
    }

    c_i.push_back(c_i_tmp);

    inputfile.close();

    return;
}

void Read::Hgh::read_nonlocal(string filename, string type, vector< vector< vector<double> > >& h_ij_total, vector< vector< vector<double> > >& k_ij_total, vector< vector<double> >& r_l, vector< vector<int> >& n_l, vector<int>& number_vps_file, vector<int>& oamom_tmp, int itype){
    string word;

    ifstream inputfile(filename.c_str());
    if(inputfile.fail()){
        Verbose::all() << "Read_Hgh::read_nonlocal - CANNOT find " << filename << std::endl;
        exit(EXIT_FAILURE);
    }

    vector<double> r_l_tmp;
    vector<int> n_l_tmp;

    vector< vector<double> > h_ij;
    vector< vector<double> > k_ij;

    vector<double> h_ij_tmp;
    vector<double> k_ij_tmp;

    if(type == "HGH"){
        while(inputfile.eof() == false){
            getline(inputfile, word);
            if(word.find("r0",0) != string::npos or word.find("rs",0) != string::npos){
                oamom_tmp.push_back(0);

                h_ij_tmp.clear();
                istringstream iss (word);
                string sub;
                iss >> sub;
                r_l_tmp.push_back(atof(sub.c_str()));

                while(iss >> sub){
                    if(String_Util::isStringDouble(sub.c_str())) h_ij_tmp.push_back(atof(sub.c_str()));
                }

                n_l_tmp.push_back(h_ij_tmp.size());

                if(h_ij_tmp.size() == 1){
                }
                else if(h_ij_tmp.size() == 2){
                    double tmp = -0.5 * sqrt(3.0/5.0) * h_ij_tmp[1];
                    h_ij_tmp.insert(h_ij_tmp.begin()+1, tmp);
                    h_ij_tmp.insert(h_ij_tmp.begin()+2, tmp);
                }
                else if(h_ij_tmp.size() == 3){
                    double tmp = -0.5 * sqrt(3.0/5.0) * h_ij_tmp[1];
                    h_ij_tmp.insert(h_ij_tmp.begin()+1, tmp);
                    h_ij_tmp.insert(h_ij_tmp.begin()+2, tmp);

                    tmp = 0.5 * sqrt(5.0/21.0) * h_ij_tmp[4];
                    h_ij_tmp.insert(h_ij_tmp.begin()+2, tmp);
                    h_ij_tmp.insert(h_ij_tmp.begin()+5, tmp);

                    tmp = -0.5 * sqrt(100.0/63.0) * h_ij_tmp[6];
                    h_ij_tmp.insert(h_ij_tmp.begin()+5, tmp);
                    h_ij_tmp.insert(h_ij_tmp.begin()+7, tmp);
                }
                else{
                    Verbose::all() << "Read_Hgh::read_nonlocal - Wrong h_ij value for rs " << h_ij_tmp.size() << "\n";
                    exit(EXIT_FAILURE);
                }

                h_ij.push_back(h_ij_tmp);
            }
            else if(word.find("r1",0) != string::npos or word.find("rp",0) != string::npos){
                oamom_tmp.push_back(1);

                h_ij_tmp.clear();
                istringstream iss (word);
                string sub;
                iss >> sub;
                r_l_tmp.push_back(atof(sub.c_str()));

                while(iss >> sub){
                    if(String_Util::isStringDouble(sub.c_str())) h_ij_tmp.push_back(atof(sub.c_str()));
                }

                n_l_tmp.push_back(h_ij_tmp.size());

                if(h_ij_tmp.size() == 1){
                }
                else if(h_ij_tmp.size() == 2){
                    double tmp = -0.5 * sqrt(5.0/7.0) * h_ij_tmp[1];
                    h_ij_tmp.insert(h_ij_tmp.begin()+1, tmp);
                    h_ij_tmp.insert(h_ij_tmp.begin()+2, tmp);
                }
                else if(h_ij_tmp.size() == 3){
                    double tmp = -0.5 * sqrt(5.0/7.0) * h_ij_tmp[1];
                    h_ij_tmp.insert(h_ij_tmp.begin()+1, tmp);
                    h_ij_tmp.insert(h_ij_tmp.begin()+2, tmp);

                    tmp = 1.0/6.0 * sqrt(35.0/11.0) * h_ij_tmp[4];
                    h_ij_tmp.insert(h_ij_tmp.begin()+2, tmp);
                    h_ij_tmp.insert(h_ij_tmp.begin()+5, tmp);

                    tmp = -1.0/6.0 * 14.0 / sqrt(11.0) * h_ij_tmp[6];
                    h_ij_tmp.insert(h_ij_tmp.begin()+5, tmp);
                    h_ij_tmp.insert(h_ij_tmp.begin()+7, tmp);
                }
                else{
                    Verbose::all() << "Read_Hgh::read_nonlocal - Wrong h_ij value for rp " << h_ij_tmp.size() << "\n";
                    exit(EXIT_FAILURE);
                }

                h_ij.push_back(h_ij_tmp);
            }
            else if(word.find("r2",0) != string::npos or word.find("rd",0) != string::npos){
                oamom_tmp.push_back(2);

                h_ij_tmp.clear();
                istringstream iss (word);
                string sub;
                iss >> sub;
                r_l_tmp.push_back(atof(sub.c_str()));

                while(iss >> sub){
                    if(String_Util::isStringDouble(sub.c_str())) h_ij_tmp.push_back(atof(sub.c_str()));
                }

                n_l_tmp.push_back(h_ij_tmp.size());

                if(h_ij_tmp.size() == 1){
                }
                else if(h_ij_tmp.size() == 2){
                    double tmp = -0.5 * sqrt(7.0/9.0) * h_ij_tmp[1];
                    h_ij_tmp.insert(h_ij_tmp.begin()+1, tmp);
                    h_ij_tmp.insert(h_ij_tmp.begin()+2, tmp);
                }
                else if(h_ij_tmp.size() == 3){
                    double tmp = -0.5 * sqrt(7.0/9.0) * h_ij_tmp[1];
                    h_ij_tmp.insert(h_ij_tmp.begin()+1, tmp);
                    h_ij_tmp.insert(h_ij_tmp.begin()+2, tmp);

                    tmp = 0.5 * sqrt(63.0/143.0) * h_ij_tmp[4];
                    h_ij_tmp.insert(h_ij_tmp.begin()+2, tmp);
                    h_ij_tmp.insert(h_ij_tmp.begin()+5, tmp);

                    tmp = -0.5 * 18.0 / sqrt(143.0) * h_ij_tmp[6];
                    h_ij_tmp.insert(h_ij_tmp.begin()+5, tmp);
                    h_ij_tmp.insert(h_ij_tmp.begin()+7, tmp);
                }
                else{
                    Verbose::all() << "Read_Hgh::read_nonlocal - Wrong h_ij value for rd " << h_ij_tmp.size() << "\n";
                    exit(EXIT_FAILURE);
                }

                h_ij.push_back(h_ij_tmp);
            }
        }

        number_vps_file.push_back(h_ij.size());

        h_ij_total.push_back(h_ij);
        //k_ij_total.push_back(k_ij);
        r_l.push_back(r_l_tmp);
        n_l.push_back(n_l_tmp);
    }
    else if(type == "Willand"){
        while(inputfile.eof() == false){
            getline(inputfile,word);
            if(word.find("nnonloc",0) != string::npos){
                istringstream iss (word);
                string sub;
                iss >> sub;
                number_vps_file.push_back(atoi(sub.c_str()));

                while(inputfile.eof() == false){
                    getline(inputfile,word);
                    if(word.find("r",0) != string::npos){
                        if(word.find("ns",0) != string::npos){
                            oamom_tmp.push_back(0);
                        }
                        else if(word.find("np",0) != string::npos){
                            oamom_tmp.push_back(1);
                        }
                        else if(word.find("nd",0) != string::npos){
                            oamom_tmp.push_back(2);
                        }
                        else if(word.find("rcore",0) != string::npos){
                        }
                        else{
                            Verbose::all() << "Wrong angular momentum in Read_Hgh.cpp" << std::endl;
                            exit(EXIT_FAILURE);
                        }

                        if(h_ij_tmp.size() != 0){
                            if(h_ij_tmp.size() == 1){
                            }
                            else if(h_ij_tmp.size() == 3){
                                double tmp = h_ij_tmp[1];
                                h_ij_tmp.insert(h_ij_tmp.begin()+1, tmp);
                            }
                            else if(h_ij_tmp.size() == 6){
                                double tmp = h_ij_tmp[1];
                                h_ij_tmp.insert(h_ij_tmp.begin()+4, tmp);

                                tmp = h_ij_tmp[3];
                                h_ij_tmp.insert(h_ij_tmp.begin()+7, tmp);

                                tmp = h_ij_tmp[6];
                                h_ij_tmp.insert(h_ij_tmp.begin()+8, tmp);
                            }
                            else{
                                Verbose::all() << "Read_Hgh::read_nonlocal - Wrong h_ij value for r" << oamom_tmp.back() << " " << h_ij_tmp.size() << "\n";
                                exit(EXIT_FAILURE);
                            }
                            h_ij.push_back(h_ij_tmp);
                            h_ij_tmp.clear();
                        }
                        if(k_ij_tmp.size() != 0){
                            if(k_ij_tmp.size() == 1){
                            }
                            else if(k_ij_tmp.size() == 3){
                                double tmp = k_ij_tmp[1];
                                k_ij_tmp.insert(k_ij_tmp.begin()+1, tmp);
                            }
                            else if(k_ij_tmp.size() == 6){
                                double tmp = h_ij_tmp[1];
                                k_ij_tmp.insert(k_ij_tmp.begin()+4, tmp);

                                tmp = k_ij_tmp[3];
                                k_ij_tmp.insert(k_ij_tmp.begin()+7, tmp);

                                tmp = k_ij_tmp[6];
                                k_ij_tmp.insert(k_ij_tmp.begin()+8, tmp);
                            }
                            else{
                                Verbose::all() << "Read_Hgh::read_nonlocal - Wrong h_ij value for r" << oamom_tmp.back() << " " << h_ij_tmp.size() << "\n";
                                exit(EXIT_FAILURE);
                            }
                            k_ij.push_back(k_ij_tmp);
                            k_ij_tmp.clear();
                        }

                        istringstream iss2 (word);
                        string sub2;
                        iss2 >> sub2;
                        r_l_tmp.push_back(atof(sub2.c_str()));
                        iss2 >> sub2;
                        int temp = atoi(sub2.c_str());
                        n_l_tmp.push_back(temp);
                        for(int i=0;i<temp;i++){
                            iss2 >> sub2;
                            h_ij_tmp.push_back(atof(sub2.c_str()));
                        }
                    }
                    else if(word.find("h",0) != string::npos){
                        istringstream iss2 (word);
                        while(true){
                            string sub2;
                            iss2 >> sub2;
                            if(sub2.find(".") != string::npos){
                                h_ij_tmp.push_back(atof(sub2.c_str()));
                            }
                            else break;
                        }
                    }
                    else if(word.find("k",0) != string::npos){
                        istringstream iss2 (word);
                        while(true){
                            string sub2;
                            iss2 >> sub2;
                            if(sub2.find(".") != string::npos){
                                k_ij_tmp.push_back(atof(sub2.c_str()));
                            }
                            else break;
                        }
                    }
                }
            }
        }

        h_ij.push_back(h_ij_tmp);
        k_ij.push_back(k_ij_tmp);

        h_ij_total.push_back(h_ij);
        k_ij_total.push_back(k_ij);

        r_l.push_back(r_l_tmp);
        n_l.push_back(n_l_tmp);
    }
    else{
        Verbose::all() << "Wrong pseudopotential format: Pseudopotential.HghFormat is wrong\n";
        exit(EXIT_FAILURE);
    }

    if( h_ij_total.size() > 0 ){
        for(int i=0; i<h_ij_total[h_ij_total.size()-1].size(); i++){
            if( r_l[r_l.size()-1].size() > 0 ) Verbose::single(Verbose::Detail) << "r_l = " << r_l[r_l.size()-1][i] << std::endl;
            for(int j=0; j<h_ij_total[h_ij_total.size()-1][i].size(); j++)
                Verbose::single(Verbose::Detail) << "h_ij_total = " << h_ij_total[h_ij_total.size()-1][i][j] << std::endl;
        }
    } else {
        Verbose::all() << "h_ij_total.size() == 0 (should not be)" << std::endl;
        exit(EXIT_FAILURE);
    }

    inputfile.close();

    return;
}

void Read::Hgh::read_core_correction(string filename, double& rcore, double& ccore){
    string word;

    ifstream inputfile(filename.c_str());
    if(inputfile.fail()){
        Verbose::all() << "Read_Hgh::read_core_correction - CANNOT find " << filename << std::endl;
        exit(EXIT_FAILURE);
    }

    while(inputfile.eof() == false){
        getline(inputfile,word);
        if(word.find("rcore",0) != string::npos){
            istringstream iss (word);
            string sub;
            iss >> sub;
            rcore = atof(sub.c_str());
            iss >> sub;
            ccore = atof(sub.c_str());
            break;
        }
    }

    inputfile.close();

    return;
}
