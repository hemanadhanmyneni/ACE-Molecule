#include "Read_Upf.hpp"
#include <iostream>
#include <cmath>
#include <cassert>
#include <sstream>
#include <fstream>
#include <cstdlib>
#include "../String_Util.hpp"
#include "../Verbose.hpp"

using std::ifstream;
using std::istringstream;
using std::string;
using std::vector;
using std::abs;
using std::atof;

bool Read::Upf::get_type(string filename){
    bool type_new;

    string word;
    ifstream inputfile(filename.c_str());

    getline(inputfile, word);
    if(word.find("UPF version",0) != string::npos){
        type_new = true;
        Verbose::single(Verbose::Detail)<< filename + " is the new type. \n" ;
    }
    else{
        type_new = false;
        Verbose::single(Verbose::Detail)<< filename + " is the old type. \n" ;
    }
    inputfile.close();

    return type_new;
}

//void Read_Upf::read_header(string filename, double* Zval, int* number_vps, int* number_pao, int* mesh_size){
void Read::Upf::read_header(string filename, double* Zval, int* number_vps, int* number_pao, int* mesh_size, bool* core_correction, bool prepend_origin/* = true*/){
    string word;
    bool type_new = get_type(filename);

    ifstream inputfile(filename.c_str());
    if(inputfile.fail()){
        Verbose::all()<< "Read_Upf::read_header - CANNOT find " +filename+"\n" ;
        exit(EXIT_FAILURE);
    }
    //int i=0;
    bool header=false;
    while(inputfile.eof()==false){
        if(type_new==true){
            getline(inputfile,word);
            if(word.find("<PP_HEADER",0) != string::npos){
                header=true;
            }

            if(header==true){
                if(word.find("z_valence",0) != string::npos){
                    //string* str = String_Util::strSplit(word,"\"");
                    vector<string> str = String_Util::strSplit(word,"\"");
                    *Zval = atof(str[1].c_str());
                }
                else if(word.find("number_of_proj",0) != string::npos){
                    *number_vps = String_Util::string_to_int(word);
                }
                else if(word.find("number_of_wfc",0) != string::npos){
                    *number_pao = String_Util::string_to_int(word);
                }
                else if(word.find("mesh_size",0) != string::npos){
                    *mesh_size = String_Util::string_to_int(word);
                }
                else if(word.find("core_correction",0) != string::npos){
                    //string* str = String_Util::strSplit(word, "\"");
                    vector<string> str = String_Util::strSplit(word, "\"");
                    if(str[1] == "T") {
                        *core_correction = true;
                        }
                    else if(str[1] == "F") {
                        *core_correction = false;
                        }
                    else{
                        Verbose::all()<< "Read_Upf::read_header - Variable \"core_correction\" in pseudopotential file is strange. \n" ;
                        Verbose::all()<< str[1] <<"\n";
                        exit(EXIT_FAILURE);
                    }
                }
            }

            if(word.find("/>",0) != string::npos){
                 break;
                }   
        }else{
            getline(inputfile,word);
            if(word.find("<PP_HEADER",0) != string::npos){
                header=true;
            }
            if(word.find("</PP_HEADER",0) != string::npos) break;

            if(header==true){
                if(word.find("Z valence",0) != string::npos){
                    istringstream iss(word);
                    string sub;
                    iss >> sub;
                    *Zval = atof(sub.c_str());
                }else if(word.find("Number of Projectors",0) != string::npos){
                    istringstream iss(word);
                    string sub;
                    iss >> sub;
                    *number_pao = atoi(sub.c_str());
                    iss >> sub;
                    *number_vps = atoi(sub.c_str());
                }else if(word.find("Number of points in mesh",0) != string::npos){
                    istringstream iss(word);
                    string sub;
                    iss >> sub;
                    *mesh_size = atof(sub.c_str());
                }
                else if(word.find("Nonlinear Core Correction", 0) != string::npos){
                    istringstream iss(word);
                    string sub;
                    iss >> sub;
                    if(sub == "T") *core_correction = true;
                    else if(sub == "F") *core_correction = false;
                    else{
                        Verbose::all()<< "Read_Upf::read_header - Variable \"core_correction\" in pseudopotential file is strange. \n";
                        Verbose::all()<< sub << "\n";
                        exit(EXIT_FAILURE);
                    }
                }
            }
        }
    }
    inputfile.close();
    if(prepend_origin){
        vector<double> rgrid = Read::Upf::read_upf(filename, "PP_R", false);
        if(rgrid[0] > 1.0E-30){
            ++(*mesh_size);
        }
    }
    return;
}

vector<double> Read::Upf::read_upf(string filename, string field, bool prepend_origin){
    vector<double> retval;

    string start = "<" + field;
    string end = "</" + field;
    string word;

    ifstream inputfile(filename.c_str());

    if(inputfile.fail()){
        Verbose::all()<< "Read_Upf::read_upf - CANNOT find " + filename +"\n" ;
        exit(EXIT_FAILURE);
    }

    bool finish=false;

    while(inputfile.eof()==false){
        getline(inputfile,word);
        if(word.find(start,0) != string::npos){
            while(true){
                getline(inputfile,word);
                if(word.find(end,0) != string::npos){ 
                    finish=true;
                    break;
                }
                vector<string> coltmp=String_Util::Split_ws(word);
                for (int i=0; i<coltmp.size(); i++){
                    double tmp=atof(coltmp[i].c_str());
                    retval.push_back(tmp);
                }
            }
        }
        if(finish==true){
            break;
        }
    }
    inputfile.close();
    while(abs(retval[retval.size()-1]) < 1.0E-30){
        retval.pop_back();
    }
    if(prepend_origin and retval.size() > 0){
        if(field == "PP_R"){
            if(retval[0] > 1.E-30){
                retval.insert(retval.begin(), 0.0);
            }
        } else if(field == "PP_RAB"){
            if(Read::Upf::read_upf(filename, "PP_R", false)[0] > 1.E-30){
                retval.insert(retval.begin(), 0.0);
            }
        } else if(field == "PP_LOCAL" or field == "PP_NLCC" or field == "PP_RHOATOM"){
            vector<double> rgrid = Read::Upf::read_upf(filename, "PP_R", false);
            if(rgrid[0] > 1.0E-30){
                retval.insert(retval.begin(), retval[0] - (retval[1] - retval[0])/(rgrid[1] - rgrid[0]) * rgrid[0]);
            }
        }
    }
    return retval;
}

// In the upf file, NONLOCAL means r*\beta(r),
// when V_NL = \sum_{i,j} D_{ij} |\beta_i> <\beta_j|.
// This code saves r*\beta(r) / r = \beta(r) to nonlocal_pp
// for integrating easily at calculate_basis.
vector< vector<double> > Read::Upf::read_nonlocal(string filename, int number_vps, vector<int>& oamom, bool prepend_origin){
    vector< vector<double> > retval;

    string word;
    bool type_new = Read::Upf::get_type(filename);

    ifstream inputfile(filename.c_str());

    if(inputfile.fail()){
        Verbose::all()<<"Read_Upf::read_nonlocal - CANNOT find " + filename +"\n";
        exit(EXIT_FAILURE);
    }
    while(inputfile.eof()==false){
        if(type_new==true){
            getline(inputfile,word);
            if(word.find("<PP_NONLOCAL",0) != string::npos){
                for(int i=0;i<number_vps;i++){
                    getline(inputfile,word);
                    if(word.find("<PP_BETA",0) != string::npos){
                        vector<double> retval_tmp;
                        // Concatenate header lines to one string.
                        while(word.find(">", 0) == string::npos){
                            word += " ";
                            string tmp;
                            getline(inputfile, tmp);
                            word += tmp;
                        }
                        // Read orbital angular momentum
                        vector<string> col2tmp = String_Util::strSplit(word," ");
                        for(int a =0; a < col2tmp.size(); a++){
                            string str = col2tmp[a];
                            if(str.find("angular_momentum",0) != string::npos){
                                string oamom_val = String_Util::strSplit(str,"=").at(1);
                                oamom.push_back(String_Util::string_to_int(oamom_val));
                                break;
                            }
                        }
                        while(true){
                            getline(inputfile,word);
                            if(word.find("</PP_BETA",0) != string::npos){
                                retval.push_back(retval_tmp);
                                break;
                            };
                            vector<string> coltmp=String_Util::Split_ws(word);
                            for (int b=0; b<coltmp.size(); b++){
                                retval_tmp.push_back(atof(coltmp[b].c_str()));
                            }
                        }
                    }
                }
            }
            else if(word.find("</PP_NONLOCAL",0) != string::npos) {
                break;
            }
        }
        else{
            getline(inputfile,word);
            if(word.find("<PP_NONLOCAL",0) != string::npos){
                for(int i=0;i<number_vps;i++){
                    getline(inputfile,word);

                    if(word.find("<PP_BETA",0) != string::npos){
                        vector<double> retval_tmp;

                        //bool in_beta=true;
                        getline(inputfile,word);
                        istringstream is(word);
                        string str;
                        is >> str;
                        is >> str;

                        // Reading orbital angular momentum
                        oamom.push_back( atoi(str.c_str()) );
                        Verbose::single(Verbose::Detail)<< "The orbital angular momentum is " << oamom[i] <<"\n";
                        // end

                        getline(inputfile,word);
                        while(true){
                            getline(inputfile,word);
                            if(word.find("</PP_BETA",0) != string::npos){
                                retval.push_back(retval_tmp);
                                break;
                            };
                            istringstream iss(word);
                            int pos=0;
                            do{
                                string sub;
                                iss >> sub;
                                if(pos<4){
                                    retval_tmp.push_back( atof(sub.c_str()) );
                                }
                                else break;
                                pos++;
                            }while(iss);
                            //if(word.find("</PP_BETA",0) != string::npos) in_beta=false;
                        }
                    }
                }
            }
            else if(word.find("</PP_NONLOCAL",0) != string::npos) break;
        }
    }
    inputfile.close();

    assert(number_vps == retval.size());

    for(int i = 0; i < retval.size(); ++i){
        while(abs(retval[i][retval[i].size()-1]) < 1.0E-30){
            retval[i].pop_back();
        }
    }

    // Divide R_BETA by r due to UPF spec. Reference: UPF spec
    vector<double> rgrid = Read::Upf::read_upf(filename, "PP_R", false);
    for(int i = 0; i < retval.size(); ++i){
        for(int k = 0; k < retval[i].size(); ++k){
            if(rgrid[k] > 1.0E-30){
                retval[i][k] /= rgrid[k];
            }
        }
    }
    if(prepend_origin){
        if(rgrid[0] < 1.0E-30){
            for(int i = 0; i < retval.size(); ++i){
                retval[i].erase(retval[i].begin());
            }
            rgrid.erase(rgrid.begin());
        }
        for(int i = 0; i < retval.size(); ++i){
            retval[i].insert(retval[i].begin(), retval[i][0] - (retval[i][1] - retval[i][0])/(rgrid[1] - rgrid[0]) * rgrid[0]);
        }
    }
    return retval;
}

vector<double> Read::Upf::read_ekb(string filename, int number_vps){
    string word;
    bool type_new = get_type(filename);
    vector<double> retval;

    ifstream inputfile(filename.c_str());

    if(inputfile.fail()){
        Verbose::all()<< "Read_Upf::read_ekb - CANNOT find " + filename + "\n";
        exit(EXIT_FAILURE);
    }
    bool finish=false;

    while(inputfile.eof()==false){
        if(type_new==true){
            getline(inputfile,word);
            if(word.find("<PP_DIJ",0) != string::npos){
                while(true){
                    getline(inputfile,word);
                    if(word.find("</PP_DIJ",0) != string::npos){
                        finish=true;
                        break;
                    }
                    istringstream iss(word);
                    int pos=0;
                    do{
                        string sub;
                        iss >> sub;
                        if(pos<4){
                            retval.push_back( atof(sub.c_str()) );
                        }else break;
                        pos++;
                    }while(iss);
                }
            }
           
            if(finish==true){
                retval.resize(number_vps*number_vps);
                break;
            }
        }else{
            retval.resize(number_vps*number_vps);
            getline(inputfile,word);
            if(word.find("<PP_DIJ",0) != string::npos){
                getline(inputfile,word);
                while(true){
                    getline(inputfile,word);
                    if(word.find("</PP_DIJ",0) != string::npos){
                        finish=true;
                        break;
                    }
                    istringstream iss(word);
                    string sub,indexi,indexj;
                    iss >> indexi;
                    iss >> indexj;
                    iss >> sub;
                    int ii = atoi(indexi.c_str())-1;
                    int ij = atoi(indexj.c_str())-1;
                    retval[ii*number_vps + ij] = atof(sub.c_str());
                }
            }
            if(finish==true) break;
        }
    }
    inputfile.close();
    return retval;
}

vector< vector<double> > Read::Upf::read_pao(string filename, int number_pao){
    string word;
    bool type_new = get_type(filename);
    vector< vector<double> > retval;

    ifstream inputfile(filename.c_str());

    if(inputfile.fail()){
        Verbose::all()<< "Read_Upf::read_pao - CANNOT find " + filename +"\n";
        exit(EXIT_FAILURE);
    }

    while(inputfile.eof()==false){
        if(type_new==true){
            getline(inputfile,word);
            if(word.find("<PP_PSWFC",0) != string::npos){
                for(int i=0;i<number_pao;i++){
                    retval.push_back( vector<double>() );
                    getline(inputfile,word);
                    if(word.find("<PP_CHI",0) != string::npos){
                        istringstream issss(word);
                        while(true){
                            if(word.find(">",0) != string::npos) break;
                            getline(inputfile,word);
                        }
                        while(true){
                            getline(inputfile,word);
                            if(word.find("</PP_CHI",0) != string::npos) break;
                            vector<string> coltmp=String_Util::Split_ws(word);
                            for (int j=0; j<coltmp.size(); j++){
                                retval[i].push_back(atof(coltmp[j].c_str()));
                            }
                        }
                    }
                }
            }
        }
        else{
            getline(inputfile,word);
            if(word.find("<PP_PSWFC",0) != string::npos){
                getline(inputfile,word);
                for(int i=0;i<number_pao;i++){
                    retval.push_back( vector<double>() );
                    if(word.find("Wavefunction",0) != string::npos){
                        while(true){
                            getline(inputfile,word);
                            if(word.find("Wavefunction",0) != string::npos) break;
                            else if(word.find("</PP_PSWFC",0) != string::npos) break;
                            istringstream iss(word);
                            int pos=0;
                            do{
                                string sub;
                                iss >> sub;
                                if(pos<4) retval[i].push_back(atof(sub.c_str()));
                                else break;
                                pos++;
                            }while(iss);
                        }
                    }
                }
            }else if(word.find("</PP_PSWFC",0) != string::npos) break;
        }
    }
    inputfile.close();
    
    return retval;
}
