#include "Read_Paw.hpp"

#include <iostream>
#include <sstream>
#include <algorithm>

#include <cmath>

#include "../String_Util.hpp"
#include "../Interpolation/Radial_Grid_Paw.hpp"
#include "../Verbose.hpp"

//*
extern "C" {
#include <xc.h>
}
//*/

using std::string;
using std::vector;
using std::min;
using std::max;
using std::abs;
using std::istringstream;
using std::map;
using String_Util::factorial;
using Read::Read_Paw;
using Read::PAW_STATE;

Read_Paw::Read_Paw(string filename, bool is_xml){
    this -> set_pawfile_type(is_xml);
    this -> read_file(filename);
}

int Read_Paw::read_file(string paw_filename){
    if(this -> type_xml == false){
        //    DECOMPRESS
    }
    this -> filename = paw_filename;
    this -> paw_radius = -1.0;

    LIBXML_TEST_VERSION

    xmlDoc *xml_doc;
    xmlNode *a_node;
    xmlNode *current_node = NULL;
    xml_doc = xmlReadFile(paw_filename.c_str(), NULL, 0);
    Verbose::single(Verbose::Normal) << "Read file " << paw_filename << std::endl;

    if( xml_doc == NULL ){
        Verbose::all() << "Dataset file parse error" << std::endl;
        exit(EXIT_FAILURE);
    }

    a_node = xmlDocGetRootElement( xml_doc );
    if( strcmp( (const char*) a_node -> name, "paw_setup" ) != 0 && strcmp( (const char*) a_node -> name, "paw_dataset" ) != 0){
        Verbose::all() << "Wrong paw_setup file: no paw_setup or paw_dataset node" << std::endl;
        exit(EXIT_FAILURE);
    }
    a_node = a_node -> children;

    for( current_node = a_node; current_node; current_node = current_node -> next ){
        if( current_node -> type == XML_ELEMENT_NODE ){
            if( strcmp( (const char*) current_node -> name, "atom") == 0 ){
                // read symbol, Z, core, valence
                xmlChar *xmlchar_attr;
                xmlchar_attr = xmlGetProp( current_node, (xmlChar*) "symbol" );
                this -> atom_symbol = string( (const char*) xmlchar_attr );
                xmlFree(xmlchar_attr);
                xmlchar_attr = xmlGetProp( current_node, (xmlChar*) "Z" );
                this -> atom_number = atoi( (const char*) xmlchar_attr );
                xmlFree(xmlchar_attr);
                xmlchar_attr = xmlGetProp( current_node, (xmlChar*) "core" );
                this -> core_electron = atoi( (const char*) xmlchar_attr );
                xmlFree(xmlchar_attr);
                xmlchar_attr = xmlGetProp( current_node, (xmlChar*) "valence" );
                this -> val_electron = atoi( (const char*) xmlchar_attr );
                xmlFree(xmlchar_attr);

            } else if( strcmp( (const char*) current_node -> name, "xc_functional" ) == 0 ){
                xmlChar *xmlchar_type, *xmlchar_name;
                xmlchar_type = xmlGetProp( current_node, (xmlChar*) "type" );
                if( strcmp( (const char*) xmlchar_type, "LDA" ) == 0 ){
                    this -> xc_type = 1;
                } else if( strcmp( (const char*) xmlchar_type, "GGA" ) == 0 ){
                    this -> xc_type = 2;
                } else if( strcmp( (const char*) xmlchar_type, "MGGA" ) == 0 ){
                    this -> xc_type = 3;
                } else if( strcmp( (const char*) xmlchar_type, "HYB" ) == 0 ){
                    this -> xc_type = 4;
                } else {
                    Verbose::single(Verbose::Normal) << "XC functional type undefined." << std::endl;
                }
                xmlchar_name = xmlGetProp( current_node, (xmlChar*) "name" );

                this -> read_xc_info( (char*) xmlchar_name );

                xmlFree(xmlchar_type);
                xmlFree(xmlchar_name);

            } else if( strcmp( (const char*) current_node -> name, "ae_energy" ) == 0 ){
                xmlChar *xmlchar_attr;
                xmlchar_attr = xmlGetProp( current_node, (xmlChar*) "kinetic" );
                this -> ae_kinetic_energy = atof( (const char*) xmlchar_attr );
                xmlFree(xmlchar_attr);
                xmlchar_attr = xmlGetProp( current_node, (xmlChar*) "xc" );
                this -> ae_xc_energy = atof( (const char*) xmlchar_attr );
                xmlFree(xmlchar_attr);
                xmlchar_attr = xmlGetProp( current_node, (xmlChar*) "electrostatic" );
                this -> ae_electrostatic_energy = atof( (const char*) xmlchar_attr );
                xmlFree(xmlchar_attr);
                xmlchar_attr = xmlGetProp( current_node, (xmlChar*) "total" );
                this -> ae_total_energy = atof( (const char*) xmlchar_attr );
                xmlFree(xmlchar_attr);

            } else if( strcmp( (const char*) current_node -> name, "core_energy" ) == 0 ){
                xmlChar *xmlchar_attr;
                xmlchar_attr = xmlGetProp( current_node, (xmlChar*) "kinetic" );
                this -> core_kinetic_energy = atof( (const char*) xmlchar_attr );
                xmlFree(xmlchar_attr);

            } else if( strcmp( (const char*) current_node -> name, "paw_radius" ) == 0 ){
                xmlChar *xmlchar_attr;
                xmlchar_attr = xmlGetProp( current_node, (xmlChar*) "rc" );
                this -> paw_radius = atof( (const char*) xmlchar_attr );
                xmlFree(xmlchar_attr);

            } else if( strcmp( (const char*) current_node -> name, "radial_grid" ) == 0 ){
                // read grid setting
                this -> read_grid_setting( current_node );

            } else if( strcmp( (const char*) current_node -> name, "ae_core_density" ) == 0 ){
                // read all electron core density
                this -> all_electron_density_core = this -> read_data_on_grid( current_node );

            } else if( strcmp( (const char*) current_node -> name, "pseudo_core_density" ) == 0 ){
                // read smooth core density
                this -> smooth_density_core = this -> read_data_on_grid( current_node );

            } else if( strcmp( (const char*) current_node -> name, "pseudo_valence_density" ) == 0 ){
                // read smooth valence density
                this -> smooth_density_valence = this -> read_data_on_grid( current_node );\

            } else if( strcmp( (const char*) current_node -> name, "valence_states" ) == 0 ){
                // valence states must be defined for partial wave
                read_state_elements( current_node );

            }
            // Partial waves have many contents. These are std::map
            else if( strcmp( (const char*) current_node -> name, "ae_partial_wave" ) == 0 ){
                //            if( strcmp( (const char*) current_node -> name, "ae_partial_wave" ) == 0 ){
                // read all electron partial wave
                xmlChar *xmlchar_state;
                xmlchar_state = xmlGetProp(current_node, (xmlChar*) "state");
                this -> all_electron_partial_waves[string( (const char*) xmlchar_state )] = this -> read_data_on_grid( current_node );
                xmlFree(xmlchar_state);

            } else if( strcmp( (const char*) current_node -> name, "pseudo_partial_wave" ) == 0 ){
                // read smooth partial wave
                xmlChar *xmlchar_state;
                xmlchar_state = xmlGetProp(current_node, (xmlChar*) "state");
                this -> smooth_partial_waves[string( (const char*) xmlchar_state )] = this -> read_data_on_grid( current_node );
                xmlFree(xmlchar_state);

            } else if( strcmp( (const char*) current_node -> name, "projector_function" ) == 0 ){
                // read projector function
                xmlChar *xmlchar_state;
                xmlchar_state = xmlGetProp(current_node, (xmlChar*) "state");
                this -> projector_functions[string( (const char*) xmlchar_state )] = this -> read_data_on_grid( current_node );
                xmlFree(xmlchar_state);

            }
            // Delta KE
            else if( strcmp( (const char*) current_node -> name, "kinetic_energy_differences" ) == 0 ){
                // read Delta KE
                this -> kinetic_energy_difference = this -> read_matrix( current_node );
            }

            else if( strcmp( (const char*) current_node -> name, "zero_potential" ) == 0 ){
                // read zero potential
                this -> zero_potential = this -> read_data_on_grid( current_node );

            } else if( strcmp( (const char*) current_node -> name, "shape_function" ) == 0 ){
                xmlChar *xmlchar_state;
                xmlchar_state = xmlGetProp(current_node, (xmlChar*) "type");

                this -> compensation_charge_lambda = 0.0;
                // Shape function charge: 1 for gauss, 2 for sinc, 3 for exp, 4 for bessel
                if( strcmp( (const char*) xmlchar_state, "gauss" ) == 0 ){
                    this -> compensation_charge_shape_type = 1;
                } else if( strcmp( (const char*) xmlchar_state, "sinc" ) == 0 ){
                    this -> compensation_charge_shape_type = 2;
                } else if( strcmp( (const char*) xmlchar_state, "exp" ) == 0 ){
                    this -> compensation_charge_shape_type = 3;
                } else if( strcmp( (const char*) xmlchar_state, "bessel" ) == 0 ){
                    this -> compensation_charge_shape_type = 4;
                    xmlFree(xmlchar_state);
                    xmlchar_state = xmlGetProp(current_node, (xmlChar*) "lamb" );
                    this -> compensation_charge_lambda = atof( (const char*) xmlchar_state );
                }
                xmlFree(xmlchar_state);
                xmlchar_state = xmlGetProp(current_node, (xmlChar*) "rc" );
                this -> compensation_charge_rc = atof( (const char*) xmlchar_state );
                xmlFree(xmlchar_state);

            }
            // Datas for MGGA
            else if( strcmp( (const char*) current_node -> name, "ae_core_kinetic_energy_density" ) == 0 ){
                // read all electron core density
                this -> all_electron_KE_density_core = this -> read_data_on_grid( current_node );

            } else if( strcmp( (const char*) current_node -> name, "pseudo_core_kinetic_energy_density" ) == 0 ){
                // read smooth core density
                this -> smooth_KE_density_core = this -> read_data_on_grid( current_node );

            }

            // Exact Exchange
            else if( strcmp( (const char*) current_node -> name, "exact_exchange") == 0 ){
                xmlChar *xmlchar_attr;
                xmlchar_attr = xmlGetProp( current_node, (xmlChar*) "core-core" );
                this -> EXX_core = atof( (const char*) xmlchar_attr );
                xmlFree(xmlchar_attr);

            } else if( strcmp( (const char*) current_node -> name, "exact_exchange_X_matrix" ) == 0 ){
                this -> EXX_matrix = this -> read_matrix( current_node );
            } else {
            }
        }
    }
    xmlFreeDoc(xml_doc);
    xmlCleanupParser();

    this -> cutoff_radius = this -> calc_cutoff_radius();
    if( this -> paw_radius < 0.0 ){
        this -> paw_radius = max( this->cutoff_radius, max( this -> get_projector_cutoff(), this -> get_partial_wave_cutoff() ) );
    }

    Verbose::single(Verbose::Normal) << "Reading " << paw_filename << " end" << std::endl;

    return 0;
}

void Read_Paw::read_xc_info( char* xc_name ){
    this -> xc_functionals.clear();
    // Short-hand notation
    if( strcmp( xc_name, "PW" ) == 0){
        this -> xc_functionals.push_back(XC(functional_get_number)("LDA_X"));
        this -> xc_functionals.push_back(XC(functional_get_number)("LDA_C_PW"));
    } else if( strcmp( xc_name, "PW91" ) == 0){
        this -> xc_functionals.push_back(XC(functional_get_number)("GGA_X_PW91"));
        this -> xc_functionals.push_back(XC(functional_get_number)("GGA_C_PW91"));
    } else if( strcmp( xc_name, "PBE" ) == 0){
        this -> xc_functionals.push_back(XC(functional_get_number)("GGA_X_PBE"));
        this -> xc_functionals.push_back(XC(functional_get_number)("GGA_C_PBE"));
    } else if( strcmp( xc_name, "RPBE" ) == 0){
        this -> xc_functionals.push_back(XC(functional_get_number)("GGA_X_RPBE"));
        this -> xc_functionals.push_back(XC(functional_get_number)("GGA_C_PBE"));
    } else if( strcmp( xc_name, "revPBE" ) == 0){
        this -> xc_functionals.push_back(XC(functional_get_number)("GGA_X_PBE_R"));
        this -> xc_functionals.push_back(XC(functional_get_number)("GGA_C_PBE"));
    } else if( strcmp( xc_name, "PBEsol" ) == 0){
        this -> xc_functionals.push_back(XC(functional_get_number)("GGA_X_PBE_SOL"));
        this -> xc_functionals.push_back(XC(functional_get_number)("GGA_C_PBE_SOL"));
    } else if( strcmp( xc_name, "AM05" ) == 0){
        this -> xc_functionals.push_back(XC(functional_get_number)("GGA_X_AM05"));
        this -> xc_functionals.push_back(XC(functional_get_number)("GGA_C_AM05"));
    } else if( strcmp( xc_name, "BLYP" ) == 0){
        this -> xc_functionals.push_back(XC(functional_get_number)("GGA_X_B88"));
        this -> xc_functionals.push_back(XC(functional_get_number)("GGA_C_LYP"));
    }
    // '+' notation
    else if( strstr( xc_name, "+" ) != NULL ){
       char * x_name = strtok( xc_name, "+" );
       char * c_name = strtok( NULL, "+" );
        this -> xc_functionals.push_back( XC(functional_get_number)(x_name) );
        this -> xc_functionals.push_back( XC(functional_get_number)(c_name) );
    } else {
        this -> xc_functionals.push_back( XC(functional_get_number)(xc_name) );
    }
    for(int i = 0; i < this -> xc_functionals.size(); ++i){
        if( this -> xc_functionals[i] == -1 ){
            Verbose::all() << "UNDEFINED XC FUNCTIONAL" << std::endl;
            exit(EXIT_FAILURE);
        }
    }
}

double Read_Paw::calc_cutoff_radius(){
    vector<double> grid = get_grid( this -> get_zero_potential_r().first );
    vector<double> zero_potential = this -> get_zero_potential_r().second;

    for(int i = 1; i < grid.size(); ++i){
        if( std::abs(zero_potential[i]) < 1.0E-8 ){
            return grid[i-1];
        }
    }
    return grid[grid.size()-1];
}

vector< vector<double> > Read_Paw::read_matrix( xmlNode* a_node ){
    xmlChar *xml_data;
    string str_line;
    vector< vector<double> > data_matrix;

    xml_data = xmlNodeGetContent( a_node );
    string str_data = string( (const char*) xml_data );
    str_data = String_Util::trim(str_data);
    istringstream iss( str_data );
    double val;

    for( int i = 0;; i++ ){
        vector<double> data_line;
        getline(iss, str_line);
        str_line = String_Util::trim(str_line);

        /*
        if( iss.eof() || (str_line.size() <= 0 && i > 0) ){
            break;
        } else if( str_line.size() == 0 && i == 0 ){
            continue;
        }
        */
        istringstream iss2( str_line );

        for( int j = 0;; j++ ){
            iss2 >> val;
            data_line.push_back( val );
            if( iss2.eof() ){
                break;
            }
        }
        if( data_line.size() > 0 ){
            data_matrix.push_back( data_line );
        }
        if( iss.eof() || str_line.size() <= 0 ){
            break;
        }
    }

    xmlFree(xml_data);
    return data_matrix;
}

void Read_Paw::read_state_elements( xmlNode* state_node ){
    xmlNode *current_node = NULL;

    for( current_node = state_node -> children; current_node; current_node = current_node -> next ){
        if( current_node -> type == XML_ELEMENT_NODE ){
            PAW_STATE current_state;
            xmlChar *xmlchar_n, *xmlchar_f, *xmlchar_l, *xmlchar_rc, *xmlchar_e, *xmlchar_id;

            xmlchar_n = xmlGetProp(current_node, (xmlChar*) "n"); // NULL if unbound
            xmlchar_f = xmlGetProp(current_node, (xmlChar*) "f"); // NULL if unbound
            xmlchar_l = xmlGetProp(current_node, (xmlChar*) "l");
            xmlchar_rc = xmlGetProp(current_node, (xmlChar*) "rc");
            xmlchar_e = xmlGetProp(current_node, (xmlChar*) "e");
            xmlchar_id = xmlGetProp(current_node, (xmlChar*) "id");

            if( xmlchar_l == NULL || xmlchar_rc == NULL || xmlchar_e == NULL || xmlchar_id == NULL ){
                Verbose::all() << "Failed while reading properties of node" << std::endl;
                exit(EXIT_FAILURE);
            }

            if( xmlchar_n == NULL ){
                current_state.n = 0;
            } else {
                current_state.n = atoi( (const char*) xmlchar_n ); // NULL if unbound
            }
            if( xmlchar_f == NULL ){
                current_state.occupation_number = 0;
            } else {
                current_state.occupation_number = atoi( (const char*) xmlchar_f ); // NULL if unbound
            }
            current_state.l = atoi( (const char*) xmlchar_l );
            current_state.cutoff_radius = atof( (const char*) xmlchar_rc );
            current_state.energy = atof( (const char*) xmlchar_e );
            current_state.state = string( (const char*) xmlchar_id );

            this -> paw_states_list.push_back( string( (const char*) xmlchar_id ) );
            this -> paw_states_map[string( (const char*) xmlchar_id )] = current_state; // Should consider UTF-8 encoding

            xmlFree( xmlchar_n );
            xmlFree( xmlchar_f );
            xmlFree( xmlchar_l );
            xmlFree( xmlchar_rc );
            xmlFree( xmlchar_e );
            xmlFree( xmlchar_id );
        }
    }
}

std::pair< string, vector<double> > Read_Paw::read_data_on_grid( xmlNode *a_node ){
    xmlChar *xml_data, *xmlchar_grid_id;
    string grid_id;
    vector<double> grid, data_vector;

    xmlchar_grid_id = xmlGetProp( a_node, (xmlChar*) "grid" );
    grid_id = string( (const char*) xmlchar_grid_id );

    if( this -> grid_map.find(grid_id) == this -> grid_map.end() ){
        Verbose::all() << "Grid setting \"" << grid_id << "\" is not specified in dataset" << std::endl;
        exit(EXIT_FAILURE);
    }
    grid = this -> grid_map[grid_id];

    xml_data = xmlNodeGetContent( a_node );
    istringstream iss( string( (const char*) xml_data ) );

    double val;
    for( int i = 0; i < grid.size(); i++ ){
        iss >> val;
        data_vector.push_back( val );
    }

    xmlFree(xmlchar_grid_id);
    xmlFree(xml_data);

    return std::make_pair( grid_id, data_vector );
}

void Read_Paw::read_grid_setting( xmlNode *grid_setting_node ){
    int istart, iend;
    xmlChar *xmlchar_eq, *xmlchar_id, *xmlchar_istart, *xmlchar_iend, *xmlchar_buf1, *xmlchar_buf2;
    char *eq;
    string id;
    double a, b, d, n;

    xmlchar_eq = xmlGetProp( grid_setting_node, (xmlChar*) "eq" );
    xmlchar_id = xmlGetProp( grid_setting_node, (xmlChar*) "id" );
    xmlchar_istart = xmlGetProp( grid_setting_node, (xmlChar*) "istart" );
    xmlchar_iend = xmlGetProp( grid_setting_node, (xmlChar*) "iend" );

    eq = (char*) xmlchar_eq;
    id = string( (const char*) xmlchar_id );
    istart = atoi( (const char*) xmlchar_istart );
    iend = atoi( (const char*) xmlchar_iend );

    int r_grid_length = iend-istart+1;
    vector<double> grid(r_grid_length);
    vector<double> grid_derivative(r_grid_length);

    if( strcmp( eq, "r=d*i" ) == 0 ){
        xmlchar_buf1 = xmlGetProp( grid_setting_node, (xmlChar*) "d" );
        d = atof( (const char*) xmlchar_buf1 );

        for( int i = istart; i <= iend; i++ ){
            grid[i] = d*i;
            grid_derivative[i] = d;
        }

    } else if( strcmp( eq, "r=a*exp(d*i)" ) == 0 ){
        xmlchar_buf1 = xmlGetProp( grid_setting_node, (xmlChar*) "a" );
        xmlchar_buf2 = xmlGetProp( grid_setting_node, (xmlChar*) "d" );
        a = atof( (const char*) xmlchar_buf1 );
        d = atof( (const char*) xmlchar_buf2 );

        for( int i = istart; i <= iend; i++ ){
            grid[i] = a*exp(d*i);
            grid_derivative[i] = grid[i]*d;
        }
        xmlFree(xmlchar_buf2);

    } else if( strcmp( eq, "r=a*(exp(d*i)-1)" ) == 0 ){
        xmlchar_buf1 = xmlGetProp( grid_setting_node, (xmlChar*) "a" );
        xmlchar_buf2 = xmlGetProp( grid_setting_node, (xmlChar*) "d" );
        a = atof( (const char*) xmlchar_buf1 );
        d = atof( (const char*) xmlchar_buf2 );

        for( int i = istart; i <= iend; i++ ){
            grid[i] = a*(exp(d*i)-1);
            grid_derivative[i] = d*(grid[i]+a);
        }
        xmlFree(xmlchar_buf2);

    } else if( strcmp( eq, "r=a*i/(1-b*i)" ) == 0 ){
        xmlchar_buf1 = xmlGetProp( grid_setting_node, (xmlChar*) "a" );
        xmlchar_buf2 = xmlGetProp( grid_setting_node, (xmlChar*) "b" );
        a = atof( (const char*) xmlchar_buf1 );
        b = atof( (const char*) xmlchar_buf2 );

        for( int i = istart; i <= iend; i++ ){
            grid[i] = a*i/(1-b*i);
            grid_derivative[i] = a/(1-b*i)/(1-b*i);
        }
        xmlFree(xmlchar_buf2);

    } else if( strcmp( eq, "r=a*i/(n-i)" ) == 0 ){
        xmlchar_buf1 = xmlGetProp( grid_setting_node, (xmlChar*) "a" );
        xmlchar_buf2 = xmlGetProp( grid_setting_node, (xmlChar*) "n" );
        a = atof( (const char*) xmlchar_buf1 );
        n = atof( (const char*) xmlchar_buf2 );

        for( int i = istart; i <= iend; i++ ){
            grid[i] = a*i/(n-i);
            grid_derivative[i] = a*n/(n-i)/(n-i);
        }
        xmlFree(xmlchar_buf2);

    } else if( strcmp( eq, "r=(i/n+a)^5/a-a^4" ) == 0 ){
        xmlchar_buf1 = xmlGetProp( grid_setting_node, (xmlChar*) "a" );
        xmlchar_buf2 = xmlGetProp( grid_setting_node, (xmlChar*) "n" );
        a = atof( (const char*) xmlchar_buf1 );
        n = atof( (const char*) xmlchar_buf2 );

        for( int i = istart; i <= iend; i++ ){
            grid[i] = pow(i/n+a,5)/a-pow(a,4);
            grid_derivative[i] = 5.0/a/n*pow(i/n+a,4);
        }
        xmlFree(xmlchar_buf2);

    } else {
        Verbose::all() << "Cannot understand grid_setting \"eq\"" << std::endl
                  << "eq should be one of the followings." << std::endl
                  << "r=d*i, r=a*exp(d*i), r=a*(exp(d*i)-1), r=a*i/(1-b*i), r=a*i/(n-i), r=(i/n+a)^5/a-a^4" << std::endl
                  << "No spacing allowed" << std::endl;
        exit(EXIT_FAILURE);
    }

    this -> grid_map[id] = grid;
    this -> grid_derivative_map[id] = grid_derivative;

    xmlFree(xmlchar_eq);
    xmlFree(xmlchar_id);
    xmlFree(xmlchar_istart);
    xmlFree(xmlchar_iend);
    xmlFree(xmlchar_buf1);
}

// From here, just return values
double Read_Paw::get_cutoff_radius(){
    return this -> cutoff_radius;
}

void Read_Paw::set_pawfile_type(bool is_xml){
    this -> type_xml = is_xml;
}

vector<string> Read_Paw::get_grid_types(){
    vector<string> grid_types;

    for( std::map< string, vector<double> >::iterator it = this -> grid_map.begin(); it != this -> grid_map.end(); ++it ){
        grid_types.push_back(it->first);
    }
    return grid_types;
}

vector<double> Read_Paw::get_grid( string grid_id ){
    if( this -> grid_map.find(grid_id) == this -> grid_map.end() ){
        Verbose::all() << "No grid setting named \"" << grid_id << "\"." << std::endl;
        exit(EXIT_FAILURE);
    }
    return this -> grid_map[grid_id];
}

//vector<double> Read_Paw::get_grid( const char* grid_id ){
//    return this -> get_grid( string( grid_id ) );
//}

vector<double> Read_Paw::get_grid_derivative( string grid_id ){
    if( this -> grid_derivative_map.find(grid_id) == this -> grid_derivative_map.end() ){
        Verbose::all() << "No grid setting named \"" << grid_id << "\"." << std::endl;
        exit(EXIT_FAILURE);
    }
    return this -> grid_derivative_map[grid_id];
}

//vector<double> Read_Paw::get_grid_derivative( const char* grid_id ){
//    return this -> get_grid_derivative( string( grid_id ) );
//}

std::pair< string, vector<double> > Read_Paw::get_all_electron_core_density_r(){
    return this -> all_electron_density_core;
}

std::pair< string, vector<double> > Read_Paw::get_smooth_core_density_r(){
    return this -> smooth_density_core;
}

std::pair< string, vector<double> > Read_Paw::get_smooth_valence_density_r(){
    return this -> smooth_density_valence;
}

std::pair< string, vector<double> > Read_Paw::get_zero_potential_r(){
    return this -> zero_potential;
}

vector<string> Read_Paw::get_partial_wave_types( ){
    return this -> paw_states_list;
}

std::pair< string, vector<double> > Read_Paw::get_all_electron_partial_wave_r( string wave_state ){
    if( this -> all_electron_partial_waves.find( wave_state ) == this -> all_electron_partial_waves.end() ){
        Verbose::all() << "No all electron partial wave of state \"" << wave_state << "\"." << std::endl;
        exit(EXIT_FAILURE);
    }
    return this -> all_electron_partial_waves[wave_state];
}

std::pair< string, vector<double> > Read_Paw::get_smooth_partial_wave_r( string wave_state ){
    if( this -> smooth_partial_waves.find( wave_state ) == this -> smooth_partial_waves.end() ){
        Verbose::all() << "No smooth partial wave of state \"" << wave_state << "\"." << std::endl;
        exit(EXIT_FAILURE);
    }
    return this -> smooth_partial_waves[wave_state];
}

std::pair< string, vector<double> > Read_Paw::get_projector_function_r( string wave_state ){
    if( this -> projector_functions.find( wave_state ) == this -> projector_functions.end() ){
        Verbose::all() << "No projector function of state \"" << wave_state << "\"." << std::endl;
        exit(EXIT_FAILURE);
    }
    return this -> projector_functions[wave_state];
}

PAW_STATE Read_Paw::get_partial_wave_state( string wave_state ){
    if( this -> paw_states_map.find( wave_state ) == this -> paw_states_map.end() ){
        Verbose::all() << "No partial wave of state \"" << wave_state << "\"." << std::endl;
        exit(EXIT_FAILURE);
    }
    return this -> paw_states_map[wave_state];
}

/*
std::pair< string, vector<double> > Read_Paw::get_all_electron_partial_wave_r( const char *wave_state ){
    return this -> get_all_electron_partial_wave_r( string( wave_state ) );
}

std::pair< string, vector<double> > Read_Paw::get_smooth_partial_wave_r( const char *wave_state ){
    return this -> get_smooth_partial_wave_r( string( wave_state ) );
}

std::pair< string, vector<double> > Read_Paw::get_projector_function_r( const char *wave_state ){
    return this -> get_projector_function_r( string( wave_state ) );
}

PAW_STATE Read_Paw::get_partial_wave_state( const char *wave_state ){
    return this -> get_partial_wave_state( string( wave_state ) );
}
*/

std::pair< string, vector<double> > Read_Paw::get_all_electron_partial_wave_r( int wave_state ){
    return this -> get_all_electron_partial_wave_r( this -> get_partial_wave_types().at(wave_state) );
}

std::pair< string, vector<double> > Read_Paw::get_smooth_partial_wave_r( int wave_state ){
    return this -> get_smooth_partial_wave_r( this -> get_partial_wave_types().at(wave_state) );
}

std::pair< string, vector<double> > Read_Paw::get_projector_function_r( int wave_state ){
    return this -> get_projector_function_r( this -> get_partial_wave_types().at(wave_state) );
}

PAW_STATE Read_Paw::get_partial_wave_state( int wave_state ){
    return this -> get_partial_wave_state( this -> get_partial_wave_types().at(wave_state) );
}

vector< vector<double> > Read_Paw::get_kinetic_energy_difference_matrix (){
    return this -> kinetic_energy_difference;
}

double Read_Paw::get_core_kinetic_energy(){
    return this -> core_kinetic_energy;
}

double Read_Paw::get_ae_kinetic_energy(){
    return this -> ae_kinetic_energy;
}

double Read_Paw::get_ae_xc_energy(){
    return this -> ae_xc_energy;
}

double Read_Paw::get_ae_electrostatic_energy(){
    return this -> ae_electrostatic_energy;
}

double Read_Paw::get_ae_total_energy(){
    return this -> ae_total_energy;
}

std::pair< string, vector<double> > Read_Paw::get_all_electron_core_KE_density_r(){
    return this -> all_electron_density_core;
}

std::pair< string, vector<double> > Read_Paw::get_smooth_core_KE_density_r(){
    return this -> smooth_density_core;
}

string Read_Paw::get_atom_symbol(){
    return this -> atom_symbol;
}

int Read_Paw::get_atom_number(){
    return this -> atom_number;
}

int Read_Paw::get_core_electron_number(){
    return this -> core_electron;
}

int Read_Paw::get_valence_electron_number(){
    return this -> val_electron;
}

unsigned int Read_Paw::get_shape_function_type(){
    return this -> compensation_charge_shape_type;
}

double Read_Paw::get_shape_function_rc(){
    return this -> compensation_charge_rc;
}

double Read_Paw::get_shape_function_lambda(){
    return this -> compensation_charge_lambda;
}

vector<int> Read_Paw::get_xc_functionals(){
    return this -> xc_functionals;
}

double Read_Paw::get_EXX_core(){
    return this -> EXX_core;
}

double Read_Paw::get_paw_radius(){
    return this -> paw_radius;
}

vector< vector<double> > Read_Paw::get_EXX_matrix(){
    return this -> EXX_matrix;
}

vector<double> Read_Paw::get_compensation_charge_on_grid( int l, string grid_id ){
    vector<double> grid = this -> grid_map[grid_id];
    vector<double> grid_derivative = this -> grid_derivative_map[grid_id];
    vector<double> retval;
    retval.reserve(grid.size());

    double rc = grid.back();

    for(int i = 0; i < grid.size(); ++i){
        //rc = sqrt(10)*this -> compensation_charge_rc;
        if(grid[i] <= rc){
            retval.push_back(this -> get_radial_compensation_charge(grid[i], l));
        } else {
            retval.push_back(0);
        }
    }

    // Test
    // normalize: int d vect(r) r^l Y_L g_L' = delta_LL' so int r^2 dr r^l g_l should be 1
    // I only got analytic norm for this charge for gaussian form only.
    //Verbose::single(Verbose::Detail) << "comp charge rc: " << rc << std::endl;
    if( this -> compensation_charge_shape_type != 1 ){
        double intval = 0.0;
        vector<double> pow_func;
        for(int i = 0; i < grid.size(); ++i){
            pow_func.push_back( pow(grid[i], l) );
        }
        intval = Radial_Grid::Paw::radial_integrate( pow_func, retval, grid, grid_derivative );
        for(int i = 0; i < grid.size(); ++i){
            retval[i] /= intval;
        }
    }
    //intval = Radial_Grid::Paw::radial_integrate( pow_func, retval, grid, grid_derivative );
    //Verbose::single(Verbose::Detail) << "COMP charge r-grid intval of L = " << l << " = " << intval << std::endl;
    return retval;
}

int Read_Paw::get_xc_type(){
    return this -> xc_type;
}

bool Read_Paw::get_pawfile_type(){
    return this -> type_xml;
}

double Read_Paw::get_projector_cutoff(){
    double rc = -1.0;
    for(int i = 0; i < this -> get_partial_wave_types().size(); ++i){
        vector<double> proj = this -> get_projector_function_r(i).second;
        vector<double> grid = this -> get_grid( this -> get_projector_function_r(i).first );
        double tmprc = 0.0;
        int j = grid.size()-2;
        for(j = grid.size()-2; std::abs( proj[j] ) < 1.0E-30; --j){
        }
        if( tmprc > rc ){
            rc = grid[j+1];
        }
    }
    return rc;
}

double Read_Paw::get_radial_compensation_charge(double r, int l){
    double retval = 0.0;

    if( this -> compensation_charge_shape_type == 1 ){
        // GAUSS
        retval = 1.0/sqrt(4*M_PI) * factorial(l) / factorial(2*l+1) * pow(2.0/this->compensation_charge_rc, 2*l+3) * pow(r, l) * exp(-pow(r/this -> compensation_charge_rc, 2));
        //retval.push_back( pow(r, l) * exp(-pow(r/this -> compensation_charge_rc, 2)) );
    } else if( this -> compensation_charge_shape_type == 2 ){
        // SINC
        if( r < 1.0E-10 ){
            retval = r;
        } else {
            retval = pow(r, l) * pow( sin(M_PI*r/this -> compensation_charge_rc) / (M_PI*r/this -> compensation_charge_rc), 2);
        }

    } else if( this -> compensation_charge_shape_type == 3 ){
        // EXP
        retval = pow(r, l) * exp( - pow(r/this -> compensation_charge_rc, this -> compensation_charge_lambda ) );
    } else if( this -> compensation_charge_shape_type == 4 ){
        // BESSEL
        /*
           retval.push_back( pow( sin(M_PI*r/this -> compensation_charge_shape_rc) / (M_PI*grid[i]/this -> compensation_charge_shape_rc), 2) );
           */
    }

    return retval;
}

double Read_Paw::get_core_density_cutoff(){
    vector<double> grid = this -> get_grid( this -> get_all_electron_core_density_r().first );
    vector<double> dgrid = this -> get_grid_derivative( this -> get_all_electron_core_density_r().first );
    vector<double> core_n = this -> get_all_electron_core_density_r().second;

    double integrated = 0.0;
    int r;
    for(r = grid.size()-1; r >= 0 and integrated < 1.0E-07; --r){
        integrated += sqrt(4*M_PI) * core_n[r] * grid[r] * grid[r] * dgrid[r];
    }
    return grid.at(r+1);
}

double Read_Paw::get_partial_wave_cutoff(){
    double rc = -1.0;
    for(int i = 0; i < this -> get_partial_wave_types().size(); ++i){
        double tmprc = this -> get_partial_wave_state(i).cutoff_radius;
        if( tmprc > rc ){
            rc = tmprc;
        }
    }
    return rc;
}
