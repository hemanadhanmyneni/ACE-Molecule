#include "Read_Hgh_Internal.hpp"
#include <array>
#include <algorithm>
#include "../Verbose.hpp"

using std::string;
using std::vector;
using std::array;

#include "HghSpec.inc"

void Read::Hgh_Internal::read_header(string symbol, double& Zval, string xc_type, bool& core_correction){
    HGHSpec hgh = get_hghspec(symbol, xc_type);
    Zval = hgh.num_val;
    core_correction = false;
}

void Read::Hgh_Internal::read_local(string symbol, string xc_type, vector<double>& rloc, vector<int>& nloc, vector< vector<double> >& c_i){
    HGHSpec hgh = get_hghspec(symbol, xc_type);
    rloc.push_back(hgh.r_loc);
    nloc.push_back(hgh.coeff_loc.size());
    c_i.push_back(hgh.coeff_loc);
}

void Read::Hgh_Internal::read_nonlocal(string symbol, string xc_type,
    vector< vector< vector<double> > >& h_ij_total,
    vector< vector< vector<double> > >& k_ij_total,
    vector< vector<double> >& r_l, vector< vector<int> >& n_l,
    std::vector<int>& number_vps_file, std::vector<int>& oamom_tmp, int itype
){
    HGHSpec hgh = get_hghspec(symbol, xc_type);
    r_l.push_back(hgh.r_nl);
    number_vps_file.push_back(hgh.r_nl.size());
    n_l.push_back(hgh.nonzero_diags);

    vector< vector<double> > h_ij;
    for(int l = 0; l < hgh.coeff_nl.size(); ++l){
        oamom_tmp.push_back(l);
        vector<double> h_ij_tmp;
        switch(hgh.nonzero_diags[l]){
            case 0:
                break;
            case 1:
                h_ij_tmp.push_back(hgh.coeff_nl[l][0]);
                break;
            case 2:
                h_ij_tmp.resize(4);
                h_ij_tmp[0] = hgh.coeff_nl[l][0];
                h_ij_tmp[1] = h_ij_tmp[2] = hgh.coeff_nl[l][3];
                h_ij_tmp[3] = hgh.coeff_nl[l][1];
                break;
            case 3:
                h_ij_tmp.resize(9);
                h_ij_tmp[0] = hgh.coeff_nl[l][0];
                h_ij_tmp[1] = h_ij_tmp[3] = hgh.coeff_nl[l][3];
                h_ij_tmp[2] = h_ij_tmp[6] = hgh.coeff_nl[l][4];
                h_ij_tmp[4] = hgh.coeff_nl[l][1];
                h_ij_tmp[5] = h_ij_tmp[7] = hgh.coeff_nl[l][5];
                h_ij_tmp[8] = hgh.coeff_nl[l][2];
                break;
            default:
                Verbose::all() << "Wrong HGH pseudopotential data for " << symbol << std::endl;
                exit(EXIT_FAILURE);
        }
        h_ij.push_back(h_ij_tmp);
    }
    h_ij_total.push_back(h_ij);
}

void Read::Hgh_Internal::read_core_correction(string symbol, string xc_type, double& rcore, double& ccore){
    rcore = 0.0; ccore = 0.0;
    return;
}
