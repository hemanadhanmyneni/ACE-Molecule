#pragma once
#include <string>
#include <vector>

namespace Read{
namespace Upf {
    //< @return True if the UPF file version is 2.0.
    bool get_type(std::string filename);
    //void read_header(std::string filename, double* Zval, int* number_vps, int* number_pao, int* mesh_size);
    /**
     * @brief Read UPF header and parse the data.
     * @param filename Input filename.
     * @param Zval Number of valence electrons.
     * @param number_vps Number of projector functions
     * @param number_pao Number of atomic pseudo-orbitals in PP_PSWFC.
     * @param mesh_size UPF file mesh size.
     * @param core_correction Availablity of NLCC.
     * @param prepend_origin Add (extrapolated) origin value to the front.
     **/
    void read_header(std::string filename, double* Zval, int* number_vps, int* number_pao, int* mesh_size, bool* core_correction, bool prepend_origin = true);
    /**
     * @brief Read requested field, described on the basis.
     * @param filename UPF filename
     * @param field Database field name.
     * @param prepend_origin Add (extrapolated) origin value to the front.
     **/
    std::vector<double> read_upf(std::string filename, std::string field, bool prepend_origin = true);
    /**
     * @brief Read nonlocal potential part.
     * @param filename UPF filename.
     * @param number_vps Number of projector functions. See read_header.
     * @param oamom [out] Orbital angular momentum.
     * @param prepend_origin Add (extrapolated) origin value to the front.
     * @return Nonlocal pp. Index: [Projector function index (max number_vps)][UPF radial mesh (max mesh_size)]. Note that returned values are divided by r (UPF spec).
     **/
    std::vector< std::vector<double> > read_nonlocal(std::string filename, int number_vps, std::vector<int>& oamom, bool prepend_origin = true);
    /**
     * @brief Read E_KB (Coefficient).
     **/
    std::vector<double> read_ekb(std::string filename, int number_vps);

    /**
     * @brief Read pseudo-atomic orbitals.
     * @param filename UPF filename.
     * @param number_pao Nuber of pseudo-atomic orbitals. See read_header.
     * @return Pseudo-atomic orbitals. Index: [PS orbital index (max number_pao)][UPF radial mesh (max mesh_size)].
     **/
    std::vector< std::vector<double> > read_pao(std::string filename, int number_pao);
};
};
