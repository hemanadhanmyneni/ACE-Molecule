#include "Double_Grid.hpp"
#include <iostream>
#include <cmath>
#include "Parallel_Manager.hpp"
#include "Parallel_Util.hpp"
#include "Verbose.hpp"

using std::vector;
using std::sin;
using Teuchos::RCP;

vector<double> Double_Grid::get_sampling_coefficients( std::string FilterType, int FineDimension ){
    vector<double> sampling_coefficients;
    //Coefficients for supersampling of target function//
    //RCP<ParameterList> parameters = Teuchos::sublist(parameters,"BasicInformation");

    if( FineDimension < 1 ){
        return sampling_coefficients;
    }

    if( FilterType == "Sinc" ){
    //Using Filter function as Sinc function//
        int sin_N = 60;
        for(int i = 0; i < sin_N*FineDimension; ++i){
            double new_x = static_cast<double>(i)/FineDimension;
            double value = 0.0;   
            if(i==0){
                value = 1.0;
            } else if( i % FineDimension == 0 ){
                value = 0.0;
            } else{
                value = sin(M_PI*new_x)/(M_PI*new_x);
            }
            sampling_coefficients.push_back(value);
        } 
    }

    else if( FilterType == "Lagrange"){
    //Using Filter function as Lagrange function of order 8//
        for(int i=0; i<8*FineDimension; i++){
            double value = 1.0;
            int kmin = -int(floor(17.0/2.0));
            int kmax = int(ceil(17.0/2.0));
            double new_x = static_cast<double>(i)/FineDimension;
            int jx = int(floor(new_x));

            for(int ix=kmin; ix<-jx; ix++){
                value *= (new_x-jx-ix)/(-jx-ix);
            } 
            for(int ix=-jx+1; ix<kmax+1; ix++){
                value *= (new_x-jx-ix)/(-jx-ix);
            } 
            sampling_coefficients.push_back(value);
        }
    }

    return sampling_coefficients;
}


RCP<Epetra_Vector> Double_Grid::sample(
        vector<double> fine_vals,
        vector<int> fine_inds,
        RCP<const Basis> fine_basis,
        RCP<const Basis> out_basis,
        std::string filter_type,
        int FineDimension, double beta,
        vector<double> center, double cutoff
){
    int * MyGlobalElements = out_basis -> get_map() -> MyGlobalElements();
    int NumMyElements = out_basis -> get_map() -> NumMyElements();
    std::array<double,3> fine_scaling = fine_basis -> get_scaling();

    // Interpolate proj
    double rc_in = cutoff;
    double rc_out = rc_in * beta;      // rmax = beta * rcut by SO.Ryu
    vector<double> sampling_coefficients = Double_Grid::get_sampling_coefficients(filter_type, FineDimension);
    RCP<Epetra_Vector> retval = Teuchos::rcp( new Epetra_Vector( *out_basis -> get_map(), true ) );

    // k: FILTER FUNCTION
    for(int kl = 0; kl < NumMyElements; ++kl){
        int k = MyGlobalElements[kl];
        double x_out, y_out, z_out;
        out_basis -> get_position(k, x_out, y_out, z_out);
        if( pow(x_out-center[0],2)+pow(y_out-center[1],2)+pow(z_out-center[2],2) > rc_out*rc_out ){
            continue;
        }
        // j: finer basis
        for(int j = 0; j < fine_inds.size(); ++j){
            double x_fine, y_fine, z_fine;
            fine_basis -> get_position(fine_inds[j], x_fine, y_fine, z_fine);

            int zx = round(abs(x_out - x_fine)/fine_scaling[0]);
            int zy = round(abs(y_out - y_fine)/fine_scaling[1]);
            int zz = round(abs(z_out - z_fine)/fine_scaling[2]);

            double fin_val = fine_vals[j] * sampling_coefficients[zx]
                             * sampling_coefficients[zy]  * sampling_coefficients[zz];

            fin_val /= pow(FineDimension,1.5);
            int ierr = retval -> SumIntoGlobalValue( k, 0, fin_val );
            if( ierr != 0 ){
                Verbose::all() << "fine proj SumIntoGlobalValue error" << std::endl;
            }
        }
    } // for kl

    Parallel_Manager::info().all_barrier();
    return retval;
}
// */

RCP<Epetra_MultiVector> Double_Grid::sample(
        RCP<Epetra_MultiVector> fine_vector,
        RCP<const Basis> fine_basis,
        RCP<const Basis> out_basis,
        std::string filter_type,
        int FineDimension, double beta,
        vector<double> center, double cutoff
){
    RCP<Epetra_MultiVector> retval = Teuchos::rcp( new Epetra_MultiVector( *out_basis -> get_map(), fine_vector -> NumVectors(), false) );

    vector< vector<double> > fine_vals; vector< vector<int> > fine_inds;
    Parallel_Util::group_extract_nonzero_from_multivector(fine_vector, fine_vals, fine_inds);
    for(int i = 0; i < fine_vector -> NumVectors(); ++i){
        RCP<Epetra_Vector> tmp = Double_Grid::sample(fine_vals[i], fine_inds[i], fine_basis, out_basis, filter_type, FineDimension, beta, center, cutoff);
        retval -> operator()(i) -> Update(1.0, *tmp, 0.0);
    }
    return retval;
}
RCP<Epetra_Vector> Double_Grid::sample(
        RCP<Epetra_Vector> fine_vector,
        RCP<const Basis> fine_basis,
        RCP<const Basis> out_basis,
        std::string filter_type,
        int FineDimension, double beta,
        vector<double> center, double cutoff
){
    vector<double> fine_vals; vector<int> fine_inds;
    Parallel_Util::group_extract_nonzero_from_vector(fine_vector, fine_vals, fine_inds);
    RCP<Epetra_Vector> retval = Double_Grid::sample(fine_vals, fine_inds, fine_basis, out_basis, filter_type, FineDimension, beta, center, cutoff);
    return retval;
}
// */
