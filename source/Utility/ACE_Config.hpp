#pragma once 
#include "Teuchos_config.h"
#include "Epetra_config.h"

#ifdef HAVE_MPI
    #define ACE_HAVE_MPI
#else 
    #ifdef EPETRA_ACE_HAVE_MPI
        #define ACE_HAVE_MPI
    #endif 
#endif 


#ifdef EPETRA_HAVE_OMP
    #define ACE_HAVE_OMP
#endif 

#ifdef USE_CUDA
#endif 
