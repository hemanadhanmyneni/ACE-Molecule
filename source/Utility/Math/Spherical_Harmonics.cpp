#include "Spherical_Harmonics.hpp"
#include <iostream>
#include <stdexcept>
#include <cstdlib>
#include <cmath>
#include "../String_Util.hpp"
#include "../Verbose.hpp"

#define __YLM_CUTOFF__ 1.0E-30

using std::abs;
using std::endl;
using std::min;
using std::max;
using std::sqrt;
using String_Util::factorial;

//Spherical_Harmonics::Spherical_Harmonics(){
//}

// real spherical harmonics
double Spherical_Harmonics::Ylm(int l,int m,double x,double y,double z){ 
    double r=sqrt(x*x+y*y+z*z);
    double Ylm;
    if( l!=0 && r < __YLM_CUTOFF__ ) return 0.0;
    Ylm = Spherical_Harmonics::rlYlm(l,m,x,y,z);
    for(int i = 0; i < l; ++i){
        Ylm /= r;
    }
    return Ylm;
}

// r^l * real spherical harmonics
double Spherical_Harmonics::rlYlm(int l,int m,double x,double y,double z){
    double r=sqrt(x*x+y*y+z*z);
    double rlYlm;
    if (l==0){
        if (m==0){
            rlYlm=0.28209479177387814;//1.0/sqrt(4.0*M_PI)
        }else{
            throw std::invalid_argument("Wrong spin angular momentum " + String_Util::to_string(m) + " for l = " + String_Util::to_string(l) +"!");
        }
    }else if(l==1){
        if(m==-1){
            rlYlm=0.4886025119029199*y;//sqrt(3.0/4.0/M_PI)
        }else if(m==0){
            rlYlm=0.4886025119029199*z;//sqrt(3.0/4.0/M_PI)
        }else if(m==1){
            rlYlm=0.4886025119029199*x;//sqrt(3.0/4.0/M_PI)
        }else{
            throw std::invalid_argument("Wrong spin angular momentum " + String_Util::to_string(m) + " for l = " + String_Util::to_string(l) +"!");
        }
    }else if(l==2){
        if(m==-2){
            rlYlm=1.0925484305920792*x*y;//sqrt(15.0/4.0/M_PI)
        }else if(m==-1){
            rlYlm=1.0925484305920792*y*z;//sqrt(15.0/4.0/M_PI)
        }else if(m==0){
            rlYlm=0.31539156525252005*(3.0*z*z-r*r);//sqrt(5.0/16.0/M_PI)
        }else if(m==1){
            rlYlm=1.0925484305920792*z*x;//sqrt(15.0/4.0/M_PI)
        }else if(m==2){
            rlYlm=0.5462742152960396*(x*x-y*y);//sqrt(15.0/16.0/M_PI)
        }else{
            throw std::invalid_argument("Wrong spin angular momentum " + String_Util::to_string(m) + " for l = " + String_Util::to_string(l) +"!");
        }
    }else if(l==3){
        if(m==-3){
            rlYlm=0.5900435899266435*(3.0*x*x-y*y)*y;//sqrt(35.0/32.0/M_PI)
        }else if(m==-2){
            rlYlm=2.890611442640554*x*y*z;//sqrt(105.0/4.0/M_PI)
        }else if(m==-1){
            rlYlm=0.4570457994644658*y*(4.0*z*z-x*x-y*y);//sqrt(21.0/32.0/M_PI)
        }else if(m==0){
            rlYlm=0.3731763325901154*z*(2.0*z*z-3.0*x*x-3.0*y*y);//sqrt(7.0/16.0/M_PI)
        }else if(m==1){
            rlYlm=0.4570457994644658*x*(4.0*z*z-x*x-y*y);//sqrt(21.0/32.0/M_PI)
        }else if(m==2){
            rlYlm=1.445305721320277*(x*x-y*y)*z;//sqrt(105.0/16.0/M_PI)
        }else if(m==3){
            rlYlm=0.5900435899266435*(x*x-3.0*y*y)*x;//sqrt(35.0/32.0/M_PI)
        }else{
            throw std::invalid_argument("Wrong spin angular momentum " + String_Util::to_string(m) + " for l = " + String_Util::to_string(l) +"!");
        }
    }else if(l==4){
        if(m==-4){
            rlYlm=2.5033429417967046*x*y*(x*x-y*y);//sqrt(315.0/16.0/M_PI)
        }else if(m==-3){
            rlYlm=1.7701307697799307*(3.0*x*x-y*y)*y*z;//sqrt(315.0/32.0/M_PI)
        }else if(m==-2){
            rlYlm=0.9461746957575601*x*y*(7.0*z*z-r*r);//sqrt(45.0/16.0/M_PI)
        }else if(m==-1){
            rlYlm=0.6690465435572892*y*z*(7.0*z*z-3.0*r*r);//sqrt(45.0/32.0/M_PI)
        }else if(m==0){
            rlYlm=0.10578554691520431*(35.0*z*z*z*z-30.0*z*z*r*r+3.0*r*r*r*r);//sqrt(9.0/256.0/M_PI)
        }else if(m==1){
            rlYlm=0.6690465435572892*x*z*(7.0*z*z-3.0*r*r);//sqrt(45.0/32.0/M_PI)
        }else if(m==2){
            rlYlm=0.47308734787878004*(x*x-y*y)*(7.0*z*z-r*r);//sqrt(45.0/64.0/M_PI)
        }else if(m==3){
            rlYlm=1.7701307697799307*(x*x-3.0*y*y)*x*z;//sqrt(315.0/32.0/M_PI)
        }else if(m==4){
            rlYlm=0.6258357354491761*(x*x*(x*x-3.0*y*y)-y*y*(3.0*x*x-y*y));//sqrt(315.0/256.0/M_PI)
        }else{
            throw std::invalid_argument("Wrong spin angular momentum " + String_Util::to_string(m) + " for l = " + String_Util::to_string(l) +"!");
        }
    }else if(l==5){
        if(m==-5){
            rlYlm=0.6563820568401701*y*(y*y*y*y+5*x*x*x*x-10*x*x*y*y);
        }else if(m==-4){
            rlYlm=8.302649259524165*x*y*z*(x*x-y*y);
        }else if(m==-3){
            rlYlm=0.4892382994352504*y*(y*y*r*r-9*y*y*z*z-3*x*x*r*r+27*x*x*z*z);
        }else if(m==-2){
            rlYlm=4.793536784973324*x*y*z*(3*z*z-r*r);
        }else if(m==-1){
            rlYlm=0.45294665119569694*y*(-14*z*z*r*r+r*r*r*r+21*z*z*z*z);
        }else if(m==0){
            rlYlm=0.1169503224534236*z*(63*z*z*z*z+15*r*r*r*r-70*z*z*r*r);
        }else if(m==1){
            rlYlm=0.45294665119569694*x*(r*r*r*r-14*z*z*r*r+21*z*z*z*z);
        }else if(m==2){
            rlYlm=2.396768392486662*z*(-3*y*y*z*z+y*y*r*r+3*x*x*z*z-x*x*r*r);
        }else if(m==3){
            rlYlm=0.4892382994352504*x*(9*x*x*z*z-27*y*y*z*z-x*x*r*r+3*y*y*r*r);
        }else if(m==4){
            rlYlm=2.075662314881041*z*(y*y*y*y-6*x*x*y*y+x*x*x*x);
        }else if(m==5){
            rlYlm=0.6563820568401701*x*(-10*x*x*y*y+5*y*y*y*y+x*x*x*x);
        }else{
            throw std::invalid_argument("Wrong spin angular momentum " + String_Util::to_string(m) + " for l = " + String_Util::to_string(l) +"!");
        }
    }else if(l==6){
        if(m==-6){
            rlYlm=1.3663682103838286*x*y*(-10*x*x*y*y+3*x*x*x*x+3*y*y*y*y);
        }else if(m==-5){
            rlYlm=2.366619162231752*y*z*(y*y*y*y-10*x*x*y*y+5*x*x*x*x);
        }else if(m==-4){
            rlYlm=2.0182596029148967*x*y*(-x*x*r*r+y*y*r*r-11*y*y*z*z+11*x*x*z*z);
        }else if(m==-3){
            rlYlm=0.9212052595149235*y*z*(-11*y*y*z*z-9*x*x*r*r+33*x*x*z*z+3*y*y*r*r);
        }else if(m==-2){
            rlYlm=0.9212052595149235*x*y*(r*r*r*r+33*z*z*z*z-18*z*z*r*r);
        }else if(m==-1){
            rlYlm=0.5826213625187314*y*z*(5*r*r*r*r+33*z*z*z*z-30*z*z*r*r);
        }else if(m==0){
            rlYlm=0.06356920226762842*(231*z*z*z*z*z*z-5*r*r*r*r*r*r+105*z*z*r*r*r*r-315*z*z*z*z*r*r);
        }else if(m==1){
            rlYlm=0.5826213625187314*x*z*(-30*z*z*r*r+33*z*z*z*z+5*r*r*r*r);
        }else if(m==2){
            rlYlm=0.46060262975746175*(33*x*x*z*z*z*z+x*x*r*r*r*r-y*y*r*r*r*r-18*x*x*z*z*r*r+18*y*y*z*z*r*r-33*y*y*z*z*z*z);
        }else if(m==3){
            rlYlm=0.9212052595149235*x*z*(-3*x*x*r*r-33*y*y*z*z+9*y*y*r*r+11*x*x*z*z);
        }else if(m==4){
            rlYlm=0.5045649007287242*(11*y*y*y*y*z*z-66*x*x*y*y*z*z-x*x*x*x*r*r+6*x*x*y*y*r*r+11*x*x*x*x*z*z-y*y*y*y*r*r);
        }else if(m==5){
            rlYlm=2.366619162231752*x*z*(5*y*y*y*y+x*x*x*x-10*x*x*y*y);
        }else if(m==6){
            rlYlm=0.6831841051919143*(x*x*x*x*x*x+15*x*x*y*y*y*y-15*x*x*x*x*y*y-y*y*y*y*y*y);
        }else{
            throw std::invalid_argument("Wrong spin angular momentum " + String_Util::to_string(m) + " for l = " + String_Util::to_string(l) +"!");
        }
    }else if(l>6){
        throw std::invalid_argument("Orbital angular momentum l > 6 is not supported yet.");
    }
    return rlYlm;
}

/*
// Calculate associated Legendre function
double Spherical_Harmonics::Plm(int l,int m,double x){
double P00=1.0;
double P10=x;
double P11=-sqrt(1-x*x);
double P1_1=-0.5*P11;

double Plm=0.0;

if(l==0){
if(m==0){
Plm=P00;
}else{
Verbose::all() << "Wrong spin angular momentum" << endl;
exit(EXIT_FAILURE);
}
}else if(l==1){
if(m==-1){
Plm=P1_1;
}else if(m==0){
Plm=P00;
}else if(m==1){
Plm=P11;
}else{
Verbose::all() << "Wrong spin angular momentum" << endl;
exit(EXIT_FAILURE);
}
}else if(l>1){
double Plm=P11;
// First, calculate Pll
for(int i=1;i<l;i++){
Plm = -(2.0*i+1.0) * sqrt(1.0-x*x) * Plm;
}
// Second, calculate Plm

// end
}
return Plm;
}
*/

double Spherical_Harmonics::real_YYY_integrate(int l1, int l2, int l3, int m1, int m2, int m3){
    if( l1 < 0 || l2 < 0 || l3 < 0 ){
        return 0.0;
    }
    if( abs(m1) > l1 || abs(m2) > l2 || abs(m3) > l3 ){
        return 0.0;
    }
    // m1, m2, m3 < 0
    if( m1 < 0 && m2 < 0 && m3 < 0 ){
        return 0.0;
    }
    // m1 > 0, m2, m3 < 0
    if( m1 > 0 && m2 < 0 && m3 < 0 ){
        if( abs(m1) - abs(m2) - abs(m3) == 0 ){
            //return -pow(-1., m1) * Spherical_Harmonics::YYY_integrate(l1, l2, l3, abs(m1), -abs(m2), -abs(m3)) / sqrt(2);
            return -( m1 % 2 == 0? 1: -1 ) * Spherical_Harmonics::YYY_integrate(l1, l2, l3, abs(m1), -abs(m2), -abs(m3)) / sqrt(2);
        } else if( abs(m2) - abs(m3) - abs(m1) == 0 ){
            return ( m2 % 2 == 0? 1: -1 ) * Spherical_Harmonics::YYY_integrate(l1, l2, l3, -abs(m1), abs(m2), -abs(m3)) / sqrt(2);
        } else if( abs(m3) - abs(m1) - abs(m2) == 0 ){
            return ( m3 % 2 == 0? 1: -1 ) * Spherical_Harmonics::YYY_integrate(l1, l2, l3, -abs(m1), -abs(m2), abs(m3)) / sqrt(2);
        } else {
            return 0.0;
        }
    } else if( m1 < 0 && m2 > 0 && m3 < 0 ){
        if( abs(m1) - abs(m2) - abs(m3) == 0 ){
            return ( m1 % 2 == 0? 1: -1 ) * Spherical_Harmonics::YYY_integrate(l1, l2, l3, abs(m1), -abs(m2), -abs(m3)) / sqrt(2);
        } else if( abs(m2) - abs(m3) - abs(m1) == 0 ){
            return -( m2 % 2 == 0? 1: -1 ) * Spherical_Harmonics::YYY_integrate(l1, l2, l3, -abs(m1), abs(m2), -abs(m3)) / sqrt(2);
        } else if( abs(m3) - abs(m1) - abs(m2) == 0 ){
            return ( m3 % 2 == 0? 1: -1 ) * Spherical_Harmonics::YYY_integrate(l1, l2, l3, -abs(m1), -abs(m2), abs(m3)) / sqrt(2);
        } else {
            return 0.0;
        }
    } else if( m1 < 0 && m2 < 0 && m3 > 0 ){
        if( abs(m1) - abs(m2) - abs(m3) == 0 ){
            return ( m1 % 2 == 0? 1: -1 ) * Spherical_Harmonics::YYY_integrate(l1, l2, l3, abs(m1), -abs(m2), -abs(m3)) / sqrt(2);
        } else if( abs(m2) - abs(m3) - abs(m1) == 0 ){
            return ( m2 % 2 == 0? 1: -1 ) * Spherical_Harmonics::YYY_integrate(l1, l2, l3, -abs(m1), abs(m2), -abs(m3)) / sqrt(2);
        } else if( abs(m3) - abs(m1) - abs(m2) == 0 ){
            return -( m3 % 2 == 0? 1: -1 ) * Spherical_Harmonics::YYY_integrate(l1, l2, l3, -abs(m1), -abs(m2), abs(m3)) / sqrt(2);
        } else {
            return 0.0;
        }
    }
    // m1 = 0, m2, m3 < 0
    if( m1 == 0 && m2 < 0 && m3 < 0 ){
        return ( m2 % 2 == 0? 1: -1 ) * Spherical_Harmonics::YYY_integrate(l1, l2, l3, 0, m2, -m3);
    } else if( m1 < 0 && m2 == 0 && m3 < 0 ){
        return ( m3 % 2 == 0? 1: -1 ) * Spherical_Harmonics::YYY_integrate(l1, l2, l3, -m1, 0, m3);
    } else if( m1 < 0 && m2 < 0 && m3 == 0 ){
        return ( m1 % 2 == 0? 1: -1 ) * Spherical_Harmonics::YYY_integrate(l1, l2, l3, m1, -m2, 0);
    }
    // m1 >= 0, m2 >= 0, m3 < 0
    if( m1 >= 0 && m2 >= 0 && m3 < 0 ){
        return 0.0;
    } else if( m1 < 0 && m2 >= 0 && m3 >= 0 ){
        return 0.0;
    } else if( m1 >= 0 && m2 < 0 && m3 >= 0 ){
        return 0.0;
    }
    // m1, m2, m3 = 0
    if( m1 == 0 && m2 == 0 && m3 == 0 ){
        return Spherical_Harmonics::YYY_integrate(l1, l2, l3, 0, 0, 0);
    }
    // m1, m2 > 0, m3 = 0
    if( m1 > 0 && m2 > 0 && m3 == 0 ){
        return ( m1 % 2 == 0? 1: -1 ) * Spherical_Harmonics::YYY_integrate(l1, l2, l3, m1, -m2, 0);
    } else if( m2 > 0 && m3 > 0 && m1 == 0 ){
        return ( m2 % 2 == 0? 1: -1 ) * Spherical_Harmonics::YYY_integrate(l1, l2, l3, 0, m2, -m3);
    } else if( m3 > 0 && m1 > 0 && m2 == 0 ){
        return ( m3 % 2 == 0? 1: -1 ) * Spherical_Harmonics::YYY_integrate(l1, l2, l3, -m1, 0, m3);
    }
    // m1 > 0, m2, m3 = 0
    if( m1 > 0 && m2 == 0 && m3 == 0 ){
        return 0.0;
    } else if( m1 == 0 && m2 > 0 && m3 == 0 ){
        return 0.0;
    } else if( m1 == 0 && m2 == 0 && m3 > 0 ){
        return 0.0;
    }
    // m1, m2, m3 > 0
    if( m1 > 0 && m2 > 0 && m3 > 0 ){
        if( abs(m1) - abs(m2) - abs(m3) == 0 ){
            return ( m1 % 2 == 0? 1: -1 ) * Spherical_Harmonics::YYY_integrate(l1, l2, l3, abs(m1), -abs(m2), -abs(m3)) / sqrt(2);
        } else if( abs(m2) - abs(m3) - abs(m1) == 0 ){
            return ( m2 % 2 == 0? 1: -1 ) * Spherical_Harmonics::YYY_integrate(l1, l2, l3, -abs(m1), abs(m2), -abs(m3)) / sqrt(2);
        } else if( abs(m3) - abs(m1) - abs(m2) == 0 ){
            return ( m3 % 2 == 0? 1: -1 ) * Spherical_Harmonics::YYY_integrate(l1, l2, l3, -abs(m1), -abs(m2), abs(m3)) / sqrt(2);
        } else {
            return 0.0;
        }
    }
    Verbose::all() << "LOGIC ERROR IN Spherical_Harmonics::real_YYY_integrate: " << l1 << ", " << l2 << ", " << l3 << ", " << m1 << ", " << m2 << ", " << m3 << endl;
    return 0.0;
}

// Called Gaunt coefficient
double Spherical_Harmonics::YYY_integrate(int l1, int l2, int l3, int m1, int m2, int m3){
    double norm_const = pow( static_cast<double>(2*l1+1)*(2*l2+1)*(2*l3+1)/4./M_PI, 0.5 );
    return norm_const * Wigner_3j(l1, l2, l3, 0, 0, 0) * Wigner_3j(l1, l2, l3, m1, m2, m3);
}

//double Spherical_Harmonics::Wigner_3j(int l1, int l2, int l3, int m1, int m2, int m3){
double Spherical_Harmonics::Wigner_3j(long long int l1, long long int l2, long long int l3, long long int m1, long long int m2, long long int m3){
    // Most equations are came from http://mathworld.wolfram.com/Wigner3j-Symbol.html
    // Selection rules
    if( abs(m1)>abs(l1) || abs(m2)>abs(l2) || abs(m3)>abs(l3) ){
        return 0.0;
    } else if( m1+m2+m3 != 0 ){
        return 0.0;
    } else if( abs(l1-l2)>l3 || l1+l2<l3 ){
        return 0.0;
    } else if( m1==0 && m2==0 && m3==0 && ((l1+l2+l3) % 2) == 1 ){
        return 0.0;
    }

    // Specific case (m1==-m2 for m3==0 guarenteed from selection rules)
    if( l1==l2 && l3==0 && m3==0 ){
        return ( (l1-m1) % 2 == 0? 1: -1 ) / sqrt(2*l1+1);
    } else if( l2==l3 && l1 == 0 && m1==0 ){
        return ( (l2-m2) % 2 == 0? 1: -1 ) / sqrt(2*l2+1);
    } else if( l3==l1 && l2==0 && m2==0 ){
        return ( (l1-m1) % 2 == 0? 1: -1 ) / sqrt(2*l1+1);
    }
    // Specific case
    if( l1+l2==l3 ){
        return ( (l1-l2-m3) % 2 == 0? 1: -1 ) * sqrt( static_cast<double>( factorial(2*l1)*factorial(2*l2) )/factorial(2*l1+2*l2+1)*factorial(l1+l2+m3)*factorial(l1+l2-m3)/factorial(l1+m1)/factorial(l1-m1)/factorial(l2+m2)/factorial(l2-m2) );
    } else if( l2+l3==l1 ){
        return ( (l2-l3-m1) % 2 == 0? 1: -1 ) * sqrt( static_cast<double>( factorial(2*l2)*factorial(2*l3) )/factorial(2*l2+2*l3+1)*factorial(l2+l3+m1)*factorial(l2+l3-m1)/factorial(l2+m2)/factorial(l2-m2)/factorial(l3+m3)/factorial(l3-m3) );
    } else if( l3+l1==l2 ){
        return ( (l3-l1-m2) % 2 == 0? 1: -1 ) * sqrt( static_cast<double>( factorial(2*l3)*factorial(2*l1) )/factorial(2*l3+2*l1+1)*factorial(l3+l1+m2)*factorial(l3+l1-m2)/factorial(l3+m3)/factorial(l3-m3)/factorial(l1+m1)/factorial(l1-m1) );
    }
    // Specific case// WHY m3 == 0 not guarenteed?? Though it is right
    if( l1==m1 && l1+m2==0 ){
        return ( (-l1+l2-m3) % 2 == 0? 1: -1 ) * pow( static_cast<double>( factorial(2*l1)*factorial(-l1+l2+l3) )/factorial(l1+l2+l3+1)/factorial(l1-l2+l3)*factorial(l1+l2-m3)*factorial(l3+m3)/factorial(l1+l2-l3)/factorial(-l1+l2+m3)/factorial(l3-m3), 0.5);
    } else if( l2==m2 && l2+m3==0 ){
        return Wigner_3j(l2, l3, l1, m2, m3, m1);
    } else if( l3==m3 && l3+m1==0 ){
        return Wigner_3j(l3, l1, l2, m3, m1, m2);
    } else if( l1==m1 && l1+m3==0 ){
        return Wigner_3j(l1, l3, l2, m1, m3, m2) * ( (l1+l2+l3) % 2 == 0? 1: -1 );// 160114 added odd permutations
    } else if( l2==m2 && l2+m1==0 ){
        return Wigner_3j(l2, l1, l3, m2, m1, m3) * ( (l1+l2+l3) % 2 == 0? 1: -1 );// 160114 added odd permutations
    } else if( l3==m3 && l3+m2==0 ){
        return Wigner_3j(l3, l2, l1, m3, m2, m1) * ( (l1+l2+l3) % 2 == 0? 1: -1 );// 160114 added odd permutations
    }

    // Specific case (l1+l2+l3 even guarenteed from selection rules)
    if( m1==0 && m2==0 && m3==0 ){
        long long int L = l1+l2+l3;
        //return pow(-1., L/2)*sqrt( static_cast<double>( factorial(L-2*l1)*factorial(L-2*l2) )*factorial(L-2*l3)/factorial(L+1) ) * factorial(L/2)/factorial(L/2-l1)/factorial(L/2-l2)/factorial(L/2-l3);
        return ( L % 4 == 0? 1: -1 ) * sqrt( static_cast<double>( factorial(L-2*l1)*factorial(L-2*l2) )*factorial(L-2*l3)/factorial(L+1) ) * factorial(L/2)/factorial(L/2-l1)/factorial(L/2-l2)/factorial(L/2-l3);
    }
    // General formula: may implement DOI:10.1103/PhysRevE.57.7274
    // sum part
    //Verbose::all() << "Using General Formula for " << l1 << ", " << l2 << ", " << l3 << ", " << m1 << ", " << m2 << ", " << m3 << endl;
    double retval = 0.0;
    int t_max = min(-l3+l1+l2,min(l1-m1,l2+m2));
    int t_min = max(0,(int)max(l2-l3-m1,l1-l3+m2));
    if( t_min > t_max ) return 0;
    for(int t = t_min; t <= t_max; ++t){
        if( t < 0 || l3-l2+t+m1 < 0 || l3-l1+t-m2 < 0 || l1+l2-l3-t < 0 || l1-t-m1 < 0 || l2-t+m2 < 0 ){
            throw std::invalid_argument("ERROR in Wigner_3j calculations: ERROROUS value in factorials for general formula!");
        }
        retval += ( t % 2 == 0? 1: -1 )/ static_cast<double>( factorial(t)*factorial(l3-l2+t+m1)*factorial(l3-l1+t-m2)*factorial(l1+l2-l3-t)*factorial(l1-t-m1)*factorial(l2-t+m2));
        //Verbose::all() << "t = " << t << ", retval sum = " << retval << endl;
    }
    // other coefficient part
    retval *= sqrt( static_cast<double>( factorial(-l1+l2+l3)*factorial(l1-l2+l3)*factorial(l1+l2-l3) ) / factorial(l1+l2+l3+1) );
    //Verbose::all() << "After triangle coefficient retval = " << retval << endl;
    retval *= sqrt( static_cast<double>( factorial(l1-m1) )*factorial(l1+m1)*factorial(l2-m2)*factorial(l2+m2)*factorial(l3-m3)*factorial(l3+m3) );
    //Verbose::all() << "After other coefficient retval = " << retval << endl;
    if( (l1-l2-m3) % 2 != 0 ){
        retval *= -1;
    }
    return retval;
}    

/*
int factorial(int n){
    int retval = 1;
    if(n==1||n==0){return 1;}
    else if(n<0){return 0;}
    for(int i = 2; i<=n; ++i){
        retval*=i;
    }
    return retval;
}
*/

/*
#include <iomanip>
int main(){
    const int lmax = 6;
    for(int l1 = 0; l1 <= lmax; ++l1){
        for(int l2 = 0; l2 <= lmax; ++l2){
            for(int l3 = 0; l3 <= lmax; ++l3){
                for(int m1 = -l1; m1 <= l1; ++m1){
                    for(int m2 = -l2; m2 <= l2; ++m2){
                        for(int m3 = -l3; m3 <= l3; ++m3){
                            double ang_val = Spherical_Harmonics::Wigner_3j(l1, l2, l3, m1, m2, m3);
                            if( m1+m2+m3 != 0 ){
                                if( ang_val != 0 ){
                                    Verbose::all() << "Y" << l1 << m1 << "Y" << l2 << m2 << "Y" << l3 << m3 << " Wigner_3j = " << std::fixed << std::setprecision(6) << ang_val << " ERROR!" << endl;
                                }
                            } else {
                                Verbose::all() << "Y" << l1 << m1 << "Y" << l2 << m2 << "Y" << l3 << m3 << " Wigner_3j = " << std::fixed << std::setprecision(6) << ang_val << endl;
                            }
                        }
                    }
                }
            }
        }
    }

    int l1 = 4, l2 = 4, l3 = 4, m1 = 0, m2 = 0, m3 = 0;
    double ang_val = Spherical_Harmonics::Wigner_3j(l1, l2, l3, m1, m2, m3);
    Verbose::all() << "Y" << l1 << m1 << "Y" << l2 << m2 << "Y" << l3 << m3 << " Wigner_3j = " << std::fixed << std::setprecision(6) << ang_val << endl;

    return 0;
}
// */
/*
#include <iomanip>
int main(){
    const int lmax = 2;
    for(int l1 = 0; l1 <= lmax; ++l1){
        for(int l2 = 0; l2 <= lmax; ++l2){
            for(int l3 = 0; l3 <= lmax; ++l3){
                for(int m1 = -l1; m1 <= l1; ++m1){
                    for(int m2 = -l2; m2 <= l2; ++m2){
                        for(int m3 = -l3; m3 <= l3; ++m3){
                            double ang_val = Spherical_Harmonics::real_YYY_integrate(l1, l2, l3, m1, m2, m3);
                            if( l1 == 0 ){
                                if( l2 == l3 and m2 == m3 ){
                                    if( abs(ang_val - 1/sqrt(4*M_PI)) > 1.0E-10 ){
                                        Verbose::all() << "ERROR! Y" << l1 << m1 << "Y" << l2 << m2 << "Y" << l3 << m3 << " real_YYY_integrate = " << std::fixed << std::setprecision(6) << ang_val << endl;
                                    }
                                } else {
                                    if( ang_val != 0 ){
                                        Verbose::all() << "ERROR! Y" << l1 << m1 << "Y" << l2 << m2 << "Y" << l3 << m3 << " real_YYY_integrate = " << std::fixed << std::setprecision(6) << ang_val << endl;
                                    }
                                }
                            }
                            else if( l2 == 0 ){
                                if( l3 == l1 and m3 == m1 ){
                                    if( abs(ang_val - 1/sqrt(4*M_PI)) > 1.0E-10 ){
                                        Verbose::all() << "ERROR! Y" << l1 << m1 << "Y" << l2 << m2 << "Y" << l3 << m3 << " real_YYY_integrate = " << std::fixed << std::setprecision(6) << ang_val << endl;
                                    }
                                } else {
                                    if( ang_val != 0 ){
                                        Verbose::all() << "ERROR! Y" << l1 << m1 << "Y" << l2 << m2 << "Y" << l3 << m3 << " real_YYY_integrate = " << std::fixed << std::setprecision(6) << ang_val << endl;
                                    }
                                }
                            }
                            else if( l3 == 0 ){
                                if( l1 == l2 and m1 == m2 ){
                                    if( abs(ang_val - 1/sqrt(4*M_PI)) > 1.0E-10 ){
                                        Verbose::all() << "ERROR! Y" << l1 << m1 << "Y" << l2 << m2 << "Y" << l3 << m3 << " real_YYY_integrate = " << std::fixed << std::setprecision(6) << ang_val << endl;
                                    }
                                } else {
                                    if( ang_val != 0 ){
                                        Verbose::all() << "ERROR! Y" << l1 << m1 << "Y" << l2 << m2 << "Y" << l3 << m3 << " real_YYY_integrate = " << std::fixed << std::setprecision(6) << ang_val << endl;
                                    }
                                }
                            }
                            else {
                                if( abs(m1)-abs(m2)-abs(m3) != 0 and -abs(m1)+abs(m2)-abs(m3) != 0 and -abs(m1)-abs(m2)+abs(m3) != 0 ){
                                    if( ang_val != 0 ) Verbose::all() << "ERROR! Y" << l1 << m1 << "Y" << l2 << m2 << "Y" << l3 << m3 << " real_YYY_integrate = " << std::fixed << std::setprecision(6) << ang_val << endl;
                                } else {
                                    Verbose::all() << "Y" << l1 << m1 << "Y" << l2 << m2 << "Y" << l3 << m3 << " real_YYY_integrate = " << std::fixed << std::setprecision(6) << ang_val << endl;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    int l1 = 1, l2 = 1, l3 = 2, m1 = -1, m2 = -1, m3 = 0;
    //double ang_val = Spherical_Harmonics::real_YYY_integrate(l1, l2, l3, m1, m2, m3);
    //Verbose::all() << "Y" << l1 << m1 << "Y" << l2 << m2 << "Y" << l3 << m3 << " integrate = " << std::fixed << std::setprecision(6) << ang_val << endl;
    Verbose::all() << Spherical_Harmonics::YYY_integrate(l1, l2, l3, m1, -m2, 0) << endl;
    Verbose::all() << Spherical_Harmonics::YYY_integrate(l1, l2, l3, -m1, m2, 0) << endl;

    return 0;
}
*/
