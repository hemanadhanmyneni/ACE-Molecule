#include "Spherical_Harmonics_Expansion.hpp"
#include <iostream>
#include <cmath>

#include "../Interpolation/Tricubic_Interpolation.hpp"
#include "../Calculus/Lebedev_Quadrature.hpp"

using std::vector;
using Teuchos::RCP;

vector<double> Spherical_Harmonics::Expansion::expand(
        int l, int m, 
        RCP<const Basis> mesh, 
        Teuchos::RCP<Epetra_MultiVector> values, 
        int index, 
        vector<double> dest_grid,
        double * expand_center/* = NULL*/
){
    vector<double> expand_val(dest_grid.size(), 0.0);

    double center[3];
    if( expand_center == NULL ){
        for( int i = 0; i < 3; ++i){
            center[i] = 0.0;
        }
    } else {
        for( int i = 0; i < 3; ++i){
            center[i] = expand_center[i];
        }
    }

    vector<double> weight = Lebedev_Quadrature::get_weight();
    vector< vector<double> > pts = Lebedev_Quadrature::get_points_on_unit_sphere();

    for(int n = 0; n < weight.size(); ++n){
        double Y = Spherical_Harmonics::Ylm(l,m,pts[n][0],pts[n][1],pts[n][2]);
        vector< std::array<double,3> > pos;
        for(int r = 0; r < dest_grid.size(); ++r){
            std::array<double,3> tmp;
            for(int i = 0; i < 3; ++i){
                tmp[i] =dest_grid[r]*pts[n][i]+center[i];
            }
            pos.push_back(tmp);
        }

        vector<double> radial_val = Interpolation::Tricubic::interpolate( mesh, values, index, pos);

        for(int r = 0; r < dest_grid.size(); ++r){
            expand_val[r] += radial_val[r]*Y*weight[n]*4*M_PI;
        }
    }// for n

    return expand_val;
}
