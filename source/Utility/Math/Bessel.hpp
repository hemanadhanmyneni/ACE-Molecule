#pragma once

/**
 * @brief Bessel functions
 * @author Sungwoo Kang
 * @date 2019
 * @version 0.0.1
 **/
namespace Bessel{
    //Spherical_Harmonics();
    /**
     * @brief Spherical bessel function of first kind.
     * @param x x value.
     * @param l Angular momentum.
     **/
    double jn(double x, int l);
    /**
     * @brief Spherical bessel function of second kind.
     * @param x x value.
     * @param l Angular momentum.
     **/
    double yn(double x, int l);
};
