#pragma once

/**
 * @brief Spherical harmonics
 * @details Return spherical harmonics.
 * @author Sungwoo Kang
 * @date 2015
 * @version 0.0.1
 **/
namespace Spherical_Harmonics{
    //Spherical_Harmonics();
    /**
     * @brief Get spherical harmonics.
     * @param l Angular momentum of spherical harmonics.
     * @param m Magnetic moment of spherical harmonics.
     * @param x x position.
     * @param y y position.
     * @param z z position.
     * @return Spherical harmonics value corresponding to given input.
     * @note See rlYlm(): Actual computation routine.
     **/
    double Ylm(int l,int m,double x,double y,double z);
    /**
     * @brief Get spherical harmonics * r^l.
     * @param l Angular momentum of spherical harmonics.
     * @param m Magnetic moment of spherical harmonics.
     * @param x x position.
     * @param y y position.
     * @param z z position.
     * @return Spherical harmonics * r^l, corrsponding to given input.
     **/
    double rlYlm(int l,int m,double x,double y,double z);

    /**
     * @brief Integrate multiplication of three real spherical harmonics.
     * @param l1 Angular momentum of spherical harmonics 1.
     * @param l2 Angular momentum of spherical harmonics 2.
     * @param l3 Angular momentum of spherical harmonics 3.
     * @param m1 Magnetic moment of spherical harmonics 1.
     * @param m2 Magnetic moment of spherical harmonics 2.
     * @param m3 Magnetic moment of spherical harmonics 3.
     * @return Integration of multiplication of three real spherical harmonics.
     * @note See YYY_integrate(): Computation routine.
     **/
    double real_YYY_integrate(int l1, int l2, int l3, int m1, int m2, int m3);
    /**
     * @brief Integrate multiplication of three spherical harmonics.
     * @param l1 Angular momentum of spherical harmonics 1.
     * @param l2 Angular momentum of spherical harmonics 2.
     * @param l3 Angular momentum of spherical harmonics 3.
     * @param m1 Magnetic moment of spherical harmonics 1.
     * @param m2 Magnetic moment of spherical harmonics 2.
     * @param m3 Magnetic moment of spherical harmonics 3.
     * @return Integration of multiplication of three spherical harmonics.
     * @note See Wigner_3j(): Actual computation routine.
     **/
    double YYY_integrate(int l1, int l2, int l3, int m1, int m2, int m3);
    //double Wigner_3j(int l1, int l2, int l3, int m1, int m2, int m3);
    /**
     * @brief Integrate multiplication of three spherical harmonics.
     * @param l1 Angular momentum of spherical harmonics 1.
     * @param l2 Angular momentum of spherical harmonics 2.
     * @param l3 Angular momentum of spherical harmonics 3.
     * @param m1 Magnetic moment of spherical harmonics 1.
     * @param m2 Magnetic moment of spherical harmonics 2.
     * @param m3 Magnetic moment of spherical harmonics 3.
     * @return Wigner_3j coefficient.
     * @note Input coefficients are long long int instead of int, because it is required during factorial calculations.
     * @todo Consider arXiv:1504.08329 to make this faster.
     **/
    double Wigner_3j(long long int l1, long long int l2, long long int l3, long long int m1, long long int m2, long long int m3);
};

//int factorial(int n);
