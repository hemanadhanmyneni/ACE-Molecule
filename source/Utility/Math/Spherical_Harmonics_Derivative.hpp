#pragma once

namespace Spherical_Harmonics{
/**
 * @brief Derivative of spherical harmonics.
 * @details Return derivative of spherical harmonics.
 * @author Sungwoo Kang
 * @date 2015
 **/
namespace Derivative{
    //Spherical_Harmonics_Derivative();
    /**
     * @brief Get spherical harmonics.
     * @param l Angular momentum of spherical harmonics.
     * @param m Magnetic moment of spherical harmonics.
     * @param x x position.
     * @param y y position.
     * @param z z position.
     * @param d Differentiating direction; x:1, y:2, z:3.
     * @return Derivative of spherical harmonics corresponding to given input.
     * @note See drlYlm(): Actual computation routine.
     **/
    double dYlm(int l,int m,double x,double y,double z,int d);
    /**
     * @brief Get spherical harmonics * r^l.
     * @param l Angular momentum of spherical harmonics.
     * @param m Magnetic moment of spherical harmonics.
     * @param x x position.
     * @param y y position.
     * @param z z position.
     * @param d Differentiating direction; x:1, y:2, z:3.
     * @return Derivative of spherical harmonics * r^l, corrsponding to given input.
     **/
    double drlYlm(int l,int m,double x,double y,double z,int d);
};
};
