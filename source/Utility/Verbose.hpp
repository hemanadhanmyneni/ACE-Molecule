#pragma once
#include <iostream>
#include <streambuf>
#include <cstddef>
/**
    @brief  This class takes charge of printing output message. It is designed as singleton structure.
    @author Sunghwan Choi
*/
class Verbose{
    public :
        //static std::ostream& instance();
        /**
         * @detail
         * 0 : Simple
         * 1 : Simple & Nomal: 1
         * 2 : Simple, Normal, and Detail
         * When you set VerboseLevel 1 in input, Simple and Normal cases will be printed
         **/
        enum Tag {Simple, Normal, Detail};
        /**
         * @detail
         * 0: Default. fixed point
         * 1: Energy. Width 15, precision 10, fixed.
         * 2: Occupation. Precision 3, fixed.
         * 3: Time. Precision 4, fixed.
         * 4: Position. Precision 6, fixed.
         * 5: Scientific. Scientific.
         **/
        enum NumFormat {Default, Energy, Occupation, Time, Pos, Scientific};
        static void construct(std::ostream* os = NULL,int verbose_level=1);
        static void free();

        /**
         * @brief This function prints followings on only master processor
         * @param [in] verbose It is a level of verbosity. Read explanations of Verbose::Tag
         * @hidecallergraph
         * @hidecallgraph
         */
        static std::ostream& single(int verbose=0);
        /**
            @brief This function prints followings on all processors
            @param [in] verbose It is a level of verbosity. Read explanations of Verbose::Tag
         * @hidecallergraph
         * @hidecallgraph
         */
        static std::ostream& all(int verbose=0);
        /**
            @brief This function prints followings on master processors of all groups
            @param [in] verbose It is a level of verbosity. Read explanations of Verbose::Tag
         * @hidecallergraph
         * @hidecallgraph
         */
        static std::ostream& master_group(int verbose =0);
        /**
         * @brief This function prints followings on processors belonging to master groups
         * @param [in] verbose It is a level of verbosity. Read explanations of Verbose::Tag
         * @hidecallergraph
         * @hidecallgraph
         */
        static std::ostream& master_rank(int verbose =0);
        //static int get_my_pid();
        //static int get_my_group_id();
        //static void set_ostream(std::ostream& os);
        //static std::ostream* view_printer(){return singleton_instance->out;};
        // Tried to add total mpi rank before all Verbose::debug() output. See https://stackoverflow.com/questions/23688447/inserting-text-before-each-line-using-stdostream.
        //static std::ostream& debug();

        /**
         * @brief Change numeric format
         * @param [in] Number format. See Verbose::NumFormat.
         * @hidecallergraph
         * @hidecallgraph
         */
        static void set_numformat(int num_format = Verbose::Normal);
        /**
         * @brief Get current verbosity level.
         * @hidecallergraph
         * @hidecallgraph
         */
        static int get_verbose_level();
    private:
        /**
            @brief constructor
            @param [in] os ostream object
            @param [in] verbose_level verbose level
        */
        Verbose(std::ostream* os, const int verbose_level): out(os),verbose_level(verbose_level){};
        std::ostream* out;

        /// If verbose level given in print function is smaller than or equal to this variable, it is printed
        const int verbose_level;
        static Verbose* singleton_instance;
};

/**
 * @brief Linux tee-style stream buffer: merge two streams to one.
 * @details See http://wordaligned.org/articles/cpp-streambufs.
 **/
class TeeStreamBuf: public std::streambuf{
    public:
        /**
         * @brief Constructor
         * @param sb1: std::streambuf 1.
         * @param sb2: std::streambuf 2.
         **/
        TeeStreamBuf(std::streambuf *sb1, std::streambuf *sb2):sb1(sb1), sb2(sb2){};
    protected:
        virtual int overflow(int c);
        virtual int sync();
    private:
        std::streambuf *sb1;
        std::streambuf *sb2;
};

/**
 * @brief Linux tee-style stream: merge two streams to one.
 * @details See http://wordaligned.org/articles/cpp-streambufs.
 **/
class TeeStream: public std::ostream{
    public:
        /**
         * @brief Constructor.
         * @param o1 Stream to merge 1.
         * @param o2 Stream to merge 2.
         **/
        TeeStream(std::ostream & o1, std::ostream & o2);
    private:
        TeeStreamBuf tbuf;
};
