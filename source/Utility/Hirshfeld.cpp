#include "Hirshfeld.hpp"
#include "ACE_Config.hpp"
#include "Verbose.hpp"

void Hirshfeld::hirshfeld_charge(
        Teuchos::Array< Teuchos::RCP<const Epetra_Vector> > total_density,
        Teuchos::RCP<Epetra_MultiVector> atomic_density,
        Teuchos::RCP<Epetra_MultiVector>& HB_charge_density
      ){
    int NumMyElements = atomic_density->Map().NumMyElements();
    int* MyGlobalElements = atomic_density->Map().MyGlobalElements();

    Teuchos::RCP<Epetra_Vector> atomic_density_sum = Teuchos::rcp(new Epetra_Vector(atomic_density->Map()));
    Teuchos::RCP<Epetra_Vector> total_density_sum = Teuchos::rcp(new Epetra_Vector(atomic_density->Map()));


    for(int i=0; i<total_density.size(); i++)
        total_density_sum->Update(1.0, *total_density[i], 1.0);
    double norm = 0.0;
    total_density_sum->Norm1(&norm);
    Verbose::single(Verbose::Detail) << "1norm of total density : " << norm << std::endl;
    for(int i=0; i<atomic_density->NumVectors(); i++){
        atomic_density_sum->Update(1.0, *atomic_density->operator()(i), 1.0);
    }
    //Teuchos::RCP<Epetra_Vector> diff = Teuchos::rcp(new Epetra_Vector(*total_density_sum));
    //diff->Update(1.0, *atomic_density_sum, -1.0);


    for(int i=0; i<atomic_density->NumVectors(); i++){
#ifdef ACE_HAVE_OMP
#pragma omp parallel for
#endif
        for(int j=0; j< NumMyElements;j++){
            HB_charge_density->operator[](i)[j] = total_density_sum->operator[](j) * atomic_density->operator[](i)[j]/ atomic_density_sum->operator[](j);
            //HB_charge_density->operator[](i)[j] = total_density_sum->operator[](j) * atomic_density->operator[](i)[j]/ atomic_density_sum->operator[](j);
        }
    }
    /*
    for(int i=0; i<atomic_density->NumVectors(); i++){
        HB_charge_density->operator()(i)->Update(1.0, *atomic_density->operator()(i), 1.0);
                //HB_charge_density->operator[](i)[j] = total_density_sum->operator[](j) * atomic_density->operator[](i)[j]/ atomic_density_sum->operator[](j);
    }
    */
    return ;
}
