#pragma once
#include "Teuchos_RCP.hpp"

#include "Epetra_Vector.h"
#include "../../Basis/Basis.hpp"
#include "../Value_Coef.hpp"

namespace Integration{
    double integrate(Teuchos::RCP<const Basis> basis, Teuchos::RCP<Epetra_Vector> vector, bool is_value);
    double integrate(Teuchos::RCP<const Basis> basis, Teuchos::RCP<Epetra_MultiVector> vector, bool is_value, int index);
    double integrate(Teuchos::RCP<const Basis> basis, double* vector1, bool is_value1);
    
    double integrate(
            Teuchos::RCP<const Basis> basis, 
            Teuchos::RCP<const Epetra_MultiVector> vector1, 
            bool is_value1, int index1, 
            Teuchos::RCP<const Epetra_MultiVector> vector2, 
            bool is_value2, int index2);


    double integrate(Teuchos::RCP<const Basis> basis, 
            Teuchos::RCP<Epetra_Vector> vector1, bool is_value1, 
            Teuchos::RCP<Epetra_Vector> vector2, bool is_value2);


    double integrate(Teuchos::RCP<const Basis> basis,
            double* vector1, bool is_value1, double* vector2, bool is_value2);

}
