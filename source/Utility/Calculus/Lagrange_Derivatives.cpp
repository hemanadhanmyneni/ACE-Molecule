#include "Lagrange_Derivatives.hpp"
#include "../Value_Coef.hpp"
#include "../Verbose.hpp"
#include "../Parallel_Manager.hpp"
#include "../Parallel_Util.hpp"
#ifdef USE_CUDA
#include "../Time_Measure.hpp"
#include "../CUDA_Lagrange_Derivatives.hpp"
using namespace CUDA_Lagrange_Derivatives;
#endif

using Teuchos::RCP;
using Teuchos::Array;

#ifdef USE_CUDA
namespace Lagrange_Derivatives{
    double** first_der_matrices;
}
#endif

void Lagrange_Derivatives::gradient(RCP<const Basis> basis, RCP<const Epetra_Vector> f, Array< RCP<Epetra_Vector> >& grad_f, bool is_value){
    // copy inputs
    Array< RCP<Epetra_MultiVector> > new_grad_f;
    for (int i=0; i<grad_f.size(); i++){
        new_grad_f.append(grad_f[i]);
    }
    // run
    gradient(basis, f, new_grad_f, is_value);

    // copy results using dynamic_cast
    grad_f.clear();
    for (int i =0; i< new_grad_f.size(); i++){
        grad_f.append( Teuchos::rcp_dynamic_cast<Epetra_Vector> ( new_grad_f[i] ) );
    }
    return;
}
void Lagrange_Derivatives::gradient(RCP<const Basis> basis, RCP<const Epetra_MultiVector> f, Array< RCP<Epetra_MultiVector> >& grad_f, bool is_value){
    int NumMyElements = f->Map().NumMyElements();
    int* MyGlobalElements = f->Map().MyGlobalElements();

    if(grad_f.size() != 3){
        Verbose::all() << "Lagrange_Derivatives::gradient - The size of grad_f should be 3." << std::endl;
        Verbose::all() << "Lagrange_Derivatives::current size="<<grad_f.size() << std::endl;
        exit(EXIT_FAILURE);
    }

    Teuchos::RCP<Epetra_MultiVector> f_coeff;
    Value_Coef::Value_Coef(basis,f,is_value,false,f_coeff);

    auto points = basis->get_points();
/*
    int stencil_x,stencil_y,stencil_z;
    basis->get_stencil(&stencil_x,&stencil_y,&stencil_z);

    stencil_x=(stencil_x-1)/2;
    stencil_y=(stencil_y-1)/2;
    stencil_z=(stencil_z-1)/2;
*/
    for(int alpha=0; alpha<f_coeff->NumVectors(); alpha++){
        int Col = basis->get_original_size();

        double* tmpcoef = new double [Col];
        double* totcoef = new double [Col];
        memset(tmpcoef, 0.0, sizeof(double)*Col);
        memset(totcoef, 0.0, sizeof(double)*Col);

        #ifdef ACE_HAVE_OMP
        #pragma omp parallel for schedule(runtime)
        #endif
        for(int i=0; i<NumMyElements; i++){
            tmpcoef[MyGlobalElements[i]] = f_coeff->operator[](alpha)[i];
        }
        Parallel_Util::group_sum(tmpcoef, totcoef, Col);

        int i_x=0, i_y=0, i_z=0;
        #ifdef ACE_HAVE_OMP
        #pragma omp parallel for schedule(runtime) private(i_x,i_y,i_z)
        #endif
        for(int i=0; i<NumMyElements; i++){
            int i_x,i_y,i_z;
            basis->decompose(MyGlobalElements[i],i_x,i_y,i_z);
            double x, y, z;
            basis->get_position(MyGlobalElements[i],x,y,z);


            double bx = basis->compute_1d_basis(i_x,x,0);
            double by = basis->compute_1d_basis(i_y,y,1);
            double bz = basis->compute_1d_basis(i_z,z,2);

            double tmp = 0.0;
            for(int a=0; a<points[0]; a++){
                int ind = basis->combine(a,i_y,i_z);
                if(ind >= 0) tmp += totcoef[ind] * basis->compute_first_der(a,x,0);
            }
            tmp *= by * bz;
            grad_f[0]->ReplaceMyValue(i, alpha, tmp);

            tmp = 0.0;
            for(int b=0; b<points[1]; b++){
                int ind = basis->combine(i_x, b, i_z);
                if(ind >= 0) tmp += totcoef[ind] * basis->compute_first_der(b,y,1);
            }
            tmp *= bx * bz;
            grad_f[1]->ReplaceMyValue(i, alpha, tmp);

            tmp = 0.0;
            for(int c=0; c<points[2]; c++){
                int ind = basis->combine(i_x, i_y, c);
                if(ind >= 0) tmp += totcoef[ind] * basis->compute_first_der(c,z,2);
            }
            tmp *= bx * by;
            grad_f[2]->ReplaceMyValue(i, alpha, tmp);
        }

        delete[] tmpcoef;
        delete[] totcoef;
    }
    return;
}

void Lagrange_Derivatives::divergence(RCP<const Basis> basis, Array< RCP<Epetra_MultiVector> > f, bool is_value, Array< RCP<Epetra_Vector> >& div_f, int NGPU ){
    RCP<const Epetra_Map> map = basis->get_map();
    const int NumMyElements = map->NumMyElements();
    int* MyGlobalElements = map->MyGlobalElements();
    int size = basis->get_original_size();
    const double** scaled_grid = basis->get_scaled_grid();
    if(f[0]->NumVectors() != 3){
        Verbose::all() << "Lagrange_Derivatives::divergence - The size of input array should be 3." << std::endl;
        exit(EXIT_FAILURE);
    }

    Array< RCP<Epetra_MultiVector> > f_coeff;

    if(is_value == true){
        for(int i_spin=0; i_spin<f.size(); i_spin++){
            f_coeff.push_back(Teuchos::rcp(new Epetra_MultiVector(*f[i_spin])));
            Value_Coef::Value_Coef(basis, f[i_spin], true, false, f_coeff[i_spin]);
        }
    }
    else{
        for(int i_spin=0; i_spin<f.size(); i_spin++){
            f_coeff.push_back(Teuchos::rcp(new Epetra_MultiVector(*f[i_spin])));
        }
    }

    auto points = basis->get_points();
    if (NGPU==0){
    /*
        int stencil_x,stencil_y,stencil_z;
        basis->get_stencil(&stencil_x,&stencil_y,&stencil_z);

        stencil_x=(stencil_x-1)/2;
        stencil_y=(stencil_y-1)/2;
        stencil_z=(stencil_z-1)/2;
    */
        for(int i_spin=0; i_spin<f.size(); i_spin++){
            Parallel_Manager::info().all_barrier();
            clock_t st, et;
            st = clock();

            double** tmpcoef = new double* [3];
            double** totcoef = new double* [3];

            for(int axis=0; axis<3; axis++){
                tmpcoef[axis] = new double [size];
                totcoef[axis] = new double [size];
                memset(tmpcoef[axis], 0.0, sizeof(double)*size);
                memset(totcoef[axis], 0.0, sizeof(double)*size);
                #ifdef ACE_HAVE_OMP
                #pragma omp parallel for schedule(runtime)
                #endif
                for(int j=0; j<NumMyElements; j++){
                    tmpcoef[axis][MyGlobalElements[j]] = f_coeff[i_spin]->operator[](axis)[j];
                }
                Parallel_Util::group_sum(tmpcoef[axis], totcoef[axis], size);
            }

            int i_x=0, i_y=0, i_z=0;
            #ifdef ACE_HAVE_OMP
            #pragma omp parallel for schedule(runtime) private(i_x,i_y,i_z)
            #endif
            for(int i=0; i<NumMyElements; i++){
                int i_x,i_y,i_z;
                basis->decompose(MyGlobalElements[i],i_x,i_y,i_z);
                double x, y, z;
                basis->get_position(MyGlobalElements[i],x,y,z);
               

                double bx = basis->compute_1d_basis(i_x,x,0);
                double by = basis->compute_1d_basis(i_y,y,1);
                double bz = basis->compute_1d_basis(i_z,z,2);

                double ret = 0.0;

                double tmp = 0.0;
                for(int a=0; a<points[0]; a++){
                //for(int a=i_x-stencil_x; a<i_x+stencil_x; a++)
                    int ind = basis->combine(a,i_y,i_z);
                    if(ind >= 0){
                        tmp += totcoef[0][ind] * basis->compute_first_der(a,x,0);
                    }
                }
                tmp *= by * bz;
                ret += tmp;

                tmp = 0.0;
                for(int b=0; b<points[1]; b++){
                    int ind = basis->combine(i_x,b,i_z);
                    if(ind >= 0){
                        tmp += totcoef[1][ind] * basis->compute_first_der(b,y,1);
                    }
                }
                tmp *= bx * bz;
                ret += tmp;

                tmp = 0.0;
                for(int c=0; c<points[2]; c++){
                    int ind = basis->combine(i_x,i_y,c);
                    if(ind >= 0){
                        tmp += totcoef[2][ind] * basis->compute_first_der(c,z,2);
                    }
                }
                tmp *= bx * by;
                ret += tmp;

                div_f[i_spin]->ReplaceMyValue(i, 0, ret);
            }

            for(int axis=0; axis<3; axis++){
                delete[] tmpcoef[axis];
                delete[] totcoef[axis];
            }
            delete[] tmpcoef;
            delete[] totcoef;
        }
    }

    #ifdef USE_CUDA
    else{
        cudaError_t error;
        const int total_size = size;
        double** input = new double*[3];
        double* output = new double[total_size];
        double* total_output = new double[total_size];
        for(int i =0; i<3; i++){
            input[i] = new double[total_size];
        }
        if ( gpu_memory_allocated==false){
            Teuchos::RCP<Time_Measure> timer = Teuchos::rcp(new Time_Measure());
            Parallel_Manager::info().assign_gpus(NGPU,Parallel_Manager::Exx);
            timer->start("Initialize");
            d_input_fold1 = new double*[points[2]];
            d_input_fold2 = new double*[points[2]];
            d_input_fold3 = new double*[points[1]];
            dT_gamma = new double*[points[2]];
            dT_gamma2 = new double*[points[2]];
            dT_beta = new double*[points[1]];
            cudaDeviceSynchronize();
            if (cudaSuccess!=cudaGetLastError()){
                std::cout << "1 end :  "<< cudaGetErrorString(cudaGetLastError()) ;
                exit(-1);
            }

            cudaMalloc((void**)& d_input1,        total_size*sizeof(double));
            cudaMalloc((void**)& d_input2,        total_size*sizeof(double));
            cudaMalloc((void**)& d_input3,        total_size*sizeof(double));

            cudaMalloc((void**)& d_input_fold1_dev, points[2]*sizeof(double*));
            cudaMalloc((void**)& d_input_fold2_dev, points[2]*sizeof(double*));
            cudaMalloc((void**)& d_input_fold3_dev, points[1]*sizeof(double*));

            cudaMalloc((void**)& dT_gamma_dev, points[2]*sizeof(double*));
            cudaMalloc((void**)& dT_gamma2_dev, points[2]*sizeof(double*));
            cudaMalloc((void**)& dT_beta_dev, points[1]*sizeof(double*));

            cudaMalloc((void**)& dFx,  points[0]*points[0]*sizeof(double));
            cudaMalloc((void**)& dFy,  points[1]*points[1]*sizeof(double));
            cudaMalloc((void**)& dFz,  points[2]*points[2]*sizeof(double));
            cudaMalloc((void**)& dFx_dev, points[2]*sizeof(double*));
            cudaMalloc((void**)& dFy_dev, points[2]*sizeof(double*));
            cudaMalloc((void**)& dFz_dev, points[1]*sizeof(double*));
            cudaDeviceSynchronize();
            if (cudaSuccess!=cudaGetLastError()){
                std::cout << "2 end :  "<< cudaGetErrorString(cudaGetLastError()) ;
                exit(-1);
            }
            double** dFxs = new double*[points[2]];
            double** dFys = new double*[points[2]];
            double** dFzs = new double*[points[1]];

            for(int i =0; i<points[2]; i++){
                dFxs[i] = dFx;
                dFys[i] = dFy;
            }
            for(int i =0; i<points[1]; i++){
                dFzs[i] = dFz;
            }
            cudaMemcpy(dFx_dev,  dFxs,  points[2]*sizeof(double*), cudaMemcpyHostToDevice);
            cudaMemcpy(dFy_dev,  dFys,  points[2]*sizeof(double*), cudaMemcpyHostToDevice);
            cudaMemcpy(dFz_dev,  dFzs,  points[1]*sizeof(double*), cudaMemcpyHostToDevice);

            delete[] dFxs;
            delete[] dFys;
            delete[] dFzs;

    //        #ifdef ACE_HAVE_OMP
    //        #pragma omp parallel for
    //        #endif
            for(int i=0; i<points[2]; i++){
                cudaMalloc((void**)& d_input_fold1[i], points[0]*points[1]*sizeof(double));
                cudaMemset( d_input_fold1[i],0, points[0]*points[1]*sizeof(double));
                cudaMalloc((void**)& d_input_fold2[i], points[0]*points[1]*sizeof(double));
                cudaMemset( d_input_fold2[i],0, points[0]*points[1]*sizeof(double));
                cudaMalloc((void**)& dT_gamma[i],  points[0]*points[1]*sizeof(double));
                cudaMalloc((void**)& dT_gamma2[i],  points[0]*points[1]*sizeof(double));
            }

    //        #ifdef ACE_HAVE_OMP
    //        #pragma omp parallel for
    //        #endif
            for(int j=0;j<points[1];j++){
                cudaMalloc((void**)& d_input_fold3[j], points[0]*points[2]*sizeof(double));
                cudaMemset( d_input_fold3[j],0, points[0]*points[2]*sizeof(double));
                cudaMalloc((void**)& dT_beta[j],  points[0]*points[2]*sizeof(double));
            }
            cudaDeviceSynchronize();
            cudaError_t test = cudaGetLastError();
            if (cudaSuccess!=test){
                std::cout << "4 end :  "<< cudaGetErrorString(test)<<std::endl ;
                exit(-1);
            }

            cudaMemcpy(d_input_fold1_dev, d_input_fold1, points[2]*sizeof(double*), cudaMemcpyHostToDevice);
            cudaMemcpy(d_input_fold2_dev, d_input_fold2, points[2]*sizeof(double*), cudaMemcpyHostToDevice);
            cudaMemcpy(d_input_fold3_dev, d_input_fold3, points[1]*sizeof(double*), cudaMemcpyHostToDevice);

            cudaMemcpy(dT_gamma_dev, dT_gamma, points[2]*sizeof(double*), cudaMemcpyHostToDevice);
            cudaMemcpy(dT_gamma2_dev, dT_gamma2, points[2]*sizeof(double*), cudaMemcpyHostToDevice);
            cudaMemcpy(dT_beta_dev, dT_beta, points[1]*sizeof(double*), cudaMemcpyHostToDevice);

            cudaDeviceSynchronize();
            test = cudaGetLastError();
            if (cudaSuccess!=test){
                std::cout << "4 memcpy:  "<< cudaGetErrorString(test) ;
                exit(-1);
            }
            timer->end("Initialize");
            Verbose::single(Verbose::Normal)<<"Lagrange_Derivatives:: time to initialization for CUDA:  " << timer->get_elapsed_time("Initialize",-1)<<"s" <<std::endl;

            timer->start("generate &transfer from_basic and (i_xs,i_ys,i_zs)");
            const int points_ = points[2]*points[1]*points[0];
            int* i_xs = new int[total_size];
            int* i_ys = new int[total_size];
            int* i_zs = new int[total_size];
            int* from_basic = new int[points_];

            cudaMalloc((void**)& di_xs,  total_size*sizeof(int));
            cudaMalloc((void**)& di_ys,  total_size*sizeof(int));
            cudaMalloc((void**)& di_zs,  total_size*sizeof(int));
            cudaMalloc((void**)& d_from_basic, points_*sizeof(int));
            cudaDeviceSynchronize();
            if (cudaSuccess!=cudaGetLastError()){
                std::cout << "5 memset:  "<< cudaGetErrorString(cudaGetLastError()) ;
                exit(-1);
            }

            #ifdef ACE_HAVE_OMP
            #pragma omp parallel for
            #endif
            for(int i=0; i<total_size; i++){
                basis->decompose(i,i_xs[i],i_ys[i],i_zs[i]);
            }

            int i_z; int i_y; int i_x;
            #ifdef ACE_HAVE_OMP
            #pragma omp parallel for private(i_z,i_y,i_x)
            #endif
            for(int i =0; i<points_; i++){
                i_x = i%points[0];
                i_y = (i/points[0])%points[1];
                i_z = (i/points[0])/points[1];
                from_basic[i] = basis->combine(i_x,i_y,i_z);
            }


            cudaMemcpy(di_xs, i_xs, total_size*sizeof(int), cudaMemcpyHostToDevice);
            cudaMemcpy(di_ys, i_ys, total_size*sizeof(int), cudaMemcpyHostToDevice);
            cudaMemcpy(di_zs, i_zs, total_size*sizeof(int), cudaMemcpyHostToDevice);
            cudaMemcpy(d_from_basic, from_basic, points_*sizeof(int), cudaMemcpyHostToDevice);
            delete[] from_basic;
            delete[] i_xs; delete[] i_ys; delete[] i_zs;
            cudaDeviceSynchronize();
            if (cudaSuccess!=cudaGetLastError()){
                std::cout << "end:  "<< cudaGetErrorString(cudaGetLastError()) ;
                exit(-1);
            }

            timer->end("generate &transfer from_basic and (i_xs,i_ys,i_zs)");

            Verbose::single(Verbose::Normal)<<"Lagrange_Derivatives:: elapsed time to generate & transfer from_basic and (i_xs,i_ys,i_zs):  " << timer->get_elapsed_time("generate &transfer from_basic and (i_xs,i_ys,i_zs)",-1)<<"s" <<std::endl;

            timer->start("build & transfer first der matrices");
            first_der_matrices = new double*[3];
            for (int axis =0; axis<3; axis++){
                first_der_matrices[axis] = new double[points[axis]*points[axis]];
                for(int i =0; i<points[axis]; i++){
                    for(int j=i+1; j<points[axis]; j++){
                        first_der_matrices[axis][i*points[axis]+j] = basis->compute_first_der(i,scaled_grid[axis][j],axis);
                        first_der_matrices[axis][j*points[axis]+i] = -1*first_der_matrices[axis][i*points[axis]+j];
                    }
                    //first_der_matrices[axis][i*points[axis]+i] = basis->compute_first_der(i,scaled_grid[axis][i],axis);
                    first_der_matrices[axis][i*points[axis]+i] = 0;
                }
            }
            cudaMemcpy(dFx,first_der_matrices[0], points[0]*points[0]*sizeof(double), cudaMemcpyHostToDevice);
            cudaMemcpy(dFy,first_der_matrices[1], points[1]*points[1]*sizeof(double), cudaMemcpyHostToDevice);
            cudaMemcpy(dFz,first_der_matrices[2], points[2]*points[2]*sizeof(double), cudaMemcpyHostToDevice);
            for(int axis=0; axis<3; axis++){
                delete[] first_der_matrices[axis];
            }
            delete[] first_der_matrices;
            timer->end("build & transfer first der matrices");
            Verbose::single(Verbose::Normal)<<"Lagrange_Derivatives:: build & transfer first der matrices:  " << timer->get_elapsed_time("build & transfer first der matrices",-1)<<"s" <<std::endl;

            gpu_memory_allocated=true;

            cudaDeviceSynchronize();
            if (cudaSuccess!=cudaGetLastError()){
                std::cout << "alloc end :  "<< cudaGetErrorString(cudaGetLastError()) ;
                exit(-1);
            }
        }


        int start_z,end_z,start_y,end_y;
        Epetra_Map map_z(points[2],0,*Parallel_Manager::info().get_group_comm());
        Epetra_Map map_y(points[1],0,*Parallel_Manager::info().get_group_comm());
        start_z = map_z.MyGlobalElements()[0];
        start_y = map_y.MyGlobalElements()[0];
        end_z   = start_z+ map_z.NumMyElements();
        end_y   = start_y+ map_y.NumMyElements();

        cudaDeviceSynchronize();
        if (cudaSuccess!=cudaGetLastError()){
            std::cout << "before memset:  "<< cudaGetErrorString(cudaGetLastError()) ;
            exit(-1);
        }


        for (int i_spin=0; i_spin<f.size(); i_spin++){
            for(int axis=0; axis<3; axis++){
                memset(input[axis], 0, sizeof(double)*total_size);
            }
//            std::cout << "f_coeff[i_spin] " <<std::endl;
//            std::cout << *f_coeff[i_spin ] <<std::endl;
            Parallel_Util::group_allgather_multivector(f_coeff[i_spin],input);
            Parallel_Manager::info().group_barrier();

            divergence_gpu(basis->get_original_size(), basis->get_points(), basis->get_scaling(),
                        start_z,end_z, start_y, end_y,
                        input[0],input[1],input[2],
                        output );
            Verbose::single(Verbose::Detail) << i_spin << "th spin, start group sum " <<std::endl;
            Parallel_Util::group_sum(output, total_output, total_size);
//            Parallel_Util::group_sum(output[1], output[1], total_size);
//            Parallel_Util::group_sum(output[2], output[2], total_size);
            Verbose::single(Verbose::Detail) << i_spin << "th spin, for statement " <<std::endl;
            #ifdef ACE_HAVE_OMP
            #pragma omp parallel for
            #endif
            for(int i=0; i<NumMyElements; i++){
                div_f[i_spin]->ReplaceMyValue(i, 0, total_output[MyGlobalElements[i]]);
            }
        }

        for (int axis=0; axis<3; axis++){
            delete[] input[axis];
        }
        delete[] output;
        delete[] input;
    }
    #else
    else{
        Verbose::all() << "Divergence, ACE-Molecule is not installed with CUDA but NGPU is not zero but " << NGPU << "!" << std::endl;
        exit(-1);
    }
    #endif
    //std::cout << *div_f[0] <<std::endl;
    //exit(-1);
    return;
}

void Lagrange_Derivatives::laplacian(RCP<const Basis> basis, RCP<Epetra_MultiVector> f, RCP<Epetra_MultiVector> lapl_f, bool is_value){
    int NumMyElements = lapl_f->Map().NumMyElements();
    int* MyGlobalElements = lapl_f->Map().MyGlobalElements();
    const int size = basis->get_original_size();
    //const double** scaled_grid = mesh->get_scaled_grid();

    Teuchos::RCP<Epetra_MultiVector> f_coeff;

    Value_Coef::Value_Coef(basis,f,is_value,false,f_coeff);
/*
    if(is_value == true){
        f_coeff = Teuchos::rcp(new Epetra_MultiVector(f->Map(), f->NumVectors()));
        Interpolation::change_basis_and_grid(basis, grid_setting, f, true, basis, grid_setting, f_coeff, false);
    }
    else{
        f_coeff = Teuchos::rcp(new Epetra_MultiVector(*f));
    }
*/
/*
    int stencil_x,stencil_y,stencil_z;
    basis->get_stencil(&stencil_x,&stencil_y,&stencil_z);

    stencil_x=(stencil_x-1)/2;
    stencil_y=(stencil_y-1)/2;
    stencil_z=(stencil_z-1)/2;
*/
    auto points = basis->get_points();
    for(int alpha=0; alpha<lapl_f->NumVectors(); alpha++){

        double* tmpcoef = new double [size];
        double* totcoef = new double [size];
        memset(tmpcoef, 0.0, sizeof(double)*size);
        memset(totcoef, 0.0, sizeof(double)*size);

        #ifdef ACE_HAVE_OMP
        #pragma omp parallel for schedule(runtime)
        #endif
        for(int j=0; j<NumMyElements; j++){
            tmpcoef[MyGlobalElements[j]] = f_coeff->operator[](alpha)[j];
        }
        Parallel_Util::group_sum(tmpcoef, totcoef, size);

        int i_x=0, i_y=0, i_z=0;
        #ifdef ACE_HAVE_OMP
        #pragma omp parallel for schedule(runtime) private(i_x,i_y,i_z)
        #endif
        for(int i=0; i<NumMyElements; i++){
            double x, y, z;
            basis->decompose(MyGlobalElements[i],i_x,i_y,i_z);
            basis->get_position(MyGlobalElements[i],x,y,z);

            const double bx = basis->compute_1d_basis(i_x,x,0);
            const double by = basis->compute_1d_basis(i_y,y,1);
            const double bz = basis->compute_1d_basis(i_z,z,2);
            
            double ret = 0.0;

            double tmp = 0.0;
            for(int a=0; a<points[0]; a++){
            //for(int a=i_x-stencil_x; a<i_x+stencil_x; a++){
                int ind = basis->combine(a,i_y,i_z);
                if(ind >= 0){
                    tmp += totcoef[ind] * basis->compute_second_der(a,x,0);
                }
            }
            tmp *= by * bz;
            ret += tmp;

            tmp = 0.0;
            for(int b=0; b<points[1]; b++){
            //for(int b=i_y-stencil_y; b<i_y+stencil_y; b++){
                int ind = basis->combine(i_x,b,i_z);
                if(ind >= 0){
                    tmp += totcoef[ind] * basis->compute_second_der(b,y,1);
                }
            }
            tmp *= bx * bz;
            ret += tmp;

            tmp = 0.0;
            //for(int c=i_z-stencil_z; c<i_z+stencil_z; c++){
            for(int c=0; c<points[2]; c++){
                int ind = basis->combine(i_x,i_y,c);
                if(ind >= 0){
                    tmp += totcoef[ind] * basis->compute_second_der(c,z,2);
                }
            }
            tmp *= bx * by;
            ret += tmp;

            lapl_f->ReplaceMyValue(i, alpha, ret);
        }

        delete[] tmpcoef;
        delete[] totcoef;
    }

    return;
}
void Lagrange_Derivatives::free(std::array<int,3> points){
    #ifdef USE_CUDA
    if(gpu_memory_allocated==true){
        for(int i=0; i<points[2]; i++){
            cudaFree(d_input_fold1[i]);
            cudaFree(d_input_fold2[i]);
            cudaFree(dT_gamma[i]);
            cudaFree(dT_gamma2[i]);
        }

        for(int j=0;j<points[1];j++){
            cudaFree(d_input_fold3[j]);
            cudaFree(dT_beta[j]);
        }

        delete[] d_input_fold1;
        delete[] d_input_fold2;
        delete[] d_input_fold3;

        cudaFree( dFx);
        cudaFree( dFy);
        cudaFree( dFz);

        cudaFree(d_input1);
        cudaFree(d_input2);
        cudaFree(d_input3);

        cudaFree(d_input_fold1_dev);
        cudaFree(d_input_fold2_dev);
        cudaFree(d_input_fold3_dev);

        cudaFree( dT_gamma_dev);
        cudaFree( dT_gamma2_dev);
        cudaFree( dT_beta_dev);

        cudaFree(di_xs);    cudaFree(di_ys);    cudaFree(di_zs);
        cudaFree(d_from_basic);

        delete[] dT_gamma;
        delete[] dT_gamma2;
        delete[] dT_beta;

        gpu_memory_allocated=false;
    }
    #endif

}

Teuchos::Array< Teuchos::RCP<Epetra_CrsMatrix> > Lagrange_Derivatives::get_gradient_matrix(Teuchos::RCP<const Basis> mesh){
    auto map = mesh->get_map();
    int NumMyElements = map->NumMyElements();
    int* MyGlobalElements = map->MyGlobalElements();

    Array< Teuchos::RCP<Epetra_CrsMatrix> > gradient_matrix;
    for(int axis=0; axis<3; axis++){
        gradient_matrix.push_back(Teuchos::rcp(new Epetra_CrsMatrix(Copy, *map, 0)));
    }

    int i_x=0, i_y=0, i_z=0;
    for(int i=0; i<NumMyElements; ++i){
        double x, y ,z;
        mesh->get_position(MyGlobalElements[i], x, y, z);
        mesh->decompose(MyGlobalElements[i], i_x, i_y, i_z);

        double bx = mesh->compute_1d_basis(i_x,x,0);
        double by = mesh->compute_1d_basis(i_y,y,1);
        double bz = mesh->compute_1d_basis(i_z,z,2);

        std::vector<int> index;
        std::vector<double> value;
        int index_size;

        for(int a=0; a<mesh->get_points()[0]; ++a){
            int ind = mesh->combine(a, i_y, i_z);
            if(ind >= 0){
                double tmp = mesh->compute_first_der(a,x,0) * by * bz;
                if(abs(tmp) > 1.0E-20){
                    index.push_back(ind);
                    value.push_back(tmp);
                }
            }
        }
        index_size = index.size();
        gradient_matrix[0]->InsertGlobalValues(MyGlobalElements[i], index_size, &value[0], &index[0]);

        index.clear();
        value.clear();

        for(int b=0; b<mesh->get_points()[1]; ++b){
            int ind = mesh->combine(i_x, b, i_z);
            if(ind >= 0){
                double tmp = mesh->compute_first_der(b,y,1) * bx * bz;
                if(abs(tmp) > 1.0E-20){
                    index.push_back(ind);
                    value.push_back(tmp);
                }
            }
        }
        index_size = index.size();
        gradient_matrix[1]->InsertGlobalValues(MyGlobalElements[i], index_size, &value[0], &index[0]);

        index.clear();
        value.clear();

        for(int c=0; c<mesh->get_points()[2]; ++c){
            int ind = mesh->combine(i_x, i_y, c);
            if(ind >= 0){
                double tmp = mesh->compute_first_der(c,z,2) * bx * by;
                if(abs(tmp) > 1.0E-20){
                    index.push_back(ind);
                    value.push_back(tmp);
                }
            }
        }
        index_size = index.size();
        gradient_matrix[2]->InsertGlobalValues(MyGlobalElements[i], index_size, &value[0], &index[0]);
    }

    gradient_matrix[0]->FillComplete();
    gradient_matrix[1]->FillComplete();
    gradient_matrix[2]->FillComplete();
    return gradient_matrix;
}
