#include "Lebedev_Quadrature.hpp"
#include <cmath>
#include "../Verbose.hpp"

using std::vector;

/* http://people.sc.fsu.edu/~jburkardt/cpp_src/sphere_lebedev_rule/sphere_lebedev_rule.html
 * 
 * References from the webpage:
 *     Axel Becke,
 *     A multicenter numerical integration scheme for polyatomic molecules,
 *     Journal of Chemical Physics,
 *     Volume 88, Number 4, 15 February 1988, pages 2547-2553.
 *
 *     Vyacheslav Lebedev, Dmitri Laikov,
 *     A quadrature formula for the sphere of the 131st algebraic order of accuracy,
 *     Russian Academy of Sciences Doklady Mathematics,
 *     Volume 59, Number 3, 1999, pages 477-481.
 *
 *     Vyacheslav Lebedev,
 *     A quadrature formula for the sphere of 59th algebraic order of accuracy,
 *     Russian Academy of Sciences Doklady Mathematics,
 *     Volume 50, 1995, pages 283-286.
 *
 *     Vyacheslav Lebedev, A.L. Skorokhodov,
 *     Quadrature formulas of orders 41, 47, and 53 for the sphere,
 *     Russian Academy of Sciences Doklady Mathematics,
 *     Volume 45, 1992, pages 587-592.
 *
 *     Vyacheslav Lebedev,
 *     Spherical quadrature formulas exact to orders 25-29,
 *     Siberian Mathematical Journal,
 *     Volume 18, 1977, pages 99-107.
 *
 *     Vyacheslav Lebedev,
 *     Quadratures on a sphere,
 *     Computational Mathematics and Mathematical Physics,
 *     Volume 16, 1976, pages 10-24.
 *
 *     Vyacheslav Lebedev,
 *     Values of the nodes and weights of ninth to seventeenth order Gauss-Markov quadrature formulae invariant under the octahedron group with inversion,
 *     Computational Mathematics and Mathematical Physics,
 *     Volume 15, 1975, pages 44-51.
 *
 */

vector< vector<double> > Lebedev_Quadrature::get_points_on_unit_sphere(){
    vector< vector<double> > pts;

    // a1: 6 points (1 0 0)
    for( int i = 0; i < 3; ++i){
        for( int j = -1; j <= 1; j+=2){
            vector<double> pt(3, 0.0);
            pt[i] = j;
            pts.push_back( pt );
        }
    }

    // a2: 12 points 1/sqrt(2) * (1 1 0)
    double constant = 0.707106781186548;
    for( int i = 0; i < 3; ++i){
        for( int j = -1; j <= 1; j+=2){
            for( int k = -1; k <= 1; k+=2){
                vector<double> pt(3, 0.0);
                pt[(i+1)%3] = j * constant;
                pt[(i+2)%3] = k * constant;
                pts.push_back( pt );
            }
        }
    }

    // a3: 8 points 1/sqrt(3) * (1 1 1)
    constant = 0.577350269189626;
    for( int i = -1; i <= 1; i+=2){
        for( int j = -1; j <= 1; j+=2){
            for( int k = -1; k <= 1; k+=2){
                vector<double> pt(3, 0.0);
                pt[0] = i * constant;
                pt[1] = j * constant;
                pt[2] = k * constant;
                pts.push_back( pt );
            }
        }
    }

    // bk: 24 points (lk lk mk); 2*lk^2+mk^2=1
    double lk = 0.301511344577764;
    double mk = sqrt(1.0 - 2.0*lk*lk);
    for( int i = 0; i < 3; ++i){
        for( int j = -1; j <= 1; j+=2){
            for( int k = -1; k <= 1; k+=2){
                for( int l = -1; l <= 1; l+=2){
                    vector<double> pt(3);
                    pt[i] = mk*j;
                    pt[(i+1)%3] = lk*k;
                    pt[(i+2)%3] = lk*l;
                    pts.push_back( pt );
                }
            }
        }
    }

/*
    // ck: 24 points (pk qk 0); pk^2+qk^2=1
    double pk = ;
    double qk = sqrt(1.0 - pk*pk);
    for( int i = 0; i < 3; ++i){
        for( int j = -1; j <= 1; j+=2){
            for( int k = -1; k <= 1; k+=2){
                vector<double> pt(3, 0.0);
                pt[(i+1)%3] = j * pk;
                pt[(i+2)%3] = k * qk;
                pts.push_back( pt );
            }
        }
    }

    // dk: 48 points (rk Sk Wk);
    double rk = ;
    double Sk = ;
    double Wk = sqrt( 1.0 - rk*rk - Sk*Sk );
    for( int i = -1; i <= 1; i+=2){
        for( int j = -1; j <= 1; j+=2){
            for( int k = -1; k <= 1; k+=2){
                vector<double> pt(3, 0.0);
                pt[0] = i * rk;
                pt[1] = j * Sk;
                pt[2] = k * Wk;
                pts.push_back( pt );
            }
        }
    }
*/
    return pts;
}

vector<double> Lebedev_Quadrature::get_weight(){
    vector<double> weight;

    // a1: 6 points (1 0 0)
    for(int i = 0; i < 6; ++i){
        weight.push_back( 0.0126984126984127 );
    }
    // a2: 12 points 1/sqrt(2) * (1 1 0)
    for(int i = 0; i < 12; ++i){
        weight.push_back( 0.0225749559082892 );
    }
    // a3: 8 points 1/sqrt(3) * (1 1 1)
    for(int i = 0; i < 8; ++i){
        weight.push_back( 0.0210937500000000 );
    }
    // bk: 24 points (lk lk mk); 2*lk^2+mk^2=1
    for(int i = 0; i < 24; ++i){
        weight.push_back( 0.0201733355379189 );
    }

/*
    // ck: 24 points (pk qk 0); pk^2+qk^2=1
    for(int i = 0; i < 24; ++i){
        weight.push_back(  );
    }
    // dk: 48 points (rk Sk Wk);
    for(int i = 0; i < 48; ++i){
        weight.push_back(  );
    }
*/
    return weight;
}

double Lebedev_Quadrature::integrate( int l, int m, double val ){
    double retval = 0.0;

    vector< vector<double> > pos = Lebedev_Quadrature::get_points_on_unit_sphere();
    vector<double> weight = Lebedev_Quadrature::get_weight();

    for(int i = 0; i < weight.size(); ++i){
        retval += weight[i] * val * Spherical_Harmonics::Ylm( l, m, pos[i][0], pos[i][1], pos[i][2] );
    }

    return retval;
}

vector<double> Lebedev_Quadrature::integrate( int l, int m, vector<double> vals ){
    vector<double> retval;

    for(int i = 0; i < vals.size(); ++i){
        retval.push_back( Lebedev_Quadrature::integrate(l, m, vals[i] ) );
    }
    return retval;
}

void Lebedev_Quadrature::print_quadruture(){
    vector< vector<double> > pos = Lebedev_Quadrature::get_points_on_unit_sphere();
    vector<double> weight = Lebedev_Quadrature::get_weight();

    for(int n = 0; n < weight.size(); ++n){
        Verbose::all() << "\t" << n << "\t" << pos[n][0] << "\t" << pos[n][1] << "\t" << pos[n][2] << "\t" << weight[n] << std::endl;
    }
}
