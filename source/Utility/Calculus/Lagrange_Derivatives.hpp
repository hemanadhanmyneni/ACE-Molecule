#pragma once
#include "Epetra_MultiVector.h"
#include "Teuchos_RCP.hpp"
#include "Teuchos_Array.hpp"

#include "../../Basis/Basis.hpp"

namespace Lagrange_Derivatives {
    /**
     * @brief Returns gradient of function, as coefficient of LSFs.
     * @param basis Basis.
     * @param f Input value.
     * @param grad_f Return value.
     * @param is_value True if f is value.
     * @return gradient value corresponding to input value (not coef, value itself)
     */
    void gradient(
        Teuchos::RCP<const Basis> basis, 
        Teuchos::RCP<const Epetra_MultiVector> f, 
        Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> >& grad_f, 
        bool is_value
    );
    void gradient(
        Teuchos::RCP<const Basis> basis, 
        Teuchos::RCP<const Epetra_Vector> f, 
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> >& grad_f, 
        bool is_value
    );
    void divergence(
        Teuchos::RCP<const Basis> basis, 
        Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > f, 
        bool is_value, 
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> >& div_f,
        int NGPU
    );
    void laplacian(
        Teuchos::RCP<const Basis> basis, 
        Teuchos::RCP<Epetra_MultiVector> f, 
        Teuchos::RCP<Epetra_MultiVector> lapl_f, 
        bool is_value
    );
    void free(std::array<int,3> points);

    Teuchos::Array< Teuchos::RCP<Epetra_CrsMatrix> > get_gradient_matrix(
        Teuchos::RCP<const Basis> mesh
    );
};

