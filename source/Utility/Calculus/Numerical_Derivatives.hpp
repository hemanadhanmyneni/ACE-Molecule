#pragma once
#include <vector>

namespace Numerical_Derivatives {
    double numerical_derivatives(double x, std::vector<double> mesh, std::vector<double> value, int order, int accuracy);
};

