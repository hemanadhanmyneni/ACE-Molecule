#include "Matrix_Multiplication.hpp"
#include <string>

using std::vector;
using std::string;

vector< vector< double> > Matrix_Multiplication::multiply(vector< vector<double> > A, vector< vector< double> > B){

    if(A[0].size() == B.size()){
        int a = A.size();
        int b = B[0].size();
        vector< vector< double> > result;
        result.resize(a);
        for(int i =0; i<a; i++){
            result[i].resize(b);
        }
        for(int i=0; i< a; i++){
            for(int j=0;j< b; j++){
                double val=0;
                for(int k=0;k<A[0].size(); k++){
                    val+= A[i][k] * B[k][j];
                }
                result[i][j] = val; //result[i][j].assign(val);
            }
        }
        return result;
    }
    else{
        //Verbose::instance()<< "Matrix_Multiplication::multipy-Wrong matrix size" <<Endl::endl;
        exit(-1);
    }
}

vector<double> Matrix_Multiplication::multiply(vector< vector<double> > A, vector<double> v){
    if(A.size() == v.size()){
        vector<double> result;
        int a = A.size();
        result.resize(a);

        for(int i=0; i<a; i++){
            double val=0;
            for(int k=0; k<a; k++){
                val += A[i][k]*v[k];
            }
            result[i] = val;
        }
        return result;
    }
    else{
        //Verbose::instance()<< "Matrix_Multiplication::multipy-Wrong matrix size" <<Endl::endl;
        exit(-1);
    }
}

vector<double> Matrix_Multiplication::multiply(vector<double> v,vector< vector<double> > A){
    if(A.size() == v.size()){
        vector<double> result;
        int a = A.size();
        result.resize(a);

        for(int i=0; i<a; i++){
            double val=0;
            for(int k=0; k<a; k++){
                val += A[k][i]*v[k];
            }
            result[i] = val;
        }
        return result;
    }
    else{
        //Verbose::instance()<< "Matrix_Multiplication::multipy-Wrong matrix size" <<Endl::endl;
        exit(-1);
    }
}

double Matrix_Multiplication::dot(vector<double> v1, vector<double> v2){
    double val=0;
    if(v1.size() == v2.size()){
        for(int i=0; i < v1.size(); i++){
            val += v1[i]*v1[i];
        }
        return val;
    }
    else{
        //Verbose::instance()<< "Matrix_Multiplication::dot-Wrong vector size" <<Endl::endl;
        exit(-1);
    }
}

vector< vector<double> > Matrix_Multiplication::outer(vector<double> v1, vector<double> v2){
    int a = v1.size();
    int b = v2.size();
    vector< vector< double> > result;
    result.resize(a);
    for(int i =0; i<a; i++){
        result.at(i).resize(b);
    }
    for(int i=0; i<a; i++){
        for(int j=0; j<b; j++){
            double tmp=0;
            tmp = v1[i]*v2[j];
            result[i][j] = tmp; //result[i][j].assign(val);
        }
    }
    return result;
}
