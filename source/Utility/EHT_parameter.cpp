#include "EHT_parameter.hpp"

#include <fstream>
#include <sstream>
#include <cmath>

#include "Math/Spherical_Harmonics.hpp"
//#include "../Io/Periodic_table.hpp"
#include "Verbose.hpp"

using std::vector;
using std::string;
using std::endl;

EHT_parameter::EHT_parameter(string filename){
    //this->number_of_atoms = sizeof(Periodic_atom::periodic_table)/sizeof(Periodic_atom::periodic_table[0]);
    //this->parameter_index;
    /*
    this->number_of_valence_electron = new int[300];
    this->number_of_exponent = new int[300];
    this->principle_quantum_number = new int[300];
    this->angular_quantum_number = new int[300];
    this->Hii = new double[300];
    this->zeta1 = new double[300];
    this->zeta2 = new double[300];
    this->coeff1 = new double[300];
    this->coeff2 = new double[300];
    */
    read_EHT_param(filename);
    this->cutoff = vector<double>(number_of_atoms+1);
    calculate_cutoff();
}

double EHT_parameter::get_orbital_energy(int atomic_number, int orbital_number){
    int list_index = parameter_index.at(atomic_number-1);
    double return_val = Hii[list_index+orbital_number];
    if(return_val>-0.00001){
        Verbose::all() << "wrong input!" << std::endl;
        exit(EXIT_FAILURE);
    }
    return return_val;
}
double EHT_parameter::get_cutoff(int atomic_number){
    return cutoff[atomic_number-1];
}

double EHT_parameter::atomic_orbital(int atomic_number, int orbital_number, double* x){
    //slater orbital is written as atomic unit(Bohr)
    //R(r) = Nr^(n-1)exp(-zeta*r)
    //n : principal quantum number
    //N = (2*zeta)^n * sqrt(2*zeta/(2n)!)
    //
    //using the predefined real spherical harmonics, Ylm(l,m,x,y,z) and rlYlm(l,m,x,y,z) = (r^l)*Ylm(l,m,x,y,z)
    //psi(n,l,m,x,y,z) = R(r)*Ylm(l,m,x,y,z) = (2*zeta)^(n+0.5)*r^(n-l-1)*rlYlm(l,m,x,y,z)*exp(-zeta*r)/sqrt((2n)!)
    //
    //ex:
    //1S = sqrt(zeta^3/pi) exp(-zeta*r)
    //2S = sqrt(zeta^5/(3pi))*r*exp(-zeta*r)
    //2Pi = sqrt(zeta^5/(pi))*i*exp(-zeta*r)
    //...
    //where i = x,y,z
    //
    //for d,f orbital, R(r)= C_1 N_1 r^(n-1) exp(-zeta_1 r) + C_2 N_2 r^(n-1) exp(-zeta_2 r)
    //
    //http://en.citizendium.org/wiki/Slater_orbital
    double r = sqrt(x[0]*x[0]+x[1]*x[1]+x[2]*x[2]);
    double return_value = 0.0;
    int list_index = parameter_index.at(atomic_number-1);
    int n, l, m;
    if(orbital_number==0){
        n = principle_quantum_number[list_index];
        l = 0;
        m = 0;
    }
    else if(1<=orbital_number && orbital_number<4){
        n = principle_quantum_number[list_index+1];
        l = 1;
        m = orbital_number-2; // 1,2,3 -> -1, 0, 1
    }
    else if(4<=orbital_number && orbital_number<9){
        n = principle_quantum_number[list_index+2];
        l = 2;
        m = orbital_number-6; // 4,5,6,7,8 -> -2,-1,0,1,2
    }
    else if(9<=orbital_number && orbital_number<16){
        n = principle_quantum_number[list_index+3];
        l = 3;
        m = orbital_number-12; // 9,10,11,... -> -3,-2,...
    }
    else{
        Verbose::all() << "wrong orbital number!" << endl;
        exit(1);
    }
    int npack=1;
    for(int a=2;a<=2*n;a++){
        npack *=a;
    }
    if(atomic_number<=0){
        Verbose::all() << "wrong atom number!" << endl;
        exit(1);
    }
    else if(number_of_exponent[list_index+l]==1){
        return_value = Spherical_Harmonics::rlYlm(l,m,x[0],x[1],x[2])*pow(2*zeta1[list_index+l],(double)n+0.5)*pow(r,n-l-1)*exp(-zeta1[list_index+l]*r)/sqrt(npack);
    }
    else if(number_of_exponent[list_index+l]==2){
        return_value = coeff1[list_index+l]*pow(2.0*zeta1[list_index+l],(double)n+0.5)*exp(-zeta1[list_index+l]*r) + coeff2[list_index+l]*pow(2*zeta2[list_index+l],(double)n+0.5)*exp(-zeta2[list_index+l]*r);
        return_value *= Spherical_Harmonics::rlYlm(l,m,x[0],x[1],x[2])*pow(r,n-l-1)/sqrt(npack);
    }
    else{
        Verbose::all() << "wrong atomic number!" << endl;
        exit(1);
    }
    return return_value;
}

double EHT_parameter::get_number_of_basis_function(int atomic_number){
    int tmp = parameter_index.at(atomic_number) - parameter_index.at(atomic_number-1);
    if(tmp == 0){
        Verbose::all() << "The extended Huckel parameter is undefined for " << this -> atom_label.at(atomic_number-1) << endl;
        exit(1);
    }
    return tmp*tmp;
}

bool EHT_parameter::is_orbital_index_ended(int atomic_number, int orbital_number){
    if(atomic_number<=0){
        Verbose::all() << "WRONG ATOMIC NUMBER(EHT_parameter::is_orbital_index_ended)" << endl;
        exit(1);
    }
    else{
        int number_of_atomic_orbital = (parameter_index.at(atomic_number) - parameter_index.at(atomic_number-1));
        number_of_atomic_orbital *= number_of_atomic_orbital; // 1,4,9,16
        if(orbital_number < 0){
            Verbose::all() << "ERROR IN FUNCITON (EHT_parameter::is_orbital_index_ended)" << endl;
            exit(1);
        }
        else if( orbital_number < number_of_atomic_orbital-1){
            return false;
        }
        else if( orbital_number == number_of_atomic_orbital -1){
            return true;
        }
        else{
            Verbose::all() << "ERROR IN FUNCITON (EHT_parmeter::is_orbital_index_ended)" << endl;
            exit(1);
        }
    }
}

int EHT_parameter::get_number_of_valence_electron(int atomic_number){
    return number_of_valence_electron.at(parameter_index[atomic_number-1]);
}

double EHT_parameter::get_zeta1(int atomic_number, int angular_momentum){
    int index = parameter_index.at(atomic_number-1)+angular_momentum;
    return zeta1[index];
}

string EHT_parameter::get_orbital_name(int atomic_number, int orbital_number){
    
    string orbital_name;
    orbital_name = this -> atom_label.at(atomic_number-1);
    char buffer[3];
    sprintf(buffer, "%d", principle_quantum_number[parameter_index.at(atomic_number-1)+orbital_number]);
    orbital_name.append(buffer);
    string angular_momentum;
    switch(orbital_number){
        case 0:        angular_momentum = "S";        break;

        case 1:        angular_momentum = "PY";    break;
        case 2:        angular_momentum = "PZ";    break;
        case 3:        angular_momentum = "PX";    break;

        case 4:        angular_momentum = "DXY";    break;
        case 5:        angular_momentum = "DZX";    break;
        case 6:        angular_momentum = "DZZ";    break;
        case 7:        angular_momentum = "DYZ";    break;
        case 8:        angular_momentum = "D(XX-YY)";    break;

        case 9:        angular_momentum = "FY(3XX-YY)";    break;
        case 10:    angular_momentum = "FXYZ";    break;
        case 11:    angular_momentum = "FYZZ";    break;
        case 12:    angular_momentum = "FZZZ";    break;
        case 13:    angular_momentum = "FXZZ";    break;
        case 14:    angular_momentum = "FZ(XX-YY)";    break;
        case 15:    angular_momentum = "FX(XX-3YY)";    break;

        case 16:    angular_momentum = "GXY(XX-YY)";    break;
        case 17:    angular_momentum = "GZYYY";    break;
        case 18:    angular_momentum = "GZZXY";    break;
        case 19:    angular_momentum = "GZZZY";    break;
        case 20:    angular_momentum = "GZZZZ";    break;
        case 21:    angular_momentum = "GZZZX";    break;
        case 22:    angular_momentum = "GZZXY";    break;
        case 23:    angular_momentum = "GZXXX";    break;
        case 24:    angular_momentum = "G(X^4+Y^4)";    break;

        default:
            Verbose::all() << "unexpected orbital number" << endl;
            exit(1);
    }
    orbital_name += angular_momentum;
    
    return orbital_name;
}

void EHT_parameter::read_EHT_param(std::string filename){
    string word;
    string Atomlab, AtNo, Nvalen, Nzeta, Nquant, Ang, IP, exp1, exp2, c1, c2;
    int line_num = -1;
    std::ifstream inputfile(filename);
    if(inputfile.fail()){
        Verbose::all() << "INVALID EXTENDED HUCKEL PARAMETER NAME: " << filename << endl;
        exit(1);
    }
    getline(inputfile,word);

    int atomic_num;
    while(inputfile.eof()==false){
        string::size_type lastPos=word.find_first_not_of(" ",0);
        if(word.find(";") == string::npos && lastPos != string::npos){
            line_num++;
            std::istringstream iss(word);
            iss >> Atomlab >> AtNo >> Nvalen >> Nzeta >> Nquant >> Ang >> IP >> exp1 >> exp2 >> c1 >> c2;
            if(c2==""){
                Verbose::all() << "INVALID FORMAT(EXTENDED HUCKEL PARAMETER)" << endl;
                exit(EXIT_FAILURE);
            }
            else{
                atomic_num = atoi(AtNo.c_str())-1;
                if(atof(exp1.c_str())<0.00001){
                    line_num--;
                }
                else{
                    //if(atomic_num >= number_of_atoms){
                    //    parameter_index.at(number_of_atoms)=line_num;
                    //  break;
                    if(atomic_num+1 >= parameter_index.size()){
                        parameter_index.resize(atomic_num+1, -1);
                        parameter_index.back() = line_num;
                    }
                    /*
                    else if(!(Periodic_atom::periodic_table[atomic_num]==Atomlab)){
                        Verbose::all() << "WRONG ATOMIC SYMBOL OR ATOMIC NUMBER" << endl;
                        exit(1);
                    }
                    */
                    //else{
                        if(parameter_index.at(atomic_num)==-1){
                            parameter_index.at(atomic_num)=line_num;
                        }
                        this -> atom_label.push_back(Atomlab);
                        number_of_valence_electron.push_back( atoi(Nvalen.c_str()) );
                        number_of_exponent.push_back( atoi(Nzeta.c_str()) );
                        principle_quantum_number.push_back( atoi(Nquant.c_str()) );
                        switch(Ang[0]){
                            case 's':
                            case 'S':
                            case '0':
                                angular_quantum_number.push_back(0);
                                break;
                            case 'p':
                            case 'P':
                            case '1':
                                angular_quantum_number.push_back(1);
                                break;
                            case 'd':
                            case 'D':
                            case '2':
                                angular_quantum_number.push_back(2);
                                break;
                            case 'f':
                            case 'F':
                            case '3':
                                angular_quantum_number.push_back(3);
                                break;
                            default: Verbose::all() << "wrong input " << endl;
                        }
                        Hii.push_back( atof(IP.c_str()) );
                        zeta1.push_back( atof(exp1.c_str()) );
                        zeta2.push_back( atof(exp2.c_str()) );
                        coeff1.push_back( atof(c1.c_str()) );
                        coeff2.push_back( atof(c2.c_str()) );
                    //}
                }
            }//if(c2=="") else{...}
        }
        getline(inputfile,word);
    }
    inputfile.close();
    this -> number_of_atoms = this -> parameter_index.size();
    parameter_index.push_back(line_num+1);
    for(int i=number_of_atoms-1;i>=0;i--){
        if(parameter_index.at(i)==-1){
            parameter_index.at(i)=parameter_index.at(i+1);
        }
    }
}

void EHT_parameter::calculate_cutoff(){
    //calculate cutoff radius for all atoms(until 38)
    //only consider largest n, smallest zeta contribution
    //R(r) <=exp(-18.5) ~ 1E-8

    for(int i=0;i<number_of_atoms;i++){
        if(parameter_index.at(i+1)-parameter_index.at(i)<0){
            Verbose::all() << "ERROR IN READ_EHT_PARAM! " << endl;
            exit(1);
        }
        else if(parameter_index.at(i+1)-parameter_index.at(i)==0){
            cutoff.at(i) = 0.0;
        }
        else{
            int n, list_index;
            if(parameter_index.at(i+1)-parameter_index.at(i)==1){
                list_index = parameter_index.at(i);  //atom have only s orbital
            }
            else{
                list_index = parameter_index.at(i)+1; //only consider p orbital, p orbital have larger principle quantum number than d,f orbital & decay slower than s orbital.
            }
            n = principle_quantum_number.at(list_index);
            double zeta = zeta1.at(list_index);
            //double coeff = coeff1.at(list_index);
            int npack = 1;
            for(int j=2;j<=2*n;j++){
                npack *= j;
            }
            cutoff.at(i) = (18.5 + ((double)n+0.5)*log(2*zeta) - 0.5*log(npack) +(double)(n-1)*3.5)/zeta;
        }
    }
}
