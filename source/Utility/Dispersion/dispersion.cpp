#include "dispersion.hpp"

using std::vector;
using std::setw;
using std::array;

using Teuchos::Array;
using Teuchos::RCP;
using Teuchos::rcp;

extern "C" {
    double dispersion_interaction(double* rxyz,int atom_num,int* atom_num_list, char* inp_functional, int f_len ,int type_return, int i_atom, int i_axis);
}
// type_return : 1 -> Energy, else : Force
// need i_atom and i_axis when only calculating force
double Dispersion_Calculation::dispersion_interaction_c(RCP<State> state, string functional, int type_return, int i_atom, int i_axis){
    double dispersion_c;
    auto atoms = state->get_atoms();
    int nat = atoms -> get_size();
    vector< array<double,3> > rxyz2 = state-> atoms() -> get_positions();
     double* rxyz = new double[nat*3];
     for(int i=0; i< nat;i++ ){
         for(int j=0; j<3; j++){
             rxyz[j*nat+i]= rxyz2.at(i)[j];
         }
     }

    vector<int> atom_num_list2 = state->atoms() -> get_atomic_numbers();
    int* atom_num_list = new int[nat];
    for(int i=0; i<nat;i++){
        atom_num_list[i] = atom_num_list2[i];
    }
    char* inp_functional = (char*) functional.c_str();
    int f_len = strlen(inp_functional);
    dispersion_c = dispersion_interaction(rxyz, nat, atom_num_list,inp_functional, f_len, type_return, i_atom, i_axis);
    return dispersion_c;

};

vector<double> Dispersion_Calculation::dispersion_forces(RCP<State> state, string functional ,int atom_num){
    vector<double> return_force;
    double force_element;
    for(int i =0; i< atom_num; i++){
        for(int j=0; j < 3; j++){
            force_element = dispersion_interaction_c(state, functional, 2, i+1, j+1);
            return_force.push_back(force_element);
        }
    }

    return return_force;
};
