#include "Set_Scf.hpp"
#include "Set_Default_Factory.hpp"
#include "Set_External_Field.hpp"

using Teuchos::RCP;
using Teuchos::ParameterList;
using std::string;

Default_Values::Set_Scf::Set_Scf(){
    set_default_values.append(Default_Values::Set_Default_Factory::create_single("External_Field"));
}
int Default_Values::Set_Scf::set_values(RCP<ParameterList>& total_parameters){
    RCP<ParameterList> parameters;
    if(total_parameters -> name() == "Scf"){
        parameters = total_parameters;
    /*
    // Backward compatability
    } else if(total_parameters -> isSublist("Scf")){
        parameters = Teuchos::sublist(total_parameters, "Scf");
    */
    } else {
        return 1;
    }
    parameters->get<int>("IterateMaxCycle",256);
    parameters->get<string>("Convergence","Density");
    parameters->get<double>("ConvergenceTolerance",1.0E-7);
    parameters->sublist("ExchangeCorrelation").get<string>("XCLibrary","Libxc");
    parameters -> set<double>("ConvergenceTolerance", Teuchos::getDoubleParameter(parameters, "ConvergenceTolerance"));
    return  1+call_components(parameters);
}
