#include "Set_Default_Factory.hpp"
#include "Set_Basic_Information.hpp"
#include "Set_External_Field.hpp"
#include "Set_Scf.hpp"

RCP<Set_Default_Values> Default_Values::Set_Default_Factory::create_single(std::string class_name){
    if(class_name=="BasicInformation"){
        return new Set_Basic_Information();
    }
    else if (class_name=="External_Field"){
        return new Set_External_Field();
    }
    else if (class_name=="Scf"){
        return new Set_Scf();
    }
    else{
        Verbose::all()<< "Default_Values::Set_Default_Factory::create_single class name is wrong!!"  <<std::endl;
        exit(-1);
    }

}
