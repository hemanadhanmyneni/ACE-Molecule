#pragma once 
#include <string>
#include "Set_Default_Values.hpp"
#include "../Verbose.hpp"

namespace Default_Values{
    namespace Set_Default_Factory{
        Set_Default_Values* create_instance(std::string class_name);
    }
}
