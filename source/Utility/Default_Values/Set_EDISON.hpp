#pragma once
#include "Teuchos_ParameterList.hpp"
#include "Teuchos_RCP.hpp"
#include "Set_Default_Values.hpp"

namespace Default_Values{
    class Set_EDISON: public Set_Default_Values{
        public:
            Set_EDISON();
            int set_parameters( Teuchos::Array< Teuchos::RCP<Teuchos::ParameterList> >& total_parameters);
        private:
            int set_parameters( Teuchos::RCP<Teuchos::ParameterList>& total_parameters);
            int set_parameters_basic(Teuchos::RCP<Teuchos::ParameterList>& parameters, std::string functional);
            int set_parameters_guess(Teuchos::RCP<Teuchos::ParameterList>& parameters, Teuchos::RCP<Teuchos::ParameterList> pp_param);
            int set_parameters_scf(Teuchos::RCP<Teuchos::ParameterList>& parameters);
            int set_parameters_tddft(Teuchos::RCP<Teuchos::ParameterList>& parameters);
    };
}
