#include <cstdlib>
#include "Time_Measure.hpp"
#include <iostream>
#define _TAG_LEN_ (60)

void Time_Measure::start(std::string tag){
    if( map.find(tag)==map.end()){
        int index = map.size();
        map[tag] = index;
        tags.push_back(tag);
        elapsed_times.push_back( std::vector<double>() );
        start_times.push_back(0.0);
        end_times.push_back(0.0);
    }
    auto index = map.find(tag)->second;
    start_times.at(index) = get_time();
    return;
}
void Time_Measure::end(std::string tag){
    if( map.find(tag)==map.end() ){
        std::cout << "given tag:  " << tag <<std::endl;
        std::cout << "Time_Measure::No such tag exist" <<std::endl;
        exit(-1);
    }
    auto index = map.find(tag)->second;
    end_times.at(index) = get_time();
    elapsed_times.at(index).push_back(end_times[index]-start_times[index]);
    return ;
}
double Time_Measure::get_elapsed_time(std::string tag, int order ){
    if( map.find(tag)==map.end() ){
        std::cout << "given tag:  " << tag <<std::endl;
        std::cout << "Time_Measure::No such tag exist" <<std::endl;
        exit(-1);
    }
    auto index = map.find(tag)->second;
    if(order <0){
        if(0<=static_cast<int>(elapsed_times[index].size())+order){
            return get_mean(elapsed_times[index][elapsed_times[index].size()+order]);
        }
        else{
            std::cout << "Time_Measure::Wrong order " << order << " for " << tag << std::endl;
            exit(-1);
        }
    }
    else{
        if(order<static_cast<int>(elapsed_times[index].size())){
            return get_mean(elapsed_times[index][order]);
        }
        else{
            std::cout << "Time_Measure::Wrong order " << order << " for " << tag << std::endl;
            exit(-1);
        }
    }
}

double Time_Measure::get_elapsed_time(std::string tag){
    if( map.find(tag)==map.end() ){
        std::cout << "given tag:  " << tag <<std::endl;
        std::cout << "Time_Measure::No such tag exist" <<std::endl;
        exit(-1);
    }
    auto index = map.find(tag)->second;
    double return_val = 0.0;
    for(int i=0; i<elapsed_times[index].size(); i++){
        return_val+=elapsed_times[index][i];
    }
    return get_mean(return_val);
}

void Time_Measure::print(std::ostream& out,std::string tag) const
{
    if( !tag.empty() and map.find(tag)==map.end()){
        std::cout << "Time_Measure::No such tag exist: " << tag << std::endl;
        exit(-1);
    }
    if(map.empty()){
        return;
    }
    out << std::endl;
    std::ios oldstate(nullptr);
    oldstate.copyfmt(out);
#ifdef ACE_HAVE_MPI
    out << "================ Computation Time (mpi, resolution: " << std::scientific << MPI_Wtick()<<") ================" <<std::endl;
#elif defined( ACE_HAVE_OMP)
    out << "================ Computation Time (omp, resolution: " << std::scientific << omp_get_wtick()<<") ================" <<std::endl;
#else
    out << "=============================== Computation Time =================================" <<std::endl;
#endif
    out.copyfmt(oldstate);

    if(!tag.empty()){
        auto index = map.find(tag)->second;
        double elapsed_time = 0.0;
        for(int i=0; i<elapsed_times[index].size(); i++) elapsed_time+=elapsed_times[index][i];
        out<< std::setw(_TAG_LEN_) << tag << ":    " << get_mean(elapsed_time) <<" s ("<< elapsed_times[index].size() <<")" <<std::endl;

    }
    else{
        for(auto iter = map.begin(); iter!=map.end(); iter++){
            auto index =iter->second;
            double elapsed_time=0.0;
            for(int i=0; i<elapsed_times[index].size(); i++){
                elapsed_time +=elapsed_times[index][i];
            }
            out << std::setw(_TAG_LEN_) << iter->first<< ":    " << get_mean(elapsed_time) << " s (" << elapsed_times[iter->second].size() << ")" <<std::endl;
        }
    }
    out << "===============================================================================" <<std::endl;
    out << std::endl;
    return ;
}

void Time_Measure::print(std::ostream& out, int order,std::string tag/*=""*/){
    if( !tag.empty() and map.find(tag)==map.end()){
        std::cout << "Time_Measure::No such tag exist: " << tag << std::endl;
        exit(-1);
    }
    if(map.empty()){
        return;
    }
    out << std::endl;
    std::ios oldstate(nullptr);
    oldstate.copyfmt(out);
#ifdef ACE_HAVE_MPI
    out << "================ Computation Time (mpi, resolution: " << std::scientific << MPI_Wtick()<<") ================" <<std::endl;
#elif defined( ACE_HAVE_OMP)
    out << "================ Computation Time (omp, resolution: " << std::scientific << omp_get_wtick()<<") ================" <<std::endl;
#else
    out << "=============================== Computation Time =================================" <<std::endl;
#endif
    out.copyfmt(oldstate);

    if(!tag.empty()){
        if( map.find(tag)==map.end()){
            std::cout << "Time_Measure::No such tag exist" <<std::endl;
            exit(-1);
        }

        auto index = map.find(tag)->second;
        if(order<0){
            if(0<=static_cast<int>(elapsed_times[index].size())+order){
                out<< std::setw(_TAG_LEN_) << tag << ":    " << get_mean(elapsed_times[index][order+elapsed_times[index].size()]) <<" s" <<std::endl;
            }
        } else {
            if(order<static_cast<int>(elapsed_times[index].size())){
                out<< std::setw(_TAG_LEN_) << tag << ":    " << get_mean(elapsed_times[index][order]) <<" s" <<std::endl;
            }
        }
    }
    else{
        for(auto iter = map.begin(); iter!=map.end(); iter++){
            auto index =iter->second;
            if(order<0){
                if(0<=static_cast<int>(elapsed_times[index].size())+order){
                out<< std::setw(_TAG_LEN_) << iter->first << ":    " << get_mean(elapsed_times[index][order+elapsed_times[index].size()]) <<" s" <<std::endl;
                }
            } else {
                if(order<static_cast<int>(elapsed_times[index].size())){
                    out<< std::setw(_TAG_LEN_) << iter->first << ":    " << get_mean(elapsed_times[index][order]) <<" s" <<std::endl;
                }
            }
        }
    }
    out << "===============================================================================" <<std::endl;
    out << std::endl;
    return ;
}

double Time_Measure::get_time(){
#ifdef ACE_HAVE_MPI
    return MPI_Wtime();
#elif defined( ACE_HAVE_OMP)
    return omp_get_wtick();
#else
    return ((float) clock())/CLOCKS_PER_SEC;
#endif
}
double Time_Measure::get_mean(double elapsed_time) const{
    double return_val=0.0;

    if(use_comm_){
        Parallel_Manager::info().all_allreduce(&elapsed_time,&return_val, 1, Parallel_Manager::Sum);
        return_val/=Parallel_Manager::info().get_total_mpi_size();
    }
    else{
        return_val=elapsed_time;
    }
    return return_val;
}
std::ostream &operator << (std::ostream &c, const Time_Measure  &timer){
    timer.print(c);
    return c;
}
std::ostream &operator << (std::ostream &c, const Time_Measure* &timer){
    timer->print(c);
    return c;
}

std::vector<std::string> Time_Measure::get_tags(){
    return this -> tags;
}
