#include "Change_Indice.hpp"
#include <cmath>
#include "Verbose.hpp"
#include "Parallel_Manager.hpp"
#include "Parallel_Util.hpp"

using std::vector;
using std::endl;
using Teuchos::RCP;

Change_Indice::Change_Indice(RCP<const Basis> basis1, RCP<const Basis> basis2){ // it makes vetor containing index from grid_setting1 to grid_setting2
    if(basis1->get_grid_type()!=basis2->get_grid_type()){
        Verbose::all() << "grid setting type is different" << endl;
        exit(-1);
    }

    int size1 = basis1->get_original_size();
    int size2 = basis2->get_original_size();
     
    int NumMyElements1 = basis1->get_map()->NumMyElements();
    int* MyGlobalElements1 =basis1->get_map()->MyGlobalElements();
    //int NumMyElements2 = basis2->get_map()->NumMyElements();
    //int* MyGlobalElements2 =basis2->get_map()->MyGlobalElements();

    int* tmp_index_list = new int[size1];
    index_list.resize(size1);
    //memset(index_list, 0, sizeof(int)*size1);
    memset(tmp_index_list, 0, sizeof(int)*size1);
    int* points1 = new int[3];
    points1[0] = basis1->get_points()[0];
    points1[1] = basis1->get_points()[1];
    points1[2] = basis1->get_points()[2];
    int* points2 = new int[3];
    points2[0] = basis1->get_points()[0];
    points2[1] = basis1->get_points()[1];
    points2[2] = basis1->get_points()[2];

    if(size1 < size2){
        for(int i=0; i<NumMyElements1; i++){
            
            int i_x=0,i_y=0,i_z=0;
            basis1->decompose(MyGlobalElements1[i],i_x,i_y,i_z);
            i_x = (-basis1->get_points()[0]+basis2->get_points()[0])/2+i_x;
            i_y = (-basis1->get_points()[1]+basis2->get_points()[1])/2+i_y;
            i_z = (-basis1->get_points()[2]+basis2->get_points()[2])/2+i_z;
            int index = basis2->combine(i_x,i_y,i_z );
            if(index==-1)
                Verbose::all() << i << " " << i_x << " " << i_y << " " << i_z << endl;
            tmp_index_list[MyGlobalElements1[i]] = index;
        }
    }
    else if(size1==size2){
        //Verbose::all() << "you don't have to do it" << endl;
        //exit(-1);
        for(int i = 0; i < NumMyElements1; ++i){
            tmp_index_list[MyGlobalElements1[i]] = MyGlobalElements1[i];
        }
    }
    else{
        for(int i=0; i<NumMyElements1; i++){
            int i_x=0,i_y=0,i_z=0;
            basis1->decompose(MyGlobalElements1[i],i_x,i_y,i_z);

            i_x = (-basis1->get_points()[0]+basis2->get_points()[0])/2+i_x;
            i_y = (-basis1->get_points()[1]+basis2->get_points()[1])/2+i_y;
            i_z = (-basis1->get_points()[2]+basis2->get_points()[2])/2+i_z;
            if(std::abs(i_x)<basis2->get_points()[0] and std::abs(i_y)<basis2->get_points()[1] and std::abs(i_z)<basis2->get_points()[2]){

                int index = basis2->combine(i_x,i_y,i_z );
                tmp_index_list[MyGlobalElements1[i]] = index;
            }
            else{
                tmp_index_list[MyGlobalElements1[i]] = -1;
            }
        }
    }
    Parallel_Util::group_sum(tmp_index_list, &index_list[0], size1);
    /*
    cout << "indice test" << endl;
    for(int i=0; i<size1; i++){
        cout << index_list[i] << "   ";
    }
    
    cout << endl;
    exit(-1);
    */
    delete [] tmp_index_list;
    delete [] points1;
    delete [] points2;
    return;
}
int Change_Indice::change_indice(int i){
    return index_list[i];
}
