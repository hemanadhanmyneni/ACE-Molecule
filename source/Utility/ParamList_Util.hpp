#pragma once
#include <string>
#include <vector>

#include "Teuchos_Array.hpp"
#include "Teuchos_RCP.hpp"
#include "Teuchos_ParameterList.hpp"
#include "../Io/Atoms.hpp"

/**
 * @todo Change to more suitable name.
 **/
namespace ParamList_Util{
    /**
     * @brief Create pseudopotential file name from path and suffix.
     * @details filepath/[atom_symbols]filesuffix
     **/
    Teuchos::Array<std::string> PPnames_from_path(
        std::vector<int> atomic_numbers,
        std::string filepath,
        std::string filesuffix
    );
    /**
     * @brief Calculate number of total valence electrons.
     **/
    double get_total_electrons(
        std::string type, std::string format,
        Teuchos::RCP<const Atoms> atoms,
        Teuchos::Array<std::string> ps_filenames,
        std::string xc_type = std::string()
    );

    /**
     * @brief Initialize NumElectrons, SpinMultiplicity, and Polarize.
     **/
    void initialize_electron_parameters(
        Teuchos::RCP<const Atoms> atoms,
        Teuchos::Array< Teuchos::RCP<Teuchos::ParameterList> > &parameters
    );

    /**
     * @brief Get parameter sublist from Array<RCP<ParameterList>>
     **/
    Teuchos::RCP<Teuchos::ParameterList> get_subparameter(
        Teuchos::Array< Teuchos::RCP<Teuchos::ParameterList> > total_parameters,
        std::string name, int index = 0
    );
}
