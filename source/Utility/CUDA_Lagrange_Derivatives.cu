#include "CUDA_Lagrange_Derivatives.hpp"

#include "../Util/Verbose.hpp"
//#include "../Util/Parallel_Manager.hpp"

#include "../Util/String_Util.hpp"

#include <cublas_v2.h>
#include "cuda_runtime_api.h"

#include <cstdio>

#include "nvToolsExt.h"
const char* cublasGetErrorString(cublasStatus_t status);


#include <mpi.h> //for debug!!!

namespace CUDA_Lagrange_Derivatives{
    int num_threads;
    int num_blocks;

    /// temporary memory space for convolution
    double** dT_gamma;
    /// temporary memory space for convolution
    double** dT_gamma2;
    /// temporary memory space for convolution
    double** dT_beta;
    /// temporary memory space for convolution
    double** d_input_fold1;
    /// temporary memory space for convolution
    double** d_input_fold2;
    /// temporary memory space for convolution
    double** d_input_fold3;
    // pointer of pointer in Device
    double* dFx; double* dFy; double* dFz;
    // pointer of pointer in Device
    double** dFx_dev; double** dFy_dev; double** dFz_dev;
    
    double* d_result1;
    double* d_result2;
    double* d_result3;
    double* d_input1;
    double* d_input2;
    double* d_input3;
    // pointer of pointer in Device
    double** dT_gamma_dev; double** dT_gamma2_dev;
    double** dT_beta_dev; 
    double** d_input_fold1_dev;
    double** d_input_fold2_dev;
    double** d_input_fold3_dev;
    int* di_xs; int* di_ys; int* di_zs;
    int* d_from_basic;
    bool gpu_memory_allocated=false;
}
int CUDA_Lagrange_Derivatives::divergence_gpu(const int total_size, 
                    const int* points, const double* scaling,
                    int start_index_z, int end_index_z,
                    int start_index_y, int end_index_y,
                    double* input1, double* input2, double* input3,
                    double*& divergence){

    nvtxRangePushA("CUDA_Lagrange_Derivatives");

    Verbose::single(Verbose::Detail)<< std::endl << "#--------------------------------------------------- CUDA_Kernel_Integral::compute2" << std::endl;
    cudaDeviceSynchronize();
    if (cudaSuccess!=cudaGetLastError()){
        std::cout << "start on fold "<< cudaGetErrorString(cudaGetLastError()) ;
        exit(-1);
    }

    // alloc  potential in gpu memory
    cudaMalloc((void**)& d_result1,  total_size*sizeof(double));
    cudaMemset( d_result1,0,  total_size*sizeof(double));
    cudaMalloc((void**)& d_result2,  total_size*sizeof(double));
    cudaMemset( d_result2,0,  total_size*sizeof(double));
    cudaMalloc((void**)& d_result3,  total_size*sizeof(double));
    cudaMemset( d_result3,0,  total_size*sizeof(double));

    cublasStatus_t check;
    num_threads=points[0];
    num_blocks = points[1]*points[2];

    /////////////////////////////

    cudaStream_t stream[3];
    cudaStreamCreate( &stream[0] );
    cudaStreamCreate( &stream[1] );
    cudaStreamCreate( &stream[2] );

    cublasHandle_t handle1;
    cublasCreate(&handle1);
    cublasHandle_t handle2;
    cublasCreate(&handle2);
    cublasHandle_t handle3;
    cublasCreate(&handle3);

    cublasHandle_t handle;
    cublasCreate(&handle);

    check = cublasSetStream( handle1, stream[0]  );
    check = cublasSetStream( handle2, stream[1]  );
    check = cublasSetStream( handle3, stream[2]  );

//    for(int i=0; i<total_size; i++){
//        std::cout << "input1[i]: " << input1[i]<<std::endl;
//    }
    cudaMemcpyAsync(d_input1, input1, total_size*sizeof(double), cudaMemcpyHostToDevice,stream[0]);
    cudaMemcpyAsync(d_input2, input2, total_size*sizeof(double), cudaMemcpyHostToDevice,stream[1]);
    cudaMemcpyAsync(d_input3, input3, total_size*sizeof(double), cudaMemcpyHostToDevice,stream[2]);
    

    // Construct d^gamma'_{alpha',beta'}
    fold1 <<< num_blocks, num_threads,0,stream[0]>>>(start_index_z,end_index_z,
                                                    points[0],points[1],points[2],
                                                    di_xs,di_ys,di_zs,d_from_basic,d_input1,d_input_fold1_dev);
    fold1 <<< num_blocks, num_threads,0,stream[1]>>>(start_index_z,end_index_z,
                                                    points[0],points[1],points[2],
                                                    di_xs,di_ys,di_zs,d_from_basic,d_input2,d_input_fold2_dev);
    fold2 <<< num_blocks, num_threads,0,stream[2]>>>(start_index_y,end_index_y,
                                                    points[0],points[1],points[2],
                                                    di_xs,di_ys,di_zs,d_from_basic,d_input3,d_input_fold3_dev);

    double one =1.0; double zero = 0.0;
    double factor1 = 1.0/sqrt(scaling[1]*scaling[2]);
    double factor2 = 1.0/sqrt(scaling[0]*scaling[2]);
    double factor3 = 1.0/sqrt(scaling[0]*scaling[1]);

    cublasStatus_t stat1;
    stat1 =cublasDgemmBatched(handle1, CUBLAS_OP_N, CUBLAS_OP_N, 
                                points[0], points[1], points[0], &factor1, (const double**) dFx_dev, points[0],
                                (const double**) d_input_fold1_dev, points[0], 
                                &zero, dT_gamma_dev, points[0], end_index_z-start_index_z );

    stat1 = cublasDgemmBatched(handle2, CUBLAS_OP_N, /*CUBLAS_OP_N*/CUBLAS_OP_T, 
                                points[0], points[1], points[1], &factor2, (const double**) d_input_fold2_dev, points[0], 
                                (const double**) dFy_dev, points[1], 
                                &zero, dT_gamma2_dev, points[0], end_index_z-start_index_z );

    stat1 = cublasDgemmBatched(handle3, CUBLAS_OP_N, /*CUBLAS_OP_N*/CUBLAS_OP_T, 
                                points[0], points[2], points[2], &factor3, (const double**) d_input_fold3_dev, points[0], 
                                (const double**) dFz_dev, points[2], 
                                &zero, dT_beta_dev, points[0], end_index_y-start_index_y );

    unfold1 <<< num_blocks, num_threads,0,stream[0] >>> (start_index_z,end_index_z,  points[0], points[1], points[2],di_xs,di_ys,di_zs, d_from_basic, (const double**) dT_gamma_dev, d_result1 );
    unfold1<<< num_blocks, num_threads,0,stream[1] >>> (start_index_z,end_index_z, points[0], points[1], points[2],di_xs,di_ys,di_zs, d_from_basic, (const double**) dT_gamma2_dev, d_result2 );
    unfold2<<< num_blocks, num_threads,0,stream[2] >>> (start_index_y,end_index_y, points[0], points[1], points[2],di_xs,di_ys,di_zs, d_from_basic, (const double**) dT_beta_dev, d_result3 );

    cudaStreamSynchronize(stream[0]);
    cudaStreamSynchronize(stream[1]);
    cudaStreamSynchronize(stream[2]);


//    cudaMemcpy(divergence, d_result1, total_size*sizeof(double), cudaMemcpyDeviceToHost);
//    std::cout << "divergence"<<std::endl;
//    int mpi_size; int mpi_rank;
//    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
//    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
//    for (int j=0; j<mpi_size; j++){
//        if (mpi_rank == j )  {
//        std::cout << j<< "th start" <<std::endl;
//        for(int i=0; i<total_size; i++){
//            std::cout << mpi_size<< "\t" << divergence[i] <<std::endl;
//        }
//        std::cout << "end" <<std::endl;
//        }
//        
//        MPI_Barrier(MPI_COMM_WORLD);
//    }
//    exit(-1);
    cublasDaxpy(handle, total_size, &one, d_result2, 1, d_result1, 1);
    cublasDaxpy(handle, total_size, &one, d_result3, 1, d_result1, 1);

    cudaMemcpy(divergence, d_result1, total_size*sizeof(double), cudaMemcpyDeviceToHost);

    cudaFree(d_result1);
    cudaFree(d_result2);
    cudaFree(d_result3);
    cudaStreamDestroy(stream[0]);
    cudaStreamDestroy(stream[1]);
    cudaStreamDestroy(stream[2]);
    cublasDestroy(handle1);
    cublasDestroy(handle2);
    cublasDestroy(handle3);
    cublasDestroy(handle);
    nvtxRangePop();
    //nvtxRangeEnd("CUDA_ISF");
    //timer->end("Compute Hartree Potential Time");
    return 0;
}
__global__ void CUDA_Lagrange_Derivatives::fold1 (const int start_index_z, const int end_index_z,
                                            const int points_x,const int points_y,const int points_z,
                                            const int* di_xs,const int* di_ys,const int* di_zs,
                                            const int* d_from_basic,
                                            const double* d_density, double** d_rho_dev)
{
    const int idx = blockDim.x*blockIdx.x + threadIdx.x;
    const int total_size = points_x*points_y*points_z;
    const int stride = blockDim.x*gridDim.x;

    int i_x,i_y,i_z;
    int ind;
    for(int i=idx; i<total_size ; i+=stride){
        ind = d_from_basic[i];
        if (ind>=0){    
            i_x = di_xs[ind];
            i_y = di_ys[ind];
            i_z = di_zs[ind];
            if (i_z>=start_index_z and i_z<end_index_z){
                d_rho_dev[i_z-start_index_z][points_x*i_y+i_x] = d_density[ind];
            }
        }
    }
    
    return;
}
__global__ void CUDA_Lagrange_Derivatives::fold2 (const int start_index_y, const int end_index_y,
                                            const int points_x,const int points_y,const int points_z,
                                            const int* di_xs,const int* di_ys,const int* di_zs,
                                            const int* d_from_basic,
                                            const double* d_density, double** d_rho_dev2)
{
    const int idx = blockDim.x*blockIdx.x + threadIdx.x;
    const int total_size = points_x*points_y*points_z;
    const int stride = blockDim.x*gridDim.x;

    int i_x,i_y,i_z;
    int ind;
    for(int i=idx; i<total_size ; i+=stride){
        ind = d_from_basic[i];
        if (ind>=0){    
            i_x = di_xs[ind];
            i_y = di_ys[ind];
            i_z = di_zs[ind];
            if(i_y>=start_index_y and i_y<end_index_y){
                d_rho_dev2[i_y-start_index_y][points_x*i_z+i_x] = d_density[ind];
            }
        }
    }

    return;
}


__global__ void CUDA_Lagrange_Derivatives::unfold1( const int start_index_z, const int end_index_z,
                                const int points_x,const int points_y,const int points_z,
                                const int* di_xs,const int* di_ys,const int* di_zs,
                                const int* d_from_basic,
                                double const** __restrict__ original_matrices, double* values )
{
    const int idx = blockDim.x*blockIdx.x + threadIdx.x;
    const int total_size = points_x*points_y*points_z;
    const int stride = blockDim.x*gridDim.x;

    int i_x,i_y,i_z;
    int ind;
    for(int i=idx; i<total_size ; i+=stride){
        ind = d_from_basic[i];
        if (ind>=0){
            i_x = di_xs[ind];
            i_y = di_ys[ind];
            i_z = di_zs[ind];
            if(i_z>=start_index_z and i_z<end_index_z){
                values[ind] = original_matrices[i_z-start_index_z][points_x*i_y+i_x];
            }
        }
    }

    return;
}

__global__ void CUDA_Lagrange_Derivatives::unfold2( const int start_index_y, const int end_index_y,
                                const int points_x,const int points_y,const int points_z,
                                const int* di_xs,const int* di_ys,const int* di_zs,
                                const int* d_from_basic,
                                double const** __restrict__ original_matrices, double* values )
{
    const int idx = blockDim.x*blockIdx.x + threadIdx.x;
    const int total_size = points_x*points_y*points_z;
    const int stride = blockDim.x*gridDim.x;

    int i_x,i_y,i_z;
    int ind;
    for(int i=idx; i<total_size ; i+=stride){
        ind = d_from_basic[i];
        if (ind>=0){
            i_x = di_xs[ind];
            i_y = di_ys[ind];
            i_z = di_zs[ind];
            if(i_y>=start_index_y and i_y<end_index_y){
                values[ind] = original_matrices[i_y-start_index_y][points_x*i_z+i_x];
            }
        }
    }

    return;
}

