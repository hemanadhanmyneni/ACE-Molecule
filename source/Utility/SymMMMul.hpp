#pragma once 
#include "Parallel_Manager.hpp"
/**
@breif SymMMMul class support sparse matrix - sparse matrix multiplication with MPI communications 
@author Sunghwan Choi 

This class calculate new sparse matrix C by mutiplying sparse matrix A and transpose of another sparse matrix B
C = A* B^T
Used format in this class is Compressed Row Strage(CRS) format 
*/
class SymMMMul{
    public:
        /**
            @breif constructor of SymMMMul. set the method to keep sparsity of result matrix 
            
        */
        SymMMMul(double tol = 1.e-8):tol(tol){};
        /**
            @brief This compute function adopt 
        */
        int compute(const int total_row_size, const int my_row_size, int* A_row_ptr, int* A_col_ind, double* A_val, int* B_row_ptr, int* B_col_ind, double* B_val, int*& C_row_ptr, int*& C_col_ind, double*& C_val );
    protected:
        double tol;
        /**
            @brief COO_block class is data container for sparse matrix in coordinate format
            @author Sunghwan Choi
            All member functions and variables are defined as private so that it is not usable other part of code except SymMMMul class 
        */
        class COO_block{
            friend class SymMMMul;
            private:
                void clean(){
                    nnz=0; row_size=0; col_size=0;
                    if(row_ind!=nullptr) 
                        delete[] row_ind;
                    if(col_ind!=nullptr)
                        delete[] col_ind;
                    if(val!=nullptr)
                        delete[] val;
                    row_ind=nullptr; col_ind=nullptr; val=nullptr; 
                };
                void get_data(int*& row_ind, int*&  col_ind, double*& val){ 
                    row_ind = this->row_ind;
                    col_ind = this->col_ind;
                    val = this->val;
                };
                int get_nnz(){ return nnz;};
                int get_col_size(){return col_size;};
                int get_row_size(){return row_size;};

                COO_block(int nnz, int row_size, int col_size){realloc(nnz,row_size,col_size);};
                COO_block(){nnz = 0; row_size=0; col_size=0; row_ind=nullptr; col_ind=nullptr; val=nullptr;};

                int nnz; int row_size; int col_size;
                int* row_ind=nullptr; int* col_ind=nullptr; double* val=nullptr;
                void realloc(int nnz, int row_size, int col_size){
                    if(row_ind!=nullptr)
                        delete[] row_ind; 
                    if(col_ind!=nullptr)
                        delete[] col_ind;
                    if(val!=nullptr)
                        delete[] val;
                    row_ind = new int[nnz];
                    col_ind = new int[nnz];
                    val = new double[nnz]; 
                    this->nnz = nnz;
                    this->row_size = row_size;
                    this->col_size = col_size;
                    return;
                };
        };
        int match_indices(const int A_size, int* A_index, const int B_size, int* B_index,std::vector<int>& matched_A, std::vector<int>& matched_B );
        void calculate_block(const int A_row_size, int* A_row_ptr, int* A_col_ind, double* A_val, const int B_row_size, int* B_row_ptr, int* B_col_ind, double* B_val, COO_block& result);
//        void calculate_symmblock(const int A_row_size, int* A_row_ptr, int* A_col_ind, double* A_val, const int B_row_size, int* B_row_ptr, int* B_col_ind, double* B_val, COO_block& result);
        void calculate_symmblock(const int A_row_start_index, const int A_row_size, int* A_row_ptr, int* A_col_ind, double* A_val, const int B_row_start_index,const int B_row_size, int* B_row_ptr, int* B_col_ind, double* B_val, COO_block& block_data);

        /**
            @brief This function do almost same function with MPI_WaitAll 
            @param [in] n The size of request array
            @param [in] request The array of request that we need to wait
        */
        void wait(const int n, MPI_Request* request);
        /**
            @brief 
        */
        void spread(std::vector<int> send_index, std::vector<int> recv_index, std::vector<int> all_row_size, std::vector<COO_block>& blocks);
        void mergecoo(std::vector<int> all_row_size, int my_pid, int total_size, int nBlock, COO_block* blocks, int*& row_ind, int*& col_ind, double*& val );
        
        void scheduling (const int my_pid, const int nProc, std::vector<int>& recv_index, std::vector<int>& send_index);

        void isend_csr(int send_pid, int row_size, int nnz_size, int* row_ptr, int* col_ind, double* val, MPI_Request* request);
        void irecv_csr(int recv_pid, int row_size, int nnz_size, int* row_ptr, int* col_ind, double* val, MPI_Request* request);
        /**
            @brief This function is responsible for sending data through asyncronos way.
            @param [in] send_pid rank of destination
            @param [in] nnz_size number of non-zero value to send 
            @param [in] row_ind  row index pointer 
            @param [in] col_ind  col index pointer 
            @param [in] val      non zero value pointer
            @param [in/out] request array of request for anychronos communication
        */
        void isend_coo(int send_pid, int nnz_size, int* row_ind, int* col_ind, double* val, MPI_Request* request);
        void irecv_coo(int recv_pid, int nnz_size, int* row_ind, int* col_ind, double* val, MPI_Request* request);
};
