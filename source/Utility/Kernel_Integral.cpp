#include "Kernel_Integral.hpp"
#include <stdexcept>
#include <unistd.h>
#include <cmath>

#include "Epetra_Map.h"

#include "../Utility/Time_Measure.hpp"
#include "../Utility/Math/Faddeeva.hpp"
#include "../Utility/Verbose.hpp"
#include "../Utility/Parallel_Manager.hpp"
#include "../Utility/Parallel_Util.hpp"
#include "../Utility/String_Util.hpp"
#include "../Utility/Value_Coef.hpp"

#ifdef USE_CUDA
#include "CUDA_Kernel_Integral.hpp"
#endif

using std::abs;
using std::string;
//using Teuchos::Time;
//using Teuchos::TimeMonitor;
using Teuchos::RCP;
using Teuchos::rcp;
using Teuchos::Array;
using Teuchos::SerialDenseMatrix;

#ifdef USE_CUDA
using namespace CUDA_Kernel_Integral;
#endif
Kernel_Integral::Kernel_Integral(
                                RCP<const Basis> basis,
                                double t_i, double t_l, double t_f,
                                int num_points1, int num_points2,
                                bool asymtotic_correction/* = true*/,
                                int ngpus )
                                :t_i(t_i),t_l(t_l),t_f(t_f),
                                num_points1(num_points1),
                                num_points2(num_points2)

{
    auto points = basis->get_points();
    this -> asymtotic_correction = asymtotic_correction;
    this -> ngpus = ngpus;
    this -> gpu_enable = (0<ngpus);
    this -> basis = basis;
    // (PBC) 주석
    this -> periodicity = basis->get_periodicity();
    // this -> periodicity_array = mesh->get_periodicitiy_array();
    this -> n_cutoff = basis->get_Hartree_cutoff();
    if (periodicity == 3){
        //this->periodicity_array = {1,1,1};
        this->periodicity_array[0] = 1;
        this->periodicity_array[1] = 1;
        this->periodicity_array[2] = 1;
    }
    else if (periodicity == 2){
        //this->periodicity_array = {1,1,0};
        this->periodicity_array[0] = 1;
        this->periodicity_array[1] = 1;
        this->periodicity_array[2] = 0;
    }
    else if (periodicity == 1){
        //this->periodicity_array = {1,0,0};
        this->periodicity_array[0] = 1;
        this->periodicity_array[1] = 0;
        this->periodicity_array[2] = 0;
    }
    
    this -> fill_complete = false;
    this -> fill_complete2 = false;
    //this->output = "isf.out";
    //__check_input();
    timer = rcp(new Time_Measure() );
    if (gpu_enable==false){
        for(int i=0; i<points[2]; i++){
            T_gamma.append( SerialDenseMatrix<int, double> (points[0], points[1]) );
            T_gamma2.append( SerialDenseMatrix<int, double> (points[0], points[1]) );
            d_gamma.append( SerialDenseMatrix<int, double> (points[0], points[1]) );
        }

        for(int i=0;i<points[1];i++){
            T_beta.append( SerialDenseMatrix<int, double> (points[0], points[2]) );
            T_beta2.append( SerialDenseMatrix<int, double> (points[0], points[2]) );
        }
        for(int i=0;i<points[1];i++){
            v_hartree.append( SerialDenseMatrix<int, double> (points[0], points[2]) );
        }
        blas = Teuchos::rcp( new Teuchos::BLAS<int,double>() );
    }
    #ifdef USE_CUDA
    else if (gpu_enable==true and gpu_memory_allocated==false){
        Parallel_Manager::info().assign_gpus(ngpus,Parallel_Manager::Kernal_Int);

        timer->start("Initialize");
        dFx = new double*[points[2]];
        dFy = new double*[points[2]];
        dFz = new double*[points[1]];
        drho = new double*[points[2]];
        dT_gamma = new double*[points[2]];
        dT_gamma2 = new double*[points[2]];
        dT_beta = new double*[points[1]];
        dT_beta2 = new double*[points[1]];

        const int total_size = basis->get_original_size();

        cudaMalloc((void**)& d_density,        total_size*sizeof(double));

        cudaMalloc((void**)& drho_dev, points[2]*sizeof(double*));
        cudaMalloc((void**)& dT_gamma_dev, points[2]*sizeof(double*));
        cudaMalloc((void**)& dT_gamma2_dev, points[2]*sizeof(double*));
        cudaMalloc((void**)& dT_beta_dev, points[1]*sizeof(double*));
        cudaMalloc((void**)& dT_beta2_dev, points[1]*sizeof(double*));

        cudaMalloc((void**)& dFx_dev, points[2]*sizeof(double*));
        cudaMalloc((void**)& dFy_dev, points[2]*sizeof(double*));
        cudaMalloc((void**)& dFz_dev, points[1]*sizeof(double*));

//        #ifdef ACE_HAVE_OMP
//        #pragma omp parallel for
//        #endif
        for(int i=0; i<points[2]; i++){
            cudaMalloc((void**)& drho[i], points[0]*points[1]*sizeof(double));
            cudaMalloc((void**)& dT_gamma[i],  points[0]*points[1]*sizeof(double));
            cudaMalloc((void**)& dT_gamma2[i], points[0]*points[1]*sizeof(double));
            cudaMalloc((void**)& dFx[i],  points[0]*points[0]*sizeof(double));
            cudaMalloc((void**)& dFy[i],  points[1]*points[1]*sizeof(double));
            cudaMemcpy(&dFx_dev[i],  &dFx[0],  sizeof(double*), cudaMemcpyHostToDevice);
            cudaMemcpy(&dFy_dev[i],  &dFy[0],  sizeof(double*), cudaMemcpyHostToDevice);
            cudaMemset(drho[i], 0, points[0]*points[1]*sizeof(double));
        }
//        #ifdef ACE_HAVE_OMP
//        #pragma omp parallel for
//        #endif
        for(int j=0;j<points[1];j++){
            cudaMalloc((void**)& dT_beta[j],  points[0]*points[2]*sizeof(double));
            cudaMalloc((void**)& dT_beta2[j],  points[0]*points[2]*sizeof(double));
            cudaMalloc((void**)& dFz[j],  points[2]*points[2]*sizeof(double));
            cudaMemcpy(&dFz_dev[j], &dFz[0], sizeof(double*), cudaMemcpyHostToDevice);
        }

        cudaMemcpy(drho_dev, drho, points[2]*sizeof(double*), cudaMemcpyHostToDevice);
        cudaMemcpy(dT_gamma_dev, dT_gamma, points[2]*sizeof(double*), cudaMemcpyHostToDevice);
        cudaMemcpy(dT_gamma2_dev, dT_gamma2, points[2]*sizeof(double*), cudaMemcpyHostToDevice);

        cudaMemcpy(dT_beta_dev, dT_beta, points[1]*sizeof(double*), cudaMemcpyHostToDevice);
        cudaMemcpy(dT_beta2_dev, dT_beta2, points[1]*sizeof(double*), cudaMemcpyHostToDevice);
        timer->end("Initialize");
        Verbose::single(Verbose::Normal)<<"Kernel_Integration:: time to initialize CUDA memory:  " << timer->get_elapsed_time("Initialize",-1)<<"s" <<std::endl;

        timer->start("generate &transfer from_basic and (i_xs,i_ys,i_zs)");
        int* i_xs = new int[total_size];
        int* i_ys = new int[total_size];
        int* i_zs = new int[total_size];
        int* from_basic = new int[points[0]*points[1]*points[2]];

        cudaMalloc((void**)& di_xs,  total_size*sizeof(int));
        cudaMalloc((void**)& di_ys,  total_size*sizeof(int));
        cudaMalloc((void**)& di_zs,  total_size*sizeof(int));
        cudaMalloc((void**)& d_from_basic,  points[0]*points[1]*points[2]*sizeof(int));

        #ifdef ACE_HAVE_OMP
        #pragma omp parallel for
        #endif
        for(int i=0; i<total_size; i++){
            basis->decompose(i,i_xs[i],i_ys[i],i_zs[i]);
        }

        int i_z; int i_y; int i_x;
        const int points_ = points[2]*points[1]*points[0];
        #ifdef ACE_HAVE_OMP
        #pragma omp parallel for private(i_z,i_y,i_x)
        #endif
        for(int i =0; i<points_; i++){
            i_x = i%points[0];
            i_y = (i/points[0])%points[1];
            i_z = (i/points[0])/points[1];
            from_basic[i] = basis->combine(i_x,i_y,i_z);
        }

/*
        int ind=0;
        for (int i_z=0; i_z<points[2]; i_z++){
            for (int i_y=0; i_y<points[1]; i_y++) {
                for (int i_x=0; i_x<points[0]; i_x++){
                    from_basic[ind++] = basis->combine(i_x,i_y,i_z);
                }
            }
        }
*/
        timer->end("generate &transfer from_basic and (i_xs,i_ys,i_zs)");

        cudaMemcpy(di_xs, i_xs, total_size*sizeof(int), cudaMemcpyHostToDevice);
        cudaMemcpy(di_ys, i_ys, total_size*sizeof(int), cudaMemcpyHostToDevice);
        cudaMemcpy(di_zs, i_zs, total_size*sizeof(int), cudaMemcpyHostToDevice);
        cudaMemcpy(d_from_basic, from_basic, points[0]*points[1]*points[2]*sizeof(int), cudaMemcpyHostToDevice);
        delete[] from_basic;
        delete[] i_xs; delete[] i_ys; delete[] i_zs;
        Verbose::single(Verbose::Normal)<<"Kenel_Integration:: elapsed time to generate & transfer from_basic and (i_xs,i_ys,i_zs):  " << timer->get_elapsed_time("generate &transfer from_basic and (i_xs,i_ys,i_zs)",-1)<<"s" <<std::endl;
        gpu_memory_allocated=true;
    }
    #else
    else{
        Verbose::all() << "ACE-Molecule is not installed with CUDA but GPUEnalbe is turned on!" <<std::endl;
        exit(-1);
    }
    #endif
}
Kernel_Integral::~Kernel_Integral()
{
    #ifdef USE_CUDA
    if(gpu_enable == true and gpu_memory_allocated == true){
        for(int i = 0; i < basis->get_points()[2]; i++){
            cudaFree(drho[i]);
            cudaFree(dT_gamma[i]);
            cudaFree(dT_gamma2[i]);
        }

        for(int j=0;j<basis->get_points()[1];j++){
            cudaFree(dT_beta[j]);
            cudaFree(dT_beta2[j]);
        }

        cudaFree( dFx[0]);
        cudaFree( dFy[0]);
        cudaFree( dFz[0]);

        cudaFree(d_density);

        cudaFree( drho_dev);
        cudaFree( dT_gamma_dev);
        cudaFree( dT_gamma2_dev);
        cudaFree( dT_beta_dev);
        cudaFree( dT_beta2_dev);

        cudaFree( dFx_dev);
        cudaFree( dFy_dev);
        cudaFree( dFz_dev);

        cudaFree(di_xs);    cudaFree(di_ys);    cudaFree(di_zs);
        cudaFree(d_from_basic);
        delete[] drho;
        delete[] dT_gamma;
        delete[] dT_gamma2;
        delete[] dT_beta;
        delete[] dT_beta2;
        delete[] dFx;
        delete[] dFy;
        delete[] dFz;
        gpu_memory_allocated=false;
    }
    #endif
}

/*
Kernel_Integral::Kernel_Integral(RCP<const Basis> basis,RCP<ParameterList> parameters){
    this->basis = basis;
    this->parameters = parameters;

    __check_input();
}
*/

std::string Kernel_Integral::get_info_string() const{
    std::string info = "Hartree potential computer info:\n";
    info += String_Util::to_string((long long)w_total.size()) + " points quadrature\n";
    info += "Weights and points list:\n";
    if(w_total.size() < 3){
        for(int i = 0; i < w_total.size(); ++i){
            info += String_Util::to_string(w_total[i]) + "\t" + String_Util::to_string(t_total[i]) +"\n";
        }
    } else {
        for(int i = 0; i < 3; ++i){
            info += String_Util::to_string(w_total[i]) + "\t" + String_Util::to_string(t_total[i]) +"\n";
        }
        info += "...\n";
        info += String_Util::to_string(w_total[w_total.size()-1]) + "\t" + String_Util::to_string(t_total[w_total.size()-1]) +"\n";
    }
    if(w_total.size() == 0){
        info = "";
    }
    return info;
}

/*
double Kernel_Integral::compute_F(double t, int i, int j, int axis){
    long double return_val=0.0;

    const double** scaled_grid = basis->get_scaled_grid();
    const double scaling = basis->get_scaling()[axis];
    RCP<const Epetra_Map> map = basis->get_map();
    if (i==j){
        return_val= sqrt(scaling) * erf(M_PI / (2.0*scaling*t));
    }
    else{
        // In order to avoid the numerical instability comes from evaluation
        // of the error function when t is large,
        // here the scaled complementary error function is evaluated.
        // the final equation that we evaluate is
        //const double x_bar = scaled_grid[axis][i]-scaled_grid[axis][j];
        //re_w_of_z( M_PI/(2*scaling*t),t*x_bar);

        const double x_bar = scaled_grid[axis][i]-scaled_grid[axis][j];
        std::complex<double> z ( M_PI/(2*scaling*t), t*x_bar);
        std::complex<double> w_iz = Faddeeva::erfcx(z);
        return_val=exp(-t*t*x_bar*x_bar);
        //double z( M_PI/(2*scaling*t), t*x_bar);
        //double w_iz = Faddeeva::erfcx(z);
        return_val-=(exp(-t*t*x_bar*x_bar-z*z)*w_iz).real();
        return_val*=sqrt(scaling);
    }
    return return_val;
}
*/
// (PBC)
double Kernel_Integral::compute_F(double t, int i, int j, int axis){
    long double return_val=0.0;

    const double** scaled_grid = basis->get_scaled_grid();
    const double scaling = basis->get_scaling()[axis];
    RCP<const Epetra_Map> map = basis->get_map();

    //const int n_cutoff = 300; // cut_off

    if (periodicity_array[axis] == 1){
        for(int n = -n_cutoff; n < n_cutoff +1; n++){
            long double ret_val = 0.0;
            if (i==j + n*basis->get_points()[axis]){
                ret_val += sqrt(scaling) * erf(M_PI / (2.0*scaling*t));
            }
            else{
                double x_bar = scaled_grid[axis][i]-scaled_grid[axis][j] + scaling*n*basis->get_points()[axis];
                std::complex<double> z ( M_PI/(2*scaling*t), t*x_bar);
                std::complex<double> w_iz = Faddeeva::erfcx(z);
                ret_val=exp(-t*t*x_bar*x_bar);
                ret_val-=(exp(-t*t*x_bar*x_bar-z*z)*w_iz).real();
                //ret_val-=pow(-1,(i - j + n*basis->get_points()[axis])) * exp(-pow(M_PI,2) / (4*pow(scaling*t,2))) * w_iz.real();
                ret_val*=sqrt(scaling);
            }
            return_val += ret_val;
        }
    }
    else{
        if (i==j){
            return_val= sqrt(scaling) * erf(M_PI / (2.0*scaling*t));
        }
        else{
            // In order to avoid the numerical instability comes from evaluation
            // of the error function when t is large,
            // here the scaled complementary error function is evaluated.
            // the final equation that we evaluate is
            //const double x_bar = scaled_grid[axis][i]-scaled_grid[axis][j];
            //re_w_of_z( M_PI/(2*scaling*t),t*x_bar);

            const double x_bar = scaled_grid[axis][i]-scaled_grid[axis][j];
            std::complex<double> z ( M_PI/(2*scaling*t), t*x_bar);
            std::complex<double> w_iz = Faddeeva::erfcx(z);
            return_val=exp(-t*t*x_bar*x_bar);
            //double z( M_PI/(2*scaling*t), t*x_bar);
            //double w_iz = Faddeeva::erfcx(z);
            return_val-=(exp(-t*t*x_bar*x_bar-z*z)*w_iz).real();
            return_val*=sqrt(scaling);
        }
    }
    return return_val;
}

void Kernel_Integral::construct_matrix(){
    if(!is_t_sampled){
        t_sampling();
    }

    timer->start("Kernel_Integral::Compute F matrix");

    F_xs.clear();
    F_ys.clear();
    F_zs.clear();
    for(int alpha=0; alpha<i_proc.size(); alpha++){
        double t = t_proc[alpha];

        SerialDenseMatrix<int, double> F_x (basis->get_points()[0], basis->get_points()[0]);
        SerialDenseMatrix<int, double> F_y (basis->get_points()[1], basis->get_points()[1]);
        SerialDenseMatrix<int, double> F_z (basis->get_points()[2], basis->get_points()[2]);

        for(int i=0; i<basis->get_points()[0]; i++){
            for(int j=0;j<basis->get_points()[0];j++){
                F_x.operator()(i,j) = compute_F(t, i, j, 0);
            }
        }
        for(int i=0; i<basis->get_points()[1]; i++){
            for(int j=0;j<basis->get_points()[1];j++){
                F_y.operator()(i,j) = compute_F(t, i, j, 1);
            }
        }
        for(int i=0; i<basis->get_points()[2]; i++){
            for(int j=0;j<basis->get_points()[2];j++){
                F_z.operator()(i,j) = compute_F(t, i, j, 2);
            }
        }
        F_xs.append(F_x);
        F_ys.append(F_y);
        F_zs.append(F_z);
    }
    this->fill_complete = true;
    timer->end("Kernel_Integral::Compute F matrix");
    Verbose::single(Verbose::Normal) << "Kernel_Integral::  construct_matrix:  " << timer->get_elapsed_time("Kernel_Integral::Compute F matrix",-1) << " s"<<std::endl;
    Verbose::single(Verbose::Normal) << "#---------------------------------------------------------------------------" << std::endl;
    return;
}

void Kernel_Integral::construct_matrix2(){
    //if(i_proc.size()==0)
    if(!is_t_sampled){
        t_sampling();
    }

    F_xs2.clear();
    F_ys2.clear();
    F_zs2.clear();
    //timer->start("Kernel_Integral::Compute F matrix");
    for(int alpha=0; alpha<t_total.size(); alpha++){
        double t = t_total[alpha];

        SerialDenseMatrix<int, double> F_x (basis->get_points()[0], basis->get_points()[0]);
        SerialDenseMatrix<int, double> F_y (basis->get_points()[1], basis->get_points()[1]);
        SerialDenseMatrix<int, double> F_z (basis->get_points()[2], basis->get_points()[2]);

        for(int i=0; i<basis->get_points()[0]; i++){
            for(int j=0;j<basis->get_points()[0];j++){
                F_x.operator()(i,j) = compute_F(t, i, j, 0);
            }
        }
        for(int i=0; i<basis->get_points()[1]; i++){
            for(int j=0;j<basis->get_points()[1];j++){
                F_y.operator()(i,j) = compute_F(t, i, j, 1);
            }
        }
        for(int i=0; i<basis->get_points()[2]; i++){
            for(int j=0;j<basis->get_points()[2];j++){
                F_z.operator()(i,j) = compute_F(t, i, j, 2);
            }
        }
        F_xs2.append(F_x);
        F_ys2.append(F_y);
        F_zs2.append(F_z);
    }
    this->fill_complete2 = true;
    //timer->end("Kernel_Integral::Compute F matrix");
    //Verbose::all(Verbose::Detail) << "Kernel_Integral::  construct_matrix2:  " << timer->get_elapsed_time("Kernel_Integral::Compute F matrix",-1) << " s"<<std::endl;
    //Verbose::all() << "#---------------------------------------------------------------------------" << std::endl;
    return;
}
/*
int Kernel_Integral::compute(RCP<const Basis> basis, Array<RCP<State> >& states){
    RCP<Epetra_Vector> hartree_potential  = rcp(new Epetra_Vector(*basis->get_map() ) );
    if (*basis!=(*this->basis)){
        Verbose::single()<< " basis in Kernel_Integral is different to given basis" <<std::endl;
        Verbose::single()<< this->basis.get()<<std::endl;
        Verbose::single()<< basis.get() <<std::endl;
        exit(-1);
    }
    const int spin_size = states[states.size()-1]->density.size();
    double hartree_energy;

    //Verbose::single()<< "~~~~~~~~" <<std::endl;
    if (spin_size==0){
        Verbose::single()<< "Kernel_Integral:: Problem!!" <<std::endl;
        exit(-1);
    }
    //Verbose::single()<< "@@@@@@@@" <<std::endl;
    this -> compute(states[states.size()-1]->density, hartree_potential,hartree_energy);
    //Verbose::single()<< "!!!!!!!!" <<std::endl;
    RCP<State> new_state = rcp(new State(*states[states.size()-1]) );
    new_state->local_potential.clear();
    new_state->local_potential.append(hartree_potential );

    //Verbose::single()<< "&&&&&&&&" <<std::endl;
    new_state->total_energy = hartree_energy;

    states.append(new_state);
    return 0;
}
*/
double Kernel_Integral::get_energy(Array<RCP<Epetra_Vector> > density){
    double hartree_energy;
    RCP<Epetra_Vector> hartree_potential;
    compute(density,hartree_potential,hartree_energy);
    return hartree_energy;
}
int Kernel_Integral::compute(Array<RCP<Epetra_Vector> > density, RCP<Epetra_Vector>& hartree_potential,double& hartree_energy){
    if(fill_complete==false){
        construct_matrix();
    }
    if(this -> type != this -> which_t_sampling_used){
        throw std::runtime_error("SOMEONE BROKE POISSON-NGAU AGAIN!");
    }

    const int total_size = basis->get_original_size();

    hartree_potential = rcp(new Epetra_Vector(density[0]->Map() ));
    RCP<Epetra_Vector> density_value = rcp(new Epetra_Vector(density[0]->Map()));
    for(int alpha=0; alpha<density.size(); alpha++){
        density_value->Update(1.0, *density[alpha], 1.0);
    }
    // Distribute total density to all nodes
    double *total_density = new double[total_size];
    Parallel_Util::group_allgather_vector(density_value, total_density);
    // end

    // allocate memory for potential
    double* hartree = new double [total_size]();
    double* total_hartree = new double [total_size]();
//    for(int i=0; i<total_size; i++){
//        hartree[i] = 0.0;
//        total_hartree[i] = 0.0;
//    }
    //end
    timer->start("Kernel_Integral::compute");
    if (gpu_enable==false){
        compute_cpu(total_density, hartree, w_proc, F_xs, F_ys, F_zs);
    }
    #ifdef USE_CUDA
    else{
        std::vector<double*> vF_xs;
        std::vector<double*> vF_ys;
        std::vector<double*> vF_zs;
        for(int i =0;i < w_proc.size(); i++){
            vF_xs.push_back(F_xs[i].values());
            vF_ys.push_back(F_ys[i].values());
            vF_zs.push_back(F_zs[i].values());
        }
        Parallel_Manager::info().all_barrier();
        compute_gpu(total_size, basis->get_points(), basis->get_scaling(), total_density, hartree,w_proc, vF_xs, vF_ys, vF_zs);
    }
    #else
    else{
        Verbose::all() << "error!! set gpu_enable==true but ACE-Molecule is installed without CUDA "<< std::endl;
    }
    #endif


    delete[] total_density;

    Parallel_Util::group_sum(hartree, total_hartree, total_size);
    timer->end("Kernel_Integral::compute");

    timer->start("Kernel_Integral::post-processing1");
    int NumMyElements = basis->get_map()-> NumMyElements();
    int* MyGlobalElements = basis->get_map()->MyGlobalElements();
//    for(int i=0; i<NumMyElements; i++){
//        hartree_potential->ReplaceGlobalValues(NumMyElements,&total_hartree[MyGlobalElements[0]], MyGlobalElements);
//    }
    hartree_potential->ReplaceGlobalValues(NumMyElements,&total_hartree[MyGlobalElements[0]], MyGlobalElements);

    delete[] hartree;
    delete[] total_hartree;

    // Asymtotic correction term by Sundholm (JCP 132, 024102, 2010)
    if( this -> asymtotic_correction ){
        for (int i_spin=0;i_spin<density.size();i_spin++){
            hartree_potential->Update(M_PI/(t_f*t_f), *density[i_spin], 1.0);
        }
    }
    timer->end("Kernel_Integral::post-processing1");
    timer->start("Kernel_Integral::post-processing2");
    ////////////////// calculate energy //////////////////////
    RCP<Epetra_Vector> hartree_potential_coef;
    RCP<Epetra_Vector> density_coeff;
    Value_Coef::Value_Coef(basis, hartree_potential, true, false, hartree_potential_coef);
    Value_Coef::Value_Coef(basis, density_value, true, false, density_coeff);
    double total_energy=0;
    hartree_potential_coef->Dot(*density_coeff,&total_energy);
    hartree_energy = 0.5*total_energy;
//    Verbose::single() << "hartree_energy: " << hartree_energy <<std::endl;
//    exit(-1);

    //////////////////  End    //////////////////////////////////

    // end algorithm
    timer->end("Kernel_Integral::post-processing2");
    //timer->print(Verbose::single(Verbose::Normal) );
    return 0;

}

int Kernel_Integral::compute(double* density, double* hartree_potential, double& hartree_energy){
    if(fill_complete2==false){
        construct_matrix2();
    }
    if(this -> type != this -> which_t_sampling_used){
        throw std::runtime_error("SOMEONE BROKE POISSON-NGAU AGAIN!");
    }

    const int total_size = basis->get_original_size();
    if (gpu_enable==false){
        compute_cpu(density, hartree_potential,w_total, F_xs2, F_ys2, F_zs2);
    }
    else{
#ifdef USE_CUDA
        std::vector<double*> vF_xs2;
        std::vector<double*> vF_ys2;
        std::vector<double*> vF_zs2;
        for(int i =0;i < w_total.size(); i++){
            vF_xs2.push_back(F_xs2[i].values());
            vF_ys2.push_back(F_ys2[i].values());
            vF_zs2.push_back(F_zs2[i].values());
        }

        compute_gpu(total_size, basis->get_points(), basis->get_scaling(), density, hartree_potential,w_total, vF_xs2, vF_ys2, vF_zs2);
#else
        Verbose::all() << "error!! set gpu_enable==true but ACE-Molecule is installed without CUDA "<< std::endl;
#endif
    }
    if( this -> asymtotic_correction ){
        for(int i=0; i<total_size; i++)
            hartree_potential[i] +=M_PI/(t_f*t_f)*density[i];
    }

    ////////////////// calculate energy //////////////////////

    double* hartree_potential_coeff = new double[total_size];
    double* density_coeff = new double[total_size];
    Value_Coef::Value_Coef(basis, hartree_potential, true, false, hartree_potential_coeff, total_size);
    Value_Coef::Value_Coef(basis, density, true, false, density_coeff, total_size);
//    double total_energy=0;
//    #ifdef ACE_HAVE_OMP
//    #pragma omp parallel for reduction(+:total_energy)
//    #endif
//    for(int i=0; i<total_size; i++)
//        total_energy+=hartree_potential_coeff[i]*density_coeff[i];
    double total_energy=blas->DOT(total_size, hartree_potential_coeff,1, density_coeff,1);
    hartree_energy = 0.5*total_energy;

    delete [] hartree_potential_coeff;
    delete [] density_coeff;

    //////////////////  End    //////////////////////////////////

    return 0;
}

int Kernel_Integral::compute_cpu(double* density, double* hartree_potential,
            std::vector<double> w_t,
            Teuchos::Array< Teuchos::SerialDenseMatrix<int, double> > F_xs,
            Teuchos::Array< Teuchos::SerialDenseMatrix<int, double> > F_ys,
            Teuchos::Array< Teuchos::SerialDenseMatrix<int, double> > F_zs){

    //Verbose::single(Verbose::Normal)<< std::endl << "#--------------------------------------------------- Kernel_Integral::compute2" << std::endl;

    int total_size = basis->get_original_size();
    auto points = basis->get_points();
    int i,j,a,b,c; // loop indices

    timer->start("Compute Hartree Potential Time");
    //hartree_potential = new double[total_size];
    double* total_d_coeff = new double[total_size];
    Value_Coef::Value_Coef(basis, density, true, false, total_d_coeff, total_size);
    // From here, notations same as [J.Juselius; D.Sundholm, JCP, 126, 094101 (2007)]
    // See eq.(20) of the paper.

    // Construct d^gamma'_{alpha',beta'}
#ifdef ACE_HAVE_OMP
#pragma omp parallel for
#endif
    for(int i=0;i<points[2];i++){
        for(int a=0;a<points[0];a++){
            for(int b=0;b<points[1];b++){
                int ind = basis->combine(a, b, i);
                if(ind >= 0){
                    d_gamma[i].operator()(a,b) = total_d_coeff[ind];
                }
            }
        }
    }
    delete [] total_d_coeff;
#ifdef ACE_HAVE_OMP
#pragma omp parallel for
#endif
    for(int i=0; i<points[1]; i++){
        v_hartree[i].putScalar(); //initialize
    }
    for(int alpha=0; alpha<w_t.size(); alpha++){
#ifdef ACE_HAVE_OMP
#pragma omp parallel for
#endif
        for(int i=0; i<points[2]; i++){
            // Construct T^gamma'_{alpha,beta'}
            T_gamma2[i].multiply(Teuchos::NO_TRANS, Teuchos::NO_TRANS, 1.0, F_xs[alpha], d_gamma[i], 0.0);
            // end

            // Construct T^gamma'_{alpha,beta}
            T_gamma[i].multiply(Teuchos::NO_TRANS, Teuchos::TRANS, 1.0, T_gamma2[i], F_ys[alpha], 0.0);
            // end
        }
        // end
        // Reorder T^gamma' -> T^beta
#ifdef ACE_HAVE_OMP
#pragma omp parallel for //collapse(3)
#endif
        for(int i=0;i<points[1];i++){
            for(int a=0;a<points[0];a++){
                for(int c=0;c<points[2];c++){
                    T_beta[i].operator()(a,c) = T_gamma[c].operator()(a,i);
                }
            }
        }
        // end
#ifdef ACE_HAVE_OMP
#pragma omp parallel for
#endif
        for(int i=0;i<points[1];i++){
            // Construct T^beta_{alpha,gamma}
            T_beta2[i].multiply(Teuchos::NO_TRANS, Teuchos::TRANS, 1.0, T_beta[i], F_zs[alpha], 0.0);
            T_beta2[i].scale(w_t[alpha]);
            v_hartree[i]+=T_beta2[i];
            // end
        }
    }

    // Reorder V^beta_{alpha,gamma} -> V_{alpha,beta,gamma} & Distribute to all nodes
#ifdef ACE_HAVE_OMP
#pragma omp parallel for // collapse(3)
#endif
    for(int i=0; i<points[1]; i++){
        for(int a=0; a<points[0]; a++){
            for(int c=0; c<points[2]; c++){
                int ind = basis->combine(a, i, c);
                //int ind = a+c+i;
                if(ind >= 0){
                    hartree_potential[ind] = v_hartree[i](a,c);
                }
            }
        }
    }
    const double scaling_factor = 2.0/sqrt(M_PI);
/*
    #ifdef ACE_HAVE_OMP
    #pragma omp parallel for
    #endif
    for(int i=0; i<total_size; i++){
        hartree_potential[i]*=scaling_factor;
    }
*/
    blas->SCAL(total_size,scaling_factor, hartree_potential, 1);
    timer->end("Compute Hartree Potential Time");
    return 0;
}
