#pragma once
#include "Teuchos_Array.hpp"
#include "Teuchos_RCP.hpp"

#include "Epetra_MultiVector.h"
#include "Epetra_Vector.h"

#include "../Compute/Poisson_Solver.hpp"
#include "../Basis/Basis.hpp"
//#include "Verbose.hpp"

/**
 * @brief Namespace which contains two-electron calculation methods
 **/
namespace Two_e_integral{
    /**
     * @brief Two-electron two-center intergral calculation
     * @param [in] mesh Mesh object which orbital_coeff is generated.
     * @param [in] orbital_coeff orbital "coefficient" of given basis function (Sinc), not orbital "value".
     * @param [in] i phi*(r1) index
     * @param [in] j phi(r1) index 
     * @param [in] k phi*(r2) index
     * @param [in] l phi(r2) index
     * @param [in] poisson_solver poisson solver object which the two-electron integral is calculated.
     * @return calculated two-electron two-center integral value
     * @details      
     * The return value can be written as the following mulliken symbol.
     * \f[
     *    (ij|kl) = \iint dr_1 dr_2 \frac{\phi_i^*(r_1)\phi_j(r_1)\phi_k^*(r_2)\phi_l(r_2)}{|r_1-r_2|}
     * \f]
     **/
    double compute_two_center_integral(
        Teuchos::RCP<const Basis> basis, 
        Teuchos::RCP<const Epetra_MultiVector> orbital_coeff, 
        int i, int j, int k, int l, 
        Teuchos::RCP<Poisson_Solver> poisson_solver
    );
    /**
     * @brief Two-electron two-center intergral calculation
     * @param [in] mesh Mesh object which orbital_coeff is generated.
     * @param [in] orbital_coeff orbital "coefficient" of given basis function (Sinc), not orbital "value".
     * @param [in] i phi*(r1) index
     * @param [in] j phi(r1) index
     * @param [in] k phi*(r2) index
     * @param [in] l phi(r2) index
     * @param [in] poisson_solver poisson solver object which the two-electron integral is calculated.
     * @return calculated two-electron two-center integral value
     * @details      
     * The return value can be written as the following mulliken symbol.
     * \f[
     *    (ij|kl) = \iint dr_1 dr_2 \frac{\phi_i^*(r_1)\phi_j(r_1)\phi_k^*(r_2)\phi_l(r_2)}{|r_1-r_2|}
     * \f]
     **/
    double compute_two_center_integral(
        Teuchos::RCP<const Basis> basis, 
        double** orbital_coeff, 
        int i, int j, int k, int l, 
        Teuchos::RCP<Poisson_Solver> poisson_solver
    );
    /**
     * @brief Two-electron two-center intergral calculation
     * @param [in] mesh Mesh object which orbital_coeff is generated.
     * @param [in] Kji Kji(r1) = \int (phi_j(r2) phi_i(r2) / |r2-r1| ) dr2
     * @param [in] k orbital value phi*(r2)
     * @param [in] l orbital value phi(r2)
     * @param [in] size size of Kji vector
     * @return calculated two-electron two-center integral value
     * @details      
     * The return value can be written as the following mulliken symbol.
     * \f[
     *    (ij|kl) = \iint dr_1 dr_2 \frac{\phi_i^*(r_1)\phi_j(r_1)\phi_k^*(r_2)\phi_l(r_2)}{|r_1-r_2|}
     * \f]
     **/
    double compute_two_center_integral(
            Teuchos::RCP<const Basis> basis, 
            double* Kji,
            double* k,
            double* l,
            int size
    );

    /**
     * @brief Compute Kji.
     * @param [in] mesh Mesh object which orbital_coeff is generated
     * @param [in] orbital_coeff Orbital "coefficient" of given basis function (Sinc), not orbital "value".
     * @param [in] j phi(r2) index
     * @param [in] i phi*(r2) index
     * @param [in] poisson_solver poisson solver object which the two-electron integral is calculated.
     * @param [out] output calculated Kji value
     * @return calculated Kji value.
     * @details
     * The return value can be written as the following mulliken symbol.
     * \f[
     *     K_{ji}(r_1)  = \int dr_2 \frac{\phi_i^*(r_2)\phi_j(r_2)}{|r_1-r_2|}
     * \f]
     */
    int compute_Kji(
        Teuchos::RCP<const Basis> basis, 
        Teuchos::RCP<const Epetra_MultiVector> orbital_coeff, 
        int j, int i, 
        Teuchos::RCP<Poisson_Solver> poisson_solver, 
        Teuchos::RCP<Epetra_Vector>& output
    );
    //RCP<Epetra_Vector> k and l are orbital value vectors
    /**
     * @brief Two-electron two-center intergral calculation
     * @param [in] mesh Mesh object which orbital_coeff is generated.
     * @param [in] Kji Kji(r1) = \int (phi_j(r2) phi_i(r2) / |r2-r1| ) dr2
     * @param [in] k orbital value phi*(r2)
     * @param [in] l orbital value phi(r2)
     * @return calculated two-electron two-center integral value
     * @details      
     * The return value can be written as the following mulliken symbol.
     * \f[
     *     (ij|kl) = \iint dr_1 dr_2 \frac{\phi_i^*(r_1)\phi_j(r_1)\phi_k^*(r_2)\phi_l(r_2)}{|r_1-r_2|}
     * \f]
     */
    double compute_two_center_integral(
        Teuchos::RCP<const Basis> basis, 
        Teuchos::RCP<Epetra_Vector> Kji, 
        Epetra_Vector* k, Epetra_Vector* l
    );

    //RCP<Epetra_Vector> i, j, k, and l are orbital value vectors
    /**
     * @brief Two-electron two-center intergral calculation
     * @param [in] mesh Mesh object which orbital_coeff is generated.
     * @param [in] i orbital value phi*(r1)
     * @param [in] j orbital value phi(r1) 
     * @param [in] k orbital value phi*(r2)
     * @param [in] l orbital value phi(r2)
     * @param [in] poisson_solver poisson solver object which the two-electron integral is calculated.
     * @return calculated two-electron two-center integral value
     * @details      
     * The return value can be written as the following mulliken symbol.
     * \f[
     *    (ij|kl) = \iint dr_1 dr_2 \frac{\phi_i^*(r_1)\phi_j(r_1)\phi_k^*(r_2)\phi_l(r_2)}{|r_1-r_2|}
     * \f]
     **/
    double compute_two_center_integral(
        Teuchos::RCP<const Basis> basis, 
        Teuchos::RCP<Epetra_Vector> i, Teuchos::RCP<Epetra_Vector> j, 
        Teuchos::RCP<Epetra_Vector> k, Teuchos::RCP<Epetra_Vector> l, 
        Teuchos::RCP<Poisson_Solver> poisson_solver
    );
    
    /**
     * @brief Two-electron two-center intergral calculation
     * @param [in] mesh Mesh object which orbital_coeff is generated.
     * @param [in] i phi*(r1) index
     * @param [in] j phi(r1) index
     * @param [in] k phi*(r2) index
     * @param [in] l phi(r2) index
     * @param [in] poisson_solver poisson solver object which the two-electron integral is calculated.
     * @return calculated two-electron two-center integral value
     * @details      
     * The return value can be written as the following mulliken symbol.
     * \f[
     *    (ij|kl) = \iint dr_1 dr_2 \frac{\phi_i^*(r_1)\phi_j(r_1)\phi_k^*(r_2)\phi_l(r_2)}{|r_1-r_2|}
     * \f]
     **/
    double compute_two_center_integral(
        Teuchos::RCP<const Basis> basis, 
        double* i, double* j, 
        double* k, double* l, 
        Teuchos::RCP<Poisson_Solver> poisson_solver
    );

    /**
     * @brief Compute Kji.
     * @param [in] mesh Mesh object which orbital_coeff is generated
     * @param [in] j orbital value phi(r2)
     * @param [in] i orbital value phi*(r2)
     * @param [in] poisson_solver poisson solver object which the two-electron integral is calculated.
     * @param [out] output calculated Kji value
     * @return calculated Kji value.
     * @details
     * The return value can be written as the following mulliken symbol.
     * \f[
     *     K_{ji}(r_1)  = \int dr_2 \frac{\phi_i^*(r_2)\phi_j(r_2)}{|r_1-r_2|}
     * \f]
     */
    int compute_Kji(
        Teuchos::RCP<const Basis> basis, 
        Teuchos::RCP<Epetra_Vector> j, Teuchos::RCP<Epetra_Vector> i, 
        Teuchos::RCP<Poisson_Solver> poisson_solver, 
        Teuchos::RCP<Epetra_Vector>& output
    );

    //Epetra_Vector* i, j, k, and l are orbital value vectors
    /**
     * @brief Two-electron two-center intergral calculation
     * @param [in] mesh Mesh object which orbital_coeff is generated.
     * @param [in] i orbital value phi*(r1)
     * @param [in] j orbital value phi(r1) 
     * @param [in] k orbital value phi*(r2)
     * @param [in] l orbital value phi(r2)
     * @param [in] poisson_solver poisson solver object which the two-electron integral is calculated.
     * @return calculated two-electron two-center integral value
     * @details      
     * The return value can be written as the following mulliken symbol.
     * \f[
     *    (ij|kl) = \iint dr_1 dr_2 \frac{\phi_i^*(r_1)\phi_j(r_1)\phi_k^*(r_2)\phi_l(r_2)}{|r_1-r_2|}
     * \f]
     **/
    double compute_two_center_integral(
        Teuchos::RCP<const Basis> basis, 
        Epetra_Vector * i, Epetra_Vector * j, 
        Epetra_Vector * k, Epetra_Vector * l, 
        Teuchos::RCP<Poisson_Solver> poisson_solver
    );
    /**
     * @brief Compute Kji.
     * @param [in] mesh Mesh object which orbital_coeff is generated
     * @param [in] j orbital value phi(r2)
     * @param [in] i orbital value phi*(r2)
     * @param [in] poisson_solver poisson solver object which the two-electron integral is calculated.
     * @param [out] output calculated Kji value
     * @return calculated Kji value.
     * @details
     * The return value can be written as the following mulliken symbol.
     * \f[
     *     K_{ji}(r_1)  = \int dr_2 \frac{\phi_i^*(r_2)\phi_j(r_2)}{|r_1-r_2|}
     * \f]
     */
    int compute_Kji(
        Teuchos::RCP<const Basis> basis, 
        Epetra_Vector * j, Epetra_Vector * i, 
        Teuchos::RCP<Poisson_Solver> poisson_solver, 
        Teuchos::RCP<Epetra_Vector>& output
    );

    /**
     * @brief Compute Kji.
     * @param [in] mesh Mesh object which orbital_coeff is generated
     * @param [in] ji orbital value phi(r2)
     * @param [in] poisson_solver poisson solver object which the two-electron integral is calculated.
     * @param [out] output calculated Kji value
     * @return calculated Kji value.
     * @details
     * The return value can be written as the following mulliken symbol.
     * \f[
     *     K_{ji}(r_1)  = \int dr_2 \frac{\phi_i^*(r_2)\phi_j(r_2)}{|r_1-r_2|}
     * \f]
     */
    int compute_Kji(
        Teuchos::RCP<const Basis> basis,
        Epetra_Vector* ji,
        Teuchos::RCP<Poisson_Solver> poisson_solver, 
        Teuchos::RCP<Epetra_Vector>& output
    );
    /**
     * @brief Compute Kji.
     * @param [in] j orbital value phi(r2)
     * @param [in] i orbital value phi*(r2)
     * @param [in] poisson_solver poisson solver object which the two-electron integral is calculated.
     * @param [out] output calculated Kji value
     * @param [in] size size of the Kji vector
     * @return calculated Kji value.
     * @details
     * The return value can be written as the following mulliken symbol.
     * \f[
     *     K_{ji}(r_1)  = \int dr_2 \frac{\phi_i^*(r_2)\phi_j(r_2)}{|r_1-r_2|}
     * \f]
     */
    int compute_Kji(
        double* j,
        double* i,
        Teuchos::RCP<Poisson_Solver> poisson_solver,
        double* output,
        int size
    );

    /**
     * @brief Compute coulomb integral.
     * @param [in] mesh Mesh object which orbital_coeff is generated
     * @param [in] orbital_coeff Orbital "coefficient" of given basis function (Sinc), not orbital "value".
     * @param [in] i phi*(r2) index
     * @param [in] j phi(r2) index
     * @param [in] poisson_solver poisson solver object which the coulomb integral is calculated.
     * @return calculated coulomb integral value.
     * @details
     * The return value can be written as the following mulliken symbol.
     * \f[
     *     (ii|jj) = \iint dr_1 dr_2 \frac{\phi_i^*(r_1)\phi_i(r_1)\phi_j^*(r_2)\phi_j(r_2)}{|r_1-r_2|} 
     * \f]
     */
    double compute_coulomb_integral(
        Teuchos::RCP<const Basis> basis,
        Teuchos::RCP<const Epetra_MultiVector> orbital_coeff,
        int i, int j,
        Teuchos::RCP<Poisson_Solver> poisson_solver
    );

    /**
     * @brief Compute coulomb integral.
     * @param [in] mesh Mesh object which orbital_coeff is generated
     * @param [in] i orbital value phi*(r2)
     * @param [in] j orbital value phi(r2)
     * @param [in] poisson_solver poisson solver object which the coulomb integral is calculated.
     * @return calculated coulomb integral value.
     * @details
     * The return value can be written as the following mulliken symbol.
     * \f[
     *     (ii|jj) = \iint dr_1 dr_2 \frac{\phi_i^*(r_1)\phi_i(r_1)\phi_j^*(r_2)\phi_j(r_2)}{|r_1-r_2|} 
     * \f]
     */
    double compute_coulomb_integral(
        Teuchos::RCP<const Basis> basis,
        Teuchos::RCP<Epetra_Vector> i, Teuchos::RCP<Epetra_Vector> j,
        Teuchos::RCP<Poisson_Solver> poisson_solver
    );

    /**
     * @brief Compute exchange integral.
     * @param [in] mesh Mesh object which orbital_coeff is generated
     * @param [in] orbital_coeff Orbital "coefficient" of given basis function (Sinc), not orbital "value".
     * @param [in] i phi*(r2) index
     * @param [in] j phi(r2) index
     * @param [in] poisson_solver poisson solver object which the coulomb integral is calculated.
     * @return calculated exchange integral value.
     * @details
     * The return value can be written as the following mulliken symbol.
     * \f[
     *     (ij|ji) = \iint dr_1 dr_2 \frac{\phi_i^*(r_1)\phi_j(r_1)\phi_j^*(r_2)\phi_i(r_2)}{|r_1-r_2|}
     * \f]
     */
    double compute_exchange_integral(
        Teuchos::RCP<const Basis> basis,
        Teuchos::RCP<const Epetra_MultiVector> orbital_coeff,
        int i, int j,
        Teuchos::RCP<Poisson_Solver> poisson_solver
    );
    /**
     * @brief Compute exchange integral.
     * @param [in] mesh Mesh object which orbital_coeff is generated
     * @param [in] i orbital value phi*(r2)
     * @param [in] j orbital value phi(r2)
     * @param [in] poisson_solver poisson solver object which the coulomb integral is calculated.
     * @return calculated exchange integral value.
     * @details
     * The return value can be written as the following mulliken symbol.
     * \f[
     *     (ij|ji) = \iint dr_1 dr_2 \frac{\phi_i^*(r_1)\phi_j(r_1)\phi_j^*(r_2)\phi_i(r_2)}{|r_1-r_2|}
     * \f]
     */
    double compute_exchange_integral(
        Teuchos::RCP<const Basis> basis,
        Teuchos::RCP<Epetra_Vector> i, Teuchos::RCP<Epetra_Vector> j,
        Teuchos::RCP<Poisson_Solver> poisson_solver
    );
}
