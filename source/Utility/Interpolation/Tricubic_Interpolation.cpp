#include "Tricubic_Interpolation.hpp"
#include <limits>
#include <cmath>
#include "../Verbose.hpp"
#include "../Parallel_Util.hpp"

#define _ACE_TRICUBIC_CUTOFF_ (1.0E-15)
#define __DEBUG__

using std::vector;
using Teuchos::RCP;
using Teuchos::rcp;
using Teuchos::rcp_implicit_cast;

void Interpolation::Tricubic::interpolate(
        RCP<const Basis> before_basis, 
        RCP<Epetra_MultiVector> before_values, 
        RCP<const Basis> after_basis, 
        RCP<Epetra_MultiVector> &after_values, 
        double * after_center/* = NULL*/ 
){
    if( *before_basis == *after_basis ){
        after_values -> Update( 1.0, *before_values, 0.0 );
    }

    int size = before_basis -> get_original_size();
    int beforeNumMyElements = before_values -> Map().NumMyElements();
    int* beforeMyGlobalElements = before_values -> Map().MyGlobalElements();
    int afterNumMyElements = after_values -> Map().NumMyElements();
    int* afterMyGlobalElements = after_values -> Map().MyGlobalElements();

    double center[3];
    if( after_center == NULL ){
        for( int i = 0; i < 3; ++i){
            center[i] = 0.0;
        }
    } else {
        for( int i = 0; i < 3; ++i){
            center[i] = after_center[i];
        }
    }

    for (int k=0;k<before_values->NumVectors();k++){
        double *tmpval = new double [size]();
        double *srcval = new double [size]();
        memset(tmpval, 0.0, sizeof(double)*size);
        memset(srcval, 0.0, sizeof(double)*size);

        for(int j=0; j<beforeNumMyElements; j++){
            tmpval[beforeMyGlobalElements[j]] = before_values->operator[](k)[j];
        }
        Parallel_Util::group_sum(tmpval, srcval, size);
        delete[] tmpval;

        for (int j = 0; j < afterNumMyElements; j++ ){
            int j_x=0,j_y=0,j_z=0;

            double x_p, y_p, z_p;
            after_basis -> get_position(afterMyGlobalElements[j], x_p, y_p, z_p);
            
            double x, y, z;
            x = x_p - center[0];
            y = y_p - center[1];
            z = z_p - center[2];
            
            double interpolated = Interpolation::Tricubic::calculate(before_basis, srcval, x, y, z );
            after_values -> operator()(k) -> operator[](j) = interpolated;

        }
        delete[] srcval;
    }

    /*
    // Debug (Check norm convergence)
    double before_scaling = before_mesh -> get_scaling()[0]*before_mesh -> get_scaling()[1]*before_mesh -> get_scaling()[2];
    double after_scaling  = after_mesh  -> get_scaling()[0]*after_mesh  -> get_scaling()[1]*after_mesh  -> get_scaling()[2];
    RCP<Epetra_Vector> b_unitvect = rcp(new Epetra_Vector(*before_mesh -> get_map(), false));
    RCP<Epetra_Vector> a_unitvect = rcp(new Epetra_Vector(*after_mesh -> get_map(), false));
    b_unitvect -> PutScalar(1.0); a_unitvect -> PutScalar(1.0);
    for(int k = 0; k < after_values -> NumVectors(); ++k){
        double before_norm, after_norm;
        before_values -> operator()(k) -> Dot(*b_unitvect, &before_norm);
        after_values  -> operator()(k) -> Dot(*a_unitvect, &after_norm);
        before_norm *= before_scaling; after_norm *= after_scaling;

        if (abs(after_norm) < 0.01){
            Verbose::all() << "norm value is too small!! division error. " << std::endl << "before_norm = " << before_norm << " vs. " << "after_norm = " << after_norm << " for " << k << std::endl;
        }
        else{
            if(std::abs(before_norm/after_norm - 1) > 0.1){
                Verbose::all() << "Tricubic_Interpolation did not conserved norm! " << before_norm << " vs. " << after_norm << " for " << k << std::endl;
                throw std::runtime_error("Tricubic_Interpolation did not conserved norm!");
            }
        }
    }
    */
}

void Interpolation::Tricubic::interpolate(
        RCP<const Basis> before_basis, 
        RCP<Epetra_Vector> before_values, 
        RCP<const Basis> after_basis, 
        RCP<Epetra_Vector> &after_values, 
        double * after_center/* = NULL*/ 
){
    RCP<Epetra_MultiVector> after_values2 = rcp_implicit_cast<Epetra_MultiVector>(after_values);
    Interpolation::Tricubic::interpolate( 
        before_basis, rcp_implicit_cast<Epetra_MultiVector>(before_values), 
        after_basis, after_values2, after_center );
    if( after_values2 != after_values ){
        Verbose::all() << "Tricubic_Interpolation::interpolate Epetra_Vector to Epetra_Vector check!" << std::endl;
        after_values -> Update( 1.0, *after_values2 -> operator()(0), 0.0 );
    }
}

std::vector<double> Interpolation::Tricubic::interpolate(
        RCP<const Basis> before_basis, 
        RCP<Epetra_MultiVector> before_values, 
        int row, std::vector< std::array<double,3> > positions
){

    int size = before_basis -> get_original_size();
    int beforeNumMyElements = before_values -> Map().NumMyElements();
    int* beforeMyGlobalElements = before_values -> Map().MyGlobalElements();

    double *tmpval = new double [size]();
    double *srcval = new double [size]();
    memset(tmpval, 0.0, sizeof(double)*size);
    memset(srcval, 0.0, sizeof(double)*size);

    for(int j=0; j<beforeNumMyElements; j++){
        tmpval[beforeMyGlobalElements[j]] = before_values->operator[](row)[j];
    }
    Parallel_Util::group_sum(tmpval, srcval, size);
    delete[] tmpval;

    vector<double> retvals;

    for(int n = 0; n < positions.size(); ++n){
        double x = positions[n][0];
        double y = positions[n][1];
        double z = positions[n][2];

        double interpolated = Interpolation::Tricubic::calculate( before_basis, srcval, x, y, z );
        retvals.push_back(interpolated);
    }

    delete[] srcval;

    return retvals;
}

std::vector<double> Interpolation::Tricubic::interpolate(
        RCP<const Basis> before_basis, 
        RCP<Epetra_Vector> before_values, 
        std::vector< std::array<double,3> > positions
){
    return Interpolation::Tricubic::interpolate( before_basis, rcp_implicit_cast<Epetra_MultiVector>(before_values), 0, positions );
}

double Interpolation::Tricubic::calculate(
    RCP<const Basis> before_basis, 
    double * srcval,
    double x, double y, double z
){
    const double ** before_scaled_grid = before_basis -> get_scaled_grid();
    std::array<double,3> before_scaling = before_basis -> get_scaling();
    int size = before_basis -> get_original_size();

    vector< vector< vector<int> > > combined_neighbors;
    vector< vector<int> > decomposed_neighbors;
    Interpolation::Tricubic::find_neighbors( before_basis, x, y, z, decomposed_neighbors, combined_neighbors);

    if( std::abs(decomposed_neighbors[1][0]-x) < _ACE_TRICUBIC_CUTOFF_ and
        std::abs(decomposed_neighbors[1][1]-y) < _ACE_TRICUBIC_CUTOFF_ and
        std::abs(decomposed_neighbors[1][2]-z) < _ACE_TRICUBIC_CUTOFF_){
        return srcval[combined_neighbors[1][1][1]];
    }

    //if( (decomposed_neighbors[0][0] < 0 && decomposed_neighbors[1][0] < 0) ||
    //        (decomposed_neighbors[0][1] < 0 && decomposed_neighbors[1][1] < 0) ||
    //        (decomposed_neighbors[0][2] < 0 && decomposed_neighbors[1][2] < 0) ){
    if(decomposed_neighbors[1][0] < 0 or decomposed_neighbors[1][1] < 0 or decomposed_neighbors[1][2] < 0){
        if(before_basis -> get_periodicity() > 0){
            Verbose::all() << "point " << x << ", " << y << ", " << z << ": out of before_grid" << std::endl;
            throw std::invalid_argument("Point out of before_grid (Tricubic interpolation");
        }
        return 0.0;
    }

    double x1_x, y1_y, z1_z, r1_r;
    before_basis -> find_nearest_displacement(combined_neighbors[1][1][1], x, y, z, x1_x, y1_y, z1_z, r1_r);
    double x_x1 = -x1_x; double y_y1 = -y1_y; double z_z1 = -z1_z;

    vector< vector<double> > z_interpolated;
    z_interpolated.resize(4);
    for( int a = 0; a < 4; ++a ){
        z_interpolated[a].resize(4);
    }
    for( int a = 0; a < 4; ++a ){
        for( int b = 0; b < 4; ++b ){
            double vals[4];
            for( int c = 0; c < 4; ++c ){
                //conbined_neighbors[a][b][c] == -2 when the (a,b,c) is in out of the box, see Grid_Basic::combine()
                if( combined_neighbors[a][b][c] < 0 || combined_neighbors[a][b][c] > size ){
                    vals[c] = 0.0;
                } else {
                    vals[c] = srcval[combined_neighbors[a][b][c]];
                }
                //Verbose::all() << "vals["<<a<<"]["<<b<<"]["<<c<<"] = " << vals[c] << std::endl;
            }

            double z0[4];
            for(int r = 0; r < 4; ++r){
                if( decomposed_neighbors[r][2] >= 0 ){
                    z0[r] = before_scaled_grid[2][decomposed_neighbors[r][2]];// z0 = z0[1]!
                }else{
                    z0[r] = std::numeric_limits<double>::signaling_NaN();
                }
            }

            /*
            if( !std::isfinite(z0[2]) ){
                if(std::isfinite(z0[0])){
                    z0[2] = 2*z0[1]-z0[0];
                } else {
                    z0[2] = z0[1] + before_basis -> get_scaling()[2];
                }
            }
            */
            // Presume uniform grid, always.
            z0[0] = z0[1] - before_scaling[2]; z0[2] = z0[1] + before_scaling[2]; z0[3] = z0[1] + 2*before_scaling[2];

            /*
            // Use interpolated values for outside grid. Turning off will assume values outside the grid to 0.
            if( combined_neighbors[a][b][0] < 0 || combined_neighbors[a][b][0] > size ){
                vals[0] = 2*vals[1]-vals[2];
                //z0[0] = 2*z0[1]-z0[2];
            }
            if( combined_neighbors[a][b][3] < 0 || combined_neighbors[a][b][3] > size ){
                vals[3] = 2*vals[2]-vals[1];
                //z0[3] = 2*z0[2]-z0[1];
            }
            */

            double diff20 = (vals[2]-vals[0])/(z0[2]-z0[0]);
            double diff31 = (vals[3]-vals[1])/(z0[3]-z0[1]);
            double dz = z0[2]-z0[1];
            //z_z1 = z-z0[1]
            z_interpolated[a][b] = ( 2*vals[1] - 2*vals[2] + diff20*dz + diff31*dz )*pow(z_z1/dz, 3)
                + ( -3*vals[1] + 3*vals[2] - 2*diff20*dz - diff31*dz )*pow(z_z1/dz, 2)
                + ( diff20 )*z_z1 + vals[1];
#ifdef __DEBUG__
            if( !std::isfinite(z_interpolated[a][b])){
                Verbose::all() << std::scientific;
                Verbose::all() << "Tricubic_Interpolation:: result error! = " << z_interpolated[a][b] << std::endl;
                Verbose::all() << "Coordinates = " << x << ", " << y << ", " << z << std::endl;
                Verbose::all() << "Inform developers for hotfix" << std::endl;
                Verbose::all() << "z_interpolated vals["<<a<<"]["<<b<<"] = " << z_interpolated[a][b] << ", " << diff20*z_z1+vals[1] << std::endl;
                Verbose::all() << " cubic term = " << (2*vals[1]-2*vals[2]+diff20*dz+diff31*dz)*pow(z_z1/dz,3)
                    << " quadratic term = " << (-3*vals[1]+3*vals[2]-2*diff20*dz-diff31*dz)*pow(z_z1/dz, 2)
                    << " linear term = " << diff20*z_z1 << " constant = " << vals[1] << std::endl;
                Verbose::all() << "z-z0[1] = " << z_z1 << std::endl;
                Verbose::all() << "f'(0) = " << diff20 << ", f'(1) = " << diff31 << std::endl;
                Verbose::all() << "vals = " << vals[0] << ", " << vals[1] << ", " << vals[2] << ", " << vals[3] << std::endl;
                Verbose::all() << "z list = " << z0[0] << ", " << z0[1] << ", " << z0[2] << ", " << z0[3] << std::endl;
                throw std::runtime_error("Non-finite value detected!");
            }
#endif
        }
    }

    vector<double> yz_interpolated;
    yz_interpolated.resize(4);
    for( int a = 0; a < 4; ++a ){
        double vals[4];
        for(int b = 0; b < 4; ++b ){
            vals[b] = z_interpolated[a][b];
        }
        double y0[4];
        for(int r = 0; r < 4; ++r){
            y0[r] = before_scaled_grid[1][decomposed_neighbors[r][1]];// y0 = y0[1]!
        }

        /*
        for(int c = 1; c < 3; ++c){
            if( combined_neighbors[a][2][c] < 0 || combined_neighbors[a][2][c] > size ){
                y0[2] = 2*y0[1]-y0[0];
            }
            if( combined_neighbors[a][0][c] < 0 || combined_neighbors[a][0][c] > size ){
                //vals[0] = 2*vals[1]-vals[2];
                y0[0] = 2*y0[1]-y0[2];
            }
            if( combined_neighbors[a][3][c] < 0 || combined_neighbors[a][3][c] > size ){
                //vals[3] = 2*vals[2]-vals[1];
                y0[3] = 2*y0[2]-y0[1];
            }
        }
        */
        // Presume uniform grid, always.
        y0[0] = y0[1] - before_scaling[1]; y0[2] = y0[1] + before_scaling[1]; y0[3] = y0[1] + 2*before_scaling[1];

        //Verbose::all() << "y0[0] = " << y0[0] << ", y0[1] = " << y0[1] << ", y0[2] = " << y0[2] << ", y0[3] = " << y0[3] << std::endl;
        //Verbose::all() << "vals[0] = " << vals[0] << ", vals[1] = " << vals[1] << ", vals[2] = " << vals[2] << ", vals[3] = " << vals[3] << std::endl;
        double diff20 = (vals[2]-vals[0])/(y0[2]-y0[0]);
        double diff31 = (vals[3]-vals[1])/(y0[3]-y0[1]);
        //Verbose::all() << "diff0 = " << vals[2]-vals[0] << ", diff1 = " << vals[3]-vals[1] << std::endl;
        double dy = y0[2]-y0[1];
        //y_y1 = y-y0[1]
        yz_interpolated[a] = ( 2*vals[1] - 2*vals[2] + diff20*dy + diff31*dy )*pow(y_y1/dy, 3)
            + ( -3*vals[1] + 3*vals[2] - 2*diff20*dy - diff31*dy )*pow(y_y1/dy, 2)
            + ( diff20 )*y_y1 + vals[1];
        /*
        Verbose::all() << "yz_interpolated vals["<<a<<"] = " << yz_interpolated[a] << ", " << diff20*(y-y0[1])+vals[1] << std::endl;
        Verbose::all() << " cubic term = " << (2*vals[1]-2*vals[2]+diff20*dy+diff31*dy)*pow((y-y0[1])/dy,3)
                  << " quadratic term = " << (-3*vals[1]+3*vals[2]-2*diff20*dy-diff31*dy)*pow((y-y0[1])/dy, 2)
                  << " linear term = " << diff20*(y-y0[1]) << " constant = " << vals[1] << std::endl;
        Verbose::all() << "y-y0[1] = " << y-y0[1] << std::endl;
        Verbose::all() << "f'(0) = " << diff20 << ", f'(1) = " << diff31 << std::endl;
        */
#ifdef __DEBUG__
        if( !std::isfinite(yz_interpolated[a])){
            Verbose::all() << std::scientific;
            Verbose::all() << "Tricubic_Interpolation:: result error! = " << yz_interpolated[a] << std::endl;
            Verbose::all() << "Coordinates = " << x << ", " << y << ", " << z << std::endl;
            Verbose::all() << "Inform developers for hotfix" << std::endl;
            Verbose::all() << "yz_interpolated vals["<<a<<"] = " << yz_interpolated[a] << ", " << diff20*y_y1+vals[1] << std::endl;
            Verbose::all() << " cubic term = " << (2*vals[1]-2*vals[2]+diff20*dy+diff31*dy)*pow(y_y1/dy,3)
                << " quadratic term = " << (-3*vals[1]+3*vals[2]-2*diff20*dy-diff31*dy)*pow(y_y1/dy, 2)
                << " linear term = " << diff20*y_y1 << " constant = " << vals[1] << std::endl;
            Verbose::all() << "y-y0[1] = " << y_y1 << std::endl;
            Verbose::all() << "f'(0) = " << diff20 << ", f'(1) = " << diff31 << std::endl;
            Verbose::all() << "vals = " << vals[0] << ", " << vals[1] << ", " << vals[2] << ", " << vals[3] << std::endl;
            Verbose::all() << "y list = " << y0[0] << ", " << y0[1] << ", " << y0[2] << ", " << y0[3] << std::endl;
            throw std::runtime_error("Non-finite value detected!");
        }
#endif
    }

    double interpolated;
    double vals[4];
    for(int a = 0; a < 4; ++a ){
        vals[a] = yz_interpolated[a];
    }
    double x0[4];
    for(int r = 0; r < 4; ++r){
        x0[r] = before_scaled_grid[0][decomposed_neighbors[r][0]];// x0 = x[1]!
    }
    /*
    for(int b = 1; b < 3; ++b){
        for(int c = 1; c < 3; ++c){
            if( combined_neighbors[2][b][c] < 0 || combined_neighbors[2][b][c] > size ){
                x0[2] = 2*x0[1]-x0[0];
            }
            if( combined_neighbors[0][b][c] < 0 || combined_neighbors[0][b][c] > size ){
                //vals[0] = 2*vals[1]-vals[2];
                x0[0] = 2*x0[1]-x0[2];
            }
            if( combined_neighbors[3][b][c] < 0 || combined_neighbors[3][b][c] > size ){
                //vals[3] = 2*vals[2]-vals[1];
                x0[3] = 2*x0[2]-x0[1];
            }
        }
    }
    */
    // Presume uniform grid, always.
    x0[0] = x0[1] - before_scaling[0]; x0[2] = x0[1] + before_scaling[0]; x0[3] = x0[1] + 2*before_scaling[0];

    double diff20 = (vals[2]-vals[0])/(x0[2]-x0[0]);
    double diff31 = (vals[3]-vals[1])/(x0[3]-x0[1]);
    double dx = x0[2]-x0[1];
    //x_x1 = x - x0[1]
    interpolated = ( 2*vals[1] - 2*vals[2] + diff20*dx + diff31*dx )*pow(x_x1/dx, 3)
        + ( -3*vals[1] + 3*vals[2] - 2*diff20*dx - diff31*dx )*pow(x_x1/dx, 2)
        + ( diff20 )*x_x1 + vals[1];
    //Verbose::all() << "xyz_interpolated vals = " << interpolated << ", " << diff20*(x-x0[1])+vals[1] << std::endl;

    if( !std::isfinite(interpolated)){
        Verbose::all() << std::scientific;
        Verbose::all() << "Tricubic_Interpolation:: result error! = " << interpolated << std::endl;
        Verbose::all() << "Coordinates = " << x << ", " << y << ", " << z << std::endl;
        Verbose::all() << "Inform developers for hotfix" << std::endl;
#ifdef __DEBUG__
        Verbose::all() << "xyz_interpolated vals = " << interpolated << ", " << diff20*x_x1+vals[1] << std::endl;
        Verbose::all() << " cubic term = " << (2*vals[1]-2*vals[2]+diff20*dx+diff31*dx)*pow(x_x1/dx,3)
                  << " quadratic term = " << (-3*vals[1]+3*vals[2]-2*diff20*dx-diff31*dx)*pow(x_x1/dx, 2)
                  << " linear term = " << diff20*x_x1 << " constant = " << vals[1] << std::endl;
        Verbose::all() << "x-x0[1] = " << x_x1 << std::endl;
        Verbose::all() << "f'(0) = " << diff20 << ", f'(1) = " << diff31 << std::endl;
        Verbose::all() << "vals = " << vals[0] << ", " << vals[1] << ", " << vals[2] << ", " << vals[3] << std::endl;
        Verbose::all() << "x list = " << x0[0] << ", " << x0[1] << ", " << x0[2] << ", " << x0[3] << std::endl;
#endif
        throw std::runtime_error("Non-finite value detected!");
    }
    return interpolated;
}

/*
 * decomposed_index: input of scaled_grid = [i][axis]
 * combined_index: index for Vector = [x_i][y_i][z_i]
 * 
 * i, x_i, y_i, z_i info
 * .: grid point, @: desired point
 * .  .@ .  .
 * 0  1  2  3
 * .  @  .  .
 * 0  1  2  3
 * (If desired point is on grid point, it gets index 1.
 */
void Interpolation::Tricubic::find_neighbors( 
        RCP<const Basis> basis, 
        double x, double y, double z, 
        std::vector< std::vector<int> > &decomposed_index, 
        std::vector< std::vector< std::vector<int> > > &combined_index 
    ){

    std::array<int,3> grid_size = basis -> get_points();
    const double ** scaled_grid = basis -> get_scaled_grid();

    decomposed_index.clear();
    decomposed_index.resize(4);
    for(int i = 0; i < 4; ++i){
        decomposed_index[i].resize(3, -1);
    }

    double r[3];
    r[0] = x;
    r[1] = y;
    r[2] = z;

    if (basis->get_periodicity() == 0){
        for(int i = 0; i < 3; ++i){
            for(int j = 0; j < grid_size[i]-1; ++j){
                if( scaled_grid[i][j] <= r[i] && scaled_grid[i][j+1] > r[i] ){
                    decomposed_index[0][i] = j-1;
                    decomposed_index[1][i] = j;
                    decomposed_index[2][i] = j+1;
                    decomposed_index[3][i] = j+2;
                    break;
                }
            }
            if( scaled_grid[i][grid_size[i]-1] == r[i] ){
                decomposed_index[0][i] = grid_size[i]-2;
                decomposed_index[1][i] = grid_size[i]-1;
                if(i < basis -> get_periodicity()){
                    // This axis is periodic
                    decomposed_index[2][i] = 0;
                    decomposed_index[3][i] = 1;
                }
            }
            if(i < basis -> get_periodicity()){
                for(int j = 0; j < 4; ++j){
                    decomposed_index[j][i] %= grid_size[i];
                    if(decomposed_index[j][i] < 0){
                        decomposed_index[j][i] += grid_size[i];
                    }
                }
            }
        }
    }
    // (PBC)
    // Note that in PBC the shape of grid is different from noPBC grid. 
    else{
        std::array<double,3> scaling = basis -> get_scaling();
        for(int i = 0; i < 3; ++i){
            for(int j = 0; j < grid_size[i]; ++j){
                if( scaled_grid[i][j] <= r[i] && scaled_grid[i][j] + scaling[i] > r[i] ){
                    // scaled_grid[i][j+1] == scaled_grid[i][j] + scaling[i]
                    decomposed_index[0][i] = j-1;
                    decomposed_index[1][i] = j;
                    decomposed_index[2][i] = j+1;
                    decomposed_index[3][i] = j+2;
                    break;
                }
            }

            if (scaled_grid[i][grid_size[i]-1] + scaling[i] == r[i]){
                decomposed_index[0][i] = grid_size[i]-1;
            }

            if (i < basis ->get_periodicity()){
                // This axis is periodic
                for (int j=0; j < 4; ++j){
                    decomposed_index[j][i] %= grid_size[i];
                    if (decomposed_index[j][i] < 0){
                        decomposed_index[j][i] += grid_size[i];
                    }
                }
                
                //if (grid[i][grid_size[i]-2] <= r[i] && r[i] < grid[i][grid_size[i]-1]){
                //    decomposed_index[3][i] = 0;
                //}
                //if (grid[i][grid_size[i]-1] <= r[i] && r[i] < grid[i][grid_size[i]-1] + scaling[i]){
                //    decomposed_index[2][i] = 0;
                //    decompoxed_index[3][i] = 1;
                //}
                
                if (scaled_grid[i][grid_size[i]-1] + scaling[i] == r[i]){
                    decomposed_index[0][i] = grid_size[i]-1;
                    decomposed_index[1][i] = 0;
                    decomposed_index[2][i] = 1;
                    decomposed_index[3][i] = 2;
                }
            }
            //if (grid[i][grid_size[i]-2] <= r[i] && r[i] < grid[i][grid_size[i]-1]){
            //    decomposed_index[3][i] = 0;
            //}
            //if (grid[i][grid_size[i]-1] <= r[i] && r[i] < grid[i][grid_size[i]-1] + scaling[i]){
            //    decomposed_index[2][i] = 0;
            //    decompoxed_index[3][i] = 1;
            //}
            
            if (scaled_grid[i][grid_size[i]-1] + scaling[i] == r[i]){
                decomposed_index[0][i] = grid_size[i]-1;
                decomposed_index[1][i] = 0;
                decomposed_index[2][i] = 1;
                decomposed_index[3][i] = 2;
            }
        }
    }
    //Verbose::single() << "find_neighbor " << x << ", " << y << ", " << z << " = " << grid[0][decomposed_index[1][0]] << " " << grid[1][decomposed_index[1][1]] << " " << grid[1][decomposed_index[1][2]] << std::endl;

    combined_index.clear();
    combined_index.resize(4);
    for(int i = 0; i < 4; ++i){
        combined_index[i].resize(4);
        for(int j = 0; j < 4; ++j){
            combined_index[i][j].resize(4, -1);
        }
    }

    for(int i = 0; i < 4; ++i){
        for(int j = 0; j < 4; ++j){
            for(int k = 0; k < 4; ++k){
                combined_index[i][j][k] = basis -> combine( decomposed_index[i][0], decomposed_index[j][1], decomposed_index[k][2] );
            }
        }
    }
}

/*
#include "Epetra_Comm.h"
#include "Epetra_SerialComm.h"
#include "Teuchos_ParameterList.hpp"
#include "../Basis/Create_Basis.hpp"
int main(){
    int points = 4;
    int mid = (points-1)/2;
    Epetra_SerialComm comm;
    Verbose::single(0);
    RCP<Teuchos::ParameterList> parameters = rcp( new Teuchos::ParameterList() );
    parameters -> sublist("BasicInformation").set("ScalingX", 0.5);
    parameters -> sublist("BasicInformation").set("ScalingY", 0.5);
    parameters -> sublist("BasicInformation").set("ScalingZ", 0.5);
    parameters -> sublist("BasicInformation").set("AllowOddPoints", 1);
    parameters -> sublist("BasicInformation").set("PointX", 4);
    parameters -> sublist("BasicInformation").set("PointY", 4);
    parameters -> sublist("BasicInformation").set("PointZ", 4);
    parameters -> sublist("BasicInformation").set("Grid", "Basic");
    parameters -> sublist("BasicInformation").set("Basis", "Sinc");
    RCP<Basis> basis = Create_Basis::Create_Basis(comm, parameters);
    RCP<Epetra_Vector> test = rcp( new Epetra_Vector( *basis -> get_map() ) );

    *
    for(int i = 0; i < points; ++i){
        int index = basis -> combine(mid, mid, i);
        if(index > 0) test -> operator[](index) = i;
    }
    for(int i = 0; i < points; ++i){
        int index = basis -> combine(mid, mid+1, i);
        if(index > 0) test -> operator[](index) = i;
    }
    for(int i = 0; i < points; ++i){
        int index = basis -> combine(mid, mid-1, i);
        if(index > 0) test -> operator[](index) = i-1;
    }
    for(int i = 0; i < points; ++i){
        int index = basis -> combine(mid, mid+2, i);
        if(index > 0) test -> operator[](index) = i-1;
    }
    *
    for(int i = 0; i < 4; ++i){
        for(int j = 0; j < 4; ++j){
            int index;
            index = basis -> combine(i, j, 0);
            test -> operator[](index) = -2;
            index = basis -> combine(i, j, 1);
            test -> operator[](index) = -1;
            index = basis -> combine(i, j, 2);
            test -> operator[](index) = 1;
            index = basis -> combine(i, j, 3);
            test -> operator[](index) = 2;
        }
    }
    std::vector<double> offset(3, 0.0);
    basis -> write_along_axis("test0", 0, test, offset);
    basis -> write_along_axis("test0", 1, test, offset);
    basis -> write_along_axis("test0", 2, test, offset);
    //offset[1] = 0.25;
    //basis -> write_along_axis("test1", 2, test, offset);
    //offset[1] = 0.5;
    //basis -> write_along_axis("test2", 2, test, offset);
    return 0;
}
// */
