#include "Radial_Grid_Paw.hpp"
#include <iostream>
#include <cmath>
#include "Spline_Interpolation.hpp"
//#include "Linear_Interpolation.hpp"
#include "../ACE_Config.hpp"
#include "../Math/Spherical_Harmonics.hpp"
#include "../Math/Spherical_Harmonics_Derivative.hpp"
#include "../Value_Coef.hpp"
#include "../Verbose.hpp"
#include "omp.h"

#define RGD2GD_CUTOFF 1.0E-20
#define HARTREE_CUTOFF 1.0E-50

using std::max;
using std::min;
using std::vector;
using std::abs;
using Teuchos::RCP;
using Spherical_Harmonics::Ylm;
using Spherical_Harmonics::Derivative::dYlm;

vector<double> Radial_Grid::Paw::sum( vector<double> data1, vector<double> data2 ){
    vector<double> retval;
    retval.resize( min(data1.size(), data2.size()) );

    for(int i = 0; i < retval.size(); ++i){
        retval[i] = data1[i] + data2[i];
    }
    return retval;
}

double Radial_Grid::Paw::radial_integrate( vector<double> data1, vector<double> data2, vector<double> grid_val, double cutoff/* = -1.0*/ ){
    double retval = 0.0;
    int size = grid_val.size();
    if(size != data1.size() or size != data2.size()){
        std::cout << "Radial_Grid::Paw::radial_integrate error!" << std::endl
                  << "Grid size          : " << size << std::endl
                  << "data1 size         : " << data1.size() << std::endl
                  << "data2 size         : " << data2.size() << std::endl;
        exit(EXIT_FAILURE);
    }

    // trapezoidal rule with spherical coordinate volume factor radial part (r^2)
    for( int i = 0; i < size-1; i++){
        retval += (data1[i]*data2[i] * grid_val[i]*grid_val[i] + data1[i+1]*data2[i+1] * grid_val[i+1]*grid_val[i+1]) / 2.0 * (grid_val[i+1]-grid_val[i]);
        if( cutoff >= 0.0 && grid_val[i] > cutoff ){
            break;
        }
    }
    return retval;
}

double Radial_Grid::Paw::linear_integrate( vector<double> data1, vector<double> data2, vector<double> grid_val, double cutoff/* = -1.0*/ ){
    double retval = 0.0;
    int size = grid_val.size();
    if(size != data1.size() or size != data2.size()){
        std::cout << "Radial_Grid::Paw::linear_integrate error!" << std::endl
                  << "Grid size          : " << size << std::endl
                  << "data1 size         : " << data1.size() << std::endl
                  << "data2 size         : " << data2.size() << std::endl;
        exit(EXIT_FAILURE);
    }

    // trapezoidal rule
    for( int i = 0; i < size-1; i++){
        retval += (data1[i]*data2[i] + data1[i+1]*data2[i+1] ) / 2.0 * (grid_val[i+1]-grid_val[i]);
        if( cutoff >= 0.0 && grid_val[i] > cutoff ){
            break;
        }
    }
    return retval;
}

vector<double> Radial_Grid::Paw::linear_derivative( vector<double> data, vector<double> grid_val ){
    vector<double> retval;
    retval.resize( grid_val.size() );

    // cant use central finite difference, since it is not uniform mesh.
    // backward finite difference, accuracy 1
    retval[0] = 0.0;
    for( int i = 1; i < grid_val.size(); i++){
        retval[i] = (-data[i-1] + data[i] ) / (grid_val[i]-grid_val[i-1]);
    }
    return retval;
}

double Radial_Grid::Paw::radial_integrate( vector<double> data1, vector<double> data2, vector<double> grid_val, vector<double> grid_derivative_val, double cutoff/* = -1.0*/ ){
    double retval = 0.0;
    int size = grid_val.size();
    if(size != data1.size() or size != data2.size() or size != grid_derivative_val.size()){
        std::cout << "Radial_Grid::Paw::radial_integrate error!" << std::endl
                  << "Grid size          : " << size << std::endl
                  << "Grid drivative size: " << grid_derivative_val.size() << std::endl
                  << "data1 size         : " << data1.size() << std::endl
                  << "data2 size         : " << data2.size() << std::endl;
        exit(EXIT_FAILURE);
    }

    for( int i = 0; i < size; i++){
        retval += (data1[i]*data2[i] * grid_val[i]*grid_val[i]) * grid_derivative_val[i];
        if( cutoff >= 0.0 && grid_val[i] > cutoff ){
            break;
        }
    }
    return retval;
}

double Radial_Grid::Paw::linear_integrate( vector<double> data1, vector<double> data2, vector<double> grid_val, vector<double> grid_derivative_val, double cutoff/* = -1.0*/ ){
    double retval = 0.0;
    int size = grid_val.size();
    if(size != data1.size() or size != data2.size() or size != grid_derivative_val.size()){
        std::cout << "Radial_Grid::Paw::linear_integrate error!" << std::endl
                  << "Grid size          : " << size << std::endl
                  << "Grid drivative size: " << grid_derivative_val.size() << std::endl
                  << "data1 size         : " << data1.size() << std::endl
                  << "data2 size         : " << data2.size() << std::endl;
        exit(EXIT_FAILURE);
    }

    for( int i = 0; i < size; i++){
        retval += (data1[i]*data2[i]) * grid_derivative_val[i];
        if( cutoff >= 0.0 && grid_val[i] > cutoff ){
            break;
        }
    }
    return retval;
}

vector<double> Radial_Grid::Paw::linear_derivative( vector<double> data, vector<double> grid_val, vector<double> grid_derivative ){
    int size = grid_val.size();
    vector<double> retval;
    retval.resize( size );

    retval[0] = ( data[1]-data[0] )/ grid_derivative[0];
    for( int i = 1; i < size-1 ; i++){
        retval[i] = (data[i+1] - data[i-1] ) / grid_derivative[i] / 2;
    }
    retval[size-1] = ( data[size-1]-data[size-2] )/ grid_derivative[size-1];
    return retval;
}

// dest size == grid_setting -> get_original_size
void Radial_Grid::Paw::get_gradient_from_radial_grid(
    int l, int m, vector<double> src, vector<double> src_grid,
    std::array<double,3> src_center,
    RCP<const Basis> dest_basis, RCP<Epetra_MultiVector> &retval,
    double rc/* = -1.0*/
){
    if( rc < 0.0 ){
        rc = 2*src_grid.back();
    }

    vector<double> grad = Radial_Grid::Paw::linear_derivative(src, src_grid);

    //*
    int grad_dim = grad.size();
    double ygp1, ygpn;
    if( 1.0E30 * abs(grad[1]-grad[0]) < abs(src_grid[1]-src_grid[0]) ){
        ygp1 = 0.0;
    } else {
        ygp1 = (grad[1]-grad[0])/(src_grid[1]-src_grid[0]);
    }
    if( 1.0E30 * abs(grad[grad_dim-1]-grad[grad_dim-2]) < abs(src_grid[grad_dim-1]-src_grid[grad_dim-2]) ){
        ygpn = 0.0;
    } else {
        ygpn = (grad[grad_dim-1]-grad[grad_dim-2])/(src_grid[grad_dim-1]-src_grid[grad_dim-2]);
    }

    double yp1, ypn;
    if( 1.0E30 * abs(src[1]-src[0]) < abs(src_grid[1]-src_grid[0]) ){
        yp1 = 0.0;
    } else {
        yp1 = (src[1]-src[0])/(src_grid[1]-src_grid[0]);
    }
    if( 1.0E30 * abs(src[grad_dim-1]-src[grad_dim-2]) < abs(src_grid[grad_dim-1]-src_grid[grad_dim-2]) ){
        ypn = 0.0;
    } else {
        ypn = (src[grad_dim-1]-src[grad_dim-2])/(src_grid[grad_dim-1]-src_grid[grad_dim-2]);
    }

    vector<double> y2;
    y2 = Interpolation::Spline::spline( src_grid, src, grad_dim, yp1, ypn );
    vector<double> yg2;
    yg2 = Interpolation::Spline::spline( src_grid, grad, grad_dim, ygp1, ygpn );
    // */

    int dim = dest_basis -> get_original_size();

    vector< vector<double> > retval_tmp(3);
    vector< vector<int> > retind_tmp(3);
#ifdef ACE_HAVE_OMP
#pragma omp parallel for schedule(runtime)
#endif
    for(int i = 0; i < dim; ++i ){
        if( retval -> Map().MyGID(i) ){
            double xi, yi, zi, r;
            // (PBC)
            dest_basis->find_nearest_displacement(i, src_center[0], src_center[1], src_center[2], xi, yi, zi, r);

            if( r < 1.0E-15 ){
                double new_r = src_grid[1];
                double R = src[1];
                double dR = grad[1];

                double val;
                val = Ylm(l, m, new_r, 0, 0) * dR + R * dYlm(l, m, new_r, 0, 0, 0);
                val += -Ylm(l, m, -new_r, 0, 0) * dR + R * dYlm(l, m, -new_r, 0, 0, 0);
                val *= 0.5;
#ifdef ACE_HAVE_OMP
#pragma omp critical
#endif
                {
                    retind_tmp[0].push_back(i);
                    retval_tmp[0].push_back(val);
                }
                val = Ylm(l, m, 0, new_r, 0) * dR + R * dYlm(l, m, 0, new_r, 0, 1);
                val += -Ylm(l, m, 0, -new_r, 0) * dR + R * dYlm(l, m, 0, -new_r, 0, 1);
                val *= 0.5;
#ifdef ACE_HAVE_OMP
#pragma omp critical
#endif
                {
                    retind_tmp[1].push_back(i);
                    retval_tmp[1].push_back(val);
                }
                val = Ylm(l, m, 0, 0, new_r) * dR + R * dYlm(l, m, 0, 0, new_r, 2);
                val += -Ylm(l, m, 0, 0, -new_r) * dR + R * dYlm(l, m, 0, 0, -new_r, 2);
                val *= 0.5;
#ifdef ACE_HAVE_OMP
#pragma omp critical
#endif
                {
                    retind_tmp[2].push_back(i);
                    retval_tmp[2].push_back(val);
                }
            } else if( r <= src_grid.back() && r <= rc ) {
                double R = Interpolation::Spline::splint( src_grid, src, y2, grad_dim, r);
                double dR = Interpolation::Spline::splint( src_grid, grad, yg2, grad_dim, r);
                //double R = Interpolation::Linear::linear_interpolate( r, src_grid, src);
                //double dR = Interpolation::Linear::linear_interpolate( r, src_grid, grad);
                double Y = Ylm(l, m, xi, yi, zi);

                vector<double> dY(3);
                for(int d = 0; d < 3; ++d){
                    dY[d] = dYlm(l, m, xi, yi, zi, d);
                }

                double val;
                val = Y * dR*xi/r + R * dY[0];
#ifdef ACE_HAVE_OMP
#pragma omp critical
#endif
                if( abs(val) > RGD2GD_CUTOFF ){
                    retind_tmp[0].push_back(i);
                    retval_tmp[0].push_back(val);
                }
                val = Y * dR*yi/r + R * dY[1];
#ifdef ACE_HAVE_OMP
#pragma omp critical
#endif
                if( abs(val) > RGD2GD_CUTOFF ){
                    retind_tmp[1].push_back(i);
                    retval_tmp[1].push_back(val);
                }
                val = Y * dR*zi/r + R * dY[2];
#ifdef ACE_HAVE_OMP
#pragma omp critical
#endif
                if( abs(val) > RGD2GD_CUTOFF ){
                    retind_tmp[2].push_back(i);
                    retval_tmp[2].push_back(val);
                }
            }
        }
    }
    for(int d = 0; d < 3; ++d){
        retval -> operator()(d) -> ReplaceGlobalValues(retind_tmp[d].size(), retval_tmp[d].data(), retind_tmp[d].data());
    }
}


// dest size == grid_setting -> get_original_size
void Radial_Grid::Paw::Interpolate_from_radial_grid( int l, int m, vector<double> src, vector<double> src_grid, std::array<double,3> src_center, RCP<const Basis> dest_basis, vector<double> &dest_val, vector<int> &dest_ind, double rc/* = NULL*/){
    dest_val.clear();
    dest_ind.clear();

    if( rc < 0.0 ){
        rc = 2*src_grid.back();
    }

    //*
    int src_dim = src.size();
    double yp1, ypn;
    if( 1.0E30 * abs(src[1]-src[0]) < abs(src_grid[1]-src_grid[0]) ){
        yp1 = 0.0;
    } else {
        yp1 = (src[1]-src[0])/(src_grid[1]-src_grid[0]);
    }
    if( 1.0E30 * abs(src[src_dim-1]-src[src_dim-2]) < abs(src_grid[src_dim-1]-src_grid[src_dim-2]) ){
        ypn = 0.0;
    } else {
        ypn = (src[src_dim-1]-src[src_dim-2])/(src_grid[src_dim-1]-src_grid[src_dim-2]);
    }

    vector<double> y2;
    y2 = Interpolation::Spline::spline( src_grid, src, src_dim, yp1, ypn );
    // */

    int dim = dest_basis -> get_original_size();

#ifdef ACE_HAVE_OMP
#pragma omp parallel for schedule(runtime)
#endif
    for(int i = 0; i < dim; ++i ){
        double xi, yi, zi, r;
        // (PBC)
        dest_basis->find_nearest_displacement(i, src_center[0], src_center[1], src_center[2], xi, yi, zi, r);

        if( r <= src_grid.back() && r <= rc ) {
            double val = Interpolation::Spline::splint( src_grid, src, y2, src.size(), r);
            //double val = Interpolation::Linear::linear_interpolate( r, src_grid, src );
            val *= Ylm(l, m, xi, yi, zi);

            if( abs(val) > RGD2GD_CUTOFF ){
#ifdef ACE_HAVE_OMP
#pragma omp critical
#endif
                {
                    dest_val.push_back(val);
                    dest_ind.push_back(i);
                }
            }
        }
    }
}

// dest size == grid_setting -> get_original_size
void Radial_Grid::Paw::get_basis_coeff_from_radial_grid( int l, int m, vector<double> src, vector<double> src_grid, std::array<double,3> src_center, RCP<const Basis> dest_basis, vector<double> &dest_val, vector<int> &dest_ind, double rc/* = -1*/ ){
    dest_val.clear();
    dest_ind.clear();
    if( rc < 0.0 ){
        rc = 2*src_grid.back();
    }

    //*
    int src_dim = src_grid.size();
    double yp1, ypn;
    if( 1.0E30 * abs(src[1]-src[0]) < abs(src_grid[1]-src_grid[0]) ){
        yp1 = 0.0;
    } else {
        yp1 = (src[1]-src[0])/(src_grid[1]-src_grid[0]);
    }
    if( 1.0E30 * abs(src[src_dim-1]-src[src_dim-2]) < abs(src_grid[src_dim-1]-src_grid[src_dim-2]) ){
        ypn = 0.0;
    } else {
        ypn = (src[src_dim-1]-src[src_dim-2])/(src_grid[src_dim-1]-src_grid[src_dim-2]);
    }

    vector<double> y2;
    y2 = Interpolation::Spline::spline( src_grid, src, src_dim, yp1, ypn );
    // */

    int dim = dest_basis -> get_original_size();
    double sqrt_scaling = sqrt(dest_basis -> get_scaling()[0]*dest_basis -> get_scaling()[1]*dest_basis -> get_scaling()[2]);

#ifdef ACE_HAVE_OMP
#pragma omp parallel for schedule(runtime)
#endif
    for(int i = 0; i < dim; ++i ){
        double xi, yi, zi, r;
        // (PBC)
        dest_basis->find_nearest_displacement(i, src_center[0], src_center[1], src_center[2], xi, yi, zi, r);
        double xp = xi + src_center[0]; double yp = yi + src_center[1]; double zp = zi + src_center[2];
        
        if( r <= src_grid.back() && r <= rc ) {
            double val = Interpolation::Spline::splint( src_grid, src, y2, src.size(), r);
            //double val = Interpolation::Linear::linear_interpolate( r, src_grid, src);
            val *= sqrt_scaling;
            //val /= dest_basis -> compute_basis(i, xp, yp, zp);// Does not work with PBC.
            val *= Ylm(l, m, xi, yi, zi);

            if( abs(val) > RGD2GD_CUTOFF ){
#ifdef ACE_HAVE_OMP
#pragma omp critical
#endif
                {
                    dest_val.push_back(val);
                    dest_ind.push_back(i);
                }
            }
        }
    }
}

// dest size == grid_setting -> get_original_size
int Radial_Grid::Paw::Interpolate_from_radial_grid( int l, int m, vector<double> src, vector<double> src_grid, std::array<double,3> src_center, RCP<const Basis> dest_basis, RCP<Epetra_Vector>& retval, double rc/* = -1.0*/ ){
    if( rc < 0.0 ){
        rc = 2*src_grid.back();
    }
    retval -> PutScalar(0.0);

    //*
    int src_dim = src.size();
    double yp1, ypn;
    if( 1.0E30 * abs(src[1]-src[0]) < abs(src_grid[1]-src_grid[0]) ){
        yp1 = 0.0;
    } else {
        yp1 = (src[1]-src[0])/(src_grid[1]-src_grid[0]);
    }
    if( 1.0E30 * abs(src[src_dim-1]-src[src_dim-2]) < abs(src_grid[src_dim-1]-src_grid[src_dim-2]) ){
        ypn = 0.0;
    } else {
        ypn = (src[src_dim-1]-src[src_dim-2])/(src_grid[src_dim-1]-src_grid[src_dim-2]);
    }

    vector<double> y2;
    y2 = Interpolation::Spline::spline( src_grid, src, src_dim, yp1, ypn );
    // */

    int dim = dest_basis -> get_original_size();

    vector<double> retval_tmp;
    vector<int> retind_tmp;
#ifdef ACE_HAVE_OMP
#pragma omp parallel for schedule(runtime)
#endif
    for(int i = 0; i < dim; ++i ){
        if( retval -> Map().MyGID(i) ){
            double xi, yi, zi, r;
            // (PBC)
            dest_basis->find_nearest_displacement(i, src_center[0], src_center[1], src_center[2], xi, yi, zi, r);

            if( r <= src_grid.back() && r <= rc ) {
                double val = Interpolation::Spline::splint( src_grid, src, y2, src.size(), r);
                //double val = Interpolation::Linear::linear_interpolate( r, src_grid, src);
                val *= Ylm(l, m, xi, yi, zi);

#ifdef ACE_HAVE_OMP
#pragma omp critical
#endif
                if( abs(val) > RGD2GD_CUTOFF ){
                    retval_tmp.push_back(val);
                    retind_tmp.push_back(i);
                }
            }
        }
    }
    retval -> ReplaceGlobalValues(retind_tmp.size(), retval_tmp.data(), retind_tmp.data());

    return retind_tmp.size();
}

// dest size == grid_setting -> get_original_size
int Radial_Grid::Paw::get_basis_coeff_from_radial_grid( int l, int m, vector<double> src, vector<double> src_grid, std::array<double,3> src_center, RCP<const Basis> dest_basis, RCP<Epetra_Vector>& retval, double rc/* = -1.0*/ ){
    if( rc < 0.0 ){
        rc = 2*src_grid.back();
    }
    retval -> PutScalar(0.0);

    //*
    int src_dim = src_grid.size();
    double yp1, ypn;
    if( 1.0E30 * abs(src[1]-src[0]) < abs(src_grid[1]-src_grid[0]) ){
        yp1 = 0.0;
    } else {
        yp1 = (src[1]-src[0])/(src_grid[1]-src_grid[0]);
    }
    if( 1.0E30 * abs(src[src_dim-1]-src[src_dim-2]) < abs(src_grid[src_dim-1]-src_grid[src_dim-2]) ){
        ypn = 0.0;
    } else {
        ypn = (src[src_dim-1]-src[src_dim-2])/(src_grid[src_dim-1]-src_grid[src_dim-2]);
    }

    vector<double> y2;
    y2 = Interpolation::Spline::spline( src_grid, src, src_dim, yp1, ypn );
    // */

    int dim = dest_basis -> get_original_size();
    const double ** scaled_grid = dest_basis -> get_scaled_grid();

    vector<double> retval_tmp;
    vector<int> retind_tmp;
#ifdef ACE_HAVE_OMP
#pragma omp parallel for schedule(runtime)
#endif
    for(int i = 0; i < dim; ++i ){
        if( retval -> Map().MyGID(i) ){
            double xi, yi, zi, r;
            int i_x = 0, i_y = 0, i_z = 0;
            dest_basis -> decompose(i, i_x, i_y, i_z );
            /*
            xi = scaled_grid[0][i_x] - src_center[0];
            yi = scaled_grid[1][i_y] - src_center[1];
            zi = scaled_grid[2][i_z] - src_center[2];
            r = sqrt(xi*xi+yi*yi+zi*zi);
            */
            dest_basis -> find_nearest_displacement(i, src_center[0], src_center[1], src_center[2], xi, yi, zi, r);

            if( r <= src_grid.back() && r <= rc ) {
                double val = Interpolation::Spline::splint( src_grid, src, y2, src.size(), r);
                //double val = Interpolation::Linear::linear_interpolate( r, src_grid, src);
                val /=  dest_basis -> compute_basis(i, scaled_grid[0][i_x], scaled_grid[1][i_y], scaled_grid[2][i_z]);
                val *= Ylm(l, m, xi, yi, zi);

#ifdef ACE_HAVE_OMP
#pragma omp critical
#endif
                if( abs(val) > RGD2GD_CUTOFF ){
                    retval_tmp.push_back(val);
                    retind_tmp.push_back(i);
                }
            }
        }
    }
    retval -> ReplaceGlobalValues(retind_tmp.size(), retval_tmp.data(), retind_tmp.data());

    return retind_tmp.size();
}

vector<double> Radial_Grid::Paw::change_radial_grid( vector<double> src, vector<double> src_grid, vector<double> dest_grid ){
    int dim = dest_grid.size();
    vector<double> dest;

    //*
    int src_dim = src.size();
    double yp1, ypn;
    if( 1.0E30 * abs(src[1]-src[0]) < abs(src_grid[1]-src_grid[0]) ){
        yp1 = 0.0;
    } else {
        yp1 = (src[1]-src[0])/(src_grid[1]-src_grid[0]);
    }
    if( 1.0E30 * abs(src[src_dim-1]-src[src_dim-2]) < abs(src_grid[src_dim-1]-src_grid[src_dim-2]) ){
        ypn = 0.0;
    } else {
        ypn = (src[src_dim-1]-src[src_dim-2])/(src_grid[src_dim-1]-src_grid[src_dim-2]);
    }

    vector<double> y2;
    y2 = Interpolation::Spline::spline( src_grid, src, src_dim, yp1, ypn );
    // */

    dest.resize(dim);
    for(int i = 0; i < dim; ++i ){
        double r = dest_grid[i];

        if( r > src_grid.back() ){
            dest[i] = 0.0;

        } else {
            dest[i] = Interpolation::Spline::splint( src_grid, src, y2, src.size(), r);
            //dest[i] = Interpolation::Linear::linear_interpolate( r, src_grid, src);
        }
    }
    return dest;
}

// Analytic grid_derivative versions
vector<double> Radial_Grid::Paw::calculate_Hartree_potential_r( vector<double> radial_func, int l, vector<double> grid, vector<double> grid_derivative ){
    vector<double> retval_potential(grid.size());

    //if( cutoff < 0.0 ) cutoff = 2*grid.back();

    for(int i = 0; i < grid.size(); ++i ){
        //if( grid[i] > cutoff ) break;
        /*
        double potential_on_r = 0.0;
        for(int j = 0; j < grid.size(); ++j){
            //if( grid[j] > cutoff ) break;
            if( j < i ){
                double val1 = 0.0;
                //if( abs( radial_func[j] ) > HARTREE_CUTOFF && grid[j] > HARTREE_CUTOFF ){
                if( abs( radial_func[j] ) > HARTREE_CUTOFF ){
                    val1 = radial_func[j]*grid[j]*pow(grid[j]/grid[i], l+1);
                    // If radial_func[j] == 0 and grid[j] == 0, this is zero.
                    // Though, removing this if statements gives no error since grid[i] != 0 always.
                }
                if( grid_derivative[j] != 0 ) potential_on_r += val1 * grid_derivative[j];
            } else {
                double val1 = 0.0;
                //if( l != 1 && grid[j] < HARTREE_CUTOFF ) continue;// If grid[j] == 0, this is always zero if l == 0. Should be problem if radial_func[j] * grid[i] != 0 and l > 1.
                if( l == 0 && grid[j] < HARTREE_CUTOFF ) continue;// If grid[j] == 0, this is always zero if l == 0. Should be problem if radial_func[j] * grid[i] != 0 and l > 1.
                //if( l == 1 && grid[i] < HARTREE_CUTOFF ) continue;// If grid[i] == 0, this is always zero unless l == 0
                //if( l != 0 && grid[i] < HARTREE_CUTOFF ) continue;// If grid[i] == 0, this is always zero unless l == 0
                if( l > 0 && grid[i] < HARTREE_CUTOFF ) continue;// If grid[i] == 0, this is always zero unless l == 0
                if( abs( radial_func[j] ) > HARTREE_CUTOFF ){
                    val1 = radial_func[j]*grid[j]*pow(grid[i]/grid[j], l);
                    // If radial_func[j] == 0, this is always zero. Problem if grid[j] == 0 and l > 1
                }
                if( grid_derivative[j] != 0 ) potential_on_r += val1 * grid_derivative[j];
            }

            if( !std::isfinite(potential_on_r) ){
                Verbose::all() << "Radial_Grid::Paw::calculate_Hartree_potential_r not finite! == " << potential_on_r << std::endl;
                exit(EXIT_FAILURE);
            }
        }
        potential_on_r *= 4*M_PI/(2*l+1);
        retval_potential[i] = potential_on_r;
        */
        retval_potential[i] = Radial_Grid::Paw::calculate_Hartree_potential_r(radial_func, grid[i], l, grid, grid_derivative);
    }
    return retval_potential;
}

double Radial_Grid::Paw::calculate_Hartree_potential_r( vector<double> radial_func, double r, int l, vector<double> grid, vector<double> grid_derivative ){
    double potential_on_r = 0.0;
    for(int j = 0; j < grid.size(); ++j){
        //if( grid[j] > cutoff ) break;
        if( grid[j] < r ){
            double val1 = 0.0;
            //if( abs( radial_func[j] ) > HARTREE_CUTOFF && grid[j] > HARTREE_CUTOFF ){
            if( abs( radial_func[j] ) > HARTREE_CUTOFF ){
                val1 = radial_func[j]*grid[j]*pow(grid[j]/r, l+1);
                // If radial_func[j] == 0 and grid[j] == 0, this is zero.
                // Though, removing this if statements gives no error since grid[i] != 0 always.
            }
            if( grid_derivative[j] != 0 ) potential_on_r += val1 * grid_derivative[j];
        } else {
            double val1 = 0.0;
            //if( l != 1 && grid[j] < HARTREE_CUTOFF ) continue;// If grid[j] == 0, this is always zero if l == 0. Should be problem if radial_func[j] * grid[i] != 0 and l > 1.
            if( l == 0 && grid[j] < HARTREE_CUTOFF ) continue;// If grid[j] == 0, this is always zero if l == 0. Should be problem if radial_func[j] * grid[i] != 0 and l > 1.
            //if( l == 1 && grid[i] < HARTREE_CUTOFF ) continue;// If grid[i] == 0, this is always zero unless l == 0
            //if( l != 0 && grid[i] < HARTREE_CUTOFF ) continue;// If grid[i] == 0, this is always zero unless l == 0
            if( l > 0 && r < HARTREE_CUTOFF ) continue;// If grid[i] == 0, this is always zero unless l == 0
            if( abs( radial_func[j] ) > HARTREE_CUTOFF ){
                val1 = radial_func[j]*grid[j]*pow(r/grid[j], l);
                // If radial_func[j] == 0, this is always zero. Problem if grid[j] == 0 and l > 1
            }
            if( grid_derivative[j] != 0 ) potential_on_r += val1 * grid_derivative[j];
        }

        if( !std::isfinite(potential_on_r) ){
            Verbose::all() << "Radial_Grid::Paw::calculate_Hartree_potential_r not finite! == " << potential_on_r << std::endl;
            exit(EXIT_FAILURE);
        }
    }
    potential_on_r *= 4*M_PI/(2*l+1);
    return potential_on_r;
}

double Radial_Grid::Paw::calculate_coulomb_integral( vector<double> f1, vector<double> f2, int l1, int l2, int m1, int m2, vector<double> grid, vector<double> grid_derivative, double cutoff ){
    double retval = 0.0;

    // From Orthonormality condition of spherical harmonics
    if( l1 != l2 || m1 != m2 ){
        return 0.0;
    }

    vector<double> v_f2 = Radial_Grid::Paw::calculate_Hartree_potential_r( f2, l2, grid, grid_derivative );
    retval = Radial_Grid::Paw::radial_integrate( f1, v_f2, grid, grid_derivative, cutoff );

    return retval;
}
