#include "Spline_Interpolation.hpp"
#include "../Verbose.hpp"
#include <cstdlib>
#include <cassert>

using std::vector;

vector<double> Interpolation::Spline::spline(vector<double> x, vector<double> y, int n){
    assert(x.size() == y.size() and x.size() == n);
    double yp1 = (y[1] - y[0])/(x[1] - x[0]);
    double ypn = (y[n-1] - y[n-2])/(x[n-1] - x[n-2]);
    return Interpolation::Spline::spline(x, y, n, yp1, ypn);
}

vector<double> Interpolation::Spline::spline(vector<double> x, vector<double> y, int n, double yp1, double ypn){

    vector<double> y2;

    for(int i=0;i<n;i++){
      y2.push_back(0.0);
    }

    double p, qn, sig, un;
    double* u;

    u = new double [n-1];

    if(yp1 > 0.99E30){
        y2[0] = 0.0;
        u[0] = 0.0;
    }
    else{
        y2[0] = -0.5;
        u[0] = ( 3.0/(x[1]-x[0]) ) * ((y[1]-y[0])/(x[1]-x[0])-yp1);
    }

    for(int i=2;i<=n-1;i++){
        sig = (x[i-1] - x[i-2]) / (x[i] - x[i-2]);
        p = sig * y2[i-2] + 2.0;
        y2[i-1] = (sig-1.0)/p;
        u[i-1] = (y[i]-y[i-1])/(x[i]-x[i-1]) - (y[i-1] - y[i-2])/(x[i-1]-x[i-2]);
        u[i-1] = (6.0*u[i-1]/(x[i]-x[i-2]) - sig*u[i-2]) / p;
    }
    if(ypn > 0.99E30){
        qn = 0.0;
        un = 0.0;
    }
    else{
        qn = 0.5;
        un = (3.0 / (x[n-1] - x[n-2])) * (ypn - (y[n-1] - y[n-2]) / (x[n-1]-x[n-2]));
    }
    y2[n-1] = (un - qn*u[n-2]) / (qn*y2[n-2] + 1.0);
    for(int k=n-1;k>=1;k--){
        y2[k-1] = y2[k-1] * y2[k] + u[k-1];
    }

    delete[] u;

    return y2;
}

double Interpolation::Spline::splint(vector<double> xa, vector<double> ya, vector<double> y2a, int n, double x){
    double y;

    int klo, khi, k;
    double h, b, a;

    klo = 0;
    khi = n-1;
    while(khi-klo > 1){
        k = (khi+klo) >> 1;
        if(xa[k] > x) khi = k;
        else klo = k;
    }
    h = xa[khi] - xa[klo];
    if(h==0.0){
        Verbose::all()<<  "Spline_Interpolation::splint - Bad xa input to routine splint" << std::endl;
        exit(EXIT_FAILURE);
    }

    a = (xa[khi]-x)/h;
    b = (x-xa[klo])/h;

    y = a*ya[klo] + b*ya[khi] + ((a*a*a-a)*y2a[klo] + (b*b*b-b)*y2a[khi]) * (h*h) / 6.0;

    return y;
}
