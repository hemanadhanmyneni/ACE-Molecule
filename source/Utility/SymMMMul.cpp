#include <vector>
#include <algorithm>
#include "SymMMMul.hpp"
#ifdef ACE_MKL
#include "mkl.h"
#endif
#include "Epetra_MpiComm.h"
#include "Verbose.hpp"
#include "Time_Measure.hpp"

int SymMMMul::compute(const int total_row_size, const int my_row_size, int* A_row_ptr, int* A_col_ind, double* A_val, int* B_row_ptr, int* B_col_ind, double* B_val, int*& C_row_ptr, int*& C_col_ind, double*& C_val ){
    auto timer = Teuchos::rcp(new Time_Measure());

    timer->start("pre-processing");
    const int nProc = Parallel_Manager::info().get_mpi_size();
    const int my_pid = Parallel_Manager::info().get_mpi_rank();
    int all_data[nProc*3];
    int data[3];
    data[0] = my_row_size; data[1] = A_row_ptr[my_row_size]; data[2] = B_row_ptr[my_row_size];
    Parallel_Manager::info().group_allgather(data, 3, all_data, 3);

    std::vector<int> A_all_row_size(nProc);
    std::vector<int> A_all_nnz_size(nProc);
    std::vector<int> B_all_row_size(nProc);
    std::vector<int> B_all_nnz_size(nProc);
    int count1=0; int count2=0; int count3=0;
    for (int i=0; i<3*nProc ; i++){
        if (i%3==0){
            A_all_row_size[count1++] = all_data[i];
        }
        else if (i%3==1){
            A_all_nnz_size[count2++] = all_data[i];
        }
        else{
            B_all_nnz_size[count3++] = all_data[i];
        }
    }

    std::copy( A_all_row_size.begin(), A_all_row_size.end(), B_all_row_size.begin() );

    int max_row_size = *std::max_element(B_all_row_size.begin(), B_all_row_size.end() );
    int max_nnz_size = *std::max_element(B_all_nnz_size.begin(), B_all_nnz_size.end() );

    count1=0; count2=0;
    std::vector<int> A_min_row_index(nProc);
    std::vector<int> B_min_row_index(nProc);

    for(int i =0;i<nProc; i++){
        A_min_row_index[i] = count1;
        B_min_row_index[i] = count2;

        count1+=A_all_row_size[i];
        count2+=B_all_row_size[i];
    }

    const int buff_size = 2;
    ///////////////////////////////

    std::vector<COO_block> block_data(nProc,COO_block());
    std::vector<int> recv_index;
    std::vector<int> send_index;

    int* buff_row_ptr[buff_size];
    int* buff_col_ind[buff_size];
    double* buff_val[buff_size];

    for(int i =0; i<buff_size; i++){
        buff_row_ptr[i] = new int [max_row_size+1];
        buff_col_ind[i] = new int [max_nnz_size];
        buff_val[i]     = new double [max_nnz_size];
    }

    int* result_row_ptr;
    int* result_col_ind;
    double* result_val;

    MPI_Request send_request[3*send_index.size()];
    MPI_Request recv_request[3*recv_index.size()];

    scheduling(my_pid, nProc, recv_index, send_index);  // fill out recv_index, send_index vector
    timer->end("pre-processing");
/*
    Verbose::single(Verbose::Detail) << "send_index.size()" << send_index.size() <<std::endl;
    Verbose::single(Verbose::Detail) << "recv_index.size()" << recv_index.size() <<std::endl;

    for(int i=0; i<recv_index.size(); i++){
        Verbose::single(Verbose::Detail) << "recv_index"<< recv_index[i] <<std::endl;
    }
    for(int i=0; i<send_index.size(); i++){
        Verbose::single(Verbose::Detail) << "send_index"<< send_index[i] <<std::endl;
    }
    Verbose::single(Verbose::Detail) << "1" <<std::endl;
*/
    timer->start("processing");
    for(int i=0; i<send_index.size(); i++){
        isend_csr(send_index[i], my_row_size, B_row_ptr[my_row_size], B_row_ptr, B_col_ind, B_val, &send_request[i*3] ); // isend my data
    }
    Verbose::single(Verbose::Detail) << "0" <<std::endl;
    //irecv the first one
    if(recv_index.size()>0){
       Verbose::single(Verbose::Detail) << "irecv_csr " << recv_index.size()  <<std::endl;
       irecv_csr(recv_index[0], B_all_row_size[recv_index[0]], B_all_nnz_size[recv_index[0]], buff_row_ptr[0], buff_col_ind[0], buff_val[0], &recv_request[0] );
    }
    Verbose::single(Verbose::Detail) << "1" <<std::endl;
    //calculate diagonal block
    calculate_symmblock(A_min_row_index[my_pid], my_row_size, A_row_ptr, A_col_ind, A_val,B_min_row_index[my_pid], my_row_size, B_row_ptr, B_col_ind, B_val, block_data[my_pid]);

    //now run over all blocks
    for(int i =0; i<recv_index.size() ; i++){
        int index = i%buff_size;
        int next_ind = (i+1)%buff_size;
        // sync
        wait(3,&recv_request[3*i]);

        if (i+1<recv_index.size() )
            irecv_csr( recv_index[i+1], B_all_row_size[recv_index[i+1]], B_all_nnz_size[recv_index[i+1]], buff_row_ptr[next_ind], buff_col_ind[next_ind], buff_val[next_ind] , &recv_request[3*(i+1)]);

        calculate_block(my_row_size, A_row_ptr, A_col_ind, A_val, A_all_row_size[ recv_index[i] ], buff_row_ptr[index], buff_col_ind[index], buff_val[index], block_data[ recv_index[i] ] );
    }
    wait(3*send_index.size(),send_request);
    timer->end("processing");

    timer->start("post-processing");
    spread(recv_index, send_index, A_all_row_size, block_data); // now reverse data transfer: recv->send, and send->recv
    mergecoo (A_all_row_size, my_pid,total_row_size  ,nProc, block_data.data(), C_row_ptr, C_col_ind, C_val); //move block data to csr format
    timer->end("post-processing");
    timer->print(Verbose::single() );
    return 0;
}
void SymMMMul::mergecoo(std::vector<int> all_row_size, int my_pid, int total_size,int nBlock, COO_block* block_data, int*& row_ind, int*& col_ind, double*& val ){
    const int nRow  = all_row_size[my_pid];
    std::vector<int > coo_row(nRow+1,0);
//    std::vector<std::vector<std::pair<int,double> > > coo_col_val(nRow);
    std::vector<std::vector<int> > coo_col(nRow);
    std::vector<std::vector<double > > coo_val(nRow);

    int* block_row_ind; int* block_col_ind; double* block_val;
    int current_nnz=0; int sum_nnz=0;

    for(int i=0; i<nBlock; i++)
        sum_nnz+=block_data[i].get_nnz();

    int reserve_size = sum_nnz/nRow;
    if (reserve_size <0) reserve_size = coo_val[0].max_size(); //overflow case

    for(int i=0; i<nRow; i++){
        coo_col[i].reserve(reserve_size);
        coo_val[i].reserve(reserve_size);
    }
    int count = 0; int ind;

    for(int i=0; i<nBlock; i++){
        block_data[i].get_data(block_row_ind,block_col_ind,block_val);
        current_nnz = block_data[i].get_nnz();
        for(int j=0; j<current_nnz; j++ ){
            ind = block_row_ind[j];
            coo_row[ind+1]++;
            coo_col[ind].push_back(block_col_ind[j]);
            coo_val[ind].push_back(block_val[j]);
        }
        //total_size-=block_data[i].get_col_size();
        //count+=current_nnz;
        //Verbose::all() << "block_data["<<i<<"].get_col_size()" << block_data[i].get_col_size()<<"\t" <<block_data[i].get_nnz()<<"\t"<< Parallel_Manager::info().get_mpi_rank() <<std::endl;
        block_data[i].clean();
    }
/*
    if(total_size!=count){
        Verbose::all() << "Error on SymMMMul::mergecoo  "<< total_size <<"\t" << count<< "\tmy_pid: " << my_pid <<std::endl;
        exit(-1);
    }
*/
/*
    std::cout << "merge shchoi before" <<std::endl;
    for(int i =0; i<coo_row.size(); i++){
        for(int j=0; j<coo_col[i].size(); j++){
            std::cout << "shchoi 0\t" << i << "\t" << coo_col[i][j] << "\t" << coo_val[i][j] <<std::endl;
        }
    }
    std::cout << "merge shchoi after" <<std::endl;
*/
    row_ind = new int[nRow+1];
    col_ind = new int[sum_nnz];
    val = new double[sum_nnz];

    std::memcpy(row_ind, coo_row.data(), (nRow+1)*sizeof(int));
    for(int i=nRow-1; i>0; i--){
        for(int j=i+1; j<nRow+1; j++){
            row_ind[j]+=row_ind[i];
        }
    }
/*
    int tmp1=0; int tmp2=0;
    for(int i=0; i<nRow; i++){
        tmp1+= coo_val[i].size();
        tmp2+= coo_col[i].size();
    }
*/
    count=0;
    for(int i=0; i<nRow; i++){
        std::memcpy(&col_ind[count], coo_col[i].data(), coo_col[i].size()*sizeof(int));
        std::memcpy(&val[count], coo_val[i].data(), coo_val[i].size()*sizeof(double));
        count+=coo_val[i].size();
    }

    return ;
}
int SymMMMul::match_indices(const int A_size, int* A_index, const int B_size, int* B_index,std::vector<int>& matched_A, std::vector<int>& matched_B ){
    for(int i=0; i<A_size; i++){
        for(int j=0; j<B_size; j++){
            if (A_index[i]==B_index[j]){
                matched_A.push_back(i);
                matched_B.push_back(j);
            }
        }
    }
    return matched_A.size();
}
inline
double sparsedot(const int A_col_size, const int* A_col_ind, const double* A_val, const int B_col_size, const int* B_col_ind, const double* B_val){
    int i=0,j=0;
    double val=0;
    while(i<A_col_size and j<B_col_size){
        if(A_col_ind[i]<B_col_ind[j]){
            i++;
        }
        else if(A_col_ind[i]>B_col_ind[j]){
            j++;
        }
        else{
            val+=A_val[i]*B_val[j];
            i++; j++;
        }
    }
    return val;
}

inline
void searching(const int A_row_size, int* A_row_ptr, int* A_col_ind, const int B_row_size, int* B_row_ptr, int* B_col_ind, std::vector<std::pair<int,int> >& non_zero_positions ){
    non_zero_positions.clear();
    non_zero_positions.reserve(A_row_size*B_row_size);
    int i,j,k,l;
    for (i =0; i<A_row_size; i++){
        for (j=0; j<B_row_size; j++){
            k=A_row_ptr[i]; l=B_row_ptr[j];
            while(k < A_row_ptr[i+1] and l < B_row_ptr[j+1]){
                if(A_col_ind[k]<B_col_ind[l]){
                    k++;
                }
                else if(A_col_ind[k]>B_col_ind[l]){
                    l++;
                }
                else {
                    non_zero_positions.push_back(std::make_pair(i,j));
                    break;
                }
            }
        }
    }
    return;
}

void SymMMMul::calculate_symmblock(const int A_row_start_index, const int A_row_size, int* A_row_ptr, int* A_col_ind, double* A_val, const int B_row_start_index,const int B_row_size, int* B_row_ptr, int* B_col_ind, double* B_val, COO_block& block_data){

    auto timer = Teuchos::rcp(new Time_Measure());
    timer->start("calculate_symmblock1");
    double value=0.0;
    std::vector<int> row_ind;
    std::vector<int> col_ind;
    std::vector<double> val;

    int reserve_size = std::min(((size_t)A_row_size)*((size_t)B_row_size), val.max_size());
    if(reserve_size <0) reserve_size=val.max_size();  //overflow case
    row_ind.reserve(reserve_size);
    col_ind.reserve(reserve_size);
    val.reserve(reserve_size);
    timer->end("calculate_symmblock1");
    int diff; int current_l;
//    std::vector<std::pair<int,int> > non_zero_positions;
//    timer->start("calculate_symmblock2");
//    searching(A_row_size, A_row_ptr, A_col_ind, B_row_size, B_row_ptr, B_col_ind, non_zero_positions );
//    std::cout << "non_zero_positions size: " << non_zero_positions.size() <<std::endl;
//    timer->end("calculate_symmblock2");
    timer->start("calculate_symmblock3");
    int i,j,k,l;
//    const size_t size = non_zero_positions.size();
/*
    for (i=0; i<size; i++){
        k=non_zero_positions[i].first;
        l=non_zero_positions[i].second;
        value = sparsedot(A_row_ptr[k+1]-A_row_ptr[k], &A_col_ind[A_row_ptr[k]], &A_val[A_row_ptr[k]], B_row_ptr[l+1]-B_row_ptr[l], &B_col_ind[B_row_ptr[l]], &B_val[B_row_ptr[l]] );
        if(std::fabs(value) > tol){
            row_ind.push_back(k);
            col_ind.push_back(l+B_row_start_index);
            val.push_back(value);
        }
    }
*/


    for(k=0; k< A_row_size; k++){
        for(l=0; l<B_row_size; l++){
            value = sparsedot(A_row_ptr[k+1]-A_row_ptr[k], &A_col_ind[A_row_ptr[k]], &A_val[A_row_ptr[k]], B_row_ptr[l+1]-B_row_ptr[l], &B_col_ind[B_row_ptr[l]], &B_val[B_row_ptr[l]] );
            if (std::fabs(value)>tol){
                row_ind.push_back(k);
                col_ind.push_back(l+B_row_start_index);
                val.push_back(value);
            }
        }
    }

//    int* A_tmp_col_ind=NULL;
//    int* B_tmp_col_ind=NULL;
//    int A_tmp_col_size;
//    int B_tmp_col_size;

/*
    for (i =0; i<A_row_size ; i++){
        // diagonal case //
        j=i;
        value = sparsedot(A_row_ptr[i+1]-A_row_ptr[i], &A_col_ind[A_row_ptr[i]], &A_val[A_row_ptr[i]], B_row_ptr[j+1]-B_row_ptr[j], &B_col_ind[B_row_ptr[j]], &B_val[B_row_ptr[j]] );
        if(fabs(value) >tol){
            row_ind.push_back(i);
            col_ind.push_back(j);
            val.push_back(value);
        }
//        for (k =A_row_ptr[i]; k<A_row_ptr[i+1];k++)
//            j = A_col_ind[k]; //global index of j
//            if (j<=i+A_row_start_index) continue;
//            j -= B_row_start_index; //local index;
//            if(j>=B_row_size) break;
        // offdiagonal case //
        for(j=i+1; j<B_row_size; j++){
            value = sparsedot(A_row_ptr[i+1]-A_row_ptr[i], &A_col_ind[A_row_ptr[i]], &A_val[A_row_ptr[i]], B_row_ptr[j+1]-B_row_ptr[j], &B_col_ind[B_row_ptr[j]], &B_val[B_row_ptr[j]] );
            if( std::fabs(value)>tol){
                row_ind.push_back(i);
                col_ind.push_back(j+B_row_start_index);
                val.push_back(value);
                row_ind.push_back(j);
                col_ind.push_back(i+A_row_start_index);
                val.push_back(value);
            }
        }
    }
*/
    timer->end("calculate_symmblock3");
    timer->start("calculate_symmblock4");

    const int nnz = col_ind.size();
    int* block_row_ind;
    int* block_col_ind;
    double* block_val;
    block_data.realloc( nnz, A_row_size, B_row_size);
    block_data.get_data(block_row_ind,block_col_ind,block_val);
    std::memcpy(block_row_ind, row_ind.data(), nnz*sizeof(int));
    std::memcpy(block_col_ind, col_ind.data(), nnz*sizeof(int));
    std::memcpy(block_val,     val.data(),     nnz*sizeof(double));
    timer->end("calculate_symmblock4");
    timer->print(Verbose::single(Verbose::Detail) );
    return;
}
void SymMMMul::calculate_block(const int A_row_size, int* A_row_ptr, int* A_col_ind, double* A_val, const int B_row_size, int* B_row_ptr, int* B_col_ind, double* B_val, COO_block& block_data){
    double value=0.0;
    std::vector<int> row_ind;
    std::vector<int> col_ind;
    std::vector<double> val;

    int reserve_size = std::min((size_t)A_row_size*B_row_size, val.max_size());
    if(reserve_size <0) reserve_size=val.max_size();  //overflow case
    row_ind.reserve(A_row_size);
    col_ind.reserve(reserve_size);
    val.reserve(reserve_size);
    int diff; int current_l;
    //std::cout << "i\t j\tk\tl\tA_col_ind[k]\tB_col_ind[l]\tA_val[k]*B_val[l] " <<std::endl;
    int row_index_A=0;
    int row_index_B=0;
/*
    for(i=0; i<A_row_ptr[A_row_size]; i++){
        for(j=0; j<B_row_ptr[B_row_size]; j++){

            if(j==B_row_ptr[row_index_B]){
                row_ind.push_back(row_index_A);
                col_ind.push_back(row_index_B);
                val.push_back(value);
                break;
            }
        }
    }*/
    int i,j,k,l;
    for (i =0; i<A_row_size ; i++){
        for(j=0; j<B_row_size; j++){
            value = sparsedot(A_row_ptr[i+1]-A_row_ptr[i], &A_col_ind[A_row_ptr[i]], &A_val[A_row_ptr[i]], B_row_ptr[j+1]-B_row_ptr[j], &B_col_ind[B_row_ptr[j]], &B_val[B_row_ptr[j]] );
            if( std::fabs(value)>tol){
                row_ind.push_back(i);
                col_ind.push_back(j);
                val.push_back(value);
//                std::cout << "shchoi3\t" <<i <<"\t" << j <<"\t" << value <<std::endl;
            }
        }
    }
/*
    for (int i =0; i<A_row_size ; i++){
        for(int j =0; j<B_row_size ;j++){
            value = 0.0;
            for(int k=A_row_ptr[i]; k<A_row_ptr[i+1]; k++ ){
                for(int l=B_row_ptr[j]; l<B_row_ptr[j+1]; l++){
                //for(int l=current_l; l<B_row_ptr[j+1]; l++)
                    diff = A_col_ind[k]-B_col_ind[l];

//                    if(diff<0){
//                        break;
//                    }
//                    else if (diff==0){
//                        value+=A_val[k]*B_val[l];
//                    }
                    if (diff==0){
                        value+=A_val[k]*B_val[l];
                    }
                }
            }
            if( std::fabs(value)>tol){
                row_ind.push_back(i);
                col_ind.push_back(j);
                val.push_back(value);
            }
        }

    }
*/
    const int nnz = col_ind.size();
//    std::cout << "shchoishchoishchoinnz\t" << nnz<<std::endl;
    int* block_row_ind;
    int* block_col_ind;
    double* block_val;
    block_data.realloc( nnz, A_row_size, B_row_size);
    block_data.get_data(block_row_ind,block_col_ind,block_val);
    std::memcpy(block_row_ind, row_ind.data(), nnz*sizeof(int));
    std::memcpy(block_col_ind, col_ind.data(), nnz*sizeof(int));
    std::memcpy(block_val,     val.data(),     nnz*sizeof(double));
    return;
}
void SymMMMul::spread(std::vector<int> send_index, std::vector<int> recv_index, std::vector<int> all_row_size, std::vector<COO_block>& block_data){
    int send_index_size = send_index.size();
    int recv_index_size = recv_index.size();
    if(send_index_size==0 and recv_index_size==0){
        return;
    }
    auto comm = Teuchos::rcp_dynamic_cast<Epetra_MpiComm>( Parallel_Manager::info().get_group_comm() )->Comm();
    const int nProc = Parallel_Manager::info().get_mpi_size();
    const int my_pid = Parallel_Manager::info().get_mpi_rank();

    std::vector<int> recv_nnz_size(recv_index_size);
    std::vector<int> send_nnz_size(send_index_size);
    int count=0;

    {
        int index;
        MPI_Request request[send_index_size+recv_index_size];

        for(int i = 0; i < recv_index_size; i++ ){
            index = recv_index[i];
            MPI_Irecv( &recv_nnz_size[index], 1, MPI_INT, index, index*my_pid, comm, &request[count++]   );
        }
        for(int i=0; i<send_index_size; i++){
            index = send_index[i];
            send_nnz_size[index] = block_data[index].get_nnz();
            MPI_Isend( &send_nnz_size[index], 1, MPI_INT, index, index*my_pid, comm, &request[count++] );
        }

        wait(count, request);
    }
    count=0;
    {
        int index;
        int* row_ind; int* col_ind; double* val;
        MPI_Request request[3*(send_index_size+recv_index_size)];
        for(int i =0; i< recv_index_size; i++){
            index = recv_index[i];
            block_data[index].realloc(recv_nnz_size[index], all_row_size[my_pid], all_row_size[index]); // alloc empty blocks
            block_data[index].get_data(row_ind,col_ind,val);
            irecv_coo(index, recv_nnz_size[index], row_ind, col_ind, val, &request[count]  );
            count+=3;
        }
        for(int i=0; i<send_index_size; i++){
            index = send_index[i];
            block_data[index].get_data(row_ind,col_ind,val);
            isend_coo(index, send_nnz_size[index], col_ind, row_ind, val,  &request[count]); //since transpose matrix should be sent
            count+=3;
        }
        wait(count, request);
    }
    return ;
}
void SymMMMul::wait(const int n, MPI_Request* request){
    if(n<=0) return ;
    MPI_Status status[n];
    MPI_Waitall(n, request, &status[0] );
    return;
}

void SymMMMul::isend_csr(int send_pid, int row_size, int nnz_size, int* row_ptr, int* col_ind, double* val, MPI_Request* request){
    auto comm = Teuchos::rcp_dynamic_cast<Epetra_MpiComm>( Parallel_Manager::info().get_group_comm() )->Comm();
    MPI_Isend(row_ptr, row_size+1, MPI_INT,    send_pid, 1, comm, &request[0]);
    MPI_Isend(col_ind, nnz_size,   MPI_INT,    send_pid, 2, comm, &request[1]);
    MPI_Isend(val    , nnz_size,   MPI_DOUBLE, send_pid, 3, comm, &request[2]);
    return;
}
void SymMMMul::irecv_csr(int recv_pid, int row_size, int nnz_size, int* row_ptr, int* col_ind, double* val, MPI_Request* request){
    auto comm = Teuchos::rcp_dynamic_cast<Epetra_MpiComm>( Parallel_Manager::info().get_group_comm() )->Comm();
    MPI_Irecv(row_ptr, row_size+1, MPI_INT,    recv_pid, 1, comm, &request[0]);
    MPI_Irecv(col_ind, nnz_size,   MPI_INT,    recv_pid, 2, comm, &request[1]);
    MPI_Irecv(val    , nnz_size,   MPI_DOUBLE, recv_pid, 3, comm, &request[2]);
    return;
}
void SymMMMul::isend_coo(int send_pid, int nnz_size, int* row_ind, int* col_ind, double* val, MPI_Request* request){
    auto comm = Teuchos::rcp_dynamic_cast<Epetra_MpiComm>( Parallel_Manager::info().get_group_comm() )->Comm();
    MPI_Isend(row_ind, nnz_size,   MPI_INT,    send_pid, 1, comm, &request[0]);
    MPI_Isend(col_ind, nnz_size,   MPI_INT,    send_pid, 2, comm, &request[1]);
    MPI_Isend(val    , nnz_size,   MPI_DOUBLE, send_pid, 3, comm, &request[2]);
    return;
}
void SymMMMul::irecv_coo(int recv_pid, int nnz_size, int* row_ind, int* col_ind, double* val, MPI_Request* request){
    auto comm = Teuchos::rcp_dynamic_cast<Epetra_MpiComm>( Parallel_Manager::info().get_group_comm() )->Comm();
    MPI_Irecv(row_ind, nnz_size,   MPI_INT,    recv_pid, 1, comm, &request[0]);
    MPI_Irecv(col_ind, nnz_size,   MPI_INT,    recv_pid, 2, comm, &request[1]);
    MPI_Irecv(val    , nnz_size,   MPI_DOUBLE, recv_pid, 3, comm, &request[2]);
    return;
}
void SymMMMul::scheduling (const int my_pid, const int nProc, std::vector<int>& recv_index, std::vector<int>& send_index){
    std::vector< std::vector<int> > que_vector(nProc);
    for(int i =0; i<nProc; i++){
        for(int j=i+1; j<nProc; j++){
            if (que_vector[i].size()> que_vector[j].size()){
                que_vector[i].push_back(j);
            }
            else{
                que_vector[j].push_back(i);
            }
        }
    }

    recv_index = que_vector[my_pid];
    send_index.clear();
    for (int i =0; i< nProc; i++){
        if(std::find(recv_index.begin(), recv_index.end(),i)!=recv_index.end() or i==my_pid) continue;
        send_index.push_back(i);
    }
/*
    for (int i =0; i<send_index.size(); i++){
        std::cout << "send my_pid("<< my_pid<< ")" << send_index[i]  <<std::endl;
    }
    for (int i =0; i<recv_index.size(); i++){
        std::cout << "recv my_pid("<<my_pid <<  ")" << recv_index[i] <<std::endl;
    }
*/
    return;
}
