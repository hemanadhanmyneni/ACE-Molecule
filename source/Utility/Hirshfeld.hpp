#pragma once
#include "../Basis/Basis.hpp"
#include "Epetra_MultiVector.h"
#include "Epetra_Vector.h"

namespace Hirshfeld{
    void hirshfeld_charge(
        Teuchos::Array< Teuchos::RCP<const Epetra_Vector> > total_density, 
        Teuchos::RCP<Epetra_MultiVector> atomic_density, 
        Teuchos::RCP<Epetra_MultiVector>& HB_charge_density 
    );
}
