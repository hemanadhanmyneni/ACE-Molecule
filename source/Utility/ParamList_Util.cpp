#include "ParamList_Util.hpp"
#include "Teuchos_StandardParameterEntryValidators.hpp"
#include "String_Util.hpp"
#include "Verbose.hpp"
#include "../Io/Periodic_table.hpp"
#include "Read/Read_Upf.hpp"
#include "Read/Read_Hgh.hpp"
#include "Read/Read_Hgh_Internal.hpp"
#ifdef USE_PAW
#include "Read/Read_Paw.hpp"
#endif

using std::string;
using std::vector;
using Teuchos::Array;
using Teuchos::RCP;
using Teuchos::rcp;
using Teuchos::ParameterList;
#ifdef USE_PAW
using Read::Read_Paw;
#endif

Array<string> ParamList_Util::PPnames_from_path(
    vector<int> atomic_numbers,
    string filepath,
    string filesuffix
){
    if(filepath.size() > 0){
        filepath += "/";
    }
    Array<string> filenames;
    for(int ia = 0; ia < atomic_numbers.size(); ++ia){
        string atom_symbol = Periodic_atom::periodic_table[atomic_numbers[ia]-1];
        transform(atom_symbol.begin()+1, atom_symbol.end(), atom_symbol.begin()+1, ::tolower);
        filenames.push_back( filepath + atom_symbol + filesuffix );
    }
    return filenames;
}

double ParamList_Util::get_total_electrons(
    string type, string format,
    RCP<const Atoms> atoms,
    Array<string> ps_filenames,
    string xc_type/* = string()*/
){
    transform(type.begin(), type.end(), type.begin(), ::tolower);
    double retval = 0.0;
    vector<double> val_electrons;
    if(type == "upf"){
        for(int it = 0; it < atoms -> get_num_types(); ++it){
            double Zval_tmp = 0.0;
            int number_vps, number_pao, mesh_size;
            bool core_correction = false;
            Read::Upf::read_header(ps_filenames[it], &Zval_tmp, &number_vps, &number_pao, &mesh_size, &core_correction);
            val_electrons.push_back(Zval_tmp);
        }
    } else if(type == "hgh"){
        for(int it = 0; it < atoms -> get_num_types(); ++it){
            double Zval_tmp = 0.0;
            bool core_correction = false;
            if(format == "Internal"){
                Read::Hgh_Internal::read_header(ps_filenames[it], Zval_tmp, xc_type, core_correction);
            } else {
                Read::Hgh::read_header(ps_filenames[it], Zval_tmp, format, core_correction);
            }
            val_electrons.push_back(Zval_tmp);
        }
    }
#ifdef USE_PAW
    else if(type == "paw"){
        bool paw_xml = (format=="xml")? true: false;
        for(int it = 0; it < atoms -> get_num_types(); ++it){
            RCP<Read_Paw> read_paw = rcp(new Read_Paw(ps_filenames[it], paw_xml));
            val_electrons.push_back(read_paw -> get_valence_electron_number());
        }
    }
#endif
    for(int ia = 0; ia < atoms -> get_size(); ++ia){
        int it = atoms -> get_atom_type(ia);
        retval += val_electrons.at(it);
    }
    return retval;
}

void ParamList_Util::initialize_electron_parameters(
    RCP<const Atoms> atoms,
    Array< RCP<Teuchos::ParameterList> > &parameters
){
    RCP<ParameterList> parameters_basic = ParamList_Util::get_subparameter(parameters, "BasicInformation");

    Array<string> ps_filenames;
    if(!parameters_basic -> sublist("Pseudopotential").isParameter("PSFilenames")){
        vector<int> atomic_numbers = atoms -> get_atom_types();
        string filepath = parameters_basic -> sublist("Pseudopotential").get<string>("PSFilePath", string(""));
        string filesuffix = parameters_basic -> sublist("Pseudopotential").get<string>("PSFileSuffix", string(""));
        ps_filenames = ParamList_Util::PPnames_from_path(atomic_numbers, filepath, filesuffix);
        parameters_basic -> sublist("Pseudopotential").get< Array<string> >("PSFilenames", ps_filenames);
    } else {
        ps_filenames = parameters_basic -> sublist("Pseudopotential").get< Array<string> >("PSFilenames");
    }

    if(!parameters_basic -> isParameter("Charge")){
        if(!parameters_basic -> isParameter("NumElectrons")){
            string errmsg = "No NumElectrons and Charge";
            Verbose::all() << errmsg << std::endl;
            throw std::invalid_argument(errmsg);
        }
    } else {
        string type = (parameters_basic -> sublist("Pseudopotential").get<int>("Pseudopotential") == 3)? "paw": parameters_basic -> sublist("Pseudopotential").get<string>("Format");
        string format = (type == "paw")? "xml": (type == "hgh")? parameters_basic -> sublist("Pseudopotential").get<string>("HghFormat"): "";
        string xc_type = (type == "hgh")? (format == "Internal")? parameters_basic -> sublist("Pseudopotential").get<string>("XCType"): "": "";

        double tot_elec = ParamList_Util::get_total_electrons(type, format, atoms, ps_filenames, xc_type);
        // Automatically parameters_basic has Charge
        double charge = parameters_basic -> get<double>("Charge");

        if( parameters_basic -> isParameter("NumElectrons") ){
            if( tot_elec-charge != parameters_basic -> get<double>("NumElectrons") ){
                string errmsg = "Total # of electrons (" + String_Util::to_string(tot_elec)
                               + ") != NumElectrons(" + String_Util::to_string(parameters_basic -> get<double>("NumElectrons"))
                               + ") - Charge(" + String_Util::to_string(charge) + ")!";
                Verbose::all() << errmsg << std::endl;
                throw std::invalid_argument(errmsg);
            }
        } else {
            parameters_basic -> set<double>("NumElectrons", tot_elec-charge);
        }
    }

    double num_elec = parameters_basic -> get<double>("NumElectrons");
    if(!parameters_basic -> isParameter("SpinMultiplicity")){
        string warnmsg;
        if(num_elec - static_cast<int>(num_elec) < 1.0E-10 and static_cast<int>(num_elec) % 2 == 0){
            warnmsg = "SpinMultiplicity is not set! Defaulting to singlet.";
            parameters_basic -> get<double>("SpinMultiplicity", 1.0);
        } else {
            warnmsg = "NumElectrons is odd (" + String_Util::to_string(num_elec) + ") and SpinMultiplicity is not set! Defaulting to doublet.";
            parameters_basic -> get<double>("SpinMultiplicity", 2.0);
        }
        Verbose::single() << warnmsg << std::endl;
        std::cerr << warnmsg << "\n";
    }
    if(!parameters_basic -> isParameter("Polarize")){
        string warnmsg = "Polarize is not set! Defaulting to ";
        if( parameters_basic -> get<double>("SpinMultiplicity") == 1.0 ){
            warnmsg += "unpolarized calculation.";
            parameters_basic -> get<int>("Polarize", 0);
        } else {
            warnmsg += "polarized calculation.";
            parameters_basic -> get<int>("Polarize", 1);
        }
        Verbose::single() << warnmsg << std::endl;
        std::cerr << warnmsg << "\n";
    }
}

Teuchos::RCP<Teuchos::ParameterList> ParamList_Util::get_subparameter(Teuchos::Array< Teuchos::RCP<Teuchos::ParameterList> > total_parameters, std::string name, int index/* = 0*/){

    int counter = 0;
    for(int i = 0; i < total_parameters.size(); ++i){
        if(total_parameters[i] -> name() == name){
            if(counter == index){return total_parameters[i];}
            else {++counter;}
        }
    }
    return Teuchos::null;
}
