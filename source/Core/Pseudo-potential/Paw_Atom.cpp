#include "Paw_Atom.hpp"
//#include <chrono>
#include <iostream>
#include <algorithm>
#include <cmath>
//#include <cstdio>

#include "../../Basis/Create_Basis.hpp"
#include "../../Utility/Value_Coef.hpp"
#include "../../Utility/Interpolation/Spline_Interpolation.hpp"
#include "../../Utility/Interpolation/Trilinear_Interpolation.hpp"
//#include "../../Utility/Interpolation/Tricubic_Interpolation.hpp"
#include "../../Utility/Interpolation/Radial_Grid_Paw.hpp"
#include "../../Utility/Math/Spherical_Harmonics.hpp"
#include "../../Utility/Math/Spherical_Harmonics_Derivative.hpp"
#include "../../Utility/Math/Spherical_Harmonics_Expansion.hpp"
#include "../../Utility/Double_Grid.hpp"
#include "../../Utility/Calculus/Lagrange_Derivatives.hpp"
#include "../../Utility/Verbose.hpp"
#include "../../Utility/Parallel_Manager.hpp"
#include "../../Utility/Parallel_Util.hpp"
#include "Paw_Util.hpp"
#include "../../Utility/String_Util.hpp"// factorial
#include "../../Utility/Math/Spherical_Harmonics.hpp"

#define RGD2GD_CUTOFF 1.0E-20// Same and duplicate of Utility/Radial_Grid_Paw.cpp

using std::min;
using std::max;
using std::abs;
using std::vector;
using std::string;
using Teuchos::RCP;
using Teuchos::rcp;
using Teuchos::Array;
using Teuchos::SerialDenseMatrix;
using String_Util::factorial;
using Spherical_Harmonics::Ylm;

RCP<Paw_Species> Paw_Atom::get_paw_species(){
    return this -> paw_species;
}

Paw_Atom::~Paw_Atom(){
}

Paw_Atom::Paw_Atom(
    RCP<Paw_Species> paw_species, std::array<double,3> atom_center, int spin_size,
    RCP<const Basis> basis, RCP<const Basis> fine_basis,
    RCP<Teuchos::ParameterList> addi_params
){
    this -> paw_species = paw_species;
    this -> spin_size = spin_size;
    for(int i = 0; i < 3; ++i){
        this -> position[i] = atom_center[i];
    }

    this -> basis = basis;
    this -> fine_basis = fine_basis;
    this -> fine_proj_degree = 1;

    if( addi_params -> get<int>("UsingDoubleGrid", 0) > 0 ){
        this -> fine_proj_degree = addi_params -> get<int>("FineDimension", 1);
        if( fine_proj_degree < 1 ) fine_proj_degree = 1;
        this -> fine_beta = addi_params -> get<double>("NonlocalRmax", 1.5);
    } else if( addi_params -> get<int>("UsingFiltering", 0) > 0){
        double alpha_local = 1.3; double gamma_local = 2.0;// Dummy values, since local potential is not filtered.
        RCP<Atoms> atoms = Teuchos::null; vector<double> Zvals(0);// Dummy values, will not be used since local will not be used.
        double gamma_nonlocal = addi_params->get<double>("GammaNonlocal", 1.95);
        double alpha_nonlocal = addi_params->get<double>("AlphaNonlocal", 1.1);
        double rc = this -> paw_species -> get_projector_cutoff();
        double eta = addi_params->get<double>("Eta", (3*log(10)/pow(rc*gamma_nonlocal, 2)-0.0825)/0.4871);
        // alpha = 1.1, gamma = 1.95, beta = 5*log(10)/(alpha-1.0)**2, mask = -3*log(10)*r**2/(rc*gamma)**2
        this -> filter = rcp(new Filter(basis, atoms, Zvals, gamma_local, gamma_nonlocal, alpha_local, alpha_nonlocal,eta)); 
    }

    this -> fine_filter_type = addi_params -> get<string>("FilterType", "Sinc");
    this -> store_comp_potential = addi_params -> get<int>("_CompPotStoring", 0);
    this -> id = addi_params -> get<string>("ID") + ".PAW." + this -> paw_species -> get_read_paw() -> get_atom_symbol();
    this -> occupancy_output = addi_params -> get<int>("OccupancyOutput", 2);

    Verbose::set_numformat(Verbose::Pos);
    Verbose::single(Verbose::Normal) << "------------------------------------------------" << std::endl
                                     << paw_species->get_read_paw() -> filename << "\t\t" << this->position[0] << "\t" << this->position[1] << "\t" << this->position[2] << std::endl
                                     << "------------------------------------------------" << std::endl;

    this -> index_to_l = paw_species -> get_int_to_l_map();
    this -> lmax = -1;
    if( addi_params -> isParameter("Lmax") ){
        this -> lmax = addi_params -> get<int>("Lmax");
        this -> lmax = min(this -> lmax, 2*(*std::max_element( this->index_to_l.begin(), this->index_to_l.end() )));
        this -> lmax = min(this -> lmax, this -> paw_species -> get_lmax());
    }
    if(this -> lmax < 0){
        this -> lmax = min(2*(*std::max_element( this->index_to_l.begin(), this->index_to_l.end() )), this -> paw_species -> get_lmax() );
    }

    Teuchos::RCP<Time_Measure> int_timer = Teuchos::rcp(new Time_Measure());
    int_timer -> start("PAW_Atom finegrid");
    //if( this -> fine_proj_degree > 1) this -> paw_finegrid_init();
    this -> paw_finegrid_init();
    int_timer -> end("PAW_Atom finegrid");
    int_timer -> start("PAW_Atom interpolation");
    this -> paw_function_interpolation();
    int_timer -> end("PAW_Atom interpolation");

    //this -> paw_xc_new = rcp( new Paw_XC( paw_species, rcp( this -> basis -> get_map() -> Comm().Clone() ) ) );
#if __PAW_XC_MODE__ == 1
    this -> paw_xc_new = rcp( new Paw_XC( paw_species, this -> lmax ) );
#elif __PAW_XC_MODE__ == 2
    const int * cpoints = this -> basis -> get_points();
    const double * cscaling = this -> basis -> get_scaling();
    int* points = new int[3];
    double * scaling = new double[3];
    int degree = 2;
    for(int i = 0; i < 3; ++i){
        scaling[i] = cscaling[i]/degree;
        points[i] = degree*(cpoints[i]-1)+1;
    }

    double radii = this -> paw_species -> get_paw_radius();
    //double radii = 3.0;
    int atom_number = this -> paw_species -> get_read_paw() -> get_atom_number();
    RCP<Basis> xc_basis = Create_Basis::Create_Auxiliary_Basis(points, scaling, basis -> get_map() -> Comm(), this -> basis -> get_basis_type(), Atom(atom_number, this->position, 0.0), radii);
    Verbose::single(Verbose::Normal) << "XC Mesh cutoff " << radii << std::endl << *xc_basis << std::endl;
    this -> paw_xc_new = rcp( new Paw_XC2( paw_species, xc_basis, this -> position ) );
    delete[] points; delete[] scaling;
#endif
    Verbose::set_numformat(Verbose::Time);
    int_timer -> print(Verbose::single(Verbose::Simple));
}

void Paw_Atom::update_external_potential(
    RCP<Epetra_Vector> v_ext
){
    int lmax = this -> lmax;
    vector< vector< vector<double> > > v_ext_expanded(lmax+1);
    for(int L = 0; L <= lmax; ++L){
        v_ext_expanded[L].resize(2*L+1);
        for(int M = -L; M <= L; ++M){
            v_ext_expanded[L][L+M] = Spherical_Harmonics::Expansion::expand(
                                        L, M, this -> basis,
                                        v_ext, 0, this -> paw_species -> get_grid(),
                                        this -> position.data()
                                     );
        }
    }

    this -> ext_core = this -> paw_species -> get_external_correction_scalar( v_ext_expanded );
    this -> ext_valence = this -> paw_species -> get_external_correction_matrix( v_ext_expanded );
}

void Paw_Atom::paw_finegrid_init(){
    vector<Atom> myatom;
    int atom_number = this -> paw_species -> get_read_paw() -> get_atom_number();

    myatom.push_back( Atom(atom_number, this->position, 0.0) );// symbol, position, charge
    this -> myatoms = rcp( new Atoms( myatom ) );

    // get cutoff;
    double rc = -1.0;
    for(int i = 0; i < this -> paw_species -> get_partial_wave_types().size(); ++i){
        double tmprc = this -> paw_species -> get_partial_wave_state(i).cutoff_radius;
        if( tmprc > rc ){
            rc = tmprc;
        }
    }

    auto cpoints = this -> basis -> get_points();
    auto cscaling = this -> basis -> get_scaling();
    std::array<int,3> points;
    std::array<double,3> scaling;

    Verbose::set_numformat(Verbose::Pos);
    if( this -> fine_proj_degree > 1 ){
        double radii = this->paw_species->get_projector_cutoff();
        //radii = rc;
        Verbose::single(Verbose::Normal) << "Projector function cutoff radius (bohr) = " << radii << std::endl;
        //radii *= 0.52917721092;
        for(int i = 0; i < 3; ++i){
            scaling[i] = cscaling[i]/this->fine_proj_degree;
            points[i] = this->fine_proj_degree*(cpoints[i]-1)+1;
        }
        this -> proj_basis = Create_Basis::Create_Auxiliary_Basis(points, scaling, basis -> get_map() -> Comm(), this -> basis -> get_basis_type(), this -> myatoms -> operator[](0), radii);
        Verbose::single(Verbose::Detail) << "Temporary basis for projector function doublegrid:" << std::endl;
        Verbose::single(Verbose::Detail) << *this -> proj_basis << std::endl;
    }


    //this -> fine_Ylm = this -> get_spherical_harmonics_on_grid(this -> fine_basis, this -> lmax);
}

void Paw_Atom::paw_function_interpolation(){
    Teuchos::RCP<Time_Measure> intp_timer = Teuchos::rcp(new Time_Measure());
    int state_no = this -> paw_species -> get_partial_wave_types().size();
    int state_no_m_included = this -> index_to_l.size();

    intp_timer -> start("Proj and gradient");
    // Projector functions
    RCP<Epetra_MultiVector> projector_coeffs;
    Array< RCP<Epetra_MultiVector> > projector_grads(3);
    for(int d = 0; d < 3; ++d){
        projector_grads[d] = rcp( new Epetra_MultiVector( *this -> basis -> get_map(), state_no_m_included, true ) );
    }
    if( this -> fine_proj_degree > 1 ){
        Verbose::single(Verbose::Normal) << "Using Fine Projectors of degree " << this->fine_proj_degree << std::endl;
        //this -> fine_projector_coeffs = rcp( new Epetra_MultiVector( *this -> get_projector_coeffs(this -> position) ) );
        projector_coeffs = rcp( new Epetra_MultiVector( *this -> calculate_fine_projector_coeffs(this -> position) ) );
        projector_grads = this -> calculate_fine_projector_grads(this -> position);
    } else if(this -> filter != Teuchos::null) {
        projector_coeffs = rcp( new Epetra_MultiVector( *this -> basis -> get_map(), state_no_m_included, true ) );
        vector<double> grid1 = this -> paw_species -> get_grid();
        vector<double> dgrid1 = this -> paw_species -> get_grid_derivative();
        double rc = this -> paw_species -> get_projector_cutoff();
        for(int i = 0; i < state_no; ++i){
            int l1 = this -> paw_species -> get_partial_wave_state(i).l;
            vector<double> proj1 = this -> paw_species -> get_projector_function_r(i);
            vector<double> proj2, grid2;
            this -> filter -> filter_nonlocal(proj1, grid1, dgrid1, l1, rc, proj2, grid2);
            for(int m1 = -l1; m1 <= l1; ++m1){
                RCP<Epetra_Vector> tmp = rcp( new Epetra_Vector( *this -> basis -> get_map() ) );
                Radial_Grid::Paw::get_basis_coeff_from_radial_grid(l1, m1, proj2, grid2, this -> position, this -> basis, tmp);
                projector_coeffs -> operator()( this->paw_species->get_integrated_index(i,m1) ) -> Update(1.0, *tmp, 0.0);

                // Gradient calculation
                RCP<Epetra_MultiVector> tmp_proj_grad = rcp( new Epetra_MultiVector( *this -> basis -> get_map(), 3, true ) );
                //RCP<Epetra_MultiVector> tmp_proj_grad2 = rcp( new Epetra_MultiVector( *this -> basis -> get_map(), 3, true ) );
                Radial_Grid::Paw::get_gradient_from_radial_grid(l1, m1, proj2, grid2, this -> position, this -> basis, tmp_proj_grad);
                Value_Coef::Value_Coef(this -> basis, tmp_proj_grad, true, false, tmp_proj_grad);
                for(int d = 0; d < 3; ++d){
                    projector_grads[d] -> operator()(this -> paw_species -> get_integrated_index(i,m1)) -> Update(1.0, *tmp_proj_grad -> operator()(d), 0.0);
                }
            }
        }
    } else {
        // Interpolate proj to Lagrange basis (ordinary basis)
        double rc = this -> paw_species -> get_projector_cutoff();
        projector_coeffs = rcp( new Epetra_MultiVector( *this -> basis -> get_map(), state_no_m_included, true ) );
        vector<double> grid1 = this -> paw_species -> get_grid();
        for(int i = 0; i < state_no; ++i ){
            int l1 = this -> paw_species -> get_partial_wave_state(i).l;
            vector<double> proj1 = this -> paw_species -> get_projector_function_r(i);
            for(int m1 = -l1; m1 <= l1; ++m1){
                RCP<Epetra_Vector> tmp = rcp( new Epetra_Vector( *this -> basis -> get_map() ) );
                Radial_Grid::Paw::get_basis_coeff_from_radial_grid( l1, m1, proj1, grid1, this -> position, this -> basis, tmp, rc );
                //Radial_Grid::Paw::Interpolate_from_radial_grid( l1, m1, proj1, grid1, this -> position, this -> basis, tmp, rc );
                //Radial_Grid::Paw::get_basis_coeff_from_radial_grid( l1, m1, proj1, grid1, this -> position, this -> basis, tmp );
                projector_coeffs -> operator()( this->paw_species->get_integrated_index(i,m1) ) -> Update(1.0, *tmp, 0.0);

                // Gradient calculation
                RCP<Epetra_MultiVector> tmp_proj_grad = rcp( new Epetra_MultiVector( *this -> basis -> get_map(), 3, true ) );
                //RCP<Epetra_MultiVector> tmp_proj_grad2 = rcp( new Epetra_MultiVector( *this -> basis -> get_map(), 3, true ) );
                Radial_Grid::Paw::get_gradient_from_radial_grid( l1, m1, proj1, grid1, this -> position, this -> basis, tmp_proj_grad );
                Value_Coef::Value_Coef(this -> basis, tmp_proj_grad, true, false, tmp_proj_grad);
                for(int d = 0; d < 3; ++d){
                    projector_grads[d] -> operator()(this -> paw_species -> get_integrated_index(i,m1)) -> Update(1.0, *tmp_proj_grad -> operator()(d), 0.0);
                }
            }
        }
    }
    intp_timer -> end("Proj and gradient");
    //RCP<Atoms> tmp = rcp(new Atoms(vector<Atom>(1, Atom(this -> paw_species -> get_read_paw() -> get_atom_number(), this -> position, 0))));
    //mesh -> write_cube(this -> id, tmp, projector_coeffs);
    intp_timer -> start("Distribute proj");
    Parallel_Util::group_extract_nonzero_from_multivector(projector_coeffs, this -> proj_coeffs, this -> proj_inds);
    proj_grad_coeffs.resize(3); this -> proj_grad_inds.resize(3);
    for(int d = 0; d < 3; ++d){
        Parallel_Util::group_extract_nonzero_from_multivector(projector_grads[d], this -> proj_grad_coeffs[d], this -> proj_grad_inds[d]);
    }
    Verbose::single(Verbose::Detail) << "Removed MultiVector-form projector coefficients!" << std::endl;
    projector_grads.clear(); // Should remove for OPT.
    intp_timer -> end("Distribute proj");
    for(int i = 0; i < this -> proj_inds.size(); ++i){
        if(this -> proj_inds[i].size() == 0){
            Verbose::all() << "Projector " << i << " has no interpolated values. Isn't simulation box is too coarse?" << std::endl;
            throw std::invalid_argument("Projector function has no interpolated values.");
        }
    }

    // Interpolate v_H[g_L] to Lagrange basis (fine basis)
    vector<string> pw_type = this-> paw_species -> get_partial_wave_types();
    vector<double> grid = this -> paw_species -> get_grid();
    vector<double> dgrid = this -> paw_species -> get_grid_derivative();

    vector<int> pw_l_list = this -> index_to_l;
    int lmax = this -> lmax;

    this -> compensation_potential.clear();
    if(this -> store_comp_potential == 0){
        Verbose::single(Verbose::Detail) << "Interpolate compensation potential when requested." << std::endl;
    } else if(this -> store_comp_potential == 1){
        Verbose::single(Verbose::Detail) << "Store compensation potential." << std::endl;
        intp_timer -> start("Comp pot");
        this -> compensation_potential = this -> interpolate_compensation_potential(true, this -> lmax);
        intp_timer -> end("Comp pot");
    }

    // Calculate compensation charge gradient
    /*
    intp_timer -> start("Fine comp charge grad");
    this -> compensation_charge_grad = this -> interpolate_compensation_charge_grad(this -> fine_basis, this -> lmax);
    intp_timer -> end("Fine comp charge grad");
    */

    intp_timer -> start("Coarse comp pot");
    this -> coarse_compensation_potential = this -> interpolate_compensation_potential(false, this -> lmax);
    intp_timer -> end("Coarse comp pot");

    Verbose::set_numformat(Verbose::Time);
    intp_timer -> print(Verbose::single(Verbose::Simple));
    /*
    int fine_elems = this -> fine_basis -> get_map() -> NumGlobalElements();
    int coarse_elems = this -> basis -> get_map() -> NumGlobalElements();
    int l_elems = lmax*(lmax+1)*(2*lmax+1)/6;
    float mb_to_num_double = 1024*1024/8;
    int proj_elems = 0;
    for(int i = 0; i < this -> proj_inds.size(); ++i){
        proj_elems += this -> proj_inds[i].size();
    }
    Verbose::single(Verbose::Detail) << "Memory estimation:" << std::endl
//                    << "Core density potential = " << (fine_elems+coarse_elems)/mb_to_num_double << " MB" << std::endl
                      << "Fine comp potential = " << fine_elems*l_elems/mb_to_num_double << " MB" << std::endl
                      << "Coarse comp potential = " << coarse_elems*l_elems/mb_to_num_double << " MB" << std::endl
//                    << "Fine comp charge = " << fine_elems*l_elems/mb_to_num_double << " MB" << std::endl
                      << "Proj and grad (tmp) = " << state_no_m_included*4*coarse_elems/mb_to_num_double << " MB" << std::endl
                      << "Proj and grad (permanent) = " << proj_elems/mb_to_num_double*1.5 << " MB" << std::endl;
    */
}

void Paw_Atom::get_energy_correction(
    Array< SerialDenseMatrix<int,double> > &sD_matrix,
    double &hartree_energy,
    double &int_n_vxc,
    double &x_energy,
    double &c_energy,
    std::vector<double> &kinetic_energies,
    double &zero_energy,
    double &external_energy
){
    SerialDenseMatrix<int,double> D_matrix;
    D_matrix.shape( sD_matrix[0].numRows(), sD_matrix[0].numCols() );
    for(int s = 0; s < sD_matrix.size(); ++s){
        D_matrix += sD_matrix[s];
    }

    // just returning pre-calculated values: most time-consuming part (~1s for C)
    vector< vector<double> > K_mat = this -> paw_species -> get_kinetic_correction_matrix();
    vector< vector<double> > dC_mat = this -> paw_species -> get_dE_matrix();
    vector< vector< vector< vector<double> > > > dC_tens = this -> paw_species -> get_dE_tensor();
    vector< vector<double> > MB_mat = this -> paw_species -> get_zero_correction_matrix();

    int state_no = D_matrix.numRows();

    std::vector<double> kinetic_energies_corr(sD_matrix.size(), this -> paw_species -> get_kinetic_correction_scalar()/sD_matrix.size());
    for(int s = 0; s < sD_matrix.size(); ++s){
        for(int i = 0; i < state_no; ++i){
            for(int j = 0; j < state_no; ++j){
                kinetic_energies_corr[s] += K_mat[i][j] * sD_matrix[s](i,j);
            }
        }
    }

    // external energy + zero correction
    //*
    double zero_energy_corr = this -> paw_species -> get_zero_correction_scalar();
    for(int i = 0; i < state_no; ++i){
        for(int j = 0; j < state_no; ++j){
            zero_energy_corr += MB_mat[i][j] * D_matrix(i,j);
        }
    }
    // */
    // Include external energy
    double external_energy_corr = 0.0;

    vector<double> xcval = this -> paw_xc_new -> get_xc_energy_correction( sD_matrix );

    // Include int_n_vxc
    //double int_n_vxc = this -> paw_xc_new -> get_int_n_vxc_correction( sD_matrix );
    //int_n_vxc += 0.0;

    double hartree_energy_corr1 = this -> paw_species -> get_dE_scalar();
    double hartree_energy_corr2 = 0.0;
    double hartree_energy_corr3 = 0.0;
    for(int i = 0; i < state_no; ++i){
        for(int j = 0; j < state_no; ++j){
            hartree_energy_corr2 += dC_mat[i][j] * D_matrix(i,j);
        }
    }
    for(int i = 0; i < state_no; ++i){
        for(int j = 0; j < state_no; ++j){
            for(int k = 0; k < state_no; ++k){
                for(int l = 0; l < state_no; ++l){
                    hartree_energy_corr3 += D_matrix(i,j) * dC_tens[i][j][k][l] * D_matrix(k,l);
                }
            }
        }
    }
    double hartree_energy_corr = hartree_energy_corr1 + hartree_energy_corr2 + hartree_energy_corr3;
    for(int s = 0; s < sD_matrix.size(); ++s){
        kinetic_energies[s] += kinetic_energies_corr[s];
    }
    zero_energy += zero_energy_corr;
    external_energy += external_energy_corr;
    x_energy += xcval[0];
    c_energy += xcval[1];
    hartree_energy += hartree_energy_corr;

    /*
    double kinetic_energy_corr = 0.0;
    for(int s = 0; s < sD_matrix.size(); ++s){
        kinetic_energy_corr += kinetic_energies_corr[s];
    }
    Verbose::single(Verbose::Detail) << "Atom " << this -> paw_species -> get_read_paw() -> get_atom_symbol() << " energy correction" << std::endl;
    Verbose::single(Verbose::Detail) << "----------------------------------------" << std::endl;
    Verbose::single(Verbose::Detail) << " Correction Kinetic           = " << std::setprecision(10) << kinetic_energy_corr<< " Ha" << std::endl;
    Verbose::single(Verbose::Detail) << "   Correction Kinetic (REF)   = " << std::setprecision(10) << this -> paw_species -> get_read_paw() -> get_ae_kinetic_energy() << " Ha" << std::endl;
    Verbose::single(Verbose::Detail) << "   Correction Kinetic (-REF)  = " << std::setprecision(10) << kinetic_energy_corr - this -> paw_species -> get_read_paw() -> get_ae_kinetic_energy() << " Ha" << std::endl;
    Verbose::single(Verbose::Detail) << " Correction Electrostatic = " << std::setprecision(10) << hartree_energy_corr << " Ha" << std::endl;
    Verbose::single(Verbose::Detail) << "   Correction Electrostatic core-core (REF) = " << std::setprecision(10) << hartree_energy_corr1 << " Ha" << std::endl;
    Verbose::single(Verbose::Detail) << "   Correction Electrostatic core-valn       = " << std::setprecision(10) << hartree_energy_corr2 << " Ha" << std::endl;
    Verbose::single(Verbose::Detail) << "   Correction Electrostatic valn-valn       = " << std::setprecision(10) << hartree_energy_corr3 << " Ha" << std::endl;
    Verbose::single(Verbose::Detail) << " Correction XC              = " << std::setprecision(10) << xcval[0]+xcval[1] - this -> paw_species -> get_read_paw() -> get_ae_xc_energy() << " Ha" << std::endl;
    Verbose::single(Verbose::Detail) << "   Correction XC (REF)      = " << std::setprecision(10) << xcval[0]+xcval[1] << " Ha" << std::endl;
    Verbose::single(Verbose::Detail) << "   Correction XC (-REF)     = " << std::setprecision(10) << this -> paw_species -> get_read_paw() -> get_ae_xc_energy() << " Ha" << std::endl;
    Verbose::single(Verbose::Detail) << " Correction Local+external = " << std::setprecision(10) << external_energy_corr << " Ha" << std::endl;
    Verbose::single(Verbose::Detail) << "----------------------------------------" << std::endl;
    // */
}

vector< vector< vector<double> > > Paw_Atom::projector_dot_orbitals(
    Array< RCP<const Epetra_MultiVector> > orbitals
){
    int state_no_m_included = this -> index_to_l.size();

    // Calculate proj * wf integration
    vector< vector< vector<double> > > integrated;
    integrated.resize( spin_size );
    for(int alpha = 0; alpha < spin_size; ++alpha){
        integrated[alpha].resize(orbitals[alpha] -> NumVectors());
        for(int n = 0; n < orbitals[alpha] -> NumVectors(); ++n){
            integrated[alpha][n].resize(state_no_m_included, 0.0);
        }
    }
    for(int alpha = 0; alpha < spin_size; ++alpha ){
        for(int n = 0; n < orbitals[alpha] -> NumVectors(); ++n ){
            vector<double> orb_proj_tmp(state_no_m_included);
            for(int i = 0; i < state_no_m_included; ++i){
                //this -> projector_coeffs -> operator()(i) -> Dot( *(orbitals[alpha]->operator()(n)), &integrated[alpha][n][i] );

                for(int ind = 0; ind < this -> proj_inds[i].size(); ++ind){
                    if( orbitals[alpha] -> Map().MyGID(this -> proj_inds[i][ind]) ){
                        orb_proj_tmp[i] += this -> proj_coeffs[i][ind] * orbitals[alpha] -> operator[](n)[orbitals[alpha]->Map().LID(this -> proj_inds[i][ind])];
                    }
                }
            }
            Parallel_Util::all_sum(&orb_proj_tmp[0], &integrated[alpha][n][0], state_no_m_included);
        }
    }
    return integrated;
}

Array< SerialDenseMatrix<int,double> > Paw_Atom::get_one_center_density_matrix(
    Array< RCP<const Occupation> > occupations,
    Array< RCP<const Epetra_MultiVector> > wavefunctions
){
    int state_no_m_included = this -> index_to_l.size();

    Array< SerialDenseMatrix<int,double> > sD_matrix;
    sD_matrix.resize( spin_size );
    for(int s = 0; s < spin_size; ++s){
        sD_matrix[s].shape( state_no_m_included, state_no_m_included );
    }

    // Calculate proj * wf integration
    vector< vector< vector<double> > > integrated;
    integrated = this -> projector_dot_orbitals( wavefunctions );

    // Calculate D matrix
    for(int alpha = 0; alpha < spin_size; ++alpha ){
        for(int n = 0; n < wavefunctions[alpha] -> NumVectors(); ++n){
            for(int i = 0; i < state_no_m_included; ++i ){
                for(int j = 0; j < state_no_m_included; ++j ){
                    double tmpval = occupations[alpha] -> operator[](n)
                                   * (integrated[alpha][n][i] * integrated[alpha][n][j]);
                    sD_matrix[alpha](i,j) += tmpval;
                }
            }
        }
    }
    //this -> print_orbital_occupancy(occupations, wavefunctions);
    return sD_matrix;
}

Array< SerialDenseMatrix<int,double> > Paw_Atom::get_one_center_initial_density_matrix(double charge/* = 0*/){
    int state_no = this -> paw_species -> get_partial_wave_types().size();
    int state_no_m_included = this -> index_to_l.size();

    Array< SerialDenseMatrix<int,double> > sD_matrix;
    sD_matrix.resize( this -> spin_size );
    for(int s = 0; s < this->spin_size; ++s){
        sD_matrix[s].shape( state_no_m_included, state_no_m_included );
    }

    vector<double> occupation_list;
    vector<int> ipw_list;
    for(int i = 0; i < state_no; ++i ){
        occupation_list.push_back( this -> paw_species -> get_partial_wave_state(i).occupation_number );
        ipw_list.push_back(i);
    }
    std::sort( ipw_list.begin(), ipw_list.end(),
               [&](int i, int j)->bool{
                   return this -> paw_species -> get_read_paw() -> get_partial_wave_state(i).energy < this -> paw_species -> get_read_paw() -> get_partial_wave_state(j).energy;
               }
    );
    if( charge > 0.0 ){
        for(int i = state_no-1; i >= 0; --i){
            if( charge <= 0 ) break;
            double f = occupation_list[ipw_list[i]];
            if( f > 0 ){
                if(charge < 1.0){
                    occupation_list[ipw_list[i]] -= charge;
                    charge = 0;
                    break;
                } else {
                    charge -= 1;
                    occupation_list[ipw_list[i]] -= 1;
                }
            }
        }
    } else if( charge < 0.0 ){
        for(int i = 0; i < state_no; ++i){
            if( charge >= 0 ) break;
            if( occupation_list[ipw_list[i]] < 4*this -> paw_species -> get_partial_wave_state(ipw_list[i]).l+2 ){
                if(charge > -1.0){
                    occupation_list[ipw_list[i]] -= charge;
                    charge = 0;
                    break;
                } else {
                    charge += 1;
                    occupation_list[ipw_list[i]] += 1;
                }
            }
        }
    }

    for(int i = 0; i < state_no; ++i ){
        int l = this -> paw_species -> get_partial_wave_state(i).l;
        //int f = this -> paw_species -> get_partial_wave_state(i).occupation_number;
        double f = occupation_list[i];
        if( f < 1.0E-10 ){
            //break;
            continue;
        }
        for(int m = -l; m <= l; ++m ){
            int index = this -> paw_species -> get_integrated_index(i, m);
            if( spin_size == 1 ){
                if( f >= 4*l+2 ){
                    // Fully filled
                    sD_matrix[0](index,index) = 2.0;
                } else if( f <= 2*l+1 ){
                    // Partially filled.
                    if( l+m <= f/2 ){
                        sD_matrix[0](index,index) = 1.0;
                    } else if( l+m+1 > f/2 ){
                        //sD_matrix[0](index,index) = f/2-l-m;
                        sD_matrix[0](index,index) = f - (int)f;
                    }
                    //*
                } else if( f > 2*l+1 && f < 4*l+2 ){
                    // Partially filled. Alpha spins are fully filled.
                    if( l+m+1 <= f - (2*l+1) ){
                        sD_matrix[0](index,index) = 2.0;
                    } else if( l+m < f - (2*l+1) ){
                        //sD_matrix[0](index,index) = f/2-l-m;
                        sD_matrix[0](index,index) = f - (int)f + 1.0;
                    } else {
                        sD_matrix[0](index,index) = 1.0;
                    }
                    //*/
                }
            } else if( spin_size == 2 ){
                if( f >= 4*l+2 ){
                    // Fully filled
                    sD_matrix[0](index,index) = 1.0;
                    sD_matrix[1](index,index) = 1.0;
                } else if( f <= 2*l+1 ){
                    // Partially filled. No beta spins.
                    if( l+m+1 <= f ){
                        sD_matrix[0](index,index) = 1.0;
                    } else if( l+m < f ){
                        sD_matrix[0](index,index) = f - (int)f;
                    }
                } else if( f > 2*l+1 && f < 4*l+2 ){
                    // Partially filled. Alpha spins are fully filled.
                    sD_matrix[0](index,index) = 1.0;
                    if( l+m+1 <= f - (2*l+1) ){
                        sD_matrix[1](index,index) = 1.0;
                    } else if( l+m < f - (2*l+1) ){
                        sD_matrix[1](index,index) = f - (int)f;
                    }
                }
            }
        }
    }
    return sD_matrix;
}

vector< vector< vector<double> > > Paw_Atom::get_Hamiltonian_correction_matrix(
    RCP<Epetra_Vector> Hartree_potential,
    Array< SerialDenseMatrix<int,double> > &sD_matrix
){
    vector<string> pw_type = this -> paw_species -> get_partial_wave_types();

    //Verbose::single(Verbose::Detail) << "Paw_Atom:: hamiltonian correction matrix calculation with spin size of " << sD_matrix.size() << std::endl;
    RCP<Time_Measure> timer = rcp( new Time_Measure());

    int dH_size = this -> index_to_l.size();
    vector< vector< vector<double> > > dH_list;
    dH_list.resize( sD_matrix.size() );
    for(int s = 0; s < dH_list.size(); ++s){
        dH_list[s].resize( dH_size );
        for(int i = 0; i < dH_size; ++i){
            dH_list[s][i].resize( dH_size );
        }
    }

    timer -> start("PAW_Atom XC");
    vector< vector< vector<double> > > H_xc_corr;
    // 7s - 11s for C atom PBE. 12s - 120s for Na atom PBE. FAILED to correct this, but just parallelized if using Paw_XC (1).
    H_xc_corr = this -> paw_xc_new -> get_xc_hamiltonian_correction( sD_matrix );
    timer -> end("PAW_Atom XC");

    timer -> start("PAW_Atom noXC");
// Hartree potential
    double scaling = this -> fine_basis -> get_scaling()[0] * this -> fine_basis -> get_scaling()[1] * this -> fine_basis -> get_scaling()[2];

    int lmax = this -> lmax;
    vector<double> grid_r = this -> paw_species -> get_grid();
    vector<double> dgrid_r = this -> paw_species -> get_grid_derivative();
    vector< vector<double> > Hartree_dot_comp;
    Hartree_dot_comp.resize(lmax+1);

    Array< RCP<Epetra_MultiVector> > fine_compensation_charge = this -> interpolate_compensation_charge(true, this -> lmax); // XXX KSW removing comp charge mem
    for(int L = 0; L <= lmax; ++L){
        Hartree_dot_comp[L].resize(2*L+1);
        //vector<double> g_L = this -> paw_species -> get_compensation_charge_r( L );
        for(int M = -L; M <= L; ++M){
            fine_compensation_charge[L] -> operator()(L+M) -> Dot( *Hartree_potential, &Hartree_dot_comp[L][L+M] );
            /*
            RCP<Epetra_Vector> tmp_comp_charge = rcp(new Epetra_MultiVector(*this -> fine_basis -> get_map(), true));
            tmp_comp_charge -> ReplaceGlobalValues(this -> fine_comp_inds[L][L+M].size(), this -> fine_comp_vals[L][L+M].data(), this -> fine_comp_inds[L][L+M].data())
            tmp_comp_charge -> Dot( *Hartree_potential, &Hartree_dot_comp[L][L+M] );
            */
            Hartree_dot_comp[L][L+M] *= scaling;
            //Verbose::single(Verbose::Detail) << "(g|g) LM = " << L << ", " << M << " = " << Hartree_dot_comp[L][L+M] << std::endl;
        }//for M
    }//for L

    vector< vector<double> > coeffs = this -> get_compensation_charge_expansion_coeff( sD_matrix );

    vector< vector<double> > K_mat = this -> paw_species -> get_kinetic_correction_matrix();
    vector< vector<double> > dC_mat = this -> paw_species -> get_dE_matrix();
    vector< vector< vector< vector<double> > > > dC_tens = this -> paw_species -> get_dE_tensor();
    vector< vector<double> > MB_mat = this -> paw_species -> get_zero_correction_matrix();
    vector< vector< vector< vector<double> > > > delta_mat = this -> paw_species -> get_delta_matrix();

    for(int index1 = 0; index1 < sD_matrix[0].numRows(); ++index1){
        for(int index2 = 0; index2 < sD_matrix[0].numCols(); ++index2){
            if( index1 > index2 ){
                continue;
            }

            // KE part
            double k_val = K_mat[index1][index2];

            // Nonlocal E part
            double ne_val = dC_mat[index1][index2];
            for(int i = 0; i < sD_matrix[0].numRows(); ++i){
                for(int j = 0; j < sD_matrix[0].numCols(); ++j){
                    for(int s = 0; s < sD_matrix.size(); ++s){
                        ne_val += 2 * dC_tens[index1][index2][i][j] * sD_matrix[s](i,j);// derivative by D_matrix, not sD_matrix.
                    }
                }
            }
            // Kinetic, nonlocal correction is fast (~ 0s, non-parallel)

            // zero E part
            //double ze_val = 0.0;
            double ze_val = MB_mat[index1][index2];
            // zero potential correction is fast (~ 0s, non-parallel)

            // Interaction between PS Hartree potential compensation charge
            double h_val = 0.0;
            int l1 = this -> paw_species -> get_int_to_l_map()[index1];
            int l2 = this -> paw_species -> get_int_to_l_map()[index2];
            for(int L = abs(l1-l2); L <= min(l1+l2, lmax); ++L){
            //for(int L = 0; L <= lmax; ++L){
                for(int M = -L; M <= L; ++M){
                    h_val += delta_mat[L][L+M][index1][index2] * Hartree_dot_comp[L][L+M];
                }
            }
            // Interaction with PS Hartree potential comp charge is fast (0.08~0.2s, parallel on 16 processors)
            double H_noxc = k_val + ne_val + ze_val + h_val;
            for(int s = 0; s < sD_matrix.size(); ++s){
                dH_list[s][index1][index2] = H_noxc + H_xc_corr[index1][index2][s];
                dH_list[s][index2][index1] = H_noxc + H_xc_corr[index2][index1][s];
            }
        }
    }
    timer -> end("PAW_Atom noXC");
    timer -> print(Verbose::single(Verbose::Detail));
    //*
    if(true){
        std::ios oldstate(nullptr);
        oldstate.copyfmt(Verbose::single());
        Verbose::single(Verbose::Detail) << "HAMILTONIAN CORRECTION " << Parallel_Manager::info().get_mpi_rank() << std::endl;
        for(int s = 0; s < dH_list.size(); ++s){
            Verbose::single(Verbose::Detail) << "SPIN " << s << std::endl;
            for(int i = 0; i < dH_size; ++i){
                for(int j = 0; j < dH_size; ++j){
                    Verbose::single(Verbose::Detail) << std::fixed << std::setw(8) << std::setprecision(5) << dH_list[s][i][j] << "\t";
                }
                Verbose::single(Verbose::Detail) << std::endl;
            }
            Verbose::single(Verbose::Detail) << std::endl;
        }
        Verbose::single().copyfmt(oldstate);
    }
    if(false){
        std::ios oldstate(nullptr);
        oldstate.copyfmt(Verbose::single());
        Verbose::single(Verbose::Detail) << "HAMILTONIAN XC CORRECTION " << Parallel_Manager::info().get_mpi_rank() << std::endl;
        for(int s = 0; s < dH_list.size(); ++s){
            Verbose::single(Verbose::Detail) << "SPIN " << s << std::endl;
            for(int i = 0; i < dH_size; ++i){
                for(int j = 0; j < dH_size; ++j){
                    Verbose::single(Verbose::Detail) << std::fixed << std::setw(8) << std::setprecision(5) << H_xc_corr[i][j][s] << "\t";
                }
                Verbose::single(Verbose::Detail) << std::endl;
            }
            Verbose::single(Verbose::Detail) << std::endl;
        }
        Verbose::single().copyfmt(oldstate);
    }
    // */
    // dH_ij generation is slow (~7s, parallel for xc, h) / 15 elements calculation: 5s per element.
    return dH_list;
}

RCP<Epetra_Vector> Paw_Atom::get_zero_potential(){
    vector<double> zero_potential_r = this -> paw_species -> get_zero_potential_r();
    vector<double> grid = this -> paw_species -> get_grid();
    double rc = this -> paw_species -> get_cutoff_radius();

    RCP<Epetra_Vector> zero_potential = rcp( new Epetra_Vector( *(this -> basis -> get_map()) ) );
    Radial_Grid::Paw::Interpolate_from_radial_grid( 0, 0, zero_potential_r, grid, this -> position, this -> basis, zero_potential, rc );

    return zero_potential;
}

Array< RCP<Epetra_Vector> > Paw_Atom::get_zero_potential_gradient(bool is_fine/* = false*/){
    RCP<const Basis> obasis;
    vector<double> zero_potential_r = this -> paw_species -> get_zero_potential_r();
    vector<double> grid = this -> paw_species -> get_grid();
    double rc = this -> paw_species -> get_cutoff_radius();

    if(is_fine){
        obasis = this -> fine_basis;
    } else {
        obasis = this -> basis;
    }

    Array< RCP<Epetra_Vector> > zero_potential_grad;
    for(int d = 0; d <3; ++d){
        zero_potential_grad.append( rcp( new Epetra_Vector( *(obasis -> get_map()) ) ) );
    }
    RCP<Epetra_MultiVector> tmp_zero_pot_grad = rcp( new Epetra_MultiVector( *(obasis -> get_map()), 3 ) );
    Radial_Grid::Paw::get_gradient_from_radial_grid( 0, 0, zero_potential_r, grid, this -> position, obasis, tmp_zero_pot_grad, rc );

    for(int d = 0; d <3; ++d){
        zero_potential_grad[d] -> Update(1.0, *tmp_zero_pot_grad -> operator()(d), 0.0);
    }

    return zero_potential_grad;
}

Array< RCP<Epetra_Vector> > Paw_Atom::get_atomcenter_density_vector(
        bool is_ae,
        Array< SerialDenseMatrix<int,double> > &sD_matrix,
        RCP<const Basis> obasis/* = Teuchos::null*/
){
    // Returns atomcenter ae/ps density
    if( obasis == Teuchos::null ){
        obasis = this -> basis;
    }

    vector<string> pw_type = this-> paw_species -> get_partial_wave_types();

    vector<double> density;
    if( is_ae ){
        density = this -> paw_species -> get_all_electron_core_density_r();
    } else {
        density = this -> paw_species -> get_smooth_core_density_r();
    }
    vector<double> grid = this -> paw_species -> get_grid();

    RCP<Epetra_MultiVector> pw_vectors = rcp( new Epetra_MultiVector( *obasis -> get_map(), this->index_to_l.size() ) );
    for(int i = 0; i < pw_type.size(); ++i){
        vector<double> pw;
        if( is_ae ){
            pw = this -> paw_species -> get_all_electron_partial_wave_r(i);
        } else {
            pw = this -> paw_species -> get_smooth_partial_wave_r(i);
        }
        int l = this -> paw_species -> get_partial_wave_state(i).l;
        double rc = this -> paw_species -> get_partial_wave_state(i).cutoff_radius;
        for(int m = -l; m <= l; ++m){
            int index = this -> paw_species -> get_integrated_index(i, m);
            RCP<Epetra_Vector> tmp = rcp( new Epetra_Vector( pw_vectors->Map() ) );
            Radial_Grid::Paw::Interpolate_from_radial_grid( l, m, pw, grid, this -> position, obasis, tmp, rc );
            pw_vectors -> operator()(index) -> Update(1.0, *tmp, 0.0);
        }
    }

    Array< RCP<Epetra_Vector> > density_correction;
    for(int s = 0; s < sD_matrix.size(); ++s){
        density_correction.push_back( rcp( new Epetra_Vector( *obasis -> get_map(), false ) ) );
    }

    RCP<Epetra_Vector> tmp_core = rcp( new Epetra_Vector( *obasis -> get_map() ) );
    Radial_Grid::Paw::Interpolate_from_radial_grid( 0, 0, density, grid, this->position, obasis, tmp_core );

    for( int s = 0; s < sD_matrix.size(); ++s){
        density_correction[s] -> Update( 1.0/(sD_matrix.size()), *tmp_core, 0.0 );
    }

    for(int i = 0; i < pw_type.size(); ++i){
        for(int j = 0; j < pw_type.size(); ++j){
            int l1 = this -> paw_species -> get_partial_wave_state(i).l;
            int l2 = this -> paw_species -> get_partial_wave_state(j).l;
            for(int m1 = -l1; m1 <= l1; ++m1){
                for(int m2 = -l2; m2 <= l2; ++m2){
                    int index1 = this -> paw_species -> get_integrated_index(i, m1);
                    int index2 = this -> paw_species -> get_integrated_index(j, m2);

                    /*
                    double coeff = 0.0;
                    for(int s = 0; s < sD_matrix.size(); ++s){
                        coeff += abs(sD_matrix[s][index1][index2]);
                    }
                    */
                    RCP<Epetra_Vector> tmp = rcp( new Epetra_Vector( *obasis -> get_map(), false ) );
                    tmp -> Multiply( 1.0, *pw_vectors->operator()(index1), *pw_vectors->operator()(index2), 0.0 );

                    for(int s = 0; s < sD_matrix.size(); ++s){
                        density_correction[s] -> Update( sD_matrix[s][index1][index2], *tmp, 1.0 );
                    }
                }// m2
            }// m1
        }// j
    }// i

    return density_correction;
}

Array< RCP<Epetra_Vector> > Paw_Atom::get_density_correction( Array< SerialDenseMatrix<int,double> > &sD_matrix ){
    // Returns density correction that can be directly added to the density
    Array< RCP<Epetra_Vector> > ae_density, ps_density;

    ae_density = this -> get_atomcenter_density_vector( true, sD_matrix );
    ps_density = this -> get_atomcenter_density_vector( false, sD_matrix );

    Array< RCP<Epetra_Vector> > retval;
    for(int s = 0; s < sD_matrix.size(); ++s){
        retval.push_back( rcp( new Epetra_Vector( *this -> basis -> get_map(), false ) ) );

        retval[s] -> Update( 1.0, *ae_density[s], 0.0 );
        retval[s] -> Update( -1.0, *ps_density[s], 1.0 );
    }
    return retval;
}

void Paw_Atom::get_orbital_correction(
    Array< RCP<Epetra_MultiVector> > ps_orbitals,
    Array< RCP<Epetra_MultiVector> > &ae_orbitals
){
    int spin_size = ps_orbitals.size();
    int state_no = this -> index_to_l.size();
    ae_orbitals.resize(spin_size);
    for(int s = 0; s < spin_size; ++s){
        ae_orbitals[s] = rcp( new Epetra_MultiVector( ps_orbitals[s] -> Map(), ps_orbitals[s] -> NumVectors() ) );
    }

    RCP<Epetra_MultiVector> ae_pw = rcp( new Epetra_MultiVector( *this -> basis -> get_map(), state_no ) );
    RCP<Epetra_MultiVector> ps_pw = rcp( new Epetra_MultiVector( *this -> basis -> get_map(), state_no ) );
    this -> get_atomcenter_orbitals( true, ae_pw, false );
    this -> get_atomcenter_orbitals( false, ps_pw, false );

    RCP<Epetra_MultiVector> projector_coeffs = rcp(new Epetra_MultiVector(*this -> basis -> get_map(), state_no));
    for(int i = 0; i < state_no; ++i){
        projector_coeffs -> operator()(i) -> ReplaceGlobalValues(this -> proj_inds[i].size(), this -> proj_coeffs[i].data(), this -> proj_inds[i].data());
        for(int s = 0; s < spin_size; ++s ){
            for(int n = 0; n < ps_orbitals[s] -> NumVectors(); ++n ){
                double integrated;
                projector_coeffs -> operator()(i) -> Dot( *(ps_orbitals[s]->operator()(n)), &integrated );
                //this -> projector_coeffs -> operator()(i) -> Dot( *(ps_orbitals[s]->operator()(n)), &integrated );
                ae_orbitals[s] -> operator()(n) -> Update( integrated, *ae_pw -> operator()(i), 1.0);
                ae_orbitals[s] -> operator()(n) -> Update( -integrated, *ps_pw -> operator()(i), 1.0);

            }
        }
    }
}

RCP<Epetra_Vector> Paw_Atom::get_compensation_charge(
        Array< SerialDenseMatrix<int,double> > &sD_matrix,
        bool is_fine/* = false */
){
    // Returns Hartree potential from compensation charge
    int lmax = this -> lmax;
    vector< vector<double> > coeffs = this -> get_compensation_charge_expansion_coeff( sD_matrix );
    RCP<Epetra_Vector> comp_charge;
    if( is_fine){
        comp_charge = rcp( new Epetra_Vector( *this -> fine_basis -> get_map(), true ) );
    } else {
        comp_charge = rcp( new Epetra_Vector( *this -> basis -> get_map(), true ) );
    }

    Array< RCP<Epetra_MultiVector> > fine_compensation_charge = this -> interpolate_compensation_charge(true, this -> lmax); // XXX KSW removing comp charge mem
    for(int l = 0; l <= lmax; ++l){
        for(int m = -l; m <= l; ++m){
            if( is_fine ){
                comp_charge -> Update( coeffs[l][l+m], *fine_compensation_charge[l] -> operator()(l+m), 1.0 );
                //comp_charge -> Update( coeffs[l][l+m], *this -> compensation_charge[l] -> operator()(l+m), 1.0 );
            } else {
                RCP<Epetra_Vector> tmp = rcp( new Epetra_Vector( *this -> basis -> get_map(), true ) );
                //Tricubic_Interpolation::interpolate(
                Interpolation::Trilinear::interpolate(
                    this -> fine_basis, rcp( new Epetra_Vector( *fine_compensation_charge[l] -> operator()(l+m) ) ),
                    //this -> fine_basis, rcp( new Epetra_Vector( *this -> compensation_charge[l] -> operator()(l+m) ) ),
                    this -> basis, tmp
                );
                comp_charge -> Update( coeffs[l][l+m], *tmp, 1.0 );
            }
        }
    }
    return comp_charge;
}

RCP<Epetra_MultiVector> Paw_Atom::get_compensation_charge_grad(
        Array< SerialDenseMatrix<int,double> > &sD_matrix,
        bool is_fine/* = false */
){
    int lmax = this -> lmax;
    if(this -> compensation_charge_grad.size() != 3){
        this -> compensation_charge_grad = this -> interpolate_compensation_charge_grad(this -> fine_basis, lmax);
    }
    vector< vector<double> > coeffs = this -> get_compensation_charge_expansion_coeff( sD_matrix );
    //RCP<Epetra_MultiVector> comp_charge_grad = rcp( new Epetra_MultiVector( *this -> basis -> get_map(), 3, true ) );
    RCP<Epetra_MultiVector> comp_charge_grad = rcp( new Epetra_MultiVector( *this -> fine_basis -> get_map(), 3, true ) );

    for(int l = 0; l <= lmax; ++l){
        for(int m = -l; m <= l; ++m){
            for(int i = 0; i < 3; ++i){
                if( this -> compensation_charge_grad[i][l] == Teuchos::null ){
                    Verbose::all() << "No compensation charge gradient!" << std::endl;
                    exit(EXIT_FAILURE);
                }
                comp_charge_grad -> operator()(i) -> Update( coeffs[l][l+m], *this -> compensation_charge_grad[i][l] -> operator()(l+m), 1.0 );
            }
        }
    }

    /*
    if(is_fine){
        RCP<Epetra_MultiVector> comp_charge_grad_new = rcp( new Epetra_MultiVector( *this -> fine_basis -> get_map(), 3, true ) );
        Interpolation::Trilinear::interpolate(
            this -> basis, comp_charge_grad,
            this -> fine_basis, comp_charge_grad_new
    */
    if(!is_fine){
        RCP<Epetra_MultiVector> comp_charge_grad_new = rcp( new Epetra_MultiVector( *this -> basis -> get_map(), 3, true ) );
        Interpolation::Trilinear::interpolate(
            this -> fine_basis, comp_charge_grad,
            this -> basis, comp_charge_grad_new
        );
        comp_charge_grad = comp_charge_grad_new;
    }
    return comp_charge_grad;
}

Array< RCP<Epetra_Vector> > Paw_Atom::get_smooth_core_potential(
        bool is_fine/* = false */
){
    // Returns Hartree potential from smooth core density
    vector<double> grid = this -> paw_species -> get_grid();
    vector<double> dgrid = this -> paw_species -> get_grid_derivative();
    vector<double> core_density_r = this -> paw_species -> get_smooth_core_density_r();
    vector<double> core_Hartree_r = Radial_Grid::Paw::calculate_Hartree_potential_r( core_density_r, 0, grid, dgrid );
    Array< RCP<Epetra_Vector> > core_potential;

    for(int s = 0; s < this -> spin_size; ++s){
            RCP<Epetra_Vector> core_density_potential;;
        if( is_fine){
            core_density_potential = rcp( new Epetra_Vector( *this -> fine_basis -> get_map(), true ) );
            Radial_Grid::Paw::Interpolate_from_radial_grid(0, 0, core_Hartree_r, grid, this -> position, this -> fine_basis, core_density_potential);
        } else {
            core_density_potential = rcp( new Epetra_Vector( *this -> basis -> get_map(), true ) );
            Radial_Grid::Paw::Interpolate_from_radial_grid(0, 0, core_Hartree_r, grid, this -> position, this -> basis, core_density_potential);
        }
        core_potential.push_back(core_density_potential);
        //core_potential[s] -> Scale(-1.0);// Electrons have minus charge. Already considered.
    }
    if( this -> spin_size == 2 ){
        for(int s = 0; s < this -> spin_size; ++s){
            core_potential[s] -> Scale( 0.5 );
        }
    }
    return core_potential;
}

RCP<Epetra_Vector> Paw_Atom::get_initial_hartree_potential( bool is_fine/* = false */ ){
    RCP<Time_Measure> timer = rcp(new Time_Measure());
    RCP<const Basis> obasis;
    if( is_fine ){
        obasis = this -> fine_basis;
    } else {
        obasis = this -> basis;
    }
    RCP<Epetra_Vector> retval = rcp( new Epetra_Vector( *obasis -> get_map() ) );

    timer -> start("PAW_A::initial hartree: get comp coefficient");
    // Compensation charge contribution
    Array< SerialDenseMatrix<int,double> > sD_matrix = this -> get_one_center_initial_density_matrix();
    vector< vector<double> > coeffs = this -> get_compensation_charge_expansion_coeff(sD_matrix);
    timer -> end("PAW_A::initial hartree: get comp coefficient");

    int lmax = this -> lmax;
    Array< RCP<Epetra_MultiVector> > fine_comp_potential;
    if(is_fine){
        timer -> start("PAW_A::initial hartree: retrieve fine comp potential");
        fine_comp_potential = this -> retrieve_fine_compensation_potential();
        timer -> end("PAW_A::initial hartree: retrieve fine comp potential");
    }

    timer -> start("PAW_A::initial hartree: add comp potential");
    for(int l = 0; l <= lmax; ++l){
        for(int m = -l; m <= l; ++m){
            if( is_fine ){
                retval -> Update(coeffs[l][l+m], *fine_comp_potential[l] -> operator()(l+m), 1.0);
            } else {
                retval -> Update(coeffs[l][l+m], *this -> coarse_compensation_potential[l] -> operator()(l+m), 1.0);
            }
        }
    }
    timer -> end("PAW_A::initial hartree: add comp potential");
    fine_comp_potential.clear();

    // Core density contribution
    timer -> start("PAW_A::initial hartree: add core potential");
    Array< RCP<Epetra_Vector> > core_pot = this -> get_smooth_core_potential(is_fine);
    retval -> Update( core_pot.size(), *core_pot[0], 1.0 );
    timer -> end("PAW_A::initial hartree: add core potential");

    // Valence density contribution. It works.
    timer -> start("PAW_A::initial hartree: add valence potential");
    int state_no = this -> paw_species -> get_partial_wave_types().size();
    vector<double> grid = this -> paw_species -> get_grid();
    vector<double> dgrid = this -> paw_species -> get_grid_derivative();
    for(int i = 0; i < state_no; ++i ){
        int l1 = this -> paw_species -> get_partial_wave_state(i).l;
        for(int m1 = -l1; m1 <= l1; ++m1 ){
            vector<double> pw1 = this -> paw_species -> get_smooth_partial_wave_r(i);
            for(int j = 0; j < state_no; ++j ){
                int l2 = this -> paw_species -> get_partial_wave_state(j).l;
                for(int m2 = -l2; m2 <= l2; ++m2 ){
                    vector<double> pw2 = this -> paw_species -> get_smooth_partial_wave_r(i);
                    int ind1 = this -> paw_species -> get_integrated_index(i, m1);
                    int ind2 = this -> paw_species -> get_integrated_index(j, m2);
                    vector<double> pwpw(grid.size());
                    for(int r = 0; r < grid.size(); ++r){
                        pwpw[r] =  pw1[r]*pw2[r];
                    }
                    double D_mat_coeff = 0.0;
                    for(int s = 0; s < sD_matrix.size(); ++s){
                        D_mat_coeff += sD_matrix[s](ind1, ind2);
                    }
                    if( D_mat_coeff > 0 ){
                        for(int l = abs(l1-l2); l <= min(lmax, l1+l2); ++l){
                            for(int m = -l; m <= l; ++m){
                                double YYY = Spherical_Harmonics::real_YYY_integrate(l, l1, l2, m, m1, m2);
                                if( abs(YYY) > 1.0E-30 ){
                                    vector<double> vH_pwpw = Radial_Grid::Paw::calculate_Hartree_potential_r(pwpw, l, grid, dgrid);
                                    RCP<Epetra_Vector> pw_val = rcp( new Epetra_Vector( *obasis -> get_map() ) );
                                    Radial_Grid::Paw::Interpolate_from_radial_grid(l,m,vH_pwpw,grid,this->position,obasis,pw_val);

                                    retval -> Update( YYY*D_mat_coeff, *pw_val, 1.0 );
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    timer -> end("PAW_A::initial hartree: add valence potential");
    Verbose::set_numformat(Verbose::Time);
    timer -> print(Verbose::single(Verbose::Normal));
    return retval;
}

SerialDenseMatrix<int,double> Paw_Atom::get_overlap_matrix(){
    int stateno = this -> index_to_l.size();

    vector< vector<double> > delta00_matrix = this -> paw_species -> get_delta_matrix()[0][0];
    double* delta00_array = new double[stateno*stateno];
    for(int index1 = 0; index1 < stateno; ++index1){
        for(int index2 = 0; index2 < stateno; ++index2){
            delta00_array[index1+index2*stateno] = delta00_matrix[index1][index2];
        }
    }

    SerialDenseMatrix<int,double> overlap_matrix(Teuchos::Copy, delta00_array, stateno, stateno, stateno);

    overlap_matrix.scale( sqrt(4*M_PI) );
    delete[] delta00_array;

    return overlap_matrix;
}

void Paw_Atom::get_atomcenter_orbitals( bool is_ae, RCP<Epetra_MultiVector> &pw_vectors, bool is_value ){

    vector<double> grid = this -> paw_species -> get_grid();
    pw_vectors = rcp( new Epetra_MultiVector( *this -> basis -> get_map(), this->index_to_l.size() ) );
    for(int i = 0; i < this -> paw_species -> get_partial_wave_types().size(); ++i){
        vector<double> pw;
        if( is_ae ){
            pw = this -> paw_species -> get_all_electron_partial_wave_r(i);
        } else {
            pw = this -> paw_species -> get_smooth_partial_wave_r(i);
        }

        int l = this -> paw_species -> get_partial_wave_state(i).l;
        double rc = this -> paw_species -> get_partial_wave_state(i).cutoff_radius;
        for(int m = -l; m <= l; ++m){
            int index = this -> paw_species -> get_integrated_index(i, m);
            RCP<Epetra_Vector> tmp = rcp( new Epetra_Vector( pw_vectors->Map() ) );
            if( is_value ){
                Radial_Grid::Paw::Interpolate_from_radial_grid( l, m, pw, grid, this -> position, this -> basis, tmp, rc );
            } else {
                Radial_Grid::Paw::get_basis_coeff_from_radial_grid( l, m, pw, grid, this -> position, this -> basis, tmp, rc );
            }
            pw_vectors -> operator()(index) -> Update(1.0, *tmp, 0.0);
        }
    }

    return;
}

Array< RCP<Epetra_MultiVector> > Paw_Atom::get_atomcenter_density_grad_vector(
        bool is_ae,
        Array< SerialDenseMatrix<int,double> > &sD_matrix,
        RCP<const Basis> obasis/* = Teuchos::null*/
){
    // Returns atomcenter ae density grad
    if( obasis == Teuchos::null ){
        obasis = this -> basis;
    }

    vector<string> pw_type = this-> paw_species -> get_partial_wave_types();
    vector<double> density;
    if( is_ae ){
        density = this -> paw_species -> get_all_electron_core_density_r();
    } else {
        density = this -> paw_species -> get_smooth_core_density_r();
    }
    vector<double> grid = this -> paw_species -> get_grid();

    RCP<Epetra_MultiVector> pw_vectors = rcp( new Epetra_MultiVector( *obasis -> get_map(), this->index_to_l.size() ) );
    for(int i = 0; i < pw_type.size(); ++i){
        vector<double> pw;
        if( is_ae ){
            pw = this -> paw_species -> get_all_electron_partial_wave_r(i);
        } else {
            pw = this -> paw_species -> get_smooth_partial_wave_r(i);
        }
        int l = this -> paw_species -> get_partial_wave_state(i).l;
        double rc = this -> paw_species -> get_partial_wave_state(i).cutoff_radius;
        for(int m = -l; m <= l; ++m){
            int index = this -> paw_species -> get_integrated_index(i, m);
            RCP<Epetra_Vector> tmp = rcp( new Epetra_Vector( *obasis -> get_map() ) );
            Radial_Grid::Paw::Interpolate_from_radial_grid( l, m, pw, grid, this -> position, obasis, tmp, rc );
            pw_vectors -> operator()(index) -> Update(1.0, *tmp, 0.0);
        }
    }

    Array< RCP< Epetra_MultiVector > > pw_grad;
    for(int i = 0; i < index_to_l.size(); ++i){
        pw_grad.push_back( rcp( new Epetra_MultiVector( *obasis -> get_map(), 3 ) ) );
    }
    for(int i = 0; i < pw_type.size(); ++i){
        vector<double> pw;
        if( is_ae ){
            pw = this -> paw_species -> get_all_electron_partial_wave_r(i);
        } else {
            pw = this -> paw_species -> get_smooth_partial_wave_r(i);
        }
        int l = this -> paw_species -> get_partial_wave_state(i).l;
        double rc = this -> paw_species -> get_partial_wave_state(i).cutoff_radius;
        for(int m = -l; m <= l; ++m){
            int index = this -> paw_species -> get_integrated_index(i, m);
            Radial_Grid::Paw::get_gradient_from_radial_grid( l, m, pw, grid, this -> position, obasis, pw_grad[index], rc );
        }
    }

    Array< RCP< Epetra_MultiVector > > density_grad;
    RCP<Epetra_MultiVector> tmp_core_grad = rcp( new Epetra_MultiVector( *obasis -> get_map(), 3, false ) );
    for(int s = 0; s < sD_matrix.size(); ++s){
        density_grad.push_back( rcp( new Epetra_MultiVector( *obasis -> get_map(), 3, false ) ) );
    }
    Radial_Grid::Paw::get_gradient_from_radial_grid( 0, 0, density, grid, this -> position, obasis, tmp_core_grad );

    for( int s = 0; s < sD_matrix.size(); ++s){
        density_grad[s] -> operator()(s) -> Update( 1.0/(sD_matrix.size()), *tmp_core_grad, 0.0 );
    }

    for(int i = 0; i < pw_type.size(); ++i){
        for(int j = 0; j < pw_type.size(); ++j){
            int l1 = this -> paw_species -> get_partial_wave_state(i).l;
            int l2 = this -> paw_species -> get_partial_wave_state(j).l;
            for(int m1 = -l1; m1 <= l1; ++m1){
                for(int m2 = -l2; m2 <= l2; ++m2){
                    int index1 = this -> paw_species -> get_integrated_index(i, m1);
                    int index2 = this -> paw_species -> get_integrated_index(j, m2);

                    for(int k = 0; k < 3; ++k){
                        RCP<Epetra_Vector> tmp = rcp( new Epetra_Vector( *obasis -> get_map(), false ) );
                        tmp -> Multiply( 1.0, *pw_grad[index1]->operator()(k), *pw_vectors->operator()(index2), 0.0 );
                        for(int s = 0; s < sD_matrix.size(); ++s){
                            density_grad[s] -> operator()(k) -> Update( sD_matrix[s][index1][index2], *tmp, 1.0 );
                        }
                        tmp -> Multiply( 1.0, *pw_vectors->operator()(index1), *pw_grad[index2]->operator()(k), 0.0 );
                        for(int s = 0; s < sD_matrix.size(); ++s){
                            density_grad[s] -> operator()(k) -> Update( sD_matrix[s][index1][index2], *tmp, 1.0 );
                        }
                    }
                }// m2
            }// m1
        }// j
    }// i

    return density_grad;
}

vector< vector<double> > Paw_Atom::get_compensation_charge_expansion_coeff(
    Array< SerialDenseMatrix<int,double> > &sD_matrix
){
    vector<string> pw_type = this-> paw_species -> get_partial_wave_types();
    vector<double> grid = this -> paw_species -> get_grid();
    vector< vector< vector< vector<double> > > > delta_mat = this -> paw_species -> get_delta_matrix();

    vector<int> pw_l_list = this -> index_to_l;
    int lmax = this -> lmax;

    vector< vector<double> > coeffs;
    coeffs.resize(lmax+1);
    for(int L = 0; L <= lmax; ++L){
        coeffs[L].resize(2*L+1);
    }

    for(int l = 0; l <= lmax; ++l){
        for(int m = -l; m <= l; ++m){
            double coeff = 0.0;
            if(l == 0){
                coeff += this -> paw_species -> get_delta_scalar();
            }

            for(int i = 0; i < pw_type.size(); ++i){
                for(int j = 0; j < pw_type.size(); ++j){
                    int l1 = this -> paw_species -> get_partial_wave_state(i).l;
                    int l2 = this -> paw_species -> get_partial_wave_state(j).l;
                    for(int m1 = -l1; m1 <= l1; ++m1){
                        for(int m2 = -l2; m2 <= l2; ++m2){
                            int index1 = this -> paw_species -> get_integrated_index(i,m1);
                            int index2 = this -> paw_species -> get_integrated_index(j,m2);
                            //double tmp = delta_mat[l][l+m][index1][index2];
                            for(int s = 0; s < sD_matrix.size(); ++s){
                                coeff += delta_mat[l][l+m][index1][index2] * sD_matrix[s][index1][index2];
                            }
                        }
                    }
                }
            }
            coeffs[l][l+m] = coeff;
        }
    }
    return coeffs;
}

RCP<Epetra_Vector> Paw_Atom::get_compensation_charge_Hartree_potential(
    Array< SerialDenseMatrix<int,double> > &sD_matrix,
    bool is_fine/* = false */
){
    RCP<Time_Measure> timer = rcp(new Time_Measure());
    // Returns Hartree potential from compensation charge
    int lmax = this -> lmax;
    timer -> start("PAW_A::hartree: get comp coefficeint");
    vector< vector<double> > coeffs = this -> get_compensation_charge_expansion_coeff( sD_matrix );
    timer -> end("PAW_A::hartree: get comp coefficeint");
    RCP<Epetra_Vector> potential;
    if( is_fine ){
        potential = rcp( new Epetra_Vector( *this -> fine_basis -> get_map(), true ) );
    } else {
        potential = rcp( new Epetra_Vector( *this -> basis -> get_map(), true ) );
    }

    Array< RCP<Epetra_MultiVector> > fine_comp_potential;
    if(is_fine){
        timer -> start("PAW_A::hartree: get fine comp potential");
        fine_comp_potential = this -> retrieve_fine_compensation_potential();
        timer -> end("PAW_A::hartree: get fine comp potential");
    }
    timer -> start("PAW_A::hartree: add comp potential");
    for(int l = 0; l <= lmax; ++l){
        for(int m = -l; m <= l; ++m){
            if( is_fine ){
                potential -> Update(coeffs[l][l+m], *fine_comp_potential[l] -> operator()(l+m), 1.0);
            } else {
                potential -> Update( coeffs[l][l+m], *this -> coarse_compensation_potential[l] -> operator()(l+m), 1.0 );
            }
        }
    }

    timer -> end("PAW_A::hartree: add comp potential");
    Verbose::set_numformat(Verbose::Time);
    timer -> print(Verbose::single(Verbose::Normal));
    return potential;
}

RCP<Epetra_MultiVector> Paw_Atom::calculate_fine_projector_coeffs(
        std::array<double,3> center,
        RCP<const Basis> obasis/* = Teuchos::null*/
){
    if( obasis == Teuchos::null ){
        obasis = this -> basis;
    }
    clock_t st, et;
    Parallel_Manager::info().all_barrier();
    st = clock();

    int state_no = this -> paw_species -> get_partial_wave_types().size();
    int state_no_m_included = this -> index_to_l.size();
    /*
    int count_fine = 0;

    // Interpolate proj
    double rc_out = rc_in * this -> fine_beta;
    vector<double> sampling_coefficients = Double_Grid::get_sampling_coefficients(this -> fine_filter_type, this -> fine_proj_degree);
    */
    double rc_in = this -> paw_species -> get_projector_cutoff();
    RCP<Epetra_MultiVector> projectors = rcp( new Epetra_MultiVector( *obasis -> get_map(), state_no_m_included, true ) );
    for(int i = 0; i < state_no; ++i ){
        int l1 = this -> paw_species -> get_partial_wave_state(i).l;
        vector<double> proj1 = this -> paw_species -> get_projector_function_r(i);
        vector<double> grid1 = this -> paw_species -> get_grid();

        for(int m1 = -l1; m1 <= l1; ++m1){
            int index = this -> paw_species -> get_integrated_index(i, m1);
            vector<double> vals;
            vector<int> inds;
            Radial_Grid::Paw::get_basis_coeff_from_radial_grid( l1, m1, proj1, grid1, center, this->proj_basis, vals, inds, rc_in );
            Verbose::single(Verbose::Normal) << "PAW supersampling: projector index = " << index << std::endl;
            //Verbose::all() << "Finegrid size for processor " << Parallel_Manager::info().get_group_rank() << ": "
            //               << "" << "/" << inds.size() << std::endl;


            RCP<Epetra_Vector> tmp = Double_Grid::sample(vals, inds, proj_basis, obasis,
                                this -> fine_filter_type, this -> fine_proj_degree, this -> fine_beta,
                                vector<double>(center.data(), center.data()+3), rc_in);
            projectors -> operator()(index) -> Update(1.0, *tmp, 0.0);
        }// for m1
    }// for l1
    Parallel_Manager::info().all_barrier();
    et = clock();
    Verbose::single(Verbose::Simple) << "PAW FineGrid projector generation: \t" << ( (double)(et-st) )/CLOCKS_PER_SEC << "s" << std::endl;

    return projectors;
}

Array< RCP<Epetra_MultiVector> > Paw_Atom::calculate_fine_projector_grads(
        std::array<double,3> center,
        RCP<const Basis> obasis /* = Teuchos::null*/
){
    if( obasis == Teuchos::null ){
        obasis = this -> basis;
    }
    clock_t st, et;
    Parallel_Manager::info().all_barrier();
    st = clock();

    int dim = obasis -> get_original_size();
    int state_no = this -> paw_species -> get_partial_wave_types().size();
    int state_no_m_included = this -> index_to_l.size();
    double rc_in = this -> paw_species -> get_projector_cutoff();
    Array< RCP<Epetra_MultiVector> > projector_grads;
    for(int d = 0; d < 3; ++d){
        projector_grads.append( rcp( new Epetra_MultiVector( *obasis -> get_map(), state_no_m_included, true ) ) );
    }
    for(int i = 0; i < state_no; ++i ){
        int l1 = this -> paw_species -> get_partial_wave_state(i).l;
        vector<double> proj1 = this -> paw_species -> get_projector_function_r(i);
        vector<double> grid1 = this -> paw_species -> get_grid();

        for(int m1 = -l1; m1 <= l1; ++m1){
            int index = this -> paw_species -> get_integrated_index(i, m1);
            vector< vector<double> > vals;
            vector< vector<int> > inds;
            RCP<Epetra_MultiVector> tmp_proj_grad = rcp(new Epetra_MultiVector(*this->proj_basis->get_map(), 3, true));
            RCP<Epetra_MultiVector> tmp_proj_grad2 = rcp(new Epetra_MultiVector(*this->proj_basis->get_map(), 3, true));
            Radial_Grid::Paw::get_gradient_from_radial_grid( l1, m1, proj1, grid1, this -> position, this -> proj_basis, tmp_proj_grad );
            Value_Coef::Value_Coef(this -> proj_basis, tmp_proj_grad, true, false, tmp_proj_grad2);

            Parallel_Util::group_extract_nonzero_from_multivector(tmp_proj_grad2, vals, inds);
            Verbose::single(Verbose::Normal) << "PAW supersampling: projector gradient index = " << index << std::endl;
            //Verbose::all() << "Finegrid size for processor " << Parallel_Manager::info().get_group_rank() << ": "
            //               << "" << "/" << inds.size() << std::endl;

            for(int d = 0; d < 3; ++d){
                RCP<Epetra_Vector> tmp = Double_Grid::sample(vals[d], inds[d], proj_basis, obasis,
                        this -> fine_filter_type, this -> fine_proj_degree, this -> fine_beta,
                        vector<double>(center.data(), center.data()+3), rc_in);
                projector_grads[d] -> operator()(index) -> Update(1.0, *tmp, 0.0);
            }
        }// for m1
    }// for l1
    Parallel_Manager::info().all_barrier();
    et = clock();
    Verbose::single(Verbose::Simple) << "PAW FineGrid projector gradient generation: \t" << ( (double)(et-st) )/CLOCKS_PER_SEC << "s" << std::endl;

    return projector_grads;
}

double Paw_Atom::get_total_compensation_charge(
    Array< SerialDenseMatrix<int,double> > &sD_matrix
){
    vector< vector<double> > coeffs = this -> get_compensation_charge_expansion_coeff( sD_matrix );

    return coeffs[0][0] * sqrt(4*M_PI);
}

Array< RCP<Epetra_Vector> > Paw_Atom::get_core_density_vector(
    bool is_ae,
    Teuchos::RCP<const Basis> obasis
){
    // Returns core ae/ps core density
    vector<string> pw_type = this-> paw_species -> get_partial_wave_types();

    vector<double> density;
    if( is_ae ){
        density = this -> paw_species -> get_all_electron_core_density_r();
    } else {
        density = this -> paw_species -> get_smooth_core_density_r();
    }
    vector<double> grid = this -> paw_species -> get_grid();
    double rc = -1.0;
    for(int i = 0; i < this -> paw_species -> get_partial_wave_types().size(); ++i){
        double tmprc = this -> paw_species -> get_partial_wave_state(i).cutoff_radius;
        if( tmprc > rc ){
            rc = tmprc;
        }
    }
    rc *= 2.0;

    Array< RCP<Epetra_Vector> > density_core;
    for(int s = 0; s < this -> spin_size; ++s){
        density_core.push_back( rcp( new Epetra_Vector( *obasis -> get_map(), false ) ) );
    }

    RCP<Epetra_Vector> tmp_core = rcp( new Epetra_Vector( *obasis -> get_map() ) );
    Radial_Grid::Paw::Interpolate_from_radial_grid( 0, 0, density, grid, this->position, obasis, tmp_core, rc );

    if( !is_ae ){
        double norm1;
        tmp_core -> Norm1( &norm1 );
        double scaling = obasis -> get_scaling()[0] * obasis -> get_scaling()[1] * obasis -> get_scaling()[2];
        norm1 *= scaling;
        if( norm1 > 1.0E-15 ){
            tmp_core -> Scale( this -> paw_species -> get_smooth_core_density_integral()/norm1 );
        }
    }

    for( int s = 0; s < this -> spin_size; ++s){
        density_core[s] -> Update( 1.0/this -> spin_size, *tmp_core, 0.0 );
    }

    return density_core;
}

Array< RCP<Epetra_MultiVector> > Paw_Atom::get_core_density_grad_vector(
    bool is_ae,
    Teuchos::RCP<const Basis> obasis
){
    // Returns core ae/ps density grad
    vector<string> pw_type = this-> paw_species -> get_partial_wave_types();
    vector<double> density_core;
    if( is_ae ){
        density_core = this -> paw_species -> get_all_electron_core_density_r();
    } else {
        density_core = this -> paw_species -> get_smooth_core_density_r();
    }
    vector<double> grid = this -> paw_species -> get_grid();

    double rc = -1.0;
    for(int i = 0; i < this -> paw_species -> get_partial_wave_types().size(); ++i){
        double tmprc = this -> paw_species -> get_partial_wave_state(i).cutoff_radius;
        if( tmprc > rc ){
            rc = tmprc;
        }
    }
    rc *= 2.0;

    Array< RCP< Epetra_MultiVector > > density_grad;
    RCP< Epetra_MultiVector> tmp_core_grad = rcp( new Epetra_MultiVector( *obasis -> get_map(), 3 ) );
    for(int s = 0; s < this -> spin_size; ++s){
        density_grad.push_back( rcp( new Epetra_MultiVector( *obasis -> get_map(), 3, false ) ) );
    }
    Radial_Grid::Paw::get_gradient_from_radial_grid( 0, 0, density_core, grid, this -> position, obasis, tmp_core_grad, rc );

    if( !is_ae ){
        RCP<Epetra_Vector> rho_core = rcp( new Epetra_Vector( *obasis -> get_map() ) );
        Radial_Grid::Paw::Interpolate_from_radial_grid( 0, 0, density_core, grid, this->position, obasis, rho_core, rc );
        double norm1;
        rho_core -> Norm1( &norm1 );
        double scaling = obasis -> get_scaling()[0] * obasis -> get_scaling()[1] * obasis -> get_scaling()[2];
        norm1 *= scaling;
        if( norm1 > 1.0E-15 ){
            tmp_core_grad -> Scale( this -> paw_species -> get_smooth_core_density_integral()/norm1 );
        }
    }

    for( int s = 0; s < this -> spin_size; ++s){
        density_grad[s] ->  Update( 1.0/this -> spin_size, *tmp_core_grad, 0.0 );
    }

    return density_grad;
}

/*
vector<double> Paw_Atom::get_force(
    Array< RCP<const Occupation> > occupations,
    Array< RCP<const Epetra_MultiVector> > orbitals,
    Array< RCP<const Epetra_Vector> > eff_potential,
    RCP<const Epetra_Vector> hartree_potential,
    vector< vector<double> > eigenvalues,
       Array< SerialDenseMatrix<int,double> > &sD_matrix
){
    double scaling = this -> basis -> get_scaling()[0]*this -> basis -> get_scaling()[1]*this -> basis -> get_scaling()[2];
    vector<double> retval(3);
    Array< RCP<Epetra_MultiVector> > core_ps_density_grad = this -> get_core_density_grad_vector(false);
    RCP<Epetra_MultiVector> comp_charge_grad = this -> get_compensation_charge_grad(sD_matrix);

    // Note: d/dR = -d/dr
    for(int i = 0; i < 3; ++i){
        for(int s = 0; s < occupations.size(); ++s){
            double tmp;
            // Core density contribution.
            eff_potential[s] -> Dot( *core_ps_density_grad[s] -> operator()(i), &tmp );
            retval[i] = tmp;
        }
        double tmp;
        // Core compensation charge contribution.
        hartree_potential -> Dot( *comp_charge_grad -> operator()(i), &tmp );
        retval[i] = tmp;
        retval[i] *= scaling;
    }

    // Smooth orbital contribution.
    vector< vector< vector<double> > > hamiltonian_correction = this -> get_Hamiltonian_correction_matrix(hartree_potential, sD_matrix);
    Teuchos::SerialDenseMatrix<int,double> overlap_mat = this -> get_overlap_matrix();
    vector< vector< vector<double> > > proj_dot_orb = this -> projector_dot_orbitals(occupations, orbitals);
    Array< RCP<Epetra_MultiVector> > projector_grad = this -> projector_grads;
    vector< vector< vector< vector<double> > > > proj_grad_dot_orb(3);
    for(int d = 0; d < 3; ++d){
        for(int alpha = 0; alpha < spin_size; ++alpha){
            proj_grad_dot_orb[d][alpha].resize(orbitals[alpha] -> NumVectors());
            for(int n = 0; n < orbitals[alpha] -> NumVectors(); ++n){
                proj_grad_dot_orb[d][alpha][n].resize(hamiltonian_correction[alpha].size(), 0.0);
            }
        }
    }
    for(int d = 0; d <3; ++d){
        for(int alpha = 0; alpha < spin_size; ++alpha ){
            for(int n = 0; n < orbitals[alpha] -> NumVectors(); ++n ){
                for(int i = 0; i < hamiltonian_correction[alpha].size(); ++i){
                    projector_grad[d] -> operator()(i) -> Dot( *(orbitals[alpha]->operator()(n)), &proj_grad_dot_orb[d][alpha][n][i] );
                }
            }
        }
    }
    for(int s = 0; s < occupations.size(); ++s){
        for(int n = 0; n < occupations[s] -> get_size(); ++n){
            if( occupations[s] -> operator[](n) > 1.0E-6 ){
                for(int i1 = 0; i1 < hamiltonian_correction[s].size(); ++i1){
                    for(int i2 = 0; i2 < hamiltonian_correction[s].size(); ++i2){
                        double matrix_part = hamiltonian_correction[s][i1][i2]-eigenvalues[s][n]*overlap_mat(i1,i2);
                        for(int d = 0; d < 3; ++d){
                            retval[d] = matrix_part * (proj_dot_orb[s][n][i1]*proj_grad_dot_orb[d][s][n][i2] + proj_grad_dot_orb[d][s][n][i1]*proj_dot_orb[s][n][i2]);
                        }
                    }
                }
            }
        }
    }
    return retval;
}
*/

void Paw_Atom::get_projector_coeffs_and_inds(
    vector< vector<double> > & proj_coeffs,
    vector< vector<int> > & proj_inds
){
    proj_coeffs = vector< vector<double> >( this -> proj_coeffs );
    proj_inds = vector< vector<int> >( this -> proj_inds );
}

Array< RCP<Epetra_MultiVector> > Paw_Atom::get_projector_gradients(){
    int state_no_ = this -> index_to_l.size();
    Array< RCP<Epetra_MultiVector> > retval(3);
    for(int d = 0; d < 3; ++d){
        retval[d] = rcp(new Epetra_MultiVector(*this -> basis -> get_map(), state_no_));
        for(int i = 0; i < state_no_; ++i){
            retval[d] -> operator()(i) -> ReplaceGlobalValues(this -> proj_grad_inds[d][i].size(), this -> proj_grad_coeffs[d][i].data(), this -> proj_grad_inds[d][i].data());
        }
    }
    return retval;
}

/*
double Paw_Atom::integrate_comp_and_hartree(
    RCP<Epetra_Vector> hartree_vector,
    Array< SerialDenseMatrix<int,double> > &sD_matrix
){
    int lmax = this -> lmax;
    vector<double> grid_r = this -> paw_species -> get_grid();
    vector<double> dgrid_r = this -> paw_species -> get_grid_derivative();

    double retval = 0.0;
    vector< vector<double> > coeffs = this -> get_compensation_charge_expansion_coeff( sD_matrix );
    for(int L = 0; L <= lmax; ++L){
        vector<double> g_L = this -> paw_species -> get_compensation_charge_r( L );
        for(int M = -L; M <= L; ++M){
            vector<double> vH = Spherical_Harmonics::Expansion::expand( L, M, this->fine_basis, this->fine_grid_setting,
                                           hartree_vector, 0, grid_r, this -> position );
            retval += coeffs[L][L+M] * Radial_Grid::Paw::radial_integrate( vH, g_L, grid_r, dgrid_r );
        }
    }
    return retval;
}
*/

void Paw_Atom::print_orbital_occupancy(
        Array< RCP<const Occupation> > occupations,
        Array< RCP<const Epetra_MultiVector> > orbitals
){
    int state_no_m_included = this -> index_to_l.size();
    vector< vector< vector<double> > > integrated;
    integrated.resize( spin_size );
    for(int alpha = 0; alpha < spin_size; ++alpha){
        integrated[alpha].resize(orbitals[alpha] -> NumVectors());
        for(int n = 0; n < orbitals[alpha] -> NumVectors(); ++n){
            integrated[alpha][n].resize(state_no_m_included, 0.0);
        }
    }
    for(int alpha = 0; alpha < spin_size; ++alpha ){
        for(int n = 0; n < orbitals[alpha] -> NumVectors(); ++n ){
            vector<double> orb_proj_tmp(state_no_m_included);
            for(int i = 0; i < state_no_m_included; ++i){
                //this -> projector_coeffs -> operator()(i) -> Dot( *(orbitals[alpha]->operator()(n)), &integrated[alpha][n][i] );
                for(int ind = 0; ind < this -> proj_inds[i].size(); ++ind){
                    if( orbitals[alpha] -> Map().MyGID(this -> proj_inds[i][ind]) ){
                        orb_proj_tmp[i] += this -> proj_coeffs[i][ind] * orbitals[alpha] -> operator[](n)[orbitals[alpha]->Map().LID(this -> proj_inds[i][ind])];
                    }
                }
            }
            Parallel_Util::all_sum(&orb_proj_tmp[0], &integrated[alpha][n][0], state_no_m_included);
        }
    }
    std::ios oldstate(nullptr);
    oldstate.copyfmt(Verbose::single());
    //Verbose::single() << "PAW atomic partial wave character of the orbitals." << std::endl;
    for(int alpha = 0; alpha < spin_size; ++alpha ){
        if(this -> occupancy_output == 2){
            Verbose::single(Verbose::Simple) << "Spin   " << std::setw(4) << alpha << "  ";
            for(int i = 0; i < this -> paw_species -> get_partial_wave_types().size(); ++i){
                string state_name = this -> paw_species -> get_partial_wave_state(i).state;
                int l = this -> paw_species -> get_partial_wave_state(i).l;
                for(int m = -l; m <= l; ++m){
                    Verbose::single(Verbose::Simple) << std::setw(8) << state_name << "  ";
                }
            }
            Verbose::single(Verbose::Simple) << std::endl;
            for(int n = 0; n < orbitals[alpha] -> NumVectors(); ++n){
                Verbose::single(Verbose::Simple) << "Orbital " << std::setw(3) << n+1 << "  ";
                for(int i = 0; i < state_no_m_included; ++i){
                    double tmp = (integrated[alpha][n][i])*(integrated[alpha][n][i]) * occupations[alpha] -> operator[](n);
                    Verbose::single(Verbose::Simple) << std::fixed << std::setw(8) << std::setprecision(5) << tmp << "  ";
                }
                Verbose::single(Verbose::Simple) << std::endl;
            }
        } // if(occupancy_matrix)
        if(this -> occupancy_output > 0){
            Verbose::single(Verbose::Simple) << "Spin   " << std::setw(4) << alpha << "  ";
            for(int i = 0; i < this -> paw_species -> get_partial_wave_types().size(); ++i){
                string state_name = this -> paw_species -> get_partial_wave_state(i).state;
                int l = this -> paw_species -> get_partial_wave_state(i).l;
                for(int m = -l; m <= l; ++m){
                    Verbose::single(Verbose::Simple) << std::setw(8) << state_name << "  ";
                }
            }
            Verbose::single(Verbose::Simple) << std::endl;
            Verbose::single(Verbose::Simple) << "Total      " << "  ";
            for(int i = 0; i < state_no_m_included; ++i){
                double tmp = 0.0;
                for(int n = 0; n < orbitals[alpha] -> NumVectors(); ++n){
                    tmp += (integrated[alpha][n][i])*(integrated[alpha][n][i]) * occupations[alpha] -> operator[](n);
                }
                Verbose::single(Verbose::Simple) << std::fixed << std::setw(8) << std::setprecision(5) << tmp << "  ";
            }
            Verbose::single(Verbose::Simple) << std::endl;
        }
    }
    Verbose::single().copyfmt(oldstate);
}

Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > Paw_Atom::interpolate_compensation_charge(
        bool is_fine,
        int lmax
){
    RCP<const Basis> obasis = (is_fine)? this -> fine_basis: this -> basis;
    Teuchos::RCP<Time_Measure> comp_timer = Teuchos::rcp(new Time_Measure());
    comp_timer -> start("Get comp charge");
    int type = this -> paw_species -> get_read_paw() -> get_shape_function_type();

    Array< RCP<Epetra_MultiVector> > compensation_charge;
    Array< RCP<Epetra_MultiVector> > rl;
    vector<double> grid = this -> paw_species -> get_grid();
    for(int l = 0; l <= lmax; ++l){
        compensation_charge.push_back( rcp( new Epetra_MultiVector( *obasis -> get_map(), 2*l+1 ) ) );
        // Should be rl*Ylm instead of rl or r2.
        if(type == 2 or type == 3){
            rl.push_back( rcp(new Epetra_MultiVector(*obasis -> get_map(), 2*l+1)) );
        }
    }
    double rc = this -> paw_species -> get_read_paw() -> get_shape_function_rc();
    double r_back = this -> paw_species -> get_grid().back();
    //double r_back = rc*3.5;
    if(type == 1 or type == 2 or type == 3){
    //if(false){
        std::array<double,3> scaling = obasis -> get_scaling();
        //int dim = obasis -> get_original_size();
        int NumMyElements = obasis -> get_map() -> NumMyElements();

#ifdef ACE_HAVE_OMP
//#pragma omp parallel for // Causes Segfaults somehow
#endif
        for(int i = 0; i < NumMyElements; ++i){
            //double x, y, z, r, x_p, y_p, z_p;
            double r, x, y, z;
            //(PBC)
            obasis->find_nearest_displacement(obasis->get_map()->GID(i), this->position[0], this->position[1], this->position[2], x, y, z, r);
            /*
            obasis -> get_position(obasis -> get_map() -> GID(i), x_p, y_p, z_p);
            x = x_p - this -> position[0];
            y = y_p - this -> position[1];
            z = z_p - this -> position[2];
            r = sqrt(x*x+y*y+z*z);
            */
            if(r < r_back){
                for(int l = 0; l <= lmax; ++l){
                    double val = this -> paw_species -> get_compensation_charge_r(r, l);
                    for(int m = -l; m <= l; ++m){
                        double y_lm = Ylm(l, m, x, y, z);
                        double val2 = val * y_lm;
                        if( abs(val) > RGD2GD_CUTOFF ){
                            compensation_charge[l] -> operator()(l+m) -> ReplaceMyValue(i, 0, val2);
                        }
                        if(rl.size() > 0){
                            rl[l] -> operator()(l+m) -> ReplaceMyValue(i, 0, pow(r,l)*y_lm);
                        }
                    }
                }
            }
        }
        if(rl.size() > 0){
            for(int l = 0; l <= lmax; ++l){
                for(int m = -l; m <= l; ++m){
                    double norm2;
                    rl[l] -> operator()(l+m) -> Dot(*compensation_charge[l] -> operator()(l+m), &norm2);
                    norm2 *= scaling[0] * scaling[1] * scaling[2];
                    compensation_charge[l] -> operator()(l+m) -> Scale(1.0/norm2);
                }
            }
        }
    } else {
        for(int l = 0; l <= lmax; ++l){
            for(int m = -l; m <= l; ++m){
                RCP<Epetra_Vector> tmp_comp_charge = rcp( new Epetra_Vector( *obasis -> get_map(), true ) );
                vector<double> comp_charge_r = this -> paw_species -> get_compensation_charge_r( l );
                Radial_Grid::Paw::Interpolate_from_radial_grid( l, m, comp_charge_r, grid, this -> position, obasis, tmp_comp_charge );
                compensation_charge[l] -> operator()(l+m) -> Update( 1.0, *tmp_comp_charge, 0.0 );
            }
        }
    }
    Verbose::set_numformat(Verbose::Time);
    comp_timer -> end("Get comp charge");
    comp_timer -> print(Verbose::single(Verbose::Detail));
    return compensation_charge;
}

Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > Paw_Atom::interpolate_compensation_potential(
        bool is_fine,
        int lmax
){
    Teuchos::RCP<const Basis> obasis = (is_fine)? this -> fine_basis: this -> basis;
    Array< RCP<Epetra_MultiVector> > comp_potential;
    vector<double> grid = this -> paw_species -> get_grid();
    for(int l = 0; l <= lmax; ++l){
        comp_potential.push_back( rcp( new Epetra_MultiVector( *obasis -> get_map(), 2*l+1 ) ) );
    }

    Teuchos::RCP<Time_Measure> vH_comp_timer = Teuchos::rcp(new Time_Measure());
    vH_comp_timer -> start("Get vH_comp");
    //*
    //if( this -> paw_species -> get_read_paw() -> get_shape_function_type() == 1){
    if(false){
        double rc = this -> paw_species -> get_read_paw() -> get_shape_function_rc();
        int dim = obasis -> get_original_size();

        for(int i = 0; i < dim; ++i){
            if(obasis -> get_map() -> MyGID(i)){
                //double x, y, z, r, x_p, y_p, z_p;
                double x, y, z, r;
                //(PBC)
                obasis -> find_nearest_displacement(i, this->position[0], this->position[1], this->position[2], x, y, z, r);

                /*
                obasis -> get_position(i, x_p, y_p, z_p);
                x = x_p - this -> position[0];
                y = y_p - this -> position[1];
                z = z_p - this -> position[2];
                r = sqrt(x*x+y*y+z*z);
                */
                for(int l = 0; l <= lmax; ++l){
                    double val = this -> paw_species -> get_compensation_potential_analytic_r(r, l);
                    for(int m = -l; m <= l; ++m){
                        double val2 = val * Ylm(l, m, x, y, z);
                        if( abs(val) > RGD2GD_CUTOFF ){
                            comp_potential[l] -> operator()(l+m) -> ReplaceGlobalValue(i, 0, val2);
                        }
                    }
                }
            }
        }
    } else {
        vH_comp_timer -> start("get radial vH");
        vector< vector<double> > Hartree_potential_r = this -> paw_species -> get_hartree_potential_of_compensation_charge();
        vH_comp_timer -> end("get radial vH");
        long long int tot_nonzeros = 0;
        vH_comp_timer -> start("interpolate vH");
        for(int l = 0; l <= lmax; ++l){
            for(int m = -l; m <= l; ++m){
                RCP<Epetra_Vector> tmp_comp_potential = rcp( new Epetra_Vector( *obasis -> get_map(), true ) );
                Radial_Grid::Paw::Interpolate_from_radial_grid( l, m, Hartree_potential_r[l], grid, this -> position, obasis, tmp_comp_potential );
                comp_potential[l] -> operator()(l+m) -> Update( 1.0, *tmp_comp_potential, 0.0 );
            }
        }
        vH_comp_timer -> end("interpolate vH");
        //Verbose::all() << "vH_comp nonzeros = " << tot_nonzeros << "/" << obasis -> get_map() -> NumMyElements() * lmax*(lmax+1)*(2*lmax+1)/6 << std::endl;
    }
    vH_comp_timer -> end("Get vH_comp");
    Verbose::set_numformat(Verbose::Time);
    Verbose::single(Verbose::Detail) << "Atom ID: " << this -> id << ", fine = " << is_fine << std::endl;
    vH_comp_timer -> print(Verbose::single(Verbose::Detail));
    return comp_potential;
}

std::vector< Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > > Paw_Atom::interpolate_compensation_charge_grad(
        Teuchos::RCP<const Basis> obasis,
        int lmax
){
    vector< Array< RCP<Epetra_MultiVector> > > compensation_charge_grad;
    vector<double> grid = this -> paw_species -> get_grid();
    compensation_charge_grad.resize(3);
    for(int l = 0; l <= lmax; ++l){
        for(int i = 0; i < 3; ++i){
            compensation_charge_grad[i].push_back( rcp( new Epetra_MultiVector( *obasis -> get_map(), 2*l+1 ) ) );
        }
        for(int m = -l; m <= l; ++m){
            RCP<Epetra_MultiVector> tmp_comp_grad = rcp( new Epetra_MultiVector( *obasis -> get_map(), 3, true ) );
            vector<double> comp_charge_r = this -> paw_species -> get_compensation_charge_r( l );
            Radial_Grid::Paw::get_gradient_from_radial_grid( l, m, comp_charge_r, grid, this -> position, obasis, tmp_comp_grad );
            for(int i = 0; i < 3; ++i){
                compensation_charge_grad[i][l] -> operator()(l+m) -> Update( 1.0, *tmp_comp_grad -> operator()(i), 0.0 );
            }
        }
    }
    return compensation_charge_grad;
}

Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > Paw_Atom::retrieve_fine_compensation_potential(){
    if(this -> store_comp_potential == 1){
        if(this -> compensation_potential.size() == 0){
            this -> compensation_potential = this -> interpolate_compensation_potential(true, this -> lmax);
        }
        return this -> compensation_potential;
    }
    return this -> interpolate_compensation_potential(true, this -> lmax);
}

double Paw_Atom::get_TDDFT_Hartree_correction(int i, int a, int j, int b, std::vector< std::vector<double> > proj_dot_orbitals){
    vector< vector< vector< vector<double> > > > dCiiii = this -> paw_species -> get_dE_tensor();
    double rv = 0.0;
    for(int i1 = 0; i1 < dCiiii.size(); ++i1){
        for(int i2 = 0; i2 < dCiiii.size(); ++i2){
            for(int i3 = 0; i3 < dCiiii.size(); ++i3){
                for(int i4 = 0; i4 < dCiiii.size(); ++i4){
                    rv += proj_dot_orbitals[i][i1] * proj_dot_orbitals[a][i2] * proj_dot_orbitals[j][i3] * proj_dot_orbitals[b][i4] * dCiiii[i1][i2][i3][i4];
                }
            }
        }

    }
    return rv;
}

std::vector<double> Paw_Atom::get_TDDFT_transition_dipole_correction(int i, int j, std::vector< std::vector<double> > proj_dot_orbitals){
    vector<double> rv(3);//x, y, z
    double delta_0 = this -> paw_species -> get_delta_scalar();
    vector< vector< vector< vector<double> > > > delta_ij = this -> paw_species -> get_delta_matrix();// l l+m (y, z, x) i j
    int pw_size = delta_ij[0][0].size();
    for(int i1 = 0; i1 < pw_size; ++i1){
        for(int i2 = 0; i2 < delta_ij[0][0].size(); ++i2){
            rv[0] -= (delta_ij[1][2][i1][i2] / sqrt(3) + delta_0 * this -> position[0]) * proj_dot_orbitals[i][i1] * proj_dot_orbitals[j][i2];
            rv[1] -= (delta_ij[1][0][i1][i2] / sqrt(3) + delta_0 * this -> position[1]) * proj_dot_orbitals[i][i1] * proj_dot_orbitals[j][i2];
            rv[2] -= (delta_ij[1][1][i1][i2] / sqrt(3) + delta_0 * this -> position[2]) * proj_dot_orbitals[i][i1] * proj_dot_orbitals[j][i2];
        }
    }
    for(int d = 0; d < 3; ++d){
        rv[d] *= sqrt(4*M_PI);
    }
    return rv;
}

/*
Array< RCP<Epetra_MultiVector> > Paw_Atom::get_spherical_harmonics_on_grid(RCP<const Basis> obasis, int lmax){
    int my_size = obasis -> get_map() -> NumMyElements();
    int* MyGlobalElements = obasis -> get_map() -> MyGlobalElements();
    Array< RCP<Epetra_MultiVector> > spherical;
    for(int l = 0; l <= lmax; ++l){
        spherical.append(rcp(new Epetra_MultiVector(*obasis -> get_map(), 2*l+1, false)));
    }
    if(spherical.size() == 0){return spherical;}
    spherical[0] -> PutScalar(Ylm(0, 0, 0, 0, 0));
    if(spherical.size() == 1){return spherical;}

    double *** vals = new double**[lmax];
    for(int l = 1; l <= lmax; ++l){
        vals[l-1] = new double*[2*l+1];
        for(int m = 0; m < 2*l+1; ++m){
            vals[l-1][m] = new double[my_size]();
        }
    }
#pragma omp parallel for
    for(int il = 0; il < my_size; ++il){
        int i = MyGlobalElements[il];
        double x, y, z, r;
        int i_x = 0, i_y = 0, i_z = 0;
        x = sid[0][i_x] - this -> position[0];
        y = scarid[1][i_y] - this -> position[1];
        z = scaid[2][i_z] - this -> position[2];
        r = sqrt(x*x+y*y+z*z);
        for(int l = 1; l <= lmax; ++l){
            for(int m = -l; m <= l; ++m){
                vals[l-1][l+m][il] = Ylm(l, m, x, y, z);
            }
        }
    }
    for(int l = 1; l <= lmax; ++l){
        for(int m = 0; m < 2*l+1; ++m){
            spherical[l] -> operator()(m) -> ReplaceGlobalValues(my_size, 0, vals[l-1][m], MyGlobalElements);
            delete[] vals[l-1][m];
        }
        delete[] vals[l-1];
    }
    delete[] vals;
    return spherical;
}
*/
