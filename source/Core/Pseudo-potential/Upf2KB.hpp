#pragma once
#include <string>
#include <vector>
//#include <time.h>
#include "Teuchos_RCP.hpp"
#include "Teuchos_Array.hpp"
#include "Teuchos_ParameterList.hpp"

#include "Epetra_Vector.h"
#include "KBprojector.hpp"


class Upf2KB: public KBprojector {
    public:
        /**
         * @brief Constructor.
         * @details Calls initialize_parameters, read_pp, initialize_pp, make_vectors.
         * @callergraph
         * @callgraph
         **/
        Upf2KB(Teuchos::RCP<const Basis> basis,Teuchos::RCP<const Atoms> atoms, Teuchos::RCP<Teuchos::ParameterList> parameters);
        /**
         * @brief Read information from pseudopotential file(s).
         * @details Reads following member variables from Upf files.
         * * From PP_HEADER
         *  - Zvals, number_vps_file, number_pao_file, mesh_size
         * * From PP_R and PP_RAB 
         *  - original_mesh, original_d_mesh
         * * From PP_LOCAL
         *  - local_pp_radial
         * * From PP_NLCC
         *  - nonlinear_core_correction_mesh, nonlinear_core_correction_radial,
         * * From PP_NONLOCAL
         *  - EKB, nonlocal_pp_radial
         *  - oamom
         * 
         * Plus, sets following member variables.
         *  - nonlinear_core_correction, projector_number
         *  - local_mesh, local_d_mesh
         *  - nonlocal_mesh, nonlocal_d_mesh, nonlocal_cutoff
         * *_mesh and *_d_mesh is original_mesh, and original_d_mesh, but cutted off.
         * 
         * Then prints PP informations (see pp_information)
         * @author Kwangwoo Hong
         * @date 2013-12-24
         * @callergraph
         * @callgraph
         **/
        void read_pp();

        /**
        @brief Calculate local pseudopotential.
        @author Kwangwoo Hong, Sunghwan Choi
        @date 2013-12-24
        @param local Local part of pseudopotential (The diagonal elements of the Hamiltonian matrix)
        @param iatom Atom index for finding corresponding pseudopotential file
        @param position The position of the atom
         * @callergraph
         * @callgraph
        */
        void local_potential(double* local,int iatom,double* position);

        /**
        @brief Calculate nonlocal pseudopotential.
        @author Kwangwoo Hong
        @date 2013-12-24
        @param V_nonlocal Nonlocal part of pseudopotential (The diagonal and off-diagonal elements of the Hamiltonian matrix)
        @param iatom Atom index for finding corresponding pseudopotential file
        @param position The position of the atom
         * @callergraph
         * @callgraph
        */
        //    void calculate_nonlocal(double**** V_nonlocal,int iatom,double* position);
        void nonlocal_potential(double**** V_nonlocal,int iatom,double* position);

        std::vector <int> get_number_vps_file();
        void compute_nonlinear_core_correction();
    protected:
        /**
         * @brief Initialize input parameters. Modifies contents of this -> parameters.
         * @callergraph
         * @callgraph
         **/
        void initialize_parameters();

        /**
         * @brief Assigns memory to new_nonlocal_mesh, new_nonlocal_pp_radial, new_nonlocal_cutoff.
         * @details Index order: [atom type][vps][projector_number][radial mesh index]
         * @callergraph
         * @callgraph
         **/
        void initialize_pp();
        double calculate_basis(int iatom,int i,int pp,int l,int m,double* position);

        //int read_pao(int itype);
        void pp_information(int itype);

        void get_comp_potential(std::vector<double> basis, int itype, std::vector<double>& comp_potential);

        /**
         * @brief Interpolates PP projectors to our basis and calculates gradients.
         * @details Use spline interpolation. Index order: [atom index][projector index][magnetic moment index][grid index].
         * @callergraph
         * @callgraph
         **/
        void calculate_nonlocal(int iatom,std::array<double,3> position,
                                std::vector<std::vector<std::vector<double> > >& nonlocal_pp,
                                std::vector<std::vector<std::vector<double> > >& nonlocal_dev_x,
                                std::vector<std::vector<std::vector<double> > >& nonlocal_dev_y,
                                std::vector<std::vector<std::vector<double> > >& nonlocal_dev_z,
                                std::vector<std::vector<std::vector<int> > >& nonlocal_pp_index );

        //from here, this part is added by jaechang

        /**
         * @brief Interpolates local PP to our basis.
         * @details Interpolates local PP to our basis, and calculates derivatives of them.
         * Also applies filtering and supersamplings here.
         * Use linear interpolation for filtering and normal case, and spline interpolation for supersampling.
         * @author Jaechang Lim
         * @callergraph
         * @callgraph
         **/
        virtual void make_local_pp(); 

        std::string word;
        std::vector<int> mesh_size;
        std::vector< std::vector< std::vector<double> > > pao;

        std::vector< std::vector<int> > n_l;
        std::vector< std::vector< std::vector<double> > > fitting_parameters;

        std::vector< std::vector<double> > nonlinear_core_correction_basis; // radial basis; itype, value
        std::vector< std::vector<double> > nonlinear_core_correction_radial; // radial basis; itype, value
        std::vector<bool> nonlinear_core_correction;

        std::vector< std::vector<double> > local_basis;

        bool type_new; // Upf pseudopotential type
        bool is_cal_local_dev = true; ///< calculate local_pp_dev, when we want geometry optimization, this part should be changed
        bool is_cal_nonlocal_dev = true; /// calculate local_pp_dev, when we want geometry optimization, this part should be changed

        Teuchos::Array<std::string> upf_filenames;
        std::string upf_filename;
        int fine_dimension;
};

