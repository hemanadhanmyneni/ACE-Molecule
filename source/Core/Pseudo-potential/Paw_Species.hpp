#pragma once
#include <vector>
#include <string>
#include <algorithm>

#include "Teuchos_RCP.hpp"

#include "../../Utility/Read/Read_Paw.hpp"

/**
 * @brief Atomic PAW calculations for each element.
 * @details For detailed descriptions about PAW method, see arXiv:0910.1921.
 * @author Sungwoo Kang
 * @date 2015
 **/
class Paw_Species{
    public:
        /**
         * @brief Constructor. Read PAW dataset using Read_Paw, internally.
         * @param[in] filename Path to PAW dataset file.
         * @param[in] is_xml If true, GPAW-XML type dataset. If not, it is GZ-compressed. Only support true.
         * @callergraph
         * @callgraph
         **/
        Paw_Species( std::string filename, bool is_xml );
        /**
         * @brief Constructor, from pre-made Read_Paw class.
         * @param[in] paw_data Read_Paw class.
         * @todo Remove this function.
         **/
        Paw_Species( Teuchos::RCP<Read::Read_Paw> paw_data);
        //~Paw_Species();

        // return calculated datas
        /**
         * @brief Return integration of compensation charge on radial_grid.
         * @param[in] l Orbital quantum number
         * @return Integration of compensation charge.
         * @callergraph
         * @callgraph
         **/
        double get_total_compensation_charge(int l);
        /**
         * @brief Return Read_Paw calss.
         * @return Read_Paw class.
         **/
        Teuchos::RCP<Read::Read_Paw> get_read_paw();

        /**
         * @brief Return nonlocal hartree contribution between core density and compensation charge.
         * @return \f$ \Delta C^a \f$ from reference.
         **/
        double get_dE_scalar();
        /**
         * @brief Return nonlocal hartree contribution between core density + compensation charge and valence density.
         * @return \f$ \Delta C^a_{i_1 i_2} \f$ from reference.
         **/
        std::vector< std::vector<double> > get_dE_matrix();
        /**
         * @brief Return nonlocal hartree contribution between valence density.
         * @return \f$ \Delta C^a_{i_1 i_2 i_3 i_4} \f$ from reference.
         **/
        std::vector< std::vector< std::vector< std::vector<double> > > > get_dE_tensor();
        /**
         * @brief Core part contribution of compensation charge expansion coefficient.
         * @return \f$ \Delta^a \f$ from reterence.
         **/
        double get_delta_scalar();
        /**
         * @brief Valence part contribution of compensation charge expansion coefficient.
         * @return \f$ \Delta^a_{L i_1 i_2} \f$ from reference.
         **/
        std::vector< std::vector< std::vector< std::vector<double> > > > get_delta_matrix();

        /**
         * @brief Core part contribution of zero potential correction.
         * @return \f$ B^a \f$ from reterence.
         **/
        double get_zero_correction_scalar();
        /**
         * @brief Valence part contribution of zero potential correction.
         * @return \f$ B^a_{i_1 i_2} \f$ from reference.
         **/
        std::vector< std::vector<double> > get_zero_correction_matrix();
        /**
         * @brief Return grid. First one from the dataset.
         * @return Grid value.
         * @todo Is first one good?
         **/
        std::vector<double> get_grid();
        /**
         * @brief Return analytic derivative of the grid. First one from the dataset.
         * @return Grid derivative value.
         * @todo Is first one good?
         **/
        std::vector<double> get_grid_derivative();
        /**
         * @brief Return hartree potential of compensation charge.
         * @details Calculate hartree potential on the radial grid using Radial_Grid::calculate_Hartree_potential_r.
         * @return Hartree potential of compensation charge in the index order of [l][l+m][grid index]. Result should be multiplied by corresponding spherical harmonics.
         **/
        std::vector< std::vector<double> > get_hartree_potential_of_compensation_charge();

        /**
         * @brief Return core contribution of PAW kinetic energy correction.
         * @return \f$ T^a_c \f$.
         **/
        double get_kinetic_correction_scalar();
        /**
         * @brief Return valence contribution of PAW kinetic energy correction.
         * @return \f$ T^a_{i_1 i_2} \f$.
         **/
        std::vector< std::vector< double > > get_kinetic_correction_matrix();

        /**
         * @brief Analytic integration of smooth core density.
         * @return \f$ -\sqrt{4\pi} \Delta^a - [number of valence electrons]\f$.
         * @callergraph
         * @callgraph
         **/
        double get_smooth_core_density_integral();

        /**
         * @brief Core contribution of external potential.
         * @param[in] v_ext_in_L Spherical harmonics expansion coefficient of external potential in the index order of [l][l+m][grid index]
         * @return \f$ \int d\vec{r} v_{ext}(\vec{r}) [n_c^a - \tilde{n}_c^a]\f$.
         **/
        double get_external_correction_scalar(
            std::vector< std::vector< std::vector<double> > > v_ext_in_L
        );

        /**
         * @brief Valence contribution of external potential.
         * @param[in] v_ext_in_L Spherical harmonics expansion coefficient of external potential in the index order of [l][l+m][grid index]
         * @return \f$ \Delta H_{i_1 i_2}^{a,ext}\f$ from reference.
         **/
        std::vector< std::vector<double> > get_external_correction_matrix(
            std::vector< std::vector< std::vector<double> > > v_ext_in_L
        );

        // Utilities
        /**
         * @brief Change m-independent index to m-dependent index.
         * @param[in] pw_state_no m-independent index.
         * @param[in] m Magnetic angular momentum.
         * @return m-dependent index.
         **/
        int get_integrated_index( int pw_state_no, int m );
        /**
         * @brief Make list that changes m-dependent index to m-independent index.
         * @return List that ith entry contains m-independent index for corresponding m-dependent index.
         **/
        std::vector<int> get_int_to_i_map();
        /**
         * @brief Make list that returns angular momentum for corresponding m-dependent index.
         * @return List that ith entry contains angular momentum for corresponding m-dependent index.
         **/
        std::vector<int> get_int_to_l_map();

        // return read_paw values
        /**
         * @brief Return data: Atomic number.
         * @return Atomic number.
         **/
        int get_atomic_number();
        /**
         * @brief Return data: All electron core density on radial grid.
         * @return All electron core density.
         **/
        std::vector<double> get_all_electron_core_density_r();
        /**
         * @brief Return data: Pseudo electron core density on radial grid.
         * @return Pseudo electron core density.
         **/
        std::vector<double> get_smooth_core_density_r();
        /**
         * @brief Return data: Zero potential on radial grid.
         * @return Zero potential.
         **/
        std::vector<double> get_zero_potential_r();

        /**
         * @brief Return data: ID of partial waves from dataset.
         * @return List of ID of partial waves.
         **/
        std::vector<std::string> get_partial_wave_types();
        /**
         * @brief Return data: All electron partial wave on radial grid.
         * @param[in] wave_state Partial wave ID.
         * @return All electron partial wave of given ID.
         **/
        std::vector<double> get_all_electron_partial_wave_r( std::string wave_state );
        /**
         * @brief Return data: Pseudo partial wave on radial grid.
         * @param[in] wave_state Partial wave ID.
         * @return Pseudo partial wave of given ID.
         **/
        std::vector<double> get_smooth_partial_wave_r( std::string wave_state );
        /**
         * @brief Return data: Projector fucntion on radial grid.
         * @param[in] wave_state Partial wave ID.
         * @return Projector function of given ID.
         **/
        std::vector<double> get_projector_function_r( std::string wave_state );
        /**
         * @brief Return data: All electron partial wave on radial grid.
         * @param[in] wave_state Index of partial wave.
         * @return All electron partial wave of given ID.
         **/
        std::vector<double> get_all_electron_partial_wave_r( int wave_state );
        /**
         * @brief Return data: Pseudo electron partial wave on radial grid.
         * @param[in] wave_state Index of partial wave.
         * @return Pseudo electron partial wave of given ID.
         **/
        std::vector<double> get_smooth_partial_wave_r( int wave_state );
        /**
         * @brief Return data: Projector function on radial grid.
         * @param[in] wave_state Index of partial wave.
         * @return Projector function of given ID.
         **/
        std::vector<double> get_projector_function_r( int wave_state );
        /**
         * @brief Return data: PAW state info.
         * @param[in] wave_state Partial wave ID.
         * @return PAW state data.
         **/
        Read::PAW_STATE get_partial_wave_state( std::string wave_state );
        /**
         * @brief Return data: PAW state info.
         * @param[in] wave_state Index of partial wave.
         * @return PAW state data.
         **/
        Read::PAW_STATE get_partial_wave_state( int wave_state );

        /**
         * @brief Return data: All electron core kinetic energy on radial grid.
         * @return All electron core kinetic energy density.
         **/
        std::vector<double> get_all_electron_core_KE_density_r();
        /**
         * @brief Return data: Pseudo core kinetic energy on radial grid.
         * @return Pseudo core kinetic energy density.
         **/
        std::vector<double> get_smooth_core_KE_density_r();

        /**
         * @brief Return data: Core exact-exchange energy.
         * @return Core exact-exchange energy.
         **/
        double get_EXX_core();
        /**
         * @brief Return data: Valnece exact-exchange energy.
         * @return Valence exact-exchange energy.
         **/
        std::vector< std::vector<double> > get_EXX_matrix();

        /**
         * @brief Return data: XC type.
         * @return 1: LDA, 2: GGA, 3:MGGA, 4:HYBrid.
         * @note This seems unnecessary. Even if this is necessary, string would be better.
         **/
        int get_xc_type();
        /**
         * @brief Return data: XC functional libxc number.
         * @return Libxc number of XC functionals. X first, and C last.
         **/
        std::vector<int> get_xc_functionals();

        /**
         * @brief Return data: Cutoff radius.
         * @return Cutoff radius calculated from zero potential.
         **/
        double get_cutoff_radius();
        /**
         * @brief Return outmost cutoff radius of projector functions.
         * @return Outmost cutoff radius of projector functions.
         **/
        double get_projector_cutoff();
        /**
         * @brief PAW cutoff radius.
         * @return If dataset type version is > 0.7, it is paw_radius variable. If not, max of partial wave/projector function cutoffs.
         **/
        double get_paw_radius();
        /**
         * @brief Partial wave cutoff radius.
         * @return Outmost cutoff radius of partial wave states.
         **/
        double get_partial_wave_cutoff();

        /**
         * @brief Compensation charge on first grid.
         * @param[in] l Orbital quantum number.
         * @return Compensation charge for specified angular momentum.
         **/
        std::vector<double> get_compensation_charge_r( int l );

        /**
         * @brief Return lmax.
         * @return Maximum value of angular momentum.
         **/
        int get_lmax();

        /**
         * @brief Compensation charge on given radial point.
         * @details Performs numerical integration. See Radial_Grid::Paw.
         * @param[in] r Radial point.
         * @param[in] l Orbital quantum number.
         * @return Compensation charge for specified angular momentum.
         * @callergraph
         * @callgraph
         **/
        double get_compensation_charge_r(double r, int l);

        /**
         * @brief Hartree potential from compensation charge on given radial point.
         * @details Performs numerical integration. See Radial_Grid::Paw.
         * @param[in] r Radial point.
         * @param[in] l Orbital quantum number.
         * @return Hartree potential from compensation charge for specified angular momentum.
         * @callergraph
         * @callgraph
         **/
        double get_compensation_potential_r(double r, int l);
        /**
         * @brief Perform analytic integration to get hartree potential from compensation charge on given radial point.
         * @details Works for gaussian-type compensation charge. See arXiv: 0910.1921 equation 75.
         * @param[in] r Radial point.
         * @param[in] l Orbital quantum number.
         * @return Hartree potential from compensation charge for specified angular momentum.
         * @callergraph
         * @callgraph
         **/
        double get_compensation_potential_analytic_r(double r, int l);

    private:
        // read
        /**
         * @brief Performs calculations that are independent to atomic positions, dependent only to the element types.
         * @details Computed values are: \f$ \Delta^a, \Delta^a_{Li_1i_2}, \Delta C^a, \Delta C^a_{i_1i_2}, \Delta C^a_{i_1i_2i_3i_4}, \Delta B^a, \Delta B^a_{i_1i_2}\f$, and Hartree potential of compensation charges.
         * @note Most of private members are related to this calculations.
         * @callergraph
         * @callgraph
         **/
        void system_independent_calculation();

        // Nonlocal energy correction (and Hamiltonian correction)
        /**
         * @brief Calculate \f$ \Delta^a \f$.
         * @return \f$ \Delta^a \f$.
         * @callergraph
         * @callgraph
         **/
        double calculate_delta();
        /**
         * @brief Calculate one element of \f$ \Delta^a_{Li_1i_2} \f$.
         * @param[in] l Orbital quantum nuber.
         * @param[in] m Magnetic quantum number.
         * @param[in] partial_wave1 Partial wave 1 index.
         * @param[in] partial_wave2 Partial wave 2 index.
         * @param[in] partial_wave_m1 Magnetic quantum number of partial wave 1.
         * @param[in] partial_wave_m2 Magnetic quantum number of partial wave 2.
         * @return An element of \f$ \Delta^a_{Li_1i_2} \f$.
         * @callergraph
         * @callgraph
         **/
        double calculate_delta_matrix_element( int l, int m, int partial_wave1, int partial_wave2, int partial_wave_m1, int partial_wave_m2 );

        /**
         * @brief Construct \f$ \Delta^a_{Li_1i_2} \f$.
         * @return \f$ \Delta^a_{Li_1i_2} \f$.
         * @note See actual calculation routine: calculate_delta_matrix_element.
         * @callergraph
         * @callgraph
         **/
        std::vector< std::vector< std::vector< std::vector< double > > > > calculate_delta_matrix();

        /**
         * @brief Calculate \f$ \Delta C^a \f$.
         * @return \f$ \Delta C^a \f$.
         * @callergraph
         * @callgraph
         **/
        double calculate_delta_E_scalar();
        /**
         * @brief Construct \f$ \Delta C^a_{i_1i_2} \f$.
         * @return \f$ \Delta C^a_{i_1i_2} \f$.
         * @note Calculation when l,m of i_1 and i_2 are not equal is omitted since it is zero from spherical harmonics orthonormality.
         * @callergraph
         * @callgraph
         **/
        std::vector< std::vector<double> > calculate_delta_E_matrix();
        /**
         * @brief Construct \f$ \Delta C^a_{i_1i_2i_3i_4} \f$.
         * @return \f$ \Delta C^a_{i_1i_2i_3i_4} \f$.
         * @note Several calculations are omitted since Wigner 3j selection rule,
         *       and real spherical harmonics properties insure that they are zero.
         *       Spherical harmonics orthonormality also applies for all cases.
         *       abs(m1+m2) or abs(m1-m2) should be equal to abs(m3+m4) or abs(m3-m4) from all three rules.
         *       [abs(l1-l2), l1+l2] and [abs(l3-l4), l3+l4] should overlap from Wigner 3j selection rule.
         *       The number of negative m should be even since real spherical harmonics for negative m includes single imaginary number.
         *       If one l is zero, then sum of other l should be even from all three rules.
         *       Note that the integral of product of 3 spherical harmonics have Wigner 3j symbol ( l1 l2 l3 )( 0 0 0 ) term.
         *       It seems that these reduce the calculaion time by 1.4 sec for Cu.
         * @callergraph
         * @callgraph
         **/
        std::vector< std::vector< std::vector< std::vector<double> > > > calculate_delta_E_tensor();

        /**
         * @brief Calculate an element of \f$ \Delta C^a_{i_1i_2} \f$.
         * @param[in] state1,state2 State index.
         * @param[in] m1,m2 Magnetic quantum number of corresponding state.
         * @param[in] ae_core_pot Hartree potential of all-electron core density.
         * @param[in] ps_core_pot Hartree potential of pseudo core density.
         * @param[in] g0_pot Hartree potential of compensation charge, l, m = 0, 0.
         * @param[in] coul_ps_core_g00 Coulomb integral between pseudo core density and g00.
         * @param[in] coul_g00 Coulomb integral between g00 and g00.
         * @return \f$ \Delta C^a_{i_1i_2} \f$.
         * @todo Remove this function and calculate the whole matrix using proper math function.
         * @callergraph
         * @callgraph
         **/
        double calculate_delta_E_matrix( int i1, int i2, int m1, int m2, std::vector<double> ae_core_pot,
                std::vector<double> ps_core_pot, std::vector<double> g0_pot,
                double coul_ps_core_g00, double coul_g00);
        /**
         * @brief Calculate an element of \f$ \Delta C^a_{i_1i_2i_3i_4} \f$.
         * @param[in] state1,state2,state3,state4 State index.
         * @param[in] m1,m2,m3,m4 Magnetic quantum number of corresponding state.
         * @param[in] ae_pw12_dot Product of AE partial wave 1 and 2.
         * @param[in] ps_pw12_dot Product of pseudo partial wave 1 and 2.
         * @param[in] Hartree_ae34 Hartree potential of product of AE partial wave 3 and 4. Index: [L][grid index].
         * @param[in] Hartree_ps34 Hartree potential of product of pseudo partial wave 3 and 4. Index: [L][grid index].
         * @param[in] coul_12gL Coulomb integral between the product of pseudo partial wave 1 and 2, and compensation charge. Index: [Compensation L][Compensation L+M].
         * @param[in] coul_34gL Coulomb integral between the product of pseudo partial wave 3 and 4, and compensation charge. Index: [Compensation L][Compensation L+M].
         * @param[in] coul_gL: Coulomb potential of compensation charge. Index: [L][L+M].
         * @return \f$ \Delta C^a_{i_1i_2i_3i_4} \f$.
         * @todo Remove this function and calculate the whole matrix using proper math function.
         * @callergraph
         * @callgraph
         **/
        double calculate_delta_E_4tensor(
            int state1, int state2, int state3, int state4,
            int m1, int m2,int m3, int m4,
            std::vector<double> ae_pw12_dot,
            std::vector<double> ps_pw12_dot,
            std::vector< std::vector<double> > Hartree_ae34,
            std::vector< std::vector<double> > Hartree_ps34,
            std::vector< std::vector<double> > coul_12gL,
            std::vector< std::vector<double> > coul_34gL,
            std::vector< std::vector<double> > coul_gL
        );

        // Related with zero potential
        /**
         * @brief Calculate \f$ \Delta B^a \f$.
         * @return \f$ \Delta B^a \f$.
         * @callergraph
         * @callgraph
         **/
        double calculate_zero_correction_scalar();
        /**
         * @brief Calculate an element of \f$ \Delta B^a_{i_1i_2} \f$.
         * @param[in] i,j State index.
         * @param[in] m1 Magnetic quantum number of corresponding state.
         * @return An element of \f$ \Delta B^a_{i_1i_2} \f$.
         * @callergraph
         * @callgraph
         **/
        double calculate_zero_correction_matrix_element( int i, int j, int m1, int m2 );
        /**
         * @brief Construct \f$ \Delta B^a_{i_1i_2} \f$.
         * @return \f$ \Delta B^a_{i_1i_2} \f$.
         * @callergraph
         * @callgraph
         **/
        std::vector< std::vector<double> > calculate_zero_correction_matrix();

        // related with compensation charge
        /**
         * @brief Calculate hartree potential of compensation charge.
         * @param[in] grid_id Grid ID.
         * @return Hartree potential of compensation charge, on radial grid. Index: [L][L+M][grid index].
         * @callergraph
         * @callgraph
         **/
        std::vector< std::vector<double> > calculate_radial_hartree_from_comp_charge(
            std::string grid_id
        );

        // Utilities
        /**
         * @brief Get magnetic quantum number from m-dependent index.
         * @return List that ith entry has magnetic quantum number correspond to that m-dependent index.
         **/
        std::vector<int> get_int_to_m_map();
        /**
         * @brief Change Read_Paw data to 1st grid.
         * @param data Grid ID, data vector. Return value of get_... functions.
         * @return Converted data.
         **/
        std::vector<double> convert_to_my_grid(
            std::pair< std::string, std::vector<double> > data
        );

        ///< @breif Calculated Data.
        double dE_scalar;
        ///< @breif Calculated Data.
        std::vector< std::vector<double> > dE_matrix;
        ///< @breif Calculated Data.
        std::vector< std::vector< std::vector< std::vector<double> > > > dE_tensor;
        ///< @breif Calculated Data.
        double delta_scalar;
        ///< @breif Calculated Data.
        std::vector< std::vector< std::vector< std::vector< double > > > > delta_matrix;
        ///< @breif Calculated Data.
        double zero_correction_scalar;
        ///< @breif Calculated Data.
        std::vector< std::vector<double> > zero_correction_matrix;

        std::vector<int> integrated_to_i;
        std::vector<int> index_to_l;
        std::vector<int> index_to_m;
        int lmax;

        std::string radial_grid_id;
        std::vector< std::vector<double> > compensation_hartree;

        // Data storage
        Teuchos::RCP<Read::Read_Paw> paw_data;
//        bool internal_paw_data;
};
