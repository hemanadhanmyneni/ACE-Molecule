#pragma once
#include <vector>
#include <string>

#include "Epetra_Vector.h"
#include "Epetra_MultiVector.h"
#include "Epetra_CrsMatrix.h"
//#include "Epetra_Map.h"
#include "Teuchos_RCP.hpp"
#include "Teuchos_Array.hpp"
#include "Teuchos_ParameterList.hpp"
#include "Teuchos_SerialDenseMatrix.hpp"

#include "../../Basis/Basis.hpp"

#include "../Pseudo-potential/Paw_Atom.hpp"
#include "../Occupation/Occupation.hpp"
#include "../../Io/Atoms.hpp"

#include "../../Compute/Poisson_Solver.hpp"

/**
 * @brief Concatanates atomic PAW calculations.
 * @details This class owns Paw_Atom classes, which has a atomic informations.
 *          For detailed descriptions about PAW method, see arXiv:0910.1921.
 *          This class also stores some values, like hartree potential within fine basis, default 2.
 * @author Sungwoo Kang
 * @date 2015
 * @note This class does not compute almost anything. For detailed calculation information, see Paw_Atom class.
 * @todo Currently, PAW shows bad results for d-metals. For example, AgH and Ag2 atomization energy is freaking bad.
 *         See atomization energy result from: .
 *         Also, this is freaking slow, compared to UPF. See benzene test from: .
 **/
class Paw{
    public:
        /**
         * @brief Correct energy using PAW formalism.
         * @param[in] occupations Occupations of the current state.
         * @param[in] orbitals Orbitals of the current state.
         * @param[in] hartree_vector Hartree potential, necessary for PAW calculations.
         * @param[in,out] hartree_energy Corrected hartree energy. For PAW, this is completely re-calculated since initial hartree_energy does not consider core density at all. Input and output.
         * @param[in,out] int_n_vxc Corrected \f$ \int n \times v_{xc} \f$. Input and output.
         * @param[in,out] x_energy Corrected exchange energy. Input and output.
         * @param[in,out] c_energy Corrected correlation energy. Input and output.
         * @param[in,out] kinetic_energy Corrected kinetic energy. Input and output.
         * @param[in,out] external_energy Corrected external energy. Note that external energy means zero correction for PAW, unlike KBprojector which it stands for nuclear-electron interaction. Such interaction is included in hartree_energy for PAW and is hard to separate. Input and output.
         * @return Always 3.
         * @note Currently, int_n_vxc is not update for PAW since it takes considerable time.
         * @note Currently, external energy updating is not implemented.
         * @callergraph
         * @callgraph
         **/
        int get_energy_correction(
            Teuchos::Array< Teuchos::RCP<const Occupation> > occupations,
            Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbitals,
            Teuchos::RCP<Epetra_Vector> &hartree_vector,
            double &hartree_energy,
            double &int_n_vxc,
            double &x_energy,
            double &c_energy,
            double &kinetic_energy,
            std::vector<double> &kinetic_energies,
            double &zero_energy,
            double &external_energy
        );

        /**
         * @brief Returns overlap_matrix. Teuchos::null if overlap matridx is not in formalism.
         * @param[out] overlap_matrix Output overlap matrix.
         * @return Always 3.
         * @note This does not calculate overlap matrix, actually. See calculate_overlap_matrix for details.
         * @callergraph
         * @callgraph
         **/
        int get_overlap_matrix( Teuchos::RCP<Epetra_CrsMatrix> &overlap_matrix );

        /**
         * @brief Get density correction.
         * @details This turns pseudo valence density to all-electron valence density. Does nothing with core density, though.
         * @param[in,out] density Density to correct. Input and output.
         * @return Always 3.
         * @callergraph
         * @callgraph
         **/
        int get_density_correction( Teuchos::Array< Teuchos::RCP<Epetra_Vector> > &density);
        /**
         * @brief Get orbial correction.
         * @details This turns pseudo orbitals to all-electron orbitals.
         * @param[in,out] orbitals Orbitals to update. Input and output.
         * @return Always 3.
         * @callergraph
         * @callgraph
         **/
        int get_orbital_correction( Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > &orbitals);

        /**
         * @brief Returns hamiltonian correction.
         * @param[out] dH_coeff PAW Hamiltonian correction coefficient in the index order of [atom index][spin index][matrix row index][matrix column index]
         * @callergraph
         * @callgraph
         **/
        void calculate_hamiltonian_correction_coeff(
            std::vector< std::vector< std::vector< std::vector<double> > > > &dH_coeff
        );

        /**
         * @brief Update PAW atom-centered density matrix and store it to the class, internally.
         * @param[in] occupations Occupations of the current state.
         * @param[in] wavefunctions Orbitals of the current state.
         * @callergraph
         * @callgraph
         **/
        void update_PAW_D_matrix(
            Teuchos::Array< Teuchos::RCP<const Occupation> > occupations,
            Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > wavefunctions
        );

        /**
         * @brief Construct initial PAW atom-centered density matrix and store it to the class, internally.
         * @details Initial density matrix is calculated from PAW dataset. Distribute charge to each atoms.
         * @callergraph
         * @callgraph
         **/
        void initialize_PAW_D_matrix();

        /**
         * @brief Returns zero potential for the system.
         * @return Zero potential for the system.
         * @callergraph
         * @callgraph
         **/
        Teuchos::RCP<Epetra_Vector> get_zero_potential();
        /**
         * @brief Returns zero potential gradient for the system.
         * @param[in] is_fine If true, returns fine version. If not returns coarse version for the calculation basis.
         * @return Gradient of zero potential for the system.
         * @callergraph
         * @callgraph
         **/
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > get_zero_potential_gradient(bool is_fine = false);

        /**
         * @brief Returns compensation charge for the system.
         * @param[in] is_fine If true, returns fine version. If not returns coarse version for the calculation basis.
         * @return Compensation charge.
         * @callergraph
         * @callgraph
         **/
        Teuchos::RCP<Epetra_Vector> get_compensation_charge( bool is_fine = false );

        /**
         * @brief Add hartree potential for compensation charge and core density. Also, interpolates hartree potential and stores internally.
         * @param[in,out] hartree_vector Hartree potential of pseudo valence density. Input and output.
         * @note This should not use Trilinear interpolation since the quality of interpolation is bad.
         * @callergraph
         * @callgraph
         **/
        void hartree_vector_update(
            Teuchos::Array< Teuchos::RCP<Epetra_Vector> > ps_density,
            Teuchos::RCP<Epetra_Vector> &hartree_vector
        );

        /**
         * @brief Returns hartree potential of compensation charge.
         * @param[in] is_fine If true, returns fine version. If not returns coarse version for the calculation basis.
         * @return Hartree potential of compensation charge.
         * @callergraph
         * @callgraph
         **/
        Teuchos::RCP<Epetra_Vector> get_Hartree_potential_from_comp_charge( bool is_fine = false );

        /**
         * @brief Returns analytic integration of compensation charge.
         * @return Integration of compensation charge.
         * @callergraph
         * @callgraph
         **/
        std::vector<double> get_total_compensation_charges();

        /**
         * @brief Returns the sum of core atom-center density.
         * @param[in] is_ae If true, returns all-electron density. If not, returns pseudo density.
         * @return Atom-center core density, index indicates spin.
         * @callergraph
         * @callgraph
         **/
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > get_atomcenter_core_density(bool is_ae, bool is_fine = false);

        /**
         * @brief Returns the sum of atom-center density.
         * @param[in] is_ae If true, returns all-electron density. If not, returns pseudo density.
         * @return Atom-center density, index indicates spin.
         * @callergraph
         * @callgraph
         **/
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > get_atomcenter_density(bool is_ae );

        // Constructor
        /**
         * @brief Constructor
         * @param[in] basis Calculation basis.
         * @param[in] atoms Atoms to compute.
         * @param[in] parameters Parameter for PAW. For detail, see input manual.
         * @note This calls initialize_parameters(), initialize_finegrid(), and read_all().
         * @callergraph
         * @callgraph
         **/
        Paw(
            Teuchos::RCP<const Basis> basis,
            Teuchos::RCP<const Atoms> atoms,
            Teuchos::RCP<Teuchos::ParameterList> parameters
        );

        // Data
        /**
         * @brief Mix PAW atomcenter density matrix.
         * @details This was implemented to match the mixing of PAW atom-centered density matrix to valence density.
         * @param[in] mixing_coeffs List of mixing coefficient for last few density matrixes.
         * @param[in] mixing_alpha Mixing coefficient.
         * @param[in] ispin Spin index.
         * @callergraph
         * @callgraph
         **/
        void density_matrix_mixing(
            std::vector<double> mixing_coeffs,
            double mixing_alpha,
            int ispin
        );

        /**
         * @brief Print current atom-centered density matrix.
         **/
        void print_PAW_D_matrix();

        /**
         * @brief Returns current atom-centered density matrixes.
         * @details This was implemented to support PAW_Density mixing.
         * @return Current density matrix in the index order of [atom index][spin index](matrix row, column index).
         **/
        std::vector< Teuchos::Array< Teuchos::SerialDenseMatrix<int,double> > > get_density_matrixes();
        /**
         * @brief Set current atom-centered density matrixes.
         * @details This was implemented to support PAW_Density mixing.
         * @param[in] D_matrixes Density matrix to set in the index order of [atom index][spin index](matrix row, column index).
         **/
        void set_density_matrixes(std::vector< Teuchos::Array< Teuchos::SerialDenseMatrix<int,double> > > D_matrixes);

        /**
         * @brief Returns Paw_Atom class for an atom.
         * @param[in] iatom Atom index to return.
         * @return Paw_Atom class for corresponding iatom.
         * @note This function will throw std::out_of_range exception.
         * @note Changing the return value of this function will change that of this class. However, replacing will not.
         **/
        Teuchos::RCP<Paw_Atom> get_paw_atom(int iatom);
        Teuchos::RCP<Paw_Species> get_paw_species(int itype);

        /**
         * @brief Returns fine hartree potential, lately calculated.
         * @return Hartree potential on fine grid.
         **/
        Teuchos::RCP<Epetra_Vector> get_fine_hartree_potential() const;
        /**
         * @brief Returns fine Basis class used in calculation.
         * @return Basis class, fine version.
         **/
        Teuchos::RCP<const Basis> get_fine_basis();

    protected:
        /**
         * @brief Initialize class parameter.
         * @details Retrieve pseudopotential filename, spin size, number of electrons, and overlap matrix cutoff (default 0.0001).
         * @callergraph
         * @callgraph
         **/
        void initialize_parameters();
        /**
         * @brief Create fine grid, same with calculation basis but more fine.
         * @details Scaling is divided by FineGrid order and number of points are increased to match the box size.
         * @callergraph
         * @callgraph
         **/
        void initialize_finegrid();
        /**
         * @brief Read each elements and set some initial calculations.
         * @details Read elements and stores it as Paw_Species. Construct Paw_Atom using Paw_Species.
         *          Initialize atom-center density matrix, fine hartree potential. Calculate overlap matrix.
         * @callergraph
         * @callgraph
         **/
        void read_all();

        /**
         * @brief Returns the more accurate hartree energy.
         * @details The calculation with compensation chrge is done within the fine mesh. Requires hartree_vector_update to be called.
         * @return \f$ \frac{1}{2} \int dr (n(r)+g(r)) v_H(r)\f$ where \f$ g(r)\f$ is compensation charge and \f$v_H(r)\f$ is hartree potential.
         * @callergraph
         * @callgraph
         **/
        double recalculate_hartree_energy(Teuchos::Array< Teuchos::RCP<Epetra_Vector> > scaled_density, Teuchos::RCP<Epetra_Vector> hartree_potential);

        /**
         * @brief Returns the more accurate hartree potential.
         * @details The calculation with compensation chrge is done within the fine mesh. Requires hartree_vector_update to be called.
         * @return \f$ \frac{1}{2} \int dr (n(r)+g(r)) v_H(r)\f$ where \f$ g(r)\f$ is compensation charge and \f$v_H(r)\f$ is hartree potential.
         * @callergraph
         * @callgraph
         **/
        Teuchos::RCP<Epetra_Vector> recalculate_hartree_potential(
            Teuchos::Array< Teuchos::RCP<Epetra_Vector> > ps_density,
            Teuchos::RCP<Epetra_Vector> &hartree_vector
        );
        /**
         * @brief This function gathers atomic number from Paw_Species class and store it as vector.
         **/
        void set_paw_element_types();
        /**
         * @brief Check if the PAW augmented sphere overlaps using the outmost partial wave cutoff. If they overlap, it will give error.
         **/
        void check_sphere_overlap();
        /**
         * @brief Create map between element index and atom index.
         * @return List that atom index entry contains element index.
         **/
        std::vector<int> map_atom_to_element();

        /**
         * @brief Gathers overlap matrix coefficient from Paw_Atom and creates Epetra_CrsMatrix.
         **/
        void calculate_overlap_matrix();

        /**
         * @brief Print reference energy that is same to the GPAW output.
         * @todo After debug, remove this.
         **/
        void print_reference_energy();

        Teuchos::Array<std::string> paw_filenames;
        Teuchos::Array< Teuchos::RCP<Paw_Species> > paw_species;
        Teuchos::Array< Teuchos::RCP<Paw_Atom> > paw_atoms;
        std::vector< Teuchos::Array< Teuchos::SerialDenseMatrix<int,double> > > PAW_D_matrixes;
        Teuchos::RCP<Epetra_CrsMatrix> overlap_matrix;
        std::vector< std::vector< std::vector< std::vector<double> > > > H_correction;
        std::vector<int> paw_element_types;
        std::vector<int> atom_to_element_list;
        bool type_xml = false;
        int spin_size = 0;
        int num_electrons = 0;
        double charge = 0.0;
        /**
         * @detail 0 then vH(core+compensation) from radial grid.
         *         1 then vH(val+core) from fine grid and vH(compensation) from radial grid.
         *         2 then vH(total) from fine grid.
         **/
        int hartree_type = 0;

        Teuchos::RCP<const Basis> basis;
        Teuchos::RCP<const Basis> fine_basis;
        Teuchos::RCP<const Atoms> atoms;
        Teuchos::RCP<Teuchos::ParameterList> parameters;
        Teuchos::RCP<Poisson_Solver> fine_poisson = Teuchos::null;

        Teuchos::RCP<Epetra_Vector> fine_hartree_vector;
        //Teuchos::RCP<Epetra_CrsGraph> new_hamiltonian_structure;

        std::vector< std::vector< Teuchos::Array< Teuchos::SerialDenseMatrix<int,double> > > > PAW_D_matrixes_history_in;
        std::vector< std::vector< Teuchos::Array< Teuchos::SerialDenseMatrix<int,double> > > > PAW_D_matrixes_history_out;
};
