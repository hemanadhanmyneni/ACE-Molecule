#include "Nuclear_Potential_Matrix.hpp"
#include <iostream>
#include <cmath>
#include "Epetra_Map.h"
#include "../../Utility/Verbose.hpp"
#include "../../Utility/Parallel_Util.hpp"
//#define MATRIX_CUTOFF 1.0E-20
#include "../../Utility/Time_Measure.hpp"

using std::vector;
using std::abs;
using Teuchos::rcp;
using Teuchos::RCP;

Nuclear_Potential_Matrix::Nuclear_Potential_Matrix( RCP<const Atoms> atoms, int spin_size, double cutoff/* = 1.0E-10*/ ){
    this -> atoms = atoms;
    this -> spin_size = spin_size;
    this -> matrix_cutoff = cutoff;
//    this-> blas  = rcp(new Teuchos::BLAS<int,double>() );
}

int Nuclear_Potential_Matrix::set_local_pot( RCP<Epetra_Vector> local_pot ){
    this -> local_potential = rcp( new Epetra_Vector(*local_pot) );

    return 0;
}

int Nuclear_Potential_Matrix::set_nonlocal_pot_proj(
    vector< vector< vector<double> > > proj_vector,
    vector< vector< vector<int> > > proj_index
){
    this -> proj_vector = proj_vector;
    this -> proj_index = proj_index;

    for(int iatom = 0; iatom < this -> atoms -> get_size(); ++iatom ){
        int dim = this -> proj_vector[iatom].size();
        //Verbose::single(Verbose::Detail) << "NPM number of projectors = " << dim << std::endl;
        int elements = 0;
        for(int i = 0; i < dim; ++i){
            //Verbose::single(Verbose::Detail) << this -> proj_index[iatom][i].size() << "\t";
            elements += this -> proj_index[iatom][i].size();
        }
        //Verbose::single(Verbose::Detail) << "NPM projector elements = " << elements << std::endl;
        //Verbose::single(Verbose::Detail) << std::endl;
    }
    return 0;
}
int Nuclear_Potential_Matrix::set_nonlocal_pot_coeff(
    vector< vector< vector< vector<double> > > > nonlocal_coeff,
    bool is_add/* = false */
){
    if( is_add and this -> nonlocal_coeff.size() > 0 ){
        for(int s = 0; s < this -> spin_size; ++s){
            for(int iatom = 0; iatom < this -> atoms -> get_size(); ++iatom ){
                int dim = this -> nonlocal_coeff[s][iatom].size();
                for(int i = 0; i < dim; ++i){
                    for(int j = 0; j < dim; ++j){
                        this -> nonlocal_coeff[s][iatom][i][j] += nonlocal_coeff[s][iatom][i][j];
                    }
                }
            }
        }
    } else {
        this -> nonlocal_coeff = nonlocal_coeff;
    }

    /*
    for(int s = 0; s < this -> spin_size; ++s){
        for(int iatom = 0; iatom < this -> atoms -> get_size(); ++iatom ){
            int dim = this -> nonlocal_coeff[s][iatom].size();
            for(int i = 0; i < dim; ++i){
                for(int j = 0; j < dim; ++j){
                    Verbose::single(Verbose::Detail) << this -> nonlocal_coeff[s][iatom][i][j] << "\t";
                }
                Verbose::single(Verbose::Detail) << std::endl;
            }
            Verbose::single(Verbose::Detail) << std::endl;
        }
        Verbose::single(Verbose::Detail) << std::endl;
    }
//  */
    return 0;
}

int Nuclear_Potential_Matrix::multiply(int ispin, RCP<Epetra_MultiVector> input, RCP<Epetra_MultiVector> &output) const{
    if(input -> NumVectors() != output -> NumVectors()){
        Verbose::all() << "Error on Nuclear_Potential_Matrix::multiply " <<std::endl;
        exit(EXIT_FAILURE);
    }
    const int numVecs = input -> NumVectors();
    auto map = input->Map();
    output->PutScalar(0.0);
    double* coef =new double[numVecs](); //initialize coef array as 0;
    double* total_coef =new double[numVecs](); //initialize coef array as 0;
    int index;    int proj_size;
    double value;

    for(int iatom=0;iatom<atoms->get_size();iatom++){
        for(int i1 = 0; i1 < this -> proj_index[iatom].size(); i1++){
            for(int j=0; j<numVecs; j++){
                coef[j]=0.0; total_coef[j]=0.0;
            }
            for(int i2 = 0; i2 < this -> proj_index[iatom].size(); i2++){
                if (fabs(nonlocal_coeff[ispin][iatom][i2][i1])<matrix_cutoff) continue;
                proj_size = proj_index[iatom][i2].size();
                for(int j=0; j<numVecs; j++){
                    double sum=0.0;
                    #ifdef ACE_HAVE_OMP
                    //#pragma omp parallel for private(index) reduction(+:sum)
                    #endif
                    for(int a=0; a<proj_size; a++){
                        index = map.LID(proj_index[iatom][i2][a]);
                        if(index<0) continue;
                        sum+=proj_vector[iatom][i2][a]*input->operator()(j)->operator[](index) ;
                    }
                    coef[j]+=nonlocal_coeff[ispin][iatom][i2][i1]*sum ;
                }
            }
            Parallel_Util::group_sum(coef, total_coef, numVecs);
            proj_size = proj_index[iatom][i1].size();

            for(int j=0; j<numVecs; j++){
                #ifdef ACE_HAVE_OMP
                #pragma omp parallel for private(index,value)
                #endif
                for(int a=0; a<proj_size; a++){
                    index = map.LID(proj_index[iatom][i1][a]);
                    if(index<0) continue; // this index is not belong to my processor
                    value = total_coef[j]*proj_vector[iatom][i1][a];
                    if(fabs(value)>matrix_cutoff){
                        #pragma omp critical
                        {
                            output->operator()(j)->operator[](index)+= value;
                        }
                    }
                }
            }
        }
    }
    delete[] coef;
    delete[] total_coef;
    for(int j = 0; j < input -> NumVectors();j++){
        output->operator()(j)->Multiply(1.0, *local_potential, *input->operator()(j), 1.0);
    }
    //std::cout << *output << std::endl;
    //exit(-1);
    return 0;
}
int Nuclear_Potential_Matrix::multiply(int ispin, const Anasazi::MultiVec<double>& X,
        Anasazi::MultiVec<double>& Y) const{
    return 0;
}

int Nuclear_Potential_Matrix::add(int ispin, RCP<Epetra_CrsMatrix> matrix, RCP<Epetra_CrsMatrix> &output, double scalar){
    output = rcp(new Epetra_CrsMatrix(*matrix) );
    add(ispin, output, scalar);
    return 0;
}

int Nuclear_Potential_Matrix::add(int ispin, RCP<Epetra_CrsMatrix> &matrix, double scalar){
    if( matrix -> Filled() ){
        return this -> add_filled(ispin, matrix, scalar);
    }
    RCP<Time_Measure> timer = rcp(new Time_Measure());
    timer->start("Nuclear_Potential_Matrix::add!!");
    int * MyGlobalElements = matrix->Map().MyGlobalElements();
    int NumMyElements = matrix->Map().NumMyElements();
    int ierr_all = 0;
    for(int iatom=0;iatom<atoms->get_size();iatom++){
        //int itype = atoms->get_atom_type(iatom);
        for(int i1 = 0; i1 < this -> proj_index[iatom].size(); ++i1){
            for(int i2 = 0; i2 < this -> proj_index[iatom].size(); ++i2){
                //double matrix_coeff = this -> nonlocal_coeff[ispin][iatom][i1][i2];
                double matrix_coeff = this -> nonlocal_coeff[ispin][iatom][i1][i2];
                if( abs(matrix_coeff) > this -> matrix_cutoff *1.0E-5 ){
                    const int index_size = this -> proj_index[iatom][i2].size();
                    double value =0.0; int ierr=0;
                    #ifdef ACE_HAVE_OMP
                    #pragma omp parallel for private(value,ierr)
                    #endif
                    for(int a = 0; a < index_size; ++a){
                        vector<double> values;
                        values.reserve(proj_index[iatom][i1].size());
                        vector<int> indexs;
                        indexs.reserve(proj_index[iatom][i1].size());
                        int a_index = proj_index[iatom][i2][a];
                        if(matrix->Map().MyGID(a_index)){
                            for(int b = 0; b < proj_index[iatom][i1].size(); ++b){
                                value = matrix_coeff * proj_vector[iatom][i2][a] * proj_vector[iatom][i1][b] *scalar;
                                if( fabs(value) > this -> matrix_cutoff ){
                                    values.push_back(value);
                                    indexs.push_back(proj_index[iatom][i1][b]);
                                }
                            }
                            ierr = matrix->InsertGlobalValues(a_index, values.size(), &values[0], &indexs[0]);
                            if(ierr < 0){
                                ierr_all = ierr;
                                Verbose::all() << iatom <<  " ERROR: Nuclear_Potential_Matrix add InsertGlobalValues ierr = " << ierr << std::endl;
                            }
                        }
                    }
                }
            }
        }
    }
     
    #ifdef ACE_HAVE_OMP
    #pragma omp parallel for
    #endif
    for (int i=0; i < NumMyElements; i++){
        double ierr = matrix -> InsertGlobalValues(MyGlobalElements[i], 1, &local_potential -> operator[](i), &MyGlobalElements[i]);
        if(ierr < 0){
            ierr_all = ierr;
            Verbose::all() << "ierr error (add local potential) = " << ierr << std::endl;
        }
    }
    timer->end("Nuclear_Potential_Matrix::add!!");
    Verbose::single(Verbose::Detail) << "shchoi/Nuclear_Potential_Matrix::add!!\t" << timer->get_elapsed_time("Nuclear_Potential_Matrix::add!!",-1) << "s"<<std::endl;
    return ierr_all;
}

int Nuclear_Potential_Matrix::add_filled(int ispin, RCP<Epetra_CrsMatrix> &matrix, double scalar){
    //int * MyGlobalElements = matrix->Map().MyGlobalElements();
    //int NumMyElements = matrix->Map().NumMyElements();
    RCP<Time_Measure> timer = rcp(new Time_Measure());
    timer->start("Nuclear_Potential_Matrix::add_filled!!");
    for(int iatom=0;iatom<atoms->get_size();iatom++){
        vector<double> proj_coeff_max;
        for(int ind = 0; ind < this -> proj_index[iatom].size(); ++ind){
            if( this -> proj_vector[iatom][ind].size() > 0 ){
                double max_val = *std::max_element(proj_vector[iatom][ind].begin(), proj_vector[iatom][ind].end());
                double min_val = *std::min_element(proj_vector[iatom][ind].begin(), proj_vector[iatom][ind].end());
                proj_coeff_max.push_back( std::max(abs(max_val), abs(min_val) ) );
            } else {
                proj_coeff_max.push_back(0.0);
            }
        }

        const int proj_size = this -> proj_index[iatom].size();
//        #ifdef ACE_HAVE_OMP
//        #pragma omp parallel for collapse(2)
//        #endif
        for(int i1 = 0; i1 < proj_size; ++i1){
            for(int i2 = 0; i2 < proj_size; ++i2){
                const double matrix_coeff = this -> nonlocal_coeff[ispin][iatom][i1][i2];
                //double matrix_coeff = this -> nonlocal_coeff[ispin][iatom][i2][i1];
                if( fabs(matrix_coeff * proj_coeff_max[i1] * proj_coeff_max[i2] ) > this -> matrix_cutoff ){
                    const int index_size2 = this -> proj_index[iatom][i2].size();
                    const int index_size1 = this -> proj_index[iatom][i1].size();
                    #ifdef ACE_HAVE_OMP
                    #pragma omp parallel for
                    #endif
                    for(int a = 0; a < index_size2; ++a){
                        vector<double> values;
                        values.reserve(index_size1);
                        vector<int> indexs;
                        indexs.reserve(index_size1);
                        int a_index = proj_index[iatom][i2][a];
                        if(matrix->Map().MyGID(a_index)){
                            for(int b = 0; b < index_size1; ++b){
                                double tmp = matrix_coeff * proj_vector[iatom][i2][a] * proj_vector[iatom][i1][b] *scalar;
                                if( fabs(tmp) > this -> matrix_cutoff ){
                                    values.push_back(tmp);
                                    indexs.push_back(proj_index[iatom][i1][b]);
                                }
                            }
                            //int ierr = matrix -> SumIntoGlobalValues(a_index, values.size(), &values[0], &proj_index[iatom][i1][0]);
                            int ierr = matrix -> SumIntoGlobalValues(a_index, values.size(), &values[0], &indexs[0]);
                            if(ierr < 0){
                                Verbose::all() << iatom <<  " ERROR: Nuclear_Potential_Matrix add_filled SumIntoGlobalValues ierr = " << ierr << std::endl;
                                exit(EXIT_FAILURE);
                            }
                        }
                    }
                }
            }
        }
    }

    RCP<Epetra_Vector> diagonal = rcp( new Epetra_Vector( matrix -> Map() ) );
    matrix -> ExtractDiagonalCopy( *diagonal );
    diagonal -> Update( 1.0, *local_potential, 1.0 );
    int ierr = matrix -> ReplaceDiagonalValues( *diagonal );
    if(ierr < 0){
        Verbose::all() << "ERROR: Nuclear_Potential_Matrix add_filled ReplaceDiagonalValues ierr = " << ierr << std::endl;
        exit(EXIT_FAILURE);
    }
    timer->end("Nuclear_Potential_Matrix::add_filled!!");
    Verbose::single(Verbose::Detail) << "shchoi/Nuclear_Potential_Matrix::add_filled!!\t" << timer->get_elapsed_time("Nuclear_Potential_Matrix::add_filled!!",-1) << "s"<<std::endl;

    return 0;
}

RCP<const Epetra_Vector> Nuclear_Potential_Matrix::get_local_potential(){
    return Teuchos::rcp_const_cast<const Epetra_Vector>(this -> local_potential);
}

int Nuclear_Potential_Matrix::get_projectors(
    vector< vector< vector<double> > > &proj_vector,
    vector< vector< vector<int> > > &proj_index
){
    proj_vector = this -> proj_vector;
    proj_index = this -> proj_index;
    return 0;
}

vector< vector< vector< vector<double> > > > Nuclear_Potential_Matrix::get_nonlocal_coeff(){
    return this -> nonlocal_coeff;
}

int Nuclear_Potential_Matrix::get_epetra_structure(
    RCP<Epetra_CrsMatrix> kinetic_matrix,
    RCP<Epetra_CrsGraph> &graph
){
    int * MyGlobalElements = kinetic_matrix->Map().MyGlobalElements();
    int NumMyElements = kinetic_matrix->Map().NumMyElements();
    int size = kinetic_matrix -> NumGlobalRows();

    graph = rcp( new Epetra_CrsGraph( Copy, kinetic_matrix -> Map(), 0 ) );

    int * indices = new int[size];
    for(int i = 0; i < size; ++i){
        int num_indices;
        kinetic_matrix -> Graph().ExtractGlobalRowCopy(i, size, num_indices, indices);
        graph -> InsertGlobalIndices(i, num_indices, indices);
    }
    delete[] indices;

    for(int iatom=0;iatom<atoms->get_size();iatom++){
        vector<double> proj_coeff_max;
        for(int ind = 0; ind < this -> proj_index[iatom].size(); ++ind){
            if( this -> proj_vector[iatom][ind].size() > 0 ){
                double max_val = *std::max_element(proj_vector[iatom][ind].begin(), proj_vector[iatom][ind].end());
                double min_val = *std::min_element(proj_vector[iatom][ind].begin(), proj_vector[iatom][ind].end());
                proj_coeff_max.push_back( std::max(std::fabs(max_val), std::fabs(min_val) ) );
            } else {
                proj_coeff_max.push_back(0.0);
            }
        }
        for(int i1 = 0; i1 < this -> proj_index[iatom].size(); ++i1){
            for(int i2 = 0; i2 < this -> proj_index[iatom].size(); ++i2){
                double matrix_coeff = 0.0;
                for(int s = 0; s < this -> spin_size; ++s){
                    if( abs(this -> nonlocal_coeff[s][iatom][i1][i2]) > matrix_coeff ){
                        matrix_coeff = this -> nonlocal_coeff[s][iatom][i1][i2];
                    }
                }
                if( abs(matrix_coeff * proj_coeff_max[i1] * proj_coeff_max[i2] ) > this -> matrix_cutoff * 1.0E-10 ){
                    int index_size = this -> proj_index[iatom][i2].size();
                    for(int a = 0; a < index_size; ++a){
                        vector<int> indexs;
                        int a_index = proj_index[iatom][i2][a];
                        if(kinetic_matrix->Map().MyGID(a_index)){
                            for(int b = 0; b < proj_index[iatom][i1].size(); ++b){
                                double tmp = matrix_coeff * proj_vector[iatom][i2][a] * proj_vector[iatom][i1][b];
                                if( abs(tmp) > this -> matrix_cutoff *1.0E-5 ){
                                    indexs.push_back(proj_index[iatom][i1][b]);
                                }
                            }
                            int ierr = graph -> InsertGlobalIndices(a_index, indexs.size(), &indexs[0]);
                            if(ierr != 0){
                                Verbose::all() << iatom <<  " ERROR: Nuclear_Potential_Matrix InsertGlobalIndices ierr = " << ierr << std::endl;
                                exit(EXIT_FAILURE);
                            }
                        }
                    }
                }
            }
        }
    }

    for (int i=0; i < NumMyElements; i++){
        double ierr = graph -> InsertGlobalIndices(MyGlobalElements[i], 1, &MyGlobalElements[i]);
        if(ierr != 0){
            Verbose::all() << "ERROR: Nuclear_Potential_Matrix InsertGlobalIndices (diagonal) ierr = " << ierr << std::endl;
            exit(EXIT_FAILURE);
        }
    }
    graph -> FillComplete();

    return 0;
}
