#include "Paw_Species.hpp"
#include <iostream>
#include <iomanip>
#include <cmath>

#include "../../Utility/Math/Spherical_Harmonics.hpp"
#include "../../Utility/Interpolation/Spline_Interpolation.hpp"
#include "../../Utility/Interpolation/Radial_Grid_Paw.hpp"
#include "../../Utility/String_Util.hpp"
#include "../../Utility/Verbose.hpp"
#include "../../Utility/Time_Measure.hpp"
#include "../../Utility/Math/Faddeeva.hpp"

#define VERBOSE_DEBUG (-1)

using std::min;
using std::max;
using std::abs;
using std::string;
using std::vector;
using Read::Read_Paw;
using Read::PAW_STATE;

Paw_Species::Paw_Species( string filename, bool is_xml ){
    this -> paw_data = Teuchos::rcp( new Read_Paw( filename, is_xml ) );
    this -> system_independent_calculation();
}

Paw_Species::Paw_Species( Teuchos::RCP<Read_Paw> paw_data ){
    this -> paw_data = Teuchos::rcp( new Read_Paw( *paw_data ) );
    this -> system_independent_calculation();
}

void Paw_Species::system_independent_calculation(){
    Teuchos::RCP<Time_Measure> timer = Teuchos::rcp(new Time_Measure());
    string timer_name = string("PAW initialization for ")+this -> paw_data -> filename;
    timer -> start(timer_name);

    if(VERBOSE_DEBUG >= 0) timer -> start("init");
    this -> integrated_to_i = this -> get_int_to_i_map();
    this -> index_to_l = this -> get_int_to_l_map();
    this -> index_to_m = this -> get_int_to_m_map();
    this -> lmax = 2*(*std::max_element( this -> index_to_l.begin(), this -> index_to_l.end() ) );
    //this -> lmax = 2;

    this -> radial_grid_id = this -> paw_data -> get_grid_types()[0];
    this -> compensation_hartree = this -> calculate_radial_hartree_from_comp_charge(this->radial_grid_id);
    if(VERBOSE_DEBUG >= 0) timer -> end("init");

    if(VERBOSE_DEBUG >= 0) timer -> start("delta");
    this -> delta_scalar = this -> calculate_delta();
    this -> delta_matrix = this -> calculate_delta_matrix();
    if(VERBOSE_DEBUG >= 0) timer -> end("delta");
    if(VERBOSE_DEBUG >= 0) timer -> start("dC scalar");
    this -> dE_scalar = this -> calculate_delta_E_scalar();
    if(VERBOSE_DEBUG >= 0) timer -> end("dC scalar");
    if(VERBOSE_DEBUG >= 0) timer -> start("dC matrix");
    this -> dE_matrix = this -> calculate_delta_E_matrix();
    if(VERBOSE_DEBUG >= 0) timer -> end("dC matrix");
    if(VERBOSE_DEBUG >= 0) timer -> start("dC tensor");
    this -> dE_tensor = this -> calculate_delta_E_tensor();
    if(VERBOSE_DEBUG >= 0) timer -> end("dC tensor");
    if(VERBOSE_DEBUG >= 0) timer -> start("zero correction");
    this -> zero_correction_scalar = this -> calculate_zero_correction_scalar();// fast (0s)
    this -> zero_correction_matrix = this -> calculate_zero_correction_matrix();// fast (0s)
    if(VERBOSE_DEBUG >= 0) timer -> end("zero correction");
    timer -> end(timer_name);

    Verbose::set_numformat(Verbose::Time);
    timer -> print(Verbose::single(Verbose::Normal));
    /*
    for(int L = 0; L < delta_matrix.size(); ++L){
        for(int M = -L; M <= L; ++M){
            for(int i = 0; i < this->delta_matrix[L][L+M].size(); ++i){
                for(int j = 0; j < this->delta_matrix[L][L+M].size(); ++j){
                    if( this -> delta_matrix[L][L+M][i][j] != this -> delta_matrix[L][L+M][j][i] ){
                        Verbose::single(Verbose::Detail) << "Delta matrix " << L << M << i << j << " not symmetric! " << this -> delta_matrix[L][L+M][i][j] << ", " << this -> delta_matrix[L][L+M][j][i] << std::endl;
                    }
                }
            }
        }
    }
    */
    /*
    if(Verbose::get_my_pid()==0){
        Verbose::all() << std::scientific;
        Verbose::all() << "delta_scalar = " << std::setprecision(5) << this -> delta_scalar << std::endl;
        Verbose::all() << "delta_matrix " << std::endl;
        for(int L = 0; L < delta_matrix.size(); ++L){
            Verbose::all() << "L = " << L << std::endl;
            for(int M = -L; M <= L; ++M){
                Verbose::all() << "M = " << M << std::endl;
                for(int i = 0; i < this->delta_matrix[L][L+M].size(); ++i){
                    for(int j = 0; j < this->delta_matrix[L][L+M].size(); ++j){
                        Verbose::all() << std::setprecision(5) << delta_matrix[L][L+M][i][j] << "\t";
                    }
                    Verbose::all() << std::endl;
                }
                Verbose::all() << std::endl;
            }
            Verbose::all() << std::endl;
        }
        //exit(0);
    }
// */
    /*
    int dH_size = dE_matrix.size();
    if(Parallel_Manager::info().get_total_mpi_rank()==0){
        Verbose::all() << "dC_val = " << std::setprecision(5) << dE_scalar << std::endl;
        Verbose::all() << "dC_mat " << std::endl;
        for(int i = 0; i < dH_size; ++i){
            for(int j = 0; j < dH_size; ++j){
                Verbose::all() << std::setprecision(5) << dE_matrix[i][j] << "\t";
            }
            Verbose::all() << std::endl;
        }
        Verbose::all() << std::endl;
        Verbose::all() << "dC_tens " << std::endl;
        for(int i = 0; i < dH_size; ++i){
            for(int j = 0; j < dH_size; ++j){
                for(int k = 0; k < dH_size; ++k){
                    for(int l = 0; l < dH_size; ++l){
                        Verbose::all() << std::setprecision(5) << dE_tensor[i][j][k][l] << "\t";
                    }
                    Verbose::all() << std::endl;
                }
                Verbose::all() << std::endl;
            }
            Verbose::all() << std::endl;
        }
        Verbose::all() << std::endl;
        exit(0);
    }
    // */

}

vector<int> Paw_Species::get_int_to_i_map(){
    vector<string> pw_types = paw_data -> get_partial_wave_types();
    vector<int> i_list;

    for(int i = 0; i < pw_types.size(); ++i){
        int l = paw_data -> get_partial_wave_state(i).l;
        for(int m = -l; m <= l; ++m ){
            i_list.push_back(i);
        }
    }
    return i_list;
}

vector<int> Paw_Species::get_int_to_l_map(){
    vector<string> pw_types = paw_data -> get_partial_wave_types();
    vector<int> l_list;

    for(int i = 0; i < pw_types.size(); ++i){
        int l = paw_data -> get_partial_wave_state(i).l;
        for(int m = -l; m <= l; ++m ){
            l_list.push_back(l);
        }
    }
    return l_list;
}

vector<int> Paw_Species::get_int_to_m_map(){
    vector<string> pw_types = paw_data -> get_partial_wave_types();
    vector<int> m_list;

    for(int i = 0; i < pw_types.size(); ++i){
        int l = paw_data -> get_partial_wave_state(i).l;
        for(int m = -l; m <= l; ++m ){
            m_list.push_back(m);
        }
    }
    return m_list;
}

int Paw_Species::get_integrated_index( int pw_state_no, int m ){
    int retval = 0;
    if( pw_state_no > this -> paw_data -> get_partial_wave_types().size() ){
        Verbose::all() << "Paw_Species::get_integrated_index out of range" << std::endl;
        exit(EXIT_FAILURE);
    }
    for(int i = 0; i < pw_state_no; ++i){
        retval += this -> paw_data -> get_partial_wave_state(i).l*2+1;
    }
    retval += this -> paw_data -> get_partial_wave_state(pw_state_no).l+m;
    return retval;
}

vector< vector<double> > Paw_Species::calculate_radial_hartree_from_comp_charge(
        string grid_id
){
    vector< vector<double> > hartree;

    vector<double> grid = this -> paw_data -> get_grid(grid_id);
    vector<double> dgrid = this -> paw_data -> get_grid_derivative( grid_id );

    vector<string> pw_type = this-> paw_data -> get_partial_wave_types();
    int lmax = this -> lmax;

    hartree.resize(lmax+1);

    for(int l = 0; l <= lmax; ++l){
        vector<double> comp_charge_r = this -> paw_data -> get_compensation_charge_on_grid(l, grid_id);
        hartree[l] = Radial_Grid::Paw::calculate_Hartree_potential_r( comp_charge_r, l, grid, dgrid );
    }
    return hartree;
}

vector< vector< vector< vector< double > > > > Paw_Species::calculate_delta_matrix(){
    vector< vector< vector< vector<double> > > > retval;
    //int state_no = this -> paw_data -> get_partial_wave_types().size();
    int state_no_m_included = this -> index_to_l.size();
    int lmax = this -> lmax;

    retval.resize(lmax+1);
    for(int L = 0; L <= lmax; ++L){
        retval[L].resize(2*L+1);
        for(int M = -L; M <= L; ++M){
            int M_ind = M+L;
            retval[L][M_ind].resize(state_no_m_included);

            for(int ind1 = 0; ind1 < state_no_m_included; ++ind1){
                retval[L][M_ind][ind1].resize(state_no_m_included);
            }

            for(int ind1 = 0; ind1 < state_no_m_included; ++ind1){
                for(int ind2 = 0; ind2 < state_no_m_included; ++ind2){
                    int i1 = this -> integrated_to_i[ind1];
                    int i2 = this -> integrated_to_i[ind2];
                    int m1 = this -> index_to_m[ind1];
                    int m2 = this -> index_to_m[ind2];
                    if( ind1 > ind2 ){
                        retval[L][M_ind][ind1][ind2] = retval[L][M_ind][ind2][ind1];
                    } else {
                        retval[L][M_ind][ind1][ind2] = this -> calculate_delta_matrix_element(L, M, i1, i2, m1, m2 );
                    }
                }//for ind1
            }//for ind2
        }//for M
    }//for L
    return retval;
}

vector< vector<double> > Paw_Species::calculate_delta_E_matrix(){
    vector< vector<double> > dE_matrix;
    int state_no_m_included = this -> index_to_l.size();

    dE_matrix.resize(state_no_m_included);
    for(int i = 0; i < state_no_m_included; ++i){
        dE_matrix[i].resize(state_no_m_included);
    }

    vector<double> grid = this -> paw_data -> get_grid( this -> radial_grid_id );
    vector<double> dgrid = this -> paw_data -> get_grid_derivative( this -> radial_grid_id );

    vector<double> ae_core_density = this -> get_all_electron_core_density_r();
    vector<double> smooth_core_density = this -> get_smooth_core_density_r();
    vector<double> g0 = this -> get_compensation_charge_r( 0 );// g00 = g0*Y00

    // ae (phi_i1 phi_i2 | nc)
    vector<double> ae_core_pot = Radial_Grid::Paw::calculate_Hartree_potential_r(ae_core_density, 0, grid, dgrid);
    // smooth (phi_i1 phi_i2 | nc)
    vector<double> ps_core_pot = Radial_Grid::Paw::calculate_Hartree_potential_r(smooth_core_density, 0, grid, dgrid);
    // Delta_a * smooth (phi_i1 phi_i2 | g00)
    //vector<double> g0_pot = Radial_Grid::Paw::calculate_Hartree_potential_r(g0, 0, grid, dgrid);
    vector<double> g0_pot = this -> compensation_hartree[0];

    double coul_ps_core_g00 = Radial_Grid::Paw::radial_integrate( g0, ps_core_pot, grid, dgrid, -1.0 );
    double coul_g00 = Radial_Grid::Paw::radial_integrate( g0, g0_pot, grid, dgrid, -1.0 );

    for(int ind1 = 0; ind1 < state_no_m_included; ++ind1){
        for(int ind2 = 0; ind2 < state_no_m_included; ++ind2){
            int i1 = this -> integrated_to_i[ind1];
            int i2 = this -> integrated_to_i[ind2];
            int l1 = this -> index_to_l[ind1];
            int l2 = this -> index_to_l[ind2];
            int m1 = this -> index_to_m[ind1];
            int m2 = this -> index_to_m[ind2];
            if( l1 != l2 or m1 != m2 ){
                continue;
            } else if( ind1 > ind2 ){
                dE_matrix[ind1][ind2] = dE_matrix[ind2][ind1];
            } else {
                dE_matrix[ind1][ind2] = this -> calculate_delta_E_matrix(i1, i2, m1, m2,
                        ae_core_pot, ps_core_pot, g0_pot, coul_ps_core_g00, coul_g00 );
            }
        }
    }

    return dE_matrix;
}

vector< vector< vector< vector<double> > > > Paw_Species::calculate_delta_E_tensor(){
    //Teuchos::RCP<Time_Measure> timer = Teuchos::rcp(new Time_Measure());
    //timer -> start("dC_ijkn init");
    vector< vector< vector< vector<double> > > > dE_tensor;
    int state_no = this -> paw_data -> get_partial_wave_types().size();
    int state_no_m_included = this -> index_to_l.size();

    dE_tensor.resize(state_no_m_included);
    for(int i = 0; i < state_no_m_included; ++i){
        dE_tensor[i].resize(state_no_m_included);
        for(int j = 0; j < state_no_m_included; ++j){
            dE_tensor[i][j].resize(state_no_m_included);
            for(int k = 0; k < state_no_m_included; ++k){
                dE_tensor[i][j][k].resize(state_no_m_included);
            }
        }
    }

    string grid_id = this -> radial_grid_id;
    vector<double> grid = this -> paw_data -> get_grid( grid_id );
    vector<double> dgrid = this -> paw_data -> get_grid_derivative( grid_id );
    vector< vector< vector<double> > > ae_pw2_dot, ps_pw2_dot;
    ae_pw2_dot.resize(state_no);
    ps_pw2_dot.resize(state_no);
    for(int i = 0; i < state_no; ++i){
        ae_pw2_dot[i].resize(state_no);
        ps_pw2_dot[i].resize(state_no);
    }

    for(int i = 0; i < state_no; ++i){
        for(int j = i; j < state_no; ++j){
            vector<double> ae_pw1 = this -> get_all_electron_partial_wave_r(i);
            vector<double> ae_pw2 = this -> get_all_electron_partial_wave_r(j);
            vector<double> smooth_pw1 = this -> get_smooth_partial_wave_r(i);
            vector<double> smooth_pw2 = this -> get_smooth_partial_wave_r(j);

            vector<double> ae_pw12_dot, smooth_pw12_dot;
            for(int r = 0; r < grid.size(); ++r ){
                ae_pw12_dot.push_back( ae_pw1[r]*ae_pw2[r] );
                smooth_pw12_dot.push_back( smooth_pw1[r]*smooth_pw2[r] );
            }
            ae_pw2_dot[i][j] = ae_pw12_dot;
            ae_pw2_dot[j][i] = ae_pw12_dot;
            ps_pw2_dot[i][j] = smooth_pw12_dot;
            ps_pw2_dot[j][i] = smooth_pw12_dot;
        }
    }

    int lmax = this -> lmax;
    ///< coul_ijgL (ps_phi_1 ps_phi_2 | g_L). Index: [i][j][l][l+m].
    vector< vector< vector< vector<double> > > > coul_ijgL;
    vector< vector<double> > coul_gL;
    coul_ijgL.resize(state_no);
    for(int i = 0; i < state_no; ++i){
        coul_ijgL[i].resize(state_no);
        for(int j = 0; j < state_no; ++j){
            coul_ijgL[i][j].resize( lmax+1 );
            for(int L = 0; L <= lmax; ++L){
                coul_ijgL[i][j][L].resize(2*L+1);
            }
        }
    }
    coul_gL.resize( lmax+1 );
    for(int L = 0; L <= lmax; ++L){
        coul_gL[L].resize(2*L+1);
    }
    //timer -> end("dC_ijkn init");// 0.0082 sec for Cu.

    double rc = -1.0;
    //timer -> start("dC_ijkn g_LL part");
    for(int L = 0; L <= lmax; ++L){
        vector<double> g_lm = this -> get_compensation_charge_r(L);
        for(int M = -L; M <= L; ++M){
            vector<double> g_lm_pot = this -> compensation_hartree.at(L);
            coul_gL[L][L+M] = Radial_Grid::Paw::radial_integrate(g_lm, g_lm_pot, grid, dgrid);
            for(int i = 0; i < state_no; ++i){
                for(int j = i; j < state_no; ++j){
                    //double rc = max( this -> get_partial_wave_state(i).cutoff_radius, this -> get_partial_wave_state(j).cutoff_radius )*4;
                    coul_ijgL[i][j][L][L+M] = Radial_Grid::Paw::radial_integrate(ps_pw2_dot[i][j], g_lm_pot, grid, dgrid);
                    coul_ijgL[j][i][L][L+M] = coul_ijgL[i][j][L][L+M];
                }
            }
        }
    }
    //timer -> end("dC_ijkn g_LL part");// 0.016 sec for Cu.

    //timer -> start("dC_ijkn valence part");
    vector< vector< vector< vector<double> > > > Hartree_ae_ij, Hartree_ps_ij;
    vector<double> zeros(grid.size());
    Hartree_ae_ij.resize(state_no);
    Hartree_ps_ij.resize(state_no);
    for(int i = 0; i < state_no; ++i){
        Hartree_ae_ij[i].resize(state_no);
        Hartree_ps_ij[i].resize(state_no);
        for(int j = 0; j < state_no; ++j){
            Hartree_ae_ij[i][j].resize( lmax+1 );
            Hartree_ps_ij[i][j].resize( lmax+1 );
            //double rc = max( this -> get_partial_wave_state(i).cutoff_radius, this -> get_partial_wave_state(j).cutoff_radius ) * 2;
            if( i > j ){
                Hartree_ae_ij[i][j] = Hartree_ae_ij[j][i];
                Hartree_ps_ij[i][j] = Hartree_ps_ij[j][i];
            } else {
                int l1 = this -> get_partial_wave_state( i ).l;
                int l2 = this -> get_partial_wave_state( j ).l;
                for(int L = 0; L <= lmax; ++L){
                    if( abs(l1-l2) <= L and L <= l1+l2 ){
                        Hartree_ae_ij[i][j][L] = Radial_Grid::Paw::calculate_Hartree_potential_r( ae_pw2_dot[i][j], L, grid, dgrid );
                        Hartree_ps_ij[i][j][L] = Radial_Grid::Paw::calculate_Hartree_potential_r( ps_pw2_dot[i][j], L, grid, dgrid );
                    }
                }
            }
        }
    }
    //timer -> end("dC_ijkn valence part");// 7.2 sec for Cu. Bottleneck.

    //timer -> start("dC_ijkn summation part");
    for(int ind1 = 0; ind1 < state_no_m_included; ++ind1){
        for(int ind2 = 0; ind2 < state_no_m_included; ++ind2){
            for(int ind3 = 0; ind3 < state_no_m_included; ++ind3){
                for(int ind4 = 0; ind4 < state_no_m_included; ++ind4){
                    int i1 = this -> integrated_to_i[ind1];
                    int i2 = this -> integrated_to_i[ind2];
                    int i3 = this -> integrated_to_i[ind3];
                    int i4 = this -> integrated_to_i[ind4];
                    int l1 = this -> index_to_l[ind1];
                    int l2 = this -> index_to_l[ind2];
                    int l3 = this -> index_to_l[ind3];
                    int l4 = this -> index_to_l[ind4];
                    int m1 = this -> index_to_m[ind1];
                    int m2 = this -> index_to_m[ind2];
                    int m3 = this -> index_to_m[ind3];
                    int m4 = this -> index_to_m[ind4];
                    if( abs(m1+m2) != abs(m3+m4) and abs(m1+m2) != abs(m3-m4) and abs(m1-m2) != abs(m3+m4) and abs(m1-m2) != abs(m3-m4) ){
                        continue;
                    } else if( l1+l2 < abs(l3-l4) or l3+l4 < abs(l1-l2) ){
                        continue;
                    } else if( m1<0 xor m2<0 xor m3<0 xor m4<0 ){
                        continue;// Skip calculation if there are odd number of m<0.
                    } else if( (l1 == 0 and (l2+l3+l4)%2 == 1) or (l2==0 and (l1+l3+l4)%2==1) or (l3==0 and (l1+l2+l4)%2==1) or (l4==0 and (l1+l2+l3)%2==1)){
                        continue;// Skip calculation if one l is zero and the sum of other l is odd.
                    } else if( ind1 > ind2 ){
                        if( ind3 > ind4 ){
                            dE_tensor[ind1][ind2][ind3][ind4] = dE_tensor[ind2][ind1][ind4][ind3];
                        } else {
                            dE_tensor[ind1][ind2][ind3][ind4] = dE_tensor[ind2][ind1][ind3][ind4];
                        }
                    } else {
                        if( ind3 > ind4 ){
                            dE_tensor[ind1][ind2][ind3][ind4] = dE_tensor[ind1][ind2][ind4][ind3];
                        } else {
                            if( ind1 >= ind3 && ind2 >= ind4 && (ind1 != ind3 || ind2 != ind4) ){
                                dE_tensor[ind1][ind2][ind3][ind4] = dE_tensor[ind3][ind4][ind1][ind2];
                            } else {
                                dE_tensor[ind1][ind2][ind3][ind4] = this -> calculate_delta_E_4tensor(
                                        i1, i2, i3, i4, m1, m2, m3, m4,
                                        ae_pw2_dot[i1][i2], ps_pw2_dot[i1][i2],
                                        Hartree_ae_ij[i3][i4], Hartree_ps_ij[i3][i4],
                                        coul_ijgL[i1][i2], coul_ijgL[i3][i4], coul_gL );
                            }
                        }
                    }
                }//for ind4
            }//for ind3
        }//for ind2
    }//for ind1
    //timer -> end("dC_ijkn summation part");
    //timer -> print(Verbose::single(Verbose::Detail));

    return dE_tensor;
}

double Paw_Species::calculate_delta(){
    vector<double> grid = this -> paw_data -> get_grid( this -> radial_grid_id );
    vector<double> dgrid = this -> paw_data -> get_grid_derivative( this -> radial_grid_id );

    vector<double> ae_core_density_r = this -> get_all_electron_core_density_r();
    vector<double> smooth_core_density_r = this -> get_smooth_core_density_r();

    vector<double> identity(grid.size(), 1.0);
    double retval = 0.0;

    retval = Radial_Grid::Paw::radial_integrate( ae_core_density_r, identity, grid, dgrid );
    retval -= Radial_Grid::Paw::radial_integrate( smooth_core_density_r, identity, grid, dgrid );

    retval -= static_cast<double>(this -> paw_data -> get_atom_number()) / sqrt(4*M_PI);
    return retval;
}

double Paw_Species::calculate_delta_matrix_element( int l, int m, int partial_wave1, int partial_wave2, int partial_wave_m1, int partial_wave_m2 ){
    vector<double> grid = this -> paw_data -> get_grid( this->radial_grid_id );
    vector<double> dgrid = this -> paw_data -> get_grid_derivative( this -> radial_grid_id );

    vector<double> ae_pw1 = this -> get_all_electron_partial_wave_r(partial_wave1);
    vector<double> ae_pw2 = this -> get_all_electron_partial_wave_r(partial_wave2);
    vector<double> smooth_pw1 = this -> get_smooth_partial_wave_r(partial_wave1);
    vector<double> smooth_pw2 = this -> get_smooth_partial_wave_r(partial_wave2);

    int partial_wave_l1 = this -> paw_data -> get_partial_wave_state( partial_wave1 ).l;
    int partial_wave_l2 = this -> paw_data -> get_partial_wave_state( partial_wave2 ).l;

    vector<double> mult_func, pow_func;

    mult_func.resize( grid.size() );
    pow_func.resize( grid.size() );
    for(int i = 0; i < grid.size(); ++i){
        mult_func[i] = ae_pw1[i] * ae_pw2[i] - smooth_pw1[i] * smooth_pw2[i];
        pow_func[i] = pow(grid[i], l);
    }

    double rad_val, ang_val;

    rad_val = Radial_Grid::Paw::radial_integrate( mult_func, pow_func, grid, dgrid );
    ang_val = Spherical_Harmonics::real_YYY_integrate(l, partial_wave_l1, partial_wave_l2, m, partial_wave_m1, partial_wave_m2);

    return rad_val*ang_val;
}


// Retval should be multiplied spherical harmonics
// Parameter function should be radial part
double Paw_Species::calculate_delta_E_scalar(){
    // GPAW.wfs.setups[i].M subtracts AE electrostatic energy.
    vector<double> grid = this -> paw_data -> get_grid( this->radial_grid_id );
    vector<double> dgrid = this -> paw_data -> get_grid_derivative( this -> radial_grid_id );

    vector<double> ae_core_density = this -> get_all_electron_core_density_r();
    vector<double> smooth_core_density = this -> get_smooth_core_density_r();
    vector<double> g0 = this -> paw_data -> get_compensation_charge_on_grid( 0, this -> radial_grid_id );// g00 = g0*Y00

    double retval = Radial_Grid::Paw::calculate_coulomb_integral( ae_core_density, ae_core_density, 0, 0, 0, 0, grid, dgrid );
    retval -= Radial_Grid::Paw::calculate_coulomb_integral( smooth_core_density, smooth_core_density, 0, 0, 0, 0, grid, dgrid );
    retval -= this->delta_scalar*this->delta_scalar * Radial_Grid::Paw::calculate_coulomb_integral( g0, g0, 0, 0, 0, 0, grid, dgrid );
    retval *= 0.5;

    retval -= this->delta_scalar * Radial_Grid::Paw::calculate_coulomb_integral( smooth_core_density, g0, 0, 0, 0, 0, grid, dgrid );
    retval -= this -> paw_data -> get_atom_number() * Radial_Grid::Paw::linear_integrate( ae_core_density, grid, grid, dgrid ) * sqrt(4*M_PI);// n/r * r**2. To be safe, calculate n * r
    return retval;
}

double Paw_Species::calculate_delta_E_matrix( int i1, int i2, int m1, int m2,
        vector<double> ae_core_pot, vector<double> ps_core_pot, vector<double> g0_pot,
        double coul_ps_core_g00, double coul_g00){
    vector<double> grid = this -> paw_data -> get_grid( this -> radial_grid_id );
    vector<double> dgrid = this -> paw_data -> get_grid_derivative( this -> radial_grid_id );

    vector<double> ae_pw1 = this -> get_all_electron_partial_wave_r(i1);
    vector<double> smooth_pw1 = this -> get_smooth_partial_wave_r(i1);
    vector<double> ae_pw2 = this -> get_all_electron_partial_wave_r(i2);
    vector<double> smooth_pw2 = this -> get_smooth_partial_wave_r(i2);

    //double rc = max( this -> get_partial_wave_state(i1).cutoff_radius, this -> get_partial_wave_state(i2).cutoff_radius )*4;
    double rc = -1.0;

    int l1 = this -> get_partial_wave_state( i1 ).l;
    int l2 = this -> get_partial_wave_state( i2 ).l;

    vector<double> ae_pw12_dot, smooth_pw12_dot;
    for(int i = 0; i < grid.size(); ++i ){
        ae_pw12_dot.push_back( ae_pw1[i]*ae_pw2[i] );
        smooth_pw12_dot.push_back( smooth_pw1[i]*smooth_pw2[i] );
    }

    vector<double> g0 = this -> get_compensation_charge_r( 0 );// g00 = g0*Y00
    double retval = 0.0;

    double YYY_val = Spherical_Harmonics::real_YYY_integrate( l1, l2, 0, m1, m2, 0 );
    //if( std::abs(YYY_val) > 1.0E-30 ){
        double tmpval = 0.0;
        // ae (phi_i1 phi_i2 | nc)
        //tmpval += Radial_Grid::Paw::calculate_coulomb_integral( ae_pw12_dot, ae_core_density, 0, 0, 0, 0, grid, dgrid, rc );
        tmpval += Radial_Grid::Paw::radial_integrate( ae_pw12_dot, ae_core_pot, grid, dgrid, rc );
        // smooth (phi_i1 phi_i2 | nc)
        //tmpval -= Radial_Grid::Paw::calculate_coulomb_integral( smooth_pw12_dot, smooth_core_density, 0, 0, 0, 0, grid, dgrid, rc );
        tmpval -= Radial_Grid::Paw::radial_integrate( smooth_pw12_dot, ps_core_pot, grid, dgrid, rc );
        // Delta_a * smooth (phi_i1 phi_i2 | g00)
        //tmpval -= delta_a * Radial_Grid::Paw::calculate_coulomb_integral( smooth_pw12_dot, g0, 0, 0, 0, 0, grid, dgrid, rc );
        tmpval -= this -> delta_scalar * Radial_Grid::Paw::radial_integrate( smooth_pw12_dot, g0_pot, grid, dgrid, rc );

        // Instead of multiplication of two spherical harmonics, use real_YYY_integrate
        retval += tmpval*YYY_val;
    //}

    // Z int d vect(r) phi_1 phi_2 / r
    if( l1 == l2 && m1 == m2 ){
        retval -= paw_data -> get_atom_number() * Radial_Grid::Paw::linear_integrate( ae_pw12_dot, grid, grid, dgrid, rc );
    }

    //retval -= this -> delta_matrix[0][0][this->get_integrated_index(i1,m1)][this->get_integrated_index(i2,m2)] * ( Radial_Grid::Paw::calculate_coulomb_integral( smooth_core_density, g0, 0, 0, 0, 0, grid, dgrid ) + delta_a*Radial_Grid::Paw::calculate_coulomb_integral( g0, g0, 0, 0, 0, 0, grid, dgrid ) );
    retval -= this -> delta_matrix[0][0][this->get_integrated_index(i1,m1)][this->get_integrated_index(i2,m2)]
                                                                            * ( coul_ps_core_g00 + this->delta_scalar*coul_g00 );

    return retval;
}

double Paw_Species::calculate_delta_E_4tensor(
        int state1, int state2, int state3, int state4,
        int m1, int m2,int m3, int m4,
        vector<double> ae_pw12_dot,
        vector<double> ps_pw12_dot,
        vector< vector<double> > Hartree_ae34,
        vector< vector<double> > Hartree_ps34,
        vector< vector<double> > coul_12gL,
        vector< vector<double> > coul_34gL,
        vector< vector<double> > coul_gL
){
    string grid_id = this -> radial_grid_id;
    vector<double> grid = this -> paw_data -> get_grid( grid_id );
    vector<double> dgrid = this -> paw_data -> get_grid_derivative( grid_id );

    int l1 = this -> get_partial_wave_state( state1 ).l;
    int l2 = this -> get_partial_wave_state( state2 ).l;
    int l3 = this -> get_partial_wave_state( state3 ).l;
    int l4 = this -> get_partial_wave_state( state4 ).l;

    double rc1 = this -> get_partial_wave_state( state1 ).cutoff_radius;
    double rc2 = this -> get_partial_wave_state( state2 ).cutoff_radius;
    double rc3 = this -> get_partial_wave_state( state3 ).cutoff_radius;
    double rc4 = this -> get_partial_wave_state( state4 ).cutoff_radius;

    //double rc = max( rc1, max( rc2, max(rc3, rc4) ) );
    double rc = -1.0;

    int index1 = this -> get_integrated_index(state1, m1);
    int index2 = this -> get_integrated_index(state2, m2);
    int index3 = this -> get_integrated_index(state3, m3);
    int index4 = this -> get_integrated_index(state4, m4);

    double retval = 0.0;
    int min_l = max(abs(l1-l2), abs(l3-l4));
    int max_l = min(min(l1+l2, l3+l4), this -> lmax);

    // deltaLi1i2 * ((gL)) * deltaLi3i4, deltaLi3i4 * (i1i2|gL), ...; Should satisfy both 2 conditions above to have non-zero value.
    for(int L = min_l; L <= max_l; ++L){
        for(int M = -L; M <= L; ++M){
            double tmpval = 0.0;
            double Yl1l2L = Spherical_Harmonics::real_YYY_integrate(l1, l2, L, m1, m2, M);
            double Yl3l4L = Spherical_Harmonics::real_YYY_integrate(l3, l4, L, m3, m4, M);

            // 0.5*(ae1ae2|ae3ae4) - 0.5*(ps1ps2|ps3ps4)
            if( std::abs(Yl1l2L) > 1.0E-30 and std::abs(Yl3l4L) > 1.0E-30){
                retval += 0.5 * Radial_Grid::Paw::radial_integrate( ae_pw12_dot, Hartree_ae34[L], grid, dgrid, rc ) * Yl1l2L * Yl3l4L;
                retval -= 0.5 * Radial_Grid::Paw::radial_integrate( ps_pw12_dot, Hartree_ps34[L], grid, dgrid, rc ) * Yl1l2L * Yl3l4L;
            }

            // deltaLi1i2 * ((gL)) * deltaLi3i4, deltaLi3i4 * (i1i2|gL), ...
            tmpval += 0.5*this -> delta_matrix[L][L+M][index3][index4] * coul_12gL[L][L+M] * Yl1l2L;
            tmpval += 0.5*this -> delta_matrix[L][L+M][index1][index2] * coul_34gL[L][L+M] * Yl3l4L;
            tmpval += 0.5*this -> delta_matrix[L][L+M][index1][index2] * this -> delta_matrix[L][L+M][index3][index4] * coul_gL[L][L+M];
            retval -= tmpval;
        }
    }
    return retval;
}

double Paw_Species::calculate_zero_correction_scalar(){
    vector<double> grid = this -> paw_data -> get_grid( this -> radial_grid_id );
    vector<double> dgrid = this -> paw_data -> get_grid_derivative( this -> radial_grid_id );

    vector<double> smooth_nc = this -> get_smooth_core_density_r();
    vector<double> zero_v = this -> get_zero_potential_r();

    // smooth_nc and zero_v has both angular part of Y_00 = 1/sqrt(4pi): canceled
    return - Radial_Grid::Paw::radial_integrate( smooth_nc, zero_v, grid, dgrid );
}

vector< vector<double> > Paw_Species::calculate_zero_correction_matrix(){
    vector<string> pw_type = this -> paw_data -> get_partial_wave_types();
    int dim = this -> index_to_l.size();
    vector< vector<double> > retval;

    retval.resize( dim );
    for(int i = 0; i < dim; ++i ){
        retval[i].resize( dim );
    }

    for(int i = 0; i < pw_type.size(); ++i){
        for(int j = 0; j < pw_type.size(); ++j){
            int l1 = paw_data -> get_partial_wave_state(i).l;
            int l2 = paw_data -> get_partial_wave_state(j).l;

            for(int m1 = -l1; m1 <= l1; ++m1){
                for(int m2 = -l2; m2 <= l2; ++m2){
                    int index1 = this -> get_integrated_index(i, m1);
                    int index2 = this -> get_integrated_index(j, m2);
                    retval[index1][index2] = this -> calculate_zero_correction_matrix_element(i, j, m1, m2);
                }
            }
        }
    }
    return retval;
}

double Paw_Species::calculate_zero_correction_matrix_element( int i, int j, int m1, int m2 ){
    vector<double> grid = this -> paw_data -> get_grid( this -> radial_grid_id );
    vector<double> dgrid = this -> paw_data -> get_grid_derivative( this -> radial_grid_id );

    vector<double> smooth_pw1 = this -> get_smooth_partial_wave_r(i);
    vector<double> smooth_pw2 = this -> get_smooth_partial_wave_r(j);
    vector<double> zero_v = this -> get_zero_potential_r();

    int l1 = this -> get_partial_wave_state(i).l;
    int l2 = this -> get_partial_wave_state(j).l;
    vector<double> multiplied_func;

    double retval = Spherical_Harmonics::real_YYY_integrate( l1, l2, 0, m1, m2, 0);
    if( abs(retval) < 1.0E-15 ){
        return 0.0;
    }

    multiplied_func.resize( grid.size() );
    for(int k = 0; k < grid.size(); ++k){
        multiplied_func[k] = smooth_pw1[k] * smooth_pw2[k];
    }

    retval *= -Radial_Grid::Paw::radial_integrate( multiplied_func, zero_v, grid, dgrid );
    return retval;
}

double Paw_Species::get_total_compensation_charge(int l){
    double charge = 0.0;

    vector<string> pw_type = this-> paw_data -> get_partial_wave_types();
    string grid_id = this -> radial_grid_id;
    vector<double> grid = this -> paw_data -> get_grid(grid_id);
    vector<double> dgrid = this -> paw_data -> get_grid_derivative(grid_id);

    vector<double> unitvect = vector<double>( grid.size(), 1.0 );

    vector<double> comp_charge_r = this -> paw_data -> get_compensation_charge_on_grid(l, grid_id);
    charge = Radial_Grid::Paw::radial_integrate( unitvect, comp_charge_r, grid, dgrid );
    return -charge;
}

Teuchos::RCP<Read_Paw> Paw_Species::get_read_paw(){
    return this->paw_data;
}

// From here, just returning calculated datas
double Paw_Species::get_kinetic_correction_scalar(){
    return this -> paw_data -> get_core_kinetic_energy();
}

vector< vector< double > > Paw_Species::get_kinetic_correction_matrix(){
    vector< vector<double> > retval;
    //int state_no = this -> paw_data -> get_partial_wave_types().size();
    int state_no_m_included = this -> index_to_l.size();

    retval.resize(state_no_m_included);

    for(int ind1 = 0; ind1 < state_no_m_included; ++ind1){
        retval[ind1].resize(state_no_m_included);
    }

    for(int ind1 = 0; ind1 < state_no_m_included; ++ind1){
        for(int ind2 = 0; ind2 < state_no_m_included; ++ind2){
            int i1 = this -> integrated_to_i[ind1];
            int i2 = this -> integrated_to_i[ind2];
            int m1 = this -> index_to_m[ind1];
            int m2 = this -> index_to_m[ind2];
            if( m1 == m2 ){
                retval[ind1][ind2] = this -> paw_data -> get_kinetic_energy_difference_matrix()[i1][i2];
            }
        }//for ind1
    }//for ind2
    return retval;
}


double Paw_Species::get_smooth_core_density_integral(){
    return (- this -> delta_scalar * sqrt(4*M_PI) - this -> paw_data -> get_atom_number() + this -> paw_data -> get_core_electron_number());
}

double Paw_Species::get_dE_scalar(){
    return this -> dE_scalar;
}

vector< vector<double> > Paw_Species::get_dE_matrix(){
    return this -> dE_matrix;
}

vector< vector< vector< vector<double> > > > Paw_Species::get_dE_tensor(){
    return this -> dE_tensor;
}

double Paw_Species::get_delta_scalar(){
    return this -> delta_scalar;
}

vector< vector< vector< vector<double> > > > Paw_Species::get_delta_matrix(){
    return this -> delta_matrix;
}

double Paw_Species::get_zero_correction_scalar(){
    return this -> zero_correction_scalar;
}

vector< vector<double> > Paw_Species::get_zero_correction_matrix(){
    return this -> zero_correction_matrix;
}

vector<double> Paw_Species::get_grid(){
    return this -> paw_data -> get_grid(this->radial_grid_id);
}

vector<double> Paw_Species::get_grid_derivative(){
    return this -> paw_data -> get_grid_derivative(this->radial_grid_id);
}

vector< vector<double> > Paw_Species::get_hartree_potential_of_compensation_charge(){
    return this -> compensation_hartree;
}

double Paw_Species::get_external_correction_scalar(
    vector< vector< vector<double> > > v_ext_in_L
){
    vector<double> grid = this -> get_grid();
    vector<double> dgrid = this -> get_grid_derivative();
    vector<double> ae_core_density = this -> get_all_electron_core_density_r();
    vector<double> ps_core_density = this -> get_smooth_core_density_r();
    vector<double> density_diff;

    for(int r = 0; r < grid.size(); ++r){
        density_diff.push_back( ae_core_density[r]-ps_core_density[r] );
    }

    double ext_core = Radial_Grid::Paw::radial_integrate( v_ext_in_L[0][0], density_diff, grid, dgrid );

    return ext_core;
}

vector< vector<double> > Paw_Species::get_external_correction_matrix(
    vector< vector< vector<double> > > v_ext_in_L
){
    vector<double> grid = this -> get_grid();
    vector<double> dgrid = this -> get_grid_derivative();
    vector<double> ae_core_density = this -> get_all_electron_core_density_r();
    vector<double> ps_core_density = this -> get_smooth_core_density_r();

    int state_no = this -> index_to_l.size();
    vector< vector<double> > ext_valence(state_no);
    for(int i1 = 0; i1 < state_no; ++i1){
        ext_valence[i1].resize(state_no);
        vector<double> ae_pw1 = this -> get_all_electron_partial_wave_r(this->integrated_to_i[i1]);
        vector<double> ps_pw1 = this -> get_smooth_partial_wave_r(this->integrated_to_i[i1]);
        int l1 = this -> index_to_l[i1];
        int m1 = this -> index_to_m[i1];
        for(int i2 = 0; i2 < state_no; ++i2){
            vector<double> ae_pw2 = this -> get_all_electron_partial_wave_r(this->integrated_to_i[i2]);
            vector<double> ps_pw2 = this -> get_smooth_partial_wave_r(this->integrated_to_i[i2]);
            int l2 = this -> index_to_l[i2];
            int m2 = this -> index_to_m[i2];

            vector<double> pwpw_diff;
            for(int r = 0; r < grid.size(); ++r){
                pwpw_diff.push_back( ae_pw1[r]*ae_pw2[r]-ps_pw1[r]*ps_pw2[r] );
            }
            int l_min = max( abs(l1-l2), abs(m1+m2) );
            for(int L = l_min; L <= l1+l2; ++L){
                ext_valence[i1][i2] += Radial_Grid::Paw::radial_integrate( v_ext_in_L[L][L+m1+m2], pwpw_diff, grid, dgrid );
            }
        }
    }
    return ext_valence;
}

// Core densities. Returning Read_Paw with cutoff considered
vector<double> Paw_Species::get_all_electron_core_density_r(){
    vector<double> retval = this->convert_to_my_grid( this -> paw_data -> get_all_electron_core_density_r() );

    double rc =  this -> paw_data -> get_core_density_cutoff();
    int r = 0;
    for(r = 0; r < this -> get_grid().size(); ++r){
        if( rc < this -> get_grid()[r] ){
            retval[r] = 0.0;
        }
    }
    return retval;
}
vector<double> Paw_Species::get_smooth_core_density_r(){
    vector<double> retval = this->convert_to_my_grid( this -> paw_data -> get_smooth_core_density_r() );
    double rc =  this -> paw_data -> get_core_density_cutoff();
    int r = 0;
    for(r = 0; r < this -> get_grid().size(); ++r){
        if( rc < this -> get_grid()[r] ){
            retval[r] = 0.0;
        }
    }
    return retval;
}

// From here, just returning Read_Paw values
int Paw_Species::get_atomic_number(){
    return this -> paw_data -> get_atom_number();
}

vector<double> Paw_Species::get_zero_potential_r(){
    return this->convert_to_my_grid( this -> paw_data -> get_zero_potential_r() );
}

vector<string> Paw_Species::get_partial_wave_types(){
    return this -> paw_data -> get_partial_wave_types();
}
vector<double> Paw_Species::get_all_electron_partial_wave_r( string wave_state ){
    return this->convert_to_my_grid( this -> paw_data -> get_all_electron_partial_wave_r(wave_state) );
}
vector<double> Paw_Species::get_smooth_partial_wave_r( string wave_state ){
    return this->convert_to_my_grid( this -> paw_data -> get_smooth_partial_wave_r(wave_state) );
}
vector<double> Paw_Species::get_projector_function_r( string wave_state ){
    return this->convert_to_my_grid( this -> paw_data -> get_projector_function_r(wave_state) );
}
vector<double> Paw_Species::get_all_electron_partial_wave_r( int wave_state ){
    return this->convert_to_my_grid( this -> paw_data -> get_all_electron_partial_wave_r(wave_state) );
}
vector<double> Paw_Species::get_smooth_partial_wave_r( int wave_state ){
    return this->convert_to_my_grid( this -> paw_data -> get_smooth_partial_wave_r(wave_state) );
}
vector<double> Paw_Species::get_projector_function_r( int wave_state ){
    return this->convert_to_my_grid( this -> paw_data -> get_projector_function_r(wave_state) );
}
PAW_STATE Paw_Species::get_partial_wave_state( string wave_state ){
    return this -> paw_data -> get_partial_wave_state(wave_state);
}
PAW_STATE Paw_Species::get_partial_wave_state( int wave_state ){
    return this -> paw_data -> get_partial_wave_state(wave_state);
}

vector<double> Paw_Species::get_all_electron_core_KE_density_r(){
    return this->convert_to_my_grid( this -> paw_data -> get_all_electron_core_KE_density_r() );
}
vector<double> Paw_Species::get_smooth_core_KE_density_r(){
    return this->convert_to_my_grid( this -> paw_data -> get_smooth_core_KE_density_r() );
}

double Paw_Species::get_EXX_core(){
    return this -> paw_data -> get_EXX_core();
}
vector< vector<double> > Paw_Species::get_EXX_matrix(){
    return this -> paw_data -> get_EXX_matrix();
}

int Paw_Species::get_xc_type(){
    return this -> paw_data -> get_xc_type();
}
vector<int> Paw_Species::get_xc_functionals(){
    return this -> paw_data -> get_xc_functionals();
}

double Paw_Species::get_cutoff_radius(){
    return this -> paw_data -> get_cutoff_radius();
}

double Paw_Species::get_paw_radius(){
    return this -> paw_data -> get_paw_radius();
}

double Paw_Species::get_projector_cutoff(){
    return this -> paw_data -> get_projector_cutoff();
}

double Paw_Species::get_partial_wave_cutoff(){
    return this -> paw_data -> get_partial_wave_cutoff();
}

vector<double> Paw_Species::get_compensation_charge_r( int l ){
    return this -> paw_data -> get_compensation_charge_on_grid( l, this->radial_grid_id );
}

vector<double> Paw_Species::convert_to_my_grid(
    std::pair< string, vector<double> > data
){
    if( radial_grid_id == data.first ){
        return data.second;
    }
    vector<double> src_grid = this -> paw_data -> get_grid( data.first );
    vector<double> dest_grid = this -> paw_data -> get_grid( this->radial_grid_id );
    return Radial_Grid::Paw::change_radial_grid( data.second, src_grid, dest_grid);
}

int Paw_Species::get_lmax(){
    return this -> lmax;
}

double Paw_Species::get_compensation_charge_r(double r, int l){
    return this -> paw_data -> get_radial_compensation_charge(r, l);
}

double Paw_Species::get_compensation_potential_r(double r, int l){
    vector<double> grid = this -> get_grid();
    vector<double> dgrid = this -> get_grid_derivative();
    vector<double> gl = this -> get_compensation_charge_r(l);
    return Radial_Grid::Paw::calculate_Hartree_potential_r(gl, r, l, grid, dgrid);
}

double Paw_Species::get_compensation_potential_analytic_r(double r, int l){
    int type = this -> paw_data -> get_shape_function_type();
    double rc = this -> paw_data -> get_shape_function_rc();
    const double sqrt_two = sqrt(2);
    if(type == 1){
        // Normalization coefficient * kernel coefficient [4pi/(2l+1)].
        double const_term = 1./sqrt(4*M_PI)*String_Util::factorial(l)/String_Util::factorial(2*l+1)*pow(2./rc, 2*l+3) * 4*M_PI/(2*l+1);
        /*
         * lesser term = 4PI/(2l+1) \int R(r') r ' * (r'/r)^(l+1) dr' = 4PI/(2l+1)/r^(l+1) \int r'^(2l+2) exp(r'/rc)^2 dr'
         */
        double lesser_term_sum_ = 0;
        for(int i = 0; i <= l; ++i){
            lesser_term_sum_ += String_Util::double_factorial(2*l+1)/String_Util::double_factorial(2*i+1)*pow(sqrt_two*r/rc, 2*i+1);
        }
        //double lesser_term = 0.0;
        double lesser_term = pow(rc/sqrt_two, 2*l+3) * ( -lesser_term_sum_ * exp(-r*r/rc/rc) + sqrt(M_PI/2) * String_Util::double_factorial(2*l+1) * Faddeeva::erf(r/rc) );
        //if(r > 1.0E-10){
            //lesser_term = pow(r, -l-1) * pow(rc/sqrt_two, 2*l+3) * ( -lesser_term_sum_ * exp(-r*r/rc/rc) + sqrt(M_PI/2) * String_Util::double_factorial(2*l+1) * Faddeeva::erf(r/rc) );
            //lesser_term = pow(rc/sqrt_two, 2*l+3) * ( -lesser_term_sum_ * pow(r, -l-1) * exp(-r*r/rc/rc) + sqrt(M_PI/2) * String_Util::double_factorial(2*l+1) * pow(r, -l-1) * Faddeeva::erf(r/rc) );
        //}
        if(std::abs(lesser_term) > 1.0E-10){
            lesser_term *= pow(r, -l-1);
        } else {
            lesser_term = 0;
        }

        /*
         * greater term = 4PI/(2l+1) \int R(r') r ' * (r/r')^l dr' = 4PI/(2l+1)*r^l \int r' exp(r'/rc)^2 dr'
         */
        double greater_term = pow(r, l) * rc*rc/2 * exp(-r*r/rc/rc);
        /*
        if(l%2 == 1){
            int k = (l-1)/2;
            double greater_term_sum_ = 0;
            for(int i = 0; i <= k; ++i){
                greater_term_sum_ += String_Util::double_factorial(2*k)/String_Util::double_factorial(2*i)*pow(sqrt_two*r/rc, 2*i);
            }
            greater_term = - r * pow(2., -(2*k+1.)/2.) * greater_term_sum_ * exp(-r*r/rc/rc);
        } else {
            int k = l/2-1;
            double greater_term_sum_ = 0;
            for(int i = 0; i <= k; ++i){
                greater_term_sum_ += String_Util::double_factorial(2*k+1)/String_Util::double_factorial(2*i+1)*pow(sqrt_two*r/rc, 2*i+1);
            }
            greater_term = - r * pow(2., -(k+1)) * greater_term_sum_ * exp(-r*r/rc/rc) + sqrt(2*M_PI)/pow(2, k+2) * (1+Faddeeva::erf(r));
        }
        */
        //std::cerr << rc << ", " << l << ", " << r << std::endl;
        //std::cerr << const_term << ", " << lesser_term << ", " << greater_term << std::endl;
        //throw std::logic_error("get_compensation_potential_analytic_r: Not implemented for this type!");
        return const_term*(lesser_term + greater_term);
    }
    throw std::logic_error("get_compensation_potential_analytic_r: Not implemented for this type!");
    return 0.0;
}
