#include "Nuclear_Potential.hpp"
#include "Upf2KB.hpp"
#include "Hgh2KB.hpp"
#include "../../Io/Periodic_table.hpp"
#include "../../Utility/Parallel_Manager.hpp"
#include "../../Utility/ParamList_Util.hpp"

using std::vector;
using std::string;
using Teuchos::Array;
using Teuchos::RCP;
using Teuchos::rcp;

Nuclear_Potential::Nuclear_Potential(RCP<const Basis> basis, RCP<const Atoms> atoms, RCP<Teuchos::ParameterList> parameters){
    this -> basis = basis;
    this -> atoms = atoms;
    this -> spin_size = parameters -> sublist("BasicInformation").get<int>("Polarize")+1;

    this -> kbprojector = Teuchos::null;
#ifdef USE_PAW
    this -> paw = Teuchos::null;
#endif

    if(!parameters->sublist("BasicInformation").sublist("Pseudopotential").isParameter("PSFilenames")){
        vector<int> atomic_numbers = atoms -> get_atom_types();
        //vector<string> atomic_symbols;
        Array<string> ps_filenames;
        string filepath = parameters -> sublist("BasicInformation").sublist("Pseudopotential").get<string>("PSFilePath", string(""));
        string filesuffix = parameters -> sublist("BasicInformation").sublist("Pseudopotential").get<string>("PSFileSuffix", string(""));
        /*
        for(int ia = 0; ia < atomic_numbers.size(); ++ia){
            string atom_symbol = Periodic_atom::periodic_table[atomic_numbers[ia]-1];
            transform(atom_symbol.begin()+1, atom_symbol.end(), atom_symbol.begin()+1, ::tolower);
            ps_filenames.push_back( filepath + "/" + atom_symbol + filesuffix );
        }
        */
        ps_filenames = ParamList_Util::PPnames_from_path(atomic_numbers, filepath, filesuffix);
        parameters -> sublist("BasicInformation").sublist("Pseudopotential").set("PSFilenames", ps_filenames);
    }

    this -> type = parameters->sublist("BasicInformation").sublist("Pseudopotential").get<int>("Pseudopotential");

    if(basis -> get_periodicity() > 0){
        if(this -> type != 3){
            throw std::runtime_error("PBC only works with PAW");
        }
        parameters -> sublist("BasicInformation").sublist("Pseudopotential").set<int>("HartreePotentialMethod", 2);
    }
    if(this -> type == 1){
        string format = parameters->sublist("BasicInformation").sublist("Pseudopotential").get<string>("Format");
        if(format == "upf"){
            this -> kbprojector = rcp(new Upf2KB(basis, atoms, parameters) );
        } else if(format == "hgh"){
            this -> kbprojector = rcp(new Hgh2KB(basis, atoms, parameters) );
        } else {
            Verbose::all() << "Core_Hamiltonian::construct_matrix - Wrong pseudopotential format: only upf & hgh formats are available." << std::endl;
            exit(EXIT_FAILURE);
        }
        if(parameters->sublist("BasicInformation").sublist("Pseudopotential").isParameter("OptimizeVectors")){
            if(parameters->sublist("BasicInformation").sublist("Pseudopotential").get<int>("OptimizeVectors") == 1){
                this -> optimize_pp_vectors = true;
            }
        }
    }
#ifdef USE_PAW
    else if(this -> type == 3){
        if(parameters -> sublist("BasicInformation").sublist("Pseudopotential").get<string>("Format") == "xml"){
           this -> paw = rcp( new Paw(basis, atoms, parameters) );
        } else {
            Verbose::all() << "Core_Hamiltonian::construct_matrix - Wrong pseudopotential format: only xml formats are available." << std::endl;
            exit(EXIT_FAILURE);
        }
    }
#endif
    else {
        Verbose::all() << "Unsupported Nuclear potential type = " << this -> type << std::endl;
        exit(EXIT_FAILURE);
    }
    this->timer = Teuchos::rcp(new Time_Measure());
}

Nuclear_Potential::~Nuclear_Potential(){
    Verbose::set_numformat(Verbose::Time);
    Verbose::single(Verbose::Detail) << "Nuclear_Potential time profile:" << std::endl;
    this -> timer -> print(Verbose::single(Verbose::Detail));
}

std::string Nuclear_Potential::get_type(){
    if( this -> type == 1 ){
        return "KBprojector";
    }
#ifdef USE_PAW
    else if(this -> type == 3 ){
        return "PAW";
    }
#endif
    return "ERROR";
}

int Nuclear_Potential::initialize_matrix(RCP<Nuclear_Potential_Matrix> &npm){
    if( this -> type == 1 ){
        this -> kbprojector -> compute_nonlinear_core_correction();
    }
#ifdef USE_PAW
    else if( this -> type == 3 ){
        this -> paw -> initialize_PAW_D_matrix();
        if(Verbose::get_verbose_level() > 1){
            this -> paw -> print_PAW_D_matrix();
        }
    }
#endif
    this -> set_static_local_pot_to_matrix( npm );
    this -> set_nonlocal_pot_proj_to_matrix( npm );
    if( this -> type == 1 ){
        this -> set_static_nonlocal_pot_coeff_to_matrix( npm );
        if(this -> optimize_pp_vectors) this -> kbprojector -> clear_vector();
    }
#ifdef USE_PAW
    else if( this -> type == 3 ){
        //RCP<Epetra_Vector> hartree_potential = rcp( new Epetra_Vector( *this -> basis -> get_map() ) );
        //this -> paw -> hartree_vector_update( hartree_potential );
        vector< vector< vector< vector<double> > > > nonlocal_coeff;
        this -> paw -> calculate_hamiltonian_correction_coeff( nonlocal_coeff );
        npm -> set_nonlocal_pot_coeff( nonlocal_coeff );
    }
#endif
    this -> is_mixed = true;
    return this -> type;
}

vector<double> Nuclear_Potential::get_Zvals(){
    if( this -> type == 1 ){
        return this -> kbprojector -> get_Zvals();
    }
#ifdef USE_PAW
    else if( this -> type == 3 ){
        vector<double> Zvals;
        //for(int i = 0; i < this -> atoms -> get_size(); ++i){
            //Zvals.push_back( this -> paw -> get_paw_atom(i) -> get_paw_species() -> get_read_paw() -> get_valence_electron_number() );
        for(int i = 0; i < this -> atoms -> get_num_types(); ++i){
            Zvals.push_back( this -> paw -> get_paw_species(i) -> get_read_paw() -> get_valence_electron_number() );
        }
        return Zvals;
    }
#endif
    return vector<double>();
}

vector<double> Nuclear_Potential::get_Zeffs(){
    if( this -> type == 1 ){
        auto types = atoms->get_atom_types();
        auto Z_vals = kbprojector -> get_Zvals();
        std::vector<double> return_val;
        int i,j;
//        for (int i=0; i<Z_vals.size(); i++){
//            std::cout << "\t" <<Z_vals[i] <<std::endl;
//        }
        for (i=0; i< atoms->get_size(); i++){
            for (j=0; j<types.size(); j++){
                if (types[j]==atoms->get_atomic_numbers()[i])
                    break;
            }
            return_val.push_back( Z_vals.at(j) ) ;
        }
        return return_val;
//        return this -> kbprojector -> get_Zvals();// Should resize to atom index
    }
#ifdef USE_PAW
    else if( this -> type == 3 ){
        vector<double> Zeffs = this -> paw -> get_total_compensation_charges();
        for(int i = 0; i < Zeffs.size(); ++i){
            Zeffs[i] = -Zeffs[i];
        }
        return Zeffs;
    }
#endif
    return vector<double>();
}

int Nuclear_Potential::get_overlap_matrix( RCP<Epetra_CrsMatrix> &overlap_matrix){
#ifdef USE_PAW
    if( this -> type == 3 ){
        this -> paw -> get_overlap_matrix( overlap_matrix );
    }
#endif
    return this -> type;
}

int Nuclear_Potential::get_pseudo_core_electron_info(Array< RCP<Epetra_Vector> >& density, Array< RCP<Epetra_MultiVector> >& density_grad){
    if( this -> type == 1 ){
        this -> kbprojector -> get_core_electron_info( density, density_grad );
    }
#ifdef USE_PAW
    else if( this -> type == 3){
        density = this -> paw -> get_atomcenter_core_density(false);
        vector<std::array<double,3> > atom_pos = this -> atoms -> get_positions();
        int spin_size = density.size();
        density_grad.clear();
        for(int s = 0; s < spin_size; ++s){
            density_grad.push_back( rcp( new Epetra_MultiVector( *this -> basis -> get_map(), 3 ) ) );
        }

        for(int i = 0; i < atom_pos.size(); ++i){
            Array< RCP<Epetra_MultiVector> > grad = this -> paw -> get_paw_atom(i)->get_core_density_grad_vector(false, this -> basis);
            for(int s = 0; s < spin_size; ++s){
                density_grad[s] -> Update(1.0, *grad[s], 1.0);
            }
        }
    }
#endif
    return this -> type;
}

int Nuclear_Potential::mix_nuclear_potential( vector<double> coeffs, double alpha, int ispin ){
#ifdef USE_PAW
    if( this -> type == 3 ){
        this -> paw -> density_matrix_mixing( coeffs, alpha, ispin );
        Verbose::single(Verbose::Detail) << "PAW density matrix mixing DONE" << std::endl;
        for(int i = 0; i < coeffs.size(); ++i){
            Verbose::single(Verbose::Detail) << coeffs[i] << "\t";
        }
        Verbose::single(Verbose::Detail) << alpha << std::endl;
        Verbose::single(Verbose::Detail) << "PAW density matrix after mixing:" << std::endl;
        if(Verbose::get_verbose_level() > 1){
            this -> paw -> print_PAW_D_matrix();
        }
    }
#endif
    this -> is_mixed = true;
    return this -> type;
}

int Nuclear_Potential::get_initial_atomic_density_matrix( vector< vector<double> > &descriptor ){
    descriptor.clear();
#ifdef USE_PAW
    if( this -> type == 3 ){
        vector<std::array<double,3> > positions = this -> atoms -> get_positions();
        vector< Array< Teuchos::SerialDenseMatrix<int,double> > > D_matrixes( positions.size() );

        for(int i = 0; i < positions.size(); ++i ){
            D_matrixes[i] = this -> paw -> get_paw_atom(i) -> get_one_center_initial_density_matrix();
        }

        descriptor.resize(this -> spin_size);

        for(int s = 0; s < this -> spin_size; ++s){
            for(int ia = 0; ia < D_matrixes.size(); ++ia){
                int proj_size = D_matrixes[ia][s].numRows();
                for(int i = 0; i < proj_size; ++i){
                    for(int j = 0; j < proj_size; ++j){
                        descriptor[s].push_back(D_matrixes[ia][s](i,j));
                    }
                }
            }
        }
    }
#endif
    return this -> type;
}

int Nuclear_Potential::get_atomic_density_matrix( vector< vector<double> > &descriptor ){
    descriptor.clear();
#ifdef USE_PAW
    if( this -> type == 3 ){
        vector< Array< Teuchos::SerialDenseMatrix<int, double> > > D_matrixes = this -> paw -> get_density_matrixes();
        descriptor.resize(this -> spin_size);

        for(int s = 0; s < this -> spin_size; ++s){
            for(int ia = 0; ia < D_matrixes.size(); ++ia){
                int proj_size = D_matrixes[ia][s].numRows();
                for(int i = 0; i < proj_size; ++i){
                    for(int j = 0; j < proj_size; ++j){
                        descriptor[s].push_back(D_matrixes[ia][s](i,j));
                    }
                }
            }
        }
    }
#endif
    return this -> type;
}

int Nuclear_Potential::set_atomic_density_matrix( vector< vector<double> > descriptor ){
#ifdef USE_PAW
    if( this -> type == 3 ){
        vector< Array< Teuchos::SerialDenseMatrix<int, double> > > D_matrixes = this -> paw -> get_density_matrixes();

        for(int s = 0; s < this -> spin_size; ++s){
            int index = 0;
            for(int ia = 0; ia < D_matrixes.size(); ++ia){
                int proj_size = D_matrixes[ia][s].numRows();
                for(int i = 0; i < proj_size; ++i){
                    for(int j = 0; j < proj_size; ++j){
                        D_matrixes[ia][s](i,j) = descriptor[s][index];
                        index++;
                    }
                }
            }
        }
        this -> paw -> set_density_matrixes( D_matrixes );
    }
#endif
    return this -> type;
}

int Nuclear_Potential::get_hartree_potential_correction(
    //Array< RCP<const Occupation> > occupations,
    //Array< RCP<const Epetra_MultiVector> > orbitals,
    Array< RCP<Epetra_Vector> > density,
    RCP<Epetra_Vector> &hartree_potential
){
#ifdef USE_PAW
//    this -> timer -> start("Hartree potential correction");
    if( this -> type == 3){
        //if( !is_mixed ){
        //    this -> paw -> update_PAW_D_matrix(occupations, orbitals);
        //}
        this -> paw -> hartree_vector_update( density, hartree_potential );
        //this -> paw -> hartree_vector_update( hartree_potential );
    }
#endif
//    this -> timer -> end("Hartree potential correction");
//    Verbose::single(VERBOSE_DEBUG) << "Hartree potential correction time: " << this -> timer -> get_elapsed_time("Hartree potential correction", -1) << "s" << std::endl;
    return this -> type;
}

int Nuclear_Potential::get_energy_correction(
    Array< RCP<const Occupation> > occupations,
    Array< RCP<const Epetra_MultiVector> > orbitals,
    RCP<Epetra_Vector> &hartree_vector,
    double &hartree_energy,
    double &int_n_vxc,
    double &x_energy,
    double &c_energy,
    double &kinetic_energy,
    std::vector<double> &kinetic_energies,
    double &numerical_correction,
    double &external_energy
){
#ifdef USE_PAW
//    this -> timer -> start("Energy correction");
    if( this -> type == 3 ){
        //if( !is_mixed ){
        //    this -> paw -> update_PAW_D_matrix(occupations, orbitals);
        //}

        for(int ispin = 0; ispin < occupations.size(); ++ispin){
            int orbital_size = orbitals[ispin] -> NumVectors();
            RCP<const Epetra_Vector> local_potential = paw -> get_zero_potential();
            RCP<Epetra_Vector> density = rcp( new Epetra_Vector( orbitals[ispin] -> Map() ) );
            double scaling = this -> basis -> get_scaling()[0] * this -> basis -> get_scaling()[1] * this -> basis -> get_scaling()[2];
            double Result = 0.0;

            for(int i = 0; i < orbital_size; ++i){
                density -> Multiply( occupations[ispin] -> operator[](i), *orbitals[ispin] -> operator()(i), *orbitals[ispin] -> operator()(i), 1.0 );
            }
            local_potential -> Dot(*density, &Result);

            Array< RCP<Epetra_Vector> > core_density;
            Array< RCP<Epetra_MultiVector> > core_density_grad;
            this -> get_pseudo_core_electron_info(core_density, core_density_grad);

            double Result_core = 0.0;
            local_potential -> Dot(*core_density[ispin], &Result_core);
            numerical_correction += Result+Result_core * scaling;
        }

        this -> paw -> get_energy_correction(
            occupations, orbitals, hartree_vector,
            hartree_energy, int_n_vxc, x_energy, c_energy, kinetic_energy,
            kinetic_energies, numerical_correction, external_energy);
    }
#endif
//    this -> timer -> end("Energy correction");
//    Verbose::single(VERBOSE_DEBUG) << this -> timer -> get_elapsed_time("Energy correction", -1)  << "s" << std::endl;

    return this -> type;
}

/*
int Nuclear_Potential::get_density_for_hartree_potential(
    Array< RCP<Epetra_Vector> > in_density,
    Array< RCP<Epetra_Vector> > &out_density
){
    out_density.resize( in_density.size() );
    for(int s = 0; s < in_density.size(); ++s){
        out_density[s] = rcp( new Epetra_Vector( *in_density[s] ) );
    }

    if( this -> type == 3 ){
        *
        Array< RCP<Epetra_Vector> > core_density = this -> paw -> get_atomcenter_core_density( false );
        for(int s = 0; s < in_density.size(); ++s){
            out_density[s] -> Update( 1.0, *core_density[s], 1.0 );
        }
//        *
        Verbose::single(Verbose::Detail) << "NP line 256: core density not added to calculate hartree vector" << std::endl;
    }
    return this -> type;
}
*/
int Nuclear_Potential::update(
    Array< RCP<const Occupation> > occupations,
    Array< RCP<const Epetra_MultiVector> > orbitals
){

#ifdef USE_PAW
    if( this -> type == 3 ){
        this -> paw -> update_PAW_D_matrix(occupations, orbitals);
    }
#endif
    this -> is_mixed = false;
    return this -> type;
}

int Nuclear_Potential::get_density_correction(
    Array< RCP<const Occupation> > occupations,
    Array< RCP<const Epetra_MultiVector> > orbitals,
    Array< RCP<Epetra_Vector> > &density
){
#ifdef USE_PAW
    if( this -> type == 3 ){
        this -> paw -> update_PAW_D_matrix( occupations, orbitals );
        this -> paw -> get_density_correction(density);
    }
#endif
    return this -> type;
}

int Nuclear_Potential::get_orbital_correction(
    Array< RCP<const Occupation> > occupations,
    Array< RCP<Epetra_MultiVector> > &orbitals
){
#ifdef USE_PAW
    if( this -> type == 3 ){
        Array< RCP<const Epetra_MultiVector> > tmp_orbitals;
        for(int s = 0; s < orbitals.size(); ++s){
            tmp_orbitals.push_back( Teuchos::rcp_const_cast<const Epetra_MultiVector>( orbitals[s] ) );
        }
        this -> paw -> update_PAW_D_matrix( occupations, tmp_orbitals );
        this -> paw -> get_orbital_correction(orbitals);
    }
#endif
    return this -> type;
}

int Nuclear_Potential::calculate_external_energy(
    Array< RCP<Occupation> > occupations,
    Array< RCP<Epetra_MultiVector> > orbitals,
    RCP<Nuclear_Potential_Matrix> npm,
    double &energy
){
    energy=0.0;
    for (int ispin = 0; ispin < occupations.size(); ++ispin){
        int orbital_size = orbitals[ispin] -> NumVectors();
        if(this -> type == 1){
            RCP<Epetra_MultiVector> external_tmp = rcp( new Epetra_MultiVector(orbitals[ispin] -> Map(), orbital_size) );
            npm -> multiply(ispin, orbitals[ispin], external_tmp);
            /*
            RCP<Epetra_MultiVector> external_tmp = rcp(new Epetra_MultiVector(orbitals[ispin]->Map(), orbitals[ispin]->NumVectors() ));
            RCP<Epetra_CrsMatrix> pp_matrix = rcp( new Epetra_CrsMatrix(Copy, *this->basis->get_map(), 0) );
            npm -> add(ispin, pp_matrix, 1.0);
            pp_matrix -> FillComplete();
            pp_matrix->Multiply(false,*orbitals[ispin],*external_tmp);
            */

            double* Result = new double[orbital_size];
            external_tmp->Dot(*orbitals[ispin], Result);
            //std::cout << *orbitals[ispin] << std::endl;
            for(int i = 0; i < orbital_size; ++i){
                energy += Result[i] * occupations[ispin] -> operator[](i);
            }
            delete[] Result;
        } else if( this -> type == 3){
            /*
            RCP<const Epetra_Vector> local_potential = npm -> get_local_potential();
            RCP<Epetra_Vector> density = rcp( new Epetra_Vector( orbitals[ispin] -> Map() ) );
            double scaling = this -> basis -> get_scaling()[0] * this -> basis -> get_scaling()[1] * this -> basis -> get_scaling()[2];
            double Result = 0.0;

            for(int i = 0; i < orbital_size; ++i){
                density -> Multiply( occupations[ispin] -> operator[](i), *orbitals[ispin] -> operator()(i), *orbitals[ispin] -> operator()(i), 1.0 );
            }
            local_potential -> Dot(*density, &Result);

            Array< RCP<Epetra_Vector> > core_density;
            Array< RCP<Epetra_MultiVector> > core_density_grad;
            this -> get_pseudo_core_electron_info(core_density, core_density_grad);

            double Result_core = 0.0;
            local_potential -> Dot(*core_density[ispin], &Result_core);
            energy += Result+Result_core * scaling;
            */
        }
    }
    return this -> type;
}

int Nuclear_Potential::calculate_nuclear_repulsion(double &nuclear_nuclear_repulsion){
    if(this -> type == 1){
        vector<double> Zval = this -> kbprojector -> get_Zvals();
        double ion_ion = 0.0;
        for(int i=0;i<atoms->get_size();i++){
            int itype = -1;
            for(int k=0;k<atoms->get_num_types();k++){
                if(atoms->get_atomic_numbers()[i] == atoms->get_atom_types()[k]){
                    itype=k;
                    break;
                }
            }
            int jtype = -1;
            for(int j=i+1;j<atoms->get_size();j++){
                for(int k=0;k<atoms->get_num_types();k++){
                    if(atoms->get_atomic_numbers()[j] == atoms->get_atom_types()[k]){
                        jtype=k;
                        break;
                    }
                }
                ion_ion += Zval.at(itype) * Zval.at(jtype) / atoms->distance(i,j);
            }
        }
        nuclear_nuclear_repulsion = ion_ion;
    } else if(this -> type == 2){
        nuclear_nuclear_repulsion = 0.0;
    } else if(this -> type == 3){
        nuclear_nuclear_repulsion = 0.0;
    } else{
        double ion_ion = 0.0;
        for(int i=0;i<atoms->get_size();i++){
            for(int j=i+1;j<atoms->get_size();j++){
                ion_ion += atoms->get_atomic_numbers()[i] * atoms->get_atomic_numbers()[j] / atoms->distance(i,j);
            }
        }
        nuclear_nuclear_repulsion = ion_ion;
    }
    return this -> type;
}

int Nuclear_Potential::set_static_local_pot_to_matrix( RCP<Nuclear_Potential_Matrix> &npm ){
    RCP<Epetra_Vector> local_pot;
    if( this -> type == 1 ){
        vector< vector<double> > tmp_local_pp = this -> kbprojector -> get_V_local();
        //vector<double> tmp_local_pp = this -> kbprojector -> get_V_local();
        int NumMyElements = this -> basis -> get_map() -> NumMyElements();
        int* MyGlobalElements = this -> basis -> get_map() -> MyGlobalElements();

        local_pot = rcp( new Epetra_Vector( *this -> basis -> get_map(), true ) );
        #ifdef ACE_HAVE_OMP
        #pragma omp parallel for //shchoi !!
        #endif
        for(int j = 0; j < NumMyElements; ++j){
            for(int iatom=0; iatom<atoms->get_size(); iatom++){
                local_pot -> operator[](j) +=tmp_local_pp[iatom][MyGlobalElements[j]];
            }
        }
    }
#ifdef USE_PAW
    else if( this -> type == 3 ){
        local_pot = this -> paw -> get_zero_potential();
    }
#endif
    npm -> set_local_pot( local_pot );
    return this -> type;
}

int Nuclear_Potential::set_nonlocal_pot_proj_to_matrix(
    RCP<Nuclear_Potential_Matrix> &npm
){
    vector< vector< vector<double> > > proj_vector;
    vector< vector< vector<int> > > proj_index;

    if( this -> type == 1 ){
        vector< vector< vector< vector<double> > > > V_vector = this -> kbprojector -> get_V_nl();
        vector< vector< vector< vector<int> > > > V_index = this -> kbprojector -> get_V_index();
        vector< vector<int> > oamom = this -> kbprojector -> get_oamom();

        for(int iatom=0;iatom<atoms->get_size();iatom++){
            vector< vector<double> > proj_val_atom;
            vector< vector<int> > proj_ind_atom;

            for(int i = 0; i < V_vector[iatom].size(); ++i){
                int itype = atoms->get_atom_type(iatom);
                int l = oamom[itype][i];
                for(int m = 0;m < 2*l+1; ++m){
                    proj_val_atom.push_back(V_vector[iatom][i][m]);
                    proj_ind_atom.push_back(V_index[iatom][i][m]);
                }
            }
            proj_vector.push_back(proj_val_atom);
            proj_index.push_back(proj_ind_atom);
        }
    }
#ifdef USE_PAW
    else if( this -> type == 3 ){
        for(int iatom=0;iatom<atoms->get_size();iatom++){
            vector< vector<double> > proj_val_atom;
            vector< vector<int> > proj_ind_atom;

            this -> paw -> get_paw_atom(iatom) -> get_projector_coeffs_and_inds(proj_val_atom, proj_ind_atom);

            proj_vector.push_back(proj_val_atom);
            proj_index.push_back(proj_ind_atom);
        }
    }
#endif
    npm -> set_nonlocal_pot_proj( proj_vector, proj_index );
    return this -> type;
}

int Nuclear_Potential::set_static_nonlocal_pot_coeff_to_matrix(
    RCP<Nuclear_Potential_Matrix> &npm
){
    this -> timer -> start("Hamiltonian initialization");
    vector< vector< vector< vector<double> > > > nonlocal_coeff(1);

    if( this -> type == 1 ){
        vector< vector< vector< vector<int> > > > V_index = this -> kbprojector -> get_V_index();
        vector< vector<int> > oamom = this -> kbprojector -> get_oamom();
        vector< vector< vector<double> > > EKB_matrix = this -> kbprojector -> get_EKB_matrix();

        for(int iatom=0;iatom<atoms->get_size();iatom++){
            int itype = atoms->get_atom_type(iatom);
            int dim = 0;
            for(int i=0;i<V_index[iatom].size();i++){
                int l = oamom[itype][i];
                dim += 2*l+1;
            }
            vector< vector<double> > tmp_coeff_matrix(dim);

            int index1 = 0;
            for(int i1 = 0; i1 < V_index[iatom].size(); i1++){
                int l1 = oamom[itype][i1];
                for(int m1 = 0; m1 < 2*l1+1; m1++){
                    int index2 = 0;
                    tmp_coeff_matrix.at(index1).resize(dim);
                    for(int i2 = 0; i2 < V_index[iatom].size(); i2++){
                        int l2 = oamom[itype][i2];
                        for(int m2 = 0; m2 < 2*l2+1; m2++){
                            if(m1 == m2){
                                tmp_coeff_matrix.at(index1).at(index2) = EKB_matrix.at(iatom).at(i1).at(i2);
                            }
                            ++index2;
                        }
                    }
                    ++index1;
                }
            }
            nonlocal_coeff[0].push_back( tmp_coeff_matrix );
        }
        if( this -> spin_size == 2 ){
            nonlocal_coeff.push_back( nonlocal_coeff[0] );
        }
        npm -> set_nonlocal_pot_coeff( nonlocal_coeff, false );
    }
#ifdef USE_PAW
    else if( this -> type == 3 ){
        /*
        for(int iatom=0;iatom<atoms->get_size();iatom++){
            vector< vector<double> > proj_val_atom;
            vector< vector<int> > proj_ind_atom;

            this -> paw -> get_paw_atom(iatom) -> get_projector_coeffs_and_inds( proj_val_atom, proj_ind_atom );

            nonlocal_coeff[iatom].resize(proj_ind_atom.size());
        }
        */
    }
#endif
    this -> timer -> end("Hamiltonian initialization");
    Verbose::set_numformat(Verbose::Time);
    Verbose::single(Verbose::Detail) << "Hamiltonian initialization time: " << this -> timer -> get_elapsed_time("Hamiltonian initialization", -1)  << "s" << std::endl;
    return this -> type;
}

int Nuclear_Potential::set_dynamic_nonlocal_pot_coeff_to_matrix(
    //Array< RCP<const Occupation> > occupations,
    //Array< RCP<const Epetra_MultiVector> > orbitals,
    RCP<Nuclear_Potential_Matrix> &npm
){
    this -> timer -> start("Hamiltonian update");
    vector< vector< vector< vector<double> > > > nonlocal_coeff(spin_size);

    if( this -> type == 1 ){
        /*
        vector< vector< vector< vector< vector<int> > > > > V_index = this -> kbprojector -> get_V_index();
        vector< vector< vector<double> > > tmp_coeff_matrix_spin(occupations.size());
        vector< vector<int> > oamom = this -> kbprojector -> get_oamom();

        for(int alpha = 0; alpha < occupations.size(); ++alpha){
            vector< vector< vector<double> > > tmp_coeff_matrix_spin(occupations.size());
            for(int iatom=0;iatom<atoms->get_size();iatom++){
                int itype = atoms->get_atom_type(iatom);
                int dim = 0;
                for(int i = 0; i < V_index[iatom].size(); ++i){
                    int l = oamom[itype][i];
                    for(int m = 0; m < 2*l+1; ++m){
                        dim += V_index[iatom][i][m].size();
                    }
                }

                vector< vector<double> > tmp_coeff_matrix(dim);

                for(int i = 0; i < dim; ++i){
                    tmp_coeff_matrix[i].resize(dim);
                }
                tmp_coeff_matrix_spin.push_back( tmp_coeff_matrix );
            }
            nonlocal_coeff[alpha] = tmp_coeff_matrix_spin;
        }
        */
    }
#ifdef USE_PAW
    else if( this -> type == 3){
        //if( !is_mixed ){
        //    this -> paw -> update_PAW_D_matrix( occupations, orbitals );
        //}
        if(Verbose::get_verbose_level() > 1){
            this -> paw -> print_PAW_D_matrix();
        }
        this -> paw -> calculate_hamiltonian_correction_coeff( nonlocal_coeff );
        npm -> set_nonlocal_pot_coeff( nonlocal_coeff );
    }
#endif
    this -> timer -> end("Hamiltonian update");
    Verbose::single(Verbose::Detail) << "Hamiltonain update time: " << this -> timer -> get_elapsed_time("Hamiltonian update", -1)  << "s" << std::endl;

    return this -> type;
}

RCP<KBprojector> Nuclear_Potential::get_kbprojector(){
    return this->kbprojector;
}
#ifdef USE_PAW
RCP<Paw> Nuclear_Potential::get_paw(){
    return this -> paw;
}
#endif

bool Nuclear_Potential::using_compensation_charge(){
    if(this -> type == 3){
        return true;
    }
    return false;
}
