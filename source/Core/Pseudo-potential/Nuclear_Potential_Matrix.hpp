#pragma once
#include "Epetra_CrsMatrix.h"
#include "Epetra_MultiVector.h"
#include "Epetra_Vector.h"
#include "Teuchos_RCP.hpp"
#include "../../Io/Atoms.hpp"
#include "AnasaziOperator.hpp"
#include "AnasaziTypes.hpp"
//#include "Teuchos_BLAS.hpp"
/**
 * @brief Provides matrix-form information about pseudopotential/PAW. Aims to replace Epetra_CrsMatrix.
 * @author Sungwoo Kang, Jaechang Lim
 * @date 2016.1
 * @note This class still contains Epetra_CrsMatrix compatability methods.
 **/
class Nuclear_Potential_Matrix{
    public:
        /**
         * @brief Constructor.
         * @param[in] atoms Information of atoms.
         * @param[in] spin_size Size of spin.
         * @param cutoff Matrix cutoff.
         **/
        Nuclear_Potential_Matrix(Teuchos::RCP<const Atoms> atoms, int spin_size, double cutoff = 1.0E-10);
        /**
         * @brief Multiply method.
         * @param[in] ispin Spin index.
         * @param[in] input Vector to multiply.
         * @param[out] output Multiplied vector. Output.
         * @return Always 0.
         * @note I do not know that this function is tested by LJC.
         **/
        int multiply(int ispin, Teuchos::RCP<Epetra_MultiVector> input, Teuchos::RCP<Epetra_MultiVector> &output) const;
        int multiply(int ispin, const Anasazi::MultiVec<double>& X,
        Anasazi::MultiVec<double>& Y) const;

        /**
         * @brief Add method.
         * @details [Nuclear Potential Matrix]  \f$ \times \f$ [scalar] + [matrix].
         * @param[in] ispin Spin index.
         * @param[in] matrix Matrix to add.
         * @param[out] output Resulting matrix. Output.
         * @param[in] scalar Scaling factor for matrix.
         * @note KSW does not think that putting input variable after output variable is good...
         **/
        int add(int ispin, Teuchos::RCP<Epetra_CrsMatrix> matrix, Teuchos::RCP<Epetra_CrsMatrix> &output, double scalar);
        /**
         * @brief Add method.
         * @details [Nuclear Potential Matrix]  \f$ \times \f$ [scalar] + [matrix].
         * @param[in] ispin Spin index.
         * @param[in,out] matrix Matrix to add this matrix. Input and output.
         * @param[in] scalar Scaling factor for matrix.
         * @note KSW does not think that putting input variable after output variable is good...
         **/
        int add(int ispin, Teuchos::RCP<Epetra_CrsMatrix> &matrix, double scalar);

        /**
         * @brief Set local potential to this class.
         * @param[in] local_pot Local potential to update.
         * @return Always 0.
         **/
        int set_local_pot( Teuchos::RCP<Epetra_Vector> local_pot );
        /**
         * @brief Set projector function to this class.
         * @param[in] proj_vector Projector function value in index order of [atom index][projector index][nonzero value index].
         * @param[in] proj_index Projector function index in index order of [atom index][projector index][nonzero index].
         * @return Always 0.
         * @note For KBprojector, three middle indecies(i,m,p) are linearlized to one index (projector index).
         **/
        int set_nonlocal_pot_proj(
            std::vector< std::vector< std::vector<double> > > proj_vector,
            std::vector< std::vector< std::vector<int> > > proj_index
        );
        /**
         * @brief Set coefficient for nonlocal potential to this class.
         * @param[in] nonlocal_coeff Coefficient for nonlocal potential in index order of [spin index][atom index][projector index 1][projector index 2].
         * @param[in] is_add Adds nonlocal_coeff to existing coefficient.
         * @return Always 0.
         * @note For KBprojector, three indecies(i,m,p) are linearlized to one index (projector index).
         **/
        int set_nonlocal_pot_coeff(
            std::vector< std::vector< std::vector< std::vector<double> > > > nonlocal_coeff,
            bool is_add = false
        );

        /**
         * @brief Return current local potential.
         * @return Current local potential.
         **/
        Teuchos::RCP<const Epetra_Vector> get_local_potential();

        /**
         * @brief Return current projectors.
         * @param[out] proj_vector Current projector function nonzero values in index order of [atom index][projector index][nonzero value index].
         * @param[out] proj_index Current projector function index for nonzero values in index order of [atom index][projector index][nonzero value index].
         **/
        int get_projectors(
            std::vector< std::vector< std::vector<double> > > &proj_vector,
            std::vector< std::vector< std::vector<int> > > &proj_index
        );

        /**
         * @brief Return current nonlocal coefficient values.
         * @return Coefficient for nonlocal potential in index order of [spin index][atom index][projector index 1][projector index 2].
         **/
        std::vector< std::vector< std::vector< std::vector<double> > > > get_nonlocal_coeff();

        /**
         * @brief Create Epetra_CrsGraph of kinetic_matrix + [nonzero values of this class].
         * @param[in] kinetic_matrix Matrix to add entry to the result.
         * @param[out] graph Epetra_CrsGraph entry information for this class. Output. Initialization unnecessary.
         * @return Always 0.
         * @todo Change kinetic_matrix to graph of it.
         **/
        int get_epetra_structure(
            Teuchos::RCP<Epetra_CrsMatrix> kinetic_matrix,
            Teuchos::RCP<Epetra_CrsGraph> &graph
        );

    protected:
        /**
         * @brief Add method for already filled matrix.
         * @details [Nuclear Potential Matrix] \f$\times\f$ [scalar] + [matrix].
         * @param[in] ispin Spin index.
         * @param[in,out] matrix Matrix to add this matrix. Input and output.
         * @param[in] scalar Scaling factor for matrix.
         * @return Always 0.
         * @note KSW does not think that putting input variable after output variable is good...
         * @note This function will kill entire program on error. i.e. If given matrix has no value entry for desired point.
         **/
        int add_filled(int ispin, Teuchos::RCP<Epetra_CrsMatrix> &matrix, double scalar);
        Teuchos::RCP<const Atoms> atoms;

        Teuchos::RCP<Epetra_Vector> local_potential;
        std::vector< std::vector< std::vector<double> > > proj_vector;
        std::vector< std::vector< std::vector<int> > > proj_index;
        std::vector< std::vector< std::vector< std::vector<double> > > > nonlocal_coeff;
        int spin_size;
        double matrix_cutoff;
//        Teuchos::RCP<Teuchos::BLAS<int,double> > blas;
};
