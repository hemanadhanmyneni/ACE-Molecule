#include "Hgh2KB.hpp"
#include <cmath>
#include "Epetra_Map.h"

#include "Filter.hpp"
#include "../../Basis/Create_Basis.hpp"
#include "../../Utility/Math/Spherical_Harmonics.hpp"
#include "../../Utility/Read/Read_Hgh.hpp"
#include "../../Utility/Read/Read_Hgh_Internal.hpp"
#include "../../Utility/Double_Grid.hpp"
#include "../../Utility/Verbose.hpp"
#include "../../Utility/Parallel_Util.hpp"
#include "../../Utility/Math/Spherical_Harmonics_Derivative.hpp"
#include "../../Utility/LoopUtil.hpp"
#include "../../Utility/Interpolation/Radial_Grid_Paw.hpp"
#include "../../Utility/Interpolation/Spline_Interpolation.hpp"

using std::endl;
using std::fixed;
using std::string;
using std::scientific;
using std::abs;
using std::vector;

using Teuchos::Array;
using Teuchos::rcp;
using Teuchos::RCP;

Hgh2KB::Hgh2KB(RCP<const Basis> basis,RCP<const Atoms> atoms,RCP<Teuchos::ParameterList> parameters){
    this->basis = basis;
    this->parameters=Teuchos::sublist(parameters,"BasicInformation");
    this->atoms=atoms;

    initialize_parameters();
    read_pp();  // read information from pseudopotential file; fill out all values related to radial basis
    initialize_pp();
    this -> make_vectors();
}

void Hgh2KB::initialize_parameters(){
    if(!parameters->sublist("Pseudopotential").isParameter("PSFilenames")){
        Verbose::all() << "Hgh2KB::initialize_parameters - CANNOT find PSFilenames in input file." << std::endl;
        exit(EXIT_FAILURE);
    } else {
        hgh_filenames = parameters->sublist("Pseudopotential").get< Array<string> >("PSFilenames");
    }

    parameters->sublist("Pseudopotential").get<double>("NonlocalThreshold", 1E-6);
    parameters->sublist("Pseudopotential").get<double>("LocalThreshold", 1E-7);
/*
    if(!parameters->sublist("Filter").isParameter("FilterType")){
        parameters->sublist("Filter").set("FilterType","None");
        is_filter = false;
    }
    else if(parameters->sublist("Filter").get<string>("FilterType")=="TS"){
        is_filter = true;
    }
    else{
        parameters->sublist("Filter").set("FilterType","None");
        is_filter = false;
    }
*/
    parameters->sublist("Pseudopotential").get<int>("UsingDoubleGrid", 0);
    parameters->sublist("Pseudopotential").get<int>("UsingFiltering", 0);

    if(parameters->sublist("Pseudopotential").get<int>("UsingDoubleGrid") == 1){
        parameters->sublist("Pseudopotential").get<int>("FineDimension", 3);
        Verbose::single(Verbose::Normal) << "Hgh2KB::initialize_parameters - FineDimension = " << parameters->sublist("Pseudopotential").get<int>("FineDimension") << std::endl;

        parameters->sublist("Pseudopotential").get<string>("FilterType", "Sinc");
        Verbose::single(Verbose::Normal) << "Hgh2KB::initialize_parameters - FilterType = " << parameters->sublist("Pseudopotential").get<string>("FilterType") << std::endl;

        if(parameters->sublist("Pseudopotential").get<string>("FilterType")=="Lagrange"){
            parameters->sublist("Pseudopotential").get<int>("InterpolationOrder", 8);
            Verbose::single(Verbose::Normal) << "Hgh2KB::initialize_parameters - Using Lagrange Filter function of order-" << parameters->sublist("Pseudopotential").get<int>("InterpolationOrder") << std::endl;
        }

        parameters->sublist("Pseudopotential").get<double>("LocalRgauss", 1.0);
        Verbose::single(Verbose::Normal) << "Hgh2KB::initialize_parameters - LocalRgauss = " << parameters->sublist("Pseudopotential").get<double>("LocalRgauss") << std::endl;

        parameters->sublist("Pseudopotential").get<double>("ShortDecayTol", 0.0001);
        Verbose::single(Verbose::Normal) << "Hgh2KB::initialize_parameters - ShortDecayTol = " << parameters->sublist("Pseudopotential").get<double>("ShortDecayTol") << std::endl;

        parameters->sublist("Pseudopotential").get<double>("LocalIntegrationRange", 1.25);
        Verbose::single(Verbose::Normal)<< "Hgh2KB::initialize_parameters - LocalIntegrationRange = " << parameters->sublist("Pseudopotential").get<double>("LocalIntegrationRange") << std::endl;

        /*
        if(!parameters->sublist("Pseudopotential").isParameter("NonlocalRmax")){
            parameters->sublist("Pseudopotential").set<double>("NonlocalRmax", 2.0);
        }
        Verbose::single(Verbose::Normal)<< "Hgh2KB::initialize_parameters - NonlocalRmax = " << parameters->sublist("Pseudopotential").get<double>("NonlocalRmax") << std::endl;
        */
        Verbose::single() << "WARNING: HGH doublegrid is not tested!" << std::endl;
        std::cerr << "WARNING: HGH doublegrid is not tested!" << std::endl;
    }
    if(parameters->sublist("Pseudopotential").get<int>("UsingFiltering") == 1){
        Verbose::single() << "WARNING: HGH filtering cutoff radius is not tested!" << std::endl;
        std::cerr << "WARNING: HGH filtering cutoff radius is not tested!" << std::endl;
    }

    this -> type = parameters->sublist("Pseudopotential").get<string>("HghFormat", "Internal");
    Verbose::single(Verbose::Normal) << "\n#------------------------------------------- Hgh2KB::initialize_parameters\n";
    if(type == "HGH"){
        Verbose::single(Verbose::Normal) << " HGH pseudopotential format specified in Phys. Rev. B 58, 3641 is used.\n";
    } else if(type == "Willand"){
        Verbose::single(Verbose::Normal) << " HGH pseudopotential format specified in J. Chem. Phys. 138, 104109 is used.\n";
    } else if(type == "Internal"){
        Verbose::single(Verbose::Normal) << " Hard coded HGH pseudopotential is used.\n";
        this -> xc_type = parameters -> sublist("Pseudopotential").get<string>("XCType");
    } else{
        Verbose::all() << "Wrong pseudopotential format: Pseudopotential.HghFormat is wrong\n";
        exit(EXIT_FAILURE);
    }
    Verbose::single(Verbose::Normal) << "#---------------------------------------------------------------------------\n";

    /*
    if(!parameters->sublist("Pseudopotential").isParameter("LocalIntegration")){
        parameters->sublist("Pseudopotential").set<string>("LocalIntegration", "coarse");
    }
    */

    parameters->sublist("Pseudopotential").get<string>("NonlocalIntegration", "coarse");

    if(parameters->sublist("Pseudopotential").get<string>("NonlocalIntegration") == "fine"){
        parameters->sublist("Pseudopotential").get<double>("NonlocalScaling", 5.0);
        Verbose::single(Verbose::Normal) << "Hgh2KB::initialize_parameters - Scaling = " << parameters->sublist("Pseudopotential").get<double>("NonlocalScaling") << '\n';

        parameters->sublist("Pseudopotential").get<double>("NonlocalRmax", 5.0);
        Verbose::single(Verbose::Normal) << "Hgh2KB::initialize_parameters - NonlocalRmax = " << parameters->sublist("Pseudopotential").get<double>("NonlocalRmax") << '\n';
    }

    return;
}

void Hgh2KB::read_pp(){
    for(int itype=0; itype<atoms->get_num_types(); itype++){
        double Zval_tmp = 0.0;
        bool core_correction = false;

        if(this -> type == "Internal"){
            Read::Hgh_Internal::read_header(hgh_filenames[itype], Zval_tmp, xc_type, core_correction);
        } else {
            Read::Hgh::read_header(hgh_filenames[itype], Zval_tmp, type, core_correction);
        }

        Zvals.push_back(Zval_tmp);

        if(this -> type == "Internal"){
            Read::Hgh_Internal::read_local(hgh_filenames[itype], xc_type, rloc, nloc, c_i);
        } else {
            Read::Hgh::read_local(hgh_filenames[itype], type, rloc, nloc, c_i);
        }

        vector<int> oamom_tmp;
        if(this -> type == "Internal"){
            Read::Hgh_Internal::read_nonlocal(hgh_filenames[itype], xc_type, h_ij_total, k_ij_total, r_l, projector_number, number_vps_file, oamom_tmp, itype);
        } else {
            Read::Hgh::read_nonlocal(hgh_filenames[itype], type, h_ij_total, k_ij_total, r_l, projector_number, number_vps_file, oamom_tmp, itype);
        }

        oamom_per_proj.push_back(oamom_tmp);

        // Read nonlinear core correction
        double rcore_tmp = 0.0, ccore_tmp = 0.0;
        if(core_correction == true){
            if(this -> type == "Internal"){
                Read::Hgh_Internal::read_core_correction(hgh_filenames[itype], xc_type, rcore_tmp, ccore_tmp);
            } else {
                Read::Hgh::read_core_correction(hgh_filenames[itype], rcore_tmp, ccore_tmp);
            }

            nonlinear_core_correction.push_back(true);
        }
        else{
            nonlinear_core_correction.push_back(false);
        }
        rcore.push_back(rcore_tmp);
        ccore.push_back(ccore_tmp);
        // end

        pp_information(itype);
    }

    this -> EKB_matrix.resize(this -> atoms -> get_size());
    for(int iatom = 0; iatom < atoms -> get_size(); ++iatom){
        int itype = atoms->get_atom_type(iatom);
        int dim = 0;
        for(int i = 0; i < number_vps_file[itype]; ++i){
            //dim += h_ij_total[itype].at(i).size();
            dim += projector_number[itype][i];
        }
        this -> EKB_matrix[iatom].resize(dim);
        for(int i = 0; i < dim; ++i){
            this -> EKB_matrix[iatom][i].resize(dim);
        }
        int base_index = 0;
        for(int i = 0; i < number_vps_file[itype]; ++i){
            int h_ij_mine_size = h_ij_total[itype].at(i).size();
            for(int j1 = 0; j1 < projector_number[itype][i]; ++j1){
                int i1 = base_index + j1;
                for(int j2 = 0; j2 < projector_number[itype][i]; ++j2){
                    int i2 = base_index + j2;
                    this -> EKB_matrix[iatom].at(i1).at(i2) = h_ij_total[itype][i].at(j1*projector_number[itype][i]+j2);
                }
            }
            base_index += projector_number[itype][i];
        }
    }
    this -> oamom.resize(atoms -> get_num_types());
    for(int itype = 0; itype < atoms -> get_num_types(); ++itype){
        for(int i = 0; i < number_vps_file[itype]; ++i){
            for(int p = 0; p < projector_number[itype][i]; ++p){
                this -> oamom[itype].push_back(this -> oamom_per_proj[itype][i]);
            }
        }
    }

    return;
}

void Hgh2KB::compute_nonlinear_core_correction(){
    int *MyGlobalElements = basis->get_map()->MyGlobalElements();
    int NumMyElements = basis->get_map()->NumMyElements();

    core_density = Teuchos::rcp(new Epetra_Vector(*basis->get_map()));
    core_density_grad = rcp(new Epetra_MultiVector(*basis->get_map(),3) );

    vector<std::array<double,3> > positions = atoms->get_positions();

    double analytic_core_charge = 0.0;

    for(int iatom=0; iatom<atoms->get_size(); iatom++){
        int itype = atoms->get_atom_type(iatom);

        double Z = atoms->get_atomic_numbers()[iatom];
        if(nonlinear_core_correction[itype] == true){

            analytic_core_charge += (Z - Zvals[itype]) * ccore[itype];

            for(int i=0; i<NumMyElements; i++){
                double x, y, z, r;
                /*
                double x_p, y_p, z_p;
                basis->get_position(MyGlobalElements[i], x_p, y_p, z_p);

                double x = x_p - positions[iatom][0];
                double y = y_p - positions[iatom][1];
                double z = z_p - positions[iatom][2];
                double r = sqrt(x*x + y*y + z*z);
                */
                basis->find_nearest_displacement(MyGlobalElements[i], positions[iatom][0], positions[iatom][1], positions[iatom][2], x, y, z, r);

                double tmp = ccore[itype] * (Z - Zvals[itype]) / pow(sqrt(2.0*M_PI)*rcore[itype], 3) * exp(-0.5*r*r/rcore[itype]/rcore[itype]);

                core_density->SumIntoMyValue(i, 0, tmp);
                core_density_grad->SumIntoMyValue(i, 0, -1.0/rcore[itype]/rcore[itype] * x * tmp); // x-axis
                core_density_grad->SumIntoMyValue(i, 1, -1.0/rcore[itype]/rcore[itype] * y * tmp); // y-axis
                core_density_grad->SumIntoMyValue(i, 2, -1.0/rcore[itype]/rcore[itype] * z * tmp); // z-axis
            }
        }
    }
    double numerical_core_charge = 0.0;
    core_density->Norm1(&numerical_core_charge);
    numerical_core_charge *= basis->get_scaling()[0] * basis->get_scaling()[1] * basis->get_scaling()[2];

    Verbose::single().precision(12);
    Verbose::single() << fixed;
    Verbose::single(Verbose::Normal) << endl;
    Verbose::single(Verbose::Normal) << "#------------------------------- Hgh2KB::compute_nonlinear_core_correction" << endl;
    Verbose::single(Verbose::Normal) << " Analytic core charge  = " << analytic_core_charge << endl;
    Verbose::single(Verbose::Normal) << " Numerical core charge = " << numerical_core_charge << endl;
    Verbose::single(Verbose::Normal) << "#---------------------------------------------------------------------------" << endl;
    Verbose::single().precision(6);
    Verbose::single() << scientific;

    return;
}
/*
void Hgh2KB::calculate_local(Teuchos::RCP<Epetra_Vector>& local_potential){
    int *MyGlobalElements = basis->get_map()->MyGlobalElements();
    int NumMyElements = basis->get_map()->NumMyElements();

    const double** scaled_grid = basis->get_scaled_grid();
    vector<std::array<double,3> > positions = atoms->get_positions();
    double threshold = parameters->sublist("Pseudopotential").get<double>("LocalThreshold");

    local_potential->PutScalar(0.0);

    Verbose::single(Verbose::Normal) << "\n#------------------------------------------------- Hgh2KB::calculate_local\n";
    Verbose::single(Verbose::Normal) << " Local pseudopotential integration: Gauss quadrature\n";
    Verbose::single(Verbose::Normal) << "#---------------------------------------------------------------------------\n";

    for(int iatom=0; iatom<atoms->get_size(); iatom++){
        int itype = atoms->get_atom_type(iatom);

        // Calculate local part of pseudopotential_matrix
        for(int i=0; i<NumMyElements; i++){
            double x,y,z,r;
            int i_x=0,i_y=0,i_z=0;
            basis->decompose(MyGlobalElements[i], i_x, i_y, i_z);

            // Distance between the grid point & the atom
            x = scaled_grid[0][i_x] - positions[iatom][0];
            y = scaled_grid[1][i_y] - positions[iatom][1];
            z = scaled_grid[2][i_z] - positions[iatom][2];
            r = sqrt(x*x + y*y + z*z);

            local_potential->operator[](i) += compute_Vlocal(itype, r);
            //local[i] = -Zval[itype] * tmp2 + exp( -0.5 * (r*r/(rloc[itype]*rloc[itype])) ) * tmp;
        }
        // end
    }

    return;
}*/

void Hgh2KB::get_comp_potential(vector<double> radial_mesh, int itype, vector<double>& comp_potential){
    vector<std::array<double,3>> positions =atoms->get_positions();
    auto scaling = basis->get_scaling();

    comp_potential.clear();

    double r,scaled_r, r_gauss;
    r_gauss = parameters->sublist("Pseudopotential").get<double>("LocalRgauss");
    double comp_norm = 0.0;
    for (int i=0;i< radial_mesh.size();i++){
        r=radial_mesh[i];
        scaled_r = r/r_gauss;
        comp_norm += Zvals[itype]/pow(sqrt(M_PI)*r_gauss,3)*exp(-scaled_r*scaled_r);
        comp_potential.push_back( Zvals[itype]*erf(scaled_r)/r );
    }

    comp_norm = 0.0;
    for (int j=0; j<basis->get_original_size(); j++){
        double x, y, z, r;
        basis -> get_position(j, x, y, z);
        r = sqrt(x*x + y*y + z*z);
        scaled_r = r/r_gauss;
        if(r<=local_cutoff[itype]){
            scaled_r = r/r_gauss;
            comp_norm += Zvals[itype]/pow(sqrt(M_PI)*r_gauss,3)*exp(-scaled_r*scaled_r);
        }
    }
    comp_norm*=scaling[0]*scaling[1]*scaling[2];

    return;
}

/*
void Hgh2KB::calculate_local_DG(Teuchos::RCP<Epetra_CrsMatrix>& core_hamiltonian){
    int *MyGlobalElements = basis->get_map()->MyGlobalElements();
    int NumMyElements = basis->get_map()->NumMyElements();

    Verbose::single(Verbose::Normal) << "\n#------------------------------------------- Hgh2KB::calculate_local_DG\n";
    Verbose::single(Verbose::Normal) << " Using double-grid to represent pseudopotential\n";
    Verbose::single(Verbose::Normal) << "#---------------------------------------------------------------------------\n";

    int size = basis->get_original_size();
    vector<double*> positions = atoms->get_positions();

    double threshold = parameters->sublist("Pseudopotential").get<double>("LocalThreshold");

    local_pp.resize(atoms->get_original_size());
    local_pp_dev.resize(atoms->get_original_size());

    for(int iatom=0; iatom<atoms->get_original_size(); iatom++){
        local_pp[iatom].resize(size);
        local_pp_dev[iatom].resize(size);
        for(int i=0; i<size; i++){
            local_pp[iatom][i] = 0.0;
            local_pp_dev[iatom][i] = 0.0;
        }
    }

    const double* scaling = basis->get_scaling();
    const double** scaled_grid = basis->get_scaled_grid();
    double* fine_scaling = new double[3];
    double fine_dimension = parameters->sublist("Pseudopotential").get<int>("FineDimension");
    fine_scaling[0] = scaling[0] / fine_dimension;
    fine_scaling[1] = scaling[1] / fine_dimension;
    fine_scaling[2] = scaling[2] / fine_dimension;

    double length_for_x = (scaled_grid[0][basis->get_points()[0]-1]-scaled_grid[0][0]);
    double length_for_y = (scaled_grid[1][basis->get_points()[1]-1]-scaled_grid[1][0]);
    double length_for_z = (scaled_grid[2][basis->get_points()[2]-1]-scaled_grid[2][0]);

    int numx = static_cast<int>(length_for_x / fine_scaling[0]) + 1;
    int numy = static_cast<int>(length_for_y / fine_scaling[1]) + 1;
    int numz = static_cast<int>(length_for_z / fine_scaling[2]) + 1;

    int* fine_points = new int[3];
    fine_points[0] = numx;
    fine_points[1] = numy;
    fine_points[2] = numz;

    Basis_Function* fine_basis = new Sinc(fine_points, fine_scaling);
    const double** fine_scaled_grid = fine_basis->get_scaled_grid();

    vector<double> rcut;

    for(int itype=0; itype<atoms->get_atom_types().size(); itype++){
        // Find rcut
        double rcut_tmp = 0.0;
        for(int i=0; i<11; i++){
            double r = 10.0 - static_cast<double>(i);
            double tmp = compute_Vshort(itype, r);
            if(abs(tmp) < 1.0e-4) rcut_tmp = r;
            else break;
        }
        for(int i=0; i<11; i++){
            double r = rcut_tmp - 0.1 * static_cast<double>(i);
            double tmp = compute_Vshort(itype, r);
            if(abs(tmp) < 1.0e-4) rcut_tmp = r;
            else break;
        }
        for(int i=0; i<11; i++){
            double r = rcut_tmp - 0.01 * static_cast<double>(i);
            double tmp = compute_Vshort(itype, r);
            if(abs(tmp) < 1.0e-4) rcut_tmp = r;
            else break;
        }
        for(int i=0; i<11; i++){
            double r = rcut_tmp - 0.001 * static_cast<double>(i);
            double tmp = compute_Vshort(itype, r);
            if(abs(tmp) < 1.0e-4) rcut_tmp = r;
            else break;
        }
        rcut_tmp *= parameters->sublist("Pseudopotential").get<double>("LocalIntegrationRange");
        Verbose::single() << " \"rcut\" for atom " << itype << ": " << rcut_tmp << " Bohr\n";
        rcut.push_back(rcut_tmp);
        // end
    }

    for(int iatom=0; iatom<atoms->get_original_size(); iatom++){
        int itype = atoms->get_atom_type(iatom);

        RCP<Atoms> new_atom = rcp(new Atoms());
        Atom tmp_atom = Atom(atoms->operator[](iatom));
        new_atom->push(tmp_atom);

        vector<double> radius;
        radius.push_back(rcut[itype]);
        Grid_Setting* fine_grid = new Grid_Atoms(fine_points, fine_basis, new_atom.get(), radius);

        Epetra_Map fine_map(fine_grid->get_original_size(), 0, basis->get_map()->Comm());
        int fine_NumMyElements = fine_map.NumMyElements();
        int* fine_GlobalMyElements = fine_map.MyGlobalElements();

        for(int j=0; j<size; j++){
            double X = 0.0, Y = 0.0, Z = 0.0, R = 0.0;
            int j_x = 0, j_y = 0, j_z = 0;
            basis->decompose(j, j_x, j_y, j_z);
            X = scaled_grid[0][j_x] - positions[iatom][0];
            Y = scaled_grid[1][j_y] - positions[iatom][1];
            Z = scaled_grid[2][j_z] - positions[iatom][2];
            R = sqrt( X*X + Y*Y + Z*Z );

            double Vshort_tmp = 0.0;
            double total_Vshort_tmp = 0.0;
            if(R < rcut[itype]){
                for(int fine_q=0; fine_q<fine_NumMyElements; fine_q++){
                    int fine_qx = 0;
                    int fine_qy = 0;
                    int fine_qz = 0;

                    fine_grid->decompose(fine_GlobalMyElements[fine_q], fine_basis->get_points(), fine_qx, fine_qy, fine_qz);

                    double x = fine_scaled_grid[0][fine_qx] - positions[iatom][0];
                    double y = fine_scaled_grid[1][fine_qy] - positions[iatom][1];
                    double z = fine_scaled_grid[2][fine_qz] - positions[iatom][2];
                    double r = sqrt(x*x + y*y + z*z);

                    Vshort_tmp += compute_Vshort(itype, r) * sinc((X-x)/scaling[0]) * sinc((Y-y)/scaling[1]) * sinc((Z-z)/scaling[2]);
                }
                fine_map.Comm().SumAll(&Vshort_tmp, &total_Vshort_tmp, 1);
                total_Vshort_tmp *= 1.0 / pow(fine_dimension,3.0);
            }

            double Vlocal_tmp = 0.0;
            if(R < 1.0E-15){
                Vlocal_tmp = sqrt(2.0/M_PI) / rloc[itype];
            }
            else{
                Vlocal_tmp = erf( R/(sqrt(2.0)*rloc[itype]) ) / R;
            }
            Vlocal_tmp *= -Zvals[itype];

            double Vlocal = Vlocal_tmp + total_Vshort_tmp;
            local_pp[iatom][j] += Vlocal;

            int ierr;
            if(abs(Vlocal) > threshold){
                ierr = core_hamiltonian->InsertGlobalValues(j, 1, &Vlocal, &j);
            }
        }
        delete fine_grid;
    }

    delete[] fine_points;
    delete[] fine_scaling;
    delete fine_basis;

    return;
}
*/

void Hgh2KB::calculate_nonlocal(int iatom, std::array<double,3> position, 
        vector<vector<vector<double> > >& nonlocal_pp, 
        vector<vector<vector<double> > >& nonlocal_dev_x, 
        vector<vector<vector<double> > >& nonlocal_dev_y, 
        vector<vector<vector<double> > >& nonlocal_dev_z, 
        vector<vector<vector<int> > >& nonlocal_pp_index
){

    const int size = basis->get_original_size();
    const double threshold = parameters->sublist("Pseudopotential").get<double>("NonlocalThreshold");

    const int itype = atoms->get_atom_type(iatom);

    if(parameters->sublist("Pseudopotential").get<int>("UsingDoubleGrid") == 1
            or (parameters->sublist("Pseudopotential").get<int>("UsingDoubleGrid") == 0
            and parameters->sublist("Pseudopotential").get<string>("NonlocalIntegration") == "coarse")){

        if(parameters -> sublist("Pseudopotential").get<int>("UsingFiltering") == 0){
            if(iatom == 0){
                Verbose::single(Verbose::Normal) << "\n#---------------------------------------------- Hgh2KB::calculate_nonlocal\n";
                Verbose::single(Verbose::Normal) << " Non-local pseudopotential integration: Gauss quadrature\n";
                Verbose::single(Verbose::Normal) << " Nonlocal threshold = " << threshold << endl;
                Verbose::single(Verbose::Normal) << "#---------------------------------------------------------------------------\n";
            }
            
            for(int i=0; i<number_vps_file[itype]; i++){
                int l = oamom_per_proj[itype][i];

                for(int p=0; p<projector_number[itype][i]; p++){
                    vector< vector<double> > nonlocal_pp_tmp;
                    vector< vector<int> > nonlocal_pp_index_tmp;
                    
                    vector< vector<double> > nonlocal_dev_x_tmp;
                    vector< vector<double> > nonlocal_dev_y_tmp;
                    vector< vector<double> > nonlocal_dev_z_tmp;
                    
                    
                    for(int m=0; m<2*l+1; m++){
                        vector<double> nonlocal_pp_tmp_tmp;
                        vector<int> nonlocal_pp_index_tmp_tmp;
                
                        vector<double> nonlocal_dev_x_tmp_tmp;
                        vector<double> nonlocal_dev_y_tmp_tmp;
                        vector<double> nonlocal_dev_z_tmp_tmp;

                        for(int j=0; j<size; j++){
                            double x, y, z, r;
                            double x_p, y_p, z_p;
                            basis -> get_position(j, x_p, y_p, z_p);
                            /*
                            double x = x_p - position[0];
                            double y = y_p - position[1];
                            double z = z_p - position[2];
                            double r = sqrt( x*x + y*y + z*z );
                            */
                            double denominator = basis->compute_basis(j, x_p, y_p, z_p);
                            basis -> find_nearest_displacement(j, position[0], position[1], position[2], x, y, z, r);

                            double p_li = compute_V_nl(itype, l, i, p, r);
                            double value = p_li * Spherical_Harmonics::Ylm(l,m-l,x,y,z) / basis -> compute_basis(j, x_p, y_p, z_p);

                            if (fabs(value) > threshold){
                                nonlocal_pp_tmp_tmp.push_back(value);
                                nonlocal_pp_index_tmp_tmp.push_back(j);
                            }

    //                      r = 0.1;
    //                       x = 0;
    //                        y = 0;
    //                        z = 0;
                            //std::cout.precision(20);
                            double p_li_dev = compute_V_nl_dev(itype, l, i, p, r);
                            double dylm[3]; 
                            double value_x, value_y, value_z; 
                            for (int ind =0; ind<3;ind++){
                                dylm[ind] = Spherical_Harmonics::Derivative::dYlm(l,m-l,x,y,z,ind);
                            }
                            
                            if(r>=1.0E-10){
                                value_x = (x/r)*Spherical_Harmonics::Ylm(l,m-l,x,y,z)*p_li_dev +  dylm[0]*p_li;
                                value_y = (y/r)*Spherical_Harmonics::Ylm(l,m-l,x,y,z)*p_li_dev +  dylm[1]*p_li;
                                value_z = (z/r)*Spherical_Harmonics::Ylm(l,m-l,x,y,z)*p_li_dev +  dylm[2]*p_li;
                            }
                            else{
                                value_x = dylm[0]*p_li;
                                value_y = dylm[1]*p_li;
                                value_z = dylm[2]*p_li;
                            }
                            value_x = value_x/denominator;
                            value_y = value_y/denominator;
                            value_z = value_z/denominator;
                            if (fabs(value) > threshold){
                                nonlocal_dev_x_tmp_tmp.push_back(value_x);
                                nonlocal_dev_y_tmp_tmp.push_back(value_y);
                                nonlocal_dev_z_tmp_tmp.push_back(value_z);
                            }
                            
                        }
                        nonlocal_pp_tmp.push_back(nonlocal_pp_tmp_tmp);
                        nonlocal_pp_index_tmp.push_back(nonlocal_pp_index_tmp_tmp);
                        
                        nonlocal_dev_x_tmp.push_back(nonlocal_dev_x_tmp_tmp);
                        nonlocal_dev_y_tmp.push_back(nonlocal_dev_y_tmp_tmp);
                        nonlocal_dev_z_tmp.push_back(nonlocal_dev_z_tmp_tmp);
    //                    nonlocal_dev_index_tmp.push_back(nonlocal_dev_index_tmp_tmp);
                    }
                    nonlocal_pp.push_back(nonlocal_pp_tmp);
                    nonlocal_pp_index.push_back(nonlocal_pp_index_tmp);
                
                    nonlocal_dev_x.push_back(nonlocal_dev_x_tmp);
                    nonlocal_dev_y.push_back(nonlocal_dev_y_tmp);
                    nonlocal_dev_z.push_back(nonlocal_dev_z_tmp);
    //                nonlocal_dev_index.push_back(nonlocal_dev_index_tmp);
                }
            }
        } else {
            if(iatom == 0){
                Verbose::single(Verbose::Normal) << "\n#---------------------------------------------- Hgh2KB::calculate_nonlocal\n";
                Verbose::single(Verbose::Normal) << " Non-local pseudopotential integration: Filtering\n";
                Verbose::single(Verbose::Normal) << "#---------------------------------------------------------------------------\n";
            }
            
            for(int i=0; i<number_vps_file[itype]; i++){
                int l = oamom_per_proj[itype][i];

                for(int p=0; p<projector_number[itype][i]; p++){
                    vector< vector<double> > nonlocal_pp_tmp;
                    vector< vector<int> > nonlocal_pp_index_tmp;
                    
                    vector< vector<double> > nonlocal_dev_x_tmp;
                    vector< vector<double> > nonlocal_dev_y_tmp;
                    vector< vector<double> > nonlocal_dev_z_tmp;
                    
                    
                    for(int m=0; m<2*l+1; m++){
                        vector<double> nonlocal_pp_tmp_tmp;
                        vector<int> nonlocal_pp_index_tmp_tmp;
                
                        vector<double> nonlocal_dev_x_tmp_tmp;
                        vector<double> nonlocal_dev_y_tmp_tmp;
                        vector<double> nonlocal_dev_z_tmp_tmp;

                        int rgrid_size = 5*r_l[itype][i]*100; double rgrid_dr = 0.01;
                        vector<double> rgrid(rgrid_size), drgrid(rgrid_size, rgrid_dr), nl_r_pp(rgrid_size), nl_r_pp_dev(rgrid_size);
                        for(int ir = 0; ir < rgrid_size; ++ir){
                            double r = ir * rgrid_dr;
                            rgrid[ir] = r;
                            nl_r_pp[ir] = compute_V_nl(itype, l, i, p, r);
                            //nl_r_pp_dev[ir] = compute_V_nl_dev(itype, l, i, p, r);
                        }
                        vector<double> filt_nl_r_pp, filt_nl_r_grid, filt_nl_r_pp_dev;
                        // 99.7% of nonlocal potential. TODO XXX optimize this.
                        filter -> filter_nonlocal(nl_r_pp, rgrid, drgrid, l, 3*r_l[itype][i], filt_nl_r_pp, filt_nl_r_grid);

                        /*
                        Verbose::single() << "r, filt_nl, orig_nl" << std::endl;
                        for(int r = 0; r < filt_nl_r_grid.size(); ++r){
                            Verbose::single() << std::scientific << filt_nl_r_grid[r] << ", " << filt_nl_r_pp[r] << ", " << compute_V_nl(itype, l, i, p, filt_nl_r_grid[r]) << std::endl;
                        }
                        */
                        // Remove oscillation at tail. Is this necessary?
                        int new_size;
                        for(int r = 0; r < filt_nl_r_grid.size(); ++r){
                            if(filt_nl_r_pp[0] * filt_nl_r_pp[r] < 0){
                                new_size = r-1;
                                break;
                            }
                        }
                        vector<double> orig_grid = filt_nl_r_grid;
                        for(int r = new_size+1; r < filt_nl_r_grid.size(); ++r){
                            filt_nl_r_pp[r] = 0.0;
                        }
                        filt_nl_r_pp_dev = Radial_Grid::Paw::linear_derivative(filt_nl_r_pp, filt_nl_r_grid);

                        vector<double> y2l = Interpolation::Spline::spline(filt_nl_r_grid, filt_nl_r_pp, filt_nl_r_grid.size());
                        vector<double> y2d = Interpolation::Spline::spline(filt_nl_r_grid, filt_nl_r_pp_dev, filt_nl_r_grid.size());
                        for(int j=0; j<size; j++){
                            double x, y, z, r;
                            double x_p, y_p, z_p;
                            basis -> get_position(j, x_p, y_p, z_p);
                            /*
                            double x = x_p - position[0];
                            double y = y_p - position[1];
                            double z = z_p - position[2];
                            double r = sqrt( x*x + y*y + z*z );
                            */
                            double denominator = basis->compute_basis(j, x_p, y_p, z_p);
                            basis -> find_nearest_displacement(j, position[0], position[1], position[2], x, y, z, r);

                            double value = Interpolation::Spline::splint(filt_nl_r_grid, filt_nl_r_pp, y2l, filt_nl_r_grid.size(), r) * Spherical_Harmonics::Ylm(l, m-l, x, y, z);
                            //value = compute_V_nl(itype, l, i, p, r) * Spherical_Harmonics::Ylm(l,m-l,x,y,z);
                            double value_dev = Interpolation::Spline::splint(filt_nl_r_grid, filt_nl_r_pp_dev, y2l, filt_nl_r_grid.size(), r);
                            double value_x, value_y, value_z;

                            if(r>=1.0E-10){
                                value_x = (x/r)*Spherical_Harmonics::Ylm(l,m-l,x,y,z)*value_dev + Spherical_Harmonics::Derivative::dYlm(l,m-l,x,y,z,0)*value;
                                value_y = (y/r)*Spherical_Harmonics::Ylm(l,m-l,x,y,z)*value_dev + Spherical_Harmonics::Derivative::dYlm(l,m-l,x,y,z,1)*value;
                                value_z = (z/r)*Spherical_Harmonics::Ylm(l,m-l,x,y,z)*value_dev + Spherical_Harmonics::Derivative::dYlm(l,m-l,x,y,z,2)*value;
                            }
                            else{
                                value_x = Spherical_Harmonics::Derivative::dYlm(l,m-l,x,y,z,0)*value;
                                value_y = Spherical_Harmonics::Derivative::dYlm(l,m-l,x,y,z,1)*value;
                                value_z = Spherical_Harmonics::Derivative::dYlm(l,m-l,x,y,z,2)*value;
                            }
                            value /= denominator; value_x /= denominator; value_y /= denominator; value_z /= denominator;

                            if (fabs(value) > threshold){
                                nonlocal_pp_tmp_tmp.push_back(value);
                                nonlocal_pp_index_tmp_tmp.push_back(j);
                                nonlocal_dev_x_tmp_tmp.push_back(value_x);
                                nonlocal_dev_y_tmp_tmp.push_back(value_y);
                                nonlocal_dev_z_tmp_tmp.push_back(value_z);
                            }
                            
                        }
                        nonlocal_pp_tmp.push_back(nonlocal_pp_tmp_tmp);
                        nonlocal_pp_index_tmp.push_back(nonlocal_pp_index_tmp_tmp);
                        
                        nonlocal_dev_x_tmp.push_back(nonlocal_dev_x_tmp_tmp);
                        nonlocal_dev_y_tmp.push_back(nonlocal_dev_y_tmp_tmp);
                        nonlocal_dev_z_tmp.push_back(nonlocal_dev_z_tmp_tmp);
                    }
                    nonlocal_pp.push_back(nonlocal_pp_tmp);
                    nonlocal_pp_index.push_back(nonlocal_pp_index_tmp);
                
                    nonlocal_dev_x.push_back(nonlocal_dev_x_tmp);
                    nonlocal_dev_y.push_back(nonlocal_dev_y_tmp);
                    nonlocal_dev_z.push_back(nonlocal_dev_z_tmp);
                }
            }
        }
    }
    else{
        Verbose::all() << "Hgh2KB::calculate_nonlocal - not supported." << endl;
        exit(EXIT_FAILURE);
    }

//    return;
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Below routine is that calculate nonlocal_pp_dev. Made by HS
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
    double k_plj_upper, k_plj_down, k_plj;
    double p_li_upper, p_li_down, p_li;
    double ylm, k_ylm;
    double tmp2 = 0.0;
    double p_li_dev, p_li_dev1, p_li_dev2  ;
    double k; // This is values that don't have 'r' term.
    double c; // constant_value
    double x_p, y_p, z_p;
    double factor = 1.0;
    int * MyGlobalElements = mesh->get_map()->MyGlobalElements();
    int NumMyElements = mesh->get_map()->NumMyElements();

    nonlocal_dev_x = vector< vector< vector<double> > >(number_vps_file[itype] );
    nonlocal_dev_y = vector< vector< vector<double> > >(number_vps_file[itype] );
    nonlocal_dev_z = vector< vector< vector<double> > >(number_vps_file[itype] );
    
    auto check_xyzr = [this,&MyGlobalElements,&position]
                                (int i, double& x, double& y, double& z, double& r)->void {
                                double x_p, y_p, z_p;
                                mesh-> get_position(MyGlobalElements[i], x_p, y_p, z_p);
                                x = x_p - position[0];
                                y = y_p - position[1];
                                z = z_p - position[2];
                                r = std::sqrt( x*x + y*y +z*z);
                                return;
    };



    vector< std::function<bool(int)> > filter_functions;
    for(int i =0;i<number_vps_file[itype];i++){ // projector index
        int l = oamom[itype][i]; //angular momentum
        nonlocal_pp[i].resize(2*l+1);
        nonlocal_pp_index[i].resize(2*l+1);
//        if(is_cal_nonlocal_dev){
        nonlocal_dev_x[i].resize(2*l+1);
        nonlocal_dev_y[i].resize(2*l+1);
        nonlocal_dev_z[i].resize(2*l+1);
//        }
        for(int m=0;m<2*l+1;m++){ //  magnetic momentum index
            double rcut = new_nonlocal_cutoff[itype][i]*factor;
            auto check_distance = [rcut,check_xyzr](int i)->bool {
                                        double x; double y; double z; double r;
                                        check_xyzr(i,x,y,z,r);
                                        return rcut>r;
                                        };
            filter_functions.push_back(check_distance);
        }
    }
    std::vector<std::vector<int> > indices(filter_functions.size());
    LoopUtil::filter(NumMyElements,filter_functions,indices);
    int loop_index=0;



//    for (int r_ind =0; r_ind < size; r_ind++){
    for(int r_ind=0; r_ind<number_vps_file[itype]; r_ind++){
        mesh->get_position(r_ind, x_p, y_p, z_p);
        double x = x_p - position[0];
        double y = y_p - position[1];
        double z = z_p - position[2];
        double r = sqrt( x*x + y*y + z*z );
        for(int m= 0; m <2*l+1; m++){
            double after_val[3];
            double before_val[3];
//            for (int tmp_ind; tmp_ind<3;tmp_ind++){
//                after_val[tmp_ind] = 0.0;
//                before_val[tmp_ind] = 0.0;
//            }
            
            vector<double> input_radial,input_radial_mesh;
            //switch(case_){
            switch(0){
                case (0):
                    // No filtering, no doublegrid.
                    input_radial = new_nonlocal_pp_radial[itype][i];
                    input_radial_mesh = new_nonlocal_mesh[itype][i];
                break;
                case(1):
                    /// @todo Check that we should not add origin values.
                    filter->filter_nonlocal(new_nonlocal_pp_radial[itype][i],
                                            new_nonlocal_mesh[itype][i],
                                            nonlocal_d_mesh[itype][i],
                                            l, new_nonlocal_cutoff[itype][i]*factor,
                                            input_radial, input_radial_mesh);

                break;
                case(2):
                    // Doublegrid. But since if(..."UsingDoubleGrid"...) here, it does not work.
                default:
                    Verbose::all() << "Filtering/doublegrid option case_ = " << case_ << std::endl;
                    throw std::logic_error("Invalid filtering/doublegrid option!");
                break;
            }
            // Spline interpolation
            const int mesh_size = input_radial_mesh.size();
            double yp1 = (input_radial[1]-input_radial[0])/(input_radial_mesh[1]-input_radial_mesh[0]);
            double ypn = (input_radial[mesh_size-1]-input_radial[mesh_size-2])/(input_radial_mesh[mesh_size-1]-input_radial_mesh[mesh_size-2]);
            vector<double> y2 = Interpolation::Spline::spline(input_radial_mesh, input_radial, mesh_size, yp1, ypn);
            //double tmp;

            const int loop_size = indices[loop_index].size();

            nonlocal_pp[i][m].resize(loop_size);
            nonlocal_pp_index[i][m].resize(loop_size);
            if(is_cal_nonlocal_dev){
                nonlocal_dev_x[i][m].resize(loop_size);
                nonlocal_dev_y[i][m].resize(loop_size);
                nonlocal_dev_z[i][m].resize(loop_size);
            }

            Parallel_Manager::info().all_barrier();
            #ifdef ACE_HAVE_OMP
            #pragma omp parallel for
            #endif
            for (int r_ind2 =0; r_ind2 < loop_size; r_ind2++){
                mesh->get_position(r_ind2, x_p, y_p, z_p);
                double x2 = x_p - position[0];
                double y2 = y_p - position[1];
                double z2 = z_p - position[2];
                double r2 = sqrt( x2*x2 + y2*y2 + z2*z2 );
                for (int l=0;l<r_l[itype].size(); l++){
                    vector<double> h_il = h_ij_total[itype].at(l);////;
                    for (int i=1; i<=3; i++){
                        for(int j=1;j<=3; j++){
                                ///////////// R(r)dY/dx start
                            k_ylm = Spherical_Harmonics::Ylm(l, m-l, x2, y2, z2);
                            k_plj_upper = pow(2,0.5) * pow(r2,l+2*(j-1)) *exp(-r2*r2/(2*pow(r_l[itype].at(l),0.5)) );
                            k_plj_down = pow(r_l[itype].at(l),l+4*(j-1)/2) * pow(gamma( l+ (4*j-1)/2 ) ,0.5 );
                            k_plj = k_plj_upper/k_plj_down;
                            k= h_il[l*(i-1)+j] * k_plj * k_ylm;
                            
                            p_li_upper = pow(2,0.5) * pow(r,l+2*(i-1)) *exp(-r*r/(2*pow(r_l[itype].at(l),0.5)) );
                            p_li_down = pow(r_l[itype].at(l),l+4*(i-1)/2) * pow(gamma( l+ (4*i-1)/2 ) ,0.5 );
                            p_li = p_li_upper/p_li_down;
                            for (int ind; ind<3; ind++){
                                after_val[ind] += Spherical_Harmonics::Derivative::dYlm(l,m,x,y,z,ind)*p_li*k;
                            }                
                            //////////////////// R(r)dY/dx end
                            ylm = Spherical_Harmonics::Ylm(l, m-l, x, y, z);
                            p_li_dev1 = (l+2*(i-1))*pow(r, l+2*i-3)*exp(-pow(r/r_l[itype].at(l),2)/2);      ;
                            p_li_dev2 = -(r/pow(r_l[itype].at(l),2.0)) * pow(r, l+2*i-2)*exp(-pow(r/r_l[itype].at(l),2)/2);
                            c =   pow(2,0.5)/p_li_down;
                            if(l+2*i-2 == 0){
                                p_li_dev = c*(p_li_dev2);
                            }
                            else{
                                p_li_dev = c*(p_li_dev1+p_li_dev2);
                            }
                            
                            before_val[0] = (x/r) * ylm * k * p_li_dev;
                            before_val[1] = (y/r) * ylm * k * p_li_dev;
                            before_val[2] = (z/r) * ylm * k * p_li_dev;
                            nonlocal_dev_x[r_ind][m][r_ind2] += before_val[0]+after_val[0];
                            nonlocal_dev_y[r_ind][m][r_ind2] += before_val[1]+after_val[1];
                            nonlocal_dev_z[r_ind][m][r_ind2] += before_val[2]+after_val[2];
                        }
                    }
                }
            }
        }
    }
    
    //for(int i =0;i<number_vps_file[itype];i++){ // angular momentum index
     //       for(int m=0;m<2*l+1;m++){ //  magnetic momentum index
    
    
    
    //}   
    //nonlocal_pp_index.push_back(nonlocal_dev_x.size());
    //nonlocal_pp_index.push_back(nonlocal_dev_y.size());
    //nonlocal_pp_index.push_back(nonlocal_dev_z.size());
*/




    /*
    else if(parameters->sublist("Pseudopotential").get<string>("NonlocalIntegration") == "fine"){
        if(MyPID == 0 and iatom == 0){
            cout << endl;
            cout << "#---------------------------------------------- Hgh2KB::calculate_nonlocal" << endl;
            cout << " Non-local pseudopotential integration: Numerical integration" << endl;
            cout << " Nonlocal threshold = " << threshold << endl;
            cout << "#---------------------------------------------------------------------------" << endl;
        }

        vector<double> I_x0, I_y0, I_z0;
        vector<double> I_x1, I_y1, I_z1;
        vector<double> I_x2, I_y2, I_z2;
        vector<double> I_x3, I_y3, I_z3;
        vector<double> I_x4, I_y4, I_z4;

        for(int i=0;i<number_vps_file[itype];i++){
            int l = oamom_per_proj[itype][i];
            I_x0.clear(); I_y0.clear(); I_z0.clear();
            I_x1.clear(); I_y1.clear(); I_z1.clear();
            I_x2.clear(); I_y2.clear(); I_z2.clear();
            I_x3.clear(); I_y3.clear(); I_z3.clear();

            vector< vector< vector<double> > > nonlocal_pp_tmp;
            vector< vector< vector<int> > > nonlocal_pp_index_tmp;

            switch(l){
                case(0):{
                    vector< vector<double> > nonlocal_pp_tmp_tmp;
                    vector< vector<int> > nonlocal_pp_index_tmp_tmp;
                    for(int j=1; j<=projector_number[itype][i]; j++){
                        if(abs(h_ij_total[itype][i][j-1]) > 1.0E-30){
                            if(j == 1){
                                vector<double> nonlocal_pp_tmp_tmp_tmp;
                                vector<int> nonlocal_pp_index_tmp_tmp_tmp;

                                I_x0 = integrate(r_l[itype][i], 0, 0, position[0]);
                                I_y0 = integrate(r_l[itype][i], 0, 1, position[1]);
                                I_z0 = integrate(r_l[itype][i], 0, 2, position[2]);

                                double constant = 1.0 / (pow(M_PI,0.75) * pow(r_l[itype][i],1.5));

                                int k_x=0, k_y=0, k_z=0;
                                for(int k=0; k<size; k++){
                                    grid_setting->decompose(k, basis->get_points(), k_x, k_y, k_z);
                                    double tmp = constant * I_x0[k_x] * I_y0[k_y] * I_z0[k_z];

                                    if(abs(tmp) > threshold){
                                        nonlocal_pp_tmp_tmp_tmp.push_back(tmp);
                                        nonlocal_pp_index_tmp_tmp_tmp.push_back(k);
                                    }
                                }
                                nonlocal_pp_tmp_tmp.push_back(nonlocal_pp_tmp_tmp_tmp);
                                nonlocal_pp_index_tmp_tmp.push_back(nonlocal_pp_index_tmp_tmp_tmp);
                            }
                            else if(j == 2){
                                vector<double> nonlocal_pp_tmp_tmp_tmp;
                                vector<int> nonlocal_pp_index_tmp_tmp_tmp;

                                I_x2 = integrate(r_l[itype][i], 2, 0, position[0]);
                                I_y2 = integrate(r_l[itype][i], 2, 1, position[1]);
                                I_z2 = integrate(r_l[itype][i], 2, 2, position[2]);

                                double constant = 2.0 / ( sqrt(15) * pow(M_PI,0.75) * pow(r_l[itype][i],3.5) );

                                int k_x=0, k_y=0, k_z=0;
                                for(int k=0; k<size; k++){
                                    grid_setting->decompose(k, basis->get_points(), k_x, k_y, k_z);
                                    double tmp = constant * ( I_x2[k_x] * I_y0[k_y] * I_z0[k_z] + I_x0[k_x] * I_y2[k_y] * I_z0[k_z] + I_x0[k_x] * I_y0[k_y] * I_z2[k_z] );
                                    if(abs(tmp) > threshold){
                                        nonlocal_pp_tmp_tmp_tmp.push_back(tmp);
                                        nonlocal_pp_index_tmp_tmp_tmp.push_back(k);
                                    }
                                }
                                nonlocal_pp_tmp_tmp.push_back(nonlocal_pp_tmp_tmp_tmp);
                                nonlocal_pp_index_tmp_tmp.push_back(nonlocal_pp_index_tmp_tmp_tmp);
                            }
                            else{
                                cout << "Not implemeneted." << endl;
                                exit(EXIT_FAILURE);
                            }
                        }
                        else{
                            nonlocal_pp_tmp_tmp.push_back(vector<double>());
                            nonlocal_pp_index_tmp_tmp.push_back(vector<int>());
                        }
                    }
                    nonlocal_pp_tmp.push_back(nonlocal_pp_tmp_tmp);
                    nonlocal_pp_index_tmp.push_back(nonlocal_pp_index_tmp_tmp);
                } break;

                case(1):{
                    vector< vector<double> > nonlocal_pp_tmp_tmp1;
                    vector< vector<double> > nonlocal_pp_tmp_tmp2;
                    vector< vector<double> > nonlocal_pp_tmp_tmp3;
                    vector< vector<int> > nonlocal_pp_index_tmp_tmp1;
                    vector< vector<int> > nonlocal_pp_index_tmp_tmp2;
                    vector< vector<int> > nonlocal_pp_index_tmp_tmp3;
                    for(int j=1; j<=projector_number[itype][i]; j++){
                        if(abs(h_ij_total[itype][i][j-1]) > 1.0E-30){
                            if(j == 1){
                                vector<double> nonlocal_pp_tmp_tmp_tmp1;
                                vector<double> nonlocal_pp_tmp_tmp_tmp2;
                                vector<double> nonlocal_pp_tmp_tmp_tmp3;
                                vector<int> nonlocal_pp_index_tmp_tmp_tmp1;
                                vector<int> nonlocal_pp_index_tmp_tmp_tmp2;
                                vector<int> nonlocal_pp_index_tmp_tmp_tmp3;

                                I_x0 = integrate(r_l[itype][i],0,0,position[0]);
                                I_y0 = integrate(r_l[itype][i],0,1,position[1]);
                                I_z0 = integrate(r_l[itype][i],0,2,position[2]);
                                I_x1 = integrate(r_l[itype][i],1,0,position[0]);
                                I_y1 = integrate(r_l[itype][i],1,1,position[1]);
                                I_z1 = integrate(r_l[itype][i],1,2,position[2]);

                                double constant = sqrt(2) / ( pow(M_PI,0.75) * pow(r_l[itype][i],2.5) );

                                int k_x=0, k_y=0, k_z=0;
                                for(int k=0; k<size; k++){
                                    grid_setting->decompose(k, basis->get_points(), &k_x, &k_y, &k_z);
                                    double tmp1 = constant * ( I_x0[k_x] * I_y1[k_y] * I_z0[k_z] );
                                    double tmp2 = constant * ( I_x0[k_x] * I_y0[k_y] * I_z1[k_z] );
                                    double tmp3 = constant * ( I_x1[k_x] * I_y0[k_y] * I_z0[k_z] );

                                    if(abs(tmp1) > threshold){
                                        nonlocal_pp_tmp_tmp_tmp1.push_back(tmp1);
                                        nonlocal_pp_index_tmp_tmp_tmp1.push_back(k);
                                    }
                                    if(abs(tmp2) > threshold){
                                        nonlocal_pp_tmp_tmp_tmp2.push_back(tmp2);
                                        nonlocal_pp_index_tmp_tmp_tmp2.push_back(k);
                                    }
                                    if(abs(tmp3) > threshold){
                                        nonlocal_pp_tmp_tmp_tmp3.push_back(tmp3);
                                        nonlocal_pp_index_tmp_tmp_tmp3.push_back(k);
                                    }
                                }
                                nonlocal_pp_tmp_tmp1.push_back(nonlocal_pp_tmp_tmp_tmp1);
                                nonlocal_pp_tmp_tmp2.push_back(nonlocal_pp_tmp_tmp_tmp2);
                                nonlocal_pp_tmp_tmp3.push_back(nonlocal_pp_tmp_tmp_tmp3);
                                nonlocal_pp_index_tmp_tmp1.push_back(nonlocal_pp_index_tmp_tmp_tmp1);
                                nonlocal_pp_index_tmp_tmp2.push_back(nonlocal_pp_index_tmp_tmp_tmp2);
                                nonlocal_pp_index_tmp_tmp3.push_back(nonlocal_pp_index_tmp_tmp_tmp3);
                            }
                            else{
                                cout << "Not implemented." << endl;
                                exit(EXIT_FAILURE);
                            }
                        }
                        else{
                            nonlocal_pp_tmp_tmp1.push_back(vector<double>());
                            nonlocal_pp_tmp_tmp2.push_back(vector<double>());
                            nonlocal_pp_tmp_tmp3.push_back(vector<double>());
                            nonlocal_pp_index_tmp_tmp1.push_back(vector<int>());
                            nonlocal_pp_index_tmp_tmp2.push_back(vector<int>());
                            nonlocal_pp_index_tmp_tmp3.push_back(vector<int>());
                        }
                    }
                    nonlocal_pp_tmp.push_back(nonlocal_pp_tmp_tmp1);
                    nonlocal_pp_tmp.push_back(nonlocal_pp_tmp_tmp2);
                    nonlocal_pp_tmp.push_back(nonlocal_pp_tmp_tmp3);
                    nonlocal_pp_index_tmp.push_back(nonlocal_pp_index_tmp_tmp1);
                    nonlocal_pp_index_tmp.push_back(nonlocal_pp_index_tmp_tmp2);
                    nonlocal_pp_index_tmp.push_back(nonlocal_pp_index_tmp_tmp3);

                } break;

                default:{
                    cout << "Not implemented." << endl;
                    exit(EXIT_FAILURE);
                }

            } // switch
            nonlocal_pp.push_back(nonlocal_pp_tmp);
            nonlocal_pp_index.push_back(nonlocal_pp_index_tmp);
        } // for(int i=0;i<number_vps_file[itype];i++){
    }
    */
}

/*
vector<double> Hgh2KB::integrate(double r_l, int exponent, int axis, double position){
    vector<double> value;

    // points = 500, max = 50.0 : tested for O atom of one H2O molecule
    double max = 7.0 * r_l; // In this case, the exponential term in the integral kernal is smaller than 2.3E-11

    // Adaptive scheme
    for(int i=0;i<basis->get_points()[axis];i++){

        double h = 0.01 * basis->get_scaling()[axis];
        //double h = 0.5 * basis->get_scaling()[axis]; // convergence = 1.0E-10
        int points = 2.0 * max / h;
        h = 2.0 * max / double(points-1);

        double convergence = 0.0;
        double before_val;
        double sum_val = 0.0;

        int j;
        for(j=0; j<2 || convergence > 1.0E-10; j++){
            before_val = sum_val;
            sum_val = 0.0;

            for(int k=0;k<points;k++){
                double sampling_point = - max + double(k) * h;
                sum_val += pow(sampling_point,exponent) * exp( -0.5*sampling_point*sampling_point / (r_l*r_l) ) * basis->compute_1d_basis(i,sampling_point+position,axis) * h;
            }
            convergence = abs(sum_val - before_val);
            points *= 2;
            h = 2.0 * max / double(points-1);
        }
        //cout << "No. of iterations = " << j << endl;
        //cout << "Integration spacing = " << h << endl;
        value.push_back(sum_val);
    }
    // end

    Verbose::single(Verbose::Detail) << "\n=============================================\n";
    Verbose::single(Verbose::Detail) << "Hgh pseudopotential integration\n";
    Verbose::single(Verbose::Detail) << "---------------------------------------------\n";
    Verbose::single(Verbose::Detail) << "                            r_l = " << r_l << endl;
    Verbose::single(Verbose::Detail) << "                       exponent = " << exponent << endl;
    Verbose::single(Verbose::Detail) << "                           axis = " << axis << endl;
    Verbose::single(Verbose::Detail) << "                atomic position = " << position << endl;
  //cout << "        # of integration points = " << points << endl;
    Verbose::single(Verbose::Detail) << "      maximum integration range = " << max << endl;
  //cout << "            integration spacing = " << h << endl;
    Verbose::single(Verbose::Detail) << "=============================================\n";

    return value;
}
*/

void Hgh2KB::pp_information(int itype){
    Verbose::single(Verbose::Simple) << "\n==========================================================\n";
    Verbose::single(Verbose::Simple) << "Pseudopotential file for this atom  : " << hgh_filenames[itype] << endl;
    Verbose::single(Verbose::Simple) << "Valence charge                      : " << Zvals[itype] << endl;
    Verbose::single(Verbose::Simple) << "Number of projector types           : " << number_vps_file[itype] << endl;
    for(int i=0;i<number_vps_file[itype];i++){
        Verbose::single(Verbose::Simple) << "     Orbital angular momentum       : " << oamom_per_proj[itype][i] << endl;
        Verbose::single(Verbose::Simple) << "              # of projectors       : " << projector_number[itype][i] << endl;
    }
    Verbose::single(Verbose::Simple) << "Nonlinear core correction           : " << nonlinear_core_correction[itype] << endl;
    Verbose::single(Verbose::Simple) << "==========================================================\n";

    return;
}

double Hgh2KB::compute_Vlocal(int itype, double r){
    double retval = 0.0;
    double tmp = 0.0;

    // Gaussian term exponential part
    for(int j=0; j<c_i[itype].size(); j++){
        if(j==0) tmp += c_i[itype][j];
        else tmp += c_i[itype][j] * pow(r/rloc[itype], j*2.0);
    }

    // Erf term.
    double tmp2 = 0.0;
    if(r < 1.0E-15){
        tmp2 = sqrt(2.0/M_PI) / rloc[itype];
    }
    else{
        tmp2 = erf( r/(sqrt(2)*rloc[itype]) ) / r;
    }

    retval = -Zvals[itype] * tmp2 + exp( -0.5 * (r*r/(rloc[itype]*rloc[itype])) ) * tmp;

    return retval;
}

double Hgh2KB::compute_Vshort(int itype, double r){
    double retval = 0.0;

    for(int j=0; j<c_i[itype].size(); j++){
        if(j==0) retval += c_i[itype][j];
        else retval += c_i[itype][j] * pow(r/rloc[itype], j*2.0);
    }
    retval *= exp( -0.5 * (r*r/(rloc[itype]*rloc[itype])) );

    return retval;
}

double Hgh2KB::compute_Vshort_dev(int itype, double r){
    double retval1 = 0.0; double retval2 = 0.0;

    for(int j=0; j<c_i[itype].size(); j++){
        if (j==0) retval1 += c_i[itype][j];
        else retval1 += c_i[itype][j] * pow(r/rloc[itype], j*2.0);
        if (j!=0) retval2 += c_i[itype][j] * pow(r/rloc[itype], j*2.0-1) / rloc[itype] * 2*j;
    }
    retval1 *= r/rloc[itype]/rloc[itype] * exp( -0.5 * (r*r/(rloc[itype]*rloc[itype])) );
    retval2 *= exp( -0.5 * (r*r/(rloc[itype]*rloc[itype])) );

    return retval1 + retval2;
}

double Hgh2KB::sinc(double x){
    if(x==0) return 1.0;
    else return sin(M_PI*x)/(M_PI*x);
}

void Hgh2KB::make_local_pp(){
    local_pp.resize(atoms->get_size());
    int size = basis->get_original_size();
    for(int iatom=0; iatom<atoms->get_size(); iatom++){
         local_pp[iatom].resize(size);
         for(int i=0; i<size; i++){
             local_pp[iatom][i] = 0.0;
         }
    }

    local_pp_dev.resize(atoms->get_size());
    //int size = mesh->get_original_size();
    for(int iatom=0; iatom<atoms->get_size(); iatom++){
         local_pp_dev[iatom].resize(size);
         for(int i=0; i<size; i++){
             local_pp_dev[iatom][i] = 0.0;
         }
    }
    
    if( parameters -> sublist("Pseudopotential").get<int>("UsingDoubleGrid") == 0 ){
        vector<std::array<double,3> > positions = atoms->get_positions();
        int *MyGlobalElements = basis->get_map()->MyGlobalElements();
        int NumMyElements = basis->get_map()->NumMyElements();
        if( parameters -> sublist("Pseudopotential").get<int>("UsingFiltering") == 0){
            int size = basis -> get_original_size();
            //double threshold = parameters->sublist("Pseudopotential").get<double>("LocalThreshold");

            Verbose::single(Verbose::Normal) << "\n#------------------------------------------------- Hgh2KB::calculate_local\n";
            Verbose::single(Verbose::Normal) << " Local pseudopotential integration: Gauss quadrature\n";
            Verbose::single(Verbose::Normal) << "#---------------------------------------------------------------------------\n";

            
            for(int iatom=0; iatom<atoms->get_size(); iatom++){
                int itype = atoms->get_atom_type(iatom);
                // Calculate local part of pseudopotential_matrix
                double min_r = 100.0;
                #ifdef ACE_HAVE_OMP
                #pragma omp parallel for
                #endif
                for(int i=0; i<NumMyElements; i++){
                    double x, y, z, r;
                    /*
                    double x, y, z, r, x_p, y_p, z_p;
                    basis->get_position(MyGlobalElements[i], x_p, y_p, z_p);

                    // Distance between the grid point & the atom
                    x = x_p - positions[iatom][0];
                    y = y_p - positions[iatom][1];
                    z = z_p - positions[iatom][2];
                    r = sqrt(x*x + y*y + z*z);
                    //////////////// For testing KHS
    //                r = 0.1;
                    //////////////////
                    */
                    basis -> find_nearest_displacement(MyGlobalElements[i], positions[iatom][0], positions[iatom][1], positions[iatom][2], x, y, z, r);
                    
                    
                    local_pp[iatom][MyGlobalElements[i]] += compute_Vlocal(itype, r);
                    local_pp_dev[iatom][MyGlobalElements[i]] += compute_local_pp_dev(itype, r);
                    
                    if(abs(local_pp[iatom][MyGlobalElements[i]]) < 1.0E-30 && min_r > abs(r)){
                    //    Verbose::single() << "khskhs this is cutoff "  << local_pp[iatom][MyGlobalElements[i]] << std::endl;
                        min_r = abs(r);
                    }
                    //local_pp[MyGlobalElements[i]] += compute_Vlocal(itype, r);
                }
                local_cutoff.push_back(min_r);
                //Verbose::single() << "KHS this is cutoff radius "  << min_r << std::endl;
                // end
            }
        } else {
            Verbose::single(Verbose::Normal) << "\n#------------------------------------------------- Hgh2KB::calculate_local\n";
            Verbose::single(Verbose::Normal) << " Using filtering to represent local pseudopotential\n";
            Verbose::single(Verbose::Normal) << "#---------------------------------------------------------------------------\n";
            for(int iatom=0; iatom<atoms->get_size(); iatom++){
                int itype = atoms->get_atom_type(iatom);
                int rgrid_size = 5*rloc[itype]; double rgrid_dr = 0.01;
                vector<double> rgrid(rgrid_size), drgrid(rgrid_size, rgrid_dr);
                for(int r = 0; r < rgrid_size; ++r){
                    rgrid[r] = r * rgrid_dr;
                }

                vector<double> short_r_pp(rgrid_size), short_r_pp_dev(rgrid_size);
                for(int r = 0; r < rgrid_size; ++r){
                    short_r_pp[r] = compute_Vshort(itype, r * rgrid_dr);
                }
                vector<double> filtered_short_pp, filtered_rgrid;
                // 99.7% of local potential. TODO XXX optimize this.
                filter -> filter_short(short_r_pp, rgrid, drgrid, 3*rloc[itype], filtered_short_pp, filtered_rgrid);
                vector<double> filtered_short_pp_dev = Radial_Grid::Paw::linear_derivative(filtered_short_pp, filtered_rgrid);

                vector<double> y2l = Interpolation::Spline::spline(filtered_rgrid, filtered_short_pp, filtered_rgrid.size());
                vector<double> y2d = Interpolation::Spline::spline(filtered_rgrid, filtered_short_pp_dev, filtered_rgrid.size());
                // Calculate local part of pseudopotential_matrix
                double min_r = 100.0;
                #ifdef ACE_HAVE_OMP
                #pragma omp parallel for
                #endif
                for(int i=0; i<NumMyElements; i++){
                    double x, y, z, r;
                    /*
                    double x, y, z, r, x_p, y_p, z_p;
                    basis->get_position(MyGlobalElements[i], x_p, y_p, z_p);

                    // Distance between the grid point & the atom
                    x = x_p - positions[iatom][0];
                    y = y_p - positions[iatom][1];
                    z = z_p - positions[iatom][2];
                    r = sqrt(x*x + y*y + z*z);
                    */
                    basis -> find_nearest_displacement(MyGlobalElements[i], positions[iatom][0], positions[iatom][1], positions[iatom][2], x, y, z, r);

                    local_pp[iatom][MyGlobalElements[i]] += Interpolation::Spline::splint(filtered_rgrid, filtered_short_pp, y2l, filtered_rgrid.size(), r);
                    local_pp[iatom][MyGlobalElements[i]] += compute_Vlocal(itype, r) - compute_Vshort(itype, r);
                    local_pp_dev[iatom][MyGlobalElements[i]] += Interpolation::Spline::splint(filtered_rgrid, filtered_short_pp_dev, y2d, filtered_rgrid.size(), r);
                    local_pp_dev[iatom][MyGlobalElements[i]] += compute_local_pp_dev(itype, r) - compute_Vshort_dev(itype, r);
                    
                    if(abs(local_pp[iatom][MyGlobalElements[i]]) < 1.0E-30 && min_r > abs(r)){
                        min_r = abs(r);
                    }
                }
                local_cutoff.push_back(min_r);
            }
        }

    }
    else{
        // use double grid
        int *MyGlobalElements = basis->get_map()->MyGlobalElements();
        int NumMyElements = basis->get_map()->NumMyElements();

        Verbose::single(Verbose::Normal) << "\n#------------------------------------------- Hgh2KB::calculate_local_DG\n";
        Verbose::single(Verbose::Normal) << " Using double-grid to represent local pseudopotential\n";
        Verbose::single(Verbose::Normal) << "#---------------------------------------------------------------------------\n";

        int size = basis->get_original_size();
        vector<std::array<double,3> > positions = atoms->get_positions();

        double threshold = parameters->sublist("Pseudopotential").get<double>("LocalThreshold");
        double fine_dimension = parameters->sublist("Pseudopotential").get<int>("FineDimension");

        auto scaling = basis->get_scaling();
        auto points  = basis->get_points();
        const double** scaled_grid = basis->get_scaled_grid();
        std::array<double,3> fine_scaling;
        std::array<int,3>    fine_points;
        for(int d = 0; d < 3; ++d){
            fine_scaling[d] = scaling[d] / fine_dimension;
            fine_points[d] = fine_dimension*(points[d]-1)+1;
        }

        vector<double> rcut;

        for(int itype=0; itype<atoms->get_atom_types().size(); itype++){
            // Find rcut
            double rcut_tmp = 0.0;
            for(int i=0; i<11; i++){
                double r = 10.0 - i;
                double tmp = compute_Vshort(itype, r);
                if(abs(tmp) < 1.0e-4) rcut_tmp = r;
                else break;
            }
            for(int i=0; i<11; i++){
                double r = rcut_tmp - 0.1 * i;
                double tmp = compute_Vshort(itype, r);
                if(abs(tmp) < 1.0e-4) rcut_tmp = r;
                else break;
            }
            for(int i=0; i<11; i++){
                double r = rcut_tmp - 0.01 * i;
                double tmp = compute_Vshort(itype, r);
                if(abs(tmp) < 1.0e-4) rcut_tmp = r;
                else break;
            }
            for(int i=0; i<11; i++){
                double r = rcut_tmp - 0.001 * i;
                double tmp = compute_Vshort(itype, r);
                if(abs(tmp) < 1.0e-4) rcut_tmp = r;
                else break;
            }
            rcut_tmp *= parameters->sublist("Pseudopotential").get<double>("LocalIntegrationRange");
            Verbose::single(Verbose::Normal) << " \"rcut\" for atom " << itype << ": " << rcut_tmp << " Bohr\n";
            rcut.push_back(rcut_tmp);
            // end
        }

        for(int iatom=0; iatom<atoms->get_size(); iatom++){
            int itype = atoms->get_atom_type(iatom);
            RCP<Basis> fine_basis = Create_Basis::Create_Auxiliary_Basis(fine_points, fine_scaling, basis -> get_map() -> Comm(), "Sinc", atoms->operator[](iatom), rcut[itype]);

            int fine_NumMyElements = fine_basis -> get_map() -> NumMyElements();
            int* fine_GlobalMyElements = fine_basis -> get_map() -> MyGlobalElements();

            for(int j=0; j<size; j++){
                double X = 0.0, Y = 0.0, Z = 0.0, R = 0.0;
                /*
                double x_p, y_p, z_p;
                basis-> get_position(j, x_p, y_p, z_p);
                X = x_p - positions[iatom][0];
                Y = y_p - positions[iatom][1];
                Z = z_p - positions[iatom][2];
                R = sqrt( X*X + Y*Y + Z*Z );
                */
                basis -> find_nearest_displacement(j, positions[iatom][0], positions[iatom][1], positions[iatom][2], X, Y, Z, R);

                double Vshort_tmp = 0.0;
                double total_Vshort_tmp = 0.0;
                if(R < rcut[itype]){
                    for(int fine_q=0; fine_q<fine_NumMyElements; fine_q++){
                        double x, y, z, r;
                        /*
                        double x_fine, y_fine, z_fine;
                        fine_basis -> get_position(fine_GlobalMyElements[fine_q], x_fine, y_fine, z_fine);

                        double x = x_fine - positions[iatom][0];
                        double y = y_fine - positions[iatom][1];
                        double z = z_fine - positions[iatom][2];
                        double r = sqrt(x*x + y*y + z*z);
                        */
                        fine_basis -> find_nearest_displacement(MyGlobalElements[fine_q], positions[iatom][0], positions[iatom][1], positions[iatom][2], x, y, z, r);

                        Vshort_tmp += compute_Vshort(itype, r) * sinc((X-x)/scaling[0]) * sinc((Y-y)/scaling[1]) * sinc((Z-z)/scaling[2]);
                    }
                    Parallel_Util::group_sum(&Vshort_tmp, &total_Vshort_tmp, 1);
                    total_Vshort_tmp *= 1.0 / pow(fine_dimension,3.0);
                }

                double Vlocal_tmp = 0.0;
                if(R < 1.0E-15){
                    Vlocal_tmp = sqrt(2.0/M_PI) / rloc[itype];
                }
                else{
                    Vlocal_tmp = erf( R/(sqrt(2.0)*rloc[itype]) ) / R;
                }
                Vlocal_tmp *= -Zvals[itype];

                double Vlocal = Vlocal_tmp + total_Vshort_tmp;
                local_pp[iatom][j] += Vlocal;
                //local_pp[j] += Vlocal;
            }
        }

    }
    return;
}

void Hgh2KB::initialize_pp(){ 
    if(parameters->sublist("Pseudopotential").get<int>("UsingFiltering") == 1){
        double gamma_local = parameters->sublist("Pseudopotential").get<double>("GammaLocal", 2.0);
        double gamma_nonlocal = parameters->sublist("Pseudopotential").get<double>("GammaNonlocal", 2.0);
        double alpha_local = parameters->sublist("Pseudopotential").get<double>("AlphaLocal", 1.1);
        double alpha_nonlocal = parameters->sublist("Pseudopotential").get<double>("AlphaNonlocal", 1.1);
        double eta = parameters->sublist("Pseudopotential").get<double>("Eta",0.0);
        this -> filter = rcp(new Filter(basis, atoms, Zvals, gamma_local, gamma_nonlocal, alpha_local, alpha_nonlocal,eta)); 
    }
    return;
}

double Hgh2KB::compute_V_nl(int itype, int l, int i, int p, double r){
    return sqrt(2) * pow(r, l+2*p) * exp(-0.5*r*r/(r_l[itype][i]*r_l[itype][i])) / (pow(r_l[itype][i], l+(4*(p+1)-1)*0.5) * sqrt(tgamma(l + (4*(p+1)-1)*0.5)));
}

double Hgh2KB::compute_V_nl_dev(int itype, int l, int i, int p, double r){
    double p_li_dev;
    //double p_li = sqrt(2) * pow(r, l+2*p) *  exp(-0.5*r*r/(r_l[itype][i]*r_l[itype][i]))/ (pow(r_l[itype][i], l+(4*(p+1)-1)*0.5) *sqrt(tgamma(l + (4*(p+1)-1)*0.5)));
    double p_li_dev1 = (l+2*p)*pow(r, l+2*p-1)*exp(-r*r*0.5/(r_l[itype][i]*r_l[itype][i]));  
    double p_li_dev2 = -(r/pow(r_l[itype][i],2.0)) * pow(r, l+2*p)*exp(-r*r/(2*r_l[itype][i]*r_l[itype][i]));
    double p_li_down = pow(r_l[itype][i],l+(4*(p+1)-1)*0.5) * sqrt(tgamma( l+ (4*(p+1)-1)*0.5 ));
    double c =   sqrt(2)/p_li_down;
    if(l+2*((p+1)-1) == 0){
        p_li_dev = c*(p_li_dev2);
    }
    else{
        p_li_dev = c*(p_li_dev1+p_li_dev2);
    }
    return p_li_dev;
}

double Hgh2KB::compute_V_nl_dev_dev(int itype, int l, int i, int p, double r){
    // 2019.7 by jhwoo
    double p_li_dev_dev;
    double exp_tmp = exp(- pow(r/r_l[itype][i], 2) / 2);

    double element1 = (l + 2*p - 1) * pow(r, l + 2*p - 2) - pow(r, l + 2*p) / pow(r_l[itype][i], 2);
    // element1 = (l + 2(i-1)) \{ ( l + 2 (i - 1) -1 ) r^{l + 2 (i-1) -2 } exp(- \frac{r^2}{2 r_l^2} ) - \frac{r}{r_l^2} r^{l + 2 (i-1) -1} exp(- \frac{r^2}{2r_l^2}) \}
    element1 *= l + 2*p;
    element1 *= exp_tmp;

    double element2 = -(l + 2*p + 1) * pow(r, l + 2*p) + pow(r, l + 2*p + 2) / pow(r_l[itype][i], 2);
    // element2 = \frac{1}{r_l^2} \{ -(l+2(i-1)+1) r^{l + 2(i-1)} exp(- \frac{r^2}{2 r_l^2}) + r^{l + 2(i-1) + 1} \frac{r}{r_l^2} exp(- \frac{r^2}{2 r_l^2}) \}
    element2 /= pow(r_l[itype][i], 2);
    element2 *= exp_tmp;
   
    double p_li_down = pow(r_l[itype][i],l+(4*(p+1)-1)*0.5) * sqrt(tgamma( l+ (4*(p+1)-1)*0.5 ));
    
    if(l+2*((p+1)-1) == 0){
        p_li_dev_dev = element2; 
    }
    else{
        p_li_dev_dev = element1 + element2;
    }
    p_li_dev_dev /= p_li_down;
    p_li_dev_dev *= sqrt(2);

    return p_li_dev_dev;
}

double Hgh2KB::compute_local_pp_dev(double itype, double r){
    double dev;
    double z = Zvals.at(itype);
    double element1, element2;
   // r = 0.2;
    double tmp_r = r/rloc.at(itype);
    std::setprecision(20);
    if(r >= 1.0E-5){
        element1 = (z/(r*r))*erf(tmp_r/sqrt(2)); // z/r^2 *erf(r)
        element2 = (z/(r*rloc[itype]))*pow(2/M_PI,0.5)*exp(-tmp_r*tmp_r*0.5); // z/r
    }
    else{
        element1 = 0;
        element2 = 0;
    }
    double num_c = c_i.at(itype).size();
    double element3 = 0.0;
    for (int i=0; i<num_c;i++){
        element3 += (tmp_r/rloc[itype]) * exp(-tmp_r*tmp_r/2) * ( c_i[itype].at(i) * pow(tmp_r, 2*i) );    // r^(2i+1)
    }
    double element4 = 0.0;
    for (int i=1; i<num_c;i++){
        element4 += exp(-tmp_r*tmp_r/2) *2*i * ( c_i[itype].at(i) * pow(tmp_r, 2*i-1)/rloc[itype] ) ;   // r^(2i-1)
    }
    dev = element1 -element2 -element3 + element4;
//    Verbose::single() << "local_dev_val : " << dev << std::endl;
    return dev;
}

// 2019.7 by jhwoo
double Hgh2KB::compute_local_pp_dev_dev(double itype, double r){
    // return d^2(v_loc)/dr^2
    double dev_dev;
    double z = Zvals.at(itype);

    double r_tmp = 0.0;
    // r_tmp = r/r_loc
    r_tmp = r / rloc[itype];
    double exp_tmp = 0.0;
    // exp_tmp = exp(-(r/r_loc)^2 / 2)
    exp_tmp = exp( -pow(r_tmp, 2) / 2 );

    double element1, element2;
    if(r >= 1.0E-5){
        // element1 = \frac{2z}{\sqrt{\pi} r} exp(\frac{-r^2}{2r_loc^2}) \{\frac{1}{r^2} - \frac{1}{r_loc^2} \}
        element1 = (-2) / sqrt(M_PI) / rloc[itype] * exp_tmp * (pow(r, -2) + pow(rloc[itype], -2));
        element1 *= z;

        // element2 = \frac{z}{r^2} \{ \frac{ \sqrt{frac{2}{\pi}} exp(- \frac{r^2}{2 r_loc^2})}{r_loc} - \frac{2 erf(\frac{r}{\sqrt{2} r_loc})}{r} \}
        element2 = sqrt(2/M_PI) * exp_tmp / rloc[itype] - 2 * erf( r / (sqrt(2) * rloc[itype]) ) / r;
        element2 *= pow(r, -2);
        element2 *= z;
    }
    else{
        element1 = 0;
        element2 = 0;
    }

    double element3 = 0.0;
    // element3 = exp(- \frac{r^2}{2 r_loc^2} ) \{ - \frac{ C_1 + C_2 (frac{r}{r_loc})^2 + C_3 (frac{r}{r_loc})^4 + C_4 (frac{r}{r_loc})^6 + \frac{r}{r_loc} ( 2 C_2 \frac{r}{r_loc}
    // 4 C_3 (\frac{r}{r_loc})^3 + 6 C_4 (\frac{r}{r_loc})^5 ) }{r_loc^2} + \frac{2 C_2 r}{r_loc^2} + \frac{4 C_3 r^3}{r_loc^4} + \frac{6 C_4 r^5}{r_loc^6} \}
    element3 -= ( c_i[itype].at(0) + c_i[itype].at(1)*pow(r_tmp, 2) + c_i[itype].at(2)*pow(r_tmp, 4) + c_i[itype].at(3)*pow(r_tmp, 6) + 2 * c_i[itype].at(1)*pow(r_tmp, 2) + 4 * c_i[itype].at(2)*pow(r_tmp, 4) + 6 * c_i[itype].at(3)*pow(r_tmp, 6) ) / pow(rloc[itype], 2);
    element3 += ( 2 * c_i[itype].at(1) + 12 * c_i[itype].at(2) * pow(r_tmp, 2) + 30 * c_i[itype].at(3) * pow(r_tmp, 4) ) / pow(rloc[itype], 2);
    element3 *= exp_tmp; 
    // c_i[itype][0] = C_1
    // c_i[itype][1] = C_2
    // c_i[itype][2] = C_3
    // c_i[itype][3] = C_4
    
    double element4 = 0.0;
    // element4 = \frac{ r exp( - \frac{r^2}{2 r_loc^2})}{r_loc^2} \{ \frac{ r ( C_1 + C_2 (\frac{r}{r_loc})^2 + C_3 (\frac{r}{r_loc})^4 + C_4 (\frac{r}{r_loc})^6)}{r_loc^2} 
    // - ( \frac{2 C_2 r}{r_loc^2} + \frac{4 C_3 r^3}{r_loc^4} + \frac{6 C_4 r^5}{r_loc^6}  ) \}
    element4 += r * ( c_i[itype].at(0) + c_i[itype].at(1)*pow(r_tmp, 2) + c_i[itype].at(2)*pow(r_tmp, 4) + c_i[itype].at(3)*pow(r_tmp, 6) ) / pow(rloc[itype], 2);
    element4 -= ( 2 * c_i[itype].at(1)*pow(r_tmp, 1) + 4 * c_i[itype].at(2)*pow(r_tmp, 3) + 6 * c_i[itype].at(3)*pow(r_tmp, 5) ) / rloc[itype];
    element4 *= r * exp_tmp;
    element4 /= pow(rloc[itype], 2);

    dev_dev = element1 + element2 + element3 + element4; 

    return dev_dev;
}
//void Hgh2KB::compute_non_local_pp_dev(double itype, double r, double l, double x, double y, double z, double x2, double y2, double z2, double r2){
/* // made by HS in 2019.05.29
void Hgh2KB::calculate_nonlocal(int iatom,std::array<double,3> position,
                                    vector< vector< vector<double> > >& nonlocal_pp,
                                    vector< vector< vector<double> > >& nonlocal_dev_x,
                                    vector< vector< vector<double> > >& nonlocal_dev_y,
                                    vector< vector< vector<double> > >& nonlocal_dev_z,
                                    vector< vector< vector<int> > >& nonlocal_pp_index )
{
    double k_plj_upper, k_plj_down, k_plj;
    double p_li_upper, p_li_down, p_li;
    double ylm, k_ylm;
    double tmp2 = 0.0;
    double p_li_dev, p_li_dev1, p_li_dev2  ;
    double k; // This is values that don't have 'r' term.
    double c; // constant_value
    double x_p, y_p, z_p;


    int size = mesh->get_original_size();

    for (int r_ind =0; r_ind < size; r_ind++){
        mesh->get_position(r_ind, x_p, y_p, z_p);
        double x = x_p - position[0];
        double y = y_p - position[1];
        double z = z_p - position[2];
        double r = sqrt( x*x + y*y + z*z );
        for (int r_ind2 =0; r_ind2 < size; r_ind2++){
            mesh->get_position(r_ind2, x_p, y_p, z_p);
            double x2 = x_p - position[0];
            double y2 = y_p - position[1];
            double z2 = z_p - position[2];
            double r2 = sqrt( x2*x2 + y2*y2 + z2*z2 );
            double after_val[3];
            double before_val[3];
            for (int l=0;l<r_l.size(); l++){
                vector<double> h_il = h_ij_total[itype].at(l);////;
                for (int tmp_ind; tmp_ind<3;tmp_ind++){
                    after_val[tmp_ind] = 0.0;
                    before_val[tmp_ind] = 0.0;
                }
                for(int m= 0; m <2*l+1; m++){
                    for (int i=1; i<=3; i++){
                        for(int j=1;j<=3; j++){
                                ///////////// R(r)dY/dx start
                            k_ylm = Spherical_Harmonics::Ylm(l, m, x2, y2, z2);
                            k_plj_upper = pow(2,0.5) * pow(r2,l+2*(j-1)) *exp(-r2*r2/(2*pow(r_l[itype].at(l),0.5)) );
                            k_plj_down = pow(r_l[itype].at(l),l+4*(j-1)/2) * pow(gamma( l+ (4*j-1)/2 ) ,0.5 );
                            k_plj = k_plj_upper/k_plj_down;
                            k= h_il[l*(i-1)+j] * k_plj * k_ylm;
                            
                            p_li_upper = pow(2,0.5) * pow(r,l+2*(i-1)) *exp(-r*r/(2*pow(r_l[itype].at(l),0.5)) );
                            p_li_down = pow(r_l[itype].at(l),l+4*(i-1)/2) * pow(gamma( l+ (4*i-1)/2 ) ,0.5 );
                            p_li = p_li_upper/p_li_down;
                            for (int ind; ind<3; ind++){
                                after_val[ind] += Spherical_Harmonics::Derivative::dYlm(l,m,x,y,z,ind)*p_li*k;
                            }                
                            //////////////////// R(r)dY/dx end
                            ylm = Spherical_Harmonics::Ylm(l, m, x, y, z);
                            p_li_dev1 = (l+2*(i-1))*pow(r, l+2*i-3)*exp(-pow(r/r_l[itype].at(l),2)/2);      ;
                            p_li_dev2 = -(r/pow(r_l[itype].at(l),2.0)) * pow(r, l+2*i-2)*exp(-pow(r/r_l[itype].at(l),2)/2);
                            c =   pow(2,0.5)/p_li_down;
                            if(l+2*i-2 == 0){
                                p_li_dev = c*(p_li_dev2);
                            }
                            else{
                                p_li_dev = c*(p_li_dev1+p_li_dev2);
                            }
                            before_val[0] += (x/r) * ylm * k * p_li_dev;
                            before_val[1] += (y/r) * ylm * k * p_li_dev;
                            before_val[2] += (z/r) * ylm * k * p_li_dev;
                        }
                    }
                
                nonlocal_dev_x[r_ind][m][r_ind2] += before_val[0]+after_val[0];
                nonlocal_dev_y[r_ind][m][r_ind2] += before_val[1]+after_val[1];
                nonlocal_dev_z[r_ind][m][r_ind2] += before_val[2]+after_val[2];
                
                }
            }
        }
    }
    
       
    nonlocal_pp_index.push_back(nonlocal_dev_x.size());
    nonlocal_pp_index.push_back(nonlocal_dev_y.size());
    nonlocal_pp_index.push_back(nonlocal_dev_z.size());


    return;
}

*/
