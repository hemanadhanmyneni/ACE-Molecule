#pragma once
#include <vector>
#include <string>
#include <algorithm>

#include "Teuchos_ParameterList.hpp"
#include "Teuchos_RCP.hpp"
#include "Epetra_Vector.h"

#include "KBprojector.hpp"

/**
 * @details HGH projector function form is both l- and i- dependent. So it is natural to count like "2 projectors for l=0".
 *          To make it looks like |i>D_ij<j|, two indicies are merged into one in calculate_nonlocal.
 *          Respective angular momentum is oamom. oamom_per_proj is always [0, 1, 2, ...]
 * @author Kwangwoo Hong?, Sungwoo Kang, etc.
 * 
 **/
class Hgh2KB: public KBprojector {
  public:
    Hgh2KB(Teuchos::RCP<const Basis> basis,Teuchos::RCP<const Atoms> atoms, Teuchos::RCP<Teuchos::ParameterList> parameters);

    void read_pp();
    void compute_nonlinear_core_correction();

  protected:
    Teuchos::Array<std::string> hgh_filenames;
    std::string hgh_filename;
    std::string type;
    std::string xc_type;

    // Variables for local pseudopotentials
    std::vector<double> rloc;
    std::vector<int> nloc;
    std::vector< std::vector<double> > c_i;

    std::vector< std::vector<double> > local_mesh;

    // Variables for non-local pseudopotentials
    std::vector< std::vector<double> > r_l;
    std::vector< std::vector < std::vector<double> > > k_ij_total;

    // Variables for nlcc
    std::vector<bool> nonlinear_core_correction;
    std::vector<double> rcore;
    std::vector<double> ccore;
    std::vector< std::vector<int> > projector_number;//< Number of nonzero projector functions. Index: atom type, angular momentum.
    std::vector< std::vector<int> > oamom_per_proj;///< Following HGH specification it should be [0, 1, 2, ...] per each atom type.

    int read_pao(int itype);
    void pp_information(int itype);

    void initialize_pp();
    void initialize_parameters();

    std::vector<double> integrate(double r_l, int exponent, int axis, double position);
    //void calculate_local(Teuchos::RCP<Epetra_Vector>& local_potential);
    //void calculate_local_DG(Teuchos::RCP<Epetra_CrsMatrix>& core_hamiltonian);
    virtual void calculate_nonlocal(int iatom, std::array<double,3> position,
        std::vector<std::vector<std::vector<double> > >& nonlocal_pp,
        std::vector<std::vector<std::vector<double> > >& nonlocal_dev_x,
        std::vector<std::vector<std::vector<double> > >& nonlocal_dev_y,
        std::vector<std::vector<std::vector<double> > >& nonlocal_dev_z,
        std::vector<std::vector<std::vector<int> > >& nonlocal_pp_index);
    void construct_matrix(Teuchos::RCP<Epetra_CrsMatrix>& core_hamiltonian);

    void get_comp_potential(std::vector<double> basis, int itype, std::vector<double>& comp_potential);
    double compute_Vlocal(int itype, double r);
    double compute_Vshort(int itype, double r);
    double compute_Vshort_dev(int itype, double r);
    double sinc(double x);
    double compute_local_pp_dev(double itype, double r);
    double compute_local_pp_dev_dev(double itype, double r);
    //std::vector<double> local_pp;

    //std::vector< std::vector<double> > local_pp;
    //std::vector< std::vector<double> > local_pp_dev;

    //from here, this part is added by jaechang
    virtual void make_local_pp();
    //void make_local_nonlocal_pp();
    //void arrange_local_pp();
    /**
     * @brief compute V_nl value for r.
     * @param itype Atom type.
     * @param l angular momentum.
     * @param i number_vps_file index.
     * @param p projector_number index.
     * @param r Radial position.
     **/
    double compute_V_nl(int itype, int l, int i, int p, double r);
    /**
     * @brief compute V_nl derivative value for r.
     * @param itype Atom type.
     * @param l angular momentum.
     * @param i number_vps_file index.
     * @param p projector_number index.
     * @param r Radial position.
     **/
    double compute_V_nl_dev(int itype, int l, int i, int p, double r);
    double compute_V_nl_dev_dev(int itype, int l, int i, int p, double r);
};
