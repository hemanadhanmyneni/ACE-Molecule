#pragma once
#include <vector>
#include <string>

#include "Epetra_CrsMatrix.h"
#include "Teuchos_RCP.hpp"
#include "Teuchos_Array.hpp"
#include "Teuchos_ParameterList.hpp"

#include "../../Io/Atoms.hpp"
#include "../../Basis/Basis.hpp"
#include "../Occupation/Occupation.hpp"
#include "KBprojector.hpp"
#include "Nuclear_Potential_Matrix.hpp"
#include "../../Utility/Time_Measure.hpp"
#ifdef USE_PAW
#include "Paw.hpp"
#endif

/**
 * @brief Wrapper class for PAW and Kbprojector.
 * @date 2015.12
 * @author Sungwoo Kang
 **/
class Nuclear_Potential{
    public:
        /**
         * @brief Constructor. Create Paw or KBprojector class from parameters.
         * @param[in] basis Mesh describing the system.
         * @param[in] atoms Atom types and positions.
         * @param[in] parameters Parameters describing the PAW or pseudopotential methods.
         **/
        Nuclear_Potential(
            Teuchos::RCP<const Basis> basis,
            Teuchos::RCP<const Atoms> atoms,
            Teuchos::RCP<Teuchos::ParameterList> parameters
        );

        /**
         * @brief Destructor. Print time info.
         **/
        ~Nuclear_Potential();

        /**
         * @brief Identifies current methods.
         * @return "KBprojector" if using KBprojector, "PAW" if using PAW.
         **/
        std::string get_type();
        /**
         * @brief Returns list of valence electrons.
         * @return List of (Ztot-Zcore). Atom type order.
         **/
        std::vector<double> get_Zvals();
        /**
         * @brief Returns list of effective nuclear charges.
         * @details Returns the same result with get_Zvals for KBprojector. Only different for PAW.
         * @return List of effective nuclear charges. Atom type order.
         **/
        std::vector<double> get_Zeffs();

        /**
         * @brief Returns overlap_matrix. Teuchos::null if overlap matridx is not in formalism.
         * @param[out] overlap_matrix Output overlap matrix.
         * @return 1 for KBprojector, 3 for PAW.
         **/
        int get_overlap_matrix( Teuchos::RCP<Epetra_CrsMatrix> &overlap_matrix);
        /**
         * @brief Initialize Nuclear_Potential_Matrix class.
         * @details Calls set_stat0c_local_pot_to_matrix, set_nonlocal_pot_proj_to_matrix.
         *          Also sets nonlocal potential coefficient.
         * @param[in,out] npm Input and Output Nuclear_Potential_Matrix class.
         * @return 1 for KBprojector, 3 for PAW.
         **/
        int initialize_matrix(Teuchos::RCP<Nuclear_Potential_Matrix> &npm);

        /**
         * @brief Return pseudo core electron density and its gradients.
         * @param[out] core_density Pseudo core electron density. Output.
         * @param[out] core_density_grad Pseudo core electron density gradient. Output.
         * @return 1 for KBprojector, 3 for PAW.
         **/
        int get_pseudo_core_electron_info(
            Teuchos::Array< Teuchos::RCP<Epetra_Vector> > &core_density,
            Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > &core_density_grad
        );

        /**
         * @brief Mix nuclear potential.
         * @details This was implemented to match the mixing of PAW atom-centered density matrix to valence density.
         * @param[in] coeffs List of mixing coefficient for last few density matrixes.
         * @param[in] alpha Mixing coefficient.
         * @param[in] ispin Spin index.
         * @return 1 for KBprojector, 3 for PAW.
         **/
        int mix_nuclear_potential(
            std::vector<double> coeffs, double alpha, int ispin
        );

        /**
         * @brief Returns initial atom-centered density matrix.
         * @details Dynamic descriptor means atom-centered density matrix, linearlized, for PAW and nothing for KBprojector.
         * @param[out] descriptor For PAW, this is atom-centered density matrix with index order of [spin][linearlized atom index, matrix row, matrix column index]. For KBprojector, this should be empty.
         * @return 1 for KBprojector, 3 for PAW.
         * @note This was implemented to match the PAW atom-centered density matrix mixing to valence density.
         **/
        int get_initial_atomic_density_matrix(
            std::vector< std::vector<double> > &descriptor
        );

        /**
         * @brief Returns current atom-centered density matrix.
         * @details Dynamic descriptor means atom-centered density matrix, linearlized, for PAW and nothing for KBprojector.
         * @param[out] descriptor For PAW, this is atom-centered density matrix with index order of [spin][linearlized atom index, matrix row, matrix column index]. For KBprojector, this should be empty.
         * @return 1 for KBprojector, 3 for PAW.
         * @note This was implemented to match the PAW atom-centered density matrix mixing to valence density.
         **/
        int get_atomic_density_matrix(
            std::vector< std::vector<double> > &descriptor
        );

        /**
         * @brief Set atom-centered density matrix.
         * @details Dynamic descriptor means atom-centered density matrix, linearlized, for PAW and nothing for KBprojector.
         * @param[in] descriptor For PAW, this is atom-centered density matrix with index order of [spin][linearlized atom index, matrix row, matrix column index]. For KBprojector, this should be empty.
         * @return 1 for KBprojector, 3 for PAW.
         * @note This was implemented to match the PAW atom-centered density matrix mixing to valence density.
         **/
        int set_atomic_density_matrix(
            std::vector< std::vector<double> > descriptor
        );

        /**
         * @brief Correction to the hartree potential.
         * @details This does nothing to KBprojector, and adds hartree potential for PAW. Actually this updates internal fine hartree potential data for PAW.
         * @param[in,out] hartree_potential Input, and updated hartree potential. Input and output.
         * @return 1 for KBprojector, 3 for PAW.
         **/
         // * @param occupations Occupation of current state. Unnecessary since this function does not always update PAW atom-centered density matrix anymore.
         // * @param orbitals Orbitals of current state. Unnecessary since this function does not always update PAW atom-centered density matrix anymore.
         // * @param density Density to re-calculate hartree potential. Unnecessary since the Paw::hartree_vector_update does not re-calculate hartree potential anymore.
        int get_hartree_potential_correction(
            //Teuchos::Array< Teuchos::RCP<const Occupation> > occupations,
            //Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbitals,
            Teuchos::Array< Teuchos::RCP<Epetra_Vector> > density,
            Teuchos::RCP<Epetra_Vector> &hartree_potential
        );

        /**
         * @brief Update energy according to the formalism.
         * @details This does nothing for KBprojector, and only effective for PAW.
         * @param[in] occupations Occupations of the current state.
         * @param[in] orbitals Orbitals of the current state.
         * @param[in] hartree_vector Hartree potential, necessary for PAW calculations.
         * @param[in,out] hartree_energy Corrected hartree energy. For PAW, this is completely re-calculated since initial hartree_energy does not consider core density at all. Input and output.
         * @param[in,out] int_n_vxc Corrected \f$ \int n \times v_{xc} \f$. Input and output.
         * @param[in,out] x_energy Corrected exchange energy. Input and output.
         * @param[in,out] c_energy Corrected correlation energy. Input and output.
         * @param[in,out] kinetic_energy Corrected kinetic energy. Input and output.
         * @param[in,out] external_energy Corrected external energy. Note that external energy means zero correction for PAW, unlike KBprojector which it stands for nuclear-electron interaction. Such interaction is included in hartree_energy for PAW and is hard to separate. Input and output.
         * @return 1 for KBprojector, 3 for PAW.
         * @note Currently, int_n_vxc is not update for PAW since it takes considerable time.
         * @note Currently, external energy updating is not implemented.
         **/
        int get_energy_correction(
            Teuchos::Array< Teuchos::RCP<const Occupation> > occupations,
            Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbitals,
            Teuchos::RCP<Epetra_Vector> &hartree_vector,
            double &hartree_energy,
            double &int_n_vxc,
            double &x_energy,
            double &c_energy,
            double &kinetic_energy,
            std::vector<double> &kinetic_energies,
            double &numerical_correction,
            double &external_energy
        );

        /**
         * @brief Update atom-centered density matrix.
         * @details This does nothing for KBprojector, and updates PAW atomcenter density matrix for PAW.
         * @param[in] occupations Occupations of the current state.
         * @param[in] orbitals Orbitals of the current state.
         * @return 1 for KBprojector, 3 for PAW.
         * @todo Probably set Core_Hamiltonian as friend and move this to protected
         *       to avoid direct call and confusion with Core_Hamiltonian_Matrix::update.
         **/
        int update(
            Teuchos::Array< Teuchos::RCP<const Occupation> > occupations,
            Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbitals
        );
        /*
        int get_density_for_hartree_potential(
            Teuchos::Array< Teuchos::RCP<Epetra_Vector> > in_density,
            Teuchos::Array< Teuchos::RCP<Epetra_Vector> > &out_density
        );
        */

        /**
         * @brief Get density correction.
         * @details This does nothing for KBprojector, and turns pseudo valence density to all-electron valence density. Does nothing with core density, though.
         * @param[in] occupations Occupations of the current state.
         * @param[in] orbitals Orbitals of the current state.
         * @param[in,out] density Density to correct. Input and output.
         * @return 1 for KBprojector, 3 for PAW.
         **/
        int get_density_correction(
            Teuchos::Array< Teuchos::RCP<const Occupation> > occupations,
            Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbitals,
            Teuchos::Array< Teuchos::RCP<Epetra_Vector> > &density
        );

        /**
         * @brief Get orbial correction.
         * @details This does nothing for KBprojector, and turns pseudo orbitals to all-electron orbitals.
         * @param[in] occupations Occupations of the current state.
         * @param[in,out] orbitals Orbitals to update. Input and output.
         * @return 1 for KBprojector, 3 for PAW.
         **/
        int get_orbital_correction(
            Teuchos::Array< Teuchos::RCP<const Occupation> > occupations,
            Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > &orbitals
        );

        /**
         * @brief Calculate external energy.
         * @details For KBprojector, this returns nuclear-electron interaction; the sum of the expectation values of pseudopotential matrix. For PAW, this returns the interaction between zero potential and electron density.
         * @param[in] occupations Occupations of the current state.
         * @param[in] orbitals Orbitals of the current state.
         * @param[in] npm Nuclear_Potential_Matrix class.
         * @param[in,out] energy Resulting corrected energy. Input and output.
         * @return 1 for KBprojector, 3 for PAW.
         **/
        int calculate_external_energy(
             Teuchos::Array< Teuchos::RCP<Occupation> > occupations,
             Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > orbitals,
             Teuchos::RCP<Nuclear_Potential_Matrix> npm,
             double &energy
        );

        /**
         * @brief Calculate nuclear-nuclear interaction.
         * @details For KBprojector, this is the coulomb interaction between pseudo nuclear charge. For PAW, it is zero.
         * @param[in,out] nuclear_nuclear_repulsion Coulomb interaction between pseudo nuclear charge for KBprojector. Input and output.
         * @return 1 for KBprojector, 3 for PAW.
         * @note This function contains support for model potential (parameter Pseudopotential 2), which returns 0. If the parameter Pseudopotential is not 1, 2, 3, then this returns exact coulomb interaction.
         **/
        int calculate_nuclear_repulsion(double &nuclear_nuclear_repulsion);

        /**
         * @brief Set local potential to matrix.
         * @details For KBprojector, this set V_local to npm. For PAW, this set zero potential to npm.
         * @param[in,out] npm Nuclear_Potential_Matrix class. Input and output.
         * @return 1 for KBprojector, 3 for PAW.
         **/
        int set_static_local_pot_to_matrix(
            Teuchos::RCP<Nuclear_Potential_Matrix> &npm
        );

        /**
         * @brief Set nonlocal projector to matrix.
         * @details For KBprojector, this set V_nl to matrix. For PAW, this set projector function to matrix.
         * @param[in,out] npm Nuclear_Potential_Matrix class. Input and output.
         * @return 1 for KBprojector, 3 for PAW.
         **/
        int set_nonlocal_pot_proj_to_matrix(
            Teuchos::RCP<Nuclear_Potential_Matrix> &npm
        );

        /**
         * @brief Set nonlocal potential to matrix, which will not be changed during the run.
         * @details For KBprojector-UPF, this set EKB to matrix. For KBprojector-HGH, this set h_ij to matrix. For PAW, this does nothing.
         * @param[in,out] npm Nuclear_Potential_Matrix class. Input and output.
         * @return 1 for KBprojector, 3 for PAW.
         **/
        int set_static_nonlocal_pot_coeff_to_matrix(
            Teuchos::RCP<Nuclear_Potential_Matrix> &npm
        );

        /**
         * @brief Update nonlocal potential to matrix using occupations and orbitals.
         * @details For KBprojector, this does nothing. For PAW, this calculates and update hamiltonian correction to matrix, and ignores static_nonlocal_pot (since it is zero for PAW).
         * @param[in,out] npm Nuclear_Potential_Matrix class. Input and output.
         * @return 1 for KBprojector, 3 for PAW.
         **/
         // * param occupations Occupations of the current state.
         // * param orbitals Orbitals of the current state.
        int set_dynamic_nonlocal_pot_coeff_to_matrix(
            //Teuchos::Array< Teuchos::RCP<const Occupation> > occupations,
            //Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbitals,
            Teuchos::RCP<Nuclear_Potential_Matrix> &npm
        );

        Teuchos::RCP<KBprojector> get_kbprojector();
#ifdef USE_PAW
        /**
         * @brief Return Paw class.
         * @return Paw class. Teuchos::null is using KBprojector.
         */
        Teuchos::RCP<Paw> get_paw();
#endif
        bool is_mixed = false;
        bool using_compensation_charge();
    protected:
        int type;
        int spin_size;
        bool optimize_pp_vectors = false;
        Teuchos::RCP<const Basis> basis;
        Teuchos::RCP<const Atoms> atoms;
        Teuchos::RCP<KBprojector> kbprojector;
        Teuchos::RCP<Time_Measure> timer;
#ifdef USE_PAW
        Teuchos::RCP<Paw> paw;
#endif
};
