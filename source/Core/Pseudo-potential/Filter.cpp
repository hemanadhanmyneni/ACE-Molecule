#include "Filter.hpp"
#include "../../Utility/ACE_Config.hpp"

#include <cmath>
#include <iostream>

#include "../../Io/Periodic_table.hpp"
#include "../../Basis/Basis_Function/Basis_Function.hpp"
#include "../../Basis/Grid_Setting/Grid_Setting.hpp"
#include "../../Utility/Math/Bessel.hpp"
#include "../../Utility/Interpolation/Spline_Interpolation.hpp"
#include "../../Utility/Interpolation/Radial_Grid_Paw.hpp"
#include "../../Utility/Calculus/Integration.hpp"
#include "../../Utility/String_Util.hpp"
#include "../../Utility/Verbose.hpp"

using std::min;
using std::vector;
using Teuchos::RCP;
using Teuchos::rcp;
using Teuchos::Array;

/*
@brief Filter class cut off high frequency component from local & nonlocal pseudopotential 
@details Filter class implement filtering method introduced from following reference; A general and efficient pseudopotential Fourier filtering scheme for real space methods using mask functions. The Journal of chemical physics, 124(17), 174102, 2006
*/
Filter::Filter(RCP<const Basis> basis, 
               RCP<const Atoms> atoms, 
               vector<double> Zvals, 
               double gamma_local, 
               double gamma_nonlocal, 
               double alpha_local, 
               double alpha_nonlocal, 
               double eta
              )
               :basis(basis),
                Zvals(Zvals),
                atoms(atoms),
                gamma_local(gamma_local),
                gamma_nonlocal(gamma_nonlocal),
                alpha_local(alpha_local), 
                alpha_nonlocal(alpha_nonlocal),
                eta(eta){
    comp_charge_exps.clear();
    if(alpha_local<=1.0){
        Verbose::all()<< " Alpha local value should be bigger than 1.0. Input value is " << alpha_local << std::endl;
        exit(EXIT_FAILURE);
    }
    if(alpha_nonlocal<=1.0){
        Verbose::all()<< " Alpha nonlocal value should be bigger than 1.0. Input value is " << alpha_local << std::endl;
        exit(EXIT_FAILURE);
    }
//    this -> initialize_mask_function();
}

int Filter::filter_local(vector<double> original_f, vector<double> r_g, vector<double> dr_g, int itype, vector<double>& new_f, vector<double>& new_mesh){
    vector<double> new_short_potential_radial_tmp;
    vector<double> short_potential_radial_tmp(r_g.size());
    vector<double> comp_potential_radial_tmp;
    vector<double> short_mesh_tmp;

    this ->get_comp_potential(r_g, comp_potential_radial_tmp, itype);

    vector<double> local_mesh = r_g;
    double local_cutoff = r_g[r_g.size()-1];

    #ifdef ACE_HAVE_OMP
    #pragma omp parallel for
    #endif
    for (int i =0;i<r_g.size(); i++){
        short_potential_radial_tmp.at(i) = (original_f.at(i) + comp_potential_radial_tmp.at(i));
    }
    
    double rcut_tmp = 0.0;
    for(int i=0; i<short_potential_radial_tmp.size(); i++){
        if(abs(short_potential_radial_tmp[i]) < 1.0E-4 ){
            rcut_tmp = r_g[i];
            break;
        }
    }
    double short_rcut = rcut_tmp * this -> gamma_local;

    this -> filter_short(short_potential_radial_tmp, r_g, dr_g, rcut_tmp, new_short_potential_radial_tmp, short_mesh_tmp);

    int short_size = short_mesh_tmp.size();
    /*
    double yp1 = (new_short_potential_radial_tmp[1] - new_short_potential_radial_tmp[0])/(short_mesh_tmp[1] - short_mesh_tmp[0]);
    double ypn = (new_short_potential_radial_tmp[short_size-1] - new_short_potential_radial_tmp[short_size-2])/(short_mesh_tmp[short_size-1] - short_mesh_tmp[short_size-2]);
    vector<double> y2 = Interpolation::Spline::spline(short_mesh_tmp, new_short_potential_radial_tmp, short_size, yp1, ypn);
    */
    vector<double> y2 = Interpolation::Spline::spline(short_mesh_tmp, new_short_potential_radial_tmp, short_size);

    new_mesh = r_g;
    new_f.resize(new_mesh.size());
    for(int r = 0; r < new_mesh.size(); ++r){
        new_f[r] = - comp_potential_radial_tmp[r];
        if(new_mesh[r] <= rcut_tmp * this -> gamma_local){
            double Vshort = Interpolation::Spline::splint(short_mesh_tmp, new_short_potential_radial_tmp, y2, short_size, new_mesh[r]);
            new_f[r] += Vshort;
        }
    }
    return 0;
}

int Filter::filter_short(vector<double> original_f, vector<double> r_g, vector<double> dr_g, double rcut, vector<double>& new_f, vector<double>& new_mesh){
    if (original_f.size()!=r_g.size() or original_f.size()!=dr_g.size() ){
        Verbose::all() << "Error!!!  length of original_f is not equal to length of r_g or dr_g" << std::endl;
        Verbose::all() << "size of r_g: " << r_g.size() << std::endl;
        Verbose::all() << "size of dr_g: " << dr_g.size() << std::endl;
        Verbose::all() << "size of original_f: " << original_f.size() << std::endl;
        return -1;
    }

    double cutoff_r = rcut * gamma_local;

    std::array<double,3> scaling;

    scaling[0] = basis->get_scaling()[0];
    scaling[1] = basis->get_scaling()[1];
    scaling[2] = basis->get_scaling()[2];

    double h = std::max( std::max( scaling[0],scaling[1] ),scaling[2] ) ;

    double max_freq = M_PI/h ;
    double q_cut = max_freq / alpha_local;
    double beta_fs = 5*log(10)/pow((alpha_local-1),2);
    double dq = (M_PI / h) * 0.01;
    double dr = h * 0.01;

    /////////////// generate new R basis  /////////////////////////
    vector<double> new_r_g;
    vector<double> new_dr_g;
    double value = 0.0;
    while(value <= cutoff_r*2){
        new_r_g.push_back(value);
        new_dr_g.push_back(dr);
        value+=dr;
    }
    new_mesh = new_r_g;

    /////////////// generate basis on frequency Domain  /////////////////////////
    vector<double> q_i;
    value = 0.0;
    while(value<=max_freq){
        q_i.push_back(value);
        value += dq;
    }
    new_f.clear();

    ////////////// Filtering //////////////////////
    compute(h, original_f, r_g, dr_g, 0, new_r_g, new_dr_g, q_i, dq, q_cut, cutoff_r, new_f, beta_fs);
    return 0;
}

int Filter::filter_nonlocal(vector<double>original_f, vector<double>r_g, vector<double>dr_g, int l, double nonlocal_cutoff, vector<double>& new_f, vector<double>& new_mesh){
    if (original_f.size()!=r_g.size() or original_f.size()!=dr_g.size() ){
        Verbose::all() << "Filter nonlocal" << std::endl;
        Verbose::all() << "Error!!!  length of original_f is not equal to length of r_g or dr_g" << std::endl;
        Verbose::all() << "size of r_g: " << r_g.size() << std::endl;
        Verbose::all() << "size of dr_g: " << dr_g.size() << std::endl;
        Verbose::all() << "size of original_f: " << original_f.size() << std::endl;
        exit(-1);
    }
    double cutoff_r = nonlocal_cutoff*gamma_nonlocal;
    Verbose::single() << "filter_nonlocal cutoff = " << nonlocal_cutoff * gamma_nonlocal * 2 << " = " << nonlocal_cutoff << " * 2 * " << gamma_nonlocal << std::endl;

    std::array<double,3> scaling ;
    scaling[0] = basis->get_scaling()[0];
    scaling[1] = basis->get_scaling()[1];
    scaling[2] = basis->get_scaling()[2];
    double h = std::max( std::max( scaling[0],scaling[1] ),scaling[2] ) ;

    double max_freq =  M_PI / h;

    double q_cut = max_freq / alpha_nonlocal;
    double beta_fs = 5*log(10)/pow((alpha_nonlocal-1),2);
    double dq = M_PI / h * 0.01;
    double dr = h * 0.01;

    /////////////// generate new R basis  /////////////////////////
    vector<double> new_r_g;
    vector<double> new_dr_g;
    double value = 0.0;
    while(value <= cutoff_r*2){
        new_r_g.push_back(value);
        new_dr_g.push_back(dr);
        value+=dr;
    }
    new_mesh = new_r_g;

    /////////////// generate basis on frequency Domain  /////////////////////////
    vector<double> q_i;
    value = 0.0;
    while(value<=max_freq){
        q_i.push_back(value);
        value+=dq;
    }
    new_f.clear();

    ////////////// Filtering //////////////////////
    compute(h, original_f, r_g, dr_g, l, new_r_g, new_dr_g, q_i, dq, q_cut, cutoff_r, new_f, beta_fs);
    return 0;
}

void Filter::compute(double target_scaling, vector<double> original_f, vector<double> r_g, vector<double> dr_g, int l, vector<double> new_r_g, vector<double> new_dr_g, vector<double> q_i, double dq_i, double q_cut, double cutoff_r, vector<double>& new_f, double beta_fs){
    if (l>3){
        Verbose::all() << "l is too high " << l<< std::endl;
        exit(-1);
    }

    int r_vector_length = r_g.size();
    int q_vector_length = q_i.size();

    const long double h = target_scaling;
    const long double q_max = M_PI/h;  // max frequency that grid can describe

    const long double c = sqrt(2/M_PI);  // constant for spherical Bessel transformation
    int new_r_vector_length = new_r_g.size();

    long double** qr_ig = new long double* [q_vector_length];
    long double** new_qr_ig = new long double* [q_vector_length];

    long double* r2_g = new long double [r_vector_length];
    long double* q2_i = new long double [q_vector_length] ;
    long double* new_r2_g = new long double [new_r_vector_length];

    #ifdef ACE_HAVE_OMP
    #pragma omp parallel for
    #endif
    for (int i =0; i<q_vector_length ; i++){
        qr_ig[i] = new long double [r_vector_length];
        new_qr_ig[i] = new long double [new_r_vector_length];

        for(int g=0;g<r_vector_length;g++){
            qr_ig[i][g] = q_i[i]*r_g[g];
        }

        for(int new_g=0;new_g<new_r_vector_length;new_g++){
            new_qr_ig[i][new_g] = q_i[i]*new_r_g[new_g];
        }
    }

    #ifdef ACE_HAVE_OMP
    #pragma omp parallel for
    #endif
    for(int i=0;i<q_vector_length;i++){
        q2_i[i]=q_i[i]*q_i[i];
    }
    #ifdef ACE_HAVE_OMP
    #pragma omp parallel for
    #endif
    for(int g=0;g<r_vector_length;g++){
        r2_g[g] = r_g[g]*r_g[g];
    }

    #ifdef ACE_HAVE_OMP
    #pragma omp parallel for
    #endif
    for(int new_g=0;new_g<new_r_vector_length;new_g++){
        new_r2_g[new_g] = new_r_g[new_g]*new_r_g[new_g];
    }
    
    //Fourier space filtering function F_fs(q)
    vector<long double> cut_i;
    for (int i=0;i<q_vector_length;i++){
        if (q_i[i] <= q_cut ){
            cut_i.push_back(1.0);
        } else{
            double ft_tmp = Filter::Fourier_filtering_function(q_i[i],q_cut,beta_fs);
            cut_i.push_back(ft_tmp);    
        }
    }
    vector<double> mask_grid; 
    if(eta <= 0.0){
        Verbose::single(Verbose::Normal) << " Eta is not given or 0 value is given. Using default option eta = " << q_cut*cutoff_r << std::endl;
        eta = q_cut*cutoff_r;
        if(eta < 10){ 
            Verbose::single(Verbose::Normal) << " Given Eta value is too small!!! " << eta << ". Using Eta =10.0" << std::endl;
            eta = 10.0;
        } else if(eta >50){
            Verbose::single(Verbose::Normal) << " Given Eta value is too big!!! " << eta << ". Using Eta =50.0" << std::endl;
            eta = 50.0;
        }
    }
    initialize_mask_function(eta); 
    vector<long double> fq_i;
    vector<double> fr_g;
    vector<long double> fdrim_g;
    for (int g=0;g<r_vector_length;g++){
        fdrim_g.push_back(original_f[g]*dr_g[g]);
    }

    // Perform spherical Bessel transformation from real space to reciprocal space.
    // p(r) -> p_m^{filt}(q) = F_fs(q) \int p(r) / m(r/rcut) j_l(qr) r^2 dr (?)
    for (int i=0; i<q_vector_length;i++){
        long double val=0.0;
        #ifdef ACE_HAVE_OMP
        #pragma omp parallel for reduction(+:val)
        #endif
        for (int g=0;g<fdrim_g.size();g++){
            double mask = get_mask_function_value(r_g[g], cutoff_r);
            if (r_g[g]<=cutoff_r){
                val += fdrim_g[g]*r2_g[g]/mask * Bessel::jn(qr_ig[i][g], l);
            } else{
                val += 0.0;
            }
        }
        fq_i.push_back(c*val*cut_i[i]);
    }

    // Perform spherical Bessel transformation from reciprocal space to real space
    // p(q) -> m(r/rcut) * \int p(q) j_l(qr) q^2 dq
    for (int new_g=0; new_g<new_r_vector_length; new_g++){
        long double val=0.0;
        #ifdef ACE_HAVE_OMP
        #pragma omp parallel for reduction(+:val)
        #endif
        for (int i=0; i<q_vector_length;i++){
            val += fq_i[i]*q2_i[i] * Bessel::jn(new_qr_ig[i][new_g], l);
        }
        double mask = get_mask_function_value(new_r_g[new_g], cutoff_r);
        fr_g.push_back(c*val*dq_i*mask);
    }

    for (int i=0;i<q_vector_length;i++){
        delete[] qr_ig[i];
        delete[] new_qr_ig[i];
    }

    delete[] qr_ig;
    delete[] q2_i;
    delete[] r2_g;

    delete[] new_qr_ig;
    delete[] new_r2_g;
    new_f = fr_g;
    for(int i=0; i<fr_g.size()-1; i++){
        if (!std::isfinite(fr_g[i])){
            Verbose::single()<<"Filter Warning :: filtered projection function's "<<i<<"th element is abnormal! " << fr_g[i]<< std::endl;
            exit(EXIT_FAILURE);
        }
    }
    return;
}

void Filter::optimize_comp_charge_exps(
        vector<vector<double> > atomic_density,
        vector<vector<double> > original_mesh,
        vector<vector<double> > original_d_mesh
){
    vector<int> types = atoms -> get_atom_types();
    this -> comp_charge_exps.clear();
    for(int itype = 0; itype < types.size(); ++itype){
        double init_rgaus = Periodic_atom::comp_charge_exp[types[itype]-1];
        comp_charge_exps.push_back(this -> optimize_comp_charge_exp(Zvals[itype], init_rgaus, atomic_density[itype], original_mesh[itype], original_d_mesh[itype]));
    }
}

double Filter::optimize_comp_charge_exp(
        double Zval, double initial_comp_charge_exp,
        vector<double> atomic_density,
        vector<double> original_mesh,
        vector<double> original_d_mesh
){
    double sum=std::numeric_limits<double>::max();

    auto scaling = basis->get_scaling();
    double h = std::max( std::max( scaling[0],scaling[1] ),scaling[2] ) ;
    double qmax = M_PI/h;

    double r_gaus;
    double mesh_size = min(original_mesh.size(), atomic_density.size());
    original_mesh.resize(mesh_size); original_d_mesh.resize(mesh_size); atomic_density.resize(mesh_size);
    vector<double> atomic_hartree_potential = Radial_Grid::Paw::calculate_Hartree_potential_r(atomic_density, 0, original_mesh, original_d_mesh);

    for(double val=0.1;val<=4.0;val+=0.01){
        double r_gaus_tmp=val*initial_comp_charge_exp;
        double comp_charge_norm = Zval/pow(sqrt(M_PI)*r_gaus_tmp, 3);
        double sum_tmp = 0.0;
        #ifdef ACE_HAVE_OMP
        #pragma omp parallel for reduction(+:sum_tmp)
        #endif
        for (int i=0;i< original_mesh.size();i++){
            double r=original_mesh[i];
            // Note: (n_comp - n_atom)*(V_comp + loc_pp) instead of (n_atom - n_comp) * (V_atom - V_comp)
            double scaled_r = r/r_gaus_tmp;
            double deform_density = atomic_density[i] - comp_charge_norm * exp(-scaled_r*scaled_r);
            sum_tmp += deform_density * (r*r*atomic_hartree_potential[i] - r*Zval*erf(scaled_r));
        }
        double fted_at_qmax = Zval/pow(sqrt(M_PI)*r_gaus_tmp, 2) * exp(-pow(M_PI*qmax*r_gaus_tmp, 2));
        if(fted_at_qmax > 1.E-8){
            continue;
        } else if(sum_tmp < sum){
            r_gaus = r_gaus_tmp;
            sum=sum_tmp;
        } else {
            break;
        }
    }

    return r_gaus;
}

int Filter::get_comp_potential(vector<double> original_mesh, vector<double>& return_val,int itype){
    assert(comp_charge_exps.size() == Zvals.size());
    if(comp_charge_exps.size()==0){
        return -2;
    }

    vector<std::array<double,3> > positions = atoms->get_positions();
    return_val.clear();

    double r,scaled_r,r_gaus;
    r_gaus = comp_charge_exps[itype];
    for (int i=0;i< original_mesh.size();i++){
        r=original_mesh[i];
        if( std::abs(r) > 1.0E-30 ){
            scaled_r = r/r_gaus;
            return_val.push_back( Zvals[itype]*erf(scaled_r)/r );
        } else {
            return_val.push_back(Zvals[itype] * 2/r_gaus/sqrt(M_PI));
        }
    }

    return 0;
}

void Filter::get_comp_charge_exps(vector<double>& comp_charge_exps2){
    if(comp_charge_exps.size() == 0){
        Verbose::all() << "Filter::get_comp_charge_exps - no compensation charge." << std::endl;
        exit(EXIT_FAILURE);
    }
    else if(comp_charge_exps.size() != comp_charge_exps2.size()){
        Verbose::all() << "Filter::get_comp_charge_exps - the size of compensation charge is different." << std::endl;
        exit(EXIT_FAILURE);
    }

    for(int i=0; i<comp_charge_exps2.size(); i++){
        comp_charge_exps2[i] = comp_charge_exps[i];
    }
    return;
}

double Filter::Fourier_filtering_function(double r, double r_cutoff, double beta){
    return exp(-pow((r/r_cutoff)-1,2)*beta);
}

double Filter::get_mask_function_value(double point, double cutoff){
    double tmp = point/cutoff;
    double y1, y2;
    y1 = (mask_function[1]-mask_function[0])/(mask_grid[1]-mask_grid[0]);
    y2 = (mask_function[mask_function.size()-1]-mask_function[mask_function.size()-2])/(mask_grid[mask_grid.size()-1]-mask_grid[mask_grid.size()-2]);
    vector<double> y3 = Interpolation::Spline::spline(mask_grid, mask_function, mask_grid.size(), y1, y2);
    double mask_value = Interpolation::Spline::splint(mask_grid, mask_function, y3, mask_grid.size(), tmp);
    return mask_value;
}

void Filter::initialize_mask_function(double eta){
    this -> mask_function.clear();
    this -> mask_function.push_back(1.0);
    double x;
    double k;
    k = (0.4871*eta+0.0825);
    Verbose::single(Verbose::Normal) << " Given Eta value is " <<eta<< ". Using exp(-"<<k<<"*x^2) as a mask function"<<std::endl;
    for(int i = 0; i<200; i++){
        x = 0.005*(i+1);
        this -> mask_function.push_back(exp(-k*pow(x,2)));
    }
    this -> mask_grid.clear();
    for (int i = 0; i < this -> mask_function.size(); ++i){
        this -> mask_grid.push_back(i*0.005);
    }
}
