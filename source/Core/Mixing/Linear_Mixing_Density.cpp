#include "Linear_Mixing_Density.hpp"
#include <vector>
#include "Epetra_Vector.h"
#include "Epetra_MultiVector.h"

using Teuchos::Array;
using Teuchos::RCP;

Linear_Mixing_Density::Linear_Mixing_Density(double alpha, bool update_paw/* = true*/)
    :Linear_Mixing(alpha, update_paw){
    this->mixing_type = "Density";
}

int Linear_Mixing_Density::update(RCP<const Basis> basis, Array<RCP<Scf_State> > states, int i_spin, RCP<Nuclear_Potential> nuclear_potential/* = Teuchos::null*/){
    //http://ftp.abinit.org/ws07/Liege_Torrent2_SC_Mixing.pdf
    //3rd page
    //states[states.size()-1] = states.size()-2-th out_state
    //states[states.size()-2] = states.size()-2-th in_state

    //equivalent condition orginated from older version of this code
    //Do not mix anything for first step
    if(states.size()==1 || states.size()==2) return 0;

    RCP<Epetra_Vector> out_state_den = states[states.size()-1]->density[i_spin];
    RCP<Epetra_Vector> in_state_den = states[states.size()-2]->density[i_spin];

    RCP<Epetra_Vector> output = Teuchos::rcp(new Epetra_Vector(*out_state_den));
    output->Update(1-alpha,*in_state_den,alpha);

    if(nuclear_potential != Teuchos::null and this -> update_paw){
        std::vector<double> coeffs(1, 1.0);
        nuclear_potential -> mix_nuclear_potential( coeffs, alpha, i_spin );
    }

    const int size = states[states.size()-1]->density.size();
    if (i_spin<size){
        states[states.size()-1]->density[i_spin] = output;
    }
    else if ((i_spin==0 and size ==0) or (i_spin==1 and size ==1) ){
        states[states.size()-1]->density.append( output );
    }
    else{
        Verbose::all() << "Linear_Mixing_Density: i_spin: " << i_spin << std::endl
                       << "Linear_Mixing_Density: size:" << size <<std::endl
                       << "Linear_Mixing_Density: invalid access" <<std::endl;
        exit(-1);
    }
    return 0;
}

/*
void Linear_Mixing_Density::update(Teuchos::RCP<Epetra_MultiVector> eigenvectors, Teuchos::Array<Occupation* > occupations, Teuchos::RCP<Epetra_MultiVector> new_density, int sp_polarize){
    Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > orbitals;
    orbitals.push_back( Teuchos::rcp(new Epetra_MultiVector(*eigenvectors)) );

    update(orbitals, occupations, new_density, sp_polarize);

    orbitals.clear();

    return;
}
*/
