#include "Mixing.hpp"

std::string Mixing::get_mixing_type(){
    return mixing_type;
}

int Mixing::get_used_step_number(){
    return used_step_number;
}
/*
Mixing::update_density_matrix( vector< Array< Teuchos::SerialDenseMatrix<int,double> > > PAW_D_matrixes ){
    int atom_size;
    int spin_size;
    //vector< Array< Teuchos::SerialDenseMatrix<int,double> > > PAW_D_matrix
    if( this -> PAW_D_matrixes_history_in.size() > 0 ){
        this -> PAW_D_matrixes_history_out.push_back( PAW_D_matrixes );

        for(int a = 0; a < atom_size; ++a ){
            for(int s = 0; s < spin_size; ++s ){
                PAW_D_matrixes[a][s].putScalar(0.0);
                for(int i = 0; i < mixing_coeffs.size(); ++i){
                    int index = this -> PAW_D_matrixes_history_in.size() - mixing_coeffs.size()+i;
                    Teuchos::SerialDenseMatrix<int,double> temp = this -> PAW_D_matrixes_history_in[index][a][s];
                    temp.scale( mixing_coeffs[i]*mixing_alpha);
                    PAW_D_matrixes[a][s] += temp;
                    Teuchos::SerialDenseMatrix<int,double> temp2 = this -> PAW_D_matrixes_history_out[index][a][s];
                    temp2.scale( mixing_coeffs[i]*(1-mixing_alpha));
                    PAW_D_matrixes[a][s] += temp2;
                }
            }
        }
    }
    PAW_D_matrixes_history_in.push_back( this -> PAW_D_matrixes );
}
*/
/*
void Mixing::sum_squared_orbitals(Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > eigenvectors, Teuchos::Array<Occupation*> occupations, Teuchos::RCP<Epetra_MultiVector> new_density, int sp_polarize){

    new_density->PutScalar(0.0);
    
    for(int alpha=0; alpha<occupations.size(); alpha++){
        Teuchos::RCP<Epetra_MultiVector> orbitals = Teuchos::rcp(new Epetra_MultiVector(*eigenvectors[alpha]));
        Interpolation::change_basis_and_grid(basis, grid_setting, eigenvectors[alpha], false, basis, grid_setting, orbitals, true);

        for(int i=0; i<NumMyElements; i++){
            for(int j=0; j<occupations[alpha]->get_size(); j++){
                new_density->SumIntoMyValue(i, alpha, occupations[alpha]->operator[](j) * orbitals->operator[](j)[i] * orbitals->operator[](j)[i]);
            }
        }
    }

    return;
}
*/
/*
Teuchos::Array<Occuation* > Update_Density::get_occupations(Teuchos::ParameterList* parameters){
    if(occuations==NULL){
        return construct_occupations(parameters);
    }
    return occupations;
}
Teuchos::Array<Occupation* > Update_Density::construct_occupations(Teuchos::ParameterList* parameters){
    Teuchos::Array<Occupation* > occupations;
    occupations.push_back(new Occupation_Zero_Temp(2.0));
    return occupations;
}
*/
