#pragma once
//#include <string>
//#include <vector>
#include "Teuchos_RCP.hpp"
#include "Teuchos_Array.hpp"
#include "Epetra_Vector.h"

#include "Broyden_Mixing.hpp"

/**
 * @brief Performs Broyden mixing for density.
 * @note See Broyden_Mixing class.
 **/
class Broyden_Mixing_Density: public Broyden_Mixing {
    public:
        /**
         * @brief Constructor.
         * @param start Before this value, perform linear mixing instead. If this value is less than history+1, then it is set to the history+1.
         * @param history Number of previous density to mix.
         * @param alpha Mixing coefficient for initial linear mixing part.
         * @param broyden_alpha Mixing coefficient for Broyden mixing part.
         * @param w_0 See PRC 78 014318 (2008) Section II.B
         * @param w_m See PRC 78 014318 (2008) Section II.B
         **/
        Broyden_Mixing_Density(
            int start, int history, 
            double alpha, double broyden_alpha, 
            double w_0 = 0.01, double* w_m = NULL
        );
        //    int update(Teuchos::RCP<const Basis> basis, Teuchos::Array< Teuchos::RCP<State> > states, int i_spin, Teuchos::RCP<State>& state);
        /**
         * @brief Inherited from Mixing. See Mixing.
         * @return Always 0.
         **/
        int update(
            Teuchos::RCP<const Basis> basis, 
            Teuchos::Array< Teuchos::RCP<Scf_State> > states, 
            int i_spin, 
            Teuchos::RCP<Nuclear_Potential> nuclear_potential = Teuchos::null 
        );

    private:
        //int compute(Teuchos::Array< Teuchos::RCP<Epetra_Vector> > residue, Teuchos::RCP<Epetra_Vector>& output);
        //    int compute(Teuchos::Array< Teuchos::RCP<Epetra_Vector> > residue, Teuchos::Array< Teuchos::RCP<Epetra_Vector> > density_ins, Teuchos::RCP<Epetra_Vector>& output);
        //double alpha, broyden_alpha;
        //int start, history;
        //double w_0;
        //std::vector<double> w_m;
        //Teuchos::Array< Teuchos::Array< Teuchos::RCP<Epetra_Vector> > > residue; // residue[i_spin][history_index]
};
