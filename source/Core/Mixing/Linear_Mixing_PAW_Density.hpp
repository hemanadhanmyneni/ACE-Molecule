#pragma once
//#include <string>
#include <vector>
#include "Teuchos_RCP.hpp"
#include "Teuchos_ParameterList.hpp"
#include "Epetra_MultiVector.h"

#include "Linear_Mixing_Density.hpp"
#include "Linear_Mixing.hpp"
#include "../../State/Scf_State.hpp"

/**
 * @brief Performs linear mixing for PAW density matrix and density.
 * @note See http://ftp.abinit.org/ws07/Liege_Torrent2_SC_Mixing.pdf, pg 3.
 * @note This class owns Linear_Mixing_Density to update both PAW density matrix and PS density.
 **/
class Linear_Mixing_PAW_Density: public Linear_Mixing {
    public:
        /**
         * @brief Inherited from Mixing. See Mixing.
         * @return Always 0.
         **/
        int update(
            Teuchos::RCP<const Basis> basis, 
            Teuchos::Array< Teuchos::RCP<Scf_State> > states, 
            int i_spin, 
            Teuchos::RCP<Nuclear_Potential> nuclear_potential = Teuchos::null 
        );
        /**
         * @brief Constructor.
         * @param alpha Mixing coefficient.
         **/
        Linear_Mixing_PAW_Density(double alpha);
    protected:
        std::vector< std::vector< std::vector<double> > > np_descriptor_in;
        Teuchos::RCP<Linear_Mixing_Density> linear_mdensity;
};

