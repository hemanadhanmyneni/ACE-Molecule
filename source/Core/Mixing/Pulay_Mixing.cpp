#include "Pulay_Mixing.hpp"
#include "../../Utility/String_Util.hpp"
#include "Teuchos_LAPACK.hpp"

using std::vector;
using std::string;
using std::complex;
using Teuchos::rcp;
using Teuchos::RCP;
using Teuchos::Array;

Pulay_Mixing::Pulay_Mixing(int start, int history, int max_history, double alpha, double pulay_alpha, bool update_paw/* = false*/){
    this -> start = start;
    this -> history = history;

    if( history > start ){
        this->start = history;
    }
    this -> alpha=alpha;
    this -> pulay_alpha = pulay_alpha;
    this -> max_history = max_history;
    this -> used_step_number = history;
    this -> update_paw = update_paw;
}

template <typename T>
int Pulay_Mixing::get_coeffs(Teuchos::SerialDenseMatrix<int, T> pulay_matrix, int dim, std::vector<T>& coeffs){
    vector<T> b(dim, 0.0);
    Teuchos::LAPACK<int, T> lapack;
    int info;
    int* IPIV=new int[dim]();
    int LWORK = dim;
    T* WORK = new T[LWORK]();
    int code = 0;

    // Since POTRI can be applied to symetric positive definite matrix, it cannot be used here.
    // GETRI(obtaining inverse matrix) requres GETRF(PLU decomposition) first
    lapack.GETRF(dim, dim, pulay_matrix.values(), pulay_matrix.stride(), IPIV, &info);
    if(info != 0){
        Verbose::single(Verbose::Simple) << "Pulay_Mixing::compute: LAPACK GETRF error: " << info << std::endl;
        code = info*10000;
        if(info > 0){
            Verbose::single(Verbose::Simple) << "Fallback to linear mixing." << std::endl;
            b[dim-1] = 1.0;
            coeffs = b;
            return code;
        }
    }
    lapack.GETRI(dim, pulay_matrix.values(), pulay_matrix.stride(), IPIV, WORK, LWORK, &info);
    delete[] IPIV;
    delete[] WORK;
    if(info != 0){
        Verbose::single(Verbose::Simple) << "Pulay_Mixing::compute: LAPACK GETRI error: " << info << std::endl;
        
        code = info*100;
        if(info > 0){
            Verbose::single(Verbose::Simple) << "Fallback to linear mixing." << std::endl;
            b[dim-1] = 1.0;
            coeffs = b;
            return code;
        }
    }
    //lapack.GETRI(dim, pulay_matrix.values(), pulay_matrix.stride(), IPIV, WORK, -1, &info);

    /*
    double* b = new double [dim];
    for(int i=0; i<dim; i++){
        b[i] = 0.0;
    }
    */

    for(int i=0; i<dim; i++){
        for(int j=0; j<dim; j++){
            b[i] += pulay_matrix(j, i);
        }
    }

    T b_norm = 0.0;
    for(int i=0; i<dim; i++){
        b_norm += b[i];
    }

    for(int i=0; i<dim; i++){
        b[i] /= b_norm;
    }
    coeffs = b;
    for(int i = 0; i < dim; ++i){
        if(std::norm(b[i]) > 1.0){
            Verbose::single(Verbose::Simple) << "Pulay_Mixing::compute: extrapolation detected at " << i << " with value " << b[i] << "." << std::endl;
            //Verbose::single(Verbose::Simple) << "Fallback to linear mixing." << std::endl;
            //b = vector<T>(dim);
            //b[dim-1] = 1.0;
            code = -i;
        }
    }
    return code;
}

int Pulay_Mixing::compute(
    Array< RCP<Epetra_Vector> > residue,
    Array< RCP<Epetra_Vector> > density_ins,
    double pulay_alpha, int i_spin,
    RCP<Epetra_Vector>& output,
    RCP<Nuclear_Potential> nuclear_potential/* = Teuchos::null*/
){
    // See Kresse, Phys. Rev. B 54, 11169 (1996)
    // See http://www.hector.ac.uk/cse/distributedcse/reports/conquest/conquest/node3.html

    int dim  = residue.size();
    Teuchos::SerialDenseMatrix<int, double> pulay_matrix (dim, dim);

    for( int i=0; i<dim; i++){
        for( int j=0; j<dim; j++){
            double result = 0.0;
            residue[i] -> Dot(*residue[j], &result);
            pulay_matrix(i, j) = result;
        }
    }

    vector<double> b;
    int err = this -> get_coeffs<double>(pulay_matrix, dim, b);
    //if(err != 0){
    //    return err;
    //}

    // This old version, uses
    // rho_in(n+1) = sum_i^n b_i ( pulay_alpha*rho_out(i) + (1-pulay_alpha)*rho_in(i))
    // which follows the reference
//    /*
    output->PutScalar(0.0);
    for(int i=0;i<dim;i++){
        RCP<Epetra_Vector> temp_density = rcp(new Epetra_Vector( *density_ins[i] ));
        temp_density -> Update(pulay_alpha, *residue[i], 1.0);
        output -> Update(b.at(i), *temp_density, 1.0);
    }
//    */
    if(nuclear_potential != Teuchos::null and this -> update_paw){
        nuclear_potential -> mix_nuclear_potential( b, this -> pulay_alpha, i_spin );
    }

    // I think this new version should
    // rho_in(n+1) = pulay_alpha * [sum_i^n b_i ( rho_out(i) - rho_in(i))] + (1-pulay_alpha) * rho_in(n)
    // It respects just previous input only, and does not count of ancient density.
    // Also easier to code here, because output already contains rho_in(n)
    // But confirmed to be worse than previous one.
    /*
    for(int i = 0; i < dim; ++i){
        output -> Update( b[i]*this->pulay_alpha, *residue[i], 1.0 );
    }
    */
    return err;
}

int Pulay_Mixing::compute(
    vector< vector<double> > residue,
    vector< vector<double> > density_ins,
    double pulay_alpha, int i_spin,
    vector<double> &output
){
    // See Kresse, Phys. Rev. B 54, 11169 (1996)
    // See http://www.hector.ac.uk/cse/distributedcse/reports/conquest/conquest/node3.html

    int dim = residue.size();
    int vec_size = residue[0].size();
    Teuchos::SerialDenseMatrix<int, double> pulay_matrix (dim, dim);

    for( int i=0; i<dim; i++){
        for( int j=0; j<dim; j++){
            double result = 0.0;
            for(int k = 0; k < vec_size; ++k){
                result += residue[i][k] * residue[j][k];
            }
            pulay_matrix(i, j) = result;
        }
    }

    vector<double> b;
    int err = this -> get_coeffs<double>(pulay_matrix, dim, b);
    //if(err != 0){
    //   return err;
    //}

    // This old version, uses
    // rho_in(n+1) = sum_i^n b_i ( pulay_alpha*rho_out(i) + (1-pulay_alpha)*rho_in(i))
    // which follows the reference
    output.clear();
    output.resize(vec_size);
    for(int i = 0; i < dim; ++i){
        for(int j = 0; j < vec_size; ++j){
            output[j] += b[i] * (density_ins[i][j] + pulay_alpha * residue[i][j]);
        }
    }
    return err;
}
int Pulay_Mixing::compute(
        std::vector< std::vector<std::complex<float> > > residue,
        std::vector< std::vector<std::complex<float> > > pol_ins,
        double pulay_alpha,
        std::vector<std::complex<float> >  &output
        ){
    // See Kresse, Phys. Rev. B 54, 11169 (1996)
    // See http://www.hector.ac.uk/cse/distributedcse/reports/conquest/conquest/node3.html
    int dim = residue.size();
    int vec_size = residue[0].size();
    Teuchos::SerialDenseMatrix<int, complex<float> > pulay_matrix (dim, dim);

    for( int i=0; i<dim; i++){
        for( int j=0; j<dim; j++){
            complex<float> result = 0.0;
            for(int k = 0; k < vec_size; ++k){
                result += residue[i][k] * residue[j][k];
            }
            pulay_matrix(i, j) = result;
        }
    }

    vector< complex<float> > b;
    int err = this -> get_coeffs< complex<float> >(pulay_matrix, dim, b);
    //if(err != 0){
     //   return err;
    //}

    // This old version, uses
    // rho_in(n+1) = sum_i^n b_i ( pulay_alpha*rho_out(i) + (1-pulay_alpha)*rho_in(i))
    // which follows the reference
    for(int i = 0; i < dim; ++i){
        for(int j = 0; j < vec_size; ++j){
            output[j] += b[i] * (pol_ins[i][j] + (float) pulay_alpha * residue[i][j]);
        }
    }
    return err;
}

int Pulay_Mixing::compute(
        std::vector< std::vector<std::complex<float> > > residue,
        std::vector< std::vector<std::complex<float> > > pol_ins,
        double pulay_alpha,
        std::vector<std::complex<float> >  &output,
        Teuchos::SerialDenseMatrix<int, complex<float> > pulay_matrix
        ){
    // See Kresse, Phys. Rev. B 54, 11169 (1996)
    // See http://www.hector.ac.uk/cse/distributedcse/reports/conquest/conquest/node3.html
    int dim = residue.size();
    int vec_size = residue[0].size();

    vector< complex<float> > b;
    int err = this -> get_coeffs< complex<float> >(pulay_matrix, dim, b);
    //if(err != 0){
    //    return err;
    //}

    // This old version, uses
    // rho_in(n+1) = sum_i^n b_i ( pulay_alpha*rho_out(i) + (1-pulay_alpha)*rho_in(i))
    // which follows the reference
    for(int i = 0; i < dim; ++i){
        for(int j = 0; j < vec_size; ++j){
            output[j] += b[i] * (pol_ins[i][j] + (float) pulay_alpha * residue[i][j]);
        }
    }
    return err;
}

std::string Pulay_Mixing::get_mixing_info(int width/* = 50*/){
    std::string info;
    info += "= Mixing Parameter Informations ";
    info += string(width-info.size(), '=') + "\n";
    info += " Pulay(DIIS) mixing with field " + this -> mixing_type + ".\n";
    info += " Linear mixing until " + String_Util::to_string(this -> start-1) + " steps are done.\n";
    info += " Initial linear mixing coefficient: " + String_Util::to_string(this -> alpha) + "\n";
    info += " Pulay mixing coefficient: " + String_Util::to_string(this -> pulay_alpha) + "\n";
    info += " Previous states to mix: " + String_Util::to_string(this -> history) + "\n";
    info += string(width, '=');
    return info;
}
