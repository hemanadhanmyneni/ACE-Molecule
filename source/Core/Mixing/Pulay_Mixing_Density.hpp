#pragma once
//#include <string>
#include "Teuchos_RCP.hpp"
#include "Teuchos_Array.hpp"
#include "Epetra_Vector.h"

#include "Pulay_Mixing.hpp"

/**
 * @brief Performs Pulay mixing for density.
 * @note See Pulay_Mixing class.
 **/
class Pulay_Mixing_Density: public Pulay_Mixing {
    public:
        /**
         * @param start Before this value, perform linear mixing instead. If this value is less than history+1, then it is set to the history+1.
         * @param history Number of previous densities to mix.
         * @param alpha Mixing coefficient for initial linear mixing part.
         * @param pulay_alpha Mixing coefficient for Pulay mixing part.
         **/
        Pulay_Mixing_Density(
            int start,
            int history,
            int max_history,
            double alpha,
            double pulay_alpha,
            bool update_paw = true
        );
//    int update(RCP<const Basis> basis, Array< RCP<State> > states, int i_spin, RCP<State>& state);
        /**
         * @brief Inherited from Mixing. See Mixing.
         * @return Always 0.
         **/
        int update(
            Teuchos::RCP<const Basis> basis,
            Teuchos::Array< Teuchos::RCP<Scf_State> > states,
            int i_spin,
            Teuchos::RCP<Nuclear_Potential> nuclear_potential = Teuchos::null
        );

    private:
        //int compute(Array< RCP<Epetra_Vector> > residue, RCP<Epetra_Vector>& output);
        //int compute(Array< RCP<Epetra_Vector> > residue, Array< RCP<Epetra_Vector> > density_ins, RCP<Epetra_Vector>& output);
        //double alpha, pulay_alpha;
        //int start, history;
        //Array<Array<RCP<Epetra_Vector> > > residue; // residue[i_spin][history_index]
};
