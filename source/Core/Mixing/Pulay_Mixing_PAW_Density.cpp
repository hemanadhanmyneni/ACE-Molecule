#include "Pulay_Mixing_PAW_Density.hpp"
#include "Epetra_Vector.h"

using std::vector;
using Teuchos::rcp;
using Teuchos::RCP;
using Teuchos::Array;

Pulay_Mixing_PAW_Density::Pulay_Mixing_PAW_Density(int start, int history, int max_history, double alpha, double pulay_alpha)
    :Pulay_Mixing(start, history, max_history, alpha, pulay_alpha){
    this->mixing_type = "PAW_Density";
    this -> pulay_mdensity = rcp( new Pulay_Mixing_Density(start, history, max_history, alpha, pulay_alpha) );
}

int Pulay_Mixing_PAW_Density::update( RCP<const Basis> basis, Array< RCP<Scf_State> > states, int i_spin, RCP<Nuclear_Potential> nuclear_potential/* = Teuchos::null*/){
    // 2015. 06. 25. - Jaewook Kim
    // NEW VERSION is wrong! (not converged, density_in(n-1) = density_mix(n) != density_out(n) )
    // now this code follows old version.
    // Storing "residue" = density_out - density_in
    const int size = states[states.size()-1]->density.size();
    if( nuclear_potential == Teuchos::null ){
        Verbose::all() << "Pulay_Mixing_PAW_Density::update nuclear_potential == null" << std::endl;
        return -1;
    }

    vector< vector<double> > descriptor;
    nuclear_potential -> get_atomic_density_matrix(descriptor);
    const int vec_size = descriptor[i_spin].size();
    
    vector<double> output(vec_size);

    //when states.size()==2
    //states[0] = state_in(0)
    //states[1] = state_out(0)
    if(states.size()==2){
        vector< vector<double> > initial_descriptor;
        nuclear_potential -> get_initial_atomic_density_matrix(initial_descriptor);
        this -> np_descriptor_in.push_back( vector< vector<double> >(1,initial_descriptor[i_spin]) );
        if(i_spin==0) this -> np_residue = vector< vector< vector<double> > >(size);
        vector<double> tmp_residue(vec_size);
        for(int i = 0; i < vec_size; ++i){
            tmp_residue[i] = descriptor[i_spin][i] - np_descriptor_in[i_spin][0][i];
        }
        this -> np_residue[i_spin].push_back(tmp_residue);
    }

    vector<double> out_state_des = descriptor[i_spin];
    vector<double> in_state_des = this -> np_descriptor_in[i_spin][this->np_descriptor_in[i_spin].size()-1];
    if(states.size()-1 < start){
        for(int i = 0; i < out_state_des.size(); ++i){
            output[i] = in_state_des[i]*(1-alpha)+out_state_des[i];
        }
        vector<double> tmp_residue(vec_size);
        for(int i = 0; i < vec_size; ++i){
            tmp_residue[i] = descriptor[i_spin][i] - np_descriptor_in[i_spin][states.size()-2][i];
        }
        this -> np_residue[i_spin].push_back(tmp_residue);
    } else {
        vector< vector<double> > density_ins;
        for(int i=0;i<history;i++){
            // Appends multivector object having polarize+1 vectors to residue
            density_ins.push_back( this -> np_descriptor_in[i_spin][this -> np_descriptor_in[i_spin].size()+i-history] );
        }
        // Assign value to the residue. value: save_densities[i]-save_densities[i-1]
        // residue = rho_out - rho_in
        this -> np_residue[i_spin].push_back( vector<double>(vec_size) );
        for(int i = 0; i < vec_size; ++i){
            this -> np_residue[i_spin].back()[i] = out_state_des[i] - in_state_des[i];
        }

        while(this -> np_residue[i_spin].size()>history){
            this -> np_residue[i_spin].erase(this->np_residue[i_spin].begin());
        }
//        compute(residue, density_ins, output);
        int ierr = Pulay_Mixing::compute(np_residue[i_spin], density_ins, pulay_alpha, i_spin, output);
        if( ierr < 0 ){
            for(int i = 0; i < out_state_des.size(); ++i){
                output[i] = in_state_des[i]*(1-alpha)+out_state_des[i];
            }
        }
    }

    /*
    if (i_spin == 0){
        this -> np_descriptor_in.push_back( descriptor );
    }
    this -> np_descriptor_in[i_spin].back() = output;
    */
    this -> np_descriptor_in[i_spin].push_back(output);
    if( i_spin == size-1 ){
        vector< vector<double> > new_descriptor(size);
        for(int s = 0; s < size; ++s){
            new_descriptor[s] = this -> np_descriptor_in[s].back();
        }
        nuclear_potential -> set_atomic_density_matrix( new_descriptor );
    }
    this -> pulay_mdensity -> update(basis, states, i_spin);
    return 0;
}
