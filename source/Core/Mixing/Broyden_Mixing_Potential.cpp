#include "Broyden_Mixing_Potential.hpp"
#include "Epetra_Vector.h"

using Teuchos::rcp;
using Teuchos::RCP;
using Teuchos::Array;

Broyden_Mixing_Potential::Broyden_Mixing_Potential(int start, int history, double alpha, double broyden_alpha, double w_0/* = 0.01*/, double* w_m/* = NULL*/)
    : Broyden_Mixing(start, history, alpha, broyden_alpha, w_0, w_m){
    this->mixing_type = "Potential";
}

int Broyden_Mixing_Potential::update( RCP<const Basis> basis, Array< RCP<Scf_State> > states, int i_spin, RCP<Nuclear_Potential> nuclear_potential/* = Teuchos::null*/){
    const int size = states[states.size()-1]->local_potential.size();

    RCP<Epetra_Vector> output = rcp( new Epetra_Vector(*states[states.size()-1]->local_potential[i_spin]) );

    //when states.size()==2
    //states[0] = state_in(0)
    //states[1] = state_out(0)
    this -> residue.resize(size);
    if(states.size()==2){
        if(i_spin==0){
            Array<RCP<Epetra_Vector> > tmp;
            tmp.append( rcp(new Epetra_Vector(output->Map())) );
            tmp[0]->Update(1.0, *states[1]->local_potential[i_spin], -1.0, *states[0]->local_potential[i_spin], 0.0);
            residue.append(tmp);
        } else {
            residue[i_spin].append( rcp(new Epetra_Vector(output->Map())) );
            residue[i_spin][0]->Update(1.0, *states[1]->local_potential[i_spin], -1.0, *states[0]->local_potential[i_spin], 0.0);
        }
    }

    if(states.size()-1 < start){
        RCP<const Epetra_Vector> potential_in = states[states.size()-2]->local_potential[i_spin];
        output->Update(1-this->alpha, *potential_in, this->alpha);
        if(states.size()>2){
            residue[i_spin].append( rcp(new Epetra_Vector(output->Map())));
            residue[i_spin][residue[i_spin].size()-1]->Update(1.0, *states[states.size()-1]->local_potential[i_spin], -1.0, *states[states.size()-2]->local_potential[i_spin], 0.0);
        }
    }
    else{
        Array< RCP<Epetra_Vector> > potential_ins;
        for(int i=0;i<history+1;i++){
            // Appends multivector object having polarize+1 vectors to residue
            potential_ins.append( rcp(new Epetra_Vector(*states[states.size()-history+i-2]->local_potential[i_spin])) );

            // Assign value to the residue. value: save_densities[i]-save_densities[i-1]
            // residue = rho_out - rho_in
        }
        residue[i_spin].append( rcp(new Epetra_Vector(output->Map())));
        residue[i_spin][residue[i_spin].size()-1]->Update(1.0, *states[states.size()-1]->local_potential[i_spin], -1.0, *states[states.size()-2]->local_potential[i_spin], 0.0);

        while(residue[i_spin].size()>history+1){
            residue[i_spin].remove(0);
        }
        Broyden_Mixing::compute(residue[i_spin], potential_ins, broyden_alpha, i_spin, w_0, w_m, output);
    }

    if (i_spin<size){
        states[states.size()-1]->local_potential[i_spin] = output;
    }
    else if ((i_spin==0 and size ==0) or (i_spin==1 and size ==1) ){
        states[states.size()-1]->local_potential.append( output );
    }
    else{
        Verbose::all() << "Broyden_Mixing_Potential: i_spin: " << i_spin << std::endl
                       << "Broyden_Mixing_Potential: size:" << size << std::endl
                       << "Broyden_Mixing_Potential: invalid access" << std::endl;
        exit(-1);
    }
    return 0;
}
