#include "Pulay_Mixing_Potential.hpp"
#include "Epetra_Vector.h"

using Teuchos::rcp;
using Teuchos::RCP;
using Teuchos::Array;
using std::vector;

Pulay_Mixing_Potential::Pulay_Mixing_Potential(int start, int history, int max_history, double alpha, double pulay_alpha, bool update_paw/* = false*/)
    :Pulay_Mixing(start, history, max_history, alpha, pulay_alpha, update_paw){
    this -> mixing_type = "Potential";
}

int Pulay_Mixing_Potential::update( RCP<const Basis> basis, Array< RCP<Scf_State> > states, int i_spin, RCP<Nuclear_Potential> nuclear_potential/* = Teuchos::null*/){
    // OLD VERSION
    // density = density_in = save_densities_in
    // new_density = density_out = save_densities
    // NEW VERSION (current)
    // Actually, density_out(n) = density_in(n-1)
    // No need to save density_in and density_out separately.
    //
    // I don't like auto. I think it reduces readability.
    //
    // 2015. 06. 25. - Jaewook Kim
    // NEW VERSION is wrong! (not converged, density_in(n-1) = density_mix(n) != density_out(n) )
    // now this code follows old version.
    // Storing "residue" = density_out - density_in
    const int size = states[states.size()-1] -> local_potential.size();

    RCP<Epetra_Vector> output = rcp( new Epetra_Vector(*states[states.size()-1]->local_potential[i_spin]) );

    //when states.size()==2
    //states[0] = state_in(0)
    //states[1] = state_out(0)
    this -> residue.resize(size);
    if(states.size()==2){
        if(i_spin==0){
            Array<RCP<Epetra_Vector> > tmp;
            tmp.append( rcp(new Epetra_Vector(output->Map())) );
            tmp[0]->Update(1.0, *states[1]->local_potential[i_spin], -1.0, *states[0]->local_potential[i_spin], 0.0);
            residue.append(tmp);
        } else {
            residue[i_spin].append( rcp(new Epetra_Vector(output->Map())) );
            residue[i_spin][0]->Update(1.0, *states[1]->local_potential[i_spin], -1.0, *states[0]->local_potential[i_spin], 0.0);
        }
    }

    if(states.size()-1 < start){
        RCP<const Epetra_Vector> potential_in = states[states.size()-2]->local_potential[i_spin];
        output->Update(1-this->alpha, *potential_in, this->alpha);
        if(states.size()>2){
            residue[i_spin].append( rcp(new Epetra_Vector(output->Map())));
            residue[i_spin][residue[i_spin].size()-1]->Update(1.0, *states[states.size()-1]->local_potential[i_spin], -1.0, *states[states.size()-2]->local_potential[i_spin], 0.0);
        }
        if(nuclear_potential != Teuchos::null and this -> update_paw){
            vector<double> coeffs(1, 1.0);
            nuclear_potential -> mix_nuclear_potential( coeffs, alpha, i_spin );
        }
    }
    else{
        Array< RCP<Epetra_Vector> > potential_ins;
        for(int i=0;i<history;i++){
            // Appends multivector object having polarize+1 vectors to residue
            potential_ins.append( rcp(new Epetra_Vector(*states[states.size()-history+i-1]->local_potential[i_spin])) );

            // Assign value to the residue. value: save_densities[i]-save_densities[i-1]
            // residue = rho_out - rho_in
        }
        residue[i_spin].append( rcp(new Epetra_Vector(output->Map())));
        residue[i_spin][residue[i_spin].size()-1]->Update(1.0, *states[states.size()-1]->local_potential[i_spin], -1.0, *states[states.size()-2]->local_potential[i_spin], 0.0);

        while(residue[i_spin].size()>history){
            residue[i_spin].remove(0);
        }
        int ierr = Pulay_Mixing::compute(residue[i_spin], potential_ins, pulay_alpha, i_spin, output);
        if( ierr < 0 ){
            RCP<const Epetra_Vector> potential_in = states[states.size()-2]->local_potential[i_spin];
            output->Update(1-this->alpha, *potential_in, this->alpha);
        }
    }

    if (i_spin<size){
        states[states.size()-1]->local_potential[i_spin] = output;
    }
    else if ((i_spin==0 and size ==0) or (i_spin==1 and size ==1) ){
        states[states.size()-1]->local_potential.append( output );
    }
    else{
        Verbose::all() << "Pulay_Mixing_Potential: i_spin: " << i_spin << std::endl
                       << "Pulay_Mixing_Potential: size:" << size << std::endl
                       << "Pulay_Mixing_Potential: invalid access" << std::endl;
        exit(-1);
    }
    return 0;
}
