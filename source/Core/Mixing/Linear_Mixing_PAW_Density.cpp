#include "Linear_Mixing_PAW_Density.hpp"

using std::vector;
using Teuchos::rcp;
using Teuchos::RCP;
using Teuchos::Array;

Linear_Mixing_PAW_Density::Linear_Mixing_PAW_Density(double alpha)
    :Linear_Mixing(alpha){
    this->mixing_type = "PAW_Density";
    this -> linear_mdensity = rcp( new Linear_Mixing_Density(alpha) );
}

int Linear_Mixing_PAW_Density::update(RCP<const Basis> basis, Array<RCP<Scf_State> > states, int i_spin, RCP<Nuclear_Potential> nuclear_potential/* = Teuchos::null*/){
    //http://ftp.abinit.org/ws07/Liege_Torrent2_SC_Mixing.pdf
    //3rd page
    //states[states.size()-1] = states.size()-2-th out_state
    //states[states.size()-2] = states.size()-2-th in_state
    
    if( nuclear_potential == Teuchos::null ){
        Verbose::all() << "Linear_Mixing_PAW_Density::update nuclear_potential == null" << std::endl;
        return -1;
    }
    vector< vector<double> > descriptor;// np_descriptor_out
    nuclear_potential -> get_atomic_density_matrix(descriptor);
    //equivalent condition orginated from older version of this code
    //Do not mix anything for first step

    if( states.size()==1 || states.size()==2 ){
        if( i_spin == 0 ){
            this -> np_descriptor_in.push_back( descriptor );
        }
        return 0;
    }

    vector<double> out_state_des = descriptor[i_spin];
    vector<double> in_state_des = this -> np_descriptor_in[i_spin][this->np_descriptor_in.size()-1];

    vector<double> output = vector<double>(out_state_des.size());
    for(int i = 0; i < out_state_des.size(); ++i){
        output[i] = in_state_des[i]*(1-alpha)+out_state_des[i];
    }

    const int size = states[states.size()-1]->density.size();
    /*
    if (i_spin == 0){
        this -> np_descriptor_in.push_back( descriptor );
    }
    this -> np_descriptor_in[i_spin].back() = output;
    */
    this -> np_descriptor_in[i_spin].push_back(output);
    if( i_spin == size-1 ){
        vector< vector<double> > new_descriptor(size);
        for(int s = 0; s < size; ++s){
            new_descriptor[s] = this -> np_descriptor_in[s].back();
        }
        nuclear_potential -> set_atomic_density_matrix( new_descriptor );
    }
    this -> linear_mdensity -> update( basis, states, i_spin );
    return 0;
}

