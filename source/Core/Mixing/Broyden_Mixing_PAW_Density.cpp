#include "Broyden_Mixing_PAW_Density.hpp"

using std::vector;
using Teuchos::RCP;
using Teuchos::Array;

Broyden_Mixing_PAW_Density::Broyden_Mixing_PAW_Density(int start, int history, double alpha, double broyden_alpha, double w_0/* = 0.01;*/, double* w_m/* = NULL*/) 
    : Broyden_Mixing(start, history, alpha, broyden_alpha, w_0, w_m){
    this->mixing_type = "PAW_Density";
    this -> broyden_mdensity = Teuchos::rcp( new Broyden_Mixing_Density(start, history, alpha, broyden_alpha, w_0, w_m) );
}

int Broyden_Mixing_PAW_Density::update( RCP<const Basis> basis, Array< RCP<Scf_State> > states, int i_spin, RCP<Nuclear_Potential> nuclear_potential/* = Teuchos::null*/){
    const int size = states[states.size()-1]->density.size();
    
    if( nuclear_potential == Teuchos::null ){
        Verbose::all() << "Broyden_Mixing_PAW_Density::update nuclear_potential == null" << std::endl;
        return -1;
    }
    vector< vector<double> > descriptor;// np_descriptor_out
    nuclear_potential -> get_atomic_density_matrix(descriptor);
    int vec_size = descriptor[i_spin].size();
    //when states.size()==2
    //states[0] = state_in(0)
    //states[1] = state_out(0)
    this -> np_descriptor_in[i_spin].push_back( descriptor[i_spin] );
    if(states.size()==2){
        vector< vector<double> > initial_descriptor;
        nuclear_potential -> get_initial_atomic_density_matrix(initial_descriptor);
        this -> np_descriptor_in.push_back( vector< vector<double> >(1,initial_descriptor[i_spin]) );
        if(i_spin==0) this -> np_residue = vector< vector< vector<double> > >(size);
        vector<double> tmp_residue(vec_size);
        for(int i = 0; i < vec_size; ++i){
            tmp_residue[i] = descriptor[i_spin][i] - np_descriptor_in[i_spin][0][i];
        }
        this -> np_residue[i_spin].push_back(tmp_residue);
    }

    vector<double> output(vec_size);
    vector<double> out_state_des = descriptor[i_spin];
    vector<double> in_state_des = this -> np_descriptor_in[i_spin][this->np_descriptor_in[i_spin].size()-1];
    if(states.size()-1 < start){
        for(int i = 0; i < out_state_des.size(); ++i){
            output[i] = in_state_des[i]*(1-alpha)+out_state_des[i];
        }
        vector<double> tmp_residue(vec_size);
        for(int i = 0; i < vec_size; ++i){
            tmp_residue[i] = descriptor[i_spin][i] - np_descriptor_in[i_spin][states.size()-2][i];
        }
        this -> np_residue[i_spin].push_back(tmp_residue);
    } else {
        vector< vector<double> > density_ins;
        for(int i=0;i<history+1;i++){
            // Appends multivector object having polarize+1 vectors to residue
            // NEED history+1 density_in
            density_ins.push_back( this -> np_descriptor_in[i_spin][this -> np_descriptor_in[i_spin].size()+i-history-1] );
        }

        // Assign value to the residue. value: save_densities[i]-save_densities[i-1]
        // residue = rho_out - rho_in
        this -> np_residue[i_spin].push_back( vector<double>(vec_size) );
        for(int i = 0; i < vec_size; ++i){
            this -> np_residue[i_spin].back()[i] = out_state_des[i] - in_state_des[i];
        }

        while(np_residue[i_spin].size()>history+1){// NEED history+1 residue
            this -> np_residue[i_spin].erase(this->np_residue[i_spin].begin());
        }
        int ierr = Broyden_Mixing::compute(np_residue[i_spin], density_ins, broyden_alpha, i_spin, w_0, w_m, output);
        if( ierr < 0 ){
            for(int i = 0; i < out_state_des.size(); ++i){
                output[i] = in_state_des[i]*(1-alpha)+out_state_des[i];
            }
        }
    }

    /*
    if (i_spin == 0){
        this -> np_descriptor_in.push_back( descriptor );
    }
    this -> np_descriptor_in[i_spin].back() = output;
    */
    this -> np_descriptor_in[i_spin].push_back(output);
    if( i_spin == size-1 ){
        vector< vector<double> > new_descriptor(size);
        for(int s = 0; s < size; ++s){
            new_descriptor[s] = this -> np_descriptor_in[s].back();
        }
        nuclear_potential -> set_atomic_density_matrix( new_descriptor );
    }
    this -> broyden_mdensity -> update( basis, states, i_spin );
    return 0;
}
