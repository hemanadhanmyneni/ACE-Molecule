#pragma once
#include <vector>
#include "Teuchos_Array.hpp"
#include "Teuchos_RCP.hpp"
#include "Epetra_Vector.h"
#include "Mixing.hpp"

/**
 * @brief Abstract class that includes Pulay(DIIS) mixing computation routine.
 * @author Sungwoo Kang
 * @date 2015
 **/
class Pulay_Mixing: public Mixing{
    public:
        /**
         * @brief Default constructor of Pulay_Mixing
         * @param start Before this value, perform linear mixing instead. If this value is less than history+1, then it is set to the history+1.
         * @param history Number of previous PAW density matrix to mix.
         * @param alpha Mixing coefficient for initial linear mixing part.
         * @param pulay_alpha Mixing coefficient for Pulay mixing part.
         * @callergraph
         * @callgraph
         */
        Pulay_Mixing(
            int start,
            int history,
            int max_history,
            double alpha,
            double pulay_alpha,
            bool update_paw = false
        );
        /**
         * @brief Inherited from Mixing, but still pure virtual.
         * @callergraph
         * @callgraph
         **/
        virtual int update(
            Teuchos::RCP<const Basis> basis,
            Teuchos::Array< Teuchos::RCP<Scf_State> > states,
            int i_spin,
            Teuchos::RCP<Nuclear_Potential> nuclear_potential = Teuchos::null
        ) = 0;

        /**
         * @brief Returns formatted string that describes mixing class parameters.
         * @param width Width of display box.
         * @return Informative and formatted string about mixing parameters.
         * @callergraph
         * @callgraph
         **/
        virtual std::string get_mixing_info(int width = 50);
    protected:
        /**
         * @brief Performs Pulay mixing for Epetra_Vector.
         * @param residue Array of f_out-f_in.
         * @param density_ins Array of f_in.
         * @param pulay_alpha Mixing coeff. (1-alpha)*f_in+alpha*f_mixed.
         * @param i_spin Spin index (exists for PAW density matrix support).
         * @param output Mixed vector.
         * @param nuclear_potential PAW density matrix mixing support.
         * @note See Kresse, Phys. Rev. B 54, 11169 (1996)
         *       See http://www.hector.ac.uk/cse/distributedcse/reports/conquest/conquest/node3.html
         * @callergraph
         * @callgraph
         **/
        int compute(
            Teuchos::Array< Teuchos::RCP<Epetra_Vector> > residue,
            Teuchos::Array< Teuchos::RCP<Epetra_Vector> > density_ins,
            double pulay_alpha, int i_spin,
            Teuchos::RCP<Epetra_Vector>& output,
            Teuchos::RCP<Nuclear_Potential> nuclear_potential = Teuchos::null
        );
        /**
         * @brief Performs Pulay mixing for std::vector. Used for PAW_Density
         * @param residue Array of f_out-f_in.
         * @param density_ins Array of f_in.
         * @param pulay_alpha Mixing coeff. (1-alpha)*f_in+alpha*f_mixed.
         * @param i_spin Spin index (exists for PAW density matrix support).
         * @param output Mixed vector.
         * @note See Kresse, Phys. Rev. B 54, 11169 (1996)
         *       See http://www.hector.ac.uk/cse/distributedcse/reports/conquest/conquest/node3.html
         * @callergraph
         * @callgraph
         **/
        int compute(
            std::vector< std::vector<double> > residue,
            std::vector< std::vector<double> > density_ins,
            double pulay_alpha, int i_spin,
            std::vector<double> &output
        );

        /**
         * @brief Performs Pulay mixing for std::vector.
         * @param residue Array of f_out-f_in.
         * @param density_ins Array of f_in.
         * @param pulay_alpha Mixing coeff. (1-alpha)*f_in+alpha*f_mixed.
         * @param i_spin Spin index (exists for PAW density matrix support).
         * @param output Mixed vector.
         * @note See Kresse, Phys. Rev. B 54, 11169 (1996)
         *       See http://www.hector.ac.uk/cse/distributedcse/reports/conquest/conquest/node3.html
         * @callergraph
         * @callgraph
         **/
        int compute(
            std::vector< std::vector<std::complex<float> > > residue,
            std::vector< std::vector<std::complex<float> > > pol_ins,
            double pulay_alpha,
            std::vector<std::complex<float> >  &output
        );

        /**
         * @brief Performs Pulay mixing for std::vector.
         * @param residue Array of f_out-f_in.
         * @param density_ins Array of f_in.
         * @param pulay_alpha Mixing coeff. (1-alpha)*f_in+alpha*f_mixed.
         * @param i_spin Spin index (exists for PAW density matrix support).
         * @param output Mixed vector.
         * @note See Kresse, Phys. Rev. B 54, 11169 (1996)
         *       See http://www.hector.ac.uk/cse/distributedcse/reports/conquest/conquest/node3.html
         * @callergraph
         * @callgraph
         **/
        int compute(
                std::vector< std::vector<std::complex<float> > > residue,
                std::vector< std::vector<std::complex<float> > > pol_ins,
                double pulay_alpha,
                std::vector<std::complex<float> >  &output ,
                Teuchos::SerialDenseMatrix<int, std::complex<float> > pulay_matrix
                );

        /**
         * @brief Calculate pulay mixing coefficients from pulay matrix.
         * @param pulay_matrix Pulay residual matrix.
         * @param dim Dimension of the matrix.
         * @param coeffs [out] Output coefficients. [1.0/dim]*dim if error is detected.
         * @return abs(ret) > 10000 then getrf error with info = ret/10000, abs(ret) > 100 then getri error with info = ret/100. Otherwise, solved matrix suggests extrapolation.
         * @callergraph
         * @callgraph
         **/
        template <typename T> 
        int get_coeffs(Teuchos::SerialDenseMatrix<int, T> pulay_matrix, int dim, std::vector<T>& coeffs);

        double alpha, pulay_alpha;
        int start, history, max_history;
        bool update_paw = true;

        /**
         * @brief Previous `history` residues. Index order: residue[spin index][history index].
         **/
        Teuchos::Array< Teuchos::Array< Teuchos::RCP<Epetra_Vector> > > residue;
};
