#pragma once
#include <vector>
#include "Teuchos_RCP.hpp"
#include "Teuchos_Array.hpp"
#include "Epetra_Vector.h"

#include "Broyden_Mixing.hpp"
#include "Broyden_Mixing_Density.hpp"
#include "../../State/Scf_State.hpp"

/**
 * @brief Performs Broyden mixing for PAW density matrix and density.
 * @note See Broyden_Mixing class.
 * @note This class owns Broyden_Mixing_Density to update both PAW density matrix and PS density.
 **/
class Broyden_Mixing_PAW_Density: public Broyden_Mixing {
    public:
        /**
         * @brief Constructor.
         * @param start Before this value, perform linear mixing instead. If this value is less than history+1, then it is set to the history+1.
         * @param history Number of previous potentials to mix.
         * @param alpha Mixing coefficient for initial linear mixing part.
         * @param broyden_alpha Mixing coefficient for Broyden mixing part.
         * @param w_0 See PRC 78 014318 (2008) Section II.B
         * @param w_m See PRC 78 014318 (2008) Section II.B
         **/
        Broyden_Mixing_PAW_Density(
            int start, int history, 
            double alpha, double broyden_alpha, 
            double w_0 = 0.01, double* w_m = NULL
        );
        /**
         * @brief Inherited from Mixing. See Mixing.
         * @return Always 0.
         **/
        int update(
            Teuchos::RCP<const Basis> basis, 
            Teuchos::Array< Teuchos::RCP<Scf_State> > states, 
            int i_spin, 
            Teuchos::RCP<Nuclear_Potential> nuclear_potential = Teuchos::null 
        );

    private:
        //double alpha, broyden_alpha;
        //int start, history;
        //double w_0;
        //std::vector<double> w_m;
        //Teuchos::Array< Teuchos::Array< Teuchos::RCP<Epetra_Vector> > > residue; // residue[i_spin][history_index]
        std::vector< std::vector< std::vector<double> > > np_descriptor_in;
        std::vector< std::vector< std::vector<double> > > np_residue;
        Teuchos::RCP<Broyden_Mixing_Density> broyden_mdensity;
};
