#pragma once 
#include "Teuchos_RCP.hpp"
#include "Epetra_Vector.h"
#include "../../Utility/Time_Measure.hpp"
#include "Teuchos_ParameterList.hpp"

#include "../../State/State.hpp"
#include "../../Basis/Basis.hpp"

class Solvation {
    public:
        virtual double compute( Teuchos::RCP<State> state, 
                                Teuchos::RCP<Epetra_Vector> hartree_potential) =0;
        /**
         * @brief Store psuedo charge internally.
         * @param atoms [in] atoms object  
         * @param charges [in] psuedo charge of atoms (ex. C:4)
        **/
        virtual void initialize(Teuchos::RCP<const Atoms> atoms, std::vector<double> charges)=0;

        virtual std::vector<double> interpolate_mep(Teuchos::RCP<Epetra_Vector> ia_potential)=0;
        virtual std::vector<double> interpolate_mep(double* ia_potential)=0;
        virtual double compute_polarization_from_mep_allproc(std::vector<double> ia_mep, std::vector<double> jb_mep)=0;

    protected:
        Teuchos::RCP<const Basis> mesh;
        Teuchos::RCP<Teuchos::ParameterList> parameters;
        Teuchos::RCP<Epetra_Vector> potential_due_to_solvation;
        Teuchos::RCP<Time_Measure> timer;
};
