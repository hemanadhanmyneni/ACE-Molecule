#include "Create_Solvation.hpp"
#include <stdexcept>
#include "../../Utility/Verbose.hpp"

#ifdef ACE_PCMSOLVER
#include "Interface_PCMSolver.hpp"
namespace Create_Solvation{
    PCMInput initialize_pcmsolver(Teuchos::RCP<Teuchos::ParameterList> parameters);
}
#endif 

using std::string;
using Teuchos::RCP;

Teuchos::RCP<Solvation> Create_Solvation::Create_Solvation(Teuchos::RCP<const Basis> basis, Teuchos::RCP<Teuchos::ParameterList> parameters){
#ifdef ACE_PCMSOLVER
    if(parameters -> get<string>("SolvationLibrary", "") == "PCMSolver"){
        string type = parameters -> get<string>("SolverType", "None");

        double charge_width = parameters -> get<double>("ChargeWidth", 1.0);
        if(parameters -> isParameter("PCMInputFilename")){
            return Teuchos::rcp(new Interface_PCMSolver(basis, parameters -> get<string>("PCMInputFilename"), charge_width, false));
        }
        else {
            if(type == "CPCM" or type == "IEFPCM"){
                return Teuchos::rcp(new Interface_PCMSolver(basis, type, Create_Solvation::initialize_pcmsolver(parameters), charge_width, true));
            } else if(type == "None"){
                return Teuchos::null;
            } else {
                Verbose::all() << "Unknown SolverType " << type << "!" << std::endl;
                throw std::invalid_argument("SolverType should be CPCM or IEFPCM!");
            }
        } 
    } 
#else
    if(parameters ->isParameter("SolvationLibrary") or parameters->isParameter("PCMInputFilename")){
        Verbose::single(Verbose::Simple)<< "Please check compile options. "<<std::endl;
        Verbose::single(Verbose::Simple)<< "Currently solvation is not supported."<<std::endl;
    }
#endif
    return Teuchos::null;
}


#ifdef ACE_PCMSOLVER
PCMInput Create_Solvation::initialize_pcmsolver(Teuchos::RCP<Teuchos::ParameterList> parameters){

    PCMInput host_input;

    strcpy(host_input.cavity_type, "gepol");// Just disabled restart option.
    host_input.patch_level = 2;// Undocumented default value.
    host_input.coarsity = 0.5;// Undocumented default value.
    //host_input.area = 0.3;
    host_input.min_distance = 0.1;// Undocumented default value.
    host_input.der_order = 4;// Undocumented default value.
    //host_input.scaling = true;
    //strcpy(host_input.radii_set, "bondi");
    strcpy(host_input.restart_name, "");// Just disabled restart option.
    //host_input.min_radius = 100.0;

    //strcpy(host_input.solver_type, "cpcm");
    //strcpy(host_input.solvent, "water");
    strcpy(host_input.equation_type, "secondkind");// Undocumented default value.
    //host_input.correction = 0.0;
    //host_input.probe_radius = 1.0;

    strcpy(host_input.inside_type, "vacuum");
    // Following 2 lines are irrelevant unless solvent is Explicit.
    host_input.outside_epsilon = 1.0;
    strcpy(host_input.outside_type, "uniformdielectric");

    // Top section inputs
    //parameters -> set<string>("Units", "AU");
    //parameters -> set<int>("CODATA", 2010);// Defines AU <-> SI constant version.

    // Cavity section inputs
    strcpy(host_input.radii_set, parameters -> get<string>("RadiiSet", "Bondi").c_str()); // Choice between Bondi, UFF, Allinger

    // scaling true turns on the scaling by 1.2
    host_input.scaling = (1 == parameters -> get<int>("Scaling", 0));

    /*
    // Mode is choice between Implicit, Atoms, Explicit.
    // Unsupported for use in host.
    parameters -> get<string>("Mode", "Implicit");
    if(parameters -> get<string>("Mode") == "Explicit"){
    //parameters -> set< Array<double> >("Spheres");
    }
    else if(parameters -> get<string>("Mode", "Atoms")){
    //parameters -> set< Array<int> >("Atoms");
    //parameters -> set<Array<double> >("Radii");
    }
    */
    host_input.area = parameters -> get<double>("Area", 0.3);
    host_input.min_radius = parameters -> get<double>("MinRadius", 100);

    // Medium section inputs
    strcpy(host_input.solver_type, parameters -> get<string>("SolverType", "CPCM").c_str()); // PCMSolver default is IEFPCM
    // UNIFORMDIELECTRIC with specified Eps and EpsDyn.
    strcpy(host_input.solvent, parameters -> get<string>("Solvent").c_str());
    //if(parameters -> get<string>("SolverType", "IEFPCM")){
    //    parameters -> get<bool>("MatrixSymm", true);// Unsupported for use in host.
    //}
    host_input.correction = parameters -> get<double>("Correction", 0);
    host_input.probe_radius = parameters -> get<double>("ProbeRadius", 1.0);
    //parameters -> set<string>("DiagonalIntegrator", "COLLOCATION"); // Only option by 1.1.11 // Unsupported for use in host.
    //host_input.parameters -> get<double>("DiagonalScaling", 1.07);// Unsupported for use in host.

    // Nonequilibrium calculations are done by calling pcmsolver_compute_response_asc instead of pcmsolver_compute_asc
    //this -> is_neq = parameters -> get<bool>("Nonequilibrium", false);

    // Green section inputs for Explicit solvent.
    // Note: EpsDyn is eps_\inf.
    // ChargeDistribution section inputs

    return host_input;
}
#endif

