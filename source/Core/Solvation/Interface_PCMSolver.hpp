#pragma once
#include <complex>
#include "Solvation.hpp"
#include "Teuchos_RCP.hpp"
#include "Teuchos_ParameterList.hpp"
#include "Teuchos_Array.hpp"

#include "../../State/State.hpp"
#include "../../Core/Pseudo-potential/Nuclear_Potential.hpp"
#include "../../Basis/Basis.hpp"
#include "../../Utility/Time_Measure.hpp"

//#define ACE_ (1)
#ifdef ACE_PCMSOLVER
extern "C"{
#include "PCMSolver/pcmsolver.h"
}
/**
 * @author Hyeonsu Kim, Sungwoo Kang
 * @todo
 * @note For PCMSolver, see https://pcmsolver.readthedocs.io/en/stable/.
 * @node Reference: DOI: 10.1063/1.4932593 and DOI: 10.1063/1.1394921
 **/
class Interface_PCMSolver:public Solvation{
    public:
        /**
         * @brief Default constructor.
         * @param basis [in] Lagrange basis.
         * @param solver_type [in] string solver type.
         * @param pcm_input [in] Input for Interface_PCMSolver library.
         * @param charge_width [in] Gaussian width to be used for broadning ASC.
         * @param allow_nonequilibrium  [in] If false, always calls pcmsolver_compute_asc, so ASC can never be nonequilibrium.
         **/
        Interface_PCMSolver(Teuchos::RCP<const Basis> basis, 
                            std::string solver_type, 
                            PCMInput pcm_input, 
                            double charge_width = 1.0, 
                            bool allow_nonequilibrium = false);

        /**
         * @brief Default constructor.
         * @param basis [in] Lagrange basis.
         * @param input_filename [in] Input filename for Interface_PCMSolver library.
         * @param charge_width [in]  Gaussian width to be used for broadning ASC.
         * @param allow_nonequilibrium [in] If false, always calls pcmsolver_compute_asc, so ASC can never be nonequilibrium
         */
        Interface_PCMSolver(Teuchos::RCP<const Basis> basis, 
                            std::string input_filename, 
                            double charge_width = 1.0, 
                            bool allow_nonequilibrium = false);
        ~Interface_PCMSolver();


        /**
         * @brief Store psuedo charge internally.
         * @param atoms [in] atoms object  
         * @param charges [in] psuedo charge of atoms (ex. C:4)
        **/
        void initialize(Teuchos::RCP<const Atoms> atoms, std::vector<double> charges);

        /**
          * @brief This function returns electric potential due to the solvent
          */
        Teuchos::RCP<const Epetra_Vector> get_PCM_local_potential();

        /**
         * @brief returns MEP on cavity surface from Hartree potential of electron density.
         * @param[in] ia_potential Hartree potential of electron density.
         **/
        std::vector<double> interpolate_mep(Teuchos::RCP<Epetra_Vector> ia_potential);

        /**
         * @brief returns MEP on cavity surface from Hartree potential of electron density.
         * @param[in] ia_potential Hartree potential of electron density.
         **/
        std::vector<double> interpolate_mep(double* ia_potential);

        /**
         * @brief Returns polarization energy from MEP. All processors runs this.
         * @param[in] ia_mep First MEP.
         * @param[in] jb_mep Second MEP.
         * @return Integrated value of first MEP and ASC from second MEP.
         **/
        double compute_polarization_from_mep_allproc(std::vector<double> ia_mep, std::vector<double> jb_mep);

        /**
         * @brief Returns PCMSolver cavity grid.
         **/
        std::vector< std::array<double,3> > get_grid();
        /**
         * @brief Returns PCMSolver catity grid quadrature weight.
         **/
        std::vector<double> get_areas();

        /**
         * @brief Calculate PCM solvation energy and potential. Store potential internally.
         * @param state State to compute
         * @param hartree_potential Hartree potential (electrostatic potential of electron density)
         * @param nulcear_charges Effective nuclear charges 
         * @return Solvation polarization energy.
         **/
        double compute(Teuchos::RCP<State> state, 
                       Teuchos::RCP<Epetra_Vector> hartree_potential); 

    protected:
        /**
         * @return Nuclear contribution to molecular electrostatic potential. Should be delete[]-ed after use.
         **/
        double * nuclear_mep(std::vector<double> charges, std::vector< std::array<double,3> > coordinates, int grid_size, double * grid);

        /**
         * @brief Interpolate hartree potential to PCM cavity.
         * @param [in] basis 
         * @param [in] hartree_potential 
         * @param [in] grid_size
         * @param [in] grid 
         * @return Electron contribution to molecular electrostatic potential. Should be delete[]-ed after use.
         **/
        double * electron_mep(Teuchos::RCP<const Basis> basis, Teuchos::RCP<Epetra_Vector> hartree_potential, int grid_size, double * grid);
        /**
         * @brief Interpolate hartree potential to PCM cavity.
         * @param [in] basis 
         * @param [in] hartree_potential 
         * @param [in] grid_size
         * @param [in] grid 
         * @return Electron contribution to molecular electrostatic potential. Should be delete[]-ed after use.
         **/
        double * electron_mep(Teuchos::RCP<const Basis> basis, double* hartree_potential, int grid_size, double * grid);

        Teuchos::RCP<Epetra_Vector> calculate_polarization_potential(
            Teuchos::RCP<const Basis> basis, double alpha, int grid_size,
            double* areas, double* grid_charge, double* grid
        );
        //std::vector<double> atom_type_charges;
        std::vector<double> pseudo_charges;
        Teuchos::RCP<Epetra_Vector> PCM_local_potential;
        Teuchos::RCP<const Basis> basis;
        Teuchos::RCP<Teuchos::ParameterList> parameters;
//        Teuchos::RCP<const Atoms> atoms;
        bool allow_nonequilibrium = false;
        double alpha = 1.0;
        struct PCMInput host_input;
        std::string input_filename;
        pcmsolver_context_t *pcm_context = NULL;
        int grid_size = 0;
        double * grid = NULL;
        double * areas = NULL;
        std::vector<double> mol_charges;
        Teuchos::RCP<Time_Measure> timer;
};
#else
//struct PCMInput{}; // Dummy routine.
// Dummy class.
class PCMSolver{
   public:
       double compute(Teuchos::RCP<State> state, Teuchos::RCP<Epetra_Vector> hartree_potential){return 0.0;};

       Teuchos::RCP<const Epetra_Vector> get_PCM_local_potential(){return Teuchos::null;};
       void initialize(Teuchos::RCP<const Atoms> atoms, std::vector<double> charges){};
};
#endif
