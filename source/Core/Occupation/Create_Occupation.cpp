#include "Create_Occupation.hpp"
#include <vector>
#include <string>
#include "Occupation_Zero_Temp.hpp"
#include "Occupation_Fermi.hpp"
#include "Occupation_From_Input.hpp"
#include "../../Utility/String_Util.hpp"

using std::vector;
using std::string;
using Teuchos::Array;

Teuchos::Array<Teuchos::RCP<Occupation> > Create_Occupation::Create_Occupation(Teuchos::RCP<Teuchos::ParameterList> parameters ){
    std::string occupation_type = parameters->get<std::string>("OccupationMethod", "ZeroTemp");
    if (occupation_type.compare("Fermi")==0 and !parameters->isParameter("Temperature")){
        parameters->set("Temperature",5.0);
        Verbose::single()<< "Create_Occupation::Fermi temperature: " << parameters->get<double>("Temperature") <<std::endl;
    }

    double num_electrons = parameters->get<double>("NumElectrons");
    double spin_multiplicity =  parameters->get<double>("SpinMultiplicity") ;
    bool is_polar = (spin_multiplicity!=1.0);
    int num_occupied_orbitals = int(num_electrons/2.0);
    if(num_electrons > num_occupied_orbitals*2.0) num_occupied_orbitals++;
    int occupation_size = num_occupied_orbitals;

    if(parameters -> isParameter("Polarize")){
         if(parameters -> get<int>("Polarize") == 1){
             is_polar = true;
         } else {
             is_polar = false;
         }
    }
    if(parameters -> isParameter("OccupationSize")){
        int inp_occ_size = parameters -> get<int>("OccupationSize");
        if(inp_occ_size > occupation_size){
            occupation_size = inp_occ_size;
        }
    } else if (parameters -> isParameter("NumberOfVirtualStates")){
        occupation_size += parameters -> get<int>("NumberOfVirtualStats");
    }
    vector< vector<double> > occ_from_inp(is_polar? 2: 1);
    if(parameters -> get<string>("OccupationMethod") == "Input"){
        for(int s = 0; s < occ_from_inp.size(); ++s){
            vector<string> occ_str_vec = String_Util::Split_ws(parameters -> get< Array<string> >("OccupationList")[s]);
            for(int i = 0; i < occ_str_vec.size(); ++i){
                if(String_Util::isStringDouble(occ_str_vec[i].c_str())){
                    occ_from_inp[s].push_back(String_Util::stod(occ_str_vec[i]));
                } else {
                    int times = String_Util::stoi(occ_str_vec[i].substr(1));
                    for(int j = 0; j < times-1; ++j){
                        occ_from_inp[s].push_back(occ_from_inp[s].back());
                    }
                }
            }
        }
    }

    Teuchos::Array<Teuchos::RCP<Occupation> > occupations;

    if(!is_polar){
        if(occupation_type.compare("ZeroTemp")==0){
            occupations.append(Teuchos::rcp(new Occupation_Zero_Temp(is_polar, spin_multiplicity, 0, num_electrons, occupation_size) ) );
        }
        else if (occupation_type.compare("Fermi")==0){
            double temp = parameters->get<double>("Temperature");
            occupations.append(Teuchos::rcp(new Occupation_Fermi( is_polar, spin_multiplicity, 0, num_electrons, temp, occupation_size) ) );
        }
        else if (occupation_type.compare("Input") == 0){
            occupations.append(Teuchos::rcp(new Occupation_From_Input(occ_from_inp[0])));
        }

    } else{
        double spin_up = (num_electrons + spin_multiplicity - 1.0) / 2.0;
        double spin_down = num_electrons-spin_up ;
        if(spin_up>occupation_size){
            occupation_size = spin_up;
        }
        if(spin_down>occupation_size){
            occupation_size = spin_down;
        }

        if(occupation_type.compare("ZeroTemp")==0){
            occupations.append(Teuchos::rcp( new Occupation_Zero_Temp(is_polar, spin_multiplicity, 0, num_electrons, occupation_size)  ));
            occupations.append(Teuchos::rcp( new Occupation_Zero_Temp(is_polar, spin_multiplicity, 1, num_electrons, occupation_size)  ));
        }
        else if(occupation_type.compare("Fermi")==0){
            double temp = parameters->get<double>("Temperature");
            occupations.append(Teuchos::rcp(new Occupation_Fermi( is_polar, spin_multiplicity, 0, num_electrons, temp, occupation_size) ) );
            occupations.append(Teuchos::rcp(new Occupation_Fermi( is_polar, spin_multiplicity, 1, num_electrons, temp, occupation_size) ) );
        }
        else if (occupation_type.compare("Input")==0){
            occupations.append(Teuchos::rcp(new Occupation_From_Input(occ_from_inp[0])));
            occupations.append(Teuchos::rcp(new Occupation_From_Input(occ_from_inp[1])));
        }
    }

    return occupations;
}
