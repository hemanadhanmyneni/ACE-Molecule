#pragma once
#include "Occupation.hpp"
class Occupation_Zero_Temp: public Occupation{
    public:
        Occupation_Zero_Temp(bool is_polar,double spin_multiplicity, int spin_index, double num_elec,int size);
        ~Occupation_Zero_Temp(){};
        Occupation* clone() const;
};
