#pragma once
#include <vector>
#include <iostream>
#include <AnasaziTypes.hpp>
#include "../../Utility/Verbose.hpp"

class Occupation {

    //friend std::ostream &operator << (std::ostream &c, Occupation &occupation);
    friend std::ostream &operator << (std::ostream &c, const Occupation &occupation);
    //friend std::ostream &operator << (std::ostream &c, Occupation* poccupation);
    friend std::ostream &operator << (std::ostream &c, const Occupation* &occupation);

    public:
        virtual double operator[](int index) const;
        Occupation& operator=(const Occupation& original_occupation);
        virtual ~Occupation(){};
        virtual double get_total_occupation() const;
        int get_size() const;
        void print(std::vector<double > eigenvalues) const;
        void resize(int new_size);
        virtual void set_eigenvalues(std::vector<double> eigenvalues) {
            this->eigenvalues.clear();
            this->eigenvalues.assign(eigenvalues.begin(), eigenvalues.end());
            this->size = eigenvalues.size();
        };
        std::vector<double> get_occupation_list() const {return occupation;} ;
        std::vector<double> get_eigenvalues() const {return eigenvalues;} ;
        //double get_num_occ_orbitals() const {return num_occupied_orbitals;} ;
        virtual Occupation* clone() const =0;
        std::string get_type(){return type;};
        //virtual void compute( std::vector<double > eigenvalues,double number_of_occupied_states)=0;
        //virtual void compute(double number_of_occupied_states) = 0;
        //virtual void compute(double number_of_occupied_states, int total_number_of_states) = 0;

        //void compute(std::vector<double> occupation);
        
        void print_occupation() const;

    protected:
        std::vector<double> eigenvalues;
        std::vector<double> occupation;
        std::string type;
        bool is_polar;
        double size;
        double temp;
        double spin_multiplicity;
        double num_elec;
        int spin_index;
};
