#pragma once
#include "../../State/Dynamic_Accessor.hpp"
#include "Convergence.hpp"

class Convergence_EigenvalueSum: public Convergence{
    public:
        virtual bool converge(
            Teuchos::RCP<const Basis> basis,
            Teuchos::Array< Teuchos::RCP<Scf_State> > states
        );
        Convergence_EigenvalueSum(double tolerance);
    protected:
};
