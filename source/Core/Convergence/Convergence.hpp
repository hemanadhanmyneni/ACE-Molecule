#pragma once
#include <string>
#include "Teuchos_RCP.hpp"
#include "Teuchos_Array.hpp"

#include "../../Basis/Basis.hpp"
#include "../../State/Scf_State.hpp"

class Convergence{
    public:
        /**
         * @brief Virtual destructor for abstract class.
         **/
        virtual ~Convergence(){};
        virtual bool converge( 
            Teuchos::RCP<const Basis> basis, 
            Teuchos::Array< Teuchos::RCP<Scf_State> > states
        )=0;
        bool is_diagonalize_converged = false;
        double get_tolerance(){return tolerance;};
    protected:
        int norm;
        double tolerance;
        std::string convergence_type;
        void print(double current_value){Verbose::single() << "Convergence:: current_value / criteria \nConvergence:: "<<current_value <<" / " << tolerance <<std::endl;};
};
