#pragma once
//#include "../../State/Dynamic_Accessor.hpp"
#include "Teuchos_RCP.hpp"
#include "Teuchos_Array.hpp"

#include "Convergence.hpp"
#include "../../State/Scf_State.hpp"

class Convergence_Density: public Convergence{
    public:
        virtual bool converge(
            Teuchos::RCP<const Basis> basis,
            Teuchos::Array< Teuchos::RCP<Scf_State> > states
        );
        Convergence_Density(double tolerance, int norm);
    protected:
};
