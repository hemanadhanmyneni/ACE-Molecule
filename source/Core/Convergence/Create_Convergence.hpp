#pragma once
#include <string>

#include "Teuchos_RCP.hpp"
#include "Teuchos_ParameterList.hpp"

#include "Convergence.hpp"

namespace Create_Convergence{
    Teuchos::RCP<Convergence> Create_Convergence(Teuchos::RCP<Teuchos::ParameterList> parameters);
}
