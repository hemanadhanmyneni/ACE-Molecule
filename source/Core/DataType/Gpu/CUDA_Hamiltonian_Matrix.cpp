#ifdef USE_CUDA
#include "CUDA_Hamiltonian_Matrix.hpp"
#include "../../../Utility/Parallel_Manager.hpp"
#include "../../../Utility/Time_Measure.hpp"
#include "cuda_runtime_api.h"
#include "cublas_v2.h"
#include "device_launch_parameters.h"
#include "nvToolsExt.h"
#include "Epetra_Import.h"
#include "Epetra_MultiVector.h"

namespace CUDA{
//extern cublasHandle_t Global_handle;
using Teuchos::RCP;
using Teuchos::rcp;
using std::cout;
using std::endl;

Teuchos::RCP<Time_Measure> timer = Teuchos::rcp(new Time_Measure());
/////////////////////////////////////////////
//From now, MultiVec
////////////////////////////////////////////

template< >
void Hamiltonian_Matrix<double>::Apply(const CUDA::MultiVec<double>& X, CUDA::MultiVec<double>& Y) const {
//void Hamiltonain_Matrix<double>::Apply(const Anasazi::MultiVec<double>& X, Anasazi::MultiVec<double>& Y) const ;
    if(hamiltonian_matrix==Teuchos::null){
        std::cout << "Somthing wrong...jaechang" << std::endl;
        exit(-1);
    }

    Teuchos::RCP<Time_Measure> timer = Teuchos::rcp(new Time_Measure() );
    timer->start("Hamiltonian_Matrix::Apply");
    CUDA::MultiVec<double>* tmp_X = dynamic_cast<CUDA::MultiVec<double>* >(&const_cast<CUDA::MultiVec<double>& >(X));
    CUDA::MultiVec<double>* tmp_Y = dynamic_cast<CUDA::MultiVec<double>* >(&Y);

    int length = tmp_X->GetVecLength();
    int new_num_vec = tmp_X->GetNumberVecs();

    std::vector<double*> address_X;
    std::vector<double*> address_Y;
    tmp_X->get_address(address_X);
    tmp_Y->get_address(address_Y);
    nvtxRangePushA("Opx");
    if(!hamiltonian_matrix_in_gpu){
        ColMap = Teuchos::rcp(new Epetra_Map(hamiltonian_matrix->ColMap()));
        if(hamiltonian_matrix->Importer() != 0){
            Importer = Teuchos::rcp(new Epetra_Import(*hamiltonian_matrix->Importer()));
        }

        if(hamiltonian_matrix->StorageOptimized ()==false){
            hamiltonian_matrix-> OptimizeStorage ();
        }

        cudaError_t status;

        int* row_index ;
        int* tmp_row_index ;
        //        int* col_index ;
        int* tmp_col_index ;
        double* values ;

        nnz = hamiltonian_matrix->NumMyNonzeros();
        num_row = hamiltonian_matrix->NumMyRows();
        global_num_row = hamiltonian_matrix->NumGlobalRows();
        if (!hamiltonian_matrix->ExtractCrsDataPointers(row_index, tmp_col_index, values)==0){
            std::cout << "extraction is failed" << std::endl;
        }

        cudaMalloc((void**) &d_row_index, sizeof(int)*(num_row+1));
        cudaMalloc((void**) &d_values, sizeof(double)*nnz);
        cudaMalloc((void**) &d_col_index, sizeof(int)*nnz);
        cudaStream_t stream[3];
        for(int i =0; i<3; i++){
            cudaStreamCreate(&stream[i]);
        }

        auto info = cudaMemcpyAsync(d_values, values, sizeof(double)*nnz, cudaMemcpyHostToDevice,stream[0]);
        if (info!=0){
            std::cout << "shchoi2: " << info << std::endl;
        }
        tmp_row_index = new int[num_row+1];
        #ifdef ACE_HAVE_OMP
        #pragma omp parallel for 
        #endif 
        for(int i=0; i<num_row; i++)
            tmp_row_index[i] = row_index[i];                 
        tmp_row_index[num_row] = nnz;


        info = cudaMemcpyAsync(d_row_index, tmp_row_index, sizeof(int)*(num_row+1), cudaMemcpyHostToDevice,stream[1]);
        if (info!=0){
            std::cout << "shchoi1: " << info << std::endl;
        }


        int* col_index = new int[nnz];
        auto col_my_global_elements = hamiltonian_matrix->ColMap().MyGlobalElements();
        #ifdef ACE_HAVE_OMP
        #pragma omp parallel for 
        #endif 
        for(int i=0; i<nnz;i++){
            //col_index[i] = matrix->Graph().GCID(tmp_col_index[i]);
            //col_index[i] = matrix->ColMap().GCID(tmp_col_index[i]);
            col_index[i] = col_my_global_elements[tmp_col_index[i]];
        }
        info = cudaMemcpyAsync(d_col_index, col_index, sizeof(int)*nnz, cudaMemcpyHostToDevice,stream[2]);
        if (info!=0){
            std::cout << "shchoi3: " << info << std::endl;
        }

        for(int i =0; i<3; i++){
            cudaStreamSynchronize(stream[i]);
            cudaStreamDestroy(stream[i]);
        }

        cusparseCreate(&cusparse_handle);
        //for(int i =0; i<nnz; i++){
        //    std::cout << d_values[i]  << " " << d_col_index<<std::endl;
        //}
        Parallel_Manager::info().all_barrier();
        delete [] tmp_row_index;
    }

    hamiltonian_matrix_in_gpu=true;
    cusparseStatus_t status;
    cusparseMatDescr_t descr;
    status = cusparseCreateMatDescr(&descr);
    if (CUSPARSE_STATUS_SUCCESS != status){
        std::cout << "error on cusparseCreateMatDescr " <<std::endl;
        exit(-1);
    }
    status = cusparseSetMatType(descr,CUSPARSE_MATRIX_TYPE_GENERAL);
    if (CUSPARSE_STATUS_SUCCESS != status){
        std::cout << "error on cusparseSetMatType" <<std::endl;
        exit(-1);
    }

    cusparseSetMatIndexBase(descr,CUSPARSE_INDEX_BASE_ZERO);
    cusparseSetPointerMode(cusparse_handle, CUSPARSE_POINTER_MODE_HOST);

    double one = 1;
    double zero = 0;

    //This is part for gathering data among gpus for matrix vector multiplication
    int mpi_size = Parallel_Manager::info().get_total_mpi_size();


    if(mpi_size>1){


        nvtxRangePushA("prepare");
        int* sizeList = new int[mpi_size];
        int* sizeList2 = new int[mpi_size];
        Parallel_Manager::info().group_allgather(&num_row, 1, sizeList, 1);
        for(int i=0; i<mpi_size; i++)
            sizeList2[i] = sizeList[i]*new_num_vec;
        int* displs = new int[mpi_size];
        int* displs2 = new int[mpi_size];
        int MyPID = Parallel_Manager::info().get_total_mpi_rank();
        for(int i=0; i<mpi_size;i++){
            if (i!=0){
                displs2[i] = displs2[i-1]+sizeList2[i-1];
                displs[i] = displs[i-1]+sizeList[i-1];
            }
            else{
                displs2[i]=0;
                displs[i]=0;
            }
        }
#ifdef GPU_DIRECT
        for(int i=0; i<new_num_vec; i++){
            Parallel_Manager::info().group_allgatherv(address_X[0], num_row, &combine_Mv_X[global_num_row*i], sizeList2, displs2);
        }
#else
       //reallocate host_Mv_X when incoming multivector is larger than previous one 
        if(num_vec < new_num_vec){
            if(is_cuMallocHost){
                cudaFreeHost(host_Mv_X);
                cudaFreeHost(host_combine_Mv_X);
                cudaFree(combine_Mv_X);
            }
            cudaMallocHost((void**)& host_Mv_X, sizeof(double)*num_row*new_num_vec);
            cudaMallocHost((void**)& host_combine_Mv_X, sizeof(double)*global_num_row*new_num_vec);
            cudaMalloc((void**)&combine_Mv_X, sizeof(double)*global_num_row*new_num_vec);
            is_cuMallocHost = true;
            num_vec = new_num_vec;
        }


        nvtxRangePop();
        nvtxRangePushA("DtoH");
        cudaMemcpy(host_Mv_X, address_X[0], sizeof(double)*num_row*new_num_vec, cudaMemcpyDeviceToHost);


        nvtxRangePop();
        nvtxRangePushA("HtoH Allocation");
        int rank = Parallel_Manager::info().get_total_mpi_rank();
        Teuchos::RCP<Epetra_MultiVector> tmp_MultiVector  = Teuchos::rcp(new Epetra_MultiVector(View, hamiltonian_matrix->RowMap(), host_Mv_X, length, new_num_vec  ));
        nvtxRangePop();

        nvtxRangePushA("HtoH Communication");
        Teuchos::RCP<Epetra_MultiVector> CombineMultiVector = Teuchos::rcp( new Epetra_MultiVector(*ColMap(),new_num_vec)); 
        CombineMultiVector->Import(*tmp_MultiVector, *Importer, Insert);
        nvtxRangePop();

        nvtxRangePushA("HtoH rearrange");
        double** Xp = (double**)CombineMultiVector->Pointers();
        for(int i=0; i<new_num_vec; i++){
#ifdef ACE_HAVE_OMP
#pragma omp parallel for
            for(int j=0; j<CombineMultiVector->Map().NumMyElements(); j++){
#endif
                host_combine_Mv_X[i*global_num_row + CombineMultiVector->Map().MyGlobalElements()[j]] = Xp[i][j];
            }
        }
        nvtxRangePop();

        nvtxRangePushA("HtoD");
        cudaMemcpy(combine_Mv_X, host_combine_Mv_X, sizeof(double)*global_num_row*new_num_vec, cudaMemcpyHostToDevice);
        nvtxRangePop();
        delete[] displs;
        delete[] displs2;
        delete[] sizeList;
        delete[] sizeList2;
        nvtxRangePushA("compute");
        status = cusparseDcsrmm(cusparse_handle, CUSPARSE_OPERATION_NON_TRANSPOSE, num_row, new_num_vec, global_num_row, nnz,  &one, descr,
                d_values, d_row_index, d_col_index, combine_Mv_X, global_num_row,  &zero, address_Y[0], length);
        nvtxRangePop();
        
#endif
//        nvtxRangePushA("cpu operation");
//        Teuchos::RCP<Epetra_MultiVector> result  = Teuchos::rcp(new Epetra_MultiVector(View, hamiltonian_matrix->RowMap(), host_Mv_X, length, new_num_vec  ));
//        hamiltonian_matrix->Multiply(false, *tmp_MultiVector, *result);
//        nvtxRangePop();
//        nvtxRangePushA("rearrange");
//
//        double ** result_pointers = result->Pointers();
//        for(int i=0; i<new_num_vec; i++)
//            memcpy(&host_Mv_X[i*length], result_pointers[i], sizeof(double)*length);
//        nvtxRangePop();
//        nvtxRangePushA("HtoD");
//        cudaMemcpy(address_Y[0], host_Mv_X, sizeof(double)*new_num_vec*length, cudaMemcpyHostToDevice);
//        nvtxRangePop();

    }
    else{

        timer->start("Hamiltonian_Matrix::cusparseDcsrmm");
        status = cusparseDcsrmm(cusparse_handle, CUSPARSE_OPERATION_NON_TRANSPOSE, num_row, new_num_vec, global_num_row, nnz,  &one, descr,
                d_values, d_row_index, d_col_index, address_X[0], global_num_row,  &zero, address_Y[0], length);
        cudaDeviceSynchronize();    //jaechang
        timer->end("Hamiltonian_Matrix::cusparseDcsrmm");
        switch (status){
            case CUSPARSE_STATUS_NOT_INITIALIZED:
                std::cout << "CUSPARSE_STATUS_NOT_INITIALIZED" <<std::endl;
                break;
            case CUSPARSE_STATUS_ALLOC_FAILED:
                std::cout << "CUSPARSE_STATUS_ALLOC_FAILED" <<std::endl;
                break;
            case CUSPARSE_STATUS_INVALID_VALUE:
                std::cout << "CUSPARSE_STATUS_INVALID_VALUE" <<std::endl;
                break;
            case CUSPARSE_STATUS_ARCH_MISMATCH:
                std::cout << "CUSPARSE_STATUS_ARCH_MISMATCH" <<std::endl;
                break;
            case CUSPARSE_STATUS_EXECUTION_FAILED:
                std::cout << "CUSPARSE_STATUS_EXECUTION_FAILED" <<std::endl;
                break;
            case CUSPARSE_STATUS_INTERNAL_ERROR:
                std::cout << "CUSPARSE_STATUS_INTERNAL_ERROR" <<std::endl;
                break;
            case CUSPARSE_STATUS_MATRIX_TYPE_NOT_SUPPORTED:
                std::cout << "CUSPARSE_STATUS_MATRIX_TYPE_NOT_SUPPORTED" <<std::endl;
                break;
        }
        if (status != CUSPARSE_STATUS_SUCCESS){
            std::cout << "error on cusparseDcsrmm " << status<<std::endl;
        }
    }

    //    nvtxRangePushA("Remaining");
    if (status != CUSPARSE_STATUS_SUCCESS) {
        std::cout << "Matrix-vector multiplication failed" << std::endl;
        std::cout << status << std::endl;
    }
    nvtxRangePop();
    timer->end("Hamiltonian_Matrix::Apply");
//    timer->print(std::cout ) ;
    return;
}

template<class ScalarType>
void Hamiltonian_Matrix<ScalarType>::set_spin(int spin){
    this->ispin = spin;
}
template<class ScalarType>
Hamiltonian_Matrix<ScalarType>::~Hamiltonian_Matrix(){
    //    std::cout << "Hamiltonian_Matrix_Multiplier, destructor is called" << std::endl;
    hamiltonian_matrix = Teuchos::null;
    //cudaFree(d_local_potential);
    if(hamiltonian_matrix_in_gpu){
        cudaFree(d_row_index);
        cudaFree(d_col_index);
        cudaFree(d_values);
        cusparseDestroy(cusparse_handle);
    }
    if(is_cuMallocHost){
        cudaFreeHost(host_Mv_X);
        cudaFreeHost(host_combine_Mv_X);
    cudaFree(combine_Mv_X);
    }
    return;
}
//template<>
//int Epetra_Hamiltonian_Matrix_Multiplier::Apply(const Epetra_MultiVector X, Epetra_MultiVector& Y) const;

//template<>
//int Hamiltonian_Matrix::Apply(const Anasazi::MultiVec<double>& X, Anasazi::MultiVec<double>& Y) const;
}
#endif
