#pragma once 
#ifdef USE_CUDA
#include "AnasaziConfigDefs.hpp"
#include "AnasaziMultiVec.hpp"
#include "cublas_v2.h"
//! This namespace contain MultiVector working with CUDA 
/*! 
 *
 * \author Sunghwan Choi and Jaechang Lim
 *
 * \date Last modified on 01-Nov-05
 */

namespace CUDA{


//cudaStream_t* Global_stream;
//int NStream=32;
extern cublasHandle_t Global_handle;
template <class ScalarType>
class MultiVec
//class MultiVec : public CUDA::MultiVec<ScalarType>
{
    public:

        //! Constructor for a \c numVecs vectors of length \c Length.
        MultiVec(const int _length, const int _numVecs, bool gpu_enable=true);

        // constructor for non-owner object
        MultiVec(const int _length, std::vector<ScalarType*> pointer, bool gpu_enable=true) ;
        MultiVec(const int _length, const int _numVecs, ScalarType** pointer, bool gpu_enable=true) ;

        //! Copy constructor, performs a deep copy.
        MultiVec(const MultiVec& rhs, bool gpu_enable = true);
        //! Constructor to copy data in device pointer
        MultiVec(const int _length, const int _numVecs, ScalarType* new_d_data);
        MultiVec(const int _length, const int _numVecs, std::vector<ScalarType*> new_d_data,std::vector<ScalarType*> pointer, bool gpu_enable=true ) ;

        //! Destructor
        ~MultiVec();

        //get address of data in gpu
        void get_address(std::vector<ScalarType*>& address) const;
        void set_address(std::vector<ScalarType*> address);

        const ScalarType* view(const int iVec) const{ return 0; };
        //! Returns a clone of the current vector.
        MultiVec* Clone(const int numVecs) const;

        //! Returns a view of current vector (shallow copy)
        MultiVec* CloneViewNonConst(const std::vector< int > &index) ;

        //! Returns a view of current vector (shallow copy), const version.
        const MultiVec* CloneView(const std::vector< int > &index) const;

        int GetVecLength () const;
        int GetNumberVecs () const;
        
//        int GetGlobalLength () const { return GetVecLength(); };
        ptrdiff_t GetGlobalLength () const { return GetVecLength(); };

        // Update *this with alpha * A * B + beta * (*this). 
        void MvTimesMatAddMv (ScalarType alpha, const CUDA::MultiVec<ScalarType> &A, 
                const Teuchos::SerialDenseMatrix<int, ScalarType> &B, 
                ScalarType beta);

        // Replace *this with alpha * A + beta * B. 
        void MvAddMv (ScalarType alpha, const CUDA::MultiVec<ScalarType>& A, 
                ScalarType beta,  const CUDA::MultiVec<ScalarType>& B);

        // Compute a dense matrix B through the matrix-matrix multiply alpha * A^H * (*this). 
        void MvTransMv (ScalarType alpha, const CUDA::MultiVec<ScalarType>& A, 
                Teuchos::SerialDenseMatrix< int, ScalarType >& B
#ifdef HAVE_ANASAZI_EXPERIMENTAL
                , CUDA::ConjType conj
#endif
                ) const;


        // Compute a vector b where the components are the individual dot-products, i.e.b[i] = A[i]^H*this[i] where A[i] is the i-th column of A. 
        void MvDot (const CUDA::MultiVec<ScalarType>& A, std::vector<ScalarType> &b
#ifdef HAVE_ANASAZI_EXPERIMENTAL
                , CUDA::ConjType conj
#endif
                ) const{};

        void MvNorm ( std::vector<typename Teuchos::ScalarTraits<ScalarType>::magnitudeType> &normvec ) const ;

        // Copy the vectors in A to a set of vectors in *this. The numvecs vectors in 
        // A are copied to a subset of vectors in *this indicated by the indices given 
        // in index.
        // FIXME: not so clear what the size of A and index.size() are...
        //
        void set(const int iVec, const int size, const int* indices, const ScalarType* values) {return ; }; 

        void SetBlock (const CUDA::MultiVec<ScalarType>& A, 
                const std::vector<int> &index);

        // Scale the vectors by alpha
        void MvScale( ScalarType alpha ){};

        // Scale the i-th vector by alpha[i]
        void MvScale( const std::vector<ScalarType>& alpha );

        // Fill the vectors in *this with random numbers.
        void  MvRandom ();

        // Replace each element of the vectors in *this with alpha.
        void  MvInit (ScalarType alpha);
        // Returns a clone of the corrent multi-vector.
        MultiVec* CloneCopy() const { return 0; };

        //! Returns a clone copy of specified vectors.
        MultiVec* CloneCopy(const std::vector< int > &index) const;

        void MvPrint (std::ostream &os) const;

        //void Allocate_GPU_Memory();
        
        inline ScalarType& operator()(const int i, const int j);
        /*
        {
            std::cout << "This function is not complete" << std::endl;
            exit(-1);   
        }; */

        inline const ScalarType& operator()(const int i, const int j) const;

        ScalarType* operator[](int v);

        ScalarType* operator[](int v) const;
        
        //data transfer from gpu to cpu
        void device_to_host();
        
        //data transfer from host to gpu
        void host_to_device();
        
        void copy_to_continuous_d_data() ;
        void const_copy_to_continuous_d_data() const;
        void copy_to_d_data() ;
        void const_copy_to_d_data() const;
        
        bool get_is_data_in_gpu();
        bool check_ownership() const;
    private:
        void check();
        
        //! Length of the vectors
        const int length;
        //! Number of multi-vectors
        const int numVecs;
        //! Pointers to the storage of the vectors.
        std::vector<ScalarType*> data_;
        std::vector<ScalarType*> d_data_;
        ScalarType* continuous_d_data;
        bool is_data_in_gpu = false;
        bool gpu_enable = true;
        bool is_continuous_data_allocated = false;
        mutable bool is_continuous_data_copied = false;


        //! If \c true, then this object owns the vectors and must free them in dtor.

        std::vector<bool> ownership_;
        //ScalarType** values;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////below part is not used for diagonalization process of gpu//////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*

*/

};
template class MultiVec<double>;
};
namespace Anasazi{
  /// \brief Specialization of MultiVecTraits for Belos::MultiVec.
  ///
  /// Anasazi interfaces to every multivector implementation through a
  /// specialization of MultiVecTraits.  Thus, we provide a
  /// specialization of MultiVecTraits for the MultiVec run-time
  /// polymorphic interface above.
  ///
  /// \tparam ScalarType The type of entries in the multivector; the
  ///   template parameter of MultiVec.
  template<class ScalarType>
  class MultiVecTraits<ScalarType,CUDA::MultiVec<ScalarType> > {
  public:
    //! @name Creation methods
    //@{ 

    /// \brief Create a new empty \c MultiVec containing \c numvecs columns.
    /// \return Reference-counted pointer to the new \c MultiVec.
    static Teuchos::RCP<CUDA::MultiVec<ScalarType> > 
    Clone (const CUDA::MultiVec<ScalarType>& mv, const int numvecs) {
      return Teuchos::rcp (const_cast<CUDA::MultiVec<ScalarType>&> (mv).Clone (numvecs)); 
    }

    /*! \brief Creates a new \c CUDA::MultiVec and copies contents of \c mv into the new vector (deep copy).
      
      \return Reference-counted pointer to the new \c CUDA::MultiVec.
    */
    static Teuchos::RCP<CUDA::MultiVec<ScalarType> > CloneCopy( const CUDA::MultiVec<ScalarType>& mv )
    { return Teuchos::rcp( const_cast<CUDA::MultiVec<ScalarType>&>(mv).CloneCopy() ); }

    /*! \brief Creates a new \c CUDA::MultiVec and copies the selected contents of \c mv into the new vector (deep copy).  

      The copied vectors from \c mv are indicated by the \c index.size() indices in \c index.      
      \return Reference-counted pointer to the new \c CUDA::MultiVec.
    */
    static Teuchos::RCP<CUDA::MultiVec<ScalarType> > CloneCopy( const CUDA::MultiVec<ScalarType>& mv, const std::vector<int>& index )
    { return Teuchos::rcp( const_cast<CUDA::MultiVec<ScalarType>&>(mv).CloneCopy(index) ); }

    /*! \brief Creates a new \c CUDA::MultiVec that shares the selected contents of \c mv (shallow copy).

    The index of the \c numvecs vectors shallow copied from \c mv are indicated by the indices given in \c index.
    \return Reference-counted pointer to the new \c CUDA::MultiVec.
    */    
    static Teuchos::RCP<CUDA::MultiVec<ScalarType> > CloneViewNonConst( CUDA::MultiVec<ScalarType>& mv, const std::vector<int>& index )
    { return Teuchos::rcp( mv.CloneViewNonConst(index) ); }

    /*! \brief Creates a new const \c CUDA::MultiVec that shares the selected contents of \c mv (shallow copy).

    The index of the \c numvecs vectors shallow copied from \c mv are indicated by the indices given in \c index.
    \return Reference-counted pointer to the new const \c CUDA::MultiVec.
    */      
    static Teuchos::RCP<const CUDA::MultiVec<ScalarType> > CloneView( const CUDA::MultiVec<ScalarType>& mv, const std::vector<int>& index )
    { return Teuchos::rcp( const_cast<CUDA::MultiVec<ScalarType>&>(mv).CloneView(index) ); }

    //@}

    //! @name Attribute methods
    //@{ 

    //! Obtain the vector length of \c mv.
    static ptrdiff_t GetGlobalLength( const CUDA::MultiVec<ScalarType>& mv )
    { return mv.GetGlobalLength(); }

    //! Obtain the number of vectors in \c mv
    static int GetNumberVecs( const CUDA::MultiVec<ScalarType>& mv )
    { return mv.GetNumberVecs(); }

    //@}

    //! @name Update methods
    //@{ 

    /*! \brief Update \c mv with \f$ \alpha AB + \beta mv \f$.
     */
    static void MvTimesMatAddMv( ScalarType alpha, const CUDA::MultiVec<ScalarType>& A, 
                 const Teuchos::SerialDenseMatrix<int,ScalarType>& B, 
                 ScalarType beta, CUDA::MultiVec<ScalarType>& mv )
    { mv.MvTimesMatAddMv(alpha, A, B, beta); }

    /*! \brief Replace \c mv with \f$\alpha A + \beta B\f$.
     */
    static void MvAddMv( ScalarType alpha, const CUDA::MultiVec<ScalarType>& A, ScalarType beta, const CUDA::MultiVec<ScalarType>& B, CUDA::MultiVec<ScalarType>& mv )
    { mv.MvAddMv(alpha, A, beta, B); }

    /*! \brief Compute a dense matrix \c B through the matrix-matrix multiply \f$ \alpha A^Tmv \f$.
     */
    static void MvTransMv( ScalarType alpha, const CUDA::MultiVec<ScalarType>& A, const CUDA::MultiVec<ScalarType>& mv, Teuchos::SerialDenseMatrix<int,ScalarType>& B
#ifdef HAVE_ANASAZI_EXPERIMENTAL
               , ConjType conj = CUDA::CONJ
#endif
               )
    { mv.MvTransMv(alpha, A, B
#ifdef HAVE_ANASAZI_EXPERIMENTAL
           , conj
#endif
           ); }
    
    /*! \brief Compute a vector \c b where the components are the individual dot-products of the \c i-th columns of \c A and \c mv, i.e.\f$b[i] = A[i]^H mv[i]\f$.
     */
    static void MvDot( const CUDA::MultiVec<ScalarType>& mv, const CUDA::MultiVec<ScalarType>& A, std::vector<ScalarType> & b
#ifdef HAVE_ANASAZI_EXPERIMENTAL
               , ConjType conj = CUDA::CONJ
#endif
               )
    { mv.MvDot( A, b
#ifdef HAVE_ANASAZI_EXPERIMENTAL
        , conj
#endif
        ); }

    //! Scale each element of the vectors in \c *this with \c alpha.
    static void MvScale ( CUDA::MultiVec<ScalarType>& mv, ScalarType alpha )
    { mv.MvScale( alpha ); }
    
    //! Scale each element of the \c i-th vector in \c *this with \c alpha[i].
    static void MvScale ( CUDA::MultiVec<ScalarType>& mv, const std::vector<ScalarType>& alpha )
    { mv.MvScale( alpha ); }
    
    //@}
    //! @name Norm method
    //@{ 

    /*! \brief Compute the 2-norm of each individual vector of \c mv.  
      Upon return, \c normvec[i] holds the value of \f$||mv_i||_2\f$, the \c i-th column of \c mv.
    */
    static void MvNorm( const CUDA::MultiVec<ScalarType>& mv, std::vector<typename Teuchos::ScalarTraits<ScalarType>::magnitudeType> & normvec )
    { mv.MvNorm(normvec); }

    //@}
    //! @name Initialization methods
    //@{ 
    /*! \brief Copy the vectors in \c A to a set of vectors in \c mv indicated by the indices given in \c index.

    The \c numvecs vectors in \c A are copied to a subset of vectors in \c mv indicated by the indices given in \c index,
    i.e.<tt> mv[index[i]] = A[i]</tt>.
    */
    static void SetBlock( const CUDA::MultiVec<ScalarType>& A, const std::vector<int>& index, CUDA::MultiVec<ScalarType>& mv )
    { mv.SetBlock(A, index); }

    /*! \brief Replace the vectors in \c mv with random vectors.
     */
    static void MvRandom( CUDA::MultiVec<ScalarType>& mv )
    { mv.MvRandom(); }

    /*! \brief Replace each element of the vectors in \c mv with \c alpha.
     */
    static void MvInit( CUDA::MultiVec<ScalarType>& mv, ScalarType alpha = Teuchos::ScalarTraits<ScalarType>::zero() )
    { mv.MvInit(alpha); }

    //@}
    //! @name Print method
    //@{ 

    //! Print the \c mv multi-vector to the \c os output stream.
    static void MvPrint( const CUDA::MultiVec<ScalarType>& mv, std::ostream& os )
    { mv.MvPrint(os); }

    //@}

  };
};

#endif
