#pragma once
#include "Teuchos_RCP.hpp"
#include "Teuchos_ParameterList.hpp"
#include "PCMSolver.hpp"

namespace Create_Solvation{
    Teuchos::RCP<PCMSolver> Create_Solvation(Teuchos::RCP<const Basis> basis, Teuchos::RCP<Teuchos::ParameterList> parameters);
#ifdef ACE_PCMSOLVER
    PCMInput initialize_pcmsolver(Teuchos::RCP<Teuchos::ParameterList> parameters);
#endif
}
