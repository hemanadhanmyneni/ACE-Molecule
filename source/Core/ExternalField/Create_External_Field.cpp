#include "Create_External_Field.hpp"
#include <array>
#include <vector>

#include "External_Field_Analytic.hpp"
#include "External_Field_From_Input.hpp"
#include "External_Field_Cube.hpp"

#include "../../Utility/Verbose.hpp"
#include "../../Utility/Parallel_Manager.hpp"
#include "../../Utility/Parallel_Util.hpp"
#include "../../Utility/String_Util.hpp"
#include "../../Utility/Read/Read_Cube.hpp"
#include "../../Basis/Basis.hpp"

using Teuchos::rcp;
using Teuchos::Array;
using Teuchos::RCP;
using Teuchos::ParameterList;
using std::vector;
using std::string;

RCP<External_Field> Create_External_Field::Create_External_Field(RCP<const Basis> basis, RCP<ParameterList> parameters){
    if(!parameters->isSublist("ExternalField") ){
        return Teuchos::null;
    }
    RCP<ParameterList> ext_params = Teuchos::sublist(parameters, "ExternalField");
    RCP<External_Field> retval;

    if(parameters -> sublist("ExternalField").get<string>("InputType") == "Analytic"){
        string field =parameters->sublist("ExternalField").get<string>("ExternalField", "Electric");
        string type =parameters->sublist("ExternalField").get<string>("ExternalFieldType", "Static");
        string field_direction =parameters->sublist("ExternalField").get<string>("ExternalFieldDirection");
        double field_strength = parameters->sublist("ExternalField").get<double>("ExternalFieldStrength");
        retval = rcp(new External_Field_Analytic(basis,field,type,field_direction,field_strength )  );
    }
    else if(parameters -> sublist("ExternalField").get<string>("InputType") == "Read"){
        string filename = parameters -> sublist("ExternalField").get<string>("PotentialFilename");
        std::array<int,3> npts;
        std::array<std::array<double,3>, 3> lattice_vec;
        std::array<double,3> center;

        string interp = ext_params -> get<string>("Interpolation", "cubic");
        int interpolation = (interp == "linear")? 0: (interp == "cubic")? 1: -1;
        npts[0]   = ext_params -> get<int>("PointX");
        npts[1]   = ext_params -> get<int>("PointY");
        npts[2]   = ext_params -> get<int>("PointZ");
        center[0] = ext_params -> get<double>("PotentialOffSetX", 0.0);
        center[1] = ext_params -> get<double>("PotentialOffSetY", 0.0);
        center[2] = ext_params -> get<double>("PotentialOffSetZ", 0.0);
        vector<string> vecX = String_Util::Split_ws(ext_params -> get<string>("LatticeVecX"));
        vector<string> vecY = String_Util::Split_ws(ext_params -> get<string>("LatticeVecY"));
        vector<string> vecZ = String_Util::Split_ws(ext_params -> get<string>("LatticeVecZ"));
        for(int d = 0; d < 3; ++d){
            lattice_vec[0][d] = stod(vecX[d]);
            lattice_vec[1][d] = stod(vecY[d]);
            lattice_vec[2][d] = stod(vecZ[d]);
        }

        retval = rcp(new External_Field_From_Input(basis, filename, npts, lattice_vec, center, interpolation));
    }
    else if(parameters -> sublist("ExternalField").get<string>("InputType") == "Cube"){
        string filename = parameters -> sublist("ExternalField").get<string>("PotentialFilename");
        double scaling_factor = parameters -> sublist("ExternalField").get<double>("ScalingFactor",1.0);
        string interp = ext_params -> get<string>("Interpolation", "cubic");
        int interpolation = (interp == "linear")? 0: (interp == "cubic")? 1: -1;

        retval = rcp(new External_Field_Cube(basis, filename, interpolation, scaling_factor));
    }
    Verbose::set_numformat(Verbose::Pos);
    Verbose::single(Verbose::Simple) << *retval << std::endl;
    Verbose::set_numformat(Verbose::Default);
    return Teuchos::null;
}
