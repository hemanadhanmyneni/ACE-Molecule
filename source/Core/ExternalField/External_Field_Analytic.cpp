#include "External_Field_Analytic.hpp"
#include "../../Utility/Parallel_Manager.hpp"
#include "../../Utility/Parallel_Util.hpp"
#include "../../Utility/Verbose.hpp"

using Teuchos::rcp;
using Teuchos::Array;
using Teuchos::RCP;
using Teuchos::ParameterList;
using std::string;

External_Field_Analytic::External_Field_Analytic(RCP<const Basis> basis,string field, string type, string field_direction,double field_strength): External_Field(std::string("analytic"),basis){
    this->field=field;
    this->type=type;
    this->field_direction=field_direction;
    this->field_strength=field_strength;

    if(field == "Electric"){
        if(type != "Static"){
            Verbose::all() << "Wrong external field type - only static field is supported" << std::endl;
            exit(EXIT_FAILURE);
        }

        if(field_direction != "x" && field_direction != "y" && field_direction != "z"){
            Verbose::all() << "Wrong external field direction - choose x, y, or z" << std::endl;
            exit(EXIT_FAILURE);
        }
        calculate_potential();
        have_potential = true;
    }
    else if (field ==""){
        have_potential = false;
    }
    else{
        Verbose::all() << "Wrong external field - only electric field is supported" << std::endl;
        exit(EXIT_FAILURE);
    }
}

std::ostream& External_Field_Analytic::print(std::ostream &c) const{
    c << "=================================== ExternalField ==" << std::endl;
    c << " ExternalField: " << this -> type <<std::endl;
    c << " Field type: " << this -> field << ", " << this -> type << std::endl;
    c << " Field information: " << this -> field_strength << " x " << this -> field_direction << std::endl;
    c << "====================================================" << std::endl;
    return c;
}

void External_Field_Analytic::calculate_potential(){
    std::array<double,3> electric_field;
    int size = basis->get_map()->NumMyElements();
    int* MyGlobalElements = basis->get_map()->MyGlobalElements();
    potential = rcp( new Epetra_Vector( *basis->get_map() )) ;

    if(type == "Static"){
        // set electric field vector
        if(field_direction == "x"){
            electric_field[0] = field_strength;
        }else if(field_direction == "y"){
            electric_field[1] = field_strength;
        }else if(field_direction == "z"){
            electric_field[2] = field_strength;
        }
        // end

        int i_x=0,i_y=0,i_z=0;
        for(int i=0;i<size;i++){
            std::array<double,3> position;
            basis->get_position(MyGlobalElements[i],position[0],position[1],position[2]);
            double tmp = 0.0;
            for(int j=0;j<3;j++){
                tmp += electric_field[j] * position[j];
            }
            potential->ReplaceGlobalValue(MyGlobalElements[i],0, tmp);
        }
    }

    return;
}

/*
double External_Field::numeric_integration(int j,int index,double* electric_field,double min,double max){
    double return_val=0.0;
    const int* points= basis->get_points();
    double scaling = (max-min)/(num_integrate_point-1);
    double point=min;
    for (int i=0;i <num_integrate_point ;i++){
        return_val+=  basis->compute_1d_basis(j,point,index)* basis->compute_1d_basis(j,point,index)*electric_field[index]*scaling ;
        point += scaling;
    }

    return return_val;
}
*/
