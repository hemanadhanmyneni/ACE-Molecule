#include  "Teuchos_RCP.hpp"
#include "Teuchos_ParameterList.hpp"
#include "External_Field.hpp"
#include "../../Basis/Basis.hpp"

namespace Create_External_Field{
    Teuchos::RCP<External_Field> Create_External_Field(Teuchos::RCP<const Basis> basis,  Teuchos::RCP<Teuchos::ParameterList> parameters);
}
