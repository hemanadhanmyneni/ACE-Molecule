#include "External_Field.hpp"
#include <iostream>
#include "../../Utility/Parallel_Manager.hpp"
#include "../../Utility/Parallel_Util.hpp"
#include "../../Utility/Value_Coef.hpp"
#include "../../Utility/Interpolation/Trilinear_Interpolation.hpp"
#include "../../Utility/Calculus/Lagrange_Derivatives.hpp"

using Teuchos::rcp;
using Teuchos::Array;
using Teuchos::RCP;
using Teuchos::ParameterList;
using std::string;
using std::ostringstream;
using std::ostream;

std::ostream& operator <<(std::ostream &c, const External_Field& external_field){
    return external_field.print(c);
}

std::ostream& operator <<(std::ostream &c, const External_Field* pexternal_field){
    c << *pexternal_field ;
    return c;
}

std::ostream& External_Field::print(std::ostream &c) const {
    c << "External_Field::" << this -> type <<std::endl;
    return c;
}

External_Field::External_Field(std::string type, Teuchos::RCP<const Basis> basis){
    this->type = type;
    this -> basis = basis;
}

bool External_Field::is_field_applied(){
  return have_potential;
}
int External_Field::get_potential(Array< RCP<Epetra_Vector> >& diagonal){
    for(int alpha = 0; alpha < diagonal.size(); alpha++){
        diagonal[alpha]->Update(1.0,*potential,1.0);
    }
    return 0;
}

std::vector<double> External_Field::get_force(RCP<const State> state){
    auto atoms = state->get_atoms();
    int index =0;
    std::vector<double> return_val(3*atoms->get_size(),0.0);

    int occupation_size= state->occupations.size();
    Array < RCP<Epetra_Vector> > grad_potential;
    for (int axis = 0; axis<3; axis++){
        grad_potential.append(Teuchos::rcp( new Epetra_Vector(*basis->get_map()) ) );
    }

    Verbose::single(Verbose::Detail) << "grad_potential::" <<grad_potential.size() << std::endl;
    Lagrange_Derivatives::gradient(basis, potential, grad_potential , true );

    const int total_size = basis->get_map()->NumGlobalElements();
    double* grad_potential_all [3];

    for (int axis = 0; axis<3; axis++){
        grad_potential_all[axis] = new double[total_size]();
        Parallel_Util::group_allgather_vector( grad_potential[axis], grad_potential_all[axis]);

        for (int i =0; i<atoms->get_size(); i++){
            index= i*3+axis;

            auto position = atoms->operator[](i).get_position();
            double grad_potential_at_atom_i = Interpolation::Trilinear::calculate( basis, grad_potential_all[axis], position[0], position[1], position[2]);;

            return_val[index] += atoms->operator[](i).get_valence_charge() * grad_potential_at_atom_i;
        }
    }


    // correct double counting problem!
//    for (int i =0; i<return_val.size(); i++){
//        return_val[i]/=occupation_size;
//    }
    return return_val;
}

double External_Field::get_energy(RCP<const State> state,int alpha){
    if(this -> have_potential==false){
        return 0.0;
    }

    if(alpha>=state->occupations.size()){
        std::cout << "something wrong" <<std::endl;
        exit(-1);
    }

    auto atoms = state->get_atoms();

    // check valence charge
    int i =0;
    for (i=0; i<atoms->get_size(); i++){
        // wrong!
        if(atoms->operator[](i).get_valence_charge()<=0){
            break;
        }
    }
    if(i<atoms->get_size()){
        Verbose::single() << "valence charge is not set yet" <<std::endl;
        return 0.0;
    }

    const int my_size = basis->get_map()->NumMyElements();
    const int total_size = basis->get_map()->NumGlobalElements();
    const int orbital_size = state->occupations[alpha]->get_size();

    double energy = 0.0;
    double tmp_energy = 0.0;
    double return_val=0.0;

    Teuchos::RCP<Epetra_MultiVector> values;
    // values contain orbital values
    Value_Coef::Value_Coef(basis, state->orbitals[alpha], false, true, values );
    // now values contains density values
    values->Multiply(1.0, *values, *values, 0.0);

    //double * norm = new double[values->NumVectors()];
    //values->Norm1(norm);

    // calculate energy
    for(int i=0;i<orbital_size;++i){
        if(state -> occupations[alpha] -> operator[](i) > 0.0 ){
            values->operator()(i)->Scale(state->occupations[alpha]->operator[](i));
            values->operator()(i)->Dot(*potential, &tmp_energy);
            energy+=tmp_energy;
        }
    }
    // scaling! (grid volume)
    energy *= basis->get_scaling()[0] * basis->get_scaling()[1] * basis->get_scaling()[2];
    // make energy value be negative ()
    // return_val *=-1;
    double *srcval = new double [total_size]();
    double *tmpval = new double [total_size]();
    auto MyGlobalElements = potential->Map().MyGlobalElements();
    #ifdef ACE_HAVE_OMP
    #pragma omp parallel for
    #endif
    for(int j=0; j<my_size; j++){
        tmpval[MyGlobalElements[j]] = potential->operator[](j);
    }
    Parallel_Util::group_sum(tmpval, srcval, total_size);
    std::array<double,3> position;
    double potential_at_atom_i;

    //auto interpolate = [position, potential_at_atom_i, basis, tmpval](double x, double y, double z){ }
    for (int i=0; i<atoms->get_size(); i++){
        position = atoms->operator[](i).get_position();
        potential_at_atom_i = Interpolation::Trilinear::calculate( basis, srcval, position[0], position[1], position[2]);

        // add interaction term with atomic charge
        energy-=potential_at_atom_i*atoms->operator[](i).get_valence_charge();
    }
    return energy;
}
