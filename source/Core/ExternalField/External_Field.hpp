#pragma once
#include <string>
#include "../../Basis/Basis.hpp"
#include "../../State/State.hpp"

#include "Teuchos_ParameterList.hpp"
#include "Teuchos_RCP.hpp"
#include "Teuchos_Array.hpp"
#include "Epetra_Vector.h"

/**
 * @brief External_Feild class is abstract class that takes charge of external electric field 
 */
class External_Field{
  friend std::ostream& operator <<(std::ostream &c, const External_Field& external_field);
  friend std::ostream& operator <<(std::ostream &c, const External_Field* pexternal_field);
  public:
//    External_Field(RCP<const Basis> basis,RCP<ParameterList> parameters);
    /**
     * @brief External_Field 
     * @param [in] type Type of external field. Now, it can be "Static" only 
     * @param [in] basis basis object
     */
    External_Field(
        std::string type,
        Teuchos::RCP<const Basis> basis
    );

    /**
     * @brief potential generation function
     * @param [out] diagonal, which contains potential values on every grid points. Teuchos::Array is related to spin index
     * @return error code [not activated yet]
     */
    int get_potential(Teuchos::Array< Teuchos::RCP<Epetra_Vector> >& diagonal);
    std::vector<double> get_force(Teuchos::RCP<const State> states );
    ///

    /**
     * @brief calculate interaction energy cuased by external field
     * @param[in] state  basis Basis.
     * @param[in] alpha spin index.
     * @return    energy due to field
     */
    double get_energy(Teuchos::RCP<const State> states,int alpha);
    bool is_field_applied();
    std::string type;

    virtual std::ostream &print(std::ostream& c) const;
  protected:
    Teuchos::RCP<const Basis> basis;
    //Teuchos::RCP<Teuchos::ParameterList> parameters;

    virtual void calculate_potential() = 0;
    Teuchos::RCP<Epetra_Vector> potential;
    bool have_potential = false;
};
