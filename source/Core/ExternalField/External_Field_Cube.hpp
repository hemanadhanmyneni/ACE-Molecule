#pragma once
#include <string>
#include <array>
#include "../../Basis/Basis.hpp"
#include "External_Field.hpp"

#include "Teuchos_RCP.hpp"
#include "Teuchos_SerialDenseMatrix.hpp"
#include "Epetra_Vector.h"


/**
 * @author Sunghwan Choi
 * @date 18/08/24
 * @brief Read external field potential from cube file.
 * @note Still Unstable!
 **/
class External_Field_Cube: public External_Field{
  public:
    External_Field_Cube(
        Teuchos::RCP<const Basis> basis,
        std::string pot_filename,
        int interpolation,
        double scaling_factor
    );

    virtual std::ostream &print(std::ostream& c) const;
  protected:
    //Teuchos::RCP<Teuchos::ParameterList> parameters;
    void calculate_potential();

    std::string cube_filename;
    int interpolation_scheme;
    double scaling_factor;

};
