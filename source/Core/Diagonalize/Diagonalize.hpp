#pragma once
#include "Teuchos_ParameterList.hpp"
#include "Epetra_CrsMatrix.h"
#include "Teuchos_RCP.hpp"
//#include "Teuchos_RCPDecl.hpp"
#include "AnasaziTypes.hpp"
#include "Epetra_MultiVector.h"
#include "../../Utility/Verbose.hpp"
#include "../../Compute/Core_Hamiltonian_Matrix.hpp"

class Diagonalize{
    public:
        /**
         * @brief Virtual destructor for abstract class.
         **/
        virtual ~Diagonalize(){};
        virtual bool diagonalize(Teuchos::RCP<Epetra_CrsMatrix> matrix, int numev, Teuchos::RCP<Epetra_MultiVector> initial_eigenvector=Teuchos::null, Teuchos::RCP<Epetra_CrsMatrix> overlap_matrix = Teuchos::null)=0;

        virtual bool diagonalize(
            Teuchos::RCP<Core_Hamiltonian_Matrix> core_hamiltonian_matrix, int ispin, int numev, Teuchos::RCP<Epetra_Vector> local_potential,
            Teuchos::RCP<Epetra_MultiVector> initial_eigenvector = Teuchos::null,
            Teuchos::RCP<Epetra_CrsMatrix> overlap_matrix = Teuchos::null
        ){return false;};
        virtual Teuchos::RCP<Epetra_MultiVector> get_eigenvectors()=0;
        virtual std::vector<double > get_eigenvalues()=0;
        //virtual int get_max_iter()=0;
        //virtual void set_max_iter(int iterno)=0;
        Anasazi::ReturnType returnCode;

        int get_max_iter(){ return this->max_iter;}
        void set_max_iter(int iterno){this->max_iter = iterno;};
        double get_tol(){return this -> tolerance;}
        void set_tol(double tol ){ this -> tolerance = tol; }
    protected:
        int max_iter;
        double tolerance;
        Teuchos::RCP<Teuchos::ParameterList> parameters;
        Verbose::Tag verb_level=Verbose::Simple;
        Teuchos::RCP<Time_Measure> timer = Teuchos::rcp(new Time_Measure() );
        void MergeEigenpairs( std::vector<double>& eigenvalues, Teuchos::RCP<Epetra_MultiVector>& eigenvectors, double merge_tol);
};
