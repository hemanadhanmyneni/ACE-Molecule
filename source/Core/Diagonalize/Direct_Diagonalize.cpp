#include "Direct_Diagonalize.hpp"
#include <vector>
#include <cmath>
#include "Teuchos_SerialDenseMatrix.hpp"
#include "Teuchos_LAPACK.hpp"
#include "../../Utility/Parallel_Manager.hpp"
#include "../../Utility/Parallel_Util.hpp"
#include "../../Utility/Verbose.hpp"
#include "Epetra_LocalMap.h"
#define _COMPLEX_CUTOFF_ (1.0E-10)

using std::vector;

Direct_Diagonalize::Direct_Diagonalize(bool is_sym_real_only/* = true*/){
    this -> is_sym_real = is_sym_real_only;
    //this -> is_positive_overlap = is_positive_overlap;
}

Direct_Diagonalize::~Direct_Diagonalize(){
}

bool Direct_Diagonalize::diagonalize(Teuchos::RCP<Epetra_CrsMatrix> input, int numev, Teuchos::RCP<Epetra_MultiVector> initial_eigenvector, Teuchos::RCP<Epetra_CrsMatrix> overlap_matrix){
    timer -> start("Time to diagonalize");

    bool is_GEV = (overlap_matrix == Teuchos::null)? false: true;
    const int num_proc = Parallel_Manager::info().get_mpi_size();
    const int matrix_dimension = input->NumGlobalRows();
    const int total_nnz = input->NumGlobalNonzeros();
    Teuchos::SerialDenseMatrix<int,double> matrixA(matrix_dimension,matrix_dimension);
    Parallel_Util::matrix_serialize(input,matrixA);
    Teuchos::SerialDenseMatrix<int,double> matrixB(matrix_dimension,matrix_dimension);
    if(overlap_matrix != Teuchos::null){
        Parallel_Util::matrix_serialize(overlap_matrix,matrixB);
    }
    if(numev > matrix_dimension) numev = matrix_dimension;

    double* eigen_ = new double [matrix_dimension];
    double* eigen_comp;
    double* beta;
    Teuchos::SerialDenseMatrix<int,double> VR;
    if(!this -> is_sym_real){
        eigen_comp = new double [matrix_dimension];
        VR = Teuchos::SerialDenseMatrix<int,double>(matrix_dimension,matrix_dimension);
        if(is_GEV){
            beta = new double [matrix_dimension];
        }
    }

    if(Parallel_Manager::info().get_total_mpi_rank()==0){
        /******* move to Utility/Parallel_Util
        int start_ =0;
        *************
                matrix[matrix_dimension-1][tar_col_indices[j] ] = tar_values[j];
        */
        Teuchos::LAPACK<int,double> lapack;
        double* WORK = new double [10*matrix_dimension];
        int info;
        if(is_GEV){
            if(this -> is_sym_real){
                Verbose::single(Verbose::Detail) << "Direct_Diagonalize::solve: solving symmetric real matrix A, positive definite B, general eigenvalue problem, Ax=lBx. (SYGV)" << std::endl;
                //SYGV (const OrdinalType &itype, const char &JOBZ, const char &UPLO, const OrdinalType &n, ScalarType *A, const OrdinalType &lda, ScalarType *B, const OrdinalType &ldb, ScalarType *W, ScalarType *WORK, const OrdinalType &lwork, OrdinalType *info) const
                //input
                //  itype = 1 : Ax=lBx, 2: ABx=lx, 3: BAx=lx
                //  jobz = 'V', eigenvalue + eigenvector
                //  uplo = 'U' : upper triangular part
                //  n : matrix dimension
                //  ap : a matrix
                //  lda : the leading dimension of a
                //  bp : b matrix
                //  ldb : the leading dimension of b
                //output
                //  ap : eigenvectors
                //      Normalization
                //         if itype = 1 or 2, Z^T*B*Z=I
                //         if itype = 3, Z^T*inv(B)*Z=I
                //  bp : the triangular factor U or L from the Cholesky factorization
                //  w : eigenvalues (if success)
                //others
                //  work : buffer
                //  lwork : length of work
                //  info : error code. info==0 : success
                lapack.SYGV(1,'V','U',matrix_dimension, matrixA.values(), matrixA.stride(), matrixB.values(), matrixB.stride(), eigen_, WORK, 10*matrix_dimension, &info);
            }
            else{
                Verbose::single(Verbose::Detail) << "Direct_Diagonalize::solve: solving real nonsymmetric matrices (A,B), general eigenvalue problem, Ax=lBx. (GGEV)" << std::endl;
                Teuchos::SerialDenseMatrix<int,double> VL = Teuchos::SerialDenseMatrix<int,double>(matrix_dimension,matrix_dimension);
                //GGEV (const char &JOBVL, const char &JOBVR, const OrdinalType &n, ScalarType *A, const OrdinalType &lda, ScalarType *B, const OrdinalType &ldb, MagnitudeType *ALPHAR, MagnitudeType *ALPHAI, ScalarType *BETA, ScalarType *VL, const OrdinalType &ldvl, ScalarType *VR, const OrdinalType &ldvr, ScalarType *WORK, const OrdinalType &lwork, OrdinalType *info) const
                /* *  Purpose
                *  *  =======
                *  *
                *  *  SGGEV computes for a pair of N-by-N real nonsymmetric matrices (A,B)
                *  *  the generalized eigenvalues, and optionally, the left and/or right
                *  *  generalized eigenvectors.
                *  *
                *  *  A generalized eigenvalue for a pair of matrices (A,B) is a scalar
                *  *  lambda or a ratio alpha/beta = lambda, such that A - lambda*B is
                *  *  singular. It is usually represented as the pair (alpha,beta), as
                *  *  there is a reasonable interpretation for beta=0, and even for both
                *  *  being zero.
                *  *
                *  *  The right eigenvector v(j) corresponding to the eigenvalue lambda(j)
                *  *  of (A,B) satisfies
                *  *
                *  *                   A * v(j) = lambda(j) * B * v(j).
                *  *
                *  *  The left eigenvector u(j) corresponding to the eigenvalue lambda(j)
                *  *  of (A,B) satisfies
                *  *
                *  *                   u(j)**H * A  = lambda(j) * u(j)**H * B .
                *  *
                *  *  where u(j)**H is the conjugate-transpose of u(j).
                *  *
                *  *
                *  *  Arguments
                *  *  =========
                *  *
                *  *  JOBVL   (input) CHARACTER*1
                *  *          = 'N':  do not compute the left generalized eigenvectors;
                *  *          = 'V':  compute the left generalized eigenvectors.
                *  *
                *  *  JOBVR   (input) CHARACTER*1
                *  *          = 'N':  do not compute the right generalized eigenvectors;
                *  *          = 'V':  compute the right generalized eigenvectors.
                *  *
                *  *  N       (input) INTEGER
                *  *          The order of the matrices A, B, VL, and VR.  N >= 0.
                *  *
                *  *  A       (input/output) REAL array, dimension (LDA, N)
                *  *          On entry, the matrix A in the pair (A,B).
                *  *          On exit, A has been overwritten.
                *  *
                *  *  LDA     (input) INTEGER
                *  *          The leading dimension of A.  LDA >= max(1,N).
                *  *
                *  *  B       (input/output) REAL array, dimension (LDB, N)
                *  *          On entry, the matrix B in the pair (A,B).
                *  *          On exit, B has been overwritten.
                *  *
                *  *  LDB     (input) INTEGER
                *  *          The leading dimension of B.  LDB >= max(1,N).
                *  *
                *  *  ALPHAR  (output) REAL array, dimension (N)
                *  *  ALPHAI  (output) REAL array, dimension (N)
                *  *  BETA    (output) REAL array, dimension (N)
                *  *          On exit, (ALPHAR(j) + ALPHAI(j)*i)/BETA(j), j=1,...,N, will
                *  *          be the generalized eigenvalues.  If ALPHAI(j) is zero, then
                *  *          the j-th eigenvalue is real; if positive, then the j-th and
                *  *          (j+1)-st eigenvalues are a complex conjugate pair, with
                *  *          ALPHAI(j+1) negative.
                *  *
                *  *          Note: the quotients ALPHAR(j)/BETA(j) and ALPHAI(j)/BETA(j)
                *  *          may easily over- or underflow, and BETA(j) may even be zero.
                *  *          Thus, the user should avoid naively computing the ratio
                *  *          alpha/beta.  However, ALPHAR and ALPHAI will be always less
                *  *          than and usually comparable with norm(A) in magnitude, and
                *  *          BETA always less than and usually comparable with norm(B).
                *  *
                *  *  VL      (output) REAL array, dimension (LDVL,N)
                *  *          If JOBVL = 'V', the left eigenvectors u(j) are stored one
                *  *          after another in the columns of VL, in the same order as
                *  *          their eigenvalues. If the j-th eigenvalue is real, then
                *  *          u(j) = VL(:,j), the j-th column of VL. If the j-th and
                *  *          (j+1)-th eigenvalues form a complex conjugate pair, then
                *  *          u(j) = VL(:,j)+i*VL(:,j+1) and u(j+1) = VL(:,j)-i*VL(:,j+1).
                *  *          Each eigenvector is scaled so the largest component has
                *  *          abs(real part)+abs(imag. part)=1.
                *  *          Not referenced if JOBVL = 'N'.
                *  *
                *  *  LDVL    (input) INTEGER
                *  *          The leading dimension of the matrix VL. LDVL >= 1, and
                *  *          if JOBVL = 'V', LDVL >= N.
                *  *
                *  *  VR      (output) REAL array, dimension (LDVR,N)
                *  *          If JOBVR = 'V', the right eigenvectors v(j) are stored one
                *  *          after another in the columns of VR, in the same order as
                *  *          their eigenvalues. If the j-th eigenvalue is real, then
                *  *          v(j) = VR(:,j), the j-th column of VR. If the j-th and
                *  *          (j+1)-th eigenvalues form a complex conjugate pair, then
                *  *          v(j) = VR(:,j)+i*VR(:,j+1) and v(j+1) = VR(:,j)-i*VR(:,j+1).
                *  *          Each eigenvector is scaled so the largest component has
                *  *          abs(real part)+abs(imag. part)=1.
                *  *          Not referenced if JOBVR = 'N'.
                *  *
                *  *  LDVR    (input) INTEGER
                *  *          The leading dimension of the matrix VR. LDVR >= 1, and
                *  *          if JOBVR = 'V', LDVR >= N.
                *  *
                *  *  WORK    (workspace/output) REAL array, dimension (MAX(1,LWORK))
                *  *          On exit, if INFO = 0, WORK(1) returns the optimal LWORK.
                *  *
                *  *  LWORK   (input) INTEGER
                *  *          The dimension of the array WORK.  LWORK >= max(1,8*N).
                *  *          For good performance, LWORK must generally be larger.
                *  *
                *  *          If LWORK = -1, then a workspace query is assumed; the routine
                *  *          only calculates the optimal size of the WORK array, returns
                *  *          this value as the first entry of the WORK array, and no error
                *  *          message related to LWORK is issued by XERBLA.
                *  *
                *  *  INFO    (output) INTEGER
                *  *          = 0:  successful exit
                *  *          < 0:  if INFO = -i, the i-th argument had an illegal value.
                *  *          = 1,...,N:
                *  *                The QZ iteration failed.  No eigenvectors have been
                *  *                calculated, but ALPHAR(j), ALPHAI(j), and BETA(j)
                *  *                should be correct for j=INFO+1,...,N.
                *  *          > N:  =N+1: other than QZ iteration failed in SHGEQZ.
                *  *                =N+2: error return from STGEVC.
                *  * explanation from http://www.netlib.org/lapack/explore-3.1.1-html/sggev.f.html
                *  */
                lapack.GGEV('N', 'V', matrix_dimension, matrixA.values(), matrixA.stride(), matrixB.values(), matrixB.stride(), eigen_, eigen_comp, beta, VL.values(), matrix_dimension, VR.values(), matrix_dimension, WORK, 10*matrix_dimension, &info);
            }
        }
        else{
            if(this -> is_sym_real){
                Verbose::single(Verbose::Detail) << "Direct_Diagonalize::solve: solving symmetric real matrix (SYEV)" << std::endl;
                //SYEV (const char JOBZ, const char UPLO, const int N, double *A, const int LDA, double *W, double *WORK, const int LWORK, int *INFO)
                //input
                //  jobz = 'V', eigenvalue + eigenvector
                //  uplo = 'U' : upper triangular part
                //  n : matrix dimension
                //  a : upper triangular matrix, array
                //  lda : the leading dimension of a
                //output
                //  w : eigenvalues, array
                //  a : eigenvectors, overwritten
                //others
                //  work : buffer
                //  lwork : length of work
                //  info : error code. info==0 : success
                lapack.SYEV('V', 'U', matrix_dimension, matrixA.values(), matrixA.stride(), eigen_, WORK, 10*matrix_dimension, &info);
            } else {
                Verbose::single(Verbose::Detail) << "Direct_Diagonalize::solve: solving general matrix (GEEV)" << std::endl;
                Teuchos::SerialDenseMatrix<int,double> VL = Teuchos::SerialDenseMatrix<int,double>(matrix_dimension,matrix_dimension);
                double* rwork = new double[10*matrix_dimension];
                lapack.GEEV('N', 'V', matrix_dimension, matrixA.values(), matrixA.stride(), eigen_, eigen_comp, VL.values(), matrix_dimension, VR.values(), matrix_dimension, WORK, 10*matrix_dimension, rwork, &info);
                delete[] rwork;
            }
        }
        delete[] WORK;
        if(info==0){
            Verbose::single(Verbose::Normal) << "Direct_Diagonalize::solve: success!" << std::endl;
        }
        else{
            Verbose::single(Verbose::Simple) << "Direct_Diagonalize::solve: FAIL!!!" << std::endl;
            Verbose::single(Verbose::Simple) << "LAPACK ERROR CODE : INFO = " << info <<std::endl;
        }
    }

    Parallel_Manager::info().group_bcast(eigen_, matrix_dimension, 0);
    if(!this -> is_sym_real){
        Parallel_Manager::info().group_bcast(eigen_comp, matrix_dimension, 0);
        if(is_GEV){
            Parallel_Manager::info().group_bcast(beta, matrix_dimension, 0);
        }
    }

    vector<int> index_list;
    for(int i = 0; i < matrix_dimension; ++i){
        index_list.push_back(i);
    }
    //std::stable_sort(index_list.begin(), index_list.end(), [&](int i, int j)->bool{return eigen_[i]<eigen_[j];} );
    std::stable_sort(index_list.begin(), index_list.end(), [&](int i, int j)->bool{return (std::abs(eigen_[i]-eigen_[j])>_COMPLEX_CUTOFF_)? eigen_[i]<eigen_[j]: eigen_comp[i]>eigen_comp[j];} );

    if(is_GEV && !this->is_sym_real){
        for(int i =0; i<numev; ++i){
            int i2 = index_list[i];
            eigenValues.push_back(eigen_[i2] / beta[i2]); // may cause overflow error.
            eigenValues_imag.push_back(eigen_comp[i2]/beta[i2]);// may cause overflow error.
            if(std::abs(eigen_comp[i2]/eigen_[i2]) > 1.0E-5) Verbose::single(Verbose::Simple) << i+1 << "th eigenvalue is complex! Imag part / real part: " << eigen_comp[i2]/beta[i2] << "/" << eigen_[i2]/beta[i2] << std::endl;
        }
        delete[] beta;
    }
    else{
        for(int i =0; i<numev; ++i){
            int i2 = index_list[i];
            eigenValues.push_back(eigen_[i2]);
            if(!this -> is_sym_real){
                eigenValues_imag.push_back(eigen_comp[i2]);
            } else {
                eigenValues_imag.push_back(0.0);
            }
        }
    }
    //int tmp_size_eigval = eigenValues.size();

    /*
     * GEEV output
     * If j-th eigenvalue is real, i-th component of j-th eigenvector is VR[(i-1)*ldvl+(j-1)].
     * If j-th and j+1-th eigenvalue is conjugate, k-th component of j-th eigenvector is VR[(k-1)*ldvl+(j-1)]+i*VR[(k-1)*ldvl+j]
     * SerialDenseMatrix.values() seems to be colIndex*stride_+rowIndex and (rowIndex, colIndex).
     */
    double* arrEigenVec = new double[matrix_dimension*matrix_dimension];
    if(Parallel_Manager::info().get_mpi_rank()==0){
        int col,row;
        if(!(this -> is_sym_real)){
            for(int index=0; index< matrix_dimension*matrix_dimension; ++index){
                arrEigenVec[index] = VR(index/matrix_dimension,index%matrix_dimension);
                //arrEigenVec[index] = VR(index%matrix_dimension,index/matrix_dimension);//Prob wrong. Check after sort.
            }
        }
        else{
            for(int index=0; index< matrix_dimension*matrix_dimension; ++index){
                //col = index%matrix_dimension;
                //row = index/matrix_dimension;
                arrEigenVec[index] = matrixA(index/matrix_dimension,index%matrix_dimension);
            }
        }
    }
    Parallel_Manager::info().group_bcast(arrEigenVec,matrix_dimension*matrix_dimension,0 );

    //Parallel_Manager::info().group_scatter(matrix.values(), matrix_dimension*matrix.stride(), matrix.values(), matrix_dimension*matrix_dimension, 0 );

    vector< vector<double> > EigenVec(matrix_dimension);
    vector< vector<double> > EigenVec_imag(matrix_dimension);
    for(int col=0; col<matrix_dimension; ++col){
        EigenVec[col].resize(matrix_dimension);
        EigenVec_imag[col].resize(matrix_dimension);
    }
    if(this -> is_sym_real){
        for(int col=0; col<matrix_dimension; ++col){
            for(int row=0; row<matrix_dimension; ++row){
                int i = row*matrix_dimension+col;
                EigenVec[col][row] = arrEigenVec[i];
                //EigenVec[row][col] = arrEigenVec[i];
            }
        }
    } else {
        //if(!is_GEV){
        for(int col=0; col<matrix_dimension; ++col){
            for(int row=0; row<matrix_dimension; ++row){
                int i = row*matrix_dimension+col;
                if(std::abs(eigen_comp[col]) > 0.0){
                    int ii0 = std::distance(index_list.begin(), std::find(index_list.begin(), index_list.end(), col));
                    //if(col < matrix_dimension-1 and std::abs(eigen_[col]-eigen_[col+1]) < _COMPLEX_CUTOFF_ and std::abs(eigen_comp[col]+eigen_comp[col+1]) < _COMPLEX_CUTOFF_){
                    if(col < matrix_dimension-1 and std::abs(eigen_comp[col]+eigen_comp[col+1]) < _COMPLEX_CUTOFF_){
                        int ii1 = std::distance(index_list.begin(), std::find(index_list.begin(), index_list.end(), col+1));
                        //if(row == 0) Verbose::single() << ii0 << " and " << ii1 << " are conjugate! " << eigen_[col] << " +- i * " << eigen_comp[col+1] << std::endl;
                        EigenVec[col][row] = arrEigenVec[i];
                        EigenVec[col+1][row] = arrEigenVec[i];
                        EigenVec_imag[col][row] = arrEigenVec[i+1];
                        EigenVec_imag[col+1][row] = -arrEigenVec[i+1];
                    //} else if(!(col > 0 and std::abs(eigen_[col-1]-eigen_[col]) < _COMPLEX_CUTOFF_ and std::abs(eigen_comp[col-1]+eigen_comp[col]) < _COMPLEX_CUTOFF_)){
                    } else if(!(col > 0 and std::abs(eigen_comp[col-1]+eigen_comp[col]) < _COMPLEX_CUTOFF_)){
                        if(row == 0){
                            Verbose::all() << "Cannot find conjugate of " << ii0 << std::endl
                                           << col-1 << "th = " << eigen_[col-1] << " + i * " << eigen_comp[col-1] << std::endl
                                           << col   << "th = " << eigen_[col]   << " + i * " << eigen_comp[col]   << std::endl
                                           << col+1 << "th = " << eigen_[col+1] << " + i * " << eigen_comp[col+1] << std::endl;
                        }
                        throw std::runtime_error("Direct_Diagonalize: Unknown complex conjugate!");
                    }
                } else {
                    EigenVec[col][row] = arrEigenVec[i];
                }
            }
        }
    }

    if(!this -> is_sym_real){
        delete[] eigen_comp;
    }
    delete[] eigen_;

    eigenVectors = Teuchos::rcp(new Epetra_MultiVector(input->Map(), numev));
    eigenVectors_imag = Teuchos::rcp(new Epetra_MultiVector(input->Map(), numev));
    int* MyGlobalElements = input->Map().MyGlobalElements();
    for(int col=0; col<numev; ++col){
        for(int row=0; row<input->NumMyRows(); ++row){
            //eigenVectors->operator[](index)[i] = matrix[MyGlobalElements[i]][index];
            //eigenVectors->operator[](col)[row] = arrEigenVec[MyGlobalElements[row]*matrix_dimension+col];
            //eigenVectors -> operator[](col)[row] = EigenVec[col][MyGlobalElements[row]];
            //eigenVectors_imag -> operator[](col)[row] = EigenVec_imag[col][MyGlobalElements[row]];
            eigenVectors -> operator[](col)[row] = EigenVec[index_list[col]][MyGlobalElements[row]];
            eigenVectors_imag -> operator[](col)[row] = EigenVec_imag[index_list[col]][MyGlobalElements[row]];
        }
    }
    delete [] arrEigenVec;

    for(int i = 0; i < numev; ++i){
        if(std::abs(eigenValues_imag[i]) > 0.0){
            std::vector<double> v2(matrix_dimension);
            int iold = index_list[i];
            for(int j = 0; j < matrix_dimension; ++j){
                v2[j] = pow(EigenVec[iold][j], 2) + pow(EigenVec_imag[iold][j], 2);
            }
            int i2 = std::distance(v2.begin(), std::max_element(v2.begin(), v2.end()));
            double rot_angle = -std::atan(EigenVec_imag[iold][i2]/EigenVec[iold][i2]);
            //Verbose::single(Verbose::Detail) << "Rotating eigenvector " << i << "(" << iold << ") by " << rot_angle << " rad " << i2 << std::endl;
            Teuchos::RCP<Epetra_Vector> tmp_r = Teuchos::rcp(new Epetra_Vector(*eigenVectors -> operator()(i)));
            Teuchos::RCP<Epetra_Vector> tmp_i = Teuchos::rcp(new Epetra_Vector(*eigenVectors_imag -> operator()(i)));
            tmp_r -> Scale(std::cos(rot_angle));
            tmp_r -> Update(-std::sin(rot_angle), *eigenVectors_imag -> operator()(i), 1.0);
            tmp_i -> Scale(std::cos(rot_angle));
            tmp_i -> Update(std::sin(rot_angle), *eigenVectors -> operator()(i), 1.0);
            eigenVectors -> operator()(i) -> Update(1.0, *tmp_r, 0.0);
            eigenVectors_imag -> operator()(i) -> Update(1.0, *tmp_i, 0.0);
        }
    }

    if(!this -> is_sym_real and is_GEV){
        double *normr = new double[numev]; double *normi = new double[numev];
        Teuchos::RCP<Epetra_MultiVector> xr = Teuchos::rcp(new Epetra_MultiVector(input->Map(), numev));
        Teuchos::RCP<Epetra_MultiVector> xi = Teuchos::rcp(new Epetra_MultiVector(input->Map(), numev));
        Teuchos::RCP<Epetra_MultiVector> x2 = Teuchos::rcp(new Epetra_MultiVector(input->Map(), numev));
        overlap_matrix -> Multiply(false, *eigenVectors, *xr);
        overlap_matrix -> Multiply(false, *eigenVectors_imag, *xi);
        xr -> Dot(*eigenVectors, normr);
        xi -> Dot(*eigenVectors_imag, normi);

        for(int i = 0; i < numev; ++i){
            if(std::abs(normr[i]+normi[i]) < 1.0E-15){
                Verbose::single(Verbose::Simple) << "Direct_Diagonalize: " << i+1 << "th eignevector norm is zero!" << normr[i] << ", " << normi[i] << std::endl;
            } else {
                if(normr[i]+normi[i] < 0.0){
                    eigenVectors -> operator()(i) -> Scale(1./sqrt(-normr[i]-normi[i]));
                    eigenVectors_imag -> operator()(i) -> Scale(1./sqrt(-normr[i]-normi[i]));
                } else {
                    eigenVectors -> operator()(i) -> Scale(1./sqrt(normr[i]+normi[i]));
                    eigenVectors_imag -> operator()(i) -> Scale(1./sqrt(normr[i]+normi[i]));
                }
            }
            /*
            if(std::abs(eigenValues_imag[i]) > 0.0){
                if(i < matrix_dimension-1 and std::abs(eigenValues_imag[i]+eigenValues_imag[i+1]) < 1.E-15){
                    Verbose::single() << i << " and " << i+1 << " are conjugate! ith NORMR: " << std::scientific << normr[i] << " NORMI: * " << std::scientific << normi[i] << std::endl;
                    Verbose::single() << i << " and " << i+1 << " are conjugate! i+1th NORMR: " << std::scientific << normr[i+1] << " NORMI: * " << std::scientific << normi[i+1] << std::endl;
                }
            }
            */
        }
        delete[] normr; delete[] normi;
    }

    timer -> end("Time to diagonalize");
    timer -> print(Verbose::single(Verbose::Simple));
    return true;
}

bool Direct_Diagonalize::diagonalize(
        Teuchos::RCP<Core_Hamiltonian_Matrix> core_hamiltonian_matrix, int ispin, int numev, Teuchos::RCP<Epetra_Vector> local_potential,
        Teuchos::RCP<Epetra_MultiVector> initial_eigenvector
        ){
    std::cout << "Direct_Diagonalize, this function is not supported" << std::endl;
    exit(-1);
}
Teuchos::RCP<Epetra_MultiVector> Direct_Diagonalize::get_eigenvectors(){
    return eigenVectors;
}
std::vector<double > Direct_Diagonalize::get_eigenvalues(){
    return eigenValues;
}

bool Direct_Diagonalize::diagonalize(const int matrix_dimension, int numev, double* matrix){
    if(numev > matrix_dimension){
        numev = matrix_dimension;
    }

    Epetra_LocalMap local_map (matrix_dimension,0, *Parallel_Manager::info().get_all_comm());

    Teuchos::LAPACK<int,double> lapack;

    double* eigen_values = new double [matrix_dimension];
    timer -> start("Time to diagonalize");


    Teuchos::SerialDenseMatrix<int,double> matrixA(Teuchos::Copy,matrix, matrix_dimension,matrix_dimension,matrix_dimension);


    /******* move to Utility/Parallel_Util
    int start_ =0;
    *************
            matrix[matrix_dimension-1][tar_col_indices[j] ] = tar_values[j];
    */
    double* WORK = new double [10*matrix_dimension];
    int info;
    Verbose::single(Verbose::Detail) << "Direct_Diagonalize::solve: solving symmetric real matrix (SYEV)" << std::endl;
    lapack.SYEV('V', 'U', matrix_dimension, matrixA.values(), matrixA.stride(), eigen_values, WORK, 10*matrix_dimension, &info);

    delete[] WORK;
    if(info==0){
        Verbose::single(Verbose::Normal) << "Direct_Diagonalize::solve: success!" << std::endl;
    }
    else{
        Verbose::single(Verbose::Simple) << "Direct_Diagonalize::solve: FAIL!!!" << std::endl;
        Verbose::single(Verbose::Simple) << "LAPACK ERROR CODE : INFO = " << info <<std::endl;
        return false;
    }

    eigenValues.clear();
    eigenVectors = Teuchos::rcp(new Epetra_MultiVector(local_map, numev));
    for (int i =0; i<numev; i++){
        eigenValues.push_back(eigen_values[i]);
        for (int j=0; j<matrix_dimension; j++){
            eigenVectors -> operator[](i)[j] = matrixA(i,j);
        }
    }
    delete[] eigen_values;
    timer -> end("Time to diagonalize");
    return true;
}
