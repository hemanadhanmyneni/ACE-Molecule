#include "Pure_Diagonalize.hpp"

#include "../../Utility/ACE_Config.hpp"
#include "../../Utility/Parallel_Manager.hpp"

#include "Epetra_Import.h"
#include <cmath>
#include "AnasaziConfigDefs.hpp"
#include "AnasaziTypes.hpp"
#include "AnasaziBasicEigenproblem.hpp"
#include "AnasaziEpetraAdapter.hpp"

//#include "Teuchos_CommandLineProcessor.hpp"

//#include "Epetra_Operator.h"
#ifdef ACE_HAVE_OMP
#include "omp.h"
#endif

#include "Teuchos_Time.hpp"
#include "Teuchos_TimeMonitor.hpp"
#include "Teuchos_Array.hpp"

//#include "../DataType/Gpu/CUDA_Hamiltonian_Matrix.hpp"
//#include "../DataType/Gpu/CUDA_MultiVec.hpp"
#include "../DataType/Cpu/Implicit_Hamiltonian_Matrix.hpp"


#include "AnasazimodifiedLOBPCGSolMgr.hpp"
//#include "AnasaziLOBPCGSolMgr.hpp"
//#include "AnasaziLOBPCG.hpp"
//#ifndef EDISON
#include "Trilinos_version.h"
#ifdef USE_EX_DIAG
#if TRILINOS_MAJOR_VERSION <= 11
#error "Trilinos Version should be at least 12!"
#else
#include "Anasazi12/AnasazimodifiedBlockKrylovSchurSolMgr.hpp"
#include "Anasazi12/AnasazimodifiedBlockDavidsonSolMgr.hpp"
#endif
#endif// ifdef USE_EX_DIAG

#include "Ifpack_ConfigDefs.h"
#include "Ifpack_Utils.h"

#include "Ifpack_AdditiveSchwarz.h"
#include "Ifpack_SparseContainer.h"
#include "Ifpack_BlockRelaxation.h"
#include "Ifpack_PointRelaxation.h"
#include "Ifpack_Amesos.h"

#include "AnasaziBasicOutputManager.hpp"
#include "AnasaziStatusTestDecl.hpp"
#include "AnasaziStatusTestCombo.hpp"

#include "ml_include.h"
#include "ml_MultiLevelPreconditioner.h"

#include "../../Utility/String_Util.hpp"

#ifdef USE_CUDA
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <device_launch_parameters.h>
#include <curand.h>
#include <cublas_v2.h>
#include "../DataType/Gpu/CUDA_Hamiltonian_Matrix.hpp"
#include "../DataType/Gpu/CUDA_MultiVec.hpp"
extern cublasHandle_t CUDA::Global_handle;
//extern cudaStream_t* CUDA::Global_stream;
//extern int CUDA::NStream;
//template class CUDA::MultiVec<double>;
#endif

using std::min;
using std::string;
using Teuchos::rcp;
using Teuchos::RCP;
using Teuchos::null;
using Teuchos::ParameterList;
using Isorropia::Epetra::Redistributor;
using Isorropia::Epetra::Partitioner;

//Pure_Diagonalize::Pure_Diagonalize(int num_eigen, int block_size, int prec_overlap_level, int max_iter, string verb, bool locking, double locking_tolerance, int max_locked, bool full_ortho, double tolerance, string precond_type, string eigen_solver  ){
Pure_Diagonalize::Pure_Diagonalize(
    int block_size, int prec_overlap_level, int max_iter, string verb,
    double locking_tolerance, int max_locked, bool full_ortho,
    double tolerance, string precond_type, string eigen_solver,
    bool redistribution,double balancing_tol,std::string redistribute_method,
    bool gpu_enable, int NGPU  )
    :   block_size(block_size),prec_overlap_level(prec_overlap_level),
        verb(verb),
        locking_tolerance(locking_tolerance),max_locked(max_locked),full_ortho(full_ortho),
        precond_type(precond_type), eigen_solver(eigen_solver),
        redistribution(redistribution),balancing_tol(balancing_tol), redistribute_method(redistribute_method),
        gpu_enable(gpu_enable), NGPU(NGPU)
{
    this->max_iter = max_iter;
    this->tolerance = tolerance;
    Verbose::single(Verbose::Normal) << "\n" << "#---------------------------------------- Pure_Diagonalize::Pure_Diagonalize" << std::endl;
    Verbose::single(Verbose::Normal) << "verbosity of Pure_Diagonalize:  " << verb <<std::endl;
    //this->timer = Teuchos::rcp( new Time_Measure() );
    if ( max_locked > 0 ){
        Verbose::single(Verbose::Normal) << "Locking is used" <<std::endl;
    }
    else{
        Verbose::single(Verbose::Normal) << "Locking is not used" <<std::endl;
    }

    if(full_ortho){
        Verbose::single(Verbose::Normal) << "Full orthogonalization is used" <<std::endl;
    }
    else{
        Verbose::single(Verbose::Normal) << "Full orthogonalization is not used" <<std::endl;
    }
    Verbose::single(Verbose::Normal) << "Tolerance is " << tolerance  <<std::endl;
    Verbose::single(Verbose::Normal) << "Max iteration number is " << max_iter  <<std::endl;

    Verbose::single(Verbose::Normal)<< "#--------------------------------------------------------------------------- "<<std::endl ;
}

bool Pure_Diagonalize::diagonalize(RCP<Epetra_CrsMatrix> matrix, int numev, RCP<Epetra_MultiVector> initial_eigenvector, RCP<Epetra_CrsMatrix> overlap_matrix/* = Teuchos::null*/){

//    RCP<Anasazi::EpetraMultiVec> eigenvectors;
    RCP<Epetra_MultiVector> eigenvectors;
    Verbose::single(Verbose::Normal) << " " << std::endl;
    Verbose::single(Verbose::Normal)<< "#--------------------------------------------- Pure_Diagonalize::diagonalize "<< std::endl;

    initializer(eigenvectors, initial_eigenvector, matrix->RowMap(), numev, overlap_matrix);
/*
    Verbose::single()<< "initialized as [1,0,0,0, ... ] "<<std::endl;
    eigenvectors->PutScalar(0.0);
    if(Parallel_Manager::info().get_mpi_rank()==0){
        for(int i =0; i<eigenvectors->NumVectors(); i++) eigenvectors->operator[](i)[i] = 1.0;
    }
*/
    //int block_size = this -> block_size;

    if (!matrix->Filled()){
        Verbose::single(Verbose::Detail)<< " FillComplete is called." << std::endl;
        matrix->FillComplete();
    }

    //double time=0.0;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //BasicOutputManager<double> printer(verbose);

    //string precond_type="IC stand-alone";
    //string precond_type="ILU stand-alone";
    //    string precond_type="ILUT stand-alone";
    //string precond_type="Amesos stand-alone";
    //string precond_type = "ICT stand-alone";

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    if(!gpu_enable){

        typedef Epetra_MultiVector MV;
//        typedef Anasazi::EpetraMultiVec MV;
        typedef Epetra_Operator OP;
        //typedef Epetra_CrsMatrix OP;

        Teuchos::RCP<Epetra_CrsMatrix> new_matrix;
        Teuchos::RCP<Epetra_CrsMatrix> new_overlap;
        Teuchos::RCP<Epetra_MultiVector> new_eigenvectors;
        redistribute(matrix,overlap_matrix,eigenvectors,new_matrix,new_overlap,new_eigenvectors);
        RCP < Anasazi::BasicEigenproblem<double,MV,OP> > MyProblem = rcp(new Anasazi::BasicEigenproblem<double,MV,OP>(new_matrix, new_eigenvectors));
        MyProblem->setNEV( numev );
        set_problem<MV,OP> (MyProblem, new_matrix, new_overlap);
        //set_problem(MyProblem, matrix, overlap_matrix,this->redistribution);

    //    RCP<Epetra_MultiVector> Ritz_vectors = rcp(new Epetra_MultiVector(matrix->RowMap(), numev));
        RCP<Epetra_MultiVector> Ritz_vectors = rcp(new Epetra_MultiVector(new_matrix->RowMap(), numev)); //09.30 3:30 shchoi
        std::vector< Anasazi::Value<double> > Ritz_values;
        for(int i=0; i<numev; i++){
            Ritz_values.push_back(Anasazi::Value<double>());
        }

        //set_problem(MyProblem, matrix, overlap_matrix,this->redistribution);
        //Anasazi::ReturnType returnCode;
        timer->start("Time to diagonalize" );
        while (true){
            ParameterList MyPL;
            set_ParameterList(&MyPL);

            if(eigen_solver == "LOBPCG"){
                Anasazi::modifiedLOBPCGSolMgr<double, MV, OP> MySolverMan(MyProblem, MyPL);
                returnCode = MySolverMan.solve(Ritz_vectors, Ritz_values);
                //Anasazi::LOBPCGSolMgr<double, MV, OP> MySolverMan(MyProblem, MyPL);
                //returnCode = MySolverMan.solve();
            }
    #ifdef USE_EX_DIAG
            else if( eigen_solver == "BlockKrylovSchur"){
                Anasazi::modifiedBlockKrylovSchurSolMgr<double, MV, OP> MySolverMan(MyProblem, MyPL);
                returnCode = MySolverMan.solve(Ritz_vectors, Ritz_values);
//                Anasazi::BlockKrylovSchurSolMgr<double, MV, OP> MyBlockKrylovSchur(MyProblem, MyPL);
//                returnCode = MyBlockKrylovSchur.solve();
                //for(int k = 0; k < Ritz_vectors -> NumVectors(); ++k){
                //    double norm2;
                //    Ritz_vectors -> operator()(k) -> Norm2( &norm2 );
                //    Verbose::all() << "RITZVECTOR returned NORM2 = " << norm2 << std::endl;
                //}
            }
            else if(eigen_solver == "BlockDavidson"){
                Anasazi::modifiedBlockDavidsonSolMgr<double, MV, OP> MySolverMan(MyProblem, MyPL);
                //Anasazi::BlockDavidsonSolMgr<double, MV, OP> MySolverMan(MyProblem, MyPL);
//                returnCode = MySolverMan.solve(Ritz_vectors, Ritz_values);
                //Anasazi::LOBPCGSolMgr<double, MV, OP> MySolverMan(MyProblem, MyPL);
//                returnCode = MySolverMan.solve();
                returnCode = MySolverMan.solve(Ritz_vectors, Ritz_values);
            }
    #endif
            else{
                Verbose::all() << "There is no " << eigen_solver << " eigen solver "<<std::endl;
                exit(-1);
            }
            Parallel_Manager::info().all_barrier();
            /*
            if(MyPID == 0){
            Verbose::single()<< fixed << setprecision(8);
            cout << endl;
            cout << " Time to solve: " << double (end_time-start_time)/CLOCKS_PER_SEC << " s" << endl;
            cout << scientific << setprecision(8);
            }
            */
            if (returnCode == Anasazi::Converged){
//                Anasazi::Eigensolution<double,MV> sol = MyProblem->getSolution();
//                std::vector<Anasazi::Value<double> > evals = sol.Evals;
//                Teuchos::RCP<MV> evecs = sol.Evecs;
//                Verbose::single() << "eigenvalue" <<std::endl;
//                for (int i =0; i <evals.size(); i++){
//                    Verbose::single() << evals[i].realpart <<std::endl;
//                }
//                exit(-1);
                break;
            }
            else{
                Verbose::single(Verbose::Normal)<<" Failed to converge." <<std::endl;
                break;
            }
            /*
            switch (solverreturn) {
            case Anasazi::Unconverged:
            if (verbose){
            cout << "Anasazi::BlockKrylovSchur::solve() did not converge!" << endl;
            }
            return ;
            break;
            case Anasazi::Converged:
            if (verbose){
            cout << "Anasazi::BlockKrylovSchur::solve() converged!" << endl;
            }
            break;
            }
            */
        }
        timer->end("Time to diagonalize" );

        RitzValues.clear();
        if(redistribution){
            RitzVectors = rcp(new Epetra_MultiVector(new_matrix->RowMap(),eigenvectors->NumVectors() ));
            auto num_vec = eigenvectors->NumVectors();
            auto num_elements = new_matrix->RowMap().NumMyElements();
            #pragma omp for collapse(2)
            for(int i=0; i<num_vec; i++){
                for(int j=0; j<num_elements; j++){
                    RitzVectors->operator[](i)[j] = (*Ritz_vectors)[i][j];
                }
            }

            Epetra_MultiVector* testRitzVectors = new Epetra_MultiVector(matrix->Map(), Ritz_vectors->NumVectors()); // 09.30 3:30 shchoi
            Epetra_Import importer(matrix->Map(),new_matrix->Map());
            timer->start("Reverse redistribution of orbitals");
            testRitzVectors->Import(*Ritz_vectors, importer,Insert);
            timer->end("Reverse redistribution of orbitals");
            RitzVectors = rcp(testRitzVectors);
        }
        else{
            //RitzVectors = rcp(new Epetra_MultiVector(*Ritz_vectors)); // 09.30 3:30 shchoi
            RitzVectors = rcp(new Epetra_MultiVector(matrix->RowMap(),eigenvectors->NumVectors() ));
            auto num_vec = eigenvectors->NumVectors();
            auto num_elements = new_matrix->RowMap().NumMyElements();
            #pragma omp for collapse(2)
            for(int i=0; i<numev; i++){
                for(int j=0; j<num_elements; j++){
                    RitzVectors->operator[](i)[j] = (*Ritz_vectors)[i][j];
                }
            }

            RitzVectors = rcp(new Epetra_MultiVector(*Ritz_vectors)); // 09.30 3:30 shchoi

        }
        double* norm2 = new double[numev]();

        RitzVectors->Norm2(norm2);
        //std::cout << *RitzVectors<<std::endl; //shchoi
        //    RCP<AnasaziOrthoManager> orthoManager;

        bool is_normal = false;
        for(int i=0; i<numev; i++){
            /*
               if(norm2[i]<1E-30){
               if(i<initial_eigenvector->NumVectors()){
               RitzVectors->operator()(i)->Update(1.0, *initial_eigenvector->operator()(i), 1.0);
               }
               else{
               Verbose::single()<< "randomly generate new initial vector becuase not enough eigenvectors are found in previous step (" <<i <<")" <<std::endl;
            //                RCP<Epetra_Vector> orthoVector = rcp(new Epetra_MultiVector(initial_eigenvector->Map() ));
            //                orthoVector->Random();
            RitzVectors->operator()(i)->Random();
            }
            }
            */
            is_normal = is_normal or std::isnormal(Ritz_values[i].realpart);
            RitzValues.push_back(Ritz_values[i].realpart);
        }
        delete[] norm2;
        if(!is_normal){
            Verbose::all() << "All eigenvalue real parts are not normal!" << std::endl;
            exit(EXIT_FAILURE);
        }

        //timer->start("compute direct residuals ");
        //cout << "this is eigenvector in pure_diagonalization " <<endl;
        //vector<double > evals = solution.Evals;

        /*
        // test
        int num_ev=solution.numVecs;
        cout << "num_ev " << num_ev << endl;
        RCP<Epetra_MultiVector> evecs=solution.Evecs;
        Epetra_MultiVector Aevecs(matrix->RowMap(),num_ev);
        Teuchos::SerialDenseMatrix<int,double> B(num_ev,num_ev);
        B.putScalar(0.0);
        for(int i=0;i<num_ev;i++){
        B(i,i)=solution.Evals[i].real;
        }
        // Aevecs = matrix*evecs
        Anasazi::OperatorTraits<double,Epetra_MultiVector,Epetra_Operator>::Apply(*matrix,*evecs,Aevecs);
        std::vector<double> normA(num_ev);
        // Anasazi::MultiVecTraits
        // -1.0*evecs*B + 1.0*Aevecs
        MVT::MvTimesMatAddMv(-1.0,*evecs,B,1.0,Aevecs);
        // Aevecs의 2-norm을 구해서 normA에 넣는다.
        MVT::MvNorm(Aevecs,normA);
        cout << "Direct residual" << endl;
        for(int i=0;i<num_ev;i++){
        cout << normA[i] << endl;
        normA[i] /= Teuchos::ScalarTraits<double>::magnitude( solution.Evals[i].real );
        cout << normA[i] << endl;
        }
        // test end
        */
        //cout.precision();
        //cout << "end of diagonalize" <<endl;
        //timer->end("compute direct residuals ");
        //cout << "Pure_Diagonalize:: compute direct residuals " <<timer->get_elapsed_time("compute direct residuals ",-1) << " s" << endl;
        //cout << "Pure_Diagonalize::compute direct residuals  " << double(end_time-start_time)/CLOCKS_PER_SEC << " s" << endl;

        Verbose::single(Verbose::Normal)<< "#---------------------------------------------------------------------------" <<"\n";

        Verbose::single(Verbose::Normal) << "Pure_Diagonalize time profile:" << std::endl;
        Verbose::set_numformat(Verbose::Time);
        timer->print(Verbose::single(Verbose::Normal));

        if( returnCode == Anasazi::Converged ){
            return true;
        } else {
            return false;
        }
    }
    else{
#ifdef USE_CUDA
        cublasCreate(&CUDA::Global_handle);
        size_t total_byte, free_byte ;

        cudaError_t cuda_status = cudaMemGetInfo( &free_byte, &total_byte ) ;

        if ( cudaSuccess != cuda_status ){
            printf("Error: cudaMemGetInfo fails, %s \n", cudaGetErrorString(cuda_status) );
            exit(1);
        }

        double free_db = (double)free_byte ;
        double total_db = (double)total_byte ;
        double used_db = total_db - free_db ;

        int rank = Parallel_Manager::info().get_total_mpi_rank();
        Verbose::single(Verbose::Simple) << "rank : " << rank <<",  GPU memory usage: used =" << used_db/1024.0/1024.0 <<", free = "<< free_db/1024.0/1024.0 << " MB, total = " << total_db/1024.0/1024.0 <<" MB"<<std::endl;

        typedef CUDA::MultiVec<double> MV;
        typedef CUDA::Hamiltonian_Matrix<double> OP;
        //#typedef Anasazi::MultiVec<double> MV;
        //typedef Anasazi::Operator<double> OP;
        RCP<Anasazi::BasicEigenproblem<double,MV,OP> > MyProblem;
        RCP< OP > K = rcp( new OP(matrix, gpu_enable) );
        double* tmp_vector = new double[eigenvectors->Map().NumMyElements()*eigenvectors->NumVectors()];
//        for(int i=0; i<eigenvectors->NumVectors(); i++){
//        }
        RCP< MV > MV_eigenvectors = rcp(new MV(eigenvectors->Map().NumMyElements(), eigenvectors->NumVectors(), eigenvectors->Pointers()), true);

        MyProblem = rcp( new Anasazi::BasicEigenproblem<double,MV,OP>(K,MV_eigenvectors) );
        MyProblem->setNEV( numev );
        MyProblem->setHermitian(true);
        bool boolret = MyProblem->setProblem();

        if(boolret != true){
            Verbose::all()<< "Anasazi::BasicEigenproblem::setProblem() returned an error." << std::endl;
            exit(EXIT_FAILURE);
        }//else cout << "setProblem()" << endl;

        //RCP<Epetra_MultiVector> Ritz_vectors = rcp(new Epetra_MultiVector(local_potential->Map(), numev));
        //RCP< MultiVec<double> > Ritz_vectors = rcp(new MultiVec<double>(eigenvectors->Map().NumGlobalElements(), eigenvectors->NumVectors(), true));
        RCP< MV> Ritz_vectors = rcp(new MV(eigenvectors->Map().NumMyElements(), eigenvectors->NumVectors(), true));
        std::vector< Anasazi::Value<double> > Ritz_values;
        for(int i=0; i<numev; i++){
            Ritz_values.push_back(Anasazi::Value<double>());
        }

        timer->start("Time to diagonalize" );
        while (true){
            ParameterList MyPL;
            set_ParameterList(&MyPL);

            if(eigen_solver == "LOBPCG"){
                Anasazi::modifiedLOBPCGSolMgr<double, MV, OP> MySolverMan(MyProblem, MyPL);
                returnCode = MySolverMan.solve(Ritz_vectors, Ritz_values);
                //Anasazi::LOBPCGSolMgr<double, MV, OP> MySolverMan(MyProblem, MyPL);
                //returnCode = MySolverMan.solve();
            }
#ifdef USE_EX_DIAG
            else if( eigen_solver == "BlockKrylovSchur"){
                Anasazi::modifiedBlockKrylovSchurSolMgr<double, MV, OP> MySolverMan(MyProblem, MyPL);
                returnCode = MySolverMan.solve(Ritz_vectors, Ritz_values);
            }
            else if(eigen_solver == "BlockDavidson"){
                Anasazi::modifiedBlockDavidsonSolMgr<double, MV, OP> MySolverMan(MyProblem, MyPL);
                returnCode = MySolverMan.solve(Ritz_vectors, Ritz_values);
            }
#endif
            else{
                Verbose::all() << "There is no " << eigen_solver << "eigen solver "<<std::endl;
                exit(-1);
            }

            if (returnCode == Anasazi::Converged){
                break;
            }
            else{
                Verbose::single(Verbose::Normal) << " Fail to converge." <<"\n";
                break;
            }
        }

        cublasDestroy(CUDA::Global_handle);

        timer->end("Time to diagonalize" );

        RitzValues.clear();
        Ritz_vectors->device_to_host();

        RitzVectors = rcp(new Epetra_MultiVector(matrix->RowMap(),eigenvectors->NumVectors() ));
        for(int i=0; i<eigenvectors->NumVectors(); i++){
            for(int j=0; j<eigenvectors->Map().NumMyElements(); j++){
                RitzVectors->operator[](i)[j] = (*Ritz_vectors)(j,i);
            }
        }
        double* norm2 = new double[numev]();

        RitzVectors->Norm2(norm2);


        for(int i=0; i<numev; i++){
            RitzValues.push_back(Ritz_values[i].realpart);
        }
        delete[] norm2;


        //Verbose::single()<< "#---------------------------------------------------------------------------" <<"\n";
        timer->print(Verbose::single(Verbose::Normal));

        if( returnCode == Anasazi::Converged ){
            return true;
        }
        else {
            return false;
        }
#else
    std::cout << "This function requires compile with cuda_enable" << std::endl;
    exit(-1);
#endif
    }

}

bool Pure_Diagonalize::diagonalize(
        Teuchos::RCP<Core_Hamiltonian_Matrix> core_hamiltonian_matrix, int ispin, int numev, Teuchos::RCP<Epetra_Vector> local_potential,
        Teuchos::RCP<Epetra_MultiVector> initial_eigenvector,
        Teuchos::RCP<Epetra_CrsMatrix> overlap_matrix/* = Teuchos::null*/
        ){

//    typedef Teuchos::ScalarTraits<ST>                   SCT;
//    typedef SCT::magnitudeType                  MT;
//    typedef Epetra_MultiVector               MV;
    //typedef Hamiltonian_Matrix_Multiplier OP;
//    typedef Anasazi::Operator<double>               OP;
//    typedef Anasazi::MultiVecTraits<ST,MV>     MVT;
//    typedef Anasazi::OperatorTraits<ST,MV,OP>  OPT;
    Verbose::single() << std::endl;
    Verbose::single() << "#--------------------------------------------- Pure_Diagonalize::diagonalize "<< std::endl;
    //Teuchos::RCP<Anasazi::EpetraMultiVec> eigenvectors;
    Teuchos::RCP<Epetra_MultiVector> eigenvectors;
    initializer(eigenvectors, initial_eigenvector, local_potential->Map(), numev, overlap_matrix);
    //int block_size = this -> block_size;

    //double time=0.0;
    //clock_t start_time,end_time;

    //start_time = clock();
//    typedef Epetra_Operator               OP;
    if (!gpu_enable){
        typedef Epetra_MultiVector MV;
//        typedef Anasazi::EpetraMultiVec MV;
//        typedef Anasazi::Operator OP;
//        typedef Implicit_Hamiltonian_Matrix OP;
        typedef Epetra_Operator OP;
        Teuchos::RCP< Implicit_Hamiltonian_Matrix > K
            = Teuchos::rcp( new Implicit_Hamiltonian_Matrix(core_hamiltonian_matrix,local_potential) );
        K->set_spin(ispin);
        Teuchos::RCP<Anasazi::BasicEigenproblem<double,MV,OP> > MyProblem = Teuchos::rcp( new Anasazi::BasicEigenproblem<double,MV,OP>(K,eigenvectors) );

        MyProblem->setNEV( numev );
        set_problem<MV,OP>(MyProblem, Teuchos::null, overlap_matrix);

        auto Ritz_vectors = rcp(new Anasazi::EpetraMultiVec(Epetra_MultiVector(local_potential->Map(), numev)));
        std::vector< Anasazi::Value<double> > Ritz_values;
        for(int i=0; i<numev; i++){
            Ritz_values.push_back(Anasazi::Value<double>());
        }
        timer->start("Time to diagonalize");
        {
            ParameterList MyPL;
            set_ParameterList(&MyPL);

            //start_time = clock();
            if(eigen_solver == "LOBPCG"){
                Anasazi::modifiedLOBPCGSolMgr<double, MV, OP> MySolverMan(MyProblem, MyPL);
                returnCode = MySolverMan.solve(Ritz_vectors, Ritz_values);
            }
            #ifdef USE_EX_DIAG
            else if( eigen_solver == "BlockKrylovSchur"){
                Anasazi::modifiedBlockKrylovSchurSolMgr<double, MV, OP> MySolverMan(MyProblem, MyPL);
                returnCode = MySolverMan.solve(Ritz_vectors, Ritz_values);
            }
            else if(eigen_solver == "BlockDavidson"){
                Anasazi::modifiedBlockDavidsonSolMgr<double, MV, OP> MySolverMan(MyProblem, MyPL);
                returnCode = MySolverMan.solve(Ritz_vectors, Ritz_values);
            }
            #endif
            else{
                Verbose::all() << "There is no " << eigen_solver << "eigen solver "<<std::endl;
                exit(-1);
            }

            Parallel_Manager::info().all_barrier();

            if (returnCode == Anasazi::Unconverged){
                Verbose::single(Verbose::Normal) << " Failed to converge." <<"\n";
            }
        }

        timer->end("Time to diagonalize");

        Verbose::set_numformat(Verbose::Time);
        //Verbose::single().precision(8);
        Verbose::single() << " Time to solve: " << std::fixed << timer->get_elapsed_time("Time to diagonalize",-1) << " s" << std::endl;
        //Verbose::single()<< Verbose::defaultfloat ;
        //Verbose::single().precision(6);

        //solution = MyProblem->getSolution();
        RitzValues.clear();
        RitzVectors = rcp(new Epetra_MultiVector(*Ritz_vectors));
        double* norm2 = new double[numev]();

        RitzVectors->Norm2(norm2);


        for(int i=0; i<numev; i++){
            RitzValues.push_back(Ritz_values[i].realpart);
        }
        delete[] norm2;


        Verbose::single()<< "#---------------------------------------------------------------------------" <<"\n";

        timer->print(Verbose::single(Verbose::Simple));
        //Verbose::single() << endl;
        if( returnCode == Anasazi::Converged ){
            return true;

        } else {
            return false;
        }
    }
    else{
        std::cout << "gpu_enable + not Making hamiltonian matrix != possible " <<std::endl;
        exit(-1);
    }
}

void Pure_Diagonalize::initializer(Teuchos::RCP<Epetra_MultiVector>& eigenvectors,
        Teuchos::RCP<Epetra_MultiVector> initial_eigenvector,
        const Epetra_BlockMap& map, int numev,
        Teuchos::RCP<Epetra_CrsMatrix> overlap_matrix){


    // Initialize block size
    if(block_size<1){
        Verbose::single() << "Pure_Diagonalize::Given block size is not valid (" << block_size<< "). It will be changed to " << numev<< std::endl;
        block_size = numev;
    }

    if(block_size+max_locked < numev){
        Verbose::single() << "Pure_Diagonalize:: BlockSize+MaxLocked < EigenValues" <<std::endl;
        Verbose::single() << "MaxLoced is changed to " <<  numev-block_size <<std::endl;
        max_locked = numev-block_size ;
    }

    // set up initial eigenvector
    if(initial_eigenvector == null ){
        Verbose::single(Verbose::Detail) << "# of eigenvalues: " << numev << "\t block size: " << block_size <<std::endl;
        Verbose::single() <<  " Initial vector was randomly initialized" << std::endl;
        eigenvectors = rcp(new Epetra_MultiVector(map, block_size));
        eigenvectors->Random();
    }
    else{
        Verbose::single(Verbose::Detail) << "initial eigenvector size:  " << initial_eigenvector->NumVectors() << "" << std::endl;
        eigenvectors = rcp( new Epetra_MultiVector(map, block_size) ); // initialize vector as zeros
        /* shchoi
           if( block_size < initial_eigenvector -> NumVectors() ){
           Verbose::single() << "Pure_Diagonalize: block size (" << block_size << ") smaller than guess vector size (" << initial_eigenvector -> NumVectors() << ")" << std::endl;
           }*/
        double* normInf = new double[initial_eigenvector->NumVectors()];
        initial_eigenvector->NormInf(normInf);
        //        for (int i =0; i < min(initial_eigenvector->NumVectors(), block_size); ++i){ //shchoi

        Verbose::single(Verbose::Detail) << "eigenvectors size: " << eigenvectors->NumVectors() <<std::endl;
        Verbose::single(Verbose::Detail) << "initial eigenvector size "<<initial_eigenvector->NumVectors() <<std::endl;
        for (int i =0; i < std::min(block_size, initial_eigenvector->NumVectors()); ++i){
            if (std::isnan(normInf[i]) ){
                Verbose::single(Verbose::Normal) << "Pure_Diagonalize::" << i <<"th initial vector is randomly initialized (NaN found)" <<  std::endl;
                eigenvectors->operator()(i)->Random();
            }
            else if (normInf[i]<1.0e-30 ){
                Verbose::single(Verbose::Normal) << "Pure_Diagonalize::" << i <<"th initial vector is randomly initialized (empty vector found)" <<  std::endl;
                eigenvectors->operator()(i)->Random();
            }
            else{
                eigenvectors->operator()(i)->Update(1.0, *initial_eigenvector->operator()(i),1.0);
            }
        }
        delete[] normInf;

        for(int i =initial_eigenvector->NumVectors(); i< block_size; i++){
            Verbose::single(Verbose::Normal)<<  "Pure_Diagonalize::" << i <<"th initial vector is randomly initialized  " <<  std::endl;
            eigenvectors->operator()(i)->Random();
        }
    }

    // Normalization
    Verbose::single(Verbose::Detail) << "Pure_Diagonalize::Normalization " << std::endl;

    //Verbose::single()<<  "----------------Current second norm " << std::endl;
    double* norm2 = new double [eigenvectors->NumVectors()];
    if( overlap_matrix == Teuchos::null ){
        eigenvectors->Norm2(norm2);
    } else {
        RCP<Epetra_MultiVector> Mx = rcp( new Epetra_MultiVector( eigenvectors->Map(), eigenvectors->NumVectors() ) );
        overlap_matrix -> Apply( *eigenvectors, *Mx );
        Mx -> Dot( *eigenvectors, norm2);
    }
    for(int i=0; i<eigenvectors->NumVectors(); i++){
        if (std::fabs(norm2[i]-1.0)>1e-6){
            Verbose::single(Verbose::Detail) << "Normalization condition for "<< i << "th eigenvector is seriously broken." <<std::endl;
            Verbose::single(Verbose::Detail) << "current norm : " << norm2[i] <<std::endl;
        }
        eigenvectors->operator()(i)->Scale(1/sqrt(norm2[i]));
    }
    delete[] norm2;
    Verbose::single(Verbose::Normal) << "\n -------------------------" << std::endl;
    Verbose::single(Verbose::Normal) <<  " Initial vector for diagonalization is normalized." << std::endl;
}

void Pure_Diagonalize::redistribute(Teuchos::RCP<Epetra_CrsMatrix> hamiltonian, Teuchos::RCP<Epetra_CrsMatrix> overlap, Teuchos::RCP<Epetra_MultiVector> eigenvectors,
                    Teuchos::RCP<Epetra_CrsMatrix>& new_hamiltonian, Teuchos::RCP<Epetra_CrsMatrix>& new_overlap, Teuchos::RCP<Epetra_MultiVector>& new_eigenvectors
                    ){
    int max,min;
    int nVal = hamiltonian->NumMyNonzeros();
    hamiltonian->Comm().MaxAll(&nVal, &max, 1 );
    hamiltonian->Comm().MinAll(&nVal, &min, 1 );
    Verbose::single(Verbose::Normal) << "original max/min nonzero elements:\t"<< max << "/" << min <<std::endl;
    if(redistribution){
        timer->start("redistribution");

        if(partitioner==Teuchos::null and redistributor ==Teuchos::null){
            Teuchos::ParameterList params;
            params.set("PARTITIONING METHOD",redistribute_method);
            params.set("STRUCTURALLY SYMMETRIC", "yes");
            params.set("BALANCE OBJECTIVE","nonzeros");
            params.set("IMBALANCE TOL",String_Util::to_string(balancing_tol));
            //Teuchos::ParameterList& sublist = params.sublist("Zoltan");
            //sublist.set("PHG_CUT_OBJECTIVE","HYPEREDGES");

            partitioner = Teuchos::rcp(new Partitioner(&hamiltonian->Graph(),params));
            redistributor = Teuchos::rcp(new Redistributor(partitioner) );
        }
        new_hamiltonian = redistributor->redistribute(*hamiltonian);
        new_eigenvectors = redistributor->redistribute(*eigenvectors);
        if(overlap!=Teuchos::null) new_overlap= redistributor->redistribute(*overlap);
        else new_overlap = overlap;
        nVal = new_hamiltonian->NumMyNonzeros();
        new_hamiltonian->Comm().MaxAll(&nVal, &max, 1 );
        new_hamiltonian->Comm().MinAll(&nVal, &min, 1 );
        Verbose::single(Verbose::Normal) << "new max/min:\t"<< max << "/" << min <<std::endl;

        Verbose::single(Verbose::Normal) << "Partitioning is done" <<std::endl;
        timer->end("redistribution");
    }
    else{
        new_hamiltonian = hamiltonian;
        new_overlap = overlap;
        new_eigenvectors = eigenvectors;
    }

    return;
}
/*
void Pure_Diagonalize::reverse_redistribute(Teuchos::RCP<Epetra_MultiVector> eigenvectors,Teuchos::RCP<Epetra_MultiVector>& new_eigenvectors){
    if(reverse_partitioner==Teuchos::null and reverse_redistributor ==Teuchos::null){
        Teuchos::ParameterList params;
        params.set("STRUCTURALLY SYMMETRIC", "yes");
        params.set("BALANCE OBJECTIVE","nonzeros");
        params.set("IMBALANCE TOL","1.1");
        reverse_partitioner = Teuchos::rcp(new Partitioner(&hamiltonian->Graph(),params));
        reverse_redistributor = Teuchos::rcp(new Redistributor(partitioner) );
    }

    return;
}
*/
template< class MV, class OP>
void Pure_Diagonalize::set_prec(
        Teuchos::RCP < Anasazi::BasicEigenproblem<double,MV,OP> >& MyProblem, Teuchos::RCP<OP> matrix){
    return;
}
template< >
void Pure_Diagonalize::set_prec<Epetra_MultiVector,Epetra_Operator>(
        Teuchos::RCP < Anasazi::BasicEigenproblem<double,Epetra_MultiVector,Epetra_Operator> >& MyProblem,
        Teuchos::RCP<Epetra_Operator> matrix)
{

    if(precond_type =="None"){
        Verbose::single(Verbose::Normal) <<  " Preconditioner is not used." << "\n";
        return;
    }
    if(matrix==Teuchos::null){
        Verbose::all() <<  " Something wrong in preconditioning" << std::endl;
        exit(-1);
    }

    auto new_matrix  = Teuchos::rcp_dynamic_cast<Epetra_CrsMatrix>(matrix);

    RCP<ML_Epetra::MultiLevelPreconditioner> ml_prec;
    //RCP<Epetra_CrsMatrix> PrecOp;
    ParameterList precParams;
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    if(!is_preconditioner_constructed){
//        is_preconditioner_constructed = true;
        auto timer = Teuchos::rcp(new Time_Measure() );
        timer->start("Construct Preconditioner");
        Verbose::single(Verbose::Normal) <<  " Preconditioner: " << precond_type << std::endl;
        if (precond_type=="ml"){
            ML_Epetra::SetDefaults("DD",precParams);
            precParams.set("smoother: pre or post", "post");
            //            precParams.set("PDE equations", 1);
            precParams.set("smoother: type","IFPACK");
            precParams.set("smoother: ifpack type", "ILU");
            //precParams.set("smoother: ifpack overlap", 1);
            //precParams.sublist("smoother: ifpack list").set("fact: level-of-fill", 5);
            ml_prec =rcp(new ML_Epetra::MultiLevelPreconditioner(*new_matrix.get(), precParams, true));

            MyProblem->setPrec(ml_prec);
        }
        else if(precond_type == "additive_block_jacobi"){
            int OverlapProcs = 2;
            int OverlapBlocks = 0;

            prec = rcp(new Ifpack_AdditiveSchwarz<Ifpack_BlockRelaxation<Ifpack_SparseContainer<Ifpack_Amesos> > >(new_matrix.get(), OverlapProcs));

            precParams.set("partitioner: overlap", OverlapBlocks);
            //precParams.set("partitioner: type", "metis");
            //precParams.set("partitioner: type", "greedy");
            //precParams.set("partitioner: local parts", 4);
            precParams.set("amesos: solver type", "Amesos Dscpack");
        }
        else if(precond_type=="block_jacobi"){
            int OverlapBlocks = 0;
            prec = rcp(new Ifpack_BlockRelaxation<Ifpack_SparseContainer<Ifpack_Amesos> >(new_matrix.get()));

            precParams.set("partitioner: overlap", OverlapBlocks);
            //precParams.set("partitioner: type", "metis");
            //precParams.set("partitioner: type", "greedy");
            //precParams.set("partitioner: local parts", 4);
            precParams.set("amesos: solver type", "Amesos Dscpack");
        }
        else if(precond_type=="point_jacobi"){
            int OverlapBlocks = 0;
            prec = rcp(new Ifpack_PointRelaxation(new_matrix.get()));

            precParams.set("partitioner: overlap", OverlapBlocks);
            //precParams.set("partitioner: type", "metis");
            //precParams.set("partitioner: type", "greedy");
            //precParams.set("partitioner: local parts", 4);
            precParams.set("relaxation: damping parameter", 0.1);
            precParams.set("amesos: solver type", "Amesos Dscpack");
        }
        else{
            Ifpack precFactory;
            int overlapLevel = prec_overlap_level;
            prec = rcp(precFactory.Create(precond_type, new_matrix.get(), overlapLevel));

            precParams.set("fact: drop tolerance",1e-8);
            precParams.set("fact: level-of-fill", 0 );
            precParams.set("relaxation: type", "Gauss-Seidel");
        }
        if(precond_type!="ml"){
            prec->SetParameters(precParams);
            prec->Initialize();
            prec->Compute() ;
        }

        MyProblem->setPrec(prec);

        timer->end("Construct Preconditioner");
        Verbose::single(Verbose::Normal) << "Time to construct preconditioner : " << timer -> get_elapsed_time("Construct Preconditioner", -1) << "s" << std::endl;
    }

    return;
}
template< class MV, class OP>
void Pure_Diagonalize::set_problem(
        Teuchos::RCP < Anasazi::BasicEigenproblem<double,MV,OP> >& MyProblem,
        Teuchos::RCP<OP> matrix,
        Teuchos::RCP<OP> overlap_matrix
        ){
    MyProblem->setHermitian(true);
//    Teuchos::RCP<Epetra_CrsMatrix> new_matrix=matrix;
//    Teuchos::RCP<Epetra_CrsMatrix> new_overlap_matrix=overlap_matrix;
    if( overlap_matrix != null ){
        MyProblem -> setM( overlap_matrix );
    }
    if (matrix!=Teuchos::null){
        set_prec<MV,OP>(MyProblem,matrix);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Inform the eigenproblem that you are finishing passing it information
    //cout << "Pure_Diagonalize:: Setting preconditioner  " << double (end_time - start_time ) / CLOCKS_PER_SEC <<endl;
    bool boolret = MyProblem->setProblem();
    if(boolret != true){
        Verbose::all()<< "Anasazi::BasicEigenproblem::setProblem() returned an error." << std::endl;
        exit(EXIT_FAILURE);
    }//else cout << "setProblem()" << endl;
    //    int verb = Anasazi::Warnings + Anasazi::Errors+ Anasazi::FinalSummary + Anasazi::TimingDetails;
    //    int verb = Anasazi::TimingDetails;
    return;
}
RCP<Epetra_MultiVector> Pure_Diagonalize::get_eigenvectors (){
    return RitzVectors;
    //return solution.Evecs ;
}
std::vector<double > Pure_Diagonalize::get_eigenvalues(){
    return RitzValues;
    //return solution.Evals;
}
void Pure_Diagonalize::set_ParameterList(ParameterList* MyPL){
    int anasazi_verb;
    if(verb == "Simple"){
        //anasazi_verb = Anasazi::FinalSummary;
        anasazi_verb = Anasazi::Errors;
        //Normal user will not want information from anasazi solver
    }
    else if( verb == "Normal"){
        anasazi_verb = Anasazi::FinalSummary + Anasazi::TimingDetails;
    }
    else if( verb == "Debug"){
        anasazi_verb = Anasazi::Errors + Anasazi::Warnings + Anasazi::IterationDetails + Anasazi::OrthoDetails + Anasazi::FinalSummary + Anasazi::TimingDetails + Anasazi::StatusTestDetails + Anasazi::Debug;
    }
    else{
        Verbose::all()<< "Wrong verbosity option. \"Simple\", \"Normal\" or \"Debug\"." << std::endl;
        exit(EXIT_FAILURE);
    }
    MyPL->set( "Verbosity", anasazi_verb );
    //MyPL->set( "Maximum Restarts", 3 );
    MyPL->set( "Which", "SR" );
    //Verbose::single() << numev <<"\t "<< block_size <<std::endl;
    MyPL->set("Block Size", block_size);
    //    MyPL.set( "Num Blocks", parameters->sublist("Diagonalize").get<int>("NumBlocks"));
    MyPL->set( "Num Blocks", block_size);
    //MyPL.set( "Num Blocks", 3*numev/parameters->sublist("Diagonalize").get<int>("BlockSize"));
//    MyPL->set("Maximum Subspace Dimension",max_iter*block_size);
    //MyPL->set("Maximum Subspace Dimension",1000);
    MyPL->set( "Maximum Restarts", max_iter );
    MyPL->set("Maximum Iterations", max_iter);
    MyPL->set("Full Ortho", full_ortho );
    //MyPL->set("Recover", false);
    if( max_locked > 0 ){
        MyPL->set("Use Locking", true);
        MyPL->set( "Locking Tolerance", locking_tolerance );
        MyPL->set( "Max Locked", max_locked);
    } else {
        MyPL->set("Use Locking", false);
    }
    MyPL->set("Convergence Tolerance", tolerance );
    MyPL->set("Relative Convergence Tolerance", true); // Default: true
    MyPL->set("Convergence Norm", "2");
    if(precond_type !="None")
        MyPL->set("usePrec", true);
}


//template void  Pure_Diagonalize::set_problem(Teuchos::RCP<Anasazi::BasicEigenproblem<double, Epetra_MultiVector, Epetra_Operator> >& problem, Teuchos::RCP<Epetra_CrsMatrix> matrix, Teuchos::RCP<Epetra_CrsMatrix> overlap_matrix, bool redistribution,std::string redistribite_method);
