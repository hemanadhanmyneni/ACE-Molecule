class A():
    def test(self):
        print ("A")
    def call(self):
        self.test()

class B(A):
    def test(self):
        print("B")

a = A()
b = B()

a.call()
b.call()
