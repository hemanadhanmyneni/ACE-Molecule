#/bin/env python
import sys
sys.path.append('../../utility/')
import ACEInput
import os
import itertools
from collections import OrderedDict

################# ALL ##################
_SYSTEM_ = "messi"
exe = "../../ace"
mpi_and_omps = None
test_run = False
any_node = None # SARASWATI SHOULD USE THIS
########################################
################ MESSI #################
r1i0n_nodes = [0,1,2,3,4]
intel_nodes = []
amd_nodes = []
########################################

new_args = [
    {
        "TDDFT.Exchange_Correlation.XFunctional": 101,
        "TDDFT.Exchange_Correlation.CFunctional": 130,
        "TDDFT._TDClass": "C",
    },
    {
        "TDDFT.Exchange_Correlation.XFunctional": 101,
        "TDDFT.Exchange_Correlation.CFunctional": 130,
        "TDDFT._TDClass": "ABBA",
    },
    {
        "TDDFT.ExchangeKernel": "HF-EXX",
        "TDDFT.Exchange_Correlation.XCFunctional": 10478,
        "TDDFT.Exchange_Correlation.Gaussian_Potential": ["\"0.27 0.075\"", "\"0.18 0.006\""],
        "TDDFT.xc_kernel_without_EXX.Exchange_Correlation.XCFunctional": 10478,
        "TDDFT.xc_kernel_without_EXX.Exchange_Correlation.AutoEXXPortion": 0,
        "TDDFT.xc_kernel_without_EXX.Exchange_Correlation.Gaussian_potential": ["\"0.27 0.075\"", "\"0.18 0.006\""],
    },
    {
        "TDDFT.ExchangeKernel": "HF-EXX",
        "TDDFT.Exchange_Correlation.XFunctional": ["\"101 0.5\"", "\"-12 0.5\""],
        "TDDFT.Exchange_Correlation.CFunctional": 130,
        "TDDFT.xc_kernel_without_EXX.Exchange_Correlation.XFunctional": "\"101 0.5\"",
        "TDDFT.xc_kernel_without_EXX.Exchange_Correlation.CFunctional": 130,
    },
    {
        "TDDFT.ExchangeKernel": "HF-EXX",
        "TDDFT.DeltaCorrection": 2,
        "TDDFT.Exchange_Correlation.XFunctional": ["\"101 0.5\"", "\"-12 0.5\""],
        "TDDFT.Exchange_Correlation.CFunctional": 130,
        "TDDFT.xc_kernel_without_EXX.Exchange_Correlation.XFunctional": "\"101 0.5\"",
        "TDDFT.xc_kernel_without_EXX.Exchange_Correlation.CFunctional": 130,
    },
]

args_nick = ["benzene.C", "benzene.ABBA", "benzene.LC", "benzene.HYB", "benzene.HYB_diag"]

def initialize_nodes():
    nodes = []
    if any_node:
        nodes = ["1"]
    else:
        for node in r1i0n_nodes:
            nodes.append("r1i0n"+str(node))
        for node in amd_nodes:
            nodes.append("amd"+str(node))
        for node in intel_nodes:
            nodes.append("intel"+str(node))
    return nodes

def initialize_node_procs():
    r1i0n_nodes = range(12)
    intel_nodes = range(15)
    amd_nodes = [0]
    node_procs = {}
    for node in r1i0n_nodes:
        node_procs["r1i0n"+str(node)] = "24"
    for node in amd_nodes:
        node_procs["amd"+str(node)] = "16"
    for node in intel_nodes:
        if int(node) <= 3 or int(node) >= 11:
            node_procs["intel"+str(node)] = "16"
        else:
            node_procs["intel"+str(node)] = "20"
    if any_node:
        node_procs["1"] = str(int(any_node))
    return node_procs

nodes = initialize_nodes()
node_procs = initialize_node_procs()

env_args = {
    "messi": """module purge
module load intel
module load intel_mkl
module load openmpi-1.8.3-intel
module load trilinos-12.8.1_openmp_release_intel_openmpi-1.8.3
module load gsl-1.16_intel_openmpi-1.8.3
module load pcmsolver-1.1.11""",
    "saraswati": """export LD_LIBRARY_PATH=/home/appl/intel/compilers_and_libraries/linux//lib/intel64/:/home/wykgroup/appl/openmpi-1.10.1_build/lib:/home/wykgroup/appl/trilinos-12.8.1_Build_openmpi/lib:/home/wykgroup/appl/gsl-2.3_build/lib:/appl/intel/compilers_and_libraries/linux/mkl/lib/intel64:/home/appl/intel/compilers_and_libraries/linux/bin/intel64//lib/intel64/:/home/wykgroup/appl/gcc-5.3.0_build/lib:/home/wykgroup/appl/gcc-5.3.0_build/lib64:/home/appl/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64:/home/appl/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64_lin:/home/appl/intel/compilers_and_libraries_2017.1.132/linux/mpi/intel64/lib:/home/appl/intel/compilers_and_libraries_2017.1.132/linux/mpi/mic/lib:/home/appl/intel/compilers_and_libraries_2017.1.132/linux/ipp/lib/intel64:/home/appl/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64_lin:/home/appl/intel/compilers_and_libraries_2017.1.132/linux/mkl/lib/intel64_lin:/home/appl/intel/compilers_and_libraries_2017.1.132/linux/tbb/lib/intel64/gcc4.4:/home/appl/intel/debugger_2017/iga/lib:/home/appl/intel/debugger_2017/libipt/intel64/lib:/home/appl/intel/compilers_and_libraries_2017.1.132/linux/daal/lib/intel64_lin:/home/appl/intel/compilers_and_libraries_2017.1.132/linux/daal/../tbb/lib/intel64_lin/gcc4.4
export PATH=/home/wykgroup/appl/openmpi-1.10.1_build/bin:/home/wykgroup/appl/gsl-2.3_build:/home/appl/intel/compilers_and_libraries/linux/mkl/bin:/home/appl/intel/compilers_and_libraries/linux/bin/intel64//bin/intel64/:/home/wykgroup/appl/gcc-5.3.0_build/bin:/usr/bin/:/sbin/:/home/wykgroup/bin/:/home/wykgroup/bin/:.:/usr/local/maui-3.3.1/bin/:/usr/local/maui-3.3.1/sbin/:/home/appl/intel/compilers_and_libraries_2017.1.132/linux/bin/intel64:/home/appl/intel/compilers_and_libraries_2017.1.132/linux/mpi/intel64/bin:/home/appl/intel/debugger_2017/gdb/intel64_mic/bin:/home/appl/scheduler/torque-6.1.0/bin:/home/appl/scheduler/torque-6.1.0/sbin:/usr/lib64/qt-3.3/bin:/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin:/home/wykgroup/bin""",
}

def main():
    names = []
    if len(args_nick) == 0:
        for new_arg in new_args:
            tmp_nick = []
            for key, val in new_arg.iteritems():
                #tmp_nick.append(key.split(".")[-1]+"."+str(val))
                tmp_nick.append(str(val))
            nick += ".".join(tmp_nick)
            args_nick.append(nick)
    inputs = ACEInput.create_inputs("ACE.template", OrderedDict(zip(args_nick, new_args)))

    i = 0
    for name in inputs.keys():
        node = nodes[i%len(nodes)]
        with open("jobscript_"+name+".x", 'w') as fpt:
            #ACEInput.write_jobscript(fpt, name+".inp", name, node)
            if mpi_and_omps is None:
                write_jobscript(fpt, inputs[name], name, node, node_procs[node], exe)
            else:
                write_jobscript(fpt, inputs[name], name, node, node_procs[node], exe, mpi_and_omps[i])
        if not test_run:
            print i, name+":\t",
            sys.stdout.flush()
            os.system("qsub jobscript_"+name+".x")
        i += 1


def write_jobscript(fpt, inp_name, job_name, node_name, node_procs, exe_name = "./ace", mpi_and_omp = None):
    if mpi_and_omp is not None:
        if int(node_procs) != int(mpi_and_omp[0]) * int(mpi_and_omp[1]):
            print "Assigned "+node_procs+" cores to node "+node_name+" but MPI NPROCS ("+str(mpi_and_omp[0])+") * OMP_NUM_THREADS ("+str(mpi_and_omp[1])+") are different!"
    fpt.write("#!/bin/bash\n\n")
    fpt.write("#PBS -N "+job_name+"\n")
    fpt.write('#PBS -l nodes='+node_name+':ppn='+node_procs+'\n')
    fpt.write('#PBS -l walltime=500:00:00\n')
    fpt.write('\n')
    fpt.write('date\n')
    fpt.write('\n')
    fpt.write('export INPUT='+inp_name+'\n')
    fpt.write('export OUTPUT='+job_name+'.log\n')
    fpt.write('export EXEC='+exe_name+'\n')
    if mpi_and_omp is not None:
        fpt.write('export MPI_NPROCS='+str(mpi_and_omp[0])+'\n')
        fpt.write('export OMP_NUM_THREADS='+str(mpi_and_omp[1])+'\n')
    fpt.write('\n')
    fpt.write(env_args["messi"])
    fpt.write('\n\n')
    fpt.write('cd $PBS_O_WORKDIR\n')
    fpt.write('\n')
    fpt.write('echo `cat $PBS_NODEFILE`\n')
    fpt.write('cat $PBS_NODEFILE\n')
    fpt.write('NPROCS=`wc -l < $PBS_NODEFILE`')
    fpt.write('\n')
    if mpi_and_omp is None:
        fpt.write('mpirun -machinefile $PBS_NODEFILE -np $NPROCS $EXEC $PBS_O_WORKDIR/$INPUT > $PBS_O_WORKDIR/$OUTPUT\n')
    else:
        fpt.write('mpirun -machinefile $PBS_NODEFILE -np $MPI_NPROCS $EXEC $PBS_O_WORKDIR/$INPUT > $PBS_O_WORKDIR/$OUTPUT\n')
    fpt.write('\n')
    fpt.write('date')

if __name__ == "__main__":
    main()
