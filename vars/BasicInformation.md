## **BasicInformation**

 Parameters in this section govern mesh information, spin state and a geometry of target system.

#### **Subsection List**
  +  Pseudopotential (Mandatory)

### **VerboseLevel**

> #### **Description**
> Variable set level of verbosity during the calculation
>
> #### **Type**
>  int
>
> #### **Possible Options**
>+ 0: Simple - Print only simple outputs [default]
>+ 1: Normal - Print auxiliary outputs in addition to Simple. These auxiliary outputs include restating input parameters and resource used by technical routines.
>+ 2: Detail - Extremely verbose output which are not intended to be understood by non-developers.

### **Label**
> #### **Description**
> When additional results are generated during the calculation, filenames start from *Label* 
> #### **Type**
> string


### **Mode**
>#### **Description**
>Decides what kind of calculation is to be performed.
>
> #### **Type**
> string
>
> #### **Possible Options**
>+ Auto: Calculate anything specified in the input block in the specified order.
>+ Sp: Single point calculation will be performed. Calculate using input block Guess and then Scf.
>+ SpTDDFT: First, single point calculation will be performed, then TDDFT will start using orbitals from Sp calculation. For more information, please refer TDDFT section. Calculate using input block Guess, then Scf, and then TDDFT.
>+ TDDFT: TDDFT calculation will be performed. Cube guess with Info provided is highly recommended. Calculate using input block Guess and then TDDFT.
>+ Opt: Geometry optimization will be performed. This option is currently under test.

### **GeometryFilename**
> #### **Description**
> Name of the input geometry file. Now xyz and pdb fileformats are supported (Previsouly, name of this parameter is Geomtry_Filename but now old name does not work)
> #### **Type**
> string

### **GeometryFormat**
> #### **Description**
> Input geometry file format.(Previsouly, name of this parameter is Geomtry_Format but now old name does not work)
> #### **Type**
> string
> #### **Possible Options**
> + xyz: xyz format of geometry file.
> + pdb: pdb format of geometry file.

### **Grid**
> #### **Description**
> This is a keyword to select shape of simulation space.
> #### **Type**
> string
>
> #### **Possible Options**
> + Basic: The shape of simulation box will be rectangular. The size of the simulation box is governed by *Cell*
> + Sphere: The shape of simulation box will be sphere. The radius of the simulation sphere is governed by *Cell*
> + Atoms:  The simulation box is composed of points whose distance from atom is smaller than a certain value.(*Radius* see below for details) If you do not use *AbsoluteRadius* the radius will be set as *Radius* times of predefined value (van der Waals radii).
>

### **Type**
> #### **Description**
> This variable set how to construct mesh 
> #### **Type**
> string
> #### **Possible Options**
> + Points: Based on given *Cell* & *Points* information, mesh is generated.
> + Scaling: Based on given *Cell* & *Scaling* information, mesh is generated.
> + Cube: Share mesh with designated Cube file whose filename is set in *BasisCubeFilename*.

### **Cell**
> #### **Description**
> This indicates the size of cubic simulation domain in Bohr unit 
> When *Grid* is Basic, 2 times *Cell* indicate the length of simulation box.
> Otherwise, This parameter has no effect on the calculation
> #### **Type** 
> (positive) float 

### **CellDimensionX**
> #### **Description**
> This indicates the size of z axis of simulation domain in Bohr unit <br>
> When *Grid* is Basic, 2 times *Cell* indicate the length of simulation box. If *Cell* is set, this parameter does not work. <br>
> Otherwise, This parameter has no effect on the calculation <br>
> #### **Type** 
> (positive) float 

### **CellDimensionY**
> #### **Description**
> This indicates the size of z axis of simulation domain in Bohr unit  <br>
> When *Grid* is Basic, 2 times *Cell* indicate the length of simulation box. If *Cell* is set, this parameter does not work. <br>
> Otherwise, This parameter has no effect on the calculation <br>
> #### **Type** 
> (positive) float 

### **CellDimensionZ**
> #### **Description**
> This indicates the size of z axis of simulation domain in Bohr unit  <br>
> When *Grid* is Basic, 2 times *Cell* indicate the length of simulation box. If *Cell* is set, this parameter does not work. <br>
> Otherwise, This parameter has no effect on the calculation
> #### **Type** 
> (positive) float 

### **Points**
> #### **Description**
> Defines the number of grid points in all three dimension.\
> By this variable, *PointX*, *PointY*, and *PointZ* are automatically define by its value.\
> Affects basis size.
>
> #### **Type**
> (positive) int 

### **PointX**
> #### **Description**
> Defines the number of grid points in x dimension. If *Points* is set, this parameter is ignored.
>
> #### **Type**
> (positive) int

### **PointY**
> #### **Description**
> Defines the number of grid points in y dimension. If *Points* is set, this parameter is ignored.
> 
> #### **Type**
> (positive) int

### **PointZ**
> #### **Description**
> Defines the number of grid points in z dimension. If *Points* is set, this parameter is ignored.
> #### **Type**
> (positive) int 

### **Scaling** 
> #### **Description**
> This parameter defines the distance between the grid points in all three dimension in Bohr unit
> #### **Type**
> (positive) float

### **ScalingX**
> #### **Description**
> Defines the distance between the grid points in x dimension.\
> Affects basis size.\
> Should be written in Bohr unit.
> #### **Type**
> (positive) float

### **ScalingY**
> #### **Description**
> Defines the distance between the grid points in y dimension.\
> Affects basis size.\
> Should be written in Bohr unit.
> #### **Type**
> (positive) float

### **ScalingZ**
> #### **Description**
> Defines the distance between the grid points in z dimension.\
> Affects basis size.\
> Should be written in Bohr unit.
> #### **Type**
> (positive) float 

### **Radius**
> #### **Description**
> When *Grid* is set as Atoms, then the radius of simulation spheres need to be set. If *AbsoluteRadius* is set as zero, radius of each simulation sphere become covalent radius of atoms times *Radius* value. Otherwise, *Radius* value become radius in Angstrom unit
> #### **Type**
> (positive) float 

### **AbsoluteRadius**
>#### **Description**
> This parameter determines meaning of Radius; if AbsolutRadius is set as non-zero value, *Radius* become radius of sphere itslef (unit Angstrom) otherwise, the radius become *Radius* times covalent radii 
>
>#### **Type** 
> int
>
>#### **Possible Options**
>+ 0: radius of simulation sphere is set as *Radius* times covalent radii
>+ Otherwise: *Radius* values become radius of simulation sphere (unit: Angstrom) 
 
### **StoreCoreHamiltonian**
> #### **Description**
> This parameter determines whether core Hamiltonian matrix is stored in crs format or not.
>
> #### **Type**
> int
>
> #### **Possible Options**
> + 0: The core Hamiltonian matrix is not stored on a memory in crs format
> + Otherwise: The core Hamiltonian matrix is stored
>

### **BasisCubeFilename**
> #### **Description**
> Set simulation box as specified in this cube file.
> #### **Type**  
> string


### **NumElectrons**
> #### **Description**
> Number of electrons.
> #### **Type**
> (positive) float


### **Centered**
> #### **Description**
> Decides whether the center of mass of the input molecule to be translated to the simulation box origin.
> #### **Type**
> int
> #### **Possible Options**
> 0: Do not translate the molecule. [Defalut]
> 1: The center of mass will be translated to the origin.

### **ShallowCopyOrbitals**
> #### **Description**
> Shallow copy for orbitals
> #### **Type** 
> int
> #### **Possible Options**
> 0: Use deep copy [Default]
> 1: Use shallow copy. Less memory is used.

### **Polarize**
> #### **Description**
> Decides whether the spin-polarized or spin-restricted calculation to be performed.
> #### **Type**
> int
> #### **Possible Options**
> 0: Performs spin-restricted calculation.
> 1: Performs spin-polarized calculation.

### **SpinMultiplicity**
> #### **Description**
> Spin multipliciy of the moleulces. 
> 1.0 for singlet, 2.0 for doublet, etc.
> #### **Type**
> float

### **ForceCalculation**
> #### **Description**
> Decides the calculation of atomic forces after single point calculation.
> #### **Type**
> int 
> #### **Possible Options**
> No: Do not calculate atomic forces. [Default]
> Yes: Calculate atomic forces.
### **ForceDerivative**
> #### **Description**
> Decides method to evaluate force 
> #### **Possible Options**
> #### **Type**
> string
> Potential: 
> Orbital: 
### **Basis**
> #### **Description**
> Decides the basis function.
> #### **Type**
> string 
> #### **Possible Options**
> Sinc: Basis function will be used as basis function.
> FiniteDifference : Finite difference method will be used.

### **KineticMatrix**
> #### **Description**
> Decides basis for kinetic matrix.
> #### **Type**
> string
> #### **Possible Options**
> Finite_Difference:  Use finite difference method to construct the kinetic matrix. Works only with Sinc basis set. If you use this option, you should specify *DerivativesOrder*

### **DerivativesOrder**
> #### **Description**
> Decides derivative order for calculating kinetic matrix. Only relevant if *KineticMatrix* or *Basis* is set to Finite_Difference.
> #### **Type**
> int 
> #### **Possible Options**
>+ 3:  3-points central finite difference coefficients will be used. 
>+ 5:  5-points central finite difference coefficients will be used. 
>+ 7:  7-points central finite difference coefficients will be used. 
>+ 9:  9-points central finite difference coefficients will be used. 
>+ 11: 11-points central finite difference coefficients will be used. 
>+ 13: 13-points central finite difference coefficients will be used. 
>+ 21: 21-points central finite difference coefficients will be used. 

### **UseCubicBaseOnly**
> #### **Description**
> This option is relevant only if the Grid variable is set to Atoms. When reading cube file, this option is sometimes necessary.
> #### **Type**
> int
> #### **Possible Options**
>+ 0: Allow only cubic base grid.
>+ Otherwise: Allow non-cubic base grid. [Default] 

### **AllowOddPoints**
> ##### **Description**
> This parameter determines whether odd or even number of grid points will be used.
> #### **Type**
> int
> #### **Possible Options**
>+ 0: Use even number of grid points for each axis. [Default]
>+ Otherwise: Allow an odd number of points.

