## **TDDFT**
 This section governes parameters related to TDDFT calculations.

#### **Subsection List**
  +  ExchangeCorrelation (Mandatory)
  +  OrbitalInfo (Mandatory if ExchangeKernel is HF_EXX) \
 Contains exchange correlation functional which used to compute the orbitals.\
 Necessary if ExchangeKernel is HF_EXX.\
 Should contain ExchangeCorrelation as subsection.

### **NumberOfStates**
> #### **Description**
> This varible controls the number of excitations computed by TDDFT.
> #### **Type** 
> (positive)int

### **TheoryLevel**
> #### **Description**
> This parameter determines the way to solve TDDFT.
> #### **Type** 
> string
> #### **Possible Options**
> + Casida: It gives oscillator_strength and negative excitations are considered. [default]
> + TammDancoff: Negative excitations are ignored.


### **SortOrbital**
> #### **Description**
> This parameter controls how to print components of
> #### **Type** 
> string
> #### **Possible Options**
> + Order: Print top few components. The number is given as MaximumOrder. [default]
> + Tolerance: Print out all components that are bigger than given criteria.

>
### **MaximumOrder**
> #### **Description**
> This determines the number of printed orbital pairs. This keyword works only with SortOrbital=Sorting case.
> #### **Type** 
> int


### **OrbitalTolerance**
> #### **Description**
> If the portion of certain orbital is larger than this critera, it will be printed.
> #### **Type** 
> float


### **GradientMatrix**
> #### **Description**
> This parameter determines how to obtain the gradients of density and orbital.
> #### **Type** 
> string
> #### **Possible Options**
> + Finite_Difference: Use the finite difference method to obtain the gradient.

### **DerivativesOrder**
> #### **Description**
> Decides derivative order for calculating GradientMatrix.\
> Only relevant when GradientMatrix is Finite_Difference.
> #### **Type** 
> int

### **ExchangeKernel**
> #### **Description**
> Determines the exchange kernel for exact exchange.
> #### **Type** 
> string
> #### **Possible Options**
> + PGG: PGG kernel. [default]
> + HF or HF_EXX: Perform TDHF or TD-HFKS calculation scheme like J. Chem. Phys. 134, 034120 (2011), TDHF(EXX). Further description can be found in DeltaCorrection.


### **DeltaCorrection**
> #### **Type** 
> int
> #### **Description**
> Specifies eigenvalue correction term. Only relevant if ExchangeKernel is HF or HF_EXX.
> #### **Possible Options**
> + 1: Calculate as J. Chem. Phys. 134, 034120 (2011). [default]
> + 2: Include only diagonal elements for occupied orbital eigenvalue correction term. Not recommended.
> + 3: Include only diagonal elements for occupied orbital eigenvalue correction term. Not implemented yet.
> + 0: Do not include eigenvalue correction term. Identical to ExchangeKernel==HF and fallbacks to it.

### **_TDClass**
> #### **Type** 
> string
> #### **Description**
> Debug input. Chooses the TDDFT calculation class. \
> If not specified (normal behavior), automatically choose appropreate routine. \
> If specified class cannot compute desired theory, calculation stops.
> #### **Possible Options**
> + C: \
> Invoke Casida equation $ CZ = -\Omega^2 Z $ computation routine. HF and KS-CI exchange kernels are not implemented yet.\
> This is chosen if the TheoryLevel is Casida and EXX exchange kernel is not HF or KS-CI.
> + ABBAL: \
> Invoke TDDFT equation $ (A* B* | B A) (X Y) = -\Omega (X Y) $ computation routine.\
> This is chosen if the TheoryLevel is Casida and HF or KS-CI exchange kernel is used.\
> This class should be removed (with this input) when HF, KS-CI casida form is implemented.
> + TDA:\
> Invoke Tamm-Dancoff equation computation routine.\ 
> This is chosen is the TheoryLevel is TDA.



### **Gradient**
> #### **Description**
> Determine how to calculate gradient of pair density.\
> #### **Type** 
> int
> #### **Possible Options**
> + 0: gradient of pair density is calculated using gradient matrix [default]
> + Otherwise: gradient of pair density is calculated using saved orbital gradient.


### **Root**
> #### **Description**
> Does not count excitations from the orbitals lower than the orbitals with this index.\
> Root-1 orbitals will be ignored.
> #### **Type** 
> int


### **MemorySaveMode**
> #### **Description**
> keeping result of kernel integration loaded on memory for acceleration
> #### **Type** 
> int
> #### **Possible Options**
> + 0: calculation of iajb-pair will use higher memory and lower communication time. [default] 
> + Otherwise: it will use lower memory but needs more communication time.

