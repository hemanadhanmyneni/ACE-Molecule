## **Pseudopotential**
 This section set type of pseudopotential/PAW method.

### **Pseudopotential**
>#### **Description**
> Decides Pseudopotential/PAW method that will be used.
>#### **Type**
> int
> #### **Possible Options**
>+ 1: Pseudopotential using KB projector. [default]
>+ 3: PAW method.

### **Format**
> #### **Description**
> Decides the input format for the Pseudopotential/PAW method.
> #### **Type**
> string
> #### **Possible Options**
> +xml: PAW-XML format (https://wiki.fysik.dtu.dk/gpaw/setups/pawxml.html#pawxml).\
> This is the only option for PAW method, and exclusive to PAW method.
> + upf: UPF format (http://www.quantum-espresso.org/pseudopotentials/unified-pseudopotential-format).
> + hgh: HGH format (PRB, 58, 3641 (1998)). [default]

### **HghFormat**
> #### **Description**
> The type of HGH pseudopotential for calculations.
> #### **Type**
> string
> #### **Possible Options**
> + HGH: HGH type of pseudopotential (PRB, 58, 3641 (1998)).
> + Internal: Use hard-coded values, from same reference. [default]

### **PSFilenames**
> #### **Description**
> Path to the input pseudopotential/PAW data file. \
> PSFilenames should be given for all atoms to be calculated, and should be given in the order of increasing atomic number. \
> This variable might be inferred from PSFilePath and PSFileSuffix, if those two variables exist and PSFilenames are absent
> #### **Type**
> string

### **PSFilePath**
> #### **Description**
> Path to the directory that contains pseudopotential/PAW data file. \
> This is used to infer PSFilenames. \
> [PSFilenames] = [PSFilePath]/[Atom Symbol][PSFileSuffix]
> #### **Type**
> string

### **PSFileSuffix**
> #### **Description**
> This is used in conjunction to PSFilePath to infer PSFilenames. \
> [PSFilenames] = [PSFilePath]/[Atom Symbol][PSFileSuffix]
> #### **Type**
> string

### **UsingDoubleGrid**
> #### **Description**
> Optional argument. Exclusive to UPF and PAW pseudopotentials. \
> Use supersampling methods for accurate nuclear potential calculations.
>
> Overrides UsingFiltering.
> #### **Type**
> int
> #### **Possible Options**
> + 0: Do not use supersampling method. [default]
> + 1: Use supersampling method.

### **UsingFiltering**
> #### **Description**
> Optional argument. Exclusive to UPF pseudopotentials. \
> Use Fourier filtering method for anti-aliasing of nuclear potential representation.
> You may rafer DOI [10.1063/1.4942925] for detailed explanation.
>
> Overrided by UsingDoubleGrid.
> #### **Type**
> int
> #### **Possible Options**
> + 0: Do not use Fourier filtering method [default]
> + 1: Use Fourier filtering method.

### **NonlocalRmax**
> #### **Description**
> Optional argument. Used with both supersampling and Fourier filtering.<br>
> Decides the outer sampling region.<br>
> r_out = [NonlocalRmax] \times r_in
> Default value: 1.0
> #### **Type**
> float

### **FilterType**
> #### ** Description**
> Optional argument. Exclusive to PAW. Ignored if [FineProj] <= 1.<br>
> Decides the supersampling filter type.
> #### **Type**
> string
> #### **Possible Options**
> + Sinc: Use sinc function as supersampling filter.[default]
> + Lagrange: Use Lagrange function as supersampling filter. Currently, this option does not work with PAW method.

### **GammaLocal**
> #### ** Description**
> Optional argument. Exclusive to filtering.
> Decides cutoff radius for Bessel transformation of local potential from real space to reciprocal space
> Default value: 2.0
> cutoff_r = r_short * GammaLocal
> #### **Type**
> double

### **GammaNonlocal**
> #### ** Description**
> Optional argument. Exclusive to filtering.
> Decides cutoff radius for Bessel transformation of nonlocal potential from real space to reciprocal space
> Default value: 2.0
> cutoff_r = r_short * GammaNonlocal
> #### **Type**
> double

### **AlphaLocal**
> #### ** Description**
> Optional argument. Exclusive to filtering.
> Decides cutoff frequency for Bessel transformation of local potential from reciprocal space to real space
> Default value: 1.1
> q_cut = q_max / AlphaLocal
> #### **Type**
> double

### **AlphaNonlocal**
> #### ** Description**
> Optional argument. Exclusive to filtering.
> Decides cutoff frequency for Bessel transformation of nonlocal potential from reciprocal space to real space
> Default value: 1.1
> q_cut = q_max / AlphaNonlocal
> #### **Type**
> double

### **Eta**
> #### ** Description**
> Optional argument. Exclusive to filtering.
> Decides constant k which determine mask function exp(-k*x^2).
> Default value: q_cut*cutoff_r
> #### **Type**
> double

### **FineDimension**
> #### **Description**
> Optional argument. Exclusive to UPF and PAW pseudopotentials when UsingDoubleGrid is 1. \
> Controls upsampling rate.
> #### **Type**
> int

### **Lmax**
> #### ** Description**
> Optional argument. Exclusive to PAW.<br>
> Decides the maximum angular momentum to expand PAW partical wave.<br>
> Negative value sets the program includes all angular momentum values.<br>
> Changing this value critically affects PAW Hamiltonian correction calculation time, and may affects the total energy by order of 0.1 mHa for small molecule.
> Default value: -1
> #### **Type**
> int

### **OptimizeVectors**
> #### ** Description**
> Optional argument. Exclusive to KB projector. \
> Decides whether the local pseudopotential and nonlocal projector functions to be deleted after the Hamiltonian construction. \
> This makes less memory, but will consume additional considerable time if supersampling enabled and calculating force.  
> #### **Type**
> int
> #### **Possible Options**
>+ 0: Do not delete pseudopotential vectors. [Defalut value]
>+ 1: Delete pseudopotential vectors.

### **OverlapCutoff**
> #### ** Description**
> Controls PAW overlap matrix cutoff. Default is 1E-7, corresponds to ~1E-6 Ha energy difference. \
> Only used when PAW is selected. \
> Recommend to set this value to 0.1 $\times$ [Energy Accuracy].
> #### **Type**
> float

### **HamiltonianCutoff**
> #### ** Description**
> Controls PAW overlap matrix cutoff. Default is 1E-7, corresponds to ~1E-6 Ha energy difference. \
> Only used when PAW is selected. \
> Recommend to set this value to 0.1 $\times$ [Energy Accuracy].
> #### **Type**
> float

### **OccupancyOutput**
> #### ** Description**
> Controls PAW partial wave occupancy output, defined as the square of the dot product between orbital and PAW proejction operator.
> #### **Type**
> int
> #### **Possible Option**
> + 0: Does not print PAW partial wave occupancy at all.
> + 1: Print the sum of PAW partial wave occupancy over all orbitals.
> + 2: Print the PAW partial wave occupancy for all orbitals and atomic partial wave combinations. [default]

