
Variable Output
Type subsection
Section Guess
Description
 Optional section. See Section Output.
END

SectionInfo Opt
SectionDescription
 This section governes geometry optimization method.
END

Variable IterateMaxCycle
Type Int
Section Opt
Description
 Determine the maximum iteration number geometry optimization.<br>
 Default value : 256
END

Variable OptimizationMethod
Type Int
Section Opt
Description
 Determine the method to update atomic positions.
Option 1
 Quasi-Newton method.
Option 2
 Conjugate gradient method.
Option 3
 FIRE(Fast Inertial Relaxation Engine), not implemented yet.
END

Variable PositionMixing
Type Int
Section Opt
Description
 Determine how to update positions when using quasi-Newtonmethod.
Option 1
 Linear Mixing.
Option 2
 GDIIS, not implemented yet.
END

Variable ForceTolerance
Type double
Section Opt
 Optimization Iteration cycle is ended when the maximum force element value is lower than this parameter value.<br>
 Default value is 0.001 Hartree/Bohr.<br>
 Ignored if the value is negative.
END

Variable PositionTolerance
Type double
Section Opt
 Optimization Iteration cycle is ended when the position deviation element value is lower than this parameter value.<br>
 Default value is 0.001 Bohr.<br>
 Ignored if the value is negative.
END


SectionInfo Guess
SectionDescription
 This section governes initial guess methods.
END

Variable Guess_Guess
Type subsection
Section Guess
Description
 Specifies initial guess for Grid cutting guess. Inputs are the same with Guess section.
END

Variable Guess_Scf
Type subsection
Section Guess
Description
 Specifies the SCF calculation rules for Grid cutting guess. Inputs are the same with Scf section.
END

SectionInfo Output
SectionDescription
 This section controls output density, potential, orbitals and geometry.
END

Variable Output
Type subsection
Section Scf
Description
 Optional section. See Section Output.
END

SectionInfo Scf
SectionDescription
 This section governes variables related to Scf
END

Variable Making_Hamiltonian_Matrix
Type integer
Section Scf
Description
 Optional value, it decides whether diagonalization with making hamiltonian matrix or not.<br>
 For default, diagonalization will be done with making hamiltonian matrix.
Option 0
 Diagonalization will be done without making hamiltonian matrix.
Option others
 Diagonalization will be done with making hamiltonian matrix.
END

SectionInfo Exchange_Correlation
SectionDescription
 This section controls the exchange correlation functional for the DFT or TDDFT calculations.<br/>
 Some major exchange-correlation functional can be specified using by "Functional_Name".<br/>
 Otherwise, you can specify exchange-correlation functional using "XCFunctional" keyword, or you can specify exchange and correlation functional seperately using "XFunctional" and "CFunctional" keyword. <br/>
 The hybridization of xc functional can be done with special input. See the "XFunctional" and "CFunctional" keywords.
END

Variable Exchange_Correlation
Type subsection
Section Scf
Description
 See Exchange_Correlation
END

Variable Diagonalize
Type subsection
Section Scf
Description
 See Diagonalize
END

Variable XCLibrary
Type string
Section Exchange_Correlation
Default Libxc
Description
 This is a keyword to select library set of exchange correlation functional.
Option Libxc
 Using xc library of <a href="http://www.tddft.org/programs/octopus/wiki/index.php/Libxc">"Libxc"</a><br/>
 The list of available functionals can be found in <a href="http://www.tddft.org/programs/octopus/wiki/index.php/Libxc_functionals">here</a> or the list in <a href="http://bigdft.org/Wiki/index.php?title=XC_codes">BigDFT website</a>.
 Meta-GGA functionals are not implemented.<br/>
 Reference :  Miguel A. L. Marques, Micael J. T. Oliveira, and Tobias Burnus, <i>Comput. Phys. Commun.</i> <b>183</b>, 2272 (2012)
Option XCFun
 Using xc library of <a href="https://github.com/dftlibs/xcfun">"XCFun"</a><br/>
 Only the LDA functionals and GGA functional without NLCC are tested.<br>
 Depreciated. <br>
 Reference :  Ulf Ekström, Lucas Visscher, Radovan Bast, Andreas J. Thorvaldsen and Kenneth Ruud, <i>J. Chem. Theory Comput.</i> <b>6</b>, 1971 (2010)
END

Variable XCFunctional
Type int
Section Exchange_Correlation
Description
 This is a keyword to specify exchange-correlation functional. See the list in <a href="http://bigdft.org/Wiki/index.php?title=XC_codes">BigDFT website</a>. </br>
 We accept the Libxc functional codes in string or integer, such as LDA_X (or 1), GGA_X_PBE (or 101), etc.
 Here are some examples.
Option 160
 GGA, van Leeuwen&Baerends functional (LB)
Option 160
 Becke97, GGA
Option 402
 B3LYP Hybrid functional(not recomended)
Option 406
 PBEH or PBE0 or PBE1PBE Hybrid functional
Option 433
 CAM_B3LYP Hybrid functional(not tested)
END

Variable XFunctional
Type int, string
Section Exchange_Correlation
Description
 This is a keyword to specify exchange functional. See the list in <a href="http://bigdft.org/Wiki/index.php?title=XC_codes">BigDFT website</a>. </br>
 We accept the Libxc functional codes in string or integer, such as LDA_X (or 1), GGA_X_PBE (or 101), etc.
 Also, so-called OEP method can be used for exchange functional.</br>
 The functionals can be mixed using the input like this:</br>
    XFunctional     '106 0.72'</br>
    XFunctional     '-12 0.2'</br>
    XFunctional     '1 0.08'</br>
 which means 0.72*(Becke 88 exchange) + 0.20*(KLI exchange) + 0.08*(LDA exchange), which corresponds to B3LYP's exchange part.
Option 1
 LDA exchange
Option 101
 PBE exchange (GGA)
Option 106
 Becke 88 exchange(GGA)
Option 109
 PW91 exchange (GGA)
Option -10
 No exchange (xc kernel : KLI-PGG kernel)
Option -11
 Slater exchange potential (xc kernel : KLI-PGG kernel)
Option -12
 OEP exact exchange potential with KLI approximation (xc kernel : KLI-PGG kernel)
Opteion -13
 OEP exact exchange potential (not implemented)
END

Variable CFunctional
Type int, string
Section Exchange_Correlation
Description
 This is a keyword to specify exchange functional.See the list in <a href="http://bigdft.org/Wiki/index.php?title=XC_codes">BigDFT website</a>. </br>
 We accept the Libxc functional codes in string or integer, such as LDA_X (or 1), GGA_X_PBE (or 101), etc.
 The functionals can be mixed using the input like this:</br>
  CFunctional     '131 0.81'</br>
  CFunctional     '7 0.19'</br>
 , means 0.81*(LYP correlation) + 0.19*(VWN-5 correlation), corresponds to B3LYP functional's correlation part with VWN5 correlation.
Option 7
 VWN-5 correlation (LDA)
Option 8
 VWN-RPA correlationi (LDA)
Option 9
 Perdew-Zunger LDA correlation
Option 130
 PBE correlation (GGA)
Option 131
 LYP correlation (GGA)
Option 134
 PW91 correlation (GGA)
Option -30
 No correlation. Removed in this version.
END

Variable Gaussian_potential
Type string (Space separated list of real numbers)
Section Exchange_Correlation
Description
 This keyword defines the N-Gaussian modification of range-separated erf(mu r)/r kernel.<br>
 If this keyword is defined, KLI coulomb potential will be replaced to the N-gaussian potential.<br>
 This keyword defines the contraction coefficients and exponent in the form of "coeff exponent" for gaussian.
END

Variable CalGradientUsingDensity
Type int 
Section Exchange_Correlation
Description
Option 0
    [Default] density is calculated from Sinc interpolation of orbitals.
Option otherwise
    when non-zero value is given, gradient of density is computed from finite-difference approximation.
END


SectionInfo TDDFT
SectionDescription
 This section governes parameters related to TDDFT calculations.
END

Variable NumberOfStates
Type int
Section TDDFT
Description
 This varible controls the number of excitations computed by TDDFT.
END

Variable TheoryLevel
Type string
Section TDDFT
Description
 This parameter determines the way to solve TDDFT.
Option Casida
 [Default] It gives oscillator_strength and negative excitations are considered.
Option TammDancoff
 Negative excitations are ignored.
END

Variable SortOrbital
Type string
Section TDDFT
Description
 This parameter controls how to print components of
Option Order
 Print top few components. The number is given as MaximumOrder.
Option Tolerance
 Print out all components that are bigger than given criteria.
END

Variable MaximumOrder
Type int
Section TDDFT
Description
 This determines the number of printed orbital pairs. This keyword works only with SortOrbital=Sorting case.
END

Variable OrbitalTolerance
Type float
Section TDDFT
Description
 If the portion of certain orbital is larger than this critera, it will be printed.
END

Variable GradientMatrix
Type string
Section TDDFT
Description
 This parameter determines how to obtain the gradients of density and orbital.
Option Finite_Difference
 Use the finite difference method to obtain the gradient.
END

Variable DerivativesOrder
Type integer
Section TDDFT
Description
 Decides derivative order for calculating GradientMatrix.<br>
 Only relevant when GradientMatrix is Finite_Difference.
END

Variable Exchange_Correlation
Type subsection
Section TDDFT
Description
 Specifies TDDFT kernel.
END

Variable ExchangeKernel
Type string
Section TDDFT
Description
 Determines the exchange kernel for exact exchange.
Option PGG
 PGG kernel. Default value.
Option HF
 Use HF kernel.
Option HF_EXX
 Perform TDHF(EXX) scheme like J. Chem. Phys. 134, 034120 (2011).
END

Variable DeltaElement
Type integer
Section TDDFT
Description
 Specifies eigenvalue correction term. Only relevant if ExchangeKernel is HF_EXX.
Option 1
 Calculate as J. Chem. Phys. 134, 034120 (2011). Current default.
Option 2
 Include only diagonal elements for occupied orbital eigenvalue correction term. Future default.
Option 3
 Include only diagonal elements for occupied orbital eigenvalue correction term. Not implemented yet.
Option 0
 Do not include eigenvalue correction term. Identical to ExchangeKernel==HF and fallbacks to it.
END

Variable _TDClass
Type string
Section TDDFT
Description
 Debug input. Chooses the TDDFT calculation class.<br>
 If not specified (normal behavior), automatically choose appropreate routine.<br>
 If specified class cannot compute desired theory, calculation stops.
Option C
 Invoke Casida equation \f[ CZ = -\Omega^2 Z \f] computation routine. HF and KS-CI exchange kernels are not implemented yet.<br>
 This is chosen if the TheoryLevel is Casida and EXX exchange kernel is not HF or KS-CI.
Option ABBA
 Invoke TDDFT equation \f[(A* B* | B A) (X Y) = -\Omega (X Y) \f] computation routine.<br>
 This is chosen if the TheoryLevel is Casida and HF or KS-CI exchange kernel is used.<br>
 This class should be removed (with this input) when HF, KS-CI casida form is implemented.
Option TDA
 Invoke Tamm-Dancoff equation computation routine.<br>
 This is chosen is the TheoryLevel is TDA.
END

Variable Orbital_info
Type subsection
Section TDDFT
Description
 Contains exchange correlation functional which used to compute the orbitals.<br>
 Necessary if ExchangeKernel is KS-CI.<br>
 Should contain Exchange_Correlation as subsection.
END

Variable xc_kernel_without_EXX
Type subsection
Section TDDFT
Description
 Contains non-EXX part of exchange correlation kernel for TDDFT calculation.<br>
 Necessary if ExchangeKernel is KS-CI.<br>
 Should contain Exchange_Correlation as subsection.
END

Variable Exchange_Correlation
Type subsection
Section Orbital_info
Description
 Contains exchange correlation functional which used to compute the orbitals.
END

Variable Exchange_Correlation
Type subsection
Section xc_wo_EXX
Description
 Contains non-EXX part of exchange correlation kernel for TDDFT calculation.
END

Variable Mixing
Type subsection
Section Scf
Description
 See Section Mixing.
END

SectionInfo Mixing
SectionDescription
 This section governes parameters related to mixing methods. <br>
 Generates guess Hamiltonian for the next iteration from the output of previous iterations.
END

Variable MixingType
Type string
Section Mixing
Description
 Decieds the field that is to be mixed.
Option Density
 Mix density to generate guess Hamiltonian for the next step.<br>
 For PAW calculations, this mixes the PAW atom-centered density matrix.<br>
 We recommend this option for the PAW calculations.
Option Potential
 Mix potential to generate guess Hamiltonian for the next step.<br>
 We recommend this option for the KLI calculations.
END

Variable MixingMethod
Type integer
Section Mixing
Description
 Decides the mixing algorithm.
Option 0
 Linear mixing. <br>
 f^{n+1}_{in} = \alpha f^{n}_{out} + (1-\alpha) f^n_{in}.
Option 1
 Pulay mixing (DIIS).<br>
 See Kresse, Phys. Rev. B 54, 11169 (1996), or http://www.hector.ac.uk/cse/distributedcse/reports/conquest/conquest/node3.html.
Option 2
 Broyden mixing. <br>
 See Phys. Rev. C 78, 014318 (2008), section II.B, or Phys. Rev. B 38, 12807 (1998).
END

Variable MixingParameter
Type float
Section Mixing
Description
 Decides the mixing coefficient for the linear mixing.<br>
 As this variable approaches to 0, the mixing gives conservative guess. <br>
 If this variable is set to 0, the guess for the next iteration will be the same as the current iteration. <br>
 Setting this variable close to 1 will converge the calculation fast if the initial state is far from the converged state.
 However, the calculation may oscillate near the converged state, if this variable is too high. <br>
 Note that setting this value outside of 0-1 can work, but may not work.
END

Variable PulayMixingParameter
Type float
Section Mixing
Description
 Decides the mixing coefficient for the Pulay mixing.<br>
 This option works pretty much the same as MixingParameter. <br>
 Only difference is that this works for Pulay mixing.
END

Variable BroydenMixingParameter
Type float
Section Mixing
Description
 Decides the mixing coefficient for the Broyden mixing.<br>
 This option works pretty much the same as MixingParameter. <br>
 Only difference is that this works for Broyden mixing.
END

Variable Mixing_Start
Type integer
Section Mixing
Description
 Irrelevant to the linear mixing algorithm. <br>
 Pulay, or Broyden mixing will start when the iterations are performed for Mixing_Start times. <br>
 Mixing algorithm is linear before this value. <br>
 If this variable is smaller than Mixing_History, it will be adjusted to corresponding Mixing_History.
END

Variable Mixing_History
Type integer
Section Mixing
Description
 Irrelevant to the linear mixing algorithm. <br>
 Pulay, or Broyden mixing will use this amount of previous iteration informations. <br>
END

Variable Gradient
Type Int
Section TDDFT
Description
 Determine how to calculate gradient of pair density.<br>
 if the value is 0, gradient of pair density is calculated using gradient matrix, otherwise
 gradient of pair density is calculated using saved orbital gradient.
 Default value : 0
END

Variable Root
Type integer
Section TDDFT
Description
 Does not count excitations from the orbitals lower than the orbitals with this index.<br>
 Root-1 orbitals will be ignored.
END

Variable ErrorTermination
Type integer
Section Any computation routines
Description
 Defines behavior when the computation routine returned error code. Currently only works for SCF not converging.
Option -1
 Suppress any warning message and proceed further.
Option 0 (default)
 Displays warning message and proceed further.
Option 1
 Displays warning message and terminate program.
END


Variable MemorySaveMode
Type int
Section TDDFT
Description
 If 0, calculation of iajb-pair will use higher memory and lower communication time.
 Otherwise, it will use lower memory but needs more communication time.
END

Variable NumElectronsMode
Type string
Section Basic_Information
Option NumElectrons Specifies the total valence electrons.
 User should specify NumElectrons as a sum of number of valence electrons speficied in pseudopotential file.
 Charge input is ignored.
Option Charge Relative number of electrons respect to neutral case.
 NumElectrons input is overwritten.
END

Variable ExternalField
Type Subsection
Section Scf
Description
 Adds an additional field during the SCF run.
END

SectionInfo ExternalField
SectionDescription
 This section specifies the external field applied to the system.
END

Variable InputType
Type string
Section ExternalField
Description
 Specifies format. Required.
Option Analytic
 Apply potential in analytic form (Ex, Ey, or Ez where E is constant).
Option Read
 Specifies potential by numerical grid.
END

Varialbe ExternalField
Type string
Section ExternalField
Description
 Relevant only if InputType is Analytic. Only one default option is currently available.
Option Electric
 Only and default option.
END

Variable CalGradientUsingDensity
Type integer
Section Exchange_Correlation
Description
 Controls how density gradient calculation is done for GGA.
Option 0
 Uses orbitals to calculate density gradient. Less numerical problems with increased computatoiin time.<br>
 Recommended for LYP, LC-wPBE, etc.
Option 1
 Uses densities to calculate density gradient. More suceptable to numerical errors.<br>
 Does not produces NaN values for PBE, but often problematic for others.
END

Variable UpdatePawMatrix
Type integer
Section Mixing
Description
 Controls whether PAW atomcenter density matrix mixing. See PAW_Density mixing also.<br>
 Only works with density or potential mixing.
Option 0
 Do not update PAW atomcenter density matrix when mixing is done. Default for potential mixing. Only option for non-density non-potential mixing.
Option 1
 Update PAW atomcenter density matrix with same coefficient used to update mixing field. Default for density mixing.<br>
 In order to use same mixing scheme but different coefficient, use PAW_Density as mixing field.
END

Variable Erf_Gaussian
Type string
Section Exchange_Correlation
Description
 Fit omega to N-gau coeffieicnt and exponent. Use as \"0.4 2\" to expane omega=0.4 with 2 gaussians.<br>
 Currently only 2 gaussian expansion is supported.<br>
 Currently uses fitting coefficient from Song and Hirao, DOI: 10.1063/1.4932687 for omega=0.4 and scale the coefficients.
END
