#! /bin/bash

export ACE_PATH="../../ace"
for _INP in *.inp; do
    export INP=${_INP%%.inp}
    #echo ${INP}
    echo "${ACE_PATH} ${INP}.inp > ${INP}.log";
    time ${ACE_PATH} ${INP}.inp > ${INP}.log;
done
