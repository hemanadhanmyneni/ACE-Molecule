#!/bin/bash

#PBS -N H2force-test
#PBS -l nodes=intel9:ppn=20
#PBS -l walltime=500:00:00

date

export INPUT=H2force.inp
export OUTPUT=H2force.log
export EXEC=../../hs_file/programs/ACE-Molecule/ace

module purge
module load intel
module load intel_mkl
module load openmpi-1.8.3-intel
module load trilinos-12.8.1_openmp_release_intel_openmpi-1.8.3
module load gsl-1.16_intel_openmpi-1.8.3

cd $PBS_O_WORKDIR

echo `cat $PBS_NODEFILE`
cat $PBS_NODEFILE
NPROCS=`wc -l < $PBS_NODEFILE`
mpirun -machinefile $PBS_NODEFILE -np $NPROCS $EXEC $PBS_O_WORKDIR/$INPUT > $PBS_O_WORKDIR/$OUTPUT

date
