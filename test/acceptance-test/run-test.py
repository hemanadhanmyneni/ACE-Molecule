#-*- coding: utf-8 -*-
from __future__ import print_function
import time
import tempfile
import argparse
from sys import exit
from os.path import dirname, realpath
from os import environ
import numpy as np
from subprocess import Popen, CalledProcessError
from collections import OrderedDict
from script.ACE_read import ACE_reader

my_path = dirname(realpath(__file__))

result = {"energy": None, "time": None}
ref_tests = OrderedDict([
    ("UPF", "pp/He.upf.inp"),
])
pp_tests = OrderedDict([
    ("HGH", "pp/He.hgh.inp"),
    ("PAW", "pp/He.paw.inp"),
    ("Supersampling"    , "pp/He.dg.inp"),
    ("Filtering"        , "pp/He.filter.inp"),
    ("PAW Supersampling", "pp/He.paw.dg.inp"),
])
guess_tests = OrderedDict([
    ("Cube"        , "guess/He.cube.inp"),
    ("Grid Cutting", "guess/He.gridcutting.inp"),
    ("Hueckel"     , "guess/He.huckel.inp"),
])
basis_tests = OrderedDict([
    ("Grid Sphere", "basis/He.sphere.inp"),
    ("Grid Atoms" , "basis/He.atoms.inp"),
    ("Grid Atoms relative radius"      , "basis/He.atoms.rel.inp"),
    ("Type Points", "basis/He.points.inp"),
    ("Finite difference"               , "basis/He.fd.inp"),
    ("Kinetic Matrix Finite difference", "basis/He.kinetic.fd.inp"),
    ("AllowOddPoints", "basis/He.odd.inp"),
])
mixing_tests = OrderedDict([
    ("Broyden", "mixing/He.broyden.inp"),
    ("Linear" , "mixing/He.linear.inp"),
])
functional_tests = OrderedDict([
    ("B3LYP Orbital gradient", "functional/He.b3lyp.0.inp"),
    ("B3LYP Density gradient", "functional/He.b3lyp.1.inp"),
    ("B3LYP 0.5 portion", "functional/He.b3lyp.half.inp"),
    ("KLI", "functional/He.kli.inp"),
    ("PBE Orbital grandient", "functional/He.pbe.0.inp"),
    ("PBE Density gradient" , "functional/He.pbe.1.inp"),
    ("LC-wPBE(2gau) Orbital gradient", "functional/He.lc-2gau.0.inp"),
#    ("LC-wPBE(2gau) Density gradient", "functional/He.lc-2gau.1.inp"),
    ("LC-wPBE(2gau) Omega", "functional/He.lc-2gau.omega.inp"),
    ("LibXC LC-wPBE Orbital gradient", "functional/He.lc.0.inp"),
    ("B3LYP functional number", "functional/He.b3lyp.num.inp"),
    ("PBE functional number"  , "functional/He.xcnum.inp"),
])
tddft_tests = OrderedDict([
    ("ABBA", "td/He.td.abba.inp"),
    ("C", "td/He.td.c.inp"),
    ("Tamm-Dancoff", "td/He.td.tda.inp"),
    ("PBE", "td/He.td.pbe.inp"),
    ("PGG", "td/He.td.pgg.inp"),
    ("TDHF", "td/He.td.tdhf.inp"),
    ("TDHF hybrid", "td/He.td.tdhf-hyb.inp"),
    ("TDHF(EXX)", "td/He.td.tdhf-exx.1.inp"),
    ("TDHF(EXX)", "td/He.td.tdhf-exx.2.inp"),
    ("TDHF(EXX) hybrid", "td/He.td.tdhf-exx-hyb.inp"),
])
ci_tests = OrderedDict([
    ("CIS davidson", "ci/cis.davidson.inp"),
    ("CIS LOBPCG", "ci/cis.lobpcg.inp"),
    ("CISD daviason", "ci/cisd.davidson.inp"),
    ("CISD LOBPCG", "ci/cisd.lobpcg.inp"),
])
others_tests = OrderedDict([
    ("No CrsMatrix", "others/He.nomakeH.inp"),
    ("Solvation"  , "others/He.solv.inp"),
])
polarized_tests = OrderedDict([
    ("Triplet O2 UPF", "o2/O2.upf.inp"),
    ("Triplet O2 HGH", "o2/O2.hgh.inp"),
    ("Triplet O2 PAW", "o2/O2.paw.inp"),
])
occupation_tests = OrderedDict([
    ("O fractional occupation", "o2/O.occ.input.inp"),
    ("O2 Fermi occupation", "o2/O2.occ.fermi.inp"),
    ("O2 fractional occupation", "o2/O2.occ.input.inp"),
])
electron_input_tests = OrderedDict([
    ("He NumElectrons initialization", "num_e/He.nume.inp"),
    ("He+ NumElectrons initialization", "num_e/Hep.nume.inp"),
    ("O2 Polarize initialization", "num_e/O2.nume.inp"),
])

energy_tests = OrderedDict([
    ("Reference", ref_tests),
    ("Pseudopotential", pp_tests),
    ("InitialGuess", guess_tests),
    ("Basis", basis_tests),
    ("Mixing", mixing_tests),
    ("XC", functional_tests),
    ("Occupation", occupation_tests),
    ("PP with NL projectors", polarized_tests),
    ("# Electrons Initialization", electron_input_tests),
    ("ETC", others_tests),
])
force_tests = OrderedDict([
    ("H2", OrderedDict([("H2 force", "force/H2.force.inp"),])),
])
debug_tests = OrderedDict([
    ("Polarized", polarized_tests),
])
excited_tests = OrderedDict([
    ("TDDFT", tddft_tests),
    ("CI", ci_tests),
])

def get_reference_values(tests, quantity):
    ace_reader = ACE_reader()
    reference_values = {}
    for section, test in tests.items():
        #print("[{}]".format(section))
        reference_values[section] = {}
        for name, inp in test.items():
            fname = ".".join("/".join(inp.split("/")[1:]).split(".")[:-1])
            log = "log.ref/{}.ref".format(fname)
            log = "/".join([my_path, log])
            #print("{}: {} Ha".format(name, ace_reader.read_ACE_out(log)))
            reference_values[section][name] = ace_reader.read_ACE_out(log, quantity = quantity)
    return reference_values

e_ref_val = get_reference_values(energy_tests, "energy")
force_ref_val = get_reference_values(force_tests, "forces")
test_info = [
   {
       #"name": "SCF",
       "testset": energy_tests,
       "quantity": "energy",
       "tolerance": (5.0e-4, 1.0e-5),
       #"ref_time": 163,# without electron_input
       #"ref_time": 208,
       "ref_time": 349,
       "ref_val": e_ref_val,
   },
   {
       #"name": "Force",
       "testset": force_tests,
       "quantity": "forces",
       "tolerance": (1.0e-2, 1.0e-4),
       "ref_time": 35,
       "ref_val": force_ref_val,
   },
]

def run(tests_set, temp_log = True, target_test = None):
    ace_reader = ACE_reader()
    num_passed = 0
    num_test = 0
    my_env = environ.copy()
    my_env['OMP_NUM_THREADS'] = '1'
    for tests in tests_set:
        g_st = time.time()
        for section, test in tests["testset"].items():
            print('\033[1;37m', end='')
            print("[{}]".format(section))
            print('\033[0;0m', end='')
            for name, inp in test.items():
                file_name = '.'.join(inp.split('/')[-1].split('.')[:-1])
                if target_test is not None and (name != target_test and file_name != target_test):
                    continue
                inp = "/".join([my_path, inp])
                with open(inp) as fr:
                    print(inp)
                    print(fr.readline().strip())
                log_name = file_name+".log"
                with tempfile.NamedTemporaryFile() if temp_log else open(log_name, 'w') as toutput:
                    st = time.time()
                    calc = Popen(["../../ace", inp], stdout=toutput, cwd=my_path, env=my_env)
                    calc.wait()
                    et = time.time()
                    time_elapsed = et-st
                    num_test += 1
                    if calc.returncode != 0:
                        print("\033[1;31mError Termination at test {}!\033[0;0m".format(name))
                        print('\033[1;31m', end='')
                        print("TEST: FAILED (Error Termination)", end=' ')
                        print('\033[0;31m', end='')
                        print("with error code {} in {} sec.".format(calc.returncode, time_elapsed))
                        print('\033[0;0m', end='')
                        continue
                    energy = ace_reader.read_ACE_out(toutput.name, quantity = tests["quantity"])
                    print("{}: {} (Ref. {})".format(name, energy, tests['ref_val'][section][name]))
                    #print(type(energy), isinstance(energy, list), isinstance(energy, np.ndarray))
                    if isinstance(energy, np.ndarray):
                        energy_diff = np.sum(np.abs(energy - tests['ref_val'][section][name]))
                    else:
                        energy_diff = abs(energy - tests['ref_val'][section][name])
                    if energy_diff < tests["tolerance"][0]:
                        print('\033[1;32m', end='')
                        print("TEST: PASSED", end=' ')
                        print('\033[0;32m', end='')
                        print("with difference {} Ha in {} sec.".format(energy_diff, time_elapsed))
                        print('\033[0;0m', end='')
                        num_passed += 1
                    elif energy_diff < tests["tolerance"][1]:
                        print('\033[1;33m', end='')
                        print("TEST: PASSED", end=' ')
                        print('\033[0;33m', end='')
                        print("with difference {} Ha in {} sec.".format(energy_diff, time_elapsed))
                        print('\033[0;0m', end='')
                    else:
                        print('\033[1;31m', end='')
                        print("TEST: FAILED", end=' ')
                        print('\033[0;31m', end='')
                        print("with difference {} Ha in {} sec.".format(energy_diff, time_elapsed))
                        print('\033[0;0m', end='')
                    print()
        g_et = time.time()
        total_time = g_et - g_st
        if total_time < 2 * tests["ref_time"]:
            print('\033[1;32m', end='')
            print("Total time: {} sec".format(total_time))
            print('\033[0;0m', end='')
        else:
            print('\033[1;31m', end='')
            print("Total time: {} sec is too slow!".format(total_time))
            print('\033[0;0m', end='')
    if num_passed == num_test:
        print('\033[1;32m', end='')
        print("Total {} tests passed.".format(num_passed))
        print('\033[0;0m', end='')
    else:
        print('\033[1;31m', end='')
        print("Only {}/{} tests passed.".format(num_passed, num_test))
        print('\033[0;0m', end='')
    Popen("rm -f cavity.off__[0-9]* PEDRA.OUT__[0-9]*", shell=True, cwd=my_path)
    return num_test - num_passed, total_time

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--name", dest="testname", default=None, help="If specified, only that test will run.")
    group = parser.add_mutually_exclusive_group()
    group.add_argument("--temp", dest="is_temporary", default=True, action='store_true', help="Remove log files.")
    group.add_argument("--notemp", dest="is_temporary", action='store_false', help="Leave log files.")

    args = parser.parse_args()
    code, time = run(test_info, temp_log = args.is_temporary, target_test = args.testname)
    if code > 0:
        exit(1)
