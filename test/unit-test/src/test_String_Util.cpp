#include <string>
#include <vector>
#include "../../../source/Util/String_Util.hpp"
#include "gtest/gtest.h"

using std::string;

TEST(TEST_String_Util, Split_ws){
    std::string test_string = "a b\fa\nb\ra\tb\va";
    std::vector<std::string> rv = String_Util::Split_ws(test_string);
    EXPECT_EQ(rv[0], string("a"));
    EXPECT_EQ(rv[1], string("b"));
    EXPECT_EQ(rv[2], string("a"));
    EXPECT_EQ(rv[3], string("b"));
    EXPECT_EQ(rv[4], string("a"));
    EXPECT_EQ(rv[5], string("b"));
    EXPECT_EQ(rv[6], string("a"));
}

int main(int argc, char **argv){
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
