#include "../../../source/Basis/Basis_Function/Sinc.hpp"
#include "gtest/gtest.h"

#include <iostream>

TEST(TEST_Sinc, compute_kinetic){

    int* points = new int[3];
    double* scaling = new double[3];
    int periodicity;
    
    points[0] = 10; // only used to test
    points[1] = 10;
    points[2] = 10;
    scaling[0] = 0.7; // only used to test
    scaling[1] = 0.7;
    scaling[2] = 0.7; 
    
    // (1st case) periodicity == 0

    // If there is no pbc, the grid shape is some different from that with pbc.
    // So grid scaling is differnet from that with pbc even if they have the same points.
    double* scaling_0d = new double[3];
    scaling_0d[0] = 0.777778;
    scaling_0d[1] = 0.777778;
    scaling_0d[2] = 0.777778;
    periodicity = 0;
    double true_val_0d[3][points[0]] =
    {
        {2.71917672275,-1.65306122449,0.413265306122,-0.183673469388,0.103316326531,-0.0661224489796,0.0459183673469,-0.0337359433569,0.0258290816327,-0.0204081632653},
        {2.71917672275,-1.65306122449,0.413265306122,-0.183673469388,0.103316326531,-0.0661224489796,0.0459183673469,-0.0337359433569,0.0258290816327,-0.0204081632653},
        {2.71917672275,-1.65306122449,0.413265306122,-0.183673469388,0.103316326531,-0.0661224489796,0.0459183673469,-0.0337359433569,0.0258290816327,-0.0204081632653}
    };

    //Sinc sinc = Sinc(points, scaling, periodicity));
    Sinc sinc = Sinc(points, scaling_0d);
    //sinc->compute_kinetic(i,j,axis)

    for(int axis=0; axis<3; axis++){
        for(int j=0; j< points[0]; j++){
            int digit= 5;
            EXPECT_EQ(round(pow(10,digit)*true_val_0d[axis][j]), round(pow(10,digit)*sinc.compute_kinetic(0,j,axis)));
        }
    }

    /*

    // (2nd case) periodicity == 1
    periodicity = 1;
    double true_val_1d[3][points[0]] = 
    {
        {3.42414846568,-2.10930283899,0.58299696618,-0.30774313679,0.22268502574,-0.20142049798,0.22268502574,-0.30774313679,0.58299696618,-2.10930283899},
        {3.35700829969,-2.04081632653,0.510204081633,-0.226757369615,0.127551020408,-0.0816326530612,0.0566893424036,-0.0416493127863,0.031887755102,-0.0251952632905},
        {3.35700829969,-2.04081632653,0.510204081633,-0.226757369615,0.127551020408,-0.0816326530612,0.0566893424036,-0.0416493127863,0.031887755102,-0.0251952632905}
    };

    Sinc sinc = Sinc(points, scaling, periodicity);
    for(int axis=0; axis<3; axis++){
        for(int j=0; j< points[0]; j++){
            EXPECT_EQ(true_val_1d[axis][j], sinc.compute_kinetic(0,j,axis));
        }
    }

    // (3rd case) periodicity == 2
    periodicity = 2;
    double true_val_2d[3][points[0]] = 
    {
        {3.42414846568,-2.10930283899,0.58299696618,-0.30774313679,0.22268502574,-0.20142049798,0.22268502574,-0.30774313679,0.58299696618,-2.10930283899},
        {3.42414846568,-2.10930283899,0.58299696618,-0.30774313679,0.22268502574,-0.20142049798,0.22268502574,-0.30774313679,0.58299696618,-2.10930283899},
        {3.35700829969,-2.04081632653,0.510204081633,-0.226757369615,0.127551020408,-0.0816326530612,0.0566893424036,-0.0416493127863,0.031887755102,-0.0251952632905}
    };
    Sinc sinc = Sinc(points, scaling, periodicity);
    for(int axis=0; axis<3; axis++){
        for(int j=0; j< points[0]; j++){
            EXPECT_EQ(true_val_2d[axis][j], sinc.compute_kinetic(0,j,axis));
        }
    }
    
    // (4th case) periodicity == 3
    periodicity =3; 
    double true_val_3d[3][points[0]] = 
    {
        {3.42414846568,-2.10930283899,0.58299696618,-0.30774313679,0.22268502574,-0.20142049798,0.22268502574,-0.30774313679,0.58299696618,-2.10930283899},
        {3.42414846568,-2.10930283899,0.58299696618,-0.30774313679,0.22268502574,-0.20142049798,0.22268502574,-0.30774313679,0.58299696618,-2.10930283899},
        {3.42414846568,-2.10930283899,0.58299696618,-0.30774313679,0.22268502574,-0.20142049798,0.22268502574,-0.30774313679,0.58299696618,-2.10930283899}
    };
    Sinc sinc = Sinc(points, scaling, periodicity);
    for(int axis=0; axis<3; axis++){
        for(int j=0; j< points[0]; j++){
            EXPECT_EQ(true_val_3d[axis][j], sinc.compute_kinetic(0,j,axis));
        }
    }

    */
}

int main(int argc, char **argv){
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
