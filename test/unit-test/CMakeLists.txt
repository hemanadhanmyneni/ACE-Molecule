cmake_minimum_required(VERSION 2.8.2)

cmake_policy(SET CMP0048 NEW)
enable_testing()
# Download and unpack googletest at configure time
configure_file(CMakeLists.txt.in googletest/CMakeLists.txt)
execute_process(COMMAND ${CMAKE_COMMAND} -G "${CMAKE_GENERATOR}" .
  RESULT_VARIABLE result
  WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/googletest )
if(result)
  message(FATAL_ERROR "CMake step for googletest failed: ${result}")
endif()
execute_process(COMMAND ${CMAKE_COMMAND} --build .
  RESULT_VARIABLE result
  WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/googletest )
if(result)
  message(FATAL_ERROR "Build step for googletest failed: ${result}")
endif()

# Prevent overriding the parent project's compiler/linker
# settings on Windows
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)

# Add googletest directly to our build. This defines
# the gtest and gtest_main targets.
add_subdirectory(${CMAKE_BINARY_DIR}/googletest/googletest-src
                 ${CMAKE_BINARY_DIR}/googletest/googletest-build
                 EXCLUDE_FROM_ALL)

# The gtest/gtest_main targets carry header search path
# dependencies automatically when using CMake 2.8.11 or
# later. Otherwise we have to add them here ourselves.
if (CMAKE_VERSION VERSION_LESS 2.8.11)
  include_directories("${gtest_SOURCE_DIR}/include")
endif()

INCLUDE(../../Find_Trilinos.cmake)
#INCLUDE(../../FileLists.cmake)

SET(FILE_LISTS
    source/Util/String_Util.cpp
    source/Util/ParamList_Util.cpp
    source/Util/Parallel_Manager.cpp
    source/Util/Verbose.cpp
    source/Util/Read_Upf.cpp
    source/Util/Read_Hgh.cpp
    source/Util/Read_Hgh_Internal.cpp
    source/Io/Periodic_table.cpp
    source/Io/Atom.cpp
    source/Io/Atoms.cpp
    source/Basis/Basis_Function/Basis_Function.cpp
)

FUNCTION(PREPEND var prefix)
    SET(listVar "")
    FOREACH(f ${${var}})
        LIST(APPEND listVar "${prefix}/${f}")
    ENDFOREACH(f)
    SET(${var} "${listVar}" PARENT_SCOPE)
ENDFUNCTION(PREPEND)

PREPEND(FILE_LISTS "../..")
#list(TRANSFORM FILE_LISTS PREPEND "../")
#MESSAGE("${FILE_LISTS}")

include_directories("${Trilinos_INCLUDE_DIRS}")
link_directories("${Trilinos_LIBRARY_DIRS}")

# Now simply link against gtest or gtest_main as needed. Eg
add_executable(test-utils src/test_String_Util.cpp ../../source/Util/String_Util.cpp)
target_link_libraries(test-utils gtest_main ${GTEST_LDFLAGS})
target_compile_options(test-utils PUBLIC ${GTEST_CFLAGS})
add_test(NAME utils COMMAND test-utils)

# Now simply link against gtest or gtest_main as needed. Eg
add_executable(test-io src/test_Io_Input.cpp ../../source/Io/Io_Input.cpp ${FILE_LISTS})
target_link_libraries(test-io gtest_main ${Trilinos_LIBRARIES} ${Trilinos_TPS_LIBRARIES} ${GTEST_LDFLAGS})
target_compile_options(test-io PUBLIC ${GTEST_CFLAGS})
add_test(NAME io COMMAND test-io)

# Now simply link against gtest or gtest_main as needed. Eg
add_executable(test-basis src/test_Sinc.cpp ../../source/Basis/Basis_Function/Sinc.cpp ${FILE_LISTS})
target_link_libraries(test-basis gtest_main ${Trilinos_LIBRARIES} ${Trilinos_TPS_LIBRARIES} ${GTEST_LDFLAGS})
target_compile_options(test-basis PUBLIC ${GTEST_CFLAGS})
add_test(NAME basis COMMAND test-basis)

#add_custom_target(run-test-basis COMMAND ./test-basis)
#add_custom_target(run-test-io COMMAND ./test-io)
#add_custom_target(run-test-utils COMMAND ./test-utils)
#add_custom_target(run-test DEPENDS run-test-io run-test-utils run-test-basis)
