#INCLUDE(${CMAKE_SOURCE_DIR}/ProjectName.cmake)

# CMAKE File for "MyApp" application building against an installed Trilinos

#This file was created by modifiying the files in
#Trilinos/demos/buildAgaintsTrilinos. The primary change was to make it a single
#file cmake system in a flat directory. If you would like to run a cmake
#configure using this file you should grab this file and src_file.cpp,
#src_file.hpp, main_file.cpp from buildAgainstTrilinos and place them in a new
#directory. From there you can run:
#"cmake -DTrilinos_PREFIX=<path to trilinos>." to configure. Another
#important change is the buildAgainstTrilinos does some checking to see which
#packages and tpls are enabled and behaves accordingly. However, this file does
#only a serial configure(no mpi) and assumes that the install of Trilinos it is
#pointed to has Epetra enabled. 

# Use Trilinos_PREFIX, if the user set it, to help find Trilinos.
# The final location will actually be held in Trilinos_DIR which must
# point at "<prefix>/lib/cmake/Trilinos", but this helps the search.
SET(CMAKE_PREFIX_PATH ${Trilinos_PREFIX} ${CMAKE_PREFIX_PATH})

# Get Trilinos as one entity
#FIND_PACKAGE(Trilinos COMPONENTS Anasazi Ifpack AztecOO EpetraExt Teuchos Epetra  PATHS ${TRILINOS_PATH})
#FIND_PACKAGE(Trilinos COMPONENTS Anasazi Ifpack AztecOO EpetraExt Teuchos Epetra  PATHS ${TRILINOS_PATH} NO_DEFAULT_PATH NO_CMAKE_ENVIRONMENT_PATH NO_CMAKE_PATH NO_CMAKE_PACKAGE_REGISTRY NO_CMAKE_BUILDS_PATH NO_CMAKE_SYSTEM_PATH NO_CMAKE_SYSTEM_PACKAGE_REGISTRY )
#FIND_PACKAGE(Trilinos COMPONENTS Anasazi Ifpack AztecOO EpetraExt Teuchos Epetra  PATHS ${TRILINOS_PATH} NO_DEFAULT_PATH)
FIND_PACKAGE(Trilinos COMPONENTS Anasazi EpetraExt Teuchos Epetra Ifpack ML PATHS ${TRILINOS_PATH} NO_DEFAULT_PATH)

# Echo trilinos build info just for fun
#MESSAGE("\nDebug information useful for wreck stupid FIND_PACKAGE function: ")
#MESSAGE("   CMAKE_PREFIX_PATH = ${CMAKE_PREFIX_PATH}")
#MESSAGE("   CMAKE_FRAMEWORK_PATH = ${CMAKE_FRAMEWORK_PATH}")
#MESSAGE("   CMAKE_APPBUNDLE_PATH = ${CMAKE_APPBUNDLE_PATH}")
#MESSAGE("   Trilinos_PREFIX = ${Trilinos_PREFIX}")
#MESSAGE("   TRILINOS_PATH = ${TRILINOS_PATH}")
MESSAGE("\nFound Trilinos!  Here are the details: ")
MESSAGE("   Trilinos_DIR = ${Trilinos_DIR}")
MESSAGE("   Trilinos_VERSION = ${Trilinos_VERSION}")
MESSAGE("   Trilinos_PACKAGE_LIST = ${Trilinos_PACKAGE_LIST}")
MESSAGE("   Trilinos_LIBRARIES = ${Trilinos_LIBRARIES}")
MESSAGE("   Trilinos_INCLUDE_DIRS = ${Trilinos_INCLUDE_DIRS}")
MESSAGE("   Trilinos_LIBRARY_DIRS = ${Trilinos_LIBRARY_DIRS}")
MESSAGE("   Trilinos_TPL_LIST = ${Trilinos_TPL_LIST}")
MESSAGE("   Trilinos_TPL_INCLUDE_DIRS = ${Trilinos_TPL_INCLUDE_DIRS}")
MESSAGE("   Trilinos_TPL_LIBRARIES = ${Trilinos_TPL_LIBRARIES}")
MESSAGE("   Trilinos_TPL_LIBRARY_DIRS = ${Trilinos_TPL_LIBRARY_DIRS}")
MESSAGE("   Trilinos_BUILD_SHARED_LIBS = ${Trilinos_BUILD_SHARED_LIBS}")
MESSAGE("End of Trilinos details\n")

