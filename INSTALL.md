# Installing ACE-Molecule

## Basic Instruction

Compiling ACE-Molecule requires CMake version 2.8 or higher. Like other project using CMake, installation is done by:
```
cd PATH/TO/ACE-Molecule
cmake [options] .
make -j4
# make install is not required.
```

ACE-Molecule requires some arguments for invoking CMake. See *Example CMake commands* and *CMake argument description* below. 
Also, we recommend parallel make (using argument -j). Optimal value of threads depend on your number of processors.
If you successfully compiled ACE-Molecule, you will get execuatable file named *ace*. ACE-Molecule is single file executable and this can be freely moved to other path.

To clean the project, invoke:
```
find -iname '*cmake*' -not -name CMakeLists.txt -not -name Find_Trilinos.cmake -not -name Version.cmake -not -name FileLists.cmake  -exec rm -rf {} \+
rm Makefile ace
```

To compile the developer API document, invoke:
```
doxygen Doxyfile
```
To clean the developer API document, invoke:
```
rm -r doxygen
```


## Dependency list
### Mandatory
 - Trilinos. Version 12.8.1 and 12.0.1 is tested. Note that the compiler for ACE-Molecule is fixed to the compiler which compiled the Trilinos library. Available at https://trilinos.org/download/public-git-repository/ .
 - Libxc Version 4.0 or higher is required. Available at http://www.tddft.org/programs/libxc/download/ .
 - GNU scientific library (GSL, will be dropped). Available at https://www.gnu.org/software/gsl/ .
### Optional
 - libxml2 for PAW support.
 - PCMSolver for PCM solvation support. Available at https://pcmsolver.readthedocs.io/en/stable/ .
 - MKL for deflation lanczos diagonalization support and performance.

## CMake argument descriptions
 - TRILINOS_PATH, LIBXC_PATH, GSL_PATH: Path to corresponding library.
 - MKL_PATH: Path to MKL. If present, deflation lanczos will be enabled by default.
 - PCMSOLVER_PATH: Path to PCMSolver. If present, PCM solvation will be enabled by default.
 - LIBXML2_PATH: Path to libxml2. If present or libxml2 is detected, PAW will be enabled by default.
 - ENABLE_CI: Compile CI routines.
 - ENABLE_EX_DIAG: Compile experimental diagonalization routines.
 - DEFLATION_LANCZOS: Compile deflation lanczos.

## Example CMake commands
### With MKL
```
cmake \
-D TRILINOS_PATH:PATH=/path/to/trilinos/ \
-D LIBXC_PATH:PATH=/path/to/libxc/ \
-D GSL_PATH:PATH=/path/to/gsl/ \
-D CMAKE_CXX_FLAGS:STRING="-g -std=c++11 -Wall -Wextra -O3" \
-D PCMSOLVER_PATH:PATH=/path/to/pcmsolver/root \
-D MKL_PATH:PATH=/path/to/mkl/ \
-D DEFLATION_LANCZOS:BOOL=TRUE \
-D ENABLE_CI:BOOL=TRUE \
-D ENABLE_EX_DIAG:BOOL=TRUE \
./   \
```

### Without MKL
```
cmake \
-D LIBXC_PATH:PATH=/path/to/libxc/ \
-D TRILINOS_PATH:PATH=/path/to/trilinos/ \
-D CMAKE_CXX_FLAGS:STRING="-g -std=c++11 -O3 -Wall -Wextra -Wno-sign-compare -I/usr/include/mpi" \
-D GSL_PATH:PATH=/path/to/gsl/ \
-D DEFLATION_LANCZOS:BOOL=FALSE \
-D ENABLE_CI:BOOL=FALSE \
./
```

### With CUDA
```
cmake \
-D LIBXC_PATH:PATH=/path/to/libxc/ \
-D CMAKE_CXX_FLAGS:STRING="-g -std=c++11 -Wall -Wextra -fPIC" \
-D CUDA_TOOLKIT_ROOT_DIR:PATH=/path/to/cuda/  \
-D CUDA_FLAGS:STRING=--cudart=shared \
-D GSL_PATH:PATH=/path/to/gsl/ \
-D DEFLATION_LANCZOS:BOOL=FALSE \
-D MKL_PATH:PATH=/path/to/mkl/ \
-D MAGMA_PATH:PATH=/path/to/magma/ \
-D TRILINOS_PATH:PATH=/path/to/trilinos/ \
./ 
```
